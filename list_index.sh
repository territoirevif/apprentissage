# Dresse la liste de tous ids de l'index apprentissage dans Elastic

host="http://localhost:9200"
user="elastic"
pwd="Ox-JoQT0po=pdOT-LeE*"

index="apprentissage"
cur_url="$host/$index/_search?pretty=true"

requete_de_liste='{
 "query": {
    "match_all": {
    }
  },
  
  "size": 9000, 
  "stored_fields": ["_id"]
}'

echo "Comparaison des fichiers d'apprentissage avec les id des documents de l'index '$index' d'Elastic :"
echo -e "\t> les fichiers qui n'ont pas encore été indexés"
echo -e "\t< un id dans l'index $index d'Elastic qui ne correspond à aucun fichier connu"

if ! ingest=$(curl -s -X GET -H "Content-Type: application/json" -u "$user:$pwd" -d "$requete_de_liste" "$cur_url"); then
  echo "Echec de la requête donnant la liste des id de documents dans l'index Elastic $index" >&2
  exit $?
fi

liste_ids_lines=$(echo "$ingest" | grep -F "_id" | cut -d'"' -f4 | sort)
liste_fichiers=$(find $APPRENTISSAGE_HOME -type f -name '*.pdf' -exec basename -s .pdf {} \; | sort)

difference=$(diff <( echo "$liste_ids_lines" ) <( echo "$liste_fichiers" ))
echo "$difference"

