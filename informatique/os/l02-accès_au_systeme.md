---
title: "l02. L'accès au système Linux"
subtitle: ""
author: Marc Le Bihan
keywords:
- terminal
- port série
- suite d'échappement
- escape code
- émulateur de terminal
- terminal X
- telnet
- telecommunication network
- secure shell
- ssh
- domain name system
- dns
- /etc/hosts
- hosts
- ftpd, smtpd, sshd, httpd
- daemon
- host (commande bash)
- nslookup (commande bash)
- dig (commande bash)
- X-Windows
---

# I) La connexion directe du poste de travail

Les terminaux étaient autrefois connectés à l'unité centrale via des ports série, ce qui limitait leur nombre.

  - passifs, ils affichaient ce qu'ils recevaient à l'écran, émettaient ce qu'on tapait au clavier
  - indépendants du matériel connecté, ils étaient commandés par "_une suite d'échappement_"

\bigbreak

Puis un micro-ordinateur a pris leur place, avec une carte graphique et un logiciel appelé _émulateur de terminal_. Il y en a trois types :

   - le terminal __ascii__ : `vt100`, `vt220`, `vt340`... très connu
   - les terminaux __graphiques__ : monochromes ou couleur  
   - les terminaux __X__ : ils respectent le protocole graphique `X-Windows` (rien à voir avec _Windows_). Ils ont existé par le passé : munis d'une carte graphique, capables d'afficher des fenêtres, mais dépourvus de disques durs.

\bigbreak

Le terminal sous __Linux__ est la console. C'est un terminal émulé.

# II) La connexion par le réseau

Les terminaux avec leurs propres cartes Ethernet sont apparus, et ont permis de joindre n'importe quel serveur sur le réseau grâce au protocole _Telecomunication Network (TELNET)_, qui s'appuie sur le protocole de réseau virtuel _Network Virtual Terminal (NVT)_ et celui _Secure Shell (SSH)_.

Se connecter par Internet oblige à connaître les notions d'adressage IP (_Internet Protocol_) et de _Domain Name System (DNS)_.

un fichier \fonction{/etc/hosts} assure une correspondance adresse IP $\to$ nom textuel :

   \code

   > ```bash
   > cat /etc/hosts
   > 127.0.0.1	localhost
   > 127.0.1.1	debian
   > ```

   \endgroup

\bigbreak

Mais comme ce fichier ne peut pas contenir toutes les machines de l'Internet, on le limite souvent aux machines géographiquement proches, ou souvent utilisées.

Pour décoder les autres adresses IP, ce sera le DNS (ou plutôt : une hiérarchie de DNS) qui s'en chargera.  
une commande \fonction{host} ou \fonction{nslookup} ou \fonction{dig} permettent de l'interroger.

   \code

   > ```bash
   > host www.mit.edu
   > ``` 

   > ```log
   > www.mit.edu is an alias for www.mit.edu.edgekey.net.
   > www.mit.edu.edgekey.net is an alias for e9566.dscb.akamaiedge.net.
   > e9566.dscb.akamaiedge.net has address 104.69.7.38
   > e9566.dscb.akamaiedge.net has IPv6 address 2a02:26f0:9100:8a0::255e
   > e9566.dscb.akamaiedge.net has IPv6 address 2a02:26f0:9100:8b8::255e
   > ```

   \endgroup

\bigbreak

La plupart des logiciels qui utilisent une adresse IP de type `http`, `smtp`, `ftp` fonctionnent en mode client-serveur. Mais _Telnet_ ou `ssh` font exception.

   - par exemple, un client Web interroge un serveur logiciel nommé \mauve{\texttt{httpd}}, le \donnee{d} signifiant `daemon`.  

   - respectivement, les clients `ftp` et `smtp` s'adressent à des serveurs \fonction{ftpd} et \fonction{smtpd}.

\bigbreak

Ce n'est pas l'utilisateur qui se connecte directement sur le serveur, mais un logiciel local (par exemple : un browser web) qui se connecte à un serveur local.

En revanche, avec _Telnet_ ou `ssh` (\mauve{\texttt{telnetd}} ou \mauve{\texttt{sshd}}), tout ce qui est saisi au clavier est transmis au serveur distant sans interprétation locale.

\pagebreak
# Questions de révision

1. Qu'est-ce qu'un terminal ?
Un clavier-écran sans UC qui reçoit des chaînes d'échappement.

2. Qu'est-ce qu'un émulateur de terminal ?
Un logiciel placé sur un ordinateur pour qu'il agisse comme un terminal.

3. Quel est le rôle d'un fichier DNS ?
Jouer le rôle d'un annuaire en donnant un nom "humain" aux adresses IP.

4. Quel fichier UNIX peut jouer le rôle d'un embryon de service DNS ?
le fichier /etc/hosts, qui contient une mise en correspondance IP, nom de domaine.

5. Quel est la différence entre le modèle client/serveur et le modèle "émulation de terminal" ?
Plusieurs clients se connectent à un serveur.
L'émulation de terminal ne concerne qu'un seul ordinateur.

# Exercices
1. Adresse IP de www.ibm.com
host  www.ibm.com
104.85.44.108

2. Donnez le nom des protocoles utilisés pour le transfert de fichiers et le transfert des courriers électroniques
FTP = File Transfert Protocol
SMTP = Simple Mail Transfert Protocol

3. Nom du terminal DEC qui sert de référence aux émulateurs
vt100

4. Que signifie IP ?
Internet Protocol

5. Comment s'appelle le protocole graphique utilisé sous UNIX ?
X-Windows

6. Que signifie le terme HTTP ?
Hyper text transfert protocol
