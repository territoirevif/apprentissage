---
title: "l04. Les fichiers et les répertoires" 
subtitle: ""

keywords:
   - répertoire
   - arborescence

author: Marc Le Bihan
---


# I) L'arborescence des répertoires Linux

Les plus habituels : toutes les distributions n'ont pas toujours les mêmes.

![arborescence](../images/linux_arborescence_repertoires.jpg)

\fonction{root} est le répertoire racine de l'administrateur (sauf sur AIX où c'est `/`).

D'autres répertoires, comme \fonction{/boot} sont aussi mappés différement sur AIX.

## 1) Les principaux répertoires Linux 

| Répertoires                   | Rôle ou contenu                                                 |
| ----------------------------- | --------------------------------------------------------------- |
| /usr/bin                      | Les commandes de base du système                                |
| /usr/local/bin                | Commandes supplémentaires ajoutées                              |
| /usr/sbin                     | Commandes d'administration                                      |
| ----------------------------- | --------------------------------------------------------------- |
| /boot                         | Les commandes du noyau Linux                                    |
| ----------------------------- | --------------------------------------------------------------- |
| /dev                          | Les fichiers liés aux périphériques                             |
| ----------------------------- | --------------------------------------------------------------- |
| /etc/rc.d                     | Les répertoires de configuration du système                     |
| /etc/init.d                   | Sous-répertoire de démarrage des services Linux                 |
| ----------------------------- | --------------------------------------------------------------- |
| /home                         | Répertoire racine de tous les sous-répertoires des uilisateurs  |
| ----------------------------- | --------------------------------------------------------------- |
| /lib                          | Bibliothèques de sous-programmes ds développements              |
| ----------------------------- | --------------------------------------------------------------- |
| /media/cdrom                  |  Accès aux périphériques CDROM                                  |
| /media/usb                    |  USB                                                            |
| ----------------------------- | --------------------------------------------------------------- |
| /mnt/windows                  | Accès à une partition Windows                                   |
| ----------------------------- | --------------------------------------------------------------- |
| /opt                          | Contient des progiciels                                         |
| ----------------------------- | --------------------------------------------------------------- |
| /proc                         | Répertoire dédié aux processus                                  |
| ----------------------------- | --------------------------------------------------------------- |
| /root                         | Répertoire personnel de l'administrateur                        |
| ----------------------------- | --------------------------------------------------------------- |
| /tmp                          | Répertoire des fichiers temporaires, à nettoyer régulièrement   |
| ----------------------------- | --------------------------------------------------------------- |
| /usr                          | Principal répertoire du système Linux et des sous-systèmes      |
| /usr/include                  | Répertoire des fichiers d'entête                                |
| /usr/share/man                | Répertoire des documentations utilisateur                       |
| /usr/X11R6                    | Répertoire des fichiers liés au protocole X11                   |
| /usr/local                    | Sous-répertoire des logiciels installés localement              |
| ----------------------------- | --------------------------------------------------------------- |
| /var                          | Sous-répertoire contenant la partie "variable" du système       |
| /var/log                      | Les logs                                                        |
| /var/spool                    | Files d'attentes                                                |
| /var/spool/mail               | Boites aux lettres des utilisateurs                             |
| /var/mail                     |                                                                 |
| /var/spool/lpd                | Sous-répertoire de gestion des files d'attente d'impression     |
| /var/spool/mqueue             | Sous-répertoire d'envoi des courriers                           |
| /var/yp                       | Gestion des annuaires (_Network Information Service (NIS)_)     |

Répertoire personnel : `home`  
partagé : `tmp`

## 2) Les types d'objets, listés par `ls -l`

| Code | Type d'élément                     |
| :--: | ---------------------------------- |
|  -   | Fichier                            |
|  d   | Répertoire                         |
|  $\ell$ | Lien symbolique                 |
|  b   | Périphérique bloc (disque)         |
|  c   | Périphérique caractères (terminal) |
|  s   | Canal de communication (socket)    |
|  p   | Pile FIFO \gris{(?)} (système)     |

### a) Les options de la commande `ls`

(`which ls` dira que c'est la commande `/usr/bin/ls` du système qui est utilisée).

Peut prendre des caractères jocker `*`, `?`, mais aussi : `[abc]` ou au contraire : `[!abc]`, `[0-9]`...

Quelques options de `ls`

| Opt  | Description                                                 |
| :--: | ----------------------------------------------------------- |
|  -a  | Montrer les fichiers invisibles                             |
|  -d  | Le nom d'un répertoire, mais pas son contenu, est présenté  | 
|  -R  | Parcours récursif des répertoires                           |
|  -i  | Afficher le inode du fichier                                |

# II) Copie ou déplacement de fichiers

## 1) Copie récursive

L'option `-R` ou `-r` d'une commande de copie créera le répertoire sur la destination, s'il manque.

   \code

   > ```bash
   > cp -R Rep1 /tmp
   > # équivaut à 
   > cp -R Rep1 /tmp/Rep1
   > ```

   \endgroup

Avec l'option `-R` les fichiers spéciaux (comme les liens symboliques) sont conservés comme ils sont : ils restent des liens symboliques.

Mais avec l'option `-r`, les remplace par des fichiers normaux.

## 2) Déplacement ou renommage de répertoire

   \code

   > ```bash
   > mv Rep1 Rep2 # Renomme Rep1 en Rep2
   > cp -R Rep1 /home/dupont/Rep1 /tmp/Rep2 # Déplace Rep1 dans Rep2 en lui changeant de nom
   > ```

   \endgroup

## 3) Création et destruction de répertoires

\fonction{mkdir} a quelques options utiles :

| Opt  | Description                                                 |
| :--: | ----------------------------------------------------------- |
|  -m  | attribuer des droits : -m "a=rx"                            |
|  -p  | crée tous les sous-répertoires intermédiaires requis        |

# Questions et exercices

## 1) Questions

1. Quelle est la différence entre un nom de fichier et un répertoire ?  

> Au sens textuel, aucune. Mais l'un est unique, l'autre peut contenir des fichiers ou sous-réperoires.

2. Pourquoi un nom de fichier qui contient des espaces peut-il causer des problèmes ?  

> Il peut être partiellement compris, jusqu'à son espace, par une commande qui couperait son nom à l'espace.  
> Il faut alors le mettre entre guillemets `"` ou l'escaper avec des caractères `|`.

3. Qu'est-ce que le chemin absolu d'un fichier ?  

> Celui qui va de la racine `/` jusqu'à lui.

4. Quel est le problème de la syntaxe relative, qui n'existe pas pour la syntaxe absolue ?  

> Le risque d'être dans un répertoire de départ différent de celui auquel l'on pensait, et ne plus désigner ce que l'on espérait.

5. Comment peut-on exécuter un programme si celui-ci ne se lance pas quand ou tape son nom ?  

> `./leProgramme` : __le . indique qu'il est dans le répertoire courant.__ 
> et/ou : `chmod +x leProgramme; ./leProgramme`

6. Que se passe t-il si la commande `ls-l` est saisie sans l'espace séparateur ?  

> Elle cause une erreur.

7. Comment faire pour détruire le fichier `liste du personnel` ?  

> `rm "liste du personnel"`  
> __ou \fonction{rm -i liste*}, le `-i` faisant venir un prompt, et après avoir bien vérifié, supprimer celui voulu.__

8. Comment faire pour détruire des fichiers aux caractères bizarres ?  

> Deuxième méthode de la question précédente.

9. Quelle est la différence entre `rmdir` et la commande `rm -r`  

> Toutes deux supprimeront un répertoire
>    - la première, s'il est entièrement vide
>    - le second, avec son contenu, dont ses sous-répertoires, s'il en a

10. Comment faire la différence entre un changement de nom et un déplacement quand on utilise une commande `mv` ?  

> Si les deux fichiers sont dans le même répertoire, il y a changement de nom  
> Sinon, il y a déplacement de fichiers

11. Quel est le rôle de l'option `-p` de la commande `mkdir` ?  

> Créer l'ensemble des répertoires qui pourraient manquer depuis la racine jusqu'au répertoire destination demandé.

12. Quelle est la différence entre la commande `-R` et la commande `-r` de la commande `cp` ?  

> Toutes deux copient les sous-répertoires, mais dans le cas de fichiers link, fifo, pile (donc des fichiers spéciaux)  
>     __`-R`  ils sont copiés avec leurs liens et spécialités conservées__  
>     __`-r`  ils sont remplacés par des fichiers normaux qui "_les resolvent_", si on peut dire.__

## 2) Exercices

1. Créer un fichier essai contenant "essai de texte"

   ```bash
   echo -e "Essai de texte" > essai
   ```

2. Afficher le numéro d'inode de ce fichier 

   ```bash
   ls -i essai
   ```

3. Afficher le nom du répertoire de travail

   ```bash
    pwd
   ```

4. Afficher le contenu du répertoire de travail, en faisant apparaitre les droits et fichiers cachés

   ```bash
   ls -al
   ```

5. Aller dans le répertoire /tmp

   ```bash
   cd /tmp
   ```

6. Créer dans /tmp un répertoire Sauve, avec dedans un répertoire 2019, en une seule commande

   ```bash
   mkdir -p Sauve/2019
   ```

7. Retournez dans votre répertoire personnel

   ```bash
   cd -
   ```

8. Déplacez le fichier essai dans le répertoire Sauve, sous-répertoire 2019, de /tmp

   ```bash
   mv essai /tmp/Sauve/2019/
   ```

9. Déplacez Sauve et son sous-répertoire dans votre sous-répertoire personnel

   ```bash
   mv /tmp/Sauve/ .
   ```

10. Supprimer le répertoire Sauve

    ```bash
    rm -rf Sauve/
    ```

11. Lister les attributs de votre répertoire personnel

    ```bash
    ls -al .  
    ```

12. Lister les attributs de votre répertoire personnel, et de tous ses sous-répertoires   

    ```bash
    ls -alR .
    ```

