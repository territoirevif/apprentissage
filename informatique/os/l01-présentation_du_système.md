---
title: "l01. Présentation d'UNIX et de Linux"
subtitle: "Historique et caractéristiques d'UNIX"
author: Marc Le Bihan
keywords:
  - UNIX
  - Linux
  - POSIX
  - terminal
  - shell
  - processus
  - GNU
---

---

# I) Historique

UNIX est issu du projet MULTICS du MIT, 1964.   
Devenu UNICS en 1969 et renommé UNIX en 1972.  
En 1973, il est réécrit à l'aide du Langage C, et intègre un compilateur C.

La distribution des codes sources a été très tôt, gratuite.

Une version majeure d'UNIX a été la _System V_. Mais d'autres comme _BSD_ ou _AIX_ sont également connues.

De premières normalisations ont eu lieu :

   - de l'Open Software Fondation (OSF)
   - Portable Operating System Interface for Unix (POSIX)

\bigbreak

Pour qu'UNIX entre davantage dans le monde libre, il a été refondé en _Gnu's Not Unix (GNU)_, puis en _Linux_, en 1991.

Les principales distributions Linux sont : Slackware, Red Hat Enterprise Linux, Debian, SuSE, Fedora, CentOS, Ubuntu, Knoppix, Linux Mint, Mageia.

### Les numéros de version

Les numéros de version de Linux sont de la forme `w.x.y.z` où :

   - __w.x__ est la version du noyau
   - __y__ sa version de mise à jour
   - __z__ sa version de correction de bug, minime.

# II) Caractéristiques d'UNIX

- _UNIX_ est multi-tâche et multi-utilisateur. Chaque utilisateur :

   - Se connecte au système par un terminal
   - ou utilise son propre micro-ordinateur avec un logiciel __émulateur de terminal__.

\bigbreak

- Le système UNIX repose sur une arborescence de fichiers : _File System (FS)_, qui peut être monté ou démonté.

- Les environnement d'exécution de commandes s'appellent les _shells_ :

   - Il y en a en mode texte : _Bourne Again Shell (bash)_, _Korn Shell (ksh)_, _C-Shell (csh)_...
   - ou en mode graphique : _GNOME_, _KDE_...

- La gestion des processus y est hiérarchisée : un processus père a des processus fils.

\newpage
# Questions de révision

### 1. Qui a créé UNIX, en quelle année ?
Ken THompson, 1969 

### 2. Quelle est l'incidence de l'invention du language C pour Unix ?
Il est principalement développé en Linux et aide à le rendre portable.

### 3. Comment s'appelle le système UNIX gratuit proposé par l'Université de Berkeley ?
FreeBSD

### 4. Quel rôle important a joué la Free Software Fundation (FSF) dans le développement des logiciels Libres, et indirectement, dans Linux
Elle a jeté les bases pour le développement communautaire de logiciels qui redevenaient payants.

### 5. Qui est le créateur de Linux
Linus Thorvalds

### 6. Quel est le nom du système Unix qu'il utilisait alors et qui lui servait de référence pour produire un système encore meilleur ? Qui avait créé ce système ?
Minix. Andy Tanenbaum.

### 7. Quel est le mode de fonctionnement pour le noyau Linux ?
Des développeurs répartis dans le monde participent et L. Torvalds compile le noyau.

### 8. Quelle est la différence entre la notion de distribution Linux et la notion commericale d'Unix ?
Il y a davantage d'assistance et d'outils de packaging dans les versions commerciales.
Mais tout ce qui est commercial est cependant rapporté dans la version gratuite.

### 9. Qu'est-ce qu'un système multi-tâche ?
Un système qui peut traiter plusieurs travaux en tâche de fond.

### 10. Qu'est-ce qu'un système multi-utilisateur ?
Un système qui permet à plusieurs utilisateurs d'avoir leur environnement personnel et de travailler ensemble sur le même serveur.

### 11. Qu'est-ce qu'un shell Unix ?
Une interpréteur de commande qui fait le lien entre l'utilisateur et la machine.

### 12. Quel est le système de gestion utilisé pour les fichiers et les répertoire ?
Un système arborescent, partant d'un répertoire racine

### 13. Qu'est-ce qu'un file system ?
Un branche localisée sur de la hiérarchie localisée sur un disque ou une partition se nomme un File System.

### 14. Qu'est-ce qu'un processus ?
Un processus est une tâche, une application ou une commande en cours d'exécution.
Un processus père peut avoir des processus fils. En tant le père on tue sa sous-arborescence.

## Exercices

### 1. Quelles sont les deux grandes lignées du système Linux ?
Celle commerciale et celle libre.
Unix System V, et BSD.

### 2. Citez trois noms de distributions Unix commerciales et deux distributions Linux
Commericales : Red Hat, AIX, Solaris
Linux : Debian, CentOS, Ubuntu.

### 3. Comment s'appelle le logiciel gratuit qui adapte le protocole X-Window aux cartes graphiques sur PC ?
XFree68, Xorg

### 4. Citez quatre shells Unix
bash, bsh, c-shell, ksh

### 5. Comment appelle t-on un processus qui en exécute un autre ?
Un processus père.

