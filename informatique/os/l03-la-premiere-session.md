---
title: "l03. La première session"
subtitle: ""
author: Marc Le Bihan
category: linux
keywords:
- X-Windows
- X11
- GNOME
- KDE
- exit
- logout
- passwd
- pwd
- Bourne Shell (bsh)
- Bourne Again Shell (bash)
- Korn Shell (ksh)
- C-Shell (csh)
- who
- whoami
- finger
- user identification (uid)
- group identification (gid)
- /etc/password
- /etc/group
- groups
- chsh
- chfn
- terminal (TERM)
- vt100
- id
---

## 1) Environnements graphiques

Le mode graphique repose sur le protocole \fonction{X-Windows} ou \fonction{X11}.

Le \fonction{Common Desktop Environment (CDE)} offre la même tous ceux qui vont vouloir implémenter leur interface graphique.

\definition{GNOME} et \definition{KDE} sont ainsi deux emplois de CDE.

## 2) Sessions et changement de mot de passe

La commande \fonction{exit} termine une session, mais \fonction{Ctrl-D} fait la même chose en même temps qu'il peut terminer la saisie de données d'entrée.

\fonction{logout}, elle, servira pour finir la session sur un shell de connexion, type \mauve{telnet}.


Provoquer un changement de mot de passe

   \code

   > ```bash
   > passwd
   > ```

   \endgroup

## 3) Shells et répertoire de travail

### a) Shells

Il existe plusieurs \definition{shells} utilisables :

   - Bourne Shell (bsh, ancien)
   - Born Again Bourne shell (bash)
   - Korn Shell (ksh)
   - C-Shell (csh)

### b) Répertoire de travail

Obtenir son répertoire de travail initial (Print Working Directory)

   \code

   > ```bash
   > pwd
   > /home/lebihan/Documents/apprentissage
   > ```

   \endgroup

## 4) Qui est connecté ?

\fonction{who} affiche les utilisateurs connectés,  
\fonction{whoami} qui l'on est, soi,  
et \fonction{w}, qui indique aussi ce qu'exécute en ce moment l'utilisateur en question.

\fonction{finger} donne des informations sur l'état-civil des utilisateurs, mais n'est pas installée nativement sur _Debian_ : il faut l'installer par un `sudo apt-get install finger`.

   \code

   > ```bash
   > who
   > vagrant  tty2         2022-08-05 05:31 (tty2)
   >
   > whoami
   > vagrant
   >
   > w
   > 05:48:59 up 17 min,  1 user,  load average: 0.03, 0.02, 0.03
   > USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
   > vagrant  tty2     tty2             05:31   17:44   0.00s  0.00s /usr/libexec/gnome-session-binary --systemd
   >
   > finger
   > Login     Name       Tty      Idle  Login Time   Office     Office Phone
   > vagrant   vagrant    tty2       21  Aug  5 05:31 (tty2)
   > ```

   \endgroup

## 5) L'identification d'un utilisateur

Un utilisateur a un \fonction{UID} (User IDentification), et un nom de login (le uname). Deux utilisateurs pourraient se fondrent en un, s'ils partagaient le même UID.

et un groupe d'appartenance : le \fonction{GID} (Group IDentification) :  
Il permet à l'utilisateur de définir des droits à des membres de son groupe d'appartenance, par exemple.

Chaque utilisateur a un groupe principal, mais peut être associé à plusieurs autres groupes.

Dans \fonction{/etc/password}, l'utilisateur est identifié avec son `UID` et son `GID`,  
et le `GID` (groupe) est défini dans \fonction{/etc/group}

La commande \fonction{id} donne le `UID` de l'utilisateur connecté, et tous les groupes auxquels il appartient.

   \code

   > ```bash
   > id
   > # Elle peut aussi s'utiliser avec un autre login d'utilisateur : `id <autreLogin>`
   > ```
   > 
   > ```bash
   > uid=1000(vagrant) gid=1000(vagrant) groups=1000(vagrant),24(cdrom),25(floppy),29(audio),30(dip),44(video),46(plugdev),108(netdev),113(bluetooth),117(lpadmin),120(scanner)
   > ```

   \endgroup

Le groupe actif de `vagrant`, ici, est `vagrant` (gid=1000), mais il appartient à d'autres groupes, listés derrière `groups`.  
C'est le groupe actif de l'utilisateur connecté (il peut en changer avec la commande \methode{newgrp}{<nom du groupe>}, mais seulement pendant la durée de sa connexion. Au prochain login, il reprendra son ancien group) qui deviendra le groupe d'un fichier créé.

Respectivement \fonction{groups} va donner la liste des groupes d'un utilisateur ou de celui connecté.

   \code

   > ```bash
   > groups
   > vagrant cdrom floppy audio dip video plugdev netdev bluetooth lpadmin scanner
   > ```

   \endgroup

Le fichier `/etc/group` a cette structure :

   \code

   > ```bash
   > sudo cat /etc/group
   > root:x:0:
   > daemon:x:1:
   > bin:x:2:
   > sys:x:3:
   > scanner:x:120:saned,vagrant
   > ```

   \endgroup

nom, mot de passe, gid, et la liste des logins des utilisateurs qui en sont membres.

Et \methode{finger}{<login>} en dira plus que \fonction{finger} sans argument, si elle est utilisée sur un login spécifique :

   \code

   > ```bash
   > finger vagrant
   > Login: vagrant        			Name: vagrant
   > Directory: /home/vagrant            	Shell: /bin/bash
   > On since Mon Aug 22 15:20 (GMT) on tty2 from tty2
   >    23 minutes 39 seconds idle
   > No mail.
   > No Plan.
   > ```

   \endgroup

C'est aussi ce fichier `/etc/passwd` qui suffit à l'administrateur pour gérer les utilisateurs, qu'il peut créer, éditer ou supprimer ci.

   \code

   > ```bash
   > sudo cat /etc/passwd
   > root:x:0:0:root:/root:/bin/bash
   > daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
   > bin:x:2:2:bin:/bin:/usr/sbin/nologin
   > sys:x:3:3:sys:/dev:/usr/sbin/nologin
   > ```

   \endgroup

On y trouve le `UID`, `GID`, le champ GECOS utilisé par la commande `finger`, son $home dir, son shell de login.  

\attention{Le mot de passe de l'utilisateur ne devrait plus s'y trouver, et être rangé ailleurs, crypté, dans un répertoire accessible seulement par root}.

Mais pour faire des changement dedans, il préfèrera plutôt passer par des commandes.

Par exemple, pour changer le shell d'un utilisateur (à sa prochaine session), par un :

   \code

   > ```bash
   > chsh -s /bin/ksh dupont
   > ```

   \endgroup

\bigbreak

S'il veut changer son GECOS, il utilisera plutôt un :

   \code

   > ```bash
   > chfn -s /bin/ksh dupont
   > sudo chfn games
   > Changing the user information for games
   > Enter the new value, or press ENTER for the default
   > 	Full Name [games]: Jeux
   > 	Room Number []: 14
   > 	Work Phone []: 01 25 52 21 41
   > 	Home Phone []: 01 25 35 35 21
   > 	Other []: 
   > 
   > ```

   \endgroup

(variante en utilisant des options `-f` pour le nom, `-p` pour le téléphone, etc.

# Questions de révision

   1. Comment indiquer à UNIX que le type de terminal utilisé ou émulé est un vt100 ?
   
   > Avec la variable d'environnement `TERM`
   > `export TERM=vt100`
   
   2. Quel fichier permet d'identifier un utilisateur ?
   
   > Le fichier `/etc/passwd`
   
   3. Comment est identifié un utilisateur ?
   
   > Avec un identifiant UID. Un groupe. Un profil et un mot de passe.
   
   4. Un utilisateur peut-il changer son UID.
   
   > Oui, mais il prend un risque de semer la pagaille.
   > ==> Non, seul l'administrateur le peut.
   
   5. Quel rôle a le fichier /etc/group ?
   
   > Il définit l'ensemble des groupes auxquel peuvent appartenir des utilisateurs.

# Exercices

   1. Afficher la liste des utilisateurs connectés, avec comme information principale, le nom du poste à partir duquel ils travaillent.

   > `who`
   
   2. Afficher le nombre d'utilisateurs connectés à partir de postes dans univ.fr

   > `who | grep 'univ.fr' | wc -l`
   
   3. Afficher la liste des utilisateurs connectés qui utilisent le logiciel nedit

   > `w | grep 'nedit'`
   
   4. Afficher la liste des utilisateurs connectés qui utilisent le Korn Shell

   > `w | grep 'ksh'`
   
   5. Afficher la liste des utilisateurs dont le nom ou le prénom est Pierre

   > `finger | grep 'Pierre'`
   
   6. Afficher le nombre d'utilisateurs enregistrés sur le serveur Linux où vous êtes connecté

   > id | wc -l
   
   7. Afficher le nombre d'utilisateurs enregistrés dont le login commence par dup

   > id | grep '^dup' | wc -l
   
   8. Positionnez votre type de terminal à VT220 

   > export TERM=VT220
   
   9. Afficher les informations utilisées pour vous connecter.  
   De même pour l'utilisateur Dupont

   > cat /etc/passwd | grep moi  
   > ou id
   >
   > cat /etc/passwd | grep dupont  
   > ou id dupont
   
   10. Afficher tous les membres du groupe personnel

   > cat /etc/group | grep "^personnel:"

