---
header-includes:
  \input{apprentissage-header-include}
  \input{apprentissage-include}

title: "Unix et Linux"
subtitle: "Les droits"
author: Marc Le Bihan
geometry: margin=2cm
fontsize: 12pt
output: pdf
classoption: fleqn
urlcolor: blue
---

# Questions de révision

1. Quels sont les droits standards Linux ?
- r : lecture
- w : écriture
- x : exécution de fichier, ou droit de listage de répertoire
- d : suppression de répertoire
- suid, sgid : propagation de droits

2. À quelles catégories d'utilisateurs s'appliquent ces droits ?
- À l'utilisateur défini
- Au groupe défini
- Aux autres utilisateurs d'autres groupes

3. Comment sont évalués les droits d'un utilisateur quand il accède à un fichier ?
- L'on regarde s'il est le propriétaire du fichier,
- Ou s'il appartient au groupe du fichier
- Ou si ce que le fichier admet d'opération pour les autres groupes

4. Quel est le sens du droit x sur un répertoire
Il permet de dresser la liste de son contenu par une commande `ls` ou `find`.  
Sans ce droit il faut désigner spécifiquement le fichier voulu pour pouvoir l'ouvrir.

5. En quoi ce droit est-il important, même pour les fichiers du répertoire ?
Pour un fichier, la présence du `x` il permet d'exécuter son contenu.

6. Quelles sont les grandes familles de droit en notation octale ?
- `777` = tous les droits
- 1 : read
- 2 : write
- 4 : execute

7. Quel est le rôle de la commande `umask` ?
Filtrer les droits demandés pour établir, à la création de fichier, des droits sans utiliser `chmod`.

8. Quelles sont les sous-familles de masques ?

9. Quel est le rôle du SUID-bit et du SGID-bit ?
Propager les droits de l'utilisateur déclaré du fichier à celui qui va l'exécuter, mais le temps de l'exécution seulement.

10. Comment différencier SUID-bit de SGID-bit ?

11. Quelle est la condition pour que le `s` positionné sur un fichier binaire soit opérationnel ?

12. Que signifie un `S` majuscule à la place d'un `s` ?


