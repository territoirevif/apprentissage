---
title: "r02. Variables : types primitifs"
subtitle: ""
author: Marc Le Bihan
---

Sur une console, pour afficher le contenu d'une variable :

   \code

   > ```r
   > print(x)
   > ```

ou

   > ```r
   > x
   > ```

   \endgroup

\mauve{NA} (_Not Available_) marque une valeur manquante

Supprimer une variable :

   \code

   > ```r
   > rm(x)
   > ```

   \endgroup

## 1) Connaître le type d'une variable

\methode{mode}{(x)}\ \tab \rouge {son nom est trompeur} : il rien à voir avec le mode en statistique   
   \tab \tab \ \ \ renvoie : numeric, logical, character, list...

\methode{class}{(x)}\tab \underline{semble} avoir le même effet.

\methode{typeof}{(x)} précise son type :  
   \demitab par exemple, pour un numérique : \fonction{double}, \fonction{integer}...

\mauve{\texttt{is.}}logical(x), \mauve{\texttt{is.}}numeric(x), \mauve{\texttt{is.}}character... testent le type d'une variable,  
\mauve{\texttt{as.}}logical(x), \mauve{\texttt{as.}}numeric(x), \mauve{\texttt{as.}}character... tentent de la convertir dans ce type-là.

## 2) Autres informations

\methode{length}{(x)} \tab pour la longueur d'une variable  
   \demitab \attention{pour les strings (\mauve{character}) c'est \mauve{nchar(x)} qu'il faut utiliser}

\methode{object.size}{(x)} \tab donne la taille d'une variable en octets  

## 3) Les types primitifs

### a) La gestion des chaînes de caractères

\fonction{paste(...)} joue le rôle de concat.

   \code

   > ```r
   > paste("X", 1:5, sep="-")
   > ```
   > 
   > ```txt
   > "X-1" "X-2" "X-3" "X-4" "X-5"
   >  ``` 

   \endgroup

\fonction{substr(...)} prend une partie d'une chaîne,

\methode{grep}{(chaine, liste)} dit à quels index de postes la chaîne se trouve dans la liste,

\methode{gsub}{("foo", c("arm", "foot", "lefroo", "bafoobar"), replacement = "DON")}  
   \demitab fait une recherche/remplacement.

   \code

   > ```r
   > gsub("foo", c("arm", "foot", "lefroo", "bafoobar"), replacement = "DON")
   > ```
   > 
   > ```txt
   > "arm"      "DONt"     "lefroo"   "baDONbar"
   >  ``` 

   \endgroup

### b) Parmi les autres variables scalaires

Le booléen en R, c'est \fonction{TRUE}, \fonction{FALSE}.

\methode{is.na}{(x)} détecte si $x$ est indéfinie :

   > \mauve{NA} (_Not Available_) \tab marque une valeur manquante  
   > \mauve{NaN} (_Not a Number_} \tab une valeur indéfinie (erreur de calcul)
   >
   > \mauve{Inf} {_Infinity_} \tab l'infini.  
     \demitab Exemple : \fonction{-5 / 0} répond \fonction{-Inf}.

