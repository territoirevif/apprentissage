---
header-includes:
  \input{apprentissage-header-include}
  \input{apprentissage-include}

title: "R  \n04. Les matrices"
subtitle: ""
author: Marc Le Bihan
geometry: margin=2cm
fontsize: 12pt
output: pdf
classoption: fleqn
urlcolor: blue
---

Une matrice est définie, sous R par :

   \fonction{dim} : qui donne son nombre de lignes et de colonnes

   \code

   > ```r
   > # Ici, ncol = 2 va dire que les valeurs se répartissent sur 2 colonnes,  
   > # et nrow = 2, dirait : par deux lignes.
   > 
   > # Par défaut, les valeurs sont distribuées colonnes par colonnes, 
   > # sauf si l'on met byrow = TRUE
   > m <- matrix(c(25,3,-2,-14,5,-1), ncol=2)  
   > dim(m) 
   > ```

   ``` 
   3 2
   ```
   
   \endgroup

\bigbreak

Si la matrice est trop grande pour la taille du vecteur transmis (par exemple `c(1,2,3,4)` pour une `matrix(nrow=3, ncol=3)`, alors le motif $1,2,3,4$ sera répété, pour tout remplir.

\bigbreak

Ceci crée une matrice de $\mathbf{1}$ :

   \code

   > ```r
   > m <- matrix(1, nrow=2, ncol=4)  
   > ```

   \endgroup

\bigbreak

   \fonction{as.matrix(x)} pourra transformer un vecteur (un `c(....)`) en une matrice, si souhaité.

# I) Calcul sur les matrices

Soit les matrices :
   $$ A = \begin{bmatrix*}[r]
   1 & 2 \\
   3 & -1 \\
   -7 & 8
   \end{bmatrix*}
   ,\ 
   B = \begin{bmatrix*}[r]
   5 & 4 \\
   -2 & 11 \\
   6 & -4
   \end{bmatrix*}
   ,\ 
   C = \begin{bmatrix*}[r]
   1 & -9 & 3 \\
   -5 & 2 & 6
   \end{bmatrix*} $$
   
   \code

   > ```r 
   > a <- matrix(c(1, 3, -7, 2, -1, 8), ncol=2)
   >    
   >      [,1] [,2]
   > [1,]    1    2
   > [2,]    3   -1
   > [3,]   -7    8
   >    
   > b <- matrix(c(5, -2, 6, 4, 11, -4), ncol=2)
   >
   >      [,1] [,2]
   > [1,]    5    4
   > [2,]   -2   11
   > [3,]    6   -4
   >    
   > c <- matrix(c(1, -9, 3,-5, 2, 6), ncol=3)
   >
   >      [,1] [,2] [,3]
   > [1,]    1    3    2
   > [2,]   -9   -5    6
   > ```

   \endgroup
