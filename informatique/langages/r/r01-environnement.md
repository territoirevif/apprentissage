---
title: "r01. L'environnement"
subtitle: ""
category: programmation
author: Marc Le Bihan
---

# I) Installation de R

Par défaut, _Debian 11_ installera une version `4.0.4` de R, et certains packages peuvent désirer mieux.

Mieux vaut rajouter un dépôt avec sa clef publique :

   \code

   > ```bash
   > # Installera une des dernières versions de R
   > deb http://cloud.r-project.org/bin/linux/debian bullseye-cran40/
   > ```

   \endgroup

\bigbreak

Puis taper les commandes d'installation :

   \code

   > ```bash
   > sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-key '95C0FAF38DB3CCAD0C080A7BDC78B2DDEABC47B7'
   > sudo apt-get update
   > sudo apt install r-base
   > ```

   \endgroup

Elle installera au moins une `4.2.1` ou supérieure, à la dernière expérimentation.

## II) Commandes diverses

Fixer ou obtenir le répertoire de travail

   \code

   > ```r
   > setwd("nouveau_répertoire")
   > getwd()
   > ```
 
   \endgroup

\bigbreak

\mauve{\texttt{save.image()}} sauvegardera la session de travail dans le répertoire de travail `.RData`  
(et normalement, l'éditeur le propose aussi avant de quitter).

\mauve{\texttt{load.image()}} la restaurera.

\bigbreak

Obtenir de l'aide sur une fonction :

   \code
   
   > ```r
   > help(mean)
   > ```
   
   \endgroup

ou `?mean`

