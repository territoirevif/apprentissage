---
header-includes:
  \input{apprentissage-header-include}
  \input{apprentissage-include}

title: "R  \n03. Listes et vecteurs"
subtitle: ""
author: Marc Le Bihan
geometry: margin=2cm
fontsize: 12pt
output: pdf
classoption: fleqn
urlcolor: blue
---

# I) Les listes

Début et fin de listes

   \code

   > ```r
   > head(cars)
   > tail(cars)
   >  ``` 

   \endgroup

\methode{names}{(x)} va lister le nom des attributs que l'on peut trouver dedans.

   \code

   > ```r
   > names(cars) # avec cars, un data.frame
   > ```
   >
   > ```txt 
   > "speed" "dist"
   > ```

   \endgroup

alors que \methode{dimnames}{(x)} va répondre :

   \begingroup \footnotesize

   > ```txt
   >  [1] "1"  "2"  "3"  "4"  "5"  "6"  "7"  "8"  "9"  "10" "11" "12" "13" "14" "15" "16" "17" "18" "19" "20" "21" "22" "23"
   > [24] "24" "25" "26" "27" "28" "29" "30" "31" "32" "33" "34" "35" "36" "37" "38" "39" "40" "41" "42" "43" "44" "45" "46"
   > [47] "47" "48" "49" "50"
   > ```

   \endgroup

### Chargement et affichage facilités

Soit une liste `HospitFull`, son chargement et affichage sont facilités par les commandes :

   \code

   > ```r
   > data(HospitFull) # Charge en données courantes le contenu de la liste
   > View(HospitFull) # l'affiche 
   > ```
   >
   > \bigbreak \footnotesize
   >
   > ```
   >                        Région Tous.âges X0.9 X10.19 X20.29 X30.39 X40.49 X50.59 X60.69 X70.79 X80.89 X90.
   > 1               Ile-de-France      5474   17     27     72    128    192    521    861   1222   1607  783
   > 2        Auvergne-Rhône-Alpes      3779    1      9     15     47    112    263    571    989   1188  541
   > 3  Provence-Alpes-Côte d’Azur      3531    4      7     23     42     91    246    546    939   1088  527
   > ```

   \endgroup

\underline{Remarque :}

   > la liste a les colonnes : `Région`, `Tous.âges`, `X0.9`, `X10.19`...

   > Un `HospitFull[,3:6]` retiendra seulement les colonnes : `X0.9`, `X10.19`, `X20.29`, `X30.39` (de la $3^{ème}$ à la $6^{ème}$).

Un \fonction{plot(HospitFull[,3:6])} va faire s'afficher des petits graphes les valeurs des colonnes les unes par rapport aux autres :

   ![Plot sur les données brutes de colonnes](informatique/r_plot_sur_colonnes_de_données_brutes.jpg)

_(les valeurs de la colonne `X0.9` en abscisse par rapport à `X10.19` en ordonnée, `X10.19` par rapport à `X30.39`, etc.)_

# II) Les vecteurs 

## 1) par l'opérateur collecteur, c()

\fonction{c(...)} (_collecteur_) produit un vecteur.  
Exemple : un vecteur à 4 dimensions

   \code

   > ```r
   > v <- c(5.6, -2, 78, 42.3)
   >  ``` 

   \endgroup

On peut ensuite les traiter comme des collections pour leur création, et composer un vecteur à partir d'autres :

   \code

   > ```r
   > z <- c(v, 3, c(12,8))
   >  ``` 
   >
   > ```txt
   > 5.6 -2.0 78.0 42.3  3.0 12.0  8.0
   > ```

   \endgroup

## 2) création ou adressage 

- par l'opérateur ":"

   \code

   > ```r
   > x <- 1:6
   >  ``` 
   >
   > ```txt
   > 1 2 3 4 5 6
   > ```

   \endgroup

   `v[10:1]` les listera des postes 10 à 1.

   `v[c(6,5,1:3)]` donnera les $6^{\text{èmes}}$, $5^{\text{èmes}}$, et du $1^{\text{er}}$ et $3^{\text{èmes}}$ éléments de `v`.


- éjection d'éléments lors de la sélection :

   `v[-(1:5)]` v, sauf ses $1^{\text{er}}$ au $5^{\text{ème}}$ éléments.

   `v[-c(1,5)]` v, sauf son $1^{\text{er}}$ et $5^{\text{ème}}$ élément.

## 3) booléens déduits de vecteurs

   \code

   > ```r
   > x <- c(-1, 0, 2)  # Donne : -1  0  2
   > test <- x > 1     # Donne : FALSE FALSE  TRUE
   > ```

   \endgroup

Les fonctions \fonction{all} et \fonction{any} avisent le vecteur en entier :

   \code

   > ```r
   > all(x > 7) # Tous les x sont-ils strictement supérieurs à 7 ? Répondra TRUE ou FALSE
   > any(x > 7) # Existe t-il au moins un x strictement supérieur à 7 ?
   > ```

   \endgroup

Compbinaisons :

   \code

   > ```r
   > v <- c(5,8,3,14,27,26,41,18)
   > v[(v >= 9) & (v != 14)]
   > ```

   \endgroup

Attention ! Il n'y a \attention{qu'un seul \& pour faire le ET}, pas \&\& comme en Java ou C++ !

   > ```
   > 27 26 41 18
   > ```

Et l'on peut rapporter les entrées d'un second vecteur dont les postes ont les indexs de ceux qui ont été trouvés par un critère de recherche dans un premier.

Ici, une recherche sur v donne ses éléments n°5,6,7 et 8, qui portent des entrées dans w :

   \code

   > ```r
   > w <- c('arbre','bateau','cheval','sabre','électron','savon','étoile','lampe')
   > w[(v >= 9) & (v != 14)]
   > ```

   \endgroup

   > ```
   > "électron" "savon"    "étoile"   "lampe"   
   > ```

## 4) par les commandes seq, rep ou scan

   > \methode{seq}{(1, 18, by=2)} $\rightarrow$ `1  3  5  7  9 11 13 15 17`  
   > \methode{rep}{(1,4)} $\rightarrow$ `1 1 1 1`  
   > \methode{scan}{(n=3)} $\rightarrow$ demande à l'utilisateur de saisir trois nombres

   \code

   > ```r
   > w <- scan(n=3)
   >  ``` 
   > 
   > \bigbreak
   >  
   > ```txt
   > 1: 25
   > 2: 14.2
   > 3: -8
   > Read 3 items
   > w   # Affiche : 25.0 14.2 -8.0
   > ```

   \endgroup

# III) Cas pratiques d'affectation ou de sélection

   \code

   > ```r
   > [is.NA(x)] <- 0  # Tous les éléments NA sont affectés à zéro
   > ```

   \endgroup

## 1) which

\donnee{which} renvoie les indexs d'un tableau/vecteur, qui répondent à une condition

   \code

   > ```r
   > x <- c(23, 28, 24, 32, 25)
   > 
   > which.min(x)
   > 1             # c'est à dire : 23
   > which.max(x)
   > 4             # c'est à dire : 32
   > ```

   \endgroup

(alternative : `which(<relation>)` $\to$ `which(x == min(x))`)

   \code

   > ```r
   > which(x < max(x) & x %% 2 == 0)
   > 2 3          # Soit : 28, 24 et 32
   > ```

   \endgroup

