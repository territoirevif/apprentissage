---
title: " c02. C++ : Les variables"
subtitle: ""
author: Marc Le Bihan
---

# I) Différences entre C++ et Java

   - C++ possède les variables \fonction{unsigned} que Java n'a pas : `unsigned int`, par exemple.
   - Le \fonction{long long} est un 64 bits, comme `long`, à présent. Il n'avait de sens que sur les plateformes 32 bits.
   - C++ propose l'opérateur \methode{sizeof}{(<un type>)}, que Java n'a pas.
   - en C++, une variable globale peut-être déclarée n'importe où et elle a la portée de tout le programme.  
     En Java, elle ne pourra être définie que dans une classe (même si elle sera globale car un getter/setter ou, elle, publique permettront de la modifier et de l'utiliser de partout)  

   - en C++, le type booléen s'appelle \fonction{bool}.  
     En Java : \fonction{boolean}.  
   - les tableaux sont déclarés \fonction{type variable[]} quand en Java, c'est `type[] variable`.
   - les noms de méthodes commencent par une majuscule ; en Java, par une minuscule.

# II) Les types : fixes, redéfinis, détectés...

## 1) Les types fixes

dans l'include \fonction{<cstdint>}, on peut trouver des types \fonction{int8\_t} ou \fonction{uint8\_t}, `...16_t`, `...32_t`...  
pour toutes les situations où l'on a vraiment besoin d'un int de taille bien déterminée.

C'est le cas quand des compilations peuvent voir le type \fonction{int} prendre des tailles différentes selon l'OS cible, et que l'on doit veiller au même fonctionnement précis du programme.

## 2) typedef pour substituer des types

Le mot-clef \fonction{typedef} substitue un type de variable

   \code

   > ```cpp
   > typedef unsigned int ENTIER_STRICTEMENT_POSITIF;
   > ENTIER_STRICTEMENT_POSITIF a = 15;
   > ```

   \endgroup

## 3) auto (détection automatique du type d'une variable)

L'équivalent du mot-clef \fonction{var} de java, il laisse le compilateur décider du type d'une variable :

   \code

   > ```cpp
   > auto succes = true;      // définira succes comme bool
   > auto valeur = 1551156718 // définira valeur comme long
   > ```

   \endgroup

# II) Les constantes

## 1) const

Sont déclarées avec le mot-clef \fonction{const}

## 2) constexpr

Par ailleurs la directive de compilation \fonction{constexpr} dit au compilateur :

   >"_Tu peux, toi, réaliser seul ce calcul tout de suite, il n'y a pas besoin d'attendre que le programme s'exécute, au runtime, pour le faire_"

\bigbreak

\underline{C++ :} une évaluation résolue et assignée par le compilateur lui-même

   \demitab un \methode{Div\_Expr}{(5, 3)} sera résolu à la compilation, mais un \methode{Div\_Expr}{(a, b)} au runtime.

   \code

   > ```cpp
   > constexpr double Div_Expr(double a, double b) 
   > {
   >    return a / b;
   > }
   >
   > constexpr double pi = Div_Expr(22, 7); // Sera calculé et assigné par le compilateur lui-même
   >
   > // mais :
   > int a = 22, b = 7; // ne pourront pas être utilisées en variables passées comme arguments
   > const double pi = Div_Expr(a, b); // avec, conjointement, constexpr. 
   > // Elles devront être résolues au runtime.
   > ```

   \endgroup

## 3) consteval (C++ 20)

Déclaré en lieu et place du mot-clef `constexpr`, il __interdira__ d'utiliser dans la fonction appelée des arguments qui ne se résoudraient pas à la compilation.

\underline{C++ :} Exemple d'emploi de \fonction{constexpr} et de \fonction{consteval} : 

   \code
    
   > ```cpp
   > #include <iostream>
   > consteval double GetPi() { return 22.0 / 7; }
   > constexpr double XPi(int x) { return x * GetPi(); }
   > 
   > int main()
   > {
   >    using namespace std;
   >    constexpr double pi = GetPi(); // evaluated by compiler
   > 
   >    cout << "constexpr pi evaluated by compiler to " << pi << endl;
   >    cout << "constexpr XPi(2) evaluated by compiler to " << XPi(2) << endl;
   > 
   >    int multiple = 5;
   >    cout << "(non-const) integer multiple = " << multiple << endl;
   >    cout << "constexpr is ignored when XPi(multiple) is invoked, ";
   >    cout << "returns " << XPi(multiple) << endl;
   > 
   >    return 0;
   > }
   > ```

   ``` 
   constexpr pi evaluated by compiler to 3.14286
   constexpr XPi(2) evaluated by compiler to 6.28571
   (non-const) integer multiple = 5
   constexpr is ignored when XPi(multiple) is invoked, returns 15.7143
   ```

   \endgroup

## 4) par #define (déprécié)

Le préprocesseur peut aussi définir des constantes, par \fonction{\#define}.

Mais c'est une ancienne manière de faire, à présent.

   \code

   > ```cpp
   > #define Pi 3.14
   > ```

   \endgroup

# III) Les énumérations

À la différence de Java, les énumérations en C++ sont transformées en nombres.

Si l'on ne précise rien, le compilateur assignera 0 à la première valeur, puis +1 aux suivantes.

   \code

   > ```cpp
   > enum CardinalDirections
   > {
   >    North = 25,
   >    South,      // 26
   >    East,       // 27
   >    West        // 28
   > };
   > ```

   \endgroup

C++ autorise qu'une variable énumérée soit assignée à un entier.  

   \code

   > ```cpp
   > int someNumber = South;
   > ```

   \endgroup

mais c'est plutôt dangereux...

Depuis C++ 11, il est possible d'ajouter à la déclaration \fonction{enum}

   - soit le mot-clef \fonction{class}
   - soit le mot-clef \fonction{struct}

pour lui faire retrouver le "_comportement de Java_".

   \code

   > ```cpp
   > enum class CardinalDirections {North, South, East, West};
   > ```

   \endgroup

\bigbreak

\underline{C++ :} Ce n'est pas le \fonction{.} qui désigne une valeur d'enum, mais \fonction{::}

   \code

   > ```cpp
   > CardinalDirections direction = CardinalDirections::South; 
   > int someNumber = CardinalDirections::South; // n'est plus possible...
   > ```

   \endgroup

# IV) Les tableaux

Ceci va garantir l'initialisation d'un tableau à zéro :

   \code
 
   > int integer[5] = {};

   \endgroup

alors que ceci ne le fera pas : 

   \code

   > new integer[5];

   \endgroup

Ont peut aussi initialiser un tableau ainsi : 

   \code

   > int c[] = {3. 5};
   > // attention : pas int[] d = {3, 5}, comme en Java

   \endgroup

\bigbreak

De la même manière, \danger{il n'y a pas de vérification d'index} :  
   \demitab si l'index est hors tableau, il pointera n'importe où  
   \demitab et une modification de sa valeur pourra faire planter le programme.

# Annexe : exercices

### 1) énumération YourCards avec `Queen = 45`

   \code
   
   > ```cpp
   > #include <iostream>
   > using namespace std;
   > 
   > /** Enumération YourCards avec Queen = 45 */
   > enum YourCards
   > {
   >     Ace, Jack, Queen = 45, King
   > };
   > 
   > int main()
   > {
   >     cout << "Ace : " << YourCards::Ace << endl;
   >     cout << "Queen : " << YourCards::Queen << endl;
   >     cout << "King : " << YourCards::King << endl;
   >     return 0;
   > }
   > ```
   
   ```
   Ace : 0
   Queen : 45
   King : 46
   ```

   \endgroup

### 2) Comparer des tailles de types de variables

Ecrire un programme qui démontre que :  

   1. La taille d'un unsigned integer et celle d'un integer sont les mêmes  
   2. et qu'ils sont tous les deux plus petits qu'un long integer.

   \code
   
   > ```cpp
   > int main()
   > {
   >     cout << "taille d'un unsigned integer : " << sizeof(unsigned int) << " et d'un integer : " << sizeof(int) << endl;
   >     cout << "taille d'un long integer : " << sizeof(long int) << endl;
   > 
   >     bool a = sizeof(unsigned int) == sizeof(int);
   >     cout << "(taille unsigned integer = integer) ? " << a << endl;
   > 
   >     bool b = sizeof(unsigned int) < sizeof(long int);
   >     cout << "(taille integer < integer) ? " << b << endl;
   > }
   > ```
   
   ```
   taille d'un unsigned integer : 4 et d'un integer : 4
   taille d'un long integer : 8
   (taille unsigned integer = integer) ? 1
   (taille integer < integer) ? 1
   ```
   
   \endgroup

\bigbreak

\underline{Remarque :}

   > la précédence des opérateurs,  
   > où \fonction{<<} est plus prioritaire que \fonction{==} ou \fonction{<},  
   > fait qu'écrire ceci, sans parenthèses :
   >
   > \begingroup \footnotesize `cout << "(taille unsigned integer = integer) ? " << sizeof(unsigned int) == sizeof(int);`\endgroup
   > 
   > mènera à une erreur car il va essayer de faire une comparaison `(<<) == ?`, qui n'a pas de sens.

### 3) Calculer le périmètre et l'aire d'un cercle

   \code
   
   > ```cpp
   > #include <cmath>
   > #include <iostream>
   > using namespace std;
   > 
   > /**
   >  * Calculer le périmètre d'un cercle, mais avec l'aire et le périmètre en integer
   >  */
   > int main() {
   >     cout << "Entrez le rayon du cercle dont vous voulez calculer le périmètre : ";
   > 
   >     double rayon;
   >     cin >> rayon;
   > 
   >     int  perimetre = rayon * M_PI;
   >     int aire = (rayon * rayon) * M_PI;
   >     cout << "son périmètre est de " << perimetre << " et son aire : " << aire << endl;
   > 
   >     return 0;
   > }
   > ```
  
   ```
   Entrez le rayon du cercle dont vous voulez calculer le périmètre : 14.2
   son périmètre est de 44.6106 et son aire : 633.471
   ```
 
   \endgroup

