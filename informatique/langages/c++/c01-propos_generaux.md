---
title: " 01. Propos généraux sur le C++"
subtitle: ""
author: Marc Le Bihan
keywords: 
- CMake
- cin
- cout
- #include
- main
- namespace
- add_executable
- CLion
- std
---

_CLion_ peut demander qu'un fichier \fonction{CMake} soit généré avant de faire le moindre contrôle.  
Dans ce cas, lui laisser en créer un, par défaut.

### Les \#include

   Les \fonction{\#include} vont chercher les `.h` dans des répertoires différents selon qu'ils encadrent son nom avec un __\#__ ou des __\<\>__.

   \code

   > ```cpp
   > #include "monEntete.h" // Mon entête personnel
   > #include <iostream>    // Un entête standard
   > ```

   \endgroup

### La fonction main

La fonction \fonction{main} a deux variantes :

   \code

   > ```cpp
   > int main()                       // Sans arguments
   > int main(int argc, char *argv[]) // arguments optionnels
   > ```

   \endgroup

### std::cout et les namespaces

\fonction{std:cout} est la sortie console (à importer d'\fonction{iostream.h}).  
\fonction{std:endl} est le saut de ligne.

à l'inverse, \fonction{std::cin} demandera un contenu sur le flux d'entrée (une saisie à l'utilisateur).

\bigbreak

\fonction{std} ici, est un namespace, qui dit quelle `cout` utiliser si le compliateur en connaît plus d'un.

\bigbreak

Pour éviter de répéter `std::...` devant chaque instruction `cout`, il est possible de déclarer qu'on va l'utiliser pour l'avenir. Dans un bloc, par exemple, placer :

   \code

   > ```cpp
   > using namespace std;
   > cout << "Hello world !" << endl;  // au lieu de std::cout << "Hello world !" << std::endl;
   > ```

   \endgroup

   Remarque : \fonction{cin >> firstNumber}, pour sa part, réclamera une saisie console à l'utilisateur (input).

`using` peut aussi s'utiliser sans `namespace` accolé, pour charger n'importe quel artefact (\gris{?}).

## CMake

### Diviser un CMake pour qu'il y ait un exécutable par source

Ici, pour `C++ 20`, dans le cas où les noms de sources contiennent des espaces.

\underline{Attention :} les paramètres de \fonction{add\_executable} sont séparés par des sauts de lignes et pas par des virgules !

   \code

   > ```cmake
   > cmake_minimum_required(VERSION 3.23)
   > project(cppChapitre02)
   > 
   > set(CMAKE_CXX_STANDARD 20)
   > 
   > add_executable(HelloWorldAnalysis
   >         "2.1 HelloWorldAnalysis.cpp")
   > 
   > add_executable(UsingNamespaceDecl
   >         "2.2 UsingNamespaceDecl.cpp")
   > ```

   \endgroup
   
