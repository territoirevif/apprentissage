---
header-includes:
   \input{apprentissage-header-include}
   \input{apprentissage-include}

title: "Python  \n03. Les listes"
subtitle: ""
author: Marc Le Bihan
geometry: margin=2cm
fontsize: 12pt
output: pdf
classoption: fleqn
urlcolor: blue
---

# I) Constructions de listes

## 1) Déclarations habituelles

Une liste se déclare avec :

   \code

   > ```python
   > liste = [12, 11, 18, 7, 15, 3]
   > liste[2:] # donne [18, 7, 15, 3]
   > liste[:2] # donne [12, 11]
   > liste[:] ou iiste[0:len(liste)] # toute la liste
   > liste[-1] est le dernier élément de la liste,
   > liste[-2] l'avant-dernier...
   > ```

   \endgroup

## 2) Initialisations élaborées

### a) Strutures de tableau

   \code

   > ```python
   > liste = [2, 4, 6, 8, 10]
   > [[x, x ** 3] for x in liste]
   > [[2, 8], [4, 64], [6, 216], [8, 512], [10, 1000]]
   > ```

   \endgroup

### b) Avec des nombres aléatoires allant de 0 à 9

   \code

   > ```python
   > from random import sample
   > A = sample(range(10), 10)
   > ```

   \endgroup

### c) avec un produit cartésien

   \code

   > ```python
   > colors = ['black', 'white']
   > sizes = ['S', 'M', 'L']
   > tshirts = [(color, size) for color in colors for size in sizes]
   > ```
   > 
   > ```txt
   > tshirts
   > [('black', 'S'), ('black', 'M'), ('black', 'L'), ('white', 'S'), ('white', 'M'), ('white', 'L')]
   > ```

   \endgroup

### d) Avec une relation

Les diviseurs de 100 :

   \code

   > ```python
   > n = 100
   > [d for d in range(1, n+1) if n % d == 0]
   > [1, 2, 4, 5, 10, 20, 25, 50, 100]
   > ```

   \endgroup

### e) Avec numy.arange

   \code

   > ```python
   > import numpy as np
   >
   > # Intervalle [10, 25[, par pas de 5
   > np.arange(10, 25, 5)
   > ```

   ```
   A vaut array([10, 15, 20])
   ```

   \endgroup

## II) Opérations habituelles sur les listes

\fonction{in} teste l'existence dans une liste.

   > ci-dessus, `12 in liste` répondra `True`,  
   > `25 in liste`, `False`.

\underline{Les opérations les plus utiles sur les listes :}

   | Méthode                 | Effet                                             |
   | ----------------------- | ------------------------------------------------- |
   | \methode{len}{(L)}      | donne la taille d'une liste                       |
   | \methode{append}{(x)}   | ajoute l'élément en fin de liste                  |
   | \methode{extend}{(L)}   | ajoute en fin de liste, une autre liste           |
   | \methode{insert}{(i,x)} | insère un élément x en position i. Est équivalente à liste[i:i] = x |
   | \methode{del}{(i)}      | supprime un élément en position i. Exemple: `del(a[2])` retire l'élément d'index 2 de a. |
   | \methode{remove}{(x)}   | supprime la __première__ occurrence de x          |
   | \methode{pop}{([i])}    | supprime l'élément d'indice [i], en le renvoyant  |
   | \methode{index}{(x)}    | renvoie l'index de la __première__ occurence de x |
   | \methode{count}{(x)}    | renvoie le nombre d'occurrences de x              |
   | \methode{sort}{()}      | trie la liste                                     |
   | \methode{reverse}{()}   | trie la liste en ordre inverse                    |

\attention{Attention !}  

   \code

   > ```python
   > copie = liste    # donne la même assignation (pointeur) qu'à liste. C'est un alias.
   > copie = liste[:] # réalise la vraie copie désirée.
   > ```

   \endgroup

# III) Les types de listes

## 1) Les listes mutables

   - `list` : objet séquentiels
   - `bytearray`
   - `array.array`
   - `collections.deque`
   - `memoryview`

## 2) Les immutables

   - `tuple` : les t-upplets. `a = 'hello', 2` créera dans `a` le 2-upplet `('hello', 2)`.
   - `set` : ensemble d'éléments, où chaque élément est unique.
   - `str` : dans l'absolu... Puisqu'il se divise en caractères.
   - `bytes`

\bigbreak

   \code

   > ```python
   > tuple(ord(symbol) for symbol in symbols)
   > (36, 108, 163, 101)
   > 
   > list(ord(symbol) for symbol in symbols)
   > [36, 108, 163, 101]
   > 
   > import array
   > array.array('I', (ord(symbol) for symbol in symbols))
   > array('I', [36, 108, 163, 101])
   > ```

   \endgroup

# IV) Les dictionnaires `dict` (map)

Les \definition{dict} sont les \fonction{map} des autres languages : on accède à ses valeurs avec une clef.

Lorsque l'on veut l'initialiser statiquement, l'on utilise les accolades \fonction{\{...\}} :

   \begingroup \cadreGris \small

   > ```python
   > d = {'A': 5, 'B': 3, 'C': 'Hector', 'D': -4.3}
   > print(d)        # affiche : {'A': 5, 'B': 3, 'C': 'Hector', 'D': -4.3}
   > print(d['B'])   # affiche : 3
   > ```

   \endgroup

\fonction{dict.keys()} donnera la liste des clefs du dictionnaire,  
\fonction{dict.values()} ses valeurs.

