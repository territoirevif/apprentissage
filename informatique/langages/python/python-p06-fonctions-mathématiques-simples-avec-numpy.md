---
header-includes:
   \input{apprentissage-header-include}
   \input{apprentissage-include}

title: "Python  \n06. Le module `numpy` pour les fonctions mathématiques simples" 
subtitle: "exponentielle, logarithme, racine carrée..."

category: développement

author: Marc Le Bihan
geometry: margin=2cm
fontsize: 12pt
output: pdf
classoption: fleqn
urlcolor: blue
---

À importer depuis par un `import numpy as np`

### Exponentielle

   \code

   > ```python
   > np.exp(x)
   > ```

   \endgroup\bigbreak

### Logarithme

   \code

   > ```python
   > np.log(x)
   > no.log10(x)
   > ```

   \endgroup\bigbreak

### Racines carrées

   \code

   > ```python
   > np.sqrt(x)
   > ```

   \endgroup\bigbreak

### Sinus, cosinus...

  \code

   > ```python
   > np.sin(x)
   > np.cos(x)
   > ```

   \endgroup\bigbreak

