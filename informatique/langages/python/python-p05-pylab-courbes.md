---
header-includes:
   \input{apprentissage-header-include}
   \input{apprentissage-include}

title: "Python  \n05. Le module `pylab` pour les courbes" 
subtitle: ""
author: Marc Le Bihan
geometry: margin=2cm
fontsize: 12pt
output: pdf
classoption: fleqn
urlcolor: blue
---

# I) Points et nuages de points avec `plot`

Tracer des points $(x,y)$ est enfantin :

   \code
  
   > ```python
   > import pylab as pl
   >
   > # deux points apparaîtront aux coordonnées (2,3) et (5,6)
   > pl.plot([2,5], [3,6],'o')  
   > ```

   \endgroup 

Mais ce sera plus malin d'avoir stocké dans $X$ une liste d'abscisses et dans $Y$ une liste d'ordonnées (ou de $f(x)$), et de l'appeler par `pl.plot(X,Y,'o')`.

\underline{Remarque :} si l'on met pas le dernier paramètre `'o'`, il fait des droites.

## 1) Subdivision de graphe en deux colonnes

On peut faire des sous-graphes, supprimer des axes...

   \code
   
   > ```python
   > # pl.subplots renvoie plusieurs valeurs : 
   > #   <Figure>, array([<AxesSubplot:>, <AxesSubplot:>], dtype=object))
   > fig, (ax1, ax2) = pl.subplots(1, 2, figsize = (6, 2))
   > 
   > ax1.set_xticks(()) # Pour dire qu'on ne veut pas des abscisses
   > ax1.set_yticks(()) # et ici, pas des ordonnées sur le schéma 1
   > ax1.text(0.5, 0.5, 'Zone 1', ha = 'center', va = 'center', size = 24, alpha = 0.5)
   > 
   > ax2.set_xticks(()) # Pour dire qu'on ne veut pas des abscisses
   > ax2.set_yticks(()) # et ici, pas des ordonnées sur le schéma 2
   > ax2.text(0.5, 0.5, 'Zone 2', ha = 'center', va = 'center', size = 24, alpha = 0.5)
   > ```
 
   \endgroup

   ![graphe séparé en deux colonnes](informatique/graphe_séparé_en_deux_colonnes.jpg){width=40%}

## 2) en deux colonnes

   \code
   
   > ```python
   > fig, (ax1, ax2) = pl.subplots(2, 1, figsize = (4, 4))
   > ax1.axis(([1, 5, -5, 5]))       # axe x : [1, 5], y : [-5, 5]
   > ax1.set_yticks(range(-4, 5, 2)) # axe y : marquer les unités de -4 à 5 (exclu) par pas de 2
   > ax1.plot([3, 4], [2, 4], 'o', color = 'red')
   > 
   > ax2.axis(([1, 5, -5, 5]))
   > ax2.set_yticks(range(-4, 5, 2))
   > ax2.plot([3, 4], [2, 4], 'o-', color = 'black') # 'o-' relie les points
   > ```
 
   \endgroup

   ![graphe séparé en deux lignes](informatique/graphe_séparé_en_deux_lignes.jpg){width=40%}

# II) Histogrammes avec `bar`

   \code
  
   > ```python
   > X = [2,5,7,1]
   > Y = [3,6,0,5]
   > pl.bar(X,Y)
   > ```

   \endgroup 

![histogrammes avec bar](informatique/pylab_bar.jpg){width=40%}

On peut placer des paramètres additionnels, comme :

   - `color = 'red'` pour choisir une couleur,
   - ou `alpha = 0.5` pour lui donner une opacité.

\bigbreak

Un histogramme plus élaboré

   \code

   > ```python
   > # Premier histogramme (en gris) et second en rouge
   > pl.bar([1, 2, 3], [0.5, 0.3, 0.2], color = 'gray', edgecolor = 'black', width = 0.5)
   > pl.bar([1, 2, 3], [0.2, 0.5, 0.3], color = 'red', edgecolor = 'black', width = 0.5, alpha = 0.5)
   > pl.xticks(range(1,4), fontsize = 14)
   > ```

   \endgroup

![deux histogrammes superposés](informatique/deux_histogrammes_superposés.jpg){width=40%}


# III) Tracer des courbes

## 1) Définir les axes avec $x = a$ ou $y = a$ avec `axvline` et `axhline`, ou `axis`

(respectivement `axvline` : axe vertical, et `axhline` : axe horizontal)

   \code
  
   > ```python
   > pl.axvline(x = 5)
   > pl.axhline(y = 2)
   > ```

   \endgroup 

Créera deux axes : l'un $x = 5$ et l'autre $y = 2$, mais attention `axvline` attendra un paramètre $x$, ou qu'il ne soit pas nommé, et `axhline`, lui, un paramètre $y$.

La méthode \fonction{axis} est plus précise pour définir des axes.

   \code
  
   > ```python
   > pl.axis(xmin=-5, xmax=7, ymin=-3, ymax=2) # Axe des x : de -5 à 7, axe des y : de -3 à 2
   > ```

   \endgroup

## 2) Tracer une courbe avec `plot`

Si l'on veut tracer par exemple $\bleu{f(x) = x^2 - 3x + 1}$ sur l'intervalle $\bleu{[-2,8]}$ :

   \code
  
   > ```python
   > pl.style.use('bmh')
   > 
   > X = [-2 + k/100 for k in range(1000)] # k ira de -2 à 8 par pas de 0.01
   > Y = [x ** 2 - 3 * x + 1 for x in X]   # Pour tout x ∈ X, calcuer y = x² -3x + 1
   > 
   > pl.plot(X, Y, color='red', label = r'$\mathscr{C}_f$') # Place le Cf en légende du graphe
   > pl.legend(fontsize = 18) # Cet appel est important : sans, la légende ne s'affichera pas
   > ```

   \endgroup

   ![courbe avec plot](informatique/pylab_courbe_f\(x\).jpg){width=50%}

