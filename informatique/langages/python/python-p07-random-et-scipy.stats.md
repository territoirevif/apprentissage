---
header-includes:
   \input{apprentissage-header-include}
   \input{apprentissage-include}

title: "Python  \n06. Le module `random` pour les variables aléatoires, `scipy.stats` pour les statistiques" 
subtitle: ""

category: développement

author: Marc Le Bihan
geometry: margin=2cm
fontsize: 12pt
output: pdf
classoption: fleqn
urlcolor: blue
---

À importer depuis par un `from random import *`

# I) Génération classique de nombres aléatoires

  - Un nombre aléatoire compris dans [0,1] ou [a,b]
  - Un nombre aléatoire, mais entier, compris dans [a,b]

   \code
   
   > ```python
   > # Un nombre aléatoire compris entre 0 et 1
   > from random import *
   > print(random())
   > 
   > # Pour avoir un autre nombre aléatoire il faut additionner, multiplier...
   > print(3 * random() + 1)
   > 
   > # Ou utiliser randint(a, b) pour obtenir un nombre entier compris dans [a,b]
   > print(randint(7, 12))
   > ```

   ```
   0.7255164424196654
   1.8798736912218417
   10
   ```
    
   \endgroup\bigbreak

   - Un choix aléatoire, dans une liste

   \code
   
   > ```python
   > # Choisir aléatoirement dans une liste
   > L = [25, -3, 11, 15, 'A']
   > print(choice(L))
   > ```

   ```
   A
   ```
    
   \endgroup

# II) En demandant de suivre une loi

## 1) loi uniforme

   \code
   
   > ```python
   > print(uniform(7, 12))
   > ```
   
   ```
   8.469072712839637
   ```
   
   \endgroup

## 2) Loi Binomiale

Exemple : avec $\bleu{X \sim \mathcal{B}(100, 0.4)}$

\underline{$\mathbb{P}(X = 50)$ :}

Remarque : si `scipy` se déclare absent, utiliser la commande \fonction{pip3 install scipy} pour l'installer.

   \code
   
   > ```python
   > from scipy.stats import binom
   >
   > X = binom(50, 0.4)
   > X.pmf(50) 
   > ```

\bigbreak
  
   ```
   1.2676506002282329e-20
   ```
 
   $\mathbb{P}(X = 50) = 1.268.10^{-20}$ \demitab Normal ! Obtenir 50 succès sur 50 tirages à 40\% chacun...
   
   \endgroup

