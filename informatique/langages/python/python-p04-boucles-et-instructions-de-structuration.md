---
header-includes:
   \input{apprentissage-header-include}
   \input{apprentissage-include}

title: "Python  \n04. Boucles et instructions de structuration"
subtitle: ""
author: Marc Le Bihan
geometry: margin=2cm
fontsize: 12pt
output: pdf
classoption: fleqn
urlcolor: blue
---

# Instructions de structuration

## if

   \begingroup \cadreGris \small

   ```python
   if condition :
      instructions
   else:
      autres instructions
   ```

   \endgroup

### Les comparateurs

Le test d'égalité se fait avec \fonction{==} en Python.  
Celui d'égalité entre deux pointeurs, avec \fonction{is}.

Les autres opérateurs sont __and__, __or__, __not__

# Les boucles

## for

   \begingroup \cadreGris \small

   > ```python
   > for i in range(5)         # va de 0 à 4
   > for j in range(1, 10, 2)  # 1, 3, 5, 7, 9
   > for x in [10, 'a', 3.2]   # est possible aussi
   > for c in 'abc'            # a, b, c
   > ```

   \endgroup

Attention : dans \fonction{range}, la borne supérieure est exclue !

On peut adjoindre un \fonction{if} à \fonction{for}, mais dans ce cas il faut mettre l'instruction et sa condition entre crochet, et elle sert pour définir un ensemble en compréhension :

   \begingroup \cadreGris \small

   > ```python
   > L = range(1,10,2)
   > b = [x for x in L if x > 5]         # donnera b = [7, 9]
   >
   > L = ["silex", "véranda", "oiseau"]
   > c = [s for s in L if 's' in s]      # c = ['silex', 'oiseau']
   > ```

   \endgroup

## while

   \begingroup \cadreGris \small

   ```python
   while conditions :
     instructions
   ```

   \endgroup
