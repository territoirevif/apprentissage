---
title: "p02. Les variables"
subtitle: ""
author: Marc Le Bihan
geometry: margin=2cm
fontsize: 12pt
output: pdf
classoption: fleqn
urlcolor: blue
---

## 1) Les types de variables

   - les chaînes de caractères (\fonction{str})
   - le booléen (\fonction{bool})
   - l'entier (\fonction{int})
   - le nombre réel (\fonction{float})  
   - le nombre complexe (\fonction{complex})  
      \demitab remarque : dans sa notation, $i$ est remplacé par $\mauve{J}$ ou $\mauve{j}$ ($\to 3+4J$)

### a) Conversion d'un type en un autre

\mauve{str(a)}\ \    \tab transforme \fonction{a} en chaîne de caractères  
\mauve{int(a)}\ \    \tab en entier  
\mauve{float(a)} \tab en nombre réel

### b) Connaître le type d'une vraiable et autres manipulations

\fonction{id(x)}\ \    \tab va renvoyer le pointeur d'un objet,  
\fonction{type(x)}\tab va renvoyer son type.  
\fonction{del(x)}\   \tab supprimera l'objet.

## 2) Quelques fonctions sur les variables str (string)

   \code

   > ```python
   > s.replace("a", "b") # Replace a par b.
   > s.count("a")        # Le nombre de a dans la chaîne de caractères
   > s.upper()           # Convertit en majuscules
   > ```

   \endgroup

## 3) Substitution de variables

Une substution de variables avec Python :

   \code

   > ```python
   > x,y = y,x
   > ```

   \endgroup

\bigbreak

pas besoin de variable temporaire.

