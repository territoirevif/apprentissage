---
title: "p01. Généralités"
subtitle: ""
author: Marc Le Bihan
---

### Entête de source

En début de fichier, l'on peut préciser l'encodage du source, et quel interpréteur utiliser.

```txt
#!/usr/bin/python3
#-*- coding: Utf-8 -*-
```

### Module

Un module, en Python, est tout ce qui a une extension `.py`, en général, et que l'on importe ailleurs.

Par exemple, les modules : `math`, `pandas`, `ploty`...

### Redéfinition d'opérateurs

\underline{Remarque :} Pas besoin de mot-clef \fonction{new} pour instancier un objet.  

\underline{Python :} Redéfinition de l'addition pour deux vecteurs

   \code

   > ```python
   > v1 = Vector(2,4)
   > v2 = Vector(2,1)
   >
   > # Redéfinition de l'addition pour deux vecteurs
   > def __add__(self, other):
   >     x = self.x + other.x
   >     y = self.y + other.y
   >     return Vector(x, y)
   > ```
   >
   > produit le résultat sur la console :
   >
   > ```text
   > v1+v2
   > Vector(4, 5)
   > ```

   \endgroup

## Fonctions

Attention : si ceci fonctionne bien :

   \code

   > ```python
   > def f(x):
   >     return x ** 0.5
   > ```
   > ```txt
   > f(3)+f(4)+(f5)
   > 5.146264369941973
   > ```

   \endgroup

Cela ne retourne pas une valeur, mais \mauve{NoneType}, et provoque un bug !

   \code

   > ```python
   > def f(x):
   >     print(x ** 0.5)
   > ```
   >
   > ```txt
   > f(2)+f(3)+f(4)
   > 1.4142135623730951
   > 1.7320508075688772 traceback (most recent call last):
   >   File "/usr/lib/python3.9/code.py", line 90, in runcode
   >     exec(code, self.locals)
   >   File "<input>", line 1, in <module>
   > TypeError: unsupported operand type(s) for +: 'NoneType' and 'NoneType'
   > ```

   \endgroup

### Objets

\fonction{\_\_init\_\_} est le nom de la méthode du constructeur d'un objet.

   \code

   > ```python
   > def __repr__(self):
   >   return 'Vector(%r, %r)' % (self.x, self.y)
   > ```

   \endgroup

et \methode{\_\_str\_\_}{(self)} sont deux manières de faire un \fonction{toString()} d'un objet.  
on considère que `__str__` est plus pour une sortie à destination de l'_utilisateur_.

## Divers

   \code

   > ```python
   > print(15, 12.3, 8, sep='; ', end=':')  # => 15; 12.3; 8.
   > ```

   \endgroup

\bigbreak

   \fonction{Format} est l'équivalent de `sprintf`, et l'on peut mettre des \fonction{\{20.3f\}} si l'on veut pour customiser les paramètres.

   \code
   
   > ```python
   > x = eval(input("Entrez une valeur pour la variable x :"))
   > Entrez une valeur pour la variable x :>? 3
   > print("{}^2 = {}".format(x, x**2))
   > 3^2 = 9
   > ```

   \endgroup
  
