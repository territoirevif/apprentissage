---
title: "sb03. Démarrage d'une application"
subtitle: ""
author: Marc Le Bihan
---

## 1) Bandeau de démarrage de l'application

On peut le configurer dans `SpringAnnotation` par la class \fonction{Banner}.

   \code

   > ```java
   > @SpringBootApplication
   > public class DemoApplication {
   >    // Une bannière différente va apparaître dans les logs, au démarrage de Spring-boot
   >    public static void main(String[] args) {
   >       SpringApplication app = new SpringApplication(DemoApplication.class);
   >       
   >       // Bannière de démarrage personnalisée.
   >       app.setBanner(new Banner() {
   >          @Override
   >          public void printBanner(Environment environment, Class<?> sourceClass, PrintStream out) {
   >             out.print("*** Application de démonstration ***");
   >          }
   >       });
   >       
   >       app.run(args);
   >    }
   > }
   > ```

   Alternative en `lambda` :

   > ```java
   > app.setBanner((environment, sourceClass, out) -> out.print("*** Application de démonstration ***"));
   > ```

   \endgroup

## 2) Le SpringApplicationBuilder

Est un Builder utilitaire pour se facilier la préparation d'une application Spring :

   \code

   > ```java
   > public static void main(String[] args) throws IOException {
   >    SpringApplication app = new SpringApplication(SpringBootSimpleApplication.class);
   >    app.setBannerMode(Banner.Mode.OFF);
   >    app.run(args);
   > }
   > ```

   \endgroup

   On recommande de déclarer une application web comme un \fonction{.child(...)}. \gris{(pourquoi ?)}

   On peut aussi déclarer un profil, placer des listeners...

   \code

   > ```java
   > public static void main(String[] args) throws IOException {
   >    SpringApplication app = new SpringApplication(SpringBootSimpleApplication.class);
   >
   >    app.setBannerMode(Banner.Mode.OFF)
   >    .child(MyConfig.class)
   >    .profiles("production")
   >    .listeners(new ApplicationListener<ApplicationEvent>() {
   >       @Override
   >          public void onApplicationEvent(ApplicationEvent event) {
   >          log.info("#### > " + event.getClass().getCanonicalName());
   >       }
   >    })
   >    app.run(args);
   > }
   > ```

   \endgroup

\bigbreak

Parmi ces évènements :

   > `ApplicationStartedEvent` : au démarrage de l'application  
   > `ApplicationEnvironmentPreparedEvent` : quand l'environnement est connu  
   > `ApplicationPreparedEvent` : quand la définition des beans est établie  
   > `ApplicationReadyEvent` : quand l'application est considérée comme prête  

   > `ApplicationFailedEvent` : quand elle a, au contraire, rencontré un incident.

\bigbreak

Il est possible de dire à SpringBoot, de ne pas se considérer comme gérant un type d'application particulière (et ne pas réaliser certaines initialisations) :

   \code
   
   > ```java
   > SpringApplication app = new SpringApplication(SpringBootSimpleApplication.class);
   > app.web(WebApplicationType.NONE); // L'application (même si elle est dans un war) n'est pas une application web
   > ```

   \endgroup

\fonction{WebApplicationType.SERVLET} ou \fonction{WebApplicationType.REACTIVE} permettent, au contraire, de la caractériser, à l'inverse.

## 3) Passage des arguments dans les composants enfants

Si une application Spring débute ainsi :

   \code

   > ```java
   > public static void main(String[] args) throws IOException {
   >    SpringApplication.run(SpringBootSimpleApplication.class, args);
   > }
   > ```

   \endgroup

ses composants pourront accéder à ces paramètres, placés dans un objet de classe \fonction{ApplicationArguments} :

   \code

   > ```java
   > @Component
   > public class MonComposant {
   >    @Autowired // Important ! Il faut que le constructeur soit injecté
   >    public MonComposant(ApplicationArguments args) {
   >       // La liste des arguments qui ne sont pas des options :
   >       List<String> arguments = args.getNonOptionArgs();
   >
   >       // Est-ce qu'il y a l'option --enable ?
   >       if (args.containOption("enable") {
   >          ...
   >       }
   >    }
   > }
   > ```

   \endgroup

\underline{Remarque :}

Si l'on utilise la commande \methode{mvn}{spring-boot:run} pour lancer une application par Maven,  
on lui passera ses arguments au travers de l'option  
   \demitab `-Dspring-boot.run.arguments="a1,a2"`  
   \demitab `-Dspring-boot.run.arguments="--enable"`

## 4) ApplicationRunner et CommandLineRunner

La méthode `main(...)` d'une application Spring Boot débute en général avec l'instruction :  
`SpringApplication.run(SpringBootSimpleApplication.class, args)`

Mais il n'est pas possible d'exécuter une instruction directement en dessous de celle-ci.

### a) ApplicationRunner, pour réagir juste après le démarrage de l'application

   \code

   > ```java
   > public class SpringBootSimpleApplication implements ApplicationRunner {
   >    @Override
   >    public void run(ApplicationArguments args) throws Exception {
   >       log.info("## > ApplicationRunner Implementation...");
   >       args.getNonOptionArgs().forEach(file -> log.info(file));
   >    }   
   > }
   > ```

   \endgroup

### b) CommandLineRunner, pour examiner les arguments de la ligne de commande

   \code

   > ```java
   > public class SpringBootSimpleApplication implements CommandLineRunner {
   >    @Override
   >    public void run(String... args) throws Exception {
   >       log.info("## > CommandLineRunner Implementation...");
   >
   >       for(String arg:args)
   >          log.info(arg);
   >    }
   > }
   > ```

   \endgroup

\bigbreak

\underline{Variante :} Extraction par une annotation `@Bean`

On peut ne pas implémenter d'interface, et à la place, invoquer un `@Bean`.

   \code

   > ```java
   > @Bean
   > CommandLineRunner myMethod(){
   >    return args -> {
   >       log.info("## > CommandLineRunner Implementation...");
   >
   >       for(String arg:args)
   >       log.info(arg);
   >    };
   > }
   > ```

   \gris{(mais d'où vient le `args` ?)}

   \endgroup

\bigbreak

\underline{Remarque :} Quand il y a plusieurs méthodes annotées par `@Bean`, l'annotation \fonction{@Order} permet de choisir leur ordre d'exécution.

