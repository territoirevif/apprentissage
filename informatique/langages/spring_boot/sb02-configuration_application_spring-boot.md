---
title: "sb02. Configuration d'une application"
subtitle: ""
author: Marc Le Bihan
---

## 1) Les annotations `@Enable<Technology>` et `@EnableAutoConfiguration`

### a) Suppression de l'autoconfiguration de quelques modules

Les annotations \fonction{@SpringBootApplication} ou \fonction{@EnableAutoConfiguration} ((les classes d'auto-configuration ont le suffixe `AutoConfiguration`)

   \code

   > ```java
   > @SpringBootApplication{exclude={DataSourceAutoConfiguration.class}}
   > ```

   \endgroup

elles ont des attributs \fonction{exlude} (qui prend des classes) ou \fonction{excludeName} (qui prend des String), qui peuvent désactiver l'auto-configuration de certains éléments.

### b) Activation à l'unité

Les technologies peuvent être activées à l'unité :

\fonction{@EnableTransactionManagement}, \fonction{@EnableRabbit}, \fonction{@EnableIntegration}...

## 2) Les autres annotations de configuration

Peuvent s'associer à l'annoation \fonction{@Configuration} : \fonction{@AutoConfigureOrder}, \fonction{@ConditionalOnClass}, \fonction{@ConditionalOnMissingBean}, \fonction{@ConditionalOnProperty}...

Une configuration peut se lier à un profil particulier, par \fonction{@Profile("unProfil")},  
utiliser l'annotation \fonction{@Import} regroupant les configurations, que propose _Spring Boot_.

## 3) Les emplacements pour la configuration de l'application

Les propriétés de configuration sont souvent utilisées via l'annotation \fonction{@Value} :

   \code

   > ```java
   > @Value("${data.server}")
   > private String server;
   > ```
   
   \endgroup

On peut aussi les obtenir par l'interface \fonction{Environnement}, obtenue de l'interface \fonction{PropertyResolver}.

### a) `SPRING_APPLICATION_JSON` une variable d'environnement

Une variable d'environnement qui présente les clefs et valeurs.  
À définir ou exporter avant de lancer.

   \code

   > ```bash
   > SPRING_APPLICATION_JSON='{"data: {"server": "remoteserver:3030"}}'
   > java -jar monApplicaton.jar
   > ```
   
   \endgroup

### b) application.properties

Placé à la racine de l'application, on peut y placer :

   - nos paramétrages spécifiques

   \code

   > ```properties
   > data.server=remoteserver:3030
   > ```

   \endgroup

\bigbreak

   - un capable d'overrider le comportement de Spring-boot :  
     [le common-application.properties de Spring-boot](https://docs.spring.io/spring-boot/docs/current/reference/html/application-properties.html)  
     \avantage{à aller regarder, car des comportements intéressants et inattendus peuvent peut-être être réglés ici.}

\underline{Remarque :}

   > `application.yaml` propose la même chose, mais en yaml.

   - variable d'environnement : c'est devenu le choix du cloud

### c) La ligne de commande

On peut, en ligne de commande, passer directement sa propriété préfixée par le `--` d'option.

   \code

   > ```bash
   > java -jar monApplication.jar --data.server=remoteserver:3030
   > ```

   \endgroup

\bigbreak

Si on lance l'application par Maven (plugin:goal `spring-boot:run`), on la préfixera par `-D`

   \code

   > ```bash
   > mvn spring-boot:run -Ddata.server=remoteserver:3030
   > ```

   \endgroup

\bigbreak

\underline{Remarque :} si l'on veut passer la variable d'environnement `SPRING_APPLICATION_JSON` en ligne de commande, c'est possible :

   \code

   > ```bash
   > java -jar monApplication.jar --spring.application.json='{"data: {"server": "remoteserver:3030"}}'
   > mvn spring-boot:run -Dspring.application.json='{"data: {"server": "remoteserver:3030"}}'
   > ```

   \endgroup

\bigbreak

\avantage{Mais l'inverse \underline{est aussi} possible :}

   \code

   > ```bash
   > DATA_SERVER='remoteserver:3030'
   > java -jar monApplicaton.jar
   > ```
   
   \endgroup

### d) L'ordre d'override des propriétés de configuration

   1. Arguments de la ligne de commande
   2. `SPRING_APPLICATION_JSON`, la variable d'environnement,
   3. JNDI (java:comp/env)
   4. `System.getProperties()`
   5. Les variables d'environnement du système d'exploitation
   6. La source \fonction{RandomValuePropertySource} (`random.*`)
   7. Spécifique au profile, (`application-{profile}.jar`), à l'extérieur du jar.
   8. Spécifique au profile, (`application-{profile}.jar`), à l'intérieur du jar.
   9. un `application.properties` à l'extérieur du jar.
   10. un `application.properties` à l'intérieur du jar.
   11. La source \fonction{@PropertySource}
   12. `SpringApplication.setDefaultProperties`

### \underline{Ordre de recherche des emplacements de application.properties}

   1. le __répertoire__ `/config` situé dans le répertoire courant de lancement.
   2. le répertoire courant de lancement
   3. le classpath `/config` (donc, à l'intérieur du `jar`)
   4. le classpath `root`

\bigbreak

Un \fonction{--spring.config.name=myCfg}  
   \demitab (respectivement `-Dspring.config.name=myCfg` ou `SPRING_CONFIG_NAME=myCfg`) 
   \demitab ajouté à la ligne de commande  
      \tab (respectivement commande maven ou variable d'environnement)

peut changer le nom du fichier de propriétés à aller chercher, si on le désire.

   > Ici, `myCfg.properties` sera lu à la place de `application.properties`.

\bigbreak

Son emplacement peut être précisé par \fonction{spring.config.location}

  > Exemple : `--spring.config.location=classpath:META-INF/conf`  
  > s'il avait été rangé ailleurs dans un jar.

### \underline{Le relaxed binding}

   > De manière générale, Spring-Boot va accepter les écritures :  
   >
   > `MESSAGE_DESTINATION_NAME`  
   > `message.destination-name`  
   > `message.destinationName`

et les lier, s'il les rencontre sous l'une de ces formes.

### e) Les profils

À l'aide des profils que l'on peut s'attribuer au lancement de son application par : 

   - le paramètre \fonction{spring.profiles.active} (avec un \underline{s}, car on peut en choisir plusieurs conjointement) 
   - ou l'annotation \fonction{@ActiveProfiles} 
   - ou la variable d'environnement \fonction{SPRING\_PROFILES\_ACTIVE} 

\bigbreak

l'on enclenche le chargement d'un fichier de propriétés suffixé par le nom du profil annoncé :

   > `--spring.profiles.active=prod` fera charger le fichier `application-prod.properties`

