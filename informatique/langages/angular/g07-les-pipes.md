---
title: "g07. Les pipes"
subtitle: ""
author: Marc Le Bihan
---

Les pipes permettent de transformer des données à l'affichage pour, par exemple, les rendre plus lisibles.

### L'exemple du pipe date

\code

> ```html
> <p>Nous sommes le {{ dateDuJour | date }}<p>
> ```

\endgroup

\bigbreak

le pipe \fonction{date} est paramétrable en `date : 'short'` par exemple, ou `date : 'EEEE d MMMM y'`  
et les pipes peuvent s'enchaîner : `dateDuJour | date : 'short' | uppercase`

L'ordre des pipes est important :  
un underscore appliqué sur `dateDuJour` échouera à mettre une majuscule sur ce qui est, encore à ce moment-là, un objet de classe `Date`.

### Le pipe async

Le pipe __async__ est requis pour les promesses, asynchrones, ainsi que pour les observables.

\code

> ```ts
>   /* Date de dernière mise à jour */
>   lastUpdate:Promise<Date> = new Promise((resolve, reject) => {
>     const date:Date = new Date();
> 
>     // Elle va être disponible au bout de 2 secondes
>     setTimeout(() => {
>       resolve(date);
>     }, 2000)
>   });
> ```

\endgroup

réclamera un `lastUpdate | async | date : 'short'` pour pouvoir s'afficher. 

Un `lastUpdate | date` direct provoquera une erreur.

