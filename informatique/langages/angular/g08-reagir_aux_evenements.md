---
title: "g08. Réagir aux évènements avec Angular"
subtitle: ""
author: Marc Le Bihan
---

# I) Observables et souscriptions

## 1) Un observable émet des informations

Une \definition{observable}, c'est un objet qui émet des informations dans le temps

   - un champ de texte,
   - une barre de progression
   - et toutes les communications HTTP.

on lui associe un __observer__ : un bloc de code qui sera appelé à chaque fois qu'il émettra une information, et qu'on va déclarer avec la méthode \definition{subscribe}.

   - il peut émettre une erreur,
   - une information,
   - ou un message complete, pour dire qu'il n'émettra plus rien (et que l'observable est détruite).

## 2) Filtrer, limiter, rassembler les évènements par des opérateurs

On pourra filtrer, limiter la fréquence, et agir d'autres manière sur des informations reçues de lui grâce à des opérateurs :

   - \fonction{filter}, pour filtrer des éléments
   - \fonction{throttleTime}, qui impose un délai entre l'émission de deux valeurs
   - \fonction{scan} et \fonction{reduce} vont rassembler et pour la seconde, réduire au résultat (par exemple une somme) des informations, avant de les renvoyer.

## 3) Exemple d'observable et d'écoutant (souscripteur)

   \code

   > ```ts
   > import { Component } from '@angular/core';
   > import { interval } from 'rxjs';
   > 
   > @Component({
   >   selector: 'app-root',
   >   templateUrl: './app.component.html',
   >   styleUrls: ['./app.component.scss']
   > })
   > export class AppComponent implements OnInit {
   >   /** Nombre de secondes. */
   >   secondes: number;
   > 
   >   constructor() {
   >     this.secondes = 0;
   >   }
   > 
   >   /** Initialisation */
   >   ngOnInit() {
   >     const counter = interval(1000); // Toutes les 1000 millisecondes
   > 
   >     counter.subscribe(
   >       value => {
   >         this.secondes = value;
   >       },
   >       error => {
   >          console.log("Une erreur a été rencontrée : " + error.value)
   >       },
   >       () => {
   >          console.log("Observable complété");
   >       }
   >     );
   >   }
   > }
   > ```

\begingroup \normalsize \bigbreak en lien, par exemple pour l'affichage, avec un :\endgroup

   > ```html
   > <div class="navbar-right">
   >   <p>Vous êtes connecté depuis {{secondes}} secondes</p>
   > </div>
   >
   > ```

   \endgroup

\bigbreak

\underline{Remarque :}

\danger{Avec une souscription infinie de cette nature, l'application risque de ne pas se fermer correctement !}  
Il est recommandé de mettre le souscription dans une variable membre à part entière, lui-même.

   \code

   > ```ts
   > export class AppComponent implements OnInit, OnDestroy {
   >   [...]
   >   /** Souscription au compteur. */
   >   counterSubscription: Subscription;
   > 
   >   constructor() {
   >     [...]
   >     this.counterSubscription = Subscription.EMPTY;
   >   }
   > 
   >   /** Initialisation */
   >   ngOnInit() {
   >     const counter = interval(1000); // Toutes les 1000 millisecondes
   > 
   >     this.counterSubscription = counter.subscribe(
   >       [...]
   >     );
   >   }
   > 
   >   /** Réaction à la destruction de l'application */
   >   ngOnDestroy(): void {
   >      this.counterSubscription.unsubscribe();
   >   }
   > }
   > ```

   \endgroup

# II) Subjects

Ils peuvent choisir ce qu'une application émettra, par exemple.

Pour qu'une application diffuse un contenu qu'elle possède, on le crée :

   \code

   > ```ts
   > export class AppareilService {
   >   appareilSubject = new Subject<any[]>();
   > 
   >   /* Liste des appareils */
   >   private appareils = [
   >     {
   >       id: 1,
   >       name: 'Machine à laver',
   >       status: 'éteint'
   >     },
   > 
   >     {
   >       id: 2,
   >       name: 'Télévision',
   >       status: 'allumé'
   >     },
   > 
   >     {
   >       id: 3,
   >       name: 'Ordinateur',
   >       status: 'éteint'
   >     }
   >   ];
   > 
   > /**
   >  * Renvoie une copie de la liste des appareils
   >  */
   > emitAppareilSubject() {
   >   this.appareilSubject.next(this.appareils.slice());
   > }
   > ```

   \endgroup

\bigbreak

   Et il faut utiliser alors la méthode `emit...()` créée dans chaque setter ou initialisation qui touche à la variable membre, afin de notifier tous les écoutants d'un/des changements.


   Coté écoutants, créer une variable membre `appareilSuscription: Subscription`, et dans `ngInit()`  l'initialiser avec : 

   \code

   > ```ts
   > /** Initialisation du service */
   > ngOnInit() {
   >   this.appareilSuscription = this.appareilService.appareilSubject.subscribe((appareils: any[]) => {
   >      this.appareils = appareils;
   >   });
   >
   >   this.appareilService.emitAppareilSubject();
   > }
   > ```

   \endgroup
