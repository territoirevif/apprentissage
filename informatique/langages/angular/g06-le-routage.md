---
title: "g06. Le routage sous Angular"
subtitle: ""
author: Marc Le Bihan
---

Le routing lie des vues et composants single page : cliquer sur un lien ne changera pas forcément de page, mais appelera une fonction.

\bigbreak

# I) La déclaration des routes

1. Un tableau de routes est sous la forme \fonction{\{'path', Composant\}}.

   \code

   > ```ts
   > const appRoutes: Routes = [
   >   { path: 'appareils', component: AppareilViewComponent },
   >   { path: 'auth', component: AuthComponent },
   >   { path: '', component: AppareilViewComponent }
   > ];
   >  ```

   \endgroup

> où le :   
> \ \ \ \ \fonction{path: 'appareils'} deviendra \fonction{http://localhost:4200/appareils} à l'exécution  
> \ \ \ \ \fonction{path: ''} (deux apostrophes) dira où aller par défaut si l'utilisateur tape \fonction{http://localhost:4200}

\bigbreak

2. On initialise les routes en déclarant l'import :

   \code

   > ```ts
   > imports: 
   >    RouterModule.forRoot(appRoutes)
   > ```

   \endgroup

\bigbreak

# II) L'utilisation des routes pour la navigation

3. Et en modifiant l'affichage initial de \fonction{app.component.html} par :

   \code

   > ```html
   > <div class="content" role="main">
   >   <div class="container">
   >     <div class="row">
   >       <router-outlet></router-outlet>
   >     </div>
   >   </div>
   > </div>
   > ```

   \endgroup

\tab On peut aussi utiliser le router pour de la navigation directe, en important la classe \fonction{Router} et en la transmettant dans le constructeur, puis en utilisant \fonction{navigate} :

   \code

   > ```ts
   > this.router.navigate(['appareils']);
   > ```

   \endgroup

### Exemple : séparer l'authentification du reste de l'application

4. Une barre de navigation utilisera ces routes   
mais un `href` dans la balise `<a>` rechargerait la page, et on y place plutôt \fonction{routerLink}.

   \begingroup \cadreGris \small

   > <nav class="navbar navbar-default">
   >   <div class="container-fluid">
   >     <ul class="nav navbar-nav">
   >       <li><a routerLink="appareils">Appareils</a></li>
   >       <li><a routerLink="auth">Authentification</a></li>
   >     </ul>
   >   </div>
   > </nav>

   \endgroup

\underline{Remarque :}

   > Placer l'attribut \fonction{<li routerLinkActive="active">} met un fond au `<li>` sur l'entrée de menu active.

# III) Passer un paramètre dans une route

La variante avec \fonction{:id} recevra un paramètre `id`.

   \begingroup \cadreGris \small

   > ```json
   > { path: 'appareils', component: AppareilViewComponent },
   > { path: 'appareils/:id', component: SingleAppareilComponent },
   > ```

   \endgroup

Derrière, un composant cible pourra extraire le paramètre ainsi :

   \code

   > ```ts
   > export class SingleAppareilComponent implements OnInit {
   >   /** Nom de l'appareil. */
   >   name: string = "appareil";
   > 
   >   /** Statut de l'appareil. */
   >   statut: string = "statut"
   > 
   >   /** Route. */
   >   route: ActivatedRoute;
   > 
   >   constructor(private appareilService: AppareilService, route: ActivatedRoute) {
   >     this.route = route;
   >   }
   > 
   >   ngOnInit(): void {
   >     this.name = this.route.snapshot.params['id'];
   >   }
   > }
   > ```

   \endgroup

\bigbreak

\tab Le principe est là, mais le code doit être rendu plus sain en désignant un identifiant de l'objet appareil (qu'on lui aura ajouté) et en mettant une fonction de recherche intermédiaire de l'objet en question.

   \code

   > ```ts
   > getAppareilById(id: number) {
   >   const appareil = this.appareils.find(
   >     (appareilObject) => {
   >       return appareilObject.id === id
   >   })
   >
   >   return appareil;
   > }
   > ```

  \endgroup

\bigbreak

et l'on fait évoluer la méthode `ngInit()` plus haut :

   \code

   > ```ts
   > ngOnInit(): void {
   >   const id = this.route.snapshot.params['id'];
   >  const appareil = this.appareilService.getAppareilById(+id);
   >
   >  if (appareil) {
   >    this.name = appareil.name;
   >    this.statut = appareil.name;
   >  }
   >  else {
   >    this.name = 'inconnu';
   >    this.statut = 'inconnu';
   >  }
   > }
   > ```

   \endgroup

Et l'on complète la définition de l'appareil en lui ajoutant un `@Input() id: number;` comme variable membre,  
et en le transmettant aussi à `appareil-view` :

   \code

   >```html
   > <app-appareil *ngFor="let appareil of appareils; let i = index"
   >               [appareilName]="appareil.name"
   >               [indexOfAppareil]="i"
   >               [appareilStatus]="appareil.status"
   >               [id]=appareil.id></app-appareil>
   > ```

   \endgroup

et dans le détail du html de l'`appareil` on va ajouter un lien de type `routerLink` :

   \code

   > ```html
   > <a [routerLink]="[id]">Détail</a>
   > ```

   \endgroup

# IV) Rediriger quand une page n'existe pas

   1. Créer un composant à cette fin, et lui donner la page d'erreur qu'on veut dans son html.

   2. Dans `app-module.ts`, à la fin des directive `path`, c'est à dire : après les dernières, placer :

   \code

   > ```ts
   > // Ici pour la redirection 404, À LA FIN, c'est important
   > { path: 'not-found', component: FourOhFourComponent },
   > { path: '**', redirectTo: '/not-found'}
   > ```  

   \endgroup

# IV) Les guards

Ce sont des services pour vérifier qu'un utilisateur a le droit d'accéder à un lien.

   1. On crée son service, et son `export class` implémentera \fonction{CanActivate} depuis `@angular/router`.

   \code

   > ```ts
   > import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from "@angular/router";
   > import {Observable} from "rxjs";
   > import {Injectable} from "@angular/core";
   > import {AuthService} from "./auth.service";
   > 
   > @Injectable()
   > export class AuthGuardService implements CanActivate {
   >   constructor(private authService: AuthService, private router: Router) {}
   > 
   >   /** Déterminer si une route peut être choisie */
   >   canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
   >     Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
   > 
   >     // Si l'utilisateur est authentifié, on le laisse passer.
   >     if (this.authService.isAuth) {
   >       return true;
   >     }
   >     else {
   >       this.router.navigate(['/auth'])
   >       return false;
   >     }
   >   }
   > }
   > ```

   \endgroup

   2. On protège avec les routes voulues, dans `app.module.ts`

   \code

   > ```ts
   > { path: 'appareils', canActivate: [AuthGuardService], component: AppareilViewComponent },
   > { path: 'appareils/:id', canActivate: [AuthGuardService], component: SingleAppareilComponent },
   > ```

   \endgroup

   En oubliant pas de rajouter `AuthGuardService` à la liste des providers.

