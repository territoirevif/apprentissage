---
title: "g04. Les instructions de structuration"
subtitle: "et les boucles conditionnelles"
author: Marc Le Bihan
---

### a) ngIf

Affiche ou non un composant selon une condition.

   \code

   > ```html
   > <!-- Afficher un carré rouge si l'appareil est éteint -->
   > <div style="width: 20px; height: 20px; background-color: red;"
   >     *ngIf="appareilStatus === 'éteint'">
   > </div>
   > ```

   \endgroup

### b) ngFor

   \code

   > ```html
   > <app-appareil *ngFor="let appareil of appareils"
   >    [appareilName]="appareil.name"
   >    [appareilStatus]="appareil.status"></app-appareil>
   > ```

   \endgroup

\bigbreak

où `appareils` est par exemple un \fonction{array} :

   \code

   > ```json
   > appareils = [
   >   {name: 'Machine à laver',  status: 'éteint'}, 
   >   {name: 'Télévision', status: 'allumé'}
   > ]
   > ```

   \endgroup

\bigbreak

La condition peut être aussi :

   une méthode renvoyant un booléen  
      \demitab ou une méthode renvoyant un autre objet,  
      \demitab mais qui s'il vaut `null`,  sera assimilé à _false_.

