---
title: "g05. Les services"
subtitle: ""
author: Marc Le Bihan
---

## Niveau d'injection 

Un service injecté dans :

- `app.module.ts`, sera disponible dans tous les composants de l'application ET tous les autres services qui pourraient être créés  
- `app.component.ts`, sera disponible pour tous les components, mais pas pour les autres services  
- dans un component spécifique, et alors il ne sera utilisable que de lui. Là, il y aura autant d'instances de ce service que d'instances de ce component  

## Mise à disposition d'un service dans un composant

### Déclaration du service dans NgModule

\code

> ```ts
> import { AppareilService } from "./services/appareil.service";
> 
> @NgModule({
>   ...
>   providers: [AppareilService],
>   ...
> })
> 
> ```

\endgroup

### Utilisation

Dans le composant qui va utiliser ce service, on le fait passer dans son constructeur en paramètre privé.

\code

> ```ts
> constructor(private appareilService: AppareilService) {
>    ...
> }
> ```

\endgroup

