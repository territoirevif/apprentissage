---
header-includes:
title: "02. Les composants Angular (components)"
subtitle: ""
author: Marc Le Bihan
keywords:
- typescript
- component
- composant
- AppComponent
- selector
- templateUrl
- styleUrls
---

### TypeScript

Initialisation d'un tableau vide :

   \code

   > ```ts
   > appareils: any[] = [];
   > ```

   \endgroup

### Component

\fonction{AppComponent} est le composant principal, tous les autres sont des sous-composants.

   \code

   > ```ts
   > @Component({
   >   selector: 'app-root',                # Dit la balise qui le déclenche
   >   templateUrl: './app.component.html', # Sa page HTML associée
   >   styleUrls: ['./app.component.scss']  # Son style CSS
   > })
   > ```

   \endgroup

