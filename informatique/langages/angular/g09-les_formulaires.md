---
title: "g09. Conception de formulaires avec Angular"
subtitle: ""

abstract: | 
  Avec une balise form, des champs identifiés et une fonction ngModel de soumission  
  ou d'une manière réactive

keywords:
   - form
   - ngForm
   - name
   - submit, onSubmit
   - ngModel
   - route `app.module.ts`
   - validation simple (required, disabled)
   - émission et souscription aux évènements

author: Marc Le Bihan
---

---

Angular propose deux manières de créer des formulaires :

# I) Template : repéré par un form

## 1) Le composant

### a) Partie HTML

   1. Identifier les champs cibles par un \fonction{name} et un attribut \fonction{ngModel}
   2. Placer un bouton de \fonction{type="submit"}  
   3. Dans la balise `<form...>`, déclarer une fonction par `#` de type \fonction{ngForm}  
     exemple : \methode{\#f}{="ngForm"},  
     et annoncer sa soumission par \methode{(ngSubmit)}{="onSubmit(f)"}

   \code
  
   \underline{edit-appareil.component.html :} \nopagebreak
 
   > ```html
   > <div class="row">
   >   <div class="col-sm-8 col-sm-offset-2">
   >     <form (ngSubmit)="onSubmit(f)" #f="ngForm">
   >       <div class="form-group">
   >         <label for="name">Nom de l'appareil</label>
   >         <input type="text" id ='name' class="form-control" name="name" ngModel>
   >       </div>
   > 
   >       <div class="form-group">
   >         <label for="status">Etat de l'appareil</label>
   > 
   >         <select id="status" class="form-control" name="status" ngModel>
   >           <option value="allumé">Allumé</option>
   >           <option value="éteint">Eteint</option>
   >         </select>
   >       </div>
   > 
   >       <button type="submit" class="btn-primary">Enregistrer</button>
   >     </form>
   >   </div>
   > </div>
   > ```
   
   \endgroup

### b) Composant lié

   1. On crée une méthode \methode{onSubmit}{(form:\ NgForm)}

\bigbreak

   \code

   \underline{edit-appareil.component.ts :} \nopagebreak
 
   > ```ts
   > import { Component, OnInit } from '@angular/core';
   > import {NgForm} from "@angular/forms";
   > 
   > @Component({
   >   selector: 'app-edit-appareil',
   >   templateUrl: './edit-appareil.component.html',
   >   styleUrls: ['./edit-appareil.component.scss']
   > })
   > export class EditAppareilComponent implements OnInit {
   > 
   >   constructor() { }
   > 
   >   ngOnInit(): void {
   >   }
   > 
   >   /**
   >    * Réaction à la soumission du formulaire
   >    * @param form Formulaire
   >    */
   >   onSubmit(form: NgForm) {
   >     console.log(form)
   >   }
   > }
   > ```

   \endgroup

\bigbreak

   2. Dans \fonction{app.module.ts} on ajoute une route pour aller vers ce composant, et dans \fonction{app.component.html} on rajoute un lien pour s'y rendre :

\bigbreak

   \code

   \underline{app.module.ts :} \nopagebreak

   > ```bash
   > { path: 'edit', canActivate: [AuthGuardService], component: EditAppareilComponent }
   > ```

\bigbreak

   \underline{app.component.html :} \nopagebreak

   > ```html
   > <li routerLinkActive="active"><a routerLink="edit">Nouvel appareil</a></li>
   > ```

  \endgroup

## 2) Validation basique du formulaire

1. Placer :

   - des mots-clefs de validation \fonction{required} sur les champs html,  
   - empêcher la validation du formulaire en se basant sur \fonction{\#f} la variable que l'on a définie pour s'auto-désigner, en plaçant une condition \methode{[disabled]}{="f.invalid"} sur le bouton de soumission :

   \code

   > ```html
   > <input type="text" id ='name' class="form-control" name="name" ngModel required>
   > ...
   > <button type="submit" class="btn-primary" [disabled]="f.invalid">Enregistrer</button>
   > ```

   \endgroup

\bigbreak

2. On en profite pour initialiser par défaut le sélecteur de statut,

   - en plaçant une variable membre `defaultOnOff` de type `string` dans le typescript du composant, 
   - et en le liant par \fonction{[ngModel]} dans l'html :

   \code

   \underline{edit.appareil.component.ts :} \nopagebreak

   > ```ts
   > /** Statut par défaut de l'appareil. */
   > defaultOnOff: string = 'éteint';
   > ```

   \underline{app.component.html :} \nopagebreak

   > ```html
   > <select id="status" class="form-control" name="status" [ngModel]="defaultOnOff">
   > ```

   \endgroup

## 3) Création d'un nouvel élément

   - Créer dans le service \fonction{appareilService} une méthode  
\methode{addAppareil}{(name:string, status:string)},  
qui va ajouter l'appareil à la liste de ceux connus.

   \code

   \underline{edit-appareil.component.ts :} \nopagebreak

   > ```ts
   > /**
   >  * Ajouter un appareil.
   >  * @param name
   >  * @param status
   >  */
   > addAppareil(name:string, status:string): void {
   >   const appareilObject = {
   >     id: 0,
   >     name: "",
   >     status: ""
   >   }
   >
   >   appareilObject.id = this.appareils[this.appareils.length - 1].id + 1;
   >   appareilObject.name = name;
   >   appareilObject.status = status;
   >
   >   // Ajouter à la liste et notifier du changement les écoutants.
   >   this.appareils.push(appareilObject);
   >   this.emitAppareilSubject();
   > }
   > ```

   \endgroup

\bigbreak

   - et l'utiliser dans le composant d'ajout d'un nouvel appareil, en le faisant ensuite revenir à la page initiale.

   \code

   > ```ts
   > /**
   >  * Réaction à la soumission du formulaire
   >  * @param form Formulaire
   >  */
   > onSubmit(form: NgForm) {
   >   const name:string = form.value['name'];
   >   const status:string = form.value['status'];
   >
   >   this.appareilService.addAppareil(name, status);
   >   this.router.navigate(['/appareils']);
   > }
   > ```

   \endgroup

# II) Méthode réactive

## 1) Création d'un model

   \code

   \underline{User.model.ts :} \nopagebreak


   > ```ts
   > /**
   >  * Profil utilisateur
   >  */
   > export class User {
   >   constructor(public firstName: string, public lastName: string, public email: string,
   >        public drinkPreference: string, public hobbies?: string[]) {
   > 
   >   }
   > }
   > ```

   \endgroup

\bigbreak

\underline{Remarque :} placer \fonction{public} au côté des arguments du constructeur provoque l'assignation immédiate. 

Comme si l'on avait écrit : `this.firstName = firstName`.

## 2) Création d'un service avec souscription

   \code

   \underline{user.service.ts :} \nopagebreak

   > ```ts
   > /**
   >  * Gestion des utilisateurs.
   >  */
   > export class UserService {
   >   private users: User[] = [];
   >   userSubject:Subject<User[]> = new Subject<User[]>();
   > 
   >   /**
   >    * Emettre une copie de la liste des utilisateurs.
   >    */
   >   emitUsers(): void {
   >     this.userSubject.next(this.users.slice());
   >   }
   > 
   >   /**
   >    * Ajoute un nouvel utilisateur, et l'émet.
   >    * @param user Utilisateur.
   >    */
   >   addUser(user: User): void {
   >     this.users.push(user);
   >     this.emitUsers();
   >   }
   > }
   ```

   \endgroup

## 3) Un composant qui va émettre des évènements

### a) Typescript

   \code

   \underline{user-list.component.ts :} \nopagebreak

   > ```ts
   > @Component({
   >   selector: 'app-user-list',
   >   templateUrl: './user-list.component.html',
   >   styleUrls: ['./user-list.component.scss']
   > })
   > export class UserListComponent implements OnInit, OnDestroy {
   >   /** Liste d'utilisateurs affichée. */
   >   users: User[];
   > 
   >   /** Souscription aux évènements. */
   >   userSubscription: Subscription;
   > 
   >   constructor(private userService: UserService) {
   >     this.users = [];
   >     this.userSubscription = Subscription.EMPTY;
   >   }
   > 
   >   /**
   >    * Réaction à l'initialisation du composant
   >    */
   >   ngOnInit(): void {
   >     // Souscrire aux évènements utilisateur.
   >     this.userSubscription = this.userService.userSubject.subscribe((users: User[]) => {
   >         this.users = users;
   >       }
   >     );
   > 
   >     // Emettre nos propres utilisateurs.
   >     this.userService.emitUsers();
   >   }
   > 
   >   /**
   >    * Réaction à la destruction du composant.
   >    */
   >   ngOnDestroy() {
   >     // Annuler la souscription aux évènments utilisateur.
   >     this.userSubscription.unsubscribe();
   >   }
   > }
   ```

   \endgroup

### b) La partie HTML du composant

   \code

   \underline{user-list.component.html :} \nopagebreak

   > ```html
   > <ul class="list-group">
   >   <li class="list-group-item" *ngFor="let user of users">
   >     <h3>{{ user.firstName }} {{ user.lastName }}</h3>
   >     <p>{{ user.email }}</p>
   >     <p>Cette personne préfère le {{ user.drinkPreference }}</p>
   > 
   >     <p *ngIf="user.hobbies && user.hobbies.length > 0">
   >       Cette personne a des hobbies :
   >       <span *ngFor="let hobby of user.hobbies">{{ hobby }} - </span>
   >     </p>
   >   </li>
   > </ul>
   > ```

   \endgroup 
