---
title: "n01. Initialiser un projet Angular"
subtitle: "Installer nodeJs, npm, Angular, bootstrap"
author: Marc Le Bihan
keywords:
- angular
- ng new
- npm install bootstrap
- package.json
- angular.json
- bootstrap
---

## 0) Installer `nodeJs` et `npm` et Angular de la version souhaité

\underline{Remarque, avant de débuter :}

Il est possible que sur son environnement un simple `apt-get install nodejs npm`  
conduise à un `sudo npm install -g @angular/cli` qui ne fonctionnera pas.

Il dira sa version `npm 18.0` trop ancienne, par exemple.

Alors, il vaut mieux lancer un setup de la version voulue, avant.

### a) Installation initale de `nodeJs` et `npm`

   \code

   > ```bash
   > curl -fsSL https://deb.nodesource.com/setup_22.x -o nodesource_setup.sh
   > sudo -E bash nodesource_setup.sh
   > sudo apt-get install -y nodejs
   > sudo npm install -g @angular/cli@18
   > 
   > # Ici, je suis l'upgrade proposé de npm
   > npm install -g npm@11.1.0
   > sudo npm install -g npm@11.1.0
   > ```

   \endgroup

### b) Mettre à jour une version existante de nodeJs

   \code
      
   > ```bash
   > npm cache clean -f
   > sudo npm install -g n
   > 
   > # Choisir la version voulue
   > sudo n lts # alternative 1
   > sudo n     # alternative 2
   > ```
      
   \endgroup

## 1) Débuter un nouveau projet Angular

   \code

   > ```bash
   > ng new mon-projet-angular --style=scss --skip-tests=true
   > ```

   \endgroup

## 2) Générer des environnements

Ceux types : développement et production

   \code

   > ``` bash
   > ng generate environments
   > ```

   \endgroup

La commande produit les fichiers :

   - `src/environments/environment.development.ts`
   - `src/environments/environment.ts`

## 3) Prendre en charge bootstrap

   1. Dans la cible, exécuter :
   
      \code

      > ```bash
      > cd mon-projet-angular
      >  
      > npm install bootstrap@5.3.3 --save
      > # --save ajoute bootstrap au fichier package.json du projet
      >  ```

      \endgroup
  
\bigbreak
 
   2. éditer le fichier \fonction{angular.json} (ou `angular-cli.json`) du projet et ajouter une ligne bootstrap dans les styles
   
      \code
 
      > ```json
      > "styles": [
      >    "./node_modules/bootstrap/dist/css/bootstrap.css",
      >    "src/styles.scss"
      > ]
      > ```

      \endgroup

\bigbreak

   3. Dans le fichier \fonction{index.html} de `src/app`, placer un \fonction{<link>} vers le bootstrap de la version voulue

      \code

      > ```bash
      > <!doctype html>
      > <html lang="en">
      > <head>
      >   <meta charset="utf-8">
      >   <title>Test01</title>
      >   <base href="/">
      >   <meta name="viewport" content="width=device-width, initial-scale=1">
      >   <link rel="icon" type="image/x-icon" href="favicon.ico">
      >   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
      > </head>
      > <body>
      >   <app-root></app-root>
      > </body>
      > </html>
      > ```
      
      \endgroup

## 4) Permettre le test par `ng test` d'une application Angular

Il peut être réclamé que _Chrome_ soit installé. Dans ce cas :

   \code

   > ```bash
   > curl -fSsL https://dl.google.com/linux/linux_signing_key.pub | sudo gpg --dearmor | sudo tee /usr/share/keyrings/google-chrome.gpg >> /dev/null
   > echo deb [arch=amd64 signed-by=/usr/share/keyrings/google-chrome.gpg] http://dl.google.com/linux/chrome/deb/ stable main | sudo tee /etc/apt/sources.list.d/google-chrome.list
   > sudo apt-get update
   > sudo apt install google-chrome-stable
   > ```

   \endgroup

Un `ng test` sera alors possible.

## 5) Démarrer une application Angular (sur le port __4200__)
 
   \code

   > ```bash
   > ng serve
   > ```

   \endgroup

## 6) Construire une application Angular en production

Le pendant de `ng serve` pour une livraison en production est :

   \code

   > ```bash
   > ng build
   > ```

   \endgroup

qui va produire un sous-répertoire \fonction{dist} contenant le livrable.

