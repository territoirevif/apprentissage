---
header-includes:
title: "g03. Les types de liaisons"
subtitle: ""
author: Marc Le Bihan
keywords:
- angular
- afficher contenu variable membre
- lier un attribut à une méthode
- event binding
- émission d'évènement
- two ways binding
- réaction à un évènement
- FormsModule
- \@Input
- transmission de données entre composants
---

## 1) {{ ... }} afficher le contenu d'une variable membre

\definition{\{\{ ... \}\}} affiche, sans permettre de le modifier, le contenu d'une variable membre.

   \code

   > ```html
   > <h4>Appareil : {{ appareilName }} -- Statut : {{ getStatus() }}</h4>
   > ```

   \endgroup

   \code

   > ```ts
   > export class AppareilComponent implements OnInit {
   >    /* [...] */
   >    appareilStatus:string = "éteint";
   >
   >    getStatus() : string {
   >      return this.appareilStatus;
   >    }
   > }
   > ```

   \endgroup

## 2) [] property binding : lier un attribut/propriété à une méthode ou variable membre
 
Permet d'afficher des données, mais sans pouvoir les modifier.  
La valeur __entre crochets []__ désigne la propriété à laquelle l'on se lie.

   \code

   > ```html
   > <!-- Le bouton Tout allumer ne sera utilisable qu'authentifié -->
   > <button class="btn btn-success" [disabled]="!isAuth">Tout allumer</button>
   > ```

   \endgroup

   \code

   > ```ts
   > export class AppComponent {
   >    isAuth:boolean = false;
   > }
   > ```

   \endgroup

## 3) () event binding : émission d'évènement vers les contrôleur

Permet d'émettre des évènements vers le code typescript du contrôleur.  
La valeur __entre parenthèses ()__ désigne l'évènement auquel l'on se lie.

   \code

   > ```html
   > <button class="btn btn-success" 
   >    (click)="onAllumer()"
   > >Tout allumer</button>
   > ```

   \endgroup

   \code

   > ```ts
   > onAllumer() {
   >   console.log("On allume tout");
   > }
   > ```

   \endgroup

## 4) [()] two ways binding : émission ou réaction à un évènement

La valeur __entre crochet et parenthèses [()]__ désigne la propriété, qui est aussi source et destinataire d'évènements, à laquelle l'on se lie.  
Dans l'exemple ci-dessous, c'est `ngModel`, `appareilName`.

   - Il faut importer `FormsModule` depuis `@angular/forms` et placer `FormsModule` dans la section des imports.

   \code

   > ```html
   > <li class="list-group-item">
   >    <h4>Appareil : {{ appareilName }} -- Statut : {{ getStatus() }}</h4>
   >    <input type="text" class="form-control" [(ngModel)]="appareilName">
   > </li>
   > ```

   \endgroup

   À l'exécution, modifier la zone de saisie d'un appareil changera aussi ce qu'il y a dans le texte _Appareil :_ de sa balise `<h4>`.

## 5) @Input : transmission de données entre composants

\fonction{@Input} (importé depuis `@angular/core`) permet d'intialiser une variable membre du composant en lui désignant un attribut de même nom dans la balise HTML du composant.  
On le place a côté de la déclaration de la variable.

- Dans le component :

   \code

   > ```ts
   > /* Nom de l'appareil */
   > @Input() appareilName:string
   >
   > constructor() {
   >    this.appareilName = "";
   > }
   > ```

   \endgroup

- Dans l'html du composant parent :

   \code

   > ```html
   > <app-appareil appareilName="Machine à laver"></app-appareil>
   > <app-appareil appareilName="Télévision"></app-appareil>
   > <app-appareil appareilName="Ordinateur"></app-appareil>
   > ```

   \endgroup

   ou, pour aller rechercher ce nom dans une autre variable pour l'y transporter :  

   \code

   > ```ts
   > export class AppComponent {
   >    isAuth:boolean = false;
   >    appareilOne = 'Machine à laver';
   >    appareilTwo = 'Télévision';
   >    appareilThree = 'Ordinateur';
   > ```

   \endgroup

   \code

   > ```html
   > <ul class="list-group">
   >    <app-appareil [appareilName]="appareilOne"></app-appareil>
   >    <app-appareil [appareilName]="appareilTwo"></app-appareil>
   >    <app-appareil [appareilName]="appareilThree"></app-appareil>
   > </ul>
   > ```

   \endgroup

\underline{Remarque:} Pour passer le contenu d'une chaîne de caractères plutôt qu'une variable en argument, il faut encadrer cette chaîne par des apostrophes `'` dans les guillements :

   \code

   > ```html
   > <app-appareil [appareilName]="appareilOne" [appareilStatus]="'éteint'"></app-appareil>
   > ```

   \endgroup

## 6) Les directives par attribut

Pour modifier une balise à partir du contenu d'une méthode du document. Exemple avec \fonction{ngStyle} :

   \code

   > ```html
   > <h4 [ngStyle]="{color: getColor()}">Appareil : {{ appareilName }} -- Statut : {{ getStatus() }}</h4>
   > ```

   \endgroup

   \code

   > ```ts
   > /*
   >  * Renvoyer une couleur d'après le statut d'un appareil.
   >  * @returns green si allumé, red sinon.
   >  */
   > getColor(): string {
   >    if (this.appareilStatus === 'allumé') {
   >       return "green";
   >    }
   >    else {
   >      return "red";
   >    }
   > }
   > ```

   \endgroup

Un autre emploi, avec `[ngClass]` pour changer un fond de ligne `<li>` :

   \code

   > ```html
   > <li [ngClass]="{
   >    'list-group-item': true,
   >    'list-group-item-success': this.appareilStatus === 'allumé',
   >    'list-group-item-danger': this.appareilStatus === 'éteint'}">
   > ```

   \endgroup

## Liaison html - Component

   - {{ `variable_membre` }} : affichage du contenu d'une variable membre.
   
   - `@Input() variable_membre` dans un composant permet son initialisation par `<app-composant variable_membre = "xyz" />`
   
   - [disabled] = `variable_membre` : liaison d'une propriété d'un tag html à une variable membre.
   - (click) = `méthode_membre` : réaction à un évènement.
   - [(ngModel)] = `appareilName` : réaction et émission d'évènements (dans les deux sens).

