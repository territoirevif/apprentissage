---
title: "Intelligence Artificielle  \n02. Résolution de problèmes par exploration"
subtitle: ""

subject: "Comment les agents de résolution de problèmes appréhendent leurs explorations, à la recherche de solutions"

abstract: |
   Un agent de résolution de problèmes voit ses noeuds en boites noires avec un état.  
   Il formule son but, son problème, en fait un modèle qu'il simule et choisit la solution qu'il applique.
category: intelligence artificielle
author: Marc Le Bihan
keywords:
  - formulation de but
  - formualtion de problème
  - simulation de modèle
  - exécution du modèle choisi
---

---

Les agents de résolution de problèmes utilisent des \definition{représentations atomiques} :

   - le monde est en cases ou en noeuds,
   - chaque case ou noeuds est une __boite noire__ :  
     elle a seulement un \definition{état} : __A__, __B__, __C__.

### Principe

   1. Formulation du but : ce que je veux faire

   2. Formulation du problème : les états et les actions nécessaires pour y parvenir  
      Déterminer ce qui va changer, en fonction d'une action  
      Tout cela crée le \definition{modèle}

   3. Exploration : avant d'agir réellement, on \definition{simule} le modèle.  
      on éjecte les cas qui ne passent pas.

   4. On exécute réellement les actions du modèle choisi.

\bigbreak

Dans ce mode de fonctionnement, l'agent peut \underline{ignorer totalement les percepts} :  
il est en \definition{boucle ouverte}.

