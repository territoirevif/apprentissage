---
title: "i01. L'Intelligence Artificielle, qu'est-ce que c'est ?"
subtitle: ""
category: intelligence artificielle
author: Marc Le Bihan
---

# I) S'approcher des humains

## 1) Capacités d'un humain

   \begingroup \cadreGris

   > \definition{Test de Turing :} pour agir comme un humain, un ordinateur pourrait devoir :
   >  
   > - comprendre le language naturel (communiquer avec les humains)
   > - se représenter des connaissances, pour retenir ce qu'il sait ou entend
   > - raisonner automatiquement, pour conclure sur ce qu'il sait, ou répondre aux questions
   > - apprendre, pour s'adapter à de nouveaux contexte, détecter et extrapoler des schémas
   
   \endgroup

encore lui faudrait-il :

   - une vision (reconaissance des objets), 
   - la parole,
   - la capacité robotique de se déplacer et de manipuler des objets
   - l'introspection : capacité à saisir ses pensées à mesure qu'elles passent
   - l'observation du comportement d'autres personnes

Ce n'est pas parce qu'un ordinateur exécute bien une tâche qu'il est un bon modèle du fonctionnement de l'esprit humain.

## 2) Rationnel ? Raisonnement, comportement...

   \begingroup \cadreAvantage

   > Une intelligence artificielle devrait agir rationellement,  
   > c'est à dire : faire les bons choix...  
   > \    
   > \textbf{Mais quels sont ceux-ci ?}

   \endgroup

Humain ou rationnel ?  
Raisonnement ou comportement ?

### Agir rationnellement

La capacité à élaborer des inférences valides : déduire logiquement, par exemple, qu'une manière d'agir est meilleure qu'une autre.

Mais si la logique est sûre, elle \attention{réclame un monde certain} : une condition rarement atteinte.
 
Et agir ainsi, s'oppose à aux actions non inférentielles, comme retirer sa main d'un poële brûlant, qui sont tout aussi valide et plus efficaces dans certaines situations.

Pour évoluer dans un monde complexe, nous avons besoin :

   - de faire des phrases compréhensibles en langage naturel,
   - d'apprendre, pour réagir à des contextes nouveaux.

les __probabilités__ comblent ce problème en permettant des raisonnements certains sur des informations incertaines.

et ainsi, l'on est arrivés à la notion d'__intelligences artificielles qui font le bon choix__, qu'on appelle aussi \definition{modèle standard} :

   - un contrôleur minimise une fonction de coût (ou maximise une somme de réccompenses)
   - très pratiqué en Recheche Opérationnelle.

mais il faut l'affiner dans les environnements complexe, et la __rationalité limitée__ est tout ce que l'on peut atteindre dans certains d'entre-eux, ou quand les calculs sont trop élevés.

### Les limites du modèle standard

De plus, cela ne fait ni adopter un comportement intelligent, et encore moins s'__aligner sur des valeurs !__  :  
\tab $\rightarrow$ une IA pourrait choisir une manière / voie brutale (tricher, agresser) d'arriver à ses fins, si seul compte l'objectif !

Le modèle standard implique de fournir à la machine un objectif entièrement spécifié. C'est possible pour un jeu d'échecs, ou un calcul de plus court chemin, mais pas pour de la conduite automobile où il faut faire un compromis entre conduire et un toujours potientiel risque d'accident.

   - dans quelle mesure la voiture peut-elle faire des choix qui gêneraient les autres conducteurs ?
   - comment doit-elle gérer son accélération, freinage, direction, pour ne pas secouer le passager ?

Faire concorder des préférences réelles, humaines, avec les objectifs de la machine s'appelle \definition{l'alignement des valeurs}.

Et si l'on peut, dans un laboratoire, rebooter, réinitialiser, corriger l'objectif et essayer de nouveau,  
dans le monde réel, ce n'est plus possible.

\tab En fait, il est impossible d'imaginer tous les abus qu'une machine pourrait commettre si elle suivait le modèle standard.

\tab Il lui faut de \danger{l'incertitude}, ce qui l'incite à la prudence puisqu'elle ne connaît pas l'objectif exact, elle "_demande_" la permission, à utiliser l'observation, à s'en remettre au contrôle humain, etc.

Il faut des agents au \definition{bénéfice démontré}.

# II) Fondements de l'Intelligence Artificielle

## 1) Philosophie

   - Peut-on utiliser des règles pour tirer des conclusions valides ?

      > La raison n'est rien d'autre que du calcul ?  
      > Mais alors comment vient le libre arbitre ?  

   - D'où la connaissance provient-elle ?

      > Observations, empirisme.

   - Comment la connaissance conduit-elle à l'action ?

      > Important, car ce n'est qu'en comprennant comment des actions sont justifiées qu'on peut découvrir comment créer un agent dont les actions sont justifiables. 

Penser uniquement en termes d'actions qui réalisent des objectifs est utile, mais parfois inapplicable.

   - Par exemple, s'il y a plusieurs manière d'atteindre un objectif, il faut opérer un choix.
   - Quand on est pas certain d'atteindre un objectif, mais qu'il faut pourtant agir.

      > Maximiser la valeur monétaire attendue d'un résultat  
      > Notion générale d'__utilité__ pour dire la valeur interne et subjective d'un résultat.

   - Les bons choix sont-ils édictés par des règles ? "_Ne pas mentir_", "_Ne pas tuer_" ?

      > Elles peuvent faire des procédures de décision efficaces.

## 2) Mathématiques

Les mathématiques qui furent indispensable pour permettre le développement de l'Intelligence Artificielle.

   - La __logique du premier ordre__ (calcul des prédicats) est issue de celle propositionnelle et celle de _Boole_.
   - La __théorie des probabilités__, quand les informations sont incertaines

      > Pour prédire, après des évènements,  
      > Pour pallier à des mesures incomplètes

   - Les __statistiques__ ont émergé avec les idées de

      > Plan d'expérience  
      > Analyse de données

   - Les __algorithmes__ ont aidé à formaliser les raisonnements mathématiques sous forme de déductions logiques.

   - Apparition du __théorème d'incomplétude__ :

      > Il existe des énoncés qui n'ont pas de preuves dans la théorie elle-même,  
      >
      > ou dit autrement : il existe des fonctions sur des entiers qui n'ont pas d'algorithme, qui ne peuvent être calculées.
    
      - Par exemple, Aucun algorithme ne peut dire _en général_, si un programme retournera une réponse pour une entrée donnée ou s'il risque de continuer sans jamais s'arrêter.

   - La __practicabililté__ (_tractability_) est essentielle : un problème est dit impraticable si le temps qu'il faut pour le résoudre croît exponentiellement avec la taille de ses exemples,

     Le théorème de la __NP-complétude__ donne une méthode pour la calculer, et un problème NP-complet est dit impraticable.
 
        > $\to$ l'enthousiasme pour les "_supercerveaux électroniques_" s'en est trouvé douché, le monde étant une instance de taille extrême.

   - Quelles sont les règles formelles qui permettent de tirer des conclusions valides ?
   - Que peut-on calculer ?
   - Comment raisonne t-on à partir d'informations incertaines ?

## 3) Economie

\tab L'économie, ce n'est pas seulement réfléchir sur l'argent, mais aussi : les __souhaits et les préférences__. Les gens sont enclins à choisir des solutions suffisamment bonnes, même si elles ne sont pas optimales.

La __théorie de la décision__ donne des clefs dans un environnement incertain décrit de manière probabiliste.

La __théorie des jeux__, dit même qu'un agent doit __paraître__ avoir un comportement aléatoire pour gagner. Mais cette théorie n'a pas de méthode certaine pour sélectionner des actions.

Comment prendre des décisions rationnelles quand les perspectives de gains sont éloignées et dépendent d'actions réalisées en séquence ? Peu y ont répondu, mais la __Recherche Opérationelle__ (dont les _processus de Markov_) et l'apprentissage par renforcement y aident.

\bigbreak

   - Comment prendre des décisions en accord avec nos préférences ?
   - Comment faire quand les autres risquent de ne pas coopérer ?
   - Comment parvenir, quand les gains semblent être éloignés dans le futur ?

## 4) Neurosciences

Un simple ensemble de __neurones__ peut engendrer la pensée.

Les __interfaces neuronales directes__ avec un dispositif extérieur peuvent restaurer des fonctions de personnes handicapées.

Un ordinateur est un million de fois plus rapide qu'un cerveau humain, mais celui-ci compense avec plus de stockage et d'interconnexions.

Même la force de calcul d'un ordinateur rapide (la _singularité_ de la science-fiction où l'ordinateur devient plus fort que l'homme) ne fait pas tout.

   > Sans la bonne théorie, un ordinateur plus rapide ne fait que vous donner plus rapidement la mauvaise réponse.

\bigbreak

   - Comment pensent et agissent humains et animaux ?

## 5) Psychologie

Le mouvement _behavioriste_ rejette toute théorie faisant appel à des processus mentaux, en raison de l'impossibilité d'obtenir des résultats fiables au moyen de l'introspection. Il ne faudrait prendre en compte, disent-ils, que :

   - les percepts (stimulis)
   - les actions résultantes (réponses)

Légitimisation des termes _croyance_ et _but_.

Trois grandes étapes d'un agent basé sur les connaissances (_knowledge-based_)

   1. Stimulus, à convertir en représentation interne
   2. Celle-ci est manipulée par des processus cognitifs, pour donner de nouvelles représentations,
   3. et ces dernières sont transformées en actions.

Théorie que l'ordinateur devrait augmenter l'intelligence humaine et non pas automatiser ses tâches.

Intelligence Artificielle et Intelligence Augmentée seraient les deux faces d'une même pièce.

\bigbreak

   - Comment pensent et agissent les hommes et les animaux ?

## 6) Ingénierie Informatique

L'augmentation de la puissance des ordinateur a longtemps suivi la __Loi de Moore__, et dépendait surtout de l'augmentation du nombre des transistors des processeurs.

Elle dépend maintenant de l'augmentation du parallélisme, apporté en accroissant le nombre de CPU dans les ordinateurs, tout comme le cerveau est très parallèle, lui aussi.

Les applications d'Intelligence Artificielle profitent de matériel dédié :

   - processeurs graphiques (GPU)
   - processeurs tensoriels (TPU) : il est prévu pour accélérer les réseaux de neurones
   - wafer scale engine (WSE) \gris{= ?}

Les modèles d'entraînement sont devenus beaucoup plus rapides.

# Calculs

Logique formelle, mathématiques, probabilités participent à la recherche d'algorithmes.

Mais les IA sont rapidement bloquées par tout problème qui avec peu de données se révèle vite __NP-Complet__ ou exponentiel. Elles manquent alors de __practicabilité__ (_tractability_).

Théorie de la décision  
Théorie des jeux

Théorie du contrôle ou de la commande, où un système sait se corriger automatiquement
