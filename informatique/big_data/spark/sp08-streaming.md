---
title: "sp08. Streaming"
subtitle: ""
author: Marc Le Bihan
keywords:
- spark
- stream
- flux
- StreamingContext
- readStream
- writeStream
---

Un \fonction{StreamingContext} s'ajoute au `SparkContext` pour gérer les streams.

\bigbreak

\underline{(Scala) lance un stream en tâche de fond, puis l'interroge :}

  \code

   1. Préparation du stream

   > ```scala
   > val streamingDataFrame = spark.readStream.schema(staticSchema)
   >    .option("maxFilesPerTrigger", 1)
   >    .format("csv").option("header", "true")
   >    .load("./retail-data/by-day/*.csv")
   > ```

   > ```log
   > streamingDataFrame.isStreaming répond true
   > ```

   2. Requête de fenêtrage d'une journée sur les dates d'envoi

   > ```scala
   > val PurchaseByCustomerPerHour = streamingDataFrame.selectExpr("CustomerId", 
   >    "(UnitPrice * Quantity) as total_cost", "InvoiceDate")
   >    .groupBy($"CustomerId", window($"InvoiceDate", "1 day"))
   >    .sum("total_cost")
   > ```

   > ```log 
   > PurchaseByCustomerPerHour: org.apache.spark.sql.DataFrame = [CustomerId: double, window: struct<start: timestamp, end: timestamp> ... 1 more field]
   > ```

   3. Lancement du stream en tâche de fond, en lui donnant le nom "_customer\_purchases_"

   > ```scala
   > PurchaseByCustomerPerHour.writeStream
   >    .format("memory")
   >    .queryName("customer_purchases")
   >    .outputMode("complete")
   >    .start()
   > ```

   4. Interrogation du flux _customer\_purchases_

   > ```scala
   > spark.sql("""                                                                   
   >      |      | SELECT * FROM customer_purchases ORDER BY `sum(total_cost)` DESC
   >      |      | """).show(5)
   > ```

   > ```log
   > 22/08/21 17:11:14 WARN ResolveWriteToStream: Temporary checkpoint location created which is deleted normally when the query didn't fail: /tmp/temporary-b38405f7-8f6e-42ab-9cb1-57c4cb6a685c. If it's required to delete it under any circumstances, please set spark.sql.streaming.forceDeleteTempCheckpointLocation to true. Important to know deleting temp checkpoint folder is best effort.
   > 22/08/21 17:11:14 WARN ResolveWriteToStream: spark.sql.adaptive.enabled is not supported in streaming DataFrames/Datasets and will be disabled.
   > res5: org.apache.spark.sql.streaming.StreamingQuery = org.apache.spark.sql.execution.streaming.StreamingQueryWrapper@45c9d481
   > ```

   > ```log
   > +----------+--------------------+------------------+                            
   > |CustomerId|              window|   sum(total_cost)|
   > +----------+--------------------+------------------+
   > |      null|{2011-03-29 02:00...| 33521.39999999998|
   > |      null|{2010-12-21 01:00...|31347.479999999938|
   > |   18102.0|{2010-12-07 01:00...|          25920.37|
   > |      null|{2010-12-10 01:00...|25399.560000000012|
   > |      null|{2010-12-17 01:00...|25371.769999999768|
   > +----------+--------------------+------------------+
   > ```

