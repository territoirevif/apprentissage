---
title: "sp22. Machine Learning. Choisir un algorithme"
subtitle: "et essayer des [hyper] paramètres"
author: Marc Le Bihan
keywords:
- algorithme
- spark
- machine learning
- classifier
- regressor
- fit
- transform
- données d'apprentissage
- données de test
- features
- label
- modèle
- donnée de prédiction
- randomSplit
- base d'apprentissage
- dataset
- khi-deux
- Kolmogorov-Smirnov
- clustering
- kmeans
- random forest
- arbre de décision
- régression linéaire
- transformer
- estimator
- pipeline
---

C'est un algorithme + des données qui font le modèle,  
qu'il faut ensuite (hyper-)paramétrer. 

On ne sait pas quel algorithme donnera les meilleurs résultats avant de faire des tests. 

On peut même dire qu'une analyse de donnée n'est jamais finie et que ce sont des contraintes de temps, d'argent, etc. qui y mettent un terme...

## 1) Les algorithmes de machine learning, sous Spark

### a) Les algortithmes

Sous _Spark_, les algorithmes sont appelés les __classifiers__  
ou, pour les données numériques, les __regressors__

   - NaiveBayes
   - GBTClassifier
   - LogisticRegression
   - RandomForest

\bigbreak
Dans le package \fonction{org.apache.spark.ml.stat} se trouve :

   - Une liste de tests : _Khi-Deux_, _Kolmogorov-Smirnov_
   - Un choix de clustering : _KMeans_...
   - Un choix de classifications : _Random Forest_, _Arbre de décision_
   - Module de régression : _Régression Linéaire_

_([SRJD] p. 222 à compléter)_

### b) Les modules qui conduisent l'analyse

Il y a quatre modules principaux :

   1. Le \definition{transformer} qui ajoute ou transforme des colonnes (appelées __features__).
   
   2. L'\definition{estimator}
   
      - qui apprend d'un jeu d'apprentissage (dont on extrait des valeurs \fonction{X\_train}, \fonction{Y\_Train}) à l'aide de la fonction {fit} proposée par _Spark_  
        et duquel la fonction \fonction{transform()} qui suivra, va tirer un modèle.  
      - et \fonction{transform}, qui prédit un dataframe au format \fonction{Y\_Train} à partir d'un dataframe au format \fonction{X\_Train}.
   
   3. \fonction{param} contient les paramètres des __estimators__ et des __transformers__ avec deux classes intéressantes :
   
      - \fonction{Vector} pour optimiser les données avec beaucoup de zéros (comme les matrices creuses)
      - \fonction{Params} des dictionnaires clefs-valeurs
   
   4. Le \fonction{pipeline} qui organise le chaînage des __estimators__ et des __transformers__

## 2) Les données d'apprentissage et les données de test

### a) Les deux méthodes fit et transform des algorithmes de Spark

Ces algorithmes ont deux méthodes :

   1. \fonction{fit} : elle prend des données d'apprentissage, et à partir d'elles, crée un \definition{modèle}.  
   2. \fonction{transform} : prend une matrice de même format que celle des données d'apprentissage, mais issue des données de test, et y __ajoute une donnée de prédiction__.  
  Cette donnée de prédiction pourra être jaugée par un objet du module _evaluation_.

\bigbreak

   \code
   
   > ```java
   > donneesApprentissage = /* vecteur X de features + vecteur Y de label, issus de la partie 'apprentissage' du jeu d'essai */
   >
   > donneesTest = /* vecteur X + vecteur Y, issus de la partie 'test' du jeu d'essai */
   >
   > algorithme = RandomForestClassifier(colonnne);
   > modele = algorithme.fit(donneesApprentissage);
   > predictions = modele.transform(donneeTest);
   > ``` 
   
   \endgroup

### b) Comment bien diviser sa base d'apprentissage en un jeu de test et en jeu d'apprentissage ?

Pour faire un jeu de test avec $70\%$, par exemple, de données d'apprentissage, on peut utiliser la méthode \fonction{randomSplit}.

   \code

   > ```java
   > Dataset<Row>[] jeu = datasetApprentissage.randomSplit(new double[]{0.7, 0.3});
   > ```

   \endgroup

où : `jeu[0]` = jeu d'apprentissage  
et : `jeu[1]` = jeu de test

\bigbreak

\danger{Attention à ne pas reprendre d'enregistrements d'apprentissage dans ceux de tests !}

