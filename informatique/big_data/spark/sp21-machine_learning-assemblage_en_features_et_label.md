---
title: "sp21. Machine Learning : l'assemblage en features et label"
subtitle: ""
author: Marc Le Bihan
keywords:
- machine learning
- spark
- individu
- assemblage
- feature
- données d'apprentissage
- base d'apprentissage
- label
- cible
- vecteur
- donnée explicative
- VectorAssembler
- donnée modélisée
---

Une base de données ou liste d'individus a une grande liste d'attributs :

\demitab individu (ou enregistrement) 1 : $x_{1,1}$, $x_{1,2}$, $x_{1,3}$, $z_{1,1}$, $x_{1,4}$, $x_{1,5}$, $z_{1,2}$, $y_{1,1}$, $y_{1,2}$, $z_{1,3}$  
\demitab individu (ou enregistrement) 2 : $x_{2,1}$, $x_{2,2}$, $x_{2,3}$, $z_{2,1}$, $x_{2,4}$, $x_{2,5}$, $z_{2,2}$, $y_{2,1}$, $y_{2,2}$, $z_{2,3}$

## 1) La base d'apprentissage

Chaque enregistrement de la base d'apprentissage porte deux parties :

   \tab $\bleu{\{x_{1}, x_{2}, x_{3}, x_{4}, x_{5}\}} \rightarrow \mauve{\{y_{1}, y_{2}\}}$  

   \tab \bleu{features} $\rightarrow$ \mauve{cibles} (appelé aussi : label)

## 2) Les données explicatives : le vecteur X

Les colonnes utiles $x_{1}$, $x_{2}$, $x_{3}$  à l'algorithme de machine learning, pour s'essayer à trouver la/les cibles, s'appellent _l'ensemble des features_.

mais toutes les colonnes d'un enregistrement  
(ou tous les attributs/caractères d'un individu)  
n'en font pas nécessairement partie : on les choisit, en général. Dans notre exemple, l'on a pas retenu les champs $z$, qui ne servent ni en features, ni en labels.

Les colonnes sélectionnées à cette fin sont les données explicatives, qu'on rassemble dans un premier __vecteur__, __X__.

\bigbreak

Donc, on aura : $features = X = \{x_{1}, x_{2}, x_{3}, x_{4}, x_{5}\}$

### a) VectorAssembler

Sous _Spark_, c'est la classe \fonction{VectorAssembler} qui va rassembler les colonnes en un vecteur de features.

   \code

   > ```java
   > VectorAssembler assembler = new VectorAssembler();
   > assembler.setInputCols(new String[]{"x1", "x2", "x3"});
   > assembler.setOutputCol("features");
   > Dataset<Row> features = assembler.transform(datasetApprentissage);
   > ```

   \endgroup

### b) Retrouver les noms d'origine

\fonction{assembler.getInputCols()} redonnera le nom des colonnes d'origine, si nécessaire.

## 3) La cible : le vecteur Y

On appelle ces cibles le \definition{label}, car en général, il n'y en a qu'un :
C'est la (les) colonne(s) contenant la (les) donnée(s) à modéliser : __Y__

$label = Y = \{y_{1}, y_{2}\}$

\bigbreak

\underline{Attention :}

   1. il ne faut pas que features et label,  
   \demitab c'est à dire données explicatives et modélisées (ou cibles)  
   se recoupent. Un champ ou attribut explicatif placé dans $X$ ne peut pas être aussi dans $Y$.

   2. Ultérieurement, il ne faudra pas que des enregistrements ou individus ayant servi à l'entraînement servent à tester l'algorithme.

