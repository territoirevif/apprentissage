---
title: "sp04. Les fonctions d'agrégation"
subtitle: ""
author: Marc Le Bihan
keywords:
- sql
- python
- count
- avg
- group by
- fonction d'agrégation
- spark
---

Ces fonctions s'enclenchent typiquement, lorsque l'on utilise SQL, à l'apparition du mot-clef \fonction{GROUP BY}.

## 1) Count

\underline{Python} : grouper des données par sexe, puis faire un `count(*)` par valeur de clef

   \code

   > ```python
   > villes.groupBy("sexe").count().collect()
   > ```

   >  ```
   >  [Row(sexe='F', count=524), Row(sexe='H', count=560)]
   >  ```

   \endgroup

## 2) Moyenne (Avg)

\underline{Python :} via SQL

Passer par une exécution SQL convertira le champ `salaire` en nombre, ce qui nous est pratique.

   \code

   > ```python
   > ville.createTempView("ville") # doit avoir été fait pour que sql fonctonne
   > requete_sql = "select sexe, mean(salaire) from ville group by sexe"
   > spark.sql(requete_sql).collect()
   > ```

   ```
   [Row(sexe='F', mean(CAST(salaire AS DOUBLE))=23595.8144011948),
    Row(sexe='H', mean(CAST(salaire AS DOUBLE))=28187.828451177982)]
   ```

   \endgroup

\bigbreak

\underline{Python :} via une fonction d'agrégation

   \code

   > ```python
   > from pyspark.sql.functions import avg
   >
   > ville.groupBy(["sexe"]).agg(avg("salaire")).collect()
   > ```

   ```
   [Row(sexe='F', avg(salaire)=23595.8144011948),
   Row(sexe='H', avg(salaire)=28187.828451177982)]
   ```

   \endgroup

\bigbreak

\underline{Python :} en assurant la conversion par `withColumn`

C'est plus sûr, car les autres méthodes, que savons-nous d'à quel point elles agissent à bon escient ?

   \code

   > ```python
   > from pyspark.sql.types import DoubleType
   > 
   > ville = ville.withColumn("salaire_float", ville.salaire.cast(DoubleType()))
   > ville.groupBy(["sexe"]).mean("salaire_float").collect()
   > ```

   ```
   [Row(sexe='F', avg(salaire_float)=23595.8144011948),
   Row(sexe='H', avg(salaire_float)=28187.828451177982)]
   ```

   \endgroup

