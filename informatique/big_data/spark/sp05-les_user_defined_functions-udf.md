---
title: "sp05. Les User Defined Functions (UDF)"
subtitle: ""
author: Marc Le Bihan
keywords:
- spark
- python
- user defined function (UDF)
---

\underline{Python} :

   \code

   > ```python
   > from pyspark.sql.functions import udf
   > from pyspark.sql.types import *
   > 
   > def categorie(salaire):
   >     """
   >     Revient à la dizaine de millier d'un salaire : 34000 -> 30000
   >     """
   >     nb_de_dizaine_de_millier = float(salaire)//10000
   >     categorie                = 10000 * nb_de_dizaine_de_millier
   >     return  int(categorie)
   > 
   > 
   > ma_fonction = udf(categorie , IntegerType())
   > ville = ville.withColumn("salaire_categorie", ma_fonction("salaire"))
   > ville.show(1, vertical=True)  
   > ```

   \endgroup

