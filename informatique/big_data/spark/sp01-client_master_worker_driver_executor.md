---
title: "sp01. Client, master, worker, driver, executor"
subtitle: "et les jobs, stages, tâches..."
author: Marc Le Bihan
keywords:
- application spark
- client
- master
- driver
- executor
- spark-shell
- spark-submit
- worker
- SparkContext
- scheduler
- DAG
- task slot
- thread
- job
- stage
- action
- cluster
---

# I) La structure d'une application Spark : master, driver, worker 

## 1) Le client

Le \donnee{client} lance une application _Spark_ par

   - \fonction{spark-shell}
   - ou \fonction{spark-submit}
   - ou \fonction{java -jar ...} si l'API _Spark_ est embarquée.

## 2) Le master se crée en créant une JVM nommée driver

   - Le master connaît les ressources disponibles au niveau du cluster
   - il traite les options de déploiement.
   - Il n'y a __qu'un driver__ par application.

Il gère :

   - \definition{SparkContext}, qui :

      - demande des ressources au cluster
      - charge les données
      - crée les `RDD`...

   - La distribution du \mauve{code de l'application Spark} : le `jar`, par `spark-submit`

### a) Le master/driver demande aux workers de créer des JVM executors

Un worker peut avoir plusieurs executors

Par sa fonction \mauve{Scheduler}, le master/driver :

   - construit la logique des tâches
   - les distribue aux executors
   - gère le data locality (caches)
   - en retour, les workers indiquent au master quelles ressources sont disponibles dans leurs noeuds. 

### b) La distribution des tâches par le master/driver aux executors

Le \mauve{driver} distribue aux \mauve{workers} les tâches issues des opérations du DAG,  
et les executors de ces workers recoivent ces tâches, qu'ils exécutent dans des \mauve{tasks slots}.

Ces tasks slots :

   - sont des threads
   - leur nombre peut-être deux à trois fois supérieur au nombre de CPU

Puis, il retourne ses résultats au driver

# II) Job et stages

## 1) Job

Un \definition{job} est terminé par une \mauve{action} : `collect`, `take`, `show`...

## 2) Stage

Un \definition{stage} (ou étape) est créé par une transformation : `select`, `agg`, `filter`...  
\demitab et __toutes les fonctions de tri__ (sauf si l'on utilise \fonction{sortWithinPartitions})  
\demitab car elles provoquent des mélanges (_shuffles_)

\bigbreak

\danger{Le passage d'un stage à un autre nécessite beaucoup de ressources réseau}.

# III) Lancement et suivi d'une application Spark

## 1) Lancement d'une application

   \code

   > ```bash
   > ./bin/spark-submit \
   >    --class org.apache.spark.examples.SparkPi \
   >    --master local \
   >    ./examples/jars/spark-examples_2.12-3.3.0.jar 10
   > ```

   \endgroup

\bigbreak

\underline{Exemple :} l'application suivante charge plusieurs fichiers csv en même temps, puis les place dans une table temporaire, puis en demande la structure

   \code

   > ```scala
   > val staticDataframe = spark.read.format("csv").option("header", "true").option("inferSchema", "true").load("/Spark-The-Definitive-Guide/data/retail-data/by-day/*.csv")
   > 
   > staticDataframe.createOrReplaceTempView("retail_data")
   >
   > val staticSchema = staticDataframe.schema
   > ```

   \endgroup

   \code

   > ```bash
   > staticSchema: org.apache.spark.sql.types.StructType = StructType(StructField(InvoiceNo,StringType,true),StructField(StockCode,StringType,true),StructField(Description,StringType,true),StructField(Quantity,IntegerType,true),StructField(InvoiceDate,TimestampType,true),StructField(UnitPrice,DoubleType,true),StructField(CustomerID,DoubleType,true),StructField(Country,StringType,true))
   > ```

   \endgroup

## 2) Suivi d'une application Spark

   - Les applications ont : 

      - leur page web sur le port __4040__ (web UI)
      - et __49181__ pour l'URL

   - le master :

      - sur le __8080__ (web UI)
      - __7077__ pour l'URL
      - et __6066__ pour l'API REST

   - les workers : 

      - sur le __8081__ (web UI)
      - __45559__ pour l'URL

\bigbreak

Ces numéros de ports s'auto-incrémentent jusqu'à en trouver un de libre.

