---
header-includes:
- \usepackage{tcolorbox}
- \usepackage{fvextra}
- \DefineVerbatimEnvironment{Highlighting}{Verbatim}{breaklines,breakanywhere,breaksymbol=,breakanywheresymbolpre=,commandchars=\\\{\}}

title: "Spark  \n09. Data visualization"
subtitle: "avec Jupyter et Zeppelin"
author: Marc Le Bihan
geometry: margin=2cm
fontsize: 12pt
output: pdf
classoption: fleqn
urlcolor: blue
---

\input{apprentissage-include}

# Jupyter

## Installation

Exécuter ces commandes :

   \begingroup \cadreGris \small
 
   > ```bash
   > pip install jupyterlab
   > pip install notebook
   > pip install voila
   > ```

   \endgroup

\bigbreak

et placer \fonction{~/.local/bin} dans le `PATH`.

## Lancement

### En local

   \begingroup \cadreGris \small

   > ```bash
   > jupyter notebook
   >  ```

   \endgroup

### Livre Spark de Dunod 

Construction de l'image _Docker_ et lancement de _Jupyter_ :

   \begingroup \cadreGris \small

   >  ```bash
   >  cd docker
   >  sudo ./build_docker_image_livre_spark.sh
   >  sudo ./start_container_spark.sh
   >  ```

   \endgroup

Jupyter est accessible sur [http://localhost:10000](http://localhost:10000) avec le mot de passe `dunod`. 

### CreateTempView

Le `villes.createTempView("ville")` crée une table temporaire, utile pour les traitements et représentations graphiques que l'on va faire sous _Jupyter_ (même principe qu'avec _Zeppelin_).

Et ensuite, des requêtes _Spark SQL_ peuvent être jouées dessus :

   \begingroup \cadreGris \small

   >  ```python
   >  spark.sql("SELECT count(*) FROM ville WHERE sexe = 'H'").collect()
   >  ```

   \endgroup
