---
title: "sp07. Partitionnement et performances"
subtitle: ""
author: Marc Le Bihan
keywords:
- spark
- partitionnement
- performance
- cpu
- parallélisation
- vitesse
- shuffle
- graphe
- graphe orienté acyclique (DAG)
- directed acyclic graph (DAG)
---

### Impact du nombre de CPU sur la vitesse d'un traitement parallélisé

Augmenter le nombre de CPUs sur un traitement parallélisé multipliera sa vitesse :

   - jusqu'à 2 fois, avec 2 CPUs
   - entre 2 et 3.75, avec 4 CPUs, selon que le traitement est parallélisé de 50 à 95\%
   - de 2 à 6, avec 8 CPUs
   - de 2 à 9, avec 16 CPUs

\bigbreak

C'est le taux de parallélisation qui va avoir un impact sur la vitesse.

### DAG

- Le nombre de graphes côte à côte, sans shuffle (flèches partant vers un autre graphe) montre le niveau de paraléllisme.

