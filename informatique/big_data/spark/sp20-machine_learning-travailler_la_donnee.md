---
title: "sp20. Machine Learning : travailler la donnée"
subtitle: "déroulé d'une session de machine learning"
author: Marc Le Bihan
keywords:
- machine learning
- spark
- feature engineering
- travail sur la donnée
- data engineering
- champ date
- one hot-encoding
- StringIndexer
- variable nominale
---

Le \definition {Machine Learning} c'est l'ordinateur qui propose :

\demitab à partir de données en entrée   
\tab un __modèle__ qui les explique,  
\tab mais \rouge{en pouvant se tromper ou être imprécis...}

\bigbreak

Lors d'une session de machine learning, il y a des étapes à suivre.  
La première d'entre-elles est le travail sur la donnée : le __feature engineering__.

C'est créer de nouvelles colonnes, préciser leur type, remplir celles vides...

## 1) Pour une date

   - La séparer en champs supplémentaires : année, mois, jour, jour nommé (Dimanche, Lundi...)
   - Calculer un délai par rapport à une autre date (anniversaire, par exemple)
   - Associer une saison
   - Calculer un âge

## 2) Associer des catégories, sous-catégories

À des données comme des produits, par exemple, pour les préciser.

## 3) Le One hot-encoding

Les algorithmes de machine learning \rouge{supportent mal les variables nominales}.

On peut les remplacer par des nombres, mais \rouge{ils risquent d'établir une relation d'ordre entre-eux}, alors qu'il n'y en a pas.

Le One hot-encoding vise à résoudre ce problème, en éclatant une _variable nominale_ en plusieurs colonnes booléennes portant chacune le nom d'une de ses modalités :

   - cette série de colonnes est porteuse d'un __0__ ou __1__ (booléen) si elle est choisie
   - et il n'y a qu'un seul __1__ par variable nominale pour toutes ses modalités.

\bigbreak

Pour cela, on utilise le \fonction{StringIndexer} qui :

   1. transforme ses modalités en nombres : $1, 2, 3, 4...$
   2. puis les encode en $0$ et $1$ en créant autant de colonnes qu'il y a de modalités.

\bigbreak

Ces colonnes supplémentaires ne sont pas ajoutées brutalement au `Dataset`: elles sont rangées dans un champ de type \fonction{vector},  
sparse, puisqu'il est surtout fait de zéros.

\bigbreak

\underline{Exemple : } l'on remplace dees pays _UK_, _France_, _Germany_

par des nombres $1, 2, 3$, puis on les encode en $0$ et en $1$,  
à raison d'un seul $1$ par individu pour cette variable nominale :  

| Individual | UK  | France | Germany |
| ---        | --- | ---- | ---- |
| Marc       |  0   |  1  |  0 |
| Bob        |  1  |  0 | 0 |
| Odile      |  0  |  0 | 1 |

