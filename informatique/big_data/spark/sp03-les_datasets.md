---
title: "sp03 : les datasets Spark et leurs fonctions utiles"
subtitle: ""
category: informatique
author: Marc Le Bihan
keywords:
- spark
- quantiles approximatifs (approxQuantile)
- corrélation (corr)
- covariance (cov)
- table de fréquence pour deux paires de colonnes (crosstab)
- supprimer les enregistrements doublons (dropDuplicates)
- échantillon (sample)
- statistiques univariées (summary)
- remplacer les valeurs nulles avec une valeur (fillna)
- couper un dataset en plusieurs parties (randomSplit)
---

### Les fonctions présentes derrière la méthode `stat()`

- \fonction{approxQuantile} : quantiles approximatifs des colonnes numériques

   \code
   
   >   ```java
   >   // Retourne Q1 et Q3 pour calculer un intervalle interquartile Q3 - Q1.
   >   double[] quantiles = s.stat().approxQuantile("col", new double[] {0.25, 0.75}, 0);
   >   ```
   
   \endgroup

- \fonction{corr} : corrélation de deux colonnes

   \code
   
   >   ```java
   >   // Corrélation entre nombre de compétences exercées et population intercommunale.
   >   double correlation = interco.stat().corr("nombreCpt", "population", "pearson");
   >   ```
   
   \endgroup

- \fonction{cov} : covariance
- \fonction{crosstab} : table de fréquence pour les paires de deux colonnes

### Quelques fonctions utilitaires

   - \fonction{dropDuplicates} : supprime les enregistrements ayant un doublon sur un ensemble de colonnes
   - \fonction{substract} : enlève d'un dataset les enregistrements d'un autre
   - \fonction{exceptAll} : renvoie un dataset qui n'a aucun des enregistrements d'un autre
   - \fonction{sample} : prend un échantillon [basé sur un seed]  
   - \fonction{summary} : statistiques sur des colonnes

   \code
    
   >  ```java
   >  // Toutes les colonnes verront ces valeurs calculées.
   >  // Renvoie un dataset aux lignes : `count`, `mean`, `stddev`, `min`, `max`
   >  Dataset<Row> summary = individus.summary();
   > ```
      
   \endgroup

   - \fonction{replace} : remplace une valeur par une autre (?)
   - \fonction{fillna} : remplace les valeurs nulles de colonnes précisées par une autre valeur
   - \fonction{randomSplit} : coupe aléatoirement un Dataset en plusieurs parties, selon des poids

### Fonctions (du package fonctions)

   - fonctions \fonction{sinus}, \fonction{cosinus}...
   - fonctions de date dans la semaine, heure...
   - \fonction{encode} : change l'encodage d'une colonne
   - \fonction{flatten} : créer un tableau unique à partir d'une collection de tableaux
   - \fonction{lpad}, \fonction{rpad} : padding à droite ou à gauche

