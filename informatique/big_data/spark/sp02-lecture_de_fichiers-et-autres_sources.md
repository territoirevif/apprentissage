---
title: "sp02. La lecture de fichiers et d'autres sources"
subtitle: ""
author: Marc Le Bihan
keywords:
- lecture d'un fichier csv
- fonctions simples du dataframe
- count
- distinct
- show vertical
- createTempView
---

# I) Lecture d'un fichier CSV en Python

Lecture du fichier `ville_1.csv`, avec `show` en vertical :

   \code
   
   > ```python
   > from pyspark.sql import SparkSession
   > spark = SparkSession.builder.getOrCreate()
   >   
   > # Charger les villes
   > path = "./../data/Villes/ville_1.csv"
   > villes = spark.read.format("csv").option("header", "true").load(path)
   >   
   > # Faire un show Spark, mais en mode vertical
   > villes.show(2, vertical=True)
   > ```
 
   \endgroup

\bigbreak

   > L'effet de l'option `vertical=True` de \fonction{show} est intéressant : \nopagebreak

   \code

   > ```
   > -RECORD 0----------------------------------
   >  id                 | 5251                 
   >  vitesse_a_pied     | 0.02                 
   >  vitesse_a_velo     | 0.05                 
   >  home               | (lon:26.60 lat:28... 
   >  travail            | (lon:21.08 lat:14... 
   >  sportif            | False                
   >  casseur            | False                
   >  statut             | reserviste           
   >  salaire            | 29800.610034665042   
   >  sexe               | F                    
   >  age                | 18                   
   >  sportivite         | 0.1                  
   >  velo_perf_minimale | 0.4                  
   > -RECORD 1----------------------------------
   >  id                 | 5252                 
   >  vitesse_a_pied     | 0.14974625830876215  
   >  vitesse_a_velo     | 0.37436564577190534  
   >  home               | (lon:0.26 lat:42.61) 
   >  travail            | (lon:36.35 lat:33... 
   >  sportif            | False                
   >  casseur            | False                
   >  statut             | professeur           
   >  salaire            | 23595.44383981423    
   >  sexe               | F                    
   >  age                | 28                   
   >  sportivite         | 0.7487312915438107   
   >  velo_perf_minimale | 0.4         
   > ```

   \endgroup

\bigbreak

\fonction{villes.dtypes} donne la structure du dataset/dataframe (spécifique _Python_), 

   \code

   ```
   [('id', 'string'),
    ('vitesse_a_pied', 'string'),
    ('vitesse_a_velo', 'string'),
    ('home', 'string'),
    ('travail', 'string'),
    ('sportif', 'string'),
    ('casseur', 'string'),
    ('statut', 'string'),
    ('salaire', 'string'),
    ('sexe', 'string'),
    ('age', 'string'),
    ('sportivite', 'string'),
    ('velo_perf_minimale', 'string')]
   ```

   \endgroup

# II) Fonctions simples du dataframe
 
\fonction{villes.count()} le nombre d'enregistrements dedans,  
\fonction{villes.distinct().count()}, le nombre d'enregistrements uniques... Là, c'est du _Spark_ classique.

Le \fonction{equals()} de _Java_ est un \fonction{==} en _Python_.
 
Un \fonction{where} (ou \fonction{filter}) se fait ainsi :

  \code 

   >  ```python
   >  villes.where(villes.sexe == "H").count()
   >  ```

   \endgroup

\bigbreak

Comme _Spark_ est lazy, un \fonction{ville.groupBy("sexe").count()} n'affichera pas les résultats attendus :  
il faudra lui ajouter un \fonction{collect()} popur cela.

   \code

   > ```python
   > ville.groupBy("sexe").count().collect()
   > ```

   \endgroup

   \code

   ```log
   [Row(sexe='F', count=524), Row(sexe='H', count=560)]
   ```

   \endgroup

\bigbreak

\fonction{createTempView("ville")} va stocker le dataset `ville` dans une table temporaire, pour permettre d'y accéder par SQL.


