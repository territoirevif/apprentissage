---
title: "sp15. Machine Learning : le One Hot-Encoding"
subtitle: "intégré à un circuit de préparation des données"
author: Marc Le Bihan
---

Les algorithmes de machine learning supportent mal les variables nominales  
et il faut les leurs introduire en nombres pour qu'ils puissent en faire quelque-chose.

## 1) Comment convertir des variables nominales (modalités) en nombres ?

### a) Une conversion directe en nombres crée une fausse relation d'ordre !

Si on les remplace "brutalement" par des modalités par des nombres,

   \demitab \rouge{ils risquent d'établir une relation d'ordre entre-eux},  
   \demitab alors qu'il n'y en a pas.

   \tab "_\bleu{Nous allons au cinéma les Vendredi, Samedi, Dimanche}_" $\rightarrow$ $5, 6, 0$  
   \tab d'après les fonctions `getDayOfWeek()` classiques.

   \tab mais si l'on commet l'erreur de faire la moyenne de cette modalité :  
   \tab $\frac{5+6+0}{3} = 3.67$ \demitab qui serait... \rouge{Mercredi... fin d'après-midi ?! C'est absurde !}

### b) La solution du One hot-encoding

Le \definition{One hot-encoding} vise à résoudre ce problème,  
   \demitab en éclatant une _variable nominale_ en plusieurs colonnes booléennes  
   \demitab qui lèveront l'ambiguïté :

   \tab quand on aura __une__ valeur (de modalité "_nominale_")   
   \tab on sera sur __une__ colonne, et pas une autre.

## 2) Le rôle des classes mises en oeuvre

\fonction{StringIndexer} : elle transforme des chaînes de caractères  
   \demitab en nombres (indexs) croissants: $1,2,3,4...$

\bigbreak

\fonction{OneHotEncoder} : un nombre entier peut être \rouge{interprété faussement} comme une relation d'ordre.

   \demitab `OneHotEncoder` répartit ce nombre, qui va de 1 à $\bleu{n}$  
   \demitab  en $\bleu{n}$ nombres "booléens", $0$ ou $1$,  
   \demitab chacun devenant une variable (colonne, champ...) séparée.

\bigbreak

\underline{Exemple : } 

On remplace des pays _UK_, _France_, _Germany_

par des nombres $1, 2, 3$, puis on les encode en $0$ et en $1$,  
à raison d'un seul $1$ par individu pour cette variable nominale :  

| Individual | UK  | France | Germany |
| ---        | --- | ---- | ---- |
| Marc       |  0   |  1  |  0 |
| Bob        |  1  |  0 | 0 |
| Odile      |  0  |  0 | 1 |

\bigbreak

\fonction{VectorAssembler} : rassemble __des__ vecteur\textbf{s}  
   \demitab typiquement, pour en faire une liste de __features__  
   \demitab à soumettre pour analyse.

## 3) Mise en oeuvre d'un One Hot-Encoding

Le \fonction{Pipeline} regroupe cette séquence d'opérations de mise en forme des données  
   \demitab dans un circuit que toute donnée suivra :  
   \demitab indexeur $\rightarrow$ encodeur $\rightarrow$ assembleur

\underline{Scala :} One Hot-Encoder le jour nommé de la semaine d'une date d'envoi

   \code

   - Lecture données CSV et extraction du jour de la semaine de la date `InvoiceDate`
   - Ajout d'une colonne avec ce jour nommé : `Monday`, ...
   - Conversion en index de ce jour nommé
   - Hot-Encoding de cet index

   > ```scala
   > val csv = spark.read.format("csv").option("header", "true").option("inferSchema", "true")
   >   .load("./data/retail-data/by-day/*.csv")
   > 
   > val preparedDataFrame = csv.na.fill(0) // Remplir les zones NA avec 0
   >   .withColumn("day_of_week", date_format($"InvoiceDate", "EEEE"))
   >   
   > val indexer = new StringIndexer() // Transformer les jours nommés (Monday...) en indexs
   >   .setInputCol("day_of_week")
   >   .setOutputCol("day_of_week_index")
   > 
   > val encoder = new OneHotEncoder() // Il crée une colonne avec 0 ou 1 pour chaque jour.
   >   .setInputCol("day_of_week_index")
   >   .setOutputCol("day_of_week_encoded")
   > ```

   \endgroup

## 4) Le pipeline de transformation

Le \fonction{Pipeline} regroupe la séquence d'opérations de mise en forme des données

   \demitab dans un circuit que toute donnée suivra :  
   \tab indexeur $\rightarrow$ encodeur $\rightarrow$ assembleur

\underline{Scala :} Préparation du circuit d'entraînement des données

   \code

   - Prépare un jeu d'entraînement (apprentissage), avec les dates inférieures au `01/07/2011`  
     et un jeu de test (prédiction), avec les dates supérieures

   - Rassemblement de variables (vecteurs) les variables finales (prêtes au traitement machine learning)

   - Création d'un pipeline de transformation avec `indexer` $\rightarrow$ `encoder` $\rightarrow$ `vectorAssembler`
 
   > ```scala
   > val trainDataFrame = preparedDataFrame.where("InvoiceDate < '2011-07-01'") // 245903 individus
   > val testDataFrame = preparedDataFrame.where("InvoiceDate >= '2011-07-01'") // 296006 individus
   >
   > val vectorAssembler = new VectorAssembler()
   >   .setInputCols(Array("UnitPrice", "Quantity", "day_of_week_encoded"))
   >   .setOutputCol("features")
   > 
   > val transformationPipeline = new Pipeline()
   >   .setStages(Array(indexer, encoder, vectorAssembler))
   > 
   > val fittedPipeline = transformationPipeline.fit(trainDataFrame)
   > val transformedTraining = fittedPipeline.transform(trainDataFrame)
   > transformedTraining.cache()
   > ```

   \endgroup

