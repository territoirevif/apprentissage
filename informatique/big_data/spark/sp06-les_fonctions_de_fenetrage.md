---
title: "sp06. Les fonctions de fenêtrage"
subtitle: "window, lag, lead"
author: Marc Le Bihan
keywords:
- fenêtrage
- window
- spark
- python
- java
- scala
- sql
- dataset
- lag
- lead
- user defined function (UDF)
- partition
- regroupement
---

## 1) Le fenêtrage avec SQL :

\underline{Scala}, à l'aide de la fonction \fonction{window}, et `1 day` :

Soit un Dataset `ds`, avec `CustomerId`, `total_cost`, `InvoiceDate` :

   \code

   > ```scala
   > import org.apache.spark.sql.functions.{window, column, desc, col}
   > 
   > ds.selectExpr("CustomerId", "total_cost", "InvoiceDate")
   >    .groupBy(col("CustomerId"), window(col("InvoiceDate"), "1 day"))
   >    .sum("total_cost").show(5)
   > ```

\bigbreak

   > ```txt
   > +----------+--------------------+-----------------+                             
   > |CustomerId|              window|  sum(total_cost)|
   > +----------+--------------------+-----------------+
   > |   16057.0|{2011-12-05 01:00...|            -37.6|
   > |   14126.0|{2011-11-29 01:00...|643.6300000000001|
   > |   13500.0|{2011-11-16 01:00...|497.9700000000001|
   > |   17160.0|{2011-11-08 01:00...|516.8499999999999|
   > |   15608.0|{2011-11-11 01:00...|            122.4|
   > +----------+--------------------+-----------------+
   > ```

   \endgroup

## 2) Les fonctions lag et lead

L'on considère une fenêtre triée par des colonnes.  
La classe statique \fonction{Window} va défnir la fenêtre et son tri :  

\bigbreak

\underline{Python :}

   \code

   > ```python
   > from pyspark.sql.window import Window
   >
   > Window fenetre = Window.orderBy(["id", "date"])
   > ```

   \endgroup

\underline{Java :}

   \code

   > ```java
   > import org.apache.spark.sql.expressions.Window;
   >
   > WindowSpec fenetre = Window.orderBy("id", "date");
   > ```

   \endgroup

\bigbreak

\methode{lag}{(nom\_colonne, delta)} (en arrière)  
ou \methode{lead}{(nom\_colonne, delta)} (en avant) 

donnent le nombre de lignes qu'on veut remonter dans l'historique  
(exemple dans \fonction{lag} : 1 = un enregistrement en arrière),  
ou avancer (par \fonction{lead}).

\bigbreak

\underline{Python :}

   \code

   > ```python
   > tous_les_cyclistes = tous_les_cyclistes \
   >    .withColumn( "changement", \
   >        changement( F.lag("sur_velo", 0).over(w), F.lag("sur_velo", 1).over(w)))
   > ```

   \endgroup

\underline{Java :}

   \code

   > ```java
   > import static org.apache.spark.sql.functions.*;
   >   
   > Column courant = functions.lag("sur_velo", 0).over(fenetre);
   > Column précédent = functions.lag("sur_velo", 1).over(fenetre);
   > ```

   \endgroup

\bigbreak

Une fonction \fonction{UDF} pourra recevoir ces deux paramètres et renvoyer un booléen en détection de changement, par exemple.

Pour faire des regroupements dans les fenêtres, mieux vaut partionner celles-ci.  

\bigbreak

\underline{Python :} tri par (id, date), puis partition par id

   \code

   > ```python
   > windowval = Window.orderBy(["id", "date"])
   > windowval = windowval.partitionBy("id")
   >   
   > # unboundedPreceding : tous les enregistrements avant l'actuel.
   > windowval = windowval.rangeBetween(Window.unboundedPreceding, 0)
   >   
   > # Faire la somme de tous les changements constatés entre la ligne courante et toutes les précédentes
   > # (dans chaque partition, donc limité à chaque id)
   > tous_les_cyclistes = tous_les_cyclistes \
   >    .withColumn( "numero_de_trajet", F.sum("changement").over(windowval))
   >   
   > # Présente l'extrait qui suit
   > tous_les_cyclistes.select("id", "timestamp", "changement", "numero_de_trajet").where("changement=1").take(5)
   > ```
  
\bigbreak
 
   > ```
   > id=148, timestamp='2018-01-06 10:06:00', changement=1, numero_de_trajet=1,
   > id=148, timestamp='2018-01-07 08:31:00', changement=1, numero_de_trajet=2,
   > id=148, timestamp='2018-01-07 16:02:00', changement=1, numero_de_trajet=3,
   > id=148, timestamp='2018-01-08 07:35:00', changement=1, numero_de_trajet=4,
   > id=243, timestamp='2018-01-01 08:25:00', changement=1, numero_de_trajet=1
   > ```

   \endgroup

\bigbreak

\demitab Par la suite après un \fonction{groupBy} par `numero_de_trajet` pour récupérer les dates minimales et maximales par trajet, l'on peut faire une différence et donner les temps de trajets.

