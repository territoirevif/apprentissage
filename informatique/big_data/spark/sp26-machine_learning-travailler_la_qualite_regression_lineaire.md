---
title: "sp26. Machine Learning. La qualité d'une régression linéaire"
subtitle: ""
author: Marc Le Bihan
keywords:
- régression linéaire
- part de la variance expliquée
- variance expliquée
- variance totale
- TSS
- RSS
---

## 1) La part de la variance expliquée

![Régression linéaire](./machine_learning_ressources/regression_lineaire.jpg){width=90%}

\definition{Variance totale} : l'écart de chaque point par rapport à la moyenne (qui est une valeur unique)
$$
\rouge{TSS} = \sum_{i=1}^{n}{(y_{i} - \bar{y})^2}
$$

\bigbreak

\definition{Variance expliquée} : l'écart de chaque point prédit (sur la droite) par rapport à la valeur qu'il a réellement eue.
$$
\rouge{RSS} = \sum_{i=1}^{n}{(y_{i} - \hat{y}_{i})^2}
$$

\bigbreak

La __part__ de la variance expliquée est un __pourcentage__ : $R^2$
$$
\rouge{R^2} = 1 - \frac{RSS}{TSS} = 1 - \frac{\text{variance non expliquée}}{\text{variance totale}}
$$

