---
title: "sp24. Machine Learning. Travailler les résultats"
subtitle: "par exemple, pour une classification, choisir entre précision et rappel"
author: Marc Le Bihan
keywords:
- spark
- machine learning
- evaluator
- vrai positif
- faux positif
- vrai négatif
- faux négatif
- rappel
- recall
- précision
- accuracy
- moyenne harmonique
- F1
- weightedPrecision
- weightedRecall
- matrice coût - opportunité
- Receiver Operating Characteristic (ROC)
- TFP
- TVP
- Area Under the Curve (AUC)
- prévoir
- prédire
- classer
- prédiction naïve
---

C'est \definition{l'évaluateur (Evaluator)} de _Spark_ qui va observer les résultats et les jauger en fonction des demandes de précision et de rappel qui ont été faites.

   \code
   
   > ```java
   > accuite = evaluateur.evaluate(predictions)
   > ```
   
   \endgroup

## 1) Les vrais et les faux positifs et négatifs

- vrai positif : j'ai trouvé quelque-chose qui est vrai
- vrai négatif : j'ai rejeté à raison quelque-chose qui est faux  
- faux positif : j'ai déclaré que quelque-chose était quelque-chose, mais ce n'est pas vrai  
- faux négatif : je n'ai pas vu que quelque-chose répondait à mon critère, et je l'ai rejeté alors que j'aurais du le prendre

## 2) Le rappel et la précision

### a) Le rappel

Le \definition{rappel} (_recall_) c'est la capacité de __trouver des éléments positifs__.  
Exemple : l'algorithme trouve $7$ (Vrai positifs) sur $9$ individus (parce qu'il fait $2$ faux négatifs) = $77\%$.

Un manque de rappel ? $\implies$ l'algorithme ne trouve rien (ou quasiment rien)

On voudra un bon rappel pour la recherche d'une maladie :  
\tab on ne veut pas manquer de cas, même si parfois ça n'en sont pas.

### b) La précision

La \definition{précision} (_accuracy_) c'est le nombre de fois que l'on est correct  
   \demitab quand on prétend que quelque-chose répond à nos critères  
   \demitab c'est à dire : quand on prédit la classe positive.

Exemple : $7$ fois sur $10$ ($3$ faux positifs) = $70\%$.

Un manque de précision $\implies$ c'est l'algorithme se trompe beaucoup.

$57\%$ de précision, par exemple, c'est à peine mieux qu'une classification naïve !

On voudra une bonne précision en bourse,  
\tab pour que les affaires soient justes, quitte à en laisser passer quelques-unes.

### c) La moyenne harmonique, F1, sythétise la précision et le rappel

La \definition{moyenne harmonique F1} synthétise la précision et le rappel en un seul nombre.
$$
F1 = \frac{2(précision \times rappel)}{précision + rappel}
$$

(attention : au nominateur c'est une multiplication, au dénominateur, une addition).  
Dans les exemples, ça nous donnera un $F1 = 73.7\%$

## 3) Comment choisir ce qu'il faut favoriser ?

_Spark_ permet de déclarer quel métrique on préfère favoriser pour la mesure de la qualité du modèle :

   - \fonction{F1} : la moyenne harmonique, qu'il choisit par défaut
   - \fonction{weightedPrecision} : favoriser la précision
   - \fonction{weightedRecall} : favoriser le rappel
   - \fonction{accuracy}

\bigbreak

Une __matrice coût - opportunité__ permettra de faire des choix ce qu'il faut favoriser

### a) Contre quelle valeur naïve nous battons-nous ? Que voulons-nous prévoir ?

Se poser la bonne question est importante !

Même une optimisation n'est pas la même selon l'interlocuteur à qui l'on s'adresse : chacun ne voit pas la la même comme importante.

\bigbreak

Il faut se poser la question : "_Contre quelle valeur naïve nous battons-nous ?_" : nous voulons faire mieux que quoi ?

Si nous ne faisons pas mieux que le hasard, que les moyennes nationales connues, que les règles de détection déjà présentes dans une entreprise, notre analyse n'aura pas d'intérêt.

   - Que voulons-nous prévoir ?
   - Pourquoi veut-on prédire ou classer ?
   - Quelle est la conséquence d'une fausse prédiction ?
   - Quelle serait la prédiction naïve ?
 
### b) Le choix d'un seuil par la courbe Receiver Operating Caracteristic (ROC)

Un seuil, par défaut à $50\%$, dit quand on décidera qu'un élément peut passer d'une classe à l'autre : c'est à dire être positif.  
   \demitab $0\%$, il sera toujours positif (rappel : $100\%$)  
   \demitab $90\%$, il sera sûrement très précis, mais le rappel s'effondrera.

\bigbreak

Comme il est difficile de déterminer le bon seuil, l'on peut les tester tous grâce à une courbe de __Receiver Operating Characteristic (ROC)__.  

Cette courbe a deux axes :

- en abscisse, le pourcentage des __faux positifs__ \begingroup \large $\frac{\text{nombre des cas qui auraient dû être annoncés négatifs}}{\text{nombre total des cas qui ont été déclarés positifs}}$ \endgroup

  "\emph{\bleu{Je cueille des champignons : je crois que certains sont ceux que je veux, mais je me trompe}}"

\bigbreak

- en ordonnée, le pourcentage des __vrais positifs__ détectés \begingroup \large $\frac{\text{nombre de cas que l'algorithme a su trouver avec raison}}{\text{nombre total des cas qu'il aurait dû trouver}}$ \endgroup

  "\emph{\bleu{Je cherche des oeufs de Pâques dans le jardin. Je ne me trompe pas, mais je ne les trouve pas tous}}"

### c) Comment interpréter les résultats de la courbe ROC ?

au point (0, 0) de la courbe, le seuil réclame une certitude de $100\%$,  
\demitab et de fait, aucun (ou quasiment aucun) élément ne lui plaît.
 
au point (1, 1) de la courbe, le seuil réclame une certitude de $0\%$,  
\demitab et accepte n'importe quoi... 

![La Courbe ROC](./machine_learning_ressources/ROC_AUC.png){width=100%}

_L'aire en dessous de la ligne donne la qualité de la mesure_  
\demitab _$\rightarrow$ une courbe qui serait une droite diagonale de $(0,0)$ en $(1,1)$ dirait $50\%$_  
\demitab _et marquerait une prédiction faite totalement au hasard._

