---
header-includes:
  \input{apprentissage-header-include}
  \input{apprentissage-include}

title: "Scala  \n05. La gestion des dates et du temps"
subtitle: ""
author: Marc Le Bihan
geometry: margin=2cm
fontsize: 12pt
output: pdf
classoption: fleqn
urlcolor: blue
---

La gestion des dates en Scala, utilise un sous-ensemble des classes Java des packages \fonction{java.time} :

   - la classe \fonction{Period}, 

   \code

   > ```scala
   > import java.time.Period
   > val periode = Period.of(2, 5, 27)
   >
   > s"""${periode.getDays} années
   >   | ${periode.getMonths} mois
   >   | ${periode.getYears} années"""
   > ```
   
   \bigbreak

   > ```txt
   > val res0: String = 27 années
   > 5 mois
   > 2 années
   > ```

   \endgroup

\bigbreak

   ou bien, en utilisant les constantes de la classes \fonction{ChronoUnit} de \fonction{java.time.temporal} : `YEAR`, `MONTH`, `DAYS`, `HOUR`...

   \code

   > ```scala
   > import java.time.temporal.Period
   > import java.time.temporal.ChronoUnit
   >
   > periode.get(ChronoUnit.DAYS)
   > ```
   
   \bigbreak

   > ```txt
   > val res1: Long = 27
   > ```

   \endgroup

   On peut rajouter aux périodes des durées avec \fonction{plusYears(...)}, \fonction{plusMonths(...)}... ou en retrancher avec \fonction{minusDays(...)}...

   - la classe \fonction{Duration}...

   \code

   > ```scala
   > import java.time.Duration
   > java.time.temporal.ChronoUnit
   >
   > Duration.of(10192927, ChronoUnit.MILLIS) // PT2H49M52.927S
   > ```
 
   \endgroup

   Il est possible d'utiliser les méthodes \fonction{toHours()}, \fonction{toDays()}... Mais attention : elles retournent des nombres entiers. Ci-dessus, un `getDays()` renverra 0, car il y a 0 jours... et 2 heures.


   Et les méthodes \fonction{plus...} et \fonction{minus...} vues ci-dessus fonctionnent aussi.

   \fonction{java.util.Date}, \fonction{java.util.Calendar}, \fonction{java.time.LocalDate} ou \fonction{java.time.LocalDateTime} sont reprises de Java, et foncitonnent à l'identique avec les mêmes méthodes.
