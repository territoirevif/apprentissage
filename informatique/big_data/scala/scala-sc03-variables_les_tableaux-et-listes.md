---
header-includes:
  \input{apprentissage-header-include}
  \input{apprentissage-include}

title: "Scala  \n03. Variables : les tableaux et listes"
subtitle: ""
author: Marc Le Bihan
geometry: margin=2cm
fontsize: 12pt
output: pdf
classoption: fleqn
urlcolor: blue
---

# I) Les tableaux

## 1) Déclaration et accès aux postes d'un tableau

On accède à un poste de tableau par \fonction{()} et pas par `[]` comme en _Java_.  

\fonction{ArrayBuffer} est l'équivalent de son `ArrayList`.

   \code

   > ```scala
   > var a:Array[Int] = new Array(5, 28, 14);
   > print(a[1]);
   >
   > // Il y a les tableaux mutables et non mutables
   > import scala.collection.mutable.ArrayBuffer
   >
   > var b:ArrayBuffer[Int] = new ArrayBuffer();
   > b.append(5);
   > b += (8, 3, 2);
   > print(s"$b");
   >
   > for(i <- 0 to 3)
   >    println(b(i));
   > ```

   \endgroup

## 2) \textcolor{blue}{yield}, pour dire comment fabriquer de nouvelles valeurs à partir d'anciennes

   \code

   > ```scala
   > for(v <- tab) yield v * v // Attribue aux postes du nouveau tableau la valeur v * v
   > ```

   \endgroup

\underline{Remarque :}  
un `if` peut être inclus dans le `for` pour filtrer les valeurs sur lesquelles l'on veut faire le `yield`. Ainsi, un :

   \code

   > ```scala
   > val tab = Array(1,5,9,7,11,3)
   > for(v <- tab if v != 3) yield v * v
   > ```

   \endgroup

Ne va pas retourner `Array(1, 25, 81, 49, 121, 9)`  
mais un `Array(1, 25, 81, 49, 121)` avec l'élément _3_ exclu, et est différent de :

   \code

   > ```scala
   > for(v <- tab) yield
   >    if (v != 3) v * v else v
   > ```

   \endgroup

qui laissera le _3_ intact : `Array(1, 25, 81, 49, 121, 3)`.

## 3) Méthodes utilitaires

   - `min` ou `max` : minimum ou maximum d'un tableau
   - `sum` ou `product` : somme ou produit des éléments d'un tableau
   - `sorted` : le trie.
   
   - `mkString(séparateur)` convertit un tableau en chaîne un séparateur eentre éléments.  
Exemple : `print(tab mkString("\n"))`.

# II) Les collections

Les collections implémentent \fonction{Traversable} \gris{(= équivalent `Iterable` de Java ? Pas sûr, car il existe aussi `Iterable` en Scala)} .

  ![traits et classes](informatique/types_collections_scala.jpg)  
  _Traits (= interfaces) et classes, en Scala_

  - ont les méthodes classiques `isEmpty`, `length`, `contains`, `indexOf` issues de Java.

Les suivantes, implémentent toutes `Iterable`.

## 1) Seq, en général (trait)

C'est la plus proche de `List` en Java.  

En fait, si l'on écrit dans _Spark REPL_ :

   \code
    
   > ```scala
   > val sequence = Seq(1,2,3)
   > 
   > // l'on a la réponse : 
   > al sequence: Seq[Int] = List(1, 2, 3)
   > ```
    
   \endgroup

$\to$ il a transformé le `Seq` en `List`.  
\gris{(lui-même, paraissant être un type abstrait, sans doute en \texttt{ArrayList}, concrètement ?)}

Elle est pourvue de constructeurs.

\underline{Remarque :} on peut ajouter deux listes, en utilisant \fonction{++} : `ab = a ++ b`

Les méthodes suivantes peuvent s'appliquer, pour produire certaines séquences :

  - \fonction{diff} : `c = a diff b` fera de `c` la différence (intersection) entre `a` et `b`.
  - \fonction{distinct} : en supprimera les doublons.
  - \fonction{reverse} : inversera l'ordre des membres de la séquence.  
  - \fonction{fill(5)("mot")} : remplira une liste de cinq occurrences de `"mot"`.  
    On peut remplacer `"mot"` par un appel de fonction, qui renverra la valeur à placer.  
  - \fonction{range(0,10)} : créera une liste allant de $0$ à $9$ (borne supérieure exclue)
    `range(0,10,2)` ira par pas de $2$ (donnant ici, une liste de nombres pairs, jusqu'à 8 inclus).

En termes d'actions, 

  - \fonction{take(2)} : extrait les deux premiers éléments de la séquence.  
  - \fonction{drop(n)} : supprimera les $n$ \underline{premiers} éléments de la liste :  
  si `sequence = List(1,2,3)`, alors  
  \ \ \ \ `sequence.drop(1)` la réduira à `(1,2)`,  
  \ \ \ \ et `sequence.drop(2)` à `(3)`.  
  - \fonction{dropRight(n)} : supprimera les $n$ \underline{derniers} éléments de la liste.

## 2) List, pour les collections ordonnées

Elle s'apparente à l'`ArrayList` de Java.

On peut créer une liste à partir d'une autre liste, avec un élément de plus, grâce à l'opérateur \fonction{::} 

   \code

   > ```scala
   > val liste = List(1,2,3)
   > val c = 0 :: liste  // répond que c est l'ensemble List[Int] = List(0, 1, 2, 3)
   > ```

   \endgroup

Et concaténer deux listes avec \fonction{:::}

   \code

   > ```scala
   > val d = liste ::: liste  // répond que d est : (1, 2, 3, 1, 2, 3)
   > ```

   \endgroup

## 3) Vector, pour les collections ordonnées et indexées

\definition{Vector} est une collection d'éléments ordonnés __et indexés__.  
De fait, son indexation le rend plus rapide lors de l'accès à ses éléments.

## 4) Set, pour des collections sans doublons

\definition{Set} est une collection d'éléments ordonnés et indexés __sans doublons__.

On peut concaténer les `Set` avec `++`, et l'on verra que les doublons en sont retirés,  
mais pour avoir une stricte __intersection__, c'est l'opérateur \fonction{\&} qu'il faut utiliser :

   \code

   > ```scala
   > val a = Set(1,2,3,1,2,3) // Va purger les doublons à la création => Set(1, 2, 3)
   >
   > a ++ Set(2,3,5)   // concaténation, répond : Set(1, 2, 3, 5)
   > a & Set(2,3,5)    // intersection, répond : Set(2, 3)
   > ```

   \endgroup

## 5) Map, pour des collections de clefs/valeurs

\underline{Initialisation :}

   - alimentée : `val map = Map("A" -> 12, "B" -> 4, "C" -> 4)`
   - à vide : `val mapVide = Map.empty[String, Int]`

\underline{Recherche d'un élément :}

   - directement : 

      - `map("A")` retournera 12, 
      - mais `map("D")` lèvera une `java.util.NoSuchElementException` avec le message : `key not found: D`

   - via la méthode `get` qui renvoie une `Option` : 

      - `map.get("A")` retournera `Some(12)` et `map.get("A").get` : 12
      - `map.get("D")` : None

\underline{Ajout d'éléments dans une map}

   > `map + ("D" -> 5)` ajoute cet élément à la `map`  
   > `map1 ++ map2` concatène deux maps

## 6) ArrayBuffer : une liste mutable où l'on peut ajouter/retirer des éléments

   \code

   > ```scala
   > import scala.collection.mutable.ArrayBuffer
   > 
   > // Ajouter un élément à un ArrayBuffer
   > val buffer = ArrayBuffer(1,2,3) // = ArrayBuffer(1, 2, 3)
   > buffer += 7                     // = ArrayBuffer(1, 2, 3, 7)
   >
   > // Ajouter une liste à un ArrayBuffer   
   > val liste = List(8,12)
   > buffer ++= liste   // = ArrayBuffer(1, 2, 3, 7, 8, 12)
   >
   > // Lui supprimer des éléments
   > buffer -= 8        // = ArrayBuffer(1, 2, 3, 7, 12)
   > buffer.remove(3)   // Int = 7 : c'est l'index 3 qui est supprimé, sa valeur valait 7 
   > buffer             // = ArrayBuffer(1, 2, 3, 12)
   > ```

   \endgroup

\underline{Remarque :}

  `remove` a une variante à deux arguments : le premier est alors l'index de départ et le suivant : le nombre d'élément s à supprimer. Se tromper d'index, demander à supprimer trop d'éléments produiront tous deux un `ArrayOutOfBoundsException`.

## 7) Le HashSet est respectivement le type mutable du Set, le HashMap du Map

   \code

   > ```scala
   > import scala.collection.mutable
   >
   > val s = mutable.HashSet('a', 'b', 'c')
   > s += 'd' // = HashSet(a, b, c, d)
   > s += 'a' // = HashSet(a, b, c, d)
   > s += 'e' // = HashSet(a, b, c, d, e)
   > ```

   \endgroup

## 8) Le tuple

Il ressemble à un enregistrement, et permet d'associer par lignes, plusieurs éléments hétérogènes :

   \code

   > ```scala
   > val tuple = (1, "deux", '3', true, List(14, -3))
   > val tuple: (Int, String, Char, Boolean, List[Int]) = (1,deux,3,true,List(14, -3))
   > ```

   ```
   tuple._1
   val res1: Int = 1
    
   tuple._4
   val res3: Boolean = true
    
   tuple._5
   val res4: List[Int] = List(14, -3)
   ```

   \endgroup

Pour itérer sur les "_champs_" d'un enregistrement il faut appliquer au tuple (l'enregistrement) la méthode \fonction{productIterator}. Ainsi, ceci dump tout les champs :

   \code

   > ```scala
   > tuple.productIterator.foreach(println);
   > ```

   ```
   1
   deux
   3
   true
   List(14, -3)
   ```

   \endgroup

