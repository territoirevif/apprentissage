---
header-includes:
  \input{apprentissage-header-include}
  \input{apprentissage-include}

title: "Scala  \n04. Les fonctions"
subtitle: ""
author: Marc Le Bihan
geometry: margin=2cm
fontsize: 12pt
output: pdf
classoption: fleqn
urlcolor: blue
---

## Lambdas

\underline{Remarque:}  
__\textcolor{blue}{apply}__ ressemble à `invoke` en _Java_ :  
(les exemples ne sont pas encore très clairs)

### Lambda de Fonction-objet

```scala
// C'est un : après la déclaration du nom de la variable-fonction
val fCouleurDrapeau: (String, String, String) => String =
   (c1, c2, c3) => (c1 + " "  + c2 + " " + c3)

fCouleurDrapeau("bleu", "blanc", "rouge")
```

Il est possible de passer ces fonctions en arguemnts, et de faire qu'une fonction retourne une autre fonction, devenant une __fonction d'ordre supérieur__.

### Fonction-méthode

définie avec le mot-clef `\textcolor{blue}{def}`. Si elle renvoie un type `Unit` (= void en Java), c'est une procédure.

   ```scala
   def salutation(qui: String): Unit = 
      print("bonjour " + qui)

   salutation("toi")
   ```

### Fonction

Le symbole __=__ montre qu'elle va renvoyer une valeur.
Mais, __= Unit__ est l'équivalent de __void__ en _Java_ : ne retourne pas de valeur.

   ```scala
   // En inférant les paramètres
   def somme(v1: Int, v2: Int) = v1 + v2

   // En les déclarant explicitement
   def somme(v1: Int, v2: Int) : Int = { v1 + v2 }
   ```

## Traits (= interfaces Java)

```scala
class MaClass with trait1
```

### Variantes des appels de fonctions avec ou sans parenthèses :

```scala
"comment ça va ?".indexOf("ça")
"comment ça va ?" indexOf "ça"

// On les retire souvent pour les fonctions qui n'ont pas d'arguments.
"comment ça va ?" indexOf "aç".reverse
```

