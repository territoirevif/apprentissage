---
title: "sc01. Compilations et manipulations"
subtitle: ""
author: Marc Le Bihan
---

## 1) L'interpréteur Scala

Les anciennes versions (`2.12` et inférieures) ont un bug d'affichage, sous _Linux_, à contourner ainsi :

   \code

   > ```bash
   > export TERM=xterm-color && scala
   > ```

   \endgroup

## 2) La commande de compilation

La pure et dure, équivalente de `javac` :

   \code

   > ```bash
   > scalac -classpath . MonSource.scala
   > ```

   \endgroup

qui produit un `.class` exécutable par la commande `scala`.

## 3) Les différences syntaxiques de Scala avec Java

### a) L'import de toutes les méthodes d'un package

   \code

   > ```scala
   > import scala.Console_
   > ```

   \endgroup

## 4) La lecture / écriture depuis la console

### a) l'écriture

La couleur ou le style des caractères peut être changée sur la console.

   \code

   > ```scala
   > println(s"texte normal, ${Console.BOLD} texte gras ${Console.RESET}")
   > ```

   texte normal, __texte gras__

   \endgroup

\bigbreak

On trouvera aussi : \fonction{GREEN}, \fonction{RED}, \fonction{YELLOW}...  
\fonction{BLUE\_B} donnera un fond bleu.

Attention à bien annuler ses effets avec \fonction{Console.RESET} pour terminer.

### b) La lecture

Les instructions qui suivent provoquent l'input classique de caractères.

   \code

   > ```scala
   > import scala.io.StdIn.readLine
   > val prenom = readLine()
   > ```
  
   > ```log
   > Marc
   > val prenom: String = Marc
   > ```

   \endgroup

\bigbreak

des variantes : \fonction{readString()}, \fonction{readBoolean()}, \fonction{readInt()}...  
   \demitab tenteront de lire strictement ces contenus depuis le clavier.

   - pour un booléen, lire une valeur qui n'est pas \fonction{true} renverra un \fonction{false}
   - mais une chaîne de caractères dans un \fonction{readInt()} provoquera un \fonction{NumberFormatException}

