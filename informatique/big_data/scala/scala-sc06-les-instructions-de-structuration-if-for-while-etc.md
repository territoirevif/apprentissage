---
header-includes:
  \input{apprentissage-header-include}
  \input{apprentissage-include}

title: "Scala  \n06. Les instructions de structuration"
subtitle: "if, for, while..."
author: Marc Le Bihan
geometry: margin=2cm
fontsize: 12pt
output: pdf
classoption: fleqn
urlcolor: blue
---

# if

Le `if` est classique, mais il n'oblige pas à assigner une valeur dans ses blocs

Ceci est accepté : 

   \code

   > ```scala
   > val bonneReponse = false 
   >
   > if (bonneReponse) {
   >    "gagné"
   > }
   > else {
   >    "perdu"
   > }
   > ```

   ```
   val bonneReponse: Boolean = false
   val res0: String = perdu
   ```

   \endgroup

En Java, une affectation aurait été nécessaire :

   \code

   > ```java
   > boolean bonneReponse = false;
   > String reponse;
   >
   > if (bonneReponse) {
   >    reponse = "gagné";
   > }
   > else {
   >    reponse = "perdu";
   > }
   > ```

   ```
   bonneReponse ==> false
   reponse ==> "perdu"
   ```

   \endgroup
