---
title: "sc02. Variables, types, objets, classes"
subtitle: ""
author: Marc Le Bihan
---

## 1) Les types d'objets

   > \fonction{Unit} : équivalent de `void`  
   > \fonction{Any} : classe de base de tous les objets  
   > \fonction{AnyVal} : classe de base de tous les objets numériques  
   > \fonction{AnyRef} : classe de base de toutes les classes : équivalent d'`Object` en Java.

### a) Conversions entre types

Avec les méthodes \fonction{toInt}, \fonction{toDouble}, \fonction{toString}...

## 2) Déclaration de variables

\fonction{var} déclare une variable, \fonction{val}, une constante.

Une variable peut être évaluée à sa déclaration dans un bloc

  \code

   > ```scala
   > val a = { println("Initialisation de a"); 5 * 7; }
   > ```

   \endgroup

\bigbreak

\fonction{lazy} retarde l'évaluation de la variable au moment où elle sera réellement utilisée.

### a) Trois types d'équalités

\fonction{equals}, comme l'equals du Java  
\fonction{eq}, teste par pointeur, comme le `==` du Java  
\fonction{==}, revient à la méthode `equals` naturelle, sans redéfinition

\bigbreak

   > Attention : \fonction{if (a == null)} fonctionne si `a` est un objet,   
   > mais \fonction{a equals null} va faire un `NullPointerException`.

### b) Texte multiligne

  \code

  > ```scala
  > val texte = """Un texte
  >      | très très
  >      | long"""
  > ```
  >
  > ```txt
  > val texte: String = Un texte
  > très très
  > long
  > ```

  \endgroup

### c) Concaténation de chaînes

  \code

   > ```scala
   > var bonjour: String = "bonjour"
   >
   > // Classique
   > bonjour + " Jean"
   > val res0: String = bonjour Jean
   >
   > // avec s et "..."
   > s"$bonjour Jean"
   > val res1: String = bonjour Jean
   > ```

   \endgroup

## 3) Les classes

- Les classes peuvent redéfinir les opérateurs

  \code

   > ```scala
   > def += (monnaie: Int) {contenu += monnaie}
   > ```

   \endgroup

- Une classe ayant un constructeur avec argument peut se déclarer ainsi :

  \code

   > ```scala
   > class Tirelire(argentDepart : Int) {
   >    private var contenu : Int = argentDepart;
   >
   >   def this() = this(0); // Constructeur
   > ```

   \endgroup

- \fonction{@Override} d'une fonction :

  \code

   > ```scala
   > // Redéfinit toString
   > override def toString = "contient (" + combien + ")";
   > ```

   \endgroup

