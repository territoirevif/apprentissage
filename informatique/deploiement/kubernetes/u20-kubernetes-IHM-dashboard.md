---
title: "u20. Kubernetes : IHM - Dashboard"
subtitle: ""

keywords:
   - minikube
   - dashboard

author: Marc Le Bihan
---

\definition{Interface Utilisateur de Kubernetes} : 

On la lance par la commande : 

   \code

   > ```bash
   > minikube dashboard
   > ```

   \endgroup

et ses déploiements liés sont visibles par : 

   \code

   > ```bash
   > kubectl get deployments --namespace=kubernetes-dashboard
   > kubectl get services --namespace=kubernetes-dashboard
   > ```

   \endgroup

