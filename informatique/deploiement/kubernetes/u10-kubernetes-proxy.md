---
title: "u10. Kubernetes: installation et démarrage"
subtitle: ""

keywords:
   - kubernetes
   - proxy
   - DaemonSet

author: Marc Le Bihan
---

\definition{Proxy}: à l'aide d'un \fonction{DaemonSet}, Kubernetes véhicule le trafic par ce proxy sur tous les noeuds.

On peut voir les proxy avec cette commande:

   \code

   > ```bash
   > kubectl get daemonSets --namespace=kube-system kube-proxy
   > ```
   > \scriptsize
   > ```bash
   > NAME         DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR            AGE
   > kube-proxy   1         1         1       1            1           kubernetes.io/os=linux   153d
   > ```

   \endgroup

