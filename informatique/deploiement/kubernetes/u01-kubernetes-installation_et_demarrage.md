---
title: "u01. Kubernetes: installation et démarrage"
subtitle: ""

abstract: |  
   Installation de CRI-O. minikube dashboard.  

keywords:
   - minikube
   - kubernetes
   - k8s
   - cri-o

author: Marc Le Bihan
---

\begingroup \cadreAvantage

> Une expérimentation est possible depuis le répertoire:  
>  `~/dev/apprentissageDev/deploiement/kubernetes_pret_a_demarrer`
> \bigbreak
>
>  ```bash
>  minikube start # pour commencer la session
> ```

\endgroup

---

# I) Minikube et kubectl

\underline{remarque :} Si les commandes `kubectl` qui suivent répondent après un `minikube start` par un `command not found`, il pourra être utile de faire un alias :

   \code

   > ```bash
   > alias kubectl="minikube kubectl --"
   > ```

   \endgroup

\bigbreak

minikube $\rightarrow$ virtualbox + docker ou virtualbox + cri-o:

   \code

   > ```bash
   > # optionnel au démarrage : dire l'hyperviseur que l'on veut.
   > minicube start --vm-driver=hyperkit
   > minicube status
   > ```

   > ```bash
   > kubectl get componentstatuses
   > ```
   > \bigbreak
   > ```text
   > NAME                 STATUS    MESSAGE                         ERROR
   > scheduler            Healthy   ok                              
   > controller-manager   Healthy   ok                              
   > etcd-0               Healthy   {"health":"true","reason":""}   
   > ```

   \endgroup

# II) Installation de CRI-O

Les instructions qui suivent doivent être exécutées en root, paramétrée selon la version du kubernetes installé et de l'OS où l'on se trouve. 

Par exemple, pour `minikube version` retournant `1.25.2` et Debian 11, ce sera:

   - `Debian_11`
   - `1.25`

   \begingroup \cadreGris \scriptsize

   > ```bash
   > sudo echo "deb https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/Debian_11/ /" > /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list
   > sudo echo "deb http://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable:/cri-o:/1.25/Debian_11/ /" > /etc/apt/sources.list.d/devel:kubic:libcontainers:stable:cri-o:1.25.list
   > curl -L https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable:cri-o:1.25/Debian_11/Release.key | apt-key add -
   > curl -L https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/Debian_11/Release.key | sudo apt-key add -
   > sudo apt update
   > sudo apt upgrade
   > sudo apt install cri-o cri-o-runc
   > sudo systemctl start crio.service
   > sudo systemctl enable crio.service
   > sudo apt-get install cri-tools
   > ```

   \endgroup

