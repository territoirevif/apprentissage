---
title: "u30. Kubernetes: le déploiement"
subtitle: "et la structure de son fichier"

author: "Marc Le Bihan"

abstract: |  
  On utilise pas directement les pods: on crée un déploiement qu'on place dans un namespace.  
  Les pods et éléments créés par lui s'adressent mutuellement par des sélecteurs qui ciblent des labels. 

keywords:
  - deployment
  - déploiement
  - pod
  - label
  - sélecteur
  - kind
  - replicas
  - containers
  - images
  - apply
  - expose
---

---

On ne travaille pas sur des pods mais sur des _deployments_ (il n'existe pas de commande `kubectl create pod`).  
$\rightarrow$ on édite le déployeur d'un pod, pas le pod lui-même.

## 1) Création, édition, suppression et application d'un déploiement

La création manuelle d'un déploiement est posible, mais fastidieuse:

   \code

   > ```bash
   > kubectl create deployment <name> --image=<image>
   > kubectl get deployment   # Le montrera, mais non démarré
   > ```

   \endgroup

\bigbreak

$\rightarrow$ il mieux vaut appliquer un fichier:

   \code

   > ```bash
   > kubectl apply -f <filename>
   > # Remarque : un delete peut aussi prendre cette option -f
   > ```

   \endgroup

\bigbreak

ou obtenir le script de déploiement éditable d'un pod:

   \code

   > ```bash
   > kubectl edit <name>
   > ```

   \endgroup

\bigbreak
Un déploiement se supprime avec `kubectl delete <nom>` ou `kubectl delete -f monDeploy.yaml`

__quand un déploiement étant en cours, il faut attendre que tout ait démarré__

   \code

   > ```bash
   > kubectl get deployment   # Le montrera, en cours de démarrage
   > kubectl get pod          # Montrera son pod, en cours de démarrage aussi
   > ```

   \endgroup

\bigbreak

\attention{Par défaut, un service nouvellement déployé est privé !}
On peut le rendre visible par la commande:

   \code

   > ```bash
   > kubectl expose deployment hello-node --type=LoadBalancer --port=8080
   > ```

   \endgroup 

## 2) La structure d'un fichier de déploiement

Deployement $\supset$ ReplicaSet $\supset$ Pod $\supset$ Container

Dans le fichier de déploiement, l'on trouve:

   - __kind__: Deployment
   - __name__
   - __replicas__
   - __containers__
   - __images__

\bigbreak

Il a trois parties:

   - metadata

   - spécification: elle peut s'interpréter comme: le statut que l'on désirerait avoir. La cible.

      - dans la section __template__ se trouve la définition des __pods__.

   - status (qui peut être généré automatiquement par _Kubernetes_): son statut actuel

      > Aussi, quand _Kubernetes_ voit que \avantage{statut désiré (spécification) $\neq$ statut actuel}, il sait qu'il a quelque-chose à faire.  
      > Ce statut "Live" vient du composant `etcd`.

---

__port: 80__ Un service écoute port 80  
__targetPort: 8080__ mais il joint sont pod sur le port 8080 (doit être égal à `containerPort`).

