---
title: "u14. Kubernetes: les étiquettes et les annotations"
subtitle: ""

author: "Marc Le Bihan"

keywords:
  - label
  - sélecteur
  - pod
---

---

## 1) Les étiquettes (labels) et les annotations

Tout objet Kubernetes a une adresse RESTful.  
La connexion entre composants se fait par des \definition{labels} et des \definition{sélecteurs}.

   > Les \definition{sélecteurs} disent à quel \definition{label} se rattacher
   >
   > - Un service aura pour label : `nginx-service`
   > - et un sélecteur vers un pod : `nginx`

$\rightarrow$ ils aident à sélectionner les composants.

Pour placer un label \fonction{color=red} à un pod `mysite-drupal-d6d99c65f-lvtxf color=red` :

   \code

   > ```bash
   > kubectl label pods mysite-drupal-d6d99c65f-lvtxf color=red
   > ```

   \endgroup 

Le label est protégé. Pour l'éditer/remplacer, il faudra utiliser `--overwrite` dans la commande suivante,  

et pour le supprimer, le suffixe \fonction{-} accolé au nom du label : \fonction{color-} :

   \code

   > ```bash
   > kubectl label pods mysite-drupal-d6d99c65f-lvtxf color-
   > ```

   \endgroup 

