---
title: "u03. Les pods"
subtitle: ""
author: Marc Le Bihan
keywords:
- kubernetes
- pod
- container
- cgroup
- application
- manifeste de pod
- fichier de déploiement de pod
- déboggage de pod
- forward de port
- copie de fichier
---

Le \definition{pod} est l'unité atomique qui __regroupe plusieurs containers__.

\bigbreak

D'un point de vue _Linux_, chaque pod a son propre `cgroup`, ils peuvent se partager plusieurs esapces de noms.

# I) Applications dans un même pod ou dans des pods différents

## 1) Applications dans un même pod

Les applications qui sont dans le même pod ont le/la même :

   - Adresse IP
   - Espace de ports (espaces de nom réseau)
   - Nom d'hôte

## 2) Applications dans des pods différents

Des applications qui sont sur des pods différents, mais dans le même noeud, peuvent être sur des serveurs différents.

En général, si deux applications peuvent fonctionner sur des pods différents, parce que passer intrinsèquement par le réseau ne les gênera pas, alors il est recommandé de les mettre sur des pods différents.

Si, à l'inverse, elles montrent qu'elles ne peuvent pas fonctionner éloignées l'une de l'autre, c'est un bon motif de les regrouper sur le même pod.

# II) Créer un pod

## 1) Le manifeste de pod

Le pod fonctionne avec une \definition{configuration déclarative}.

   > C'est à dire que cette configuration dit ce que l'on voudrait atteindre, à l'inverse d'une impérative qui donne une série d'instructions pour aboutir.

\bigbreak

_Kubernetes_ cherchera les pods non encore installés sur des noeuds pour les y placer, et plusieurs pods peuvent être placés sur une même machine :

   - tant qu'elle a assez de ressources,  
   - et en prenant en considération que si elle gèle, c'est l'ensemble qui plante.  
     aussi, s'il y a des réplicas, Kubernetes mettra une même application sur des machines différentes.

## 2) Le fichier de déploiement

On place ces directives dans un fichier `yaml`:

   \code
   
   > ```yaml
   > # Pod kuard
   > apiVersion: v1
   > kind: Pod
   > 
   > metadata:
   >   name: kuard
   > 
   > spec:
   >   containers:
   >   - image: gcr.io/kuar-demo/kuard-amd64:1
   >     name: kuard
   > 
   >     ports:
   >     - containerPort: 8080
   >       name: http
   >       protocol: TCP
   > ```
   
   \endgroup

\bigbreak

puis l'on applique un : 

   \code
   
   > ```bash 
   > kubectl apply -f mlb_kuard-pod.yaml
   > 
   > # et cette commande le fera apparaître dans les pods :
   > kubectl get pods
   > ```

   ```
   NAME                            READY   STATUS    RESTARTS      AGE
   kuard                           1/1     Running   0             14s
   ```
 
   \endgroup

# III) Les principales commandes kubectl autour des pods

   \code

   > ```bash
   > kubectl get nodes
   > kubectl describe node <nom_du_noeud>
   > kubectl version
   > ``` 

   \endgroup

## 1) Autour d'une commande get

\methode{kubectl }{get <type-de-ressource> <nom-ressource>} affiche une ressource, dans l'espace de nom par défaut, si l'on ne précise rien.

   \code

   > ```bash
   > kubectl get pod mysite-mariadb-0
   > ```
   >
   > \footnotesize
   >
   > ```bash
   > NAME               READY   STATUS    RESTARTS        AGE
   > mysite-mariadb-0   1/1     Running   1 (3d13h ago)   3d14h
   > ```

   \endgroup

   \small

L'option `-o wide` donnera plus de détails.

   \code

   > ```bash
   > kubectl get pod mysite-mariadb-0 -o wide
   > ```
   >
   > \footnotesize
   >
   > ```bash
   > NAME               READY   STATUS    RESTARTS        AGE     IP           NODE       NOMINATED NODE   READINESS GATES
   > mysite-mariadb-0   1/1     Running   1 (3d13h ago)   3d14h   172.17.0.3   minikube   <none>           <none>
   > ```

   \endgroup

Des options comme `-o json` ou `-o yaml` les dumperont dans ce format-là.  
`--no-headers` retirera les entêtes de la sortie.

`-o jsonpath --template=...` provoquera une extraction précise de certains éléments.

   \code

   > ```bash
   > kubectl get pods mysite-mariadb-0 -o jsonpath --template={.status.podIP}  
   > ```
   >
   > \footnotesize
   >
   > ```bash
   > 172.17.0.3
   > ```

   \endgroup

## 2) les commandes sur les pods

   \code

   > ```bash
   > # liste tous les pods
   > kubectl get pod
   >
   > # get se décline vers d'autres cibles 
   > kubectl get services # Liste des services
   > ```

   \endgroup

\bigbreak

la liste des pods est aussi acessible par l'API REST, sur l'URL :

   > \fonction{https://mon-serveur-k8s.com/api/v1/namespaces/\textbf{default}/pods/\textbf{mon-pod}}

Cette commande donne toute la configuration d'un cluster.

   \code

   > ```bash
   > kubectl config view
   > ```
  
   \endgroup

Le détail d'un pod, pour avoir le statut des étapes de création des containers sous-jaçents, par exemple :

   \code

   > ```bash
   > kubectl describe <pod-name-complet>
   > ```

   \endgroup

# IV) Déboggage (logs, exec)

## 1) Logs

Obtenir les logs d'un pod, se fait avec l'ordre \fonction{logs}

   \code

   > ```bash
   > kubectl logs mysite-drupal-d6d99c65f-lvtxf color=red
   > ```
   > \bigbreak \footnotesize
   >
   > ```log 
   > drupal 02:54:22.44 INFO  ==> ** Starting Drupal setup **
   > realpath: /bitnami/apache/conf: No such file or directory
   > drupal 02:54:22.46 INFO  ==> Configuring the HTTP port
   > drupal 02:54:22.47 INFO  ==> Configuring the HTTPS port
   > drupal 02:54:22.47 INFO  ==> Configuring Apache ServerTokens directive
   > ```

   \endgroup 

   - L'option `-f` les fera suivre en continu, comme un `tail -f`.
   - L'option `--previous` affichera celle du précédent container exécuté, et pas l'actuel.

## 2) exec

L'équivalent de `docker exec`, mais il prend un `--` avant la comamnde à exécuter :

   \code
 
   \underline{depuis la 1.19 :}

   > ```bash
   > kubectl exec --stdin --tty kuard -- /bin/sh
   > ``` 

   \code

   > ```bash
   > kubectl exec -it mysite-drupal-d6d99c65f-lvtxf -- bash
   > ```

   \endgroup 

## 3) Forward de port

Si rien n'est spécifié, le pod ne sera pas accessible.

On peut l'arranger, ponctuellement, par un \fonction{port-forward}, qui permet d'y accéder ensuite (via un tunnel ssh) par `http://localhost:8080/`, par exemple.

   \code

   > ```bash
   > kubectl port-forward kuard 8080:8080
   > ```

   ```
   Forwarding from 127.0.0.1:8080 -> 8080
   Forwarding from [::1]:8080 -> 8080
   ```

   \endgroup

## 4) Copie de fichiers

La commande \fonction{cp} permet la copie de fichier depuis/vers un container.

Egalement depuis/vers son ordinateur local :

   \code

   > ```bash
   > kubectl cp mysite-drupal-d6d99c65f-lvtxf:/chemin1/fichier1 /chemin2/fichier2
   > ```

   \endgroup 

