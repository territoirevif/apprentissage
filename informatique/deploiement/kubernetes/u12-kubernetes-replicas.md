---
title: "u12. Kubernetes: Replicas"
subtitle: ""

keywords:
   - kubernetes
   - ReplicaSet
   - Replicas

author: Marc Le Bihan
---

---

\definition{ReplicaSets} :

Obtenir le nombre de réplicas d'un déploiement :

   \code

   > ```bash
   > kubectl get replicaset   
   > 
   > # sous la forme
   > # <NAME>-<ReplicaSetId>-Id
   > ```

   \endgroup

