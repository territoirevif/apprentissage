---
title: "u04. Les contrôles d'intégrités (sondes)"
subtitle: ""
author: Marc Le Bihan
keywords:
- kubernetes
- sonde
- état de l'application
- liveness
- sonde d'activité
- sonde de disponibilité
- readiness
- fichier de déploiement
---

_Kubernetes_ fait quelques contrôles par lui-même, et redémarre une application qui semble arrêtée, mais cela ne suffit pas.

On peut demander à ce qu'il vérifie l'état de l'application, en le précisant dans son fichier de déploiement.

# I) Les types de sondes

## 1) Sonde d'activité (liveness)

Cette section, ajoutée dans la `spec:` d'un fichier de déploiement, crée une sonde d'activité :

   \code

   > ```yaml
   > # On vérifie sur path /healthy de l'application, 
   > # 5 secondes après que tous les containers aient démarrés,
   > # que le path répond bien avec un code statut 200 dans les 1 seconde
   > # Si plus de trois sondes sont en échec, Kubernes redémarrera l'application
   > #
   > # Il fait un test de vie toutes les 10 secondes
   > livenessProbe:
   >   httpGet:
   >     path: /healthy
   >     port: 8080
   >   initialDelaySeconds: 5
   >   timeoutSeconds: 1
   >   periodSeconds: 10
   >   failureThreshold: 3
   > ```

   \endgroup

## 2) Sonde de disponibilité (readiness)

Cet état détecte si le composant est prêt à répondre aux sollicitations des utilisateurs.

Ces contrôles peuvent être adaptés (par des appels TCP au lieu d'HTTP, par des commandes exec, etc), selon le type de l'application, si c'est plus pertinent.

Dans le cas d'un script à exécuter, le code de sortie 0 sera considéré comme un succès, et tout autre code dira un échec.

