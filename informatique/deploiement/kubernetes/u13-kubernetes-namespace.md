---
title: "u13. Kubernetes: le namespace"

author: "Marc Le Bihan"

keywords:
  - namespace
  - contexte
  - set-context
  - use-context
---

---

\tab On évite de travailler dans l'environnement par défaut de _Kubernetes_. L'on se place dans un \definition{namespace} qui sanctuarise un environnement de déploiement pour son application.

## Le namespace et le contexte

Le namespace est l'équivalent d'un dossier, qui est accessible en ajoutant \fonction{--namespace=<mon-namespace>} à une commande `kubectl`.

Par commodité on peut se placer dans un \definition{contexte} avec la commande \fonction{config} où il deviendra celui par défaut.

   \code

   > ```bash
   > kubectl config set-context mon-contexte --namespace mon-contexte
   > kubectl config use-context mon-contexte
   > ```

   \endgroup

\fonction{set-context} crée le contexte, il sera placé dans \fonction{\$HOME/.kube/config}  
mais c'est \fonction{use-context} qui le fera utiliser.

Les contextes peuvent aussi être utilisés par des utilisateurs (`--users`) ou clusters (`--clusters`) qui s'y authenbtifient.

