---
title: "u11. Kubernetes : DNS"
subtitle: ""

keywords:
   - kubernetes
   - DNS

author: Marc Le Bihan
---

---

\definition{DNS}: nomme les services, et participe à l'équilibrage de charge, il gère les réplicas

   \code

   > ```bash
   > kubectl get deployments --namespace=kube-system
   > ```
   >
   > \footnotesize
   > ```bash
   > NAME      READY   UP-TO-DATE   AVAILABLE   AGE
   > coredns   1/1     1            1           349d
   > ```
   > 
   > ```bash
   > kubectl get services --namespace=kube-system
   > ```
   > \footnotesize
   > ```bash
   > NAME       TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)                  AGE
   > kube-dns   ClusterIP   10.96.0.10   <none>        53/UDP,53/TCP,9153/TCP   153d
   > ```

   \endgroup

