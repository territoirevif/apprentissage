---
title: "h01. Installation de Helm, généralités"
subtitle: ""
author: Marc Le Bihan
keywords:
- repository
- helm
- kubernetes
- chart
---

## 1) L'installation de Helm

Il faut que kubernetes soit actif :  
\demitab ne pas oublier un \fonction{minikube start} ou équivalent

et installer _Helm 3_ par :

   \code

   > ```bash
   > sudo snap install helm --classic
   > helm version
   > ```

   > ```txt
   > version.BuildInfo{Version:"v3.7.0", GitCommit:"eeac83883cb4014fe60267ec6373570374ce770b", GitTreeState:"clean", GoVersion:"go1.16.8"}
   > ```

   \endgroup

## 2) Les repository de charts et la recherche de charts

### a) Ajout d'un repository de dépôts de charts

   \code

   > ```bash
   > helm repo add bitnami https://charts.bitnami.com/bitnami
   > helm repo list
   > ```

   > ```txt
   > NAME   	URL                                 
   > bitnami	https://charts.bitnami.com/bitnami 
   > ```

   \endgroup

### b) Chercher un dépôt de charts

   \code

   > ```bash
   > helm search repo drupal 
   > ```

   > ```txt
   > NAME          	CHART VERSION	APP VERSION	DESCRIPTION  
   > bitnami/drupal	12.3.5       	9.4.5      	Drupal is one of the most versatile open source...
   > ```

   \endgroup

\bigbreak

Ajouter l'option \fonction{--versions} listera toutes les versions disponbiles du composant

   \demitab sinon, seule la plus récente sera affichée.

\bigbreak

Il y a deux notions de versions différentes : 

   - celle de l'application (APP)
   - et celle du chart proposé par Helm

