---
title: "h02. Installation et mise à jour d'une application par Helm"
subtitle: ""
category: déploiement
author: Marc Le Bihan
keywords:
- helm
- pod
- namespace
- install
- uninstall
- chart
- kubectl
- release
---

## 1) Installer une application avec Helm

Helm va :

   1. Charger un \definition{chart}
   2. Compiler un modèle (\definition{template})
   3. Passer des valeurs de configuration dedans, parfois en interrogeant Kubernetes pour les obtenir
   4. Générer un yaml, et le faire vérifier par Kubernetes qui doit en tirer des objets
   5. Alors, passer ces objets à Kubernetes pour les faire installer

### a) Installation

   \code

   > ```bash
   > helm install mysite bitnami/drupal
   > ```

   \endgroup

\bigbreak

Il répond qu'il prépare l'installation :

   \code

   > ```txt
   > NAME: mysite
   > LAST DEPLOYED: Fri Aug 26 06:17:55 2022
   > NAMESPACE: default
   > STATUS: deployed
   > REVISION: 1
   > TEST SUITE: None
   > NOTES:
   > CHART NAME: drupal
   > CHART VERSION: 12.3.5
   > APP VERSION: 9.4.5** Please be patient while the chart is being deployed **
   > ```

   \endgroup

\bigbreak

\underline{Remarque :}

   > l'option \fonction{--dry-run} permet de simuler l'opération d'installation.

### b) Observation de l'arrivée des pods, liste...

Et un peu plus tard, on peut voir des pods arriver sur Kubernetes :

   \code

   > ```bash
   > kubectl get pods
   > ```

\bigbreak

   > ```txt
   > NAME                            READY   STATUS    RESTARTS   AGE
   > mysite-drupal-d6d99c65f-lvtxf   1/1     Running   0          2m48s
   > mysite-mariadb-0                1/1     Running   0          2m48s
   > ```

   \endgroup

### c) Préciser un namespace

Ajouter une option \fonction{--namespace} ou \fonction{-n} va faire cibler ce namespace à Helm pour ses opérations.

On peut spécifier ou overrider les valeurs de configuration durant l'installation :

   - soit en utilisant l'option \fonction{--values values1.yaml [...]}
   - soit directement avec \fonction{--set userName=moi}

\bigbreak

(`helm list` est aussi affecté par les namespaces : 

   - par défaut, il ne présente que le contenu du namespace qui a été __désigné sous Kubernetes comme celui par défaut dans son fichier de configuration__,   
   - il faut lui rajouter une option `--namespace <...>` ou `-n` pour en cibler un en particulier, 
   - ou \fonction{--all-namespaces} pour les lister tous)

### helm list

et par un \methode{helm}{ list} : 

   \code

   > ```txt
   > NAME  	NAMESPACE	REVISION	UPDATED                                 	STATUS  	CHART        	APP VERSION
   > mysite	default  	1       	2022-08-26 06:17:55.234147337 +0200 CEST	deployed	drupal-12.3.5	9.4.5
   > ```

   \endgroup

## 3) Désinstaller une application

   \code

   > ```bash
   > helm uninstall mysite
   > ```

   \endgroup

toujours, en précisant le namespace, si c'est en son sein qu'on veut spécialement viser l'application.

## 2) Les mises à jour de charts

### a) Charts et Installations

Sous Helm, la notion de mise à jour (\definition{upgrade}) concerne deux parties :

   - la version du chart,
   - la configuration de l'installation

et l'ensemble est appelé \definition{release}.

\bigbreak

Par un \fonction{helm install} on installe, la première fois, implicitement un chart.

On peut, plus tard, régler un élément de sa configuration avec l'option \fonction{--set}, si on le désire :

   \code

   > ```bash
   > helm upgrade --set ingress-enabled=true  # Activer ingress
   > ```

   \endgroup

\bigbreak

tandis qu'un \fonction{helm repo upgrade...}, lui,  
   \demitab met à jour le chart, c'est à dire le procédé d'installation lui-même,  
   \demitab et alors, dans son sillage, un \fonction{helm upgrade...} peut faire évoluer la configuration.

### b) Attention à bien passer les valeurs de configuration !

Pour faire évoluer une configuration par rapport à celle par défaut proposée dans un chart,  
l'option \fonction{--values <fichier.yaml>} peut accompagner une commande \fonction{helm install}

mais \rouge{il ne faut pas l'oublier non plus} lors d'un  
\fonction{helm upgrade monApplication --values maConfig.yaml}

au risque que les valeurs configurées \rouge{reprennent leurs anciennes valeurs par défaut !}

