---
title: "e01. ELK: son emploi"
subtitle: "et comment démarrer ses composants"

abstract : |  
   Le démarrage d'Elasticsearch, logstash, kibana, filebeat...

keywords:
  - ELK
  - Elastic, Elasticsearch
  - Kibana
  - Logstash
  - Filebeats
  - Metricbeats
  - Port 9200 Elastic
  - Port 9300 Cluster ELK
  - Port 5601 Kibana
  - Port 5044 Filebeats échange avec Logstash
  - Shards
  - Replicas

author: Marc Le Bihan
---

---

# I) Le démarrage d'ELK

   \code

   > ```bash
   > # Démarrer elasticsearch
   > sudo systemctl start elasticsearch
   >
   > # en cas de mise à jour: le plugin ingest doit être réinstallé
   > sudo /usr/share/elasticsearch/bin/elasticsearch-plugin remove ingest-attachment
   > sudo /usr/share/elasticsearch/bin/elasticsearch-plugin install ingest-attachment
   > 
   > # Kibana
   > sudo systemctl start kibana
   > 
   > # Logstash
   > sudo systemctl start logstash
   > 
   > # Filebeat
   > sudo systemctl start filebeat
   > ```

   \endgroup

## 1) Quels emplois ?

Pourquoi emploie-t-on la suite ELK ?  

   - Collecte d'information
   - Collecte de métriques
   - Réaction à des évènements
   - Détections de sécurité

### Le principe de l'analyse 

   - Index/Sharding/Replicas
   
      - Shards : répartition d'un index sur plusieurs serveurs
      - Replicas : copies d'un shard, pour redondance
   
   - Lucene
   - API Rest

## 2) Quels outils ?

\definition{Elasticsearch}:

   > [local : http://localhost:9200](http://localhost:9200)  
   > Cluster ELK : port 9300  

\definition{Logstash} : un Extract Transform and Load (ETL) aux multiples plugins d'entrée

   - input
   - filter (re-travail des données), grok
   - output

\definition{Kibana} : dashboard de visualisation et requêtes

   - Visualisation de requêtes
   - Dashboard

   > [Kibana local : http://localhost:5601](http://localhost:5601)  
   > C'est un concurrent de Grafana

\definition{Filebeat}, \definition{Metricbeat} : les acquéreurs de métriques

\definition{Filebeat}:

   > Communique par le port \donnee{5044} avec logstash

---

# Références et bibliographie

@xavki
