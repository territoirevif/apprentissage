---
title: "e05. Filebeat"
subtitle: ""
author: Marc Le Bihan
---

# I) Filebeat en injecteur de logs (simple)

Il sait déjà parser et injecter les logs les plus connues.  
On peut retrouver les autres _beats_ dans la page Home de _Kibana_ rubrique "_Metrics_".

\fonction{sudo filebeat modules list} va lister les modules que _Filebeat_ qu'on peut activer sans paramétrage additionnel :

   \code

   > ```bash
   > filebeat modules enable nginx
   > ``` 

   \endgroup

où il installera également un index dans _Elasticseach_ et un dashboard dans _Kibana_.

Pour prendre en charge une log Java (avec les stacktraces contenant des Caused By à des niveaux multiples), déclarer :

   \code

   > ```yaml
   > multiline.type: pattern
   > multiline.pattern: '^[[:space:]]+(at|\.{3})[[:space:]]+\b|^Caused by:'
   > multiline.negate: false
   > multiline.match: after
   > ```

   \endgroup

# II) Filebeat, en injecteur de logs vers logstash plutôt qu'Elastic

Il permet une utilisation du \definition{Grok}.

Editer le fichier \fonction{/etc/filebeat/filebeat.yml}

### a) section filebeat

Pour parser des logs __Spring-boot__, par exemple :

   \code

   \underline{filebeat.yml :}
   \nopagebreak

   > ```yaml
   > # filestream is an input for collecting log messages from files.
   > - type: filestream
   > 
   > # Unique ID among all inputs, an ID is required.
   > id: ecoemploi-spring-boot-id
   > 
   > # Change to true to enable this input configuration.
   > enabled: true
   > 
   > # Paths that should be crawled and fetched. Glob based paths.
   > paths:
   >   - /home/lebihan/dev/Java/comptes-france/metier-et-gestion/dev/ApplicationMetierEtGestion/spark*.log
   >   #- c:\programdata\elasticsearch\logs\*
   ```

   \endgroup

Pour un essai avec apache2 :

   \code

   > ```yaml
   > - type: filestream
   >   id: apache
   >   enabled: true
   >   paths:
   >     - /var/log/apache2/access*.log
   > ```

   \endgroup

### b) Activer la section Kibana

Activer la liaison filebeat $\to$ kibana
en décommantant `host:`

   \code

   > ```yaml
   > setup.kibana:
   > # Array of hosts to connect to.
   > hosts: ["localhost:5601"]
   > ```

   \endgroup

### c) Désactiver la section elasticsearch

Désactiver la liaison filebeat $\to$ elasticsearch, car on veut aller vers logstash
en commentant `#output.elasticsearch`

   \code

   > ```yaml
   > #output.elasticsearch:
   > #  Array of hosts to connect to.
   > #  hosts: ["localhost:9200"]
   > ```

   \endgroup

### d) Activer la section logstash

En décommentant `output.logstash` et `hosts`.

   \code

   > ```yaml
   > output.logstash:
   >    # The Logstash hosts
   >    hosts: ["localhost:5044"]
   > ``` 

   \endgroup

## Editer un fichier de configuration logstash

\danger{Attention :} faire plusieurs fichiers logstash dans `conf.d`, par exemple, peut conduire a déclarer plusieurs fois le port 5044, ce qui va faire un conflit, et l'un des deux fichiers de configuration sera écarté.

Par exemple, pour apache 2.

Aller dans \fonction{/etc/logstash/conf.d} et créer un fichier `apache2.conf` avec ce contenu :

   \code

   \underline{apache2.conf :}
   \nopagebreak

   > ```yaml
   > input {
   >    beats {
   >       port => 5044
   >   }
   > }
   > 
   > filter {
   >    grok {
   >       patterns_dir => "/etc/logstash/pattern"
   >       match => { "message" => "%{IP:client} %{WORD:method} %{URIPATHPARAM:request} %{NUMBER:bytes} %{NUMBER:duration}" }
   >    }
   > }
   > 
   > output {
   >     stdout { codec => rubydebug }
   > 
   >     elasticsearch {
   >         hosts => ["localhost:9200"]
   >         index => "apache2-%{+YYYY.MM.dd}"
   >     }
   > }
   > ```

   \endgroup

Le nom de l'index elastisearch (dernière ligne) doit être en minuscules.

### d) Vérifier les index

Dans Index Management, l'index devrait arriver.

Dans le Management Elasticsearch, __Sous la rubrique Kibana__, faire ou refaire un Index Pattern, avec un `@timestamp` en champ d'horodatage.

---

# Références et bibliographie

@xavki
