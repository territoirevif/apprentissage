---
title: "e02. Installation et paramétrage d'ELK"
subtitle: ""
author: Marc Le Bihan

abstract: |  
  Réglage de la mémoire vive, des noeuds à écouter. 

keywords:
  - plugin ingest
  - mémoire vive
  - noeuds
  - logstash
  - kibana
  - filebeat
  - node
  - noeud
---

## 1) Installation d'Elasticsearch

   \code

   > ```bash
   > wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
   > 
   > echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee /etc/apt/sources.list.d/elastic-7.x.list
   > 
   > sudo apt-get update && sudo apt-get install elasticsearch
   > sudo systemctl start elasticsearch.service
   > 
   > # Pour contrôle :
   > sudo journalctl --unit elasticsearch
   > curl -X GET "localhost:9200/?pretty"
   > ```

   \endgroup

\bigbreak

Si elasticsearch ne redémarre pas sur un problème de Plugin Ingest pas à jour, il faut taper ces deux commandes pour le désinstaller et le réinstaller :

   \begingroup \footnotesize

   > ```bash
   > sudo /usr/share/elasticsearch/bin/elasticsearch-plugin remove ingest-attachment
   > sudo /usr/share/elasticsearch/bin/elasticsearch-plugin install ingest-attachment
   > ```

   \endgroup

Un cluster de noeuds Elastic communique sur le port 9300 (auto discovery)

Les noeuds sont de type :

   - master
   - data
   - client

Un index est "comme" une instance de base de données, pour Elastic

## 2) Paramétrage

Pour régler en particulier :

   - la consommation mémoire,
   - le port d'écoute,
   - les noeuds qui composent le master,
   - et lister les machines à découvrir

Sur une image `Docker`, les fichiers de configuration sont à cet emplacement :  
\fonction{/usr/share/elasticsearch/config/}

__Noeuds__ : master, data, client...

### a) Mémoire vive maximale

__\textcolor{red}{Il faut la régler}__ : faute de quoi, _Elasticsearch_ calculera sa consommation d'après l'espace libre qu'il trouvera sur le système !

   \code

   \underline{/etc/elasticsearch/jvm.options :}

   > ```
   > -Xms2g
   > -Xmx2g
   > ```

   \endgroup

### b) Déclarer les nodes à découvrir

Ecouter tous les entrants \fonction{(0.0.0.0)} pour un Docker, mais \fonction{localhost} pour une machine personnelle.

   \code

   \underline{/etc/elasticsearch/elasticsearch.yml} :

   > ```yaml
   > # Décommenter node name
   > node.name=node-1
   > 
   > # Mettre le network.host à 0.0.0.0 pour écouter toutes les adresses.
   > network.host: 0.0.0.0
   >
   > # Définir les IP permettant de nous découvrir (faire un `sudo ifconfig` pour découvrir son IP)
   > discovery.seed_hosts=["192.168.0.25", "127.0.0.1"]
   > 
   > # Réduire le cluster à notre seule machine.
   > cluster.initial_master_nodes: ["node-1"]
   > ``` 

   \endgroup

Redémarrer _Elasticsearch_ par un \fonction{sudo systemctl restart elasticsearch}.  

Un test par :  
\fonction{curl http://localhost:9200} doit renvoyer une information montrant qu'il fonctionne.

### c) Test de bon fonctionnement

   \code

   > ```bash
   > curl http://172.20.0.3:9200
   > ```
   
   > ```json
   > {
   >   "name" : "es01",
   >   "cluster_name" : "es-docker-cluster",
   >   "cluster_uuid" : "8QKY_9nrRbyTplLAXMbM8g",
   >   "version" : {
   >     "number" : "7.6.2",
   >     "build_flavor" : "default",
   >     "build_type" : "docker",
   >     "build_hash" : "ef48eb35cf30adf4db14086e8aabd07ef6fb113f",
   >     "build_date" : "2020-03-26T06:34:37.794943Z",
   >     "build_snapshot" : false,
   >     "lucene_version" : "8.4.0",
   >     "minimum_wire_compatibility_version" : "6.8.0",
   >     "minimum_index_compatibility_version" : "6.0.0-beta1"
   >   },
   >   "tagline" : "You Know, for Search"
   > }
   > ```

   \endgroup

ou bien :

   \code

   > ```bash
   > curl -X GET "http://localhost:9200/_cat/nodes?v&pretty"
   > ```

   \endgroup

va donner l'état du cluster : 

   \begingroup \scriptsize

   > ```txt
   > ip         heap.percent ram.percent cpu load_1m load_5m load_15m node.role   master name
172.23.0.1           25          50  16    4.16    4.47     3.90 cdfhilmrstw *      node-1
   > ```

   \endgroup

## 2) Logstash

### a) Installation

(l'ajout de la source dans \fonction{elastic-7.x.list} ayant été faite et les packages \fonction{apt-transport-https} et autres déjà installés)

   \code

   > ```bash
   > sudo apt-get update && sudo apt-get install logstash
   > sudo systemctl start logstash
   > ```

   \endgroup

### b) Configuration

dans \fonction{/etc/logstash/conf.d} principalement,  
ainsi que \fonction{/etc/logstash/logstash.yml}

## 3) Kibana

   \code

   > ```bash
   > sudo apt-get update && sudo apt-get install kibana
   > sudo systemctl start kibana
   > ```

   \endgroup

   Il rend la main très rapidement, mais il faut attendre quelques secondes qu'il soit réellement prêt.

## 4) Filebeats

### a) Installation

   \code

   > ```bash
   > sudo apt-get update && sudo apt-get install filebeat
   > sudo systemctl start filebeat
   > ```

   \endgroup

### b) Configuration

Il faut lui présenter _Kibana_ : 

   \code

   \underline{/etc/filebeat/filebeat.yml : }

   > ```yaml
   > setup.kibana:
   >   host: "localhost:5601"
   > ```

   \endgroup

_Logstash_ y a aussi une section, mais il n'est pas nécessaire de la décommenter immédiatement.

---

# Références et bibliographie

@xavki
