---
title: "e04. Kibana"
subtitle: ""
author: Marc Le Bihan
---

On injecte des données dans _Elastic_ par _Logstash_ pour les visualiser dans _Kibana_.

## Kibana : visualisation et requêtes, dashboard

   - Visualisation de data, par le menu _Discover_
   - Aide à l'installation d'outils de collecte de métriques

\bigbreak

_Kibana_ liste les indexs qu'il connait dans _Index Management_.

Mais souvent ce sont plusieurs mêmes fichiers avec des dates différentes : pour éviter d'en avoir cent à l'écran, on passe par un __Index Pattern__.

\bigbreak

   1. Pour indiquer le suffixe de ces fichiers, décrire leur nom

      \code
   
      > ```bash
      > # Exemple pour un fichier nginx-2021-02-17 :
      > nginx-*
      > ```

      \endgroup
   
   2. Indiquer dedans quel champ sert d'horodatage 
   
      \code

      > ```
      > @timestamp
      > ```

      \endgroup

---

# Références et bibliographie

@xavki
