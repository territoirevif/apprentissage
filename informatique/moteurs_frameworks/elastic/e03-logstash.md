---
title: "e03. Analyse de logs par logstash"
subtitle: ""
author: Marc Le Bihan
keywords:
- grok
- input
- filter
- output
- variabiliser une entrée
- herokuapp
- expression régulière
- logstash
- elastic
---

_Logstash_ est la brique \mauve{Extract Transform Load (ETL)} d'_Elasticsearch_, qui fonctionne sur un circuit Input, Filter, Output :

   - __Input__ : d'où prendre la donnée
   - __Filter__ : comment la retravailler
   - __Output__ : où l'injecter (souvent dans Elasticseach)

\bigbreak

Il s'agit de plugins d'entrée : \mauve{postgres}, \mauve{nginx}...

## 1) Le grok, pour variabiliser une entrée

Un \definition{grok} sert à __variabiliser__ une entrée (log, etc..) lue  

   > [Debugger d'expressions _Grok_](https://grokdebug.herokuapp.com/)

On utilise des \definition{patterns} : des __expressions régulières__  
   \demitab permettant la capture d'éléments pour les placer dans des variables.

En créant répertoire \fonction{pattern} dans \mauve{/etc/logstash}, on rend ces patterns réutilisables. 

   \code
   
   > ```bash
   > mkdir /etc/logstash/pattern
   > chmod 755 -R /etc/logstash/pattern
   > ```

   \endgroup

\bigbreak

Puis, dans ce répertoire \mauve{/etc/logstash/pattern}, l'on crée \mauve{nginx} :

   \code

   > ```
   > NGUSERNAME [a-zA-Z\.\@\-\+_%]+
   > NGUSER %{NGUSERNAME}
   > ```

   \endgroup
  
\mauve{\%\{NGUSER:ident\}} signifie :  
   \demitab _Réutiliser la regex `NGUSER` pour injecter une valeur dans la variable nommée \mauve{ident}._

\bigbreak

\underline{Remarque :}  

   > Dans la page Home de _Kibana_, rubrique "_logs_",  
   > se trouvent des exemples de parsage de logs pour _Apache_, _Kafka_, etc.

## 2) Un fichier de configuration Logstash type :

   - \bleu{input} : va demander l'analyse à partir du début du fichier de log

      - `/dev/null` signifie : si `logstash` redémarre, il ne reprendra pas d'où il s'est arrêté (non historisé).

   - \bleu{filter} : dans une section `grok` définit le répertoire pattern, et définit la regex d'analyse.
   - \bleu{output} : va envoyer les données parsées à elasticsearch, en donnant le nom de l'index cible.

\bigbreak

Par exemple, ce fichier peut aller dans \fonction{/etc/logstash/conf.d/nginx.conf} :

   \code

   > ```logstash
   > input {
   >    file {
   >       path => "/var/log/nginx/access.log"
   >       start_position => "beginning"
   >       sincedb_path => "/dev/null"
   >    }
   > }
   > 
   > filter {
   >    grok {
   >       patterns_dir => "/etc/logstash/pattern"
   > 
   >       match => {"message" => "%{IPORHOST:clientip} %{NGUSER:ident} %{NGUSER:auth} \[%{HTTPDATE:timestamp}\] \"%{WORD:verb} %{URIPATHPARAM:request} HTTP/%{NUMBER:httpversion}\" %{NUMBER:response}" }
   >    }
   > }
   > 
   > output {
   >    elasticsearch {
   >       hosts => ["127.0.0.1:9200"]
   >       index => "nginx-%{+YYYY.MM.dd}"
   >    }
   > }
   > ``` 

   \endgroup

---

# Références et bibliographie

@xavki
