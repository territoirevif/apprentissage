---
title: "e020. Analyse textuelle"
subtitle: "Les analyzers et l'ingester"
category: analyse textuelle

keywords:
- TF
- term frequency
- IDF
- inverse difference frequency
- mot
- ponctuation
- token
- ingester

author: Marc Le Bihan
---

Indexation des mots. La qualité de la recherche vient par l'emploi de deux indicateurs :

   - __TF__ (Term Frequency) : fréquence des mots.
   
   - __IDF__ (Inverse Difference Frequency) : moins un mot est commun, plus il a de valeur dans la recherche.

# I) Les analyzers

exemples à émettre par :

   - `curl`
   - Kibana [Management $\to$ Dev Tools (http://localhost:5601/app/dev_tools#/console en 8.x)](http://localhost:5601/app/dev_tools#/console)

## 1) analyzer: keyword

Chaque élément (mot, signe de ponctuation...) est un token possible.

   \code

   > ```json
   > POST _analyze 
   > {
   >   "analyzer": "keyword",
   >   "text": ["fox", "The quick brown FOX."]
   > }
   > ```

   \endgroup

\bigbreak
Répond :\nopagebreak 

   \code

   > ```json
   > {
   >   "tokens" : [
   >     {
   >       "token" : "fox",
   >       "start_offset" : 0,
   >       "end_offset" : 3,
   >       "type" : "word",
   >       "position" : 0
   >     },
   >     {
   >       "token" : "The quick brown FOX.",
   >       "start_offset" : 4,
   >       "end_offset" : 24,
   >       "type" : "word",
   >       "position" : 1
   >     }
   >   ]
   > }
   > ```

   \endgroup

## 2) analyzer: whitespace

Si l'on choisit \fonction{"analyzer": "whitespace"}, il y aura séparation en mots, mais pas de détection :

   - que les mots en majuscules ou minuscules sont les mêmes
   - de ponctuation

\bigbreak

   \code

   > ```json   
   > {
   >   "tokens" : [
   >     {
   >       "token" : "fox",
   >       "start_offset" : 0,
   >       "end_offset" : 3,
   >       "type" : "word",
   >       "position" : 0
   >     },
  > 
   >     [...]
   >     "token" : "The",
   >     "token" : "quick",
   >     "token" : "brown",
   >     "token" : "FOX."
   >   ]
   > }
   > ```

   \endgroup

## 3) analyzer: standard

Là, il va bien trouver les bons mots :

   \code

   > ```json
   > {
   >   "tokens" : [
   >     {
   >       "token" : "fox",
   >       "start_offset" : 0,
   >       "end_offset" : 3,
   >       "type" : "<ALPHANUM>",
   >       "position" : 0
   >     },
  >
   >     [...]
   >     "token" : "the",
   >     "token" : "quick",
   >     "token" : "brown",
   >     "token" : "fox",
   >   ]
   > }
   > ```

   \endgroup

## 4) analyzer: french

\tab Il est important de le choisir pour un texte en français, sinon les "_un_", "_et_", etc. seront mis en exergue, et les caractères accentués feront de nouveaux mots, ce que l'on ne désire pas.

\bigbreak

Avec `french`: \nopagebreak

   \code
   
   > ```json
   > POST _analyze
   > {
   >   "analyzer": "french",
   >   "text": "Un éléphant et un elephant."
   > } 
   > ```
   
   \endgroup

\bigbreak

répond correctement : \nopagebreak

   \code
   
   > ```json
   > {
   >   "tokens" : [
   >     {
   >       "token" : "elephant",
   >       "start_offset" : 3,
   >       "end_offset" : 11,
   >       "type" : "<ALPHANUM>",
   >       "position" : 1
   >     },
   >
   >     [...]
   >     "token" : "elephant",
  >   ]
   > }
   > ```
   
   \endgroup

\bigbreak
mais sans (avec `standard` ici) il énumèrera : `{'un', 'éléphant', 'et', 'un', 'elephant'}`

\demitab au lieu de : `{'elephant', 'elephant'}` avec `french`.

# II) L'ingester

Il permet d'indexer directement le contenu d'un fichier.

C'est un plugin d'Elastic, qui réclame souvent d'être réinstallé à chaque mise à jour.

   > ```bash
   > (cd /usr/share/elasticsearch/bin/ || exit 1; sudo ./elasticsearch-plugin remove ingest-attachment --silent)
   > (cd /usr/share/elasticsearch/bin/ || exit 1; sudo ./elasticsearch-plugin install ingest-attachment --silent --batch)
   > ```

\bigbreak

Et l'on fait prendre en charge à Elastic un fichier donné (dans un index créé auparavant) ainsi : 

   \begingroup \small

   > \bleu{source} : nom du fichier que l'on veut indexer dans Elastic  
   > \bleu{host} : URL Elastic  
   > \bleu{user} : user Elastic (la version 8.x réclame une authentification)  
   > \bleu{pwd} : mot de passe  
   > \bleu{index} : index de destination

   \endgroup

   \code

   > ```bash
   > #!/bin/bash
   > export source=$1
   > 
   > # Le paramètre source doit être alimenté
   > if [ -z "$source" ]; then
   >    echo "Le fichier à indexer dans Elastic est attendu en paramètre." >&2
   >    exit 1
   > fi
   > 
   > host="http://localhost:9200"
   > user="elastic"
   > pwd="monPassword"
   > 
   > index=apprentissage
   > entree=$(basename "${source%.*}")
   > json_file=$(mktemp)
   > cur_url="$host/$index/_doc/$entree?pipeline=attachment"
   > 
   > echo '{"data"  : "'"$( base64 "$source" -w 0    )"'"}' >"$json_file"
   > # echo "transfert via $json_file vers $cur_url"
   > 
   > if ! ingest=$(curl -s -X PUT -H "Content-Type: application/json" -u "$user:$pwd" -d "@$json_file" "$cur_url"); then
   >   echo "Echec de l'ingestion dans Elastic de $source : $ingest" >&2
   >   exit $?
   > fi
   > 
   > rm "$json_file"
   > echo "$source indexé dans Elastic"
   > ```

   \endgroup

\bigbreak

\underline{Remaque :} La difficulté pour la commande `curl` est qu'il lui faut convertir le contenu fichier en `base64`, le placer dans un champ `json` nommé `data` qui, parce qu'il va se révéler trop long pour simple ligne de commande, va devoir être mis dans un fichier temporaire et désigné par un `-d @json_file` dans la commande `curl`.
