---
title: "e022. Les recherches textuelles, vectorielles et hybrides"
subtitle: "et l'amélioration de leur acuité"
category: analyse textuelle
author: Marc Le Bihan
---

## 1) La recherche textuelle

La recherche textuelle a émergé avec _Tex_, elle est basée sur l'algorithme `BM25` d'Elastic.

Elle est la base de toute recherche, "de la pyramide", et est toujours utilisée, même en complément de la recherche vectorielle, pour la préciser.

Mais elle n'est souvent pas suffisante...  
Particulièrement, quand l'on ne veut pas se limiter à un critère de recherche qui soit juste un mot ou une expression.

## 2) La recherche vectorielle

La recherche vectorielle vise en particulier la pertinence sémantique d'une recherche.

Les termes de la recherche sont entrés sous forme de nombres, et la recherche se fait :

   - par distance numérique entre vecteurs
   - ou par similarité entre eux

Son atout est qu'elle peut trouver des résultats à cette question : "_Quelle vitesse doit avoir ma connexion Internet ?_"  
comme : "_vitesse de connexion requise_", que la recherche textuelle n'obtiendrait pas.

Elle fonctionne avec le texte, les images, l'audio, les données non structurées...  
_Elastic_ en fait grand usage, avec des vecteurs clairsemés (_sparse_) ou denses.

Il y a des helpers proposés pour les préparer (surtout pour la langue anglaise).

Mais elle a les limitations de la recherche textuelle : 

   - Configuration des synonymes
   - Inadéquation du vocabulaire

Elle est particulièrement faite pour les recherches qui ont un contexte.

## 3) La recherche hybride

La recherche hybride donne des résultats encore meilleurs.

score de recherche textuelle ET score recherche vectorielle $\rightarrow$ combinaison linéaire  
(implémenté par Elastic sous le nom de \definition{Reciprocal Rank Fusion (RFF)}).

\demitab $\bleu{d}$ un document de \bleu{D} l'ensemble des documents  
\demitab $\bleu{r}$ un classement parmi \bleu{R} l'ensemble des classements issus des permutations sur $1..|D|$  
\demitab $\bleu{k}$ typiquement fixé à $60$
$$
\rouge{\text{RFFscore}}(\bleu{d} \in \bleu{D})) = \sum\limits_{\bleu{r} \in \mathbb{\bleu{R}}}\frac{1}{\bleu{k} + \bleu{r(d)}}
$$

Mais quand elle peine malgré tout, il faut améliorer ses performances.

# Bibliographie et références

Boosting Search Relevance: Semantic Reranking with Retrievers by Diana Jourdan, Elastic

https://www.youtube.com/watch?v=4QSxXi4ScH8&list=PL_mJOmq4zsHY3Q4uny7NIpTTaq3UK5qfU

(6'00 sur 24')
