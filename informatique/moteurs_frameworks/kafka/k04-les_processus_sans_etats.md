---
title: "k04. Les processus sans états"
subtitle: ""
author: Marc Le Bihan

abstract: |  
   les opérateurs applicables aux flux sans état.

keywords:
   - filtrage
   - ajout et suppression de champs
   - changement de clef de messages
   - branchement de flux
   - fusion de flux
   - transformation d'enregistrements en une ou plusieurs sorties
   - Enrichissement d'enregistrements

geometry: margin=2cm
fontsize: 12pt
output: pdf
classoption: fleqn
urlcolor: blue
---

Dans une application \definition{sans état} chaque évènement est traité indépendamment des autres,  
et les flux (_streams_) suffisent.  

   - il n'y a pas à se rappeler des évènements passés.
   - l'oparateur \fonction{filter} peut-être librement utilisé : il ne fait qu'observer l'enregistrement courant.

\bigbreak

Dans une application \definition{avec état}, il faut se rappeler d'un ou plusieurs évènements du passé à différentes étapes de la topologie, en général avec un but :

   - d'agrégation de données,
   - de fenêtrage,
   - jonction de flux
   - l'emploi des opérateurs \fonction{count} rendent stateful une application.

\bigbreak

elles sont plus complexes, car elles réclament de suivre des états ou des données additionnelles.

# I) Une application d'examen de tweets

## 0) La classe main de démarrage de l'application

   \underline{App.java :} elle crée une topologie `Crypto` sur un flux simple de consumer group `dev`.

   \nopagebreak
   
   \code

   > ```java
   > public class App {
   >   public static void main(String[] args) {
   >     Topology topology = CryptoTopology.build();
   > 
   >     // set the required properties for running Kafka Streams
   >     Properties config = new Properties();
   >     config.put(StreamsConfig.APPLICATION_ID_CONFIG, "dev"); // Son ID de consummer group.
   >     config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:29092");
   > 
   >     // build the topology and start streaming!
   >     KafkaStreams streams = new KafkaStreams(topology, config);
   >     Runtime.getRuntime().addShutdownHook(new Thread(streams::close));
   > 
   >     System.out.println("Starting Twitter streams");
   >     streams.start();
   >   }
   > }
   > ```

   \endgroup

## 1) Choisir et convertir les tweets

### a) Trouver ceux qui ont #bitcoin ou #etherum dedans

Comme source.

### b) ils sont en json : il faut les désérialiser

Par défaut, _Kafka_ considère toutes les données comme des tableaux d'octets.  
Quand on veut qu'il les voie autrement, il faut le préparer.

\bigbreak

\underline{Remarque :}

En développement, \fonction{stream.print} est commode pour la mise au point.

   \code

   > ```java
   >  KStream<byte[], byte[]> stream = builder.stream("tweets");
   >  stream.print(Printed.<byte[], byte[]>toSysOut().withLabel("tweets-stream"));
   > ```

   \endgroup

\bigbreak

\begingroup \cadreAvantage

> Parce qu'il transfère toujours en `byte[]`  
> et que __c'est le client qui désérialise__ - pas _Kafka_ -  
> il transfère rapidement des données d'un système à un autre $\rightarrow$ _Kafka_ ne convertit rien.

\endgroup

\bigbreak

Mais les `byte[]` ne sont pas très manipulables, et l'on préfère :

   - désérialiser en objet une entrée de flux,
   - la re-sérialiser lors des réémissions vers un topic.

\bigbreak

\definition{Sérialisateur} et \definition{désérialisateur} sont rassemblés dans une classe nommée  
   \demitab \definition{Serdes} (_= serialisation/déserialisation_)

   \demitab déjà disponible pour les types usuels :  
      \tab `String`, `Integer`, `Double`, `Bytes`, `ByteBuffer`, `ByteArray`, `UUID`, `Void`

mais il n'y en a pas pour :

   > `JSON`, `Avro`, `Protobuf`...

et il faut les implémenter soi-même, par des custom `Serdes`.  
pour JSON, utiliser une API : `Gson` de _Google_, ou _Jackson_.

### \underline{Création d'un DTO pour la désérialisation :}

Sous _Kafka_, l'objet est stocké sous forme JSON, XML... : ce qu'on a choisi à l'émission.

  $\rightarrow$ l'on crée une classe POJO aux variables membres et getter/setters accordés.  
Ici, avec des annotations JSON de Google, par exemple.

\bigbreak

   \underline{Tweet.java :}

   \nopagebreak

   \code
   
   > ```java
   > public class Tweet {
   >   @SerializedName("CreatedAt")
   >   private Long createdAt;
   > 
   >   @SerializedName("Id")
   >   private Long id;
   > 
   >   @SerializedName("Lang")
   >   private String lang;
   > 
   >   @SerializedName("Retweet")
   >   private Boolean retweet;
   > 
   >   @SerializedName("Text")
   >   private String text;
   > 
   >   public Long getCreatedAt() {
   >     return this.createdAt;
   >   }
   > 
   >   public void setCreatedAt(Long createdAt) {
   >     this.createdAt = createdAt;
   >   }
   > 
   >   [...]
   > }
   > ```
   
   \endgroup

\bigbreak

Sur la base d'un tweet que l'on reçoit au format JSON, avec cette structure :

   \code

   > ```json
   > {
   >    "CreatedAt":1577933872630,
   >    "Id":10005,
   >    "Text":"Bitcoin has a lot of promise. I'm not too sure about #ethereum",
   >    "Lang":"en",
   >    "Retweet":false,
   >    "Source":"",
   >    "User":{
   >       "Id":"14377870",
   >       "Name":"MagicalPipelines",
   >       "Description":"Learn something magical today.",
   >       "ScreenName":"MagicalPipelines",
   >       "URL":"http://www.magicalpipelines.com",
   >       "FollowersCount":"248247",
   >       "FriendsCount":"16417"
   >    }
   > }
   > ```

   \endgroup

### c) et supprimer les champs inutiles

On exclut les champs dont on sait qu'ils ne serviront pas, pour qu'ils n'encombrent pas, en ne les mentionnant pas dans la classe DTO créée.

### d) L'invocation de la désérialisation proprement dite

C'est une classe implémentant une interface Kafka \fonction{org.apache.kafka.common.serialization.Deserializer}

où l'on redéfinit une méthode  
   \demitab \methode{deserialize}{(String topic, byte[] bytes)}    
qui retournera le DTO voulu

   \underline{TweetDeserializer.java :}

   \nopagebreak

   \code

   > ```java
   > public class TweetDeserializer implements Deserializer<Tweet> {
   >   private Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();
   > 
   >   @Override
   >   public Tweet deserialize(String topic, byte[] bytes) {
   >     // S'il n'y a rien à désérialiser, ne rien renvoyer !
   >     if (bytes == null) 
   >        return null;
   > 
   >     return gson.fromJson(new String(bytes, StandardCharsets.UTF_8), Tweet.class);
   >   }
   > }
   > ```

   \endgroup

\bigbreak

Respectivement, un sérialiseur fera l'opération d'écriture dans le topic, depuis l'objet DTO :

   \underline{TweetSerializer.java :}

   \nopagebreak

   \code

   >  ```java
   > class TweetSerializer implements Serializer<Tweet> {
   >   private Gson gson = new Gson();
   > 
   >   @Override
   >   public byte[] serialize(String topic, Tweet tweet) {
   >     if (tweet == null) return null;
   >     return gson.toJson(tweet).getBytes(StandardCharsets.UTF_8);
   >   }
   > }
   > ```

   \endgroup

\bigbreak

Et une classe de type \fonction{Serdes} associe le sérialiseur et le désérialiseur à l'objet DTO :

   \underline{TweetSerdes.java :}

   \nopagebreak

   \code

   > ```java
   > public class TweetSerdes implements Serde<Tweet> {
   >   @Override
   >   public Serializer<Tweet> serializer() {
   >     return new TweetSerializer();
   >   }
   > 
   >   @Override
   >   public Deserializer<Tweet> deserializer() {
   >     return new TweetDeserializer();
   >   }
   > }
   > 
   > ```

   \endgroup

## 2) Supprimer les re-tweets (filter)

## 3) Transférer ailleurs les tweets qui ne sont pas en anglais

### a) alors ces tweets non anglais, les traduire (map)

### b) et les réintégrer, par fusion (merge), dans ceux anglais

## 4) Chaque tweet peut parler de plusieurs devises : flatMap pour les séparer

## 5) Calculer un "sentiment score" sur chaque devise.

## 6) Sérialiser les topics résultats par Avro et les stocker dans un nouveau topic


# III) Annexe

## 1) Le docker-compose d'installation Kafka pour ce chapitre

   \underline{docker-compose.yml :}

   \code
 
   > ```yaml
   > ---
   > version: '3'
   > 
   > services:
   >   zookeeper:
   >     image: confluentinc/cp-zookeeper:6.0.0
   >     hostname: zookeeper
   >     container_name: zookeeper
   >     ports:
   >       - "32181:32181"
   >     environment:
   >       ZOOKEEPER_CLIENT_PORT: 32181
   >       ZOOKEEPER_TICK_TIME: 2000
   > 
   >   kafka:
   >     image: confluentinc/cp-enterprise-kafka:6.0.0
   >     hostname: kafka
   >     container_name: kafka
   >     depends_on:
   >       - zookeeper
   >     ports:
   >       - "29092:29092"
   >     environment:
   >       KAFKA_BROKER_ID: 1
   >       KAFKA_ZOOKEEPER_CONNECT: 'zookeeper:32181'
   >       KAFKA_LISTENER_SECURITY_PROTOCOL_MAP: PLAINTEXT:PLAINTEXT,PLAINTEXT_HOST:PLAINTEXT
   >       KAFKA_ADVERTISED_LISTENERS: PLAINTEXT://kafka:9092,PLAINTEXT_HOST://localhost:29092
   >       KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR: 1
   >       KAFKA_GROUP_INITIAL_REBALANCE_DELAY_MS: 0
   >       KAFKA_TRANSACTION_STATE_LOG_MIN_ISR: 1
   >       KAFKA_TRANSACTION_STATE_LOG_REPLICATION_FACTOR: 1
   >     working_dir: /data
   >     volumes:
   >     - ./data:/data
   > 
   >   kafka-create-topics:
   >     image: confluentinc/cp-enterprise-kafka:6.0.0
   >     depends_on:
   >       - kafka
   >     hostname: kafka-create-topics
   >     command: ["bash", "./create-topics.sh"]
   >     working_dir: /scripts
   >     volumes:
   >     - ./scripts:/scripts
   > 
   >   schema-registry:
   >     image: confluentinc/cp-schema-registry:6.0.0
   >     depends_on:
   >       - zookeeper
   >       - kafka
   >     environment:
   >       SCHEMA_REGISTRY_HOST_NAME: schema-registry
   >       SCHEMA_REGISTRY_KAFKASTORE_CONNECTION_URL: zookeeper:32181
   >     ports:
   >       - "8081:8081"
   > ```
   
   \endgroup

## 2) create-topics.sh associé

   \underline{create-topic.sh :}

   \code

   > ```bash
   > echo "Waiting for Kafka to come online..."
   > 
   > cub kafka-ready -b kafka:9092 1 20
   > 
   > # create the tweets topic
   > kafka-topics \
   >   --bootstrap-server kafka:9092 \
   >   --topic tweets \
   >   --replication-factor 1 \
   >   --partitions 4 \
   >   --create
   > 
   > # create the crypto-sentiment topic
   > kafka-topics \
   >   --bootstrap-server kafka:9092 \
   >   --topic crypto-sentiment \
   >   --replication-factor 1 \
   >   --partitions 4 \
   >   --create
   > 
   > sleep infinity
   > ```

