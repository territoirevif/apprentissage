---
title: "k02. Kafka: Topics, partitions et groupes de consommateurs"
subtitle: ""
author: Marc Le Bihan

abstract: |  
   Création de topics, alimentation et relecture depuis la console.  
   Les types de topics (Stream, Table...), leurs partitions et groupes de consommateurs.
   \linebreak
   `sudo docker-compose up` puis `sudo docker exec -it kafka bash` dans :  
   `apprentissageDev/mastering_kafka_streams/mastering-kafka-streams-and-ksqldb/chapter-02`  
   \linebreak
   Démarrage de Kafka:  
   ```bash
   zookeeper-server-start.sh /opt/kafka_2.13-3.1.0/config/zookeeper.properties
   kafka-server-start.sh /opt/kafka_2.13-3.1.0/config/server.properties
   ```
  
keywords:
  - topic
  - kafka-topics.sh
  - kafka console-producer
  - kafka console-consumer
  - partition
  - stream
  - table
  - partition
  - offset
---

---

Les \definition{topics} sont des streams nommés. Ils sont soit:

   - \definition{homogènes} (un seul type de donnéee)
   - \definition{hétérogènes} (multiples)

# I) Création de topic

   \code

   > ```bash
   > kafka-topics --bootstrap-server localhost:9092 --create --topic users --partitions 4 --replication-factor 1
   > 
   > kafka-topics --bootstrap-server localhost:9092 --describe --topic users
   > Topic: users	PartitionCount: 4	ReplicationFactor: 1	Configs: 
   > 	Topic: users	Partition: 0	Leader: 1	Replicas: 1	Isr: 1
   > 	Topic: users	Partition: 1	Leader: 1	Replicas: 1	Isr: 1
   > 	Topic: users	Partition: 2	Leader: 1	Replicas: 1	Isr: 1
   > 	Topic: users	Partition: 3	Leader: 1	Replicas: 1	Isr: 1
   > ```

   \endgroup

ou

   \code

   > ```bash
   >  kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic poids_garmin_brut
   > ```

   \endgroup

## 1) Alimenter un topic depuis la console

### a) Par saisie manuelle

La commande qui suit va permettre l'entrée de données directement dans un topic.

   \code

   > ```bash
   > kafka-console-producer --bootstrap-server localhost:9092 --topic users --property key.separator=, property parse.key=true
   > >1,mitch
   > >2,elyse
   > >3,isabelle
   > >4,sammy
   > ```

   \endgroup

### b) Par lecture d'un fichier CSV

   \code

   > ```bash
   > kafka-console-producer.sh --broker-list localhost:9092 --topic poids_garmin_brut < "Poids.csv"
   > ```

   \endgroup

## 2) Lecture d'un topic

La commande qui suit va permettre la lecture du flux:

   \code

   > ```bash
   > kafka-console-consumer --bootstrap-server localhost:9092 --topic users --from-beginning
   > 3,isabelle
   > 1,mitch
   > 4,sammy
   > 2,elyse
   > ```

   \endgroup

ou 

   \code

   > ```bash
   > kafka-console-consumer --bootstrap-server localhost:9092 --topic users --from-beginning --property print.timestamp=true --property print.key=true --property print.value=true
   > CreateTime:1624660202356	null	3,isabelle
   > CreateTime:1624660189996	null	1,mitch
   > CreateTime:1624660208724	null	4,sammy
   > CreateTime:1624660193108	null	2,elyse
   > ```

   \endgroup

ou

   \code

   > ```bash
   > kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic poids_garmin_brut --from-beginning
   > ```

   \endgroup

# II) Les types de topics

## 1) Les Streams

Ils sont utilisés par défaut dans beaucoup de traitements.  
Ils montrent l'historique entier des messages à un nouveau consommateur.

## 2) Les Tables

Les \definition{Table}, en revanche, sont comme une table de base de données où, pour une clef donnée,  
\demitab _(exemple : des valeurs associées au prénom "Isabelle")_  
un unique enregistrement est associé:  
celui qui correspond au dernier message en date.

Elles sont souvent créées à partir de "topics compactés": des topics qui, par le biais d'une propriété \fonction{cleanup.policy} de compactage, disent combien de messages ils veulent conserver.

\underline{Remarque:} les messages de topics sont en théorie immutables, et l'astuce pour que ces tables existent, par _Kafka_, c'est qu'il utilise un système de stockage nommé _RockDB_.

Par ailleurs, si les tables peuvent être construites à partir de streams, les tables peuvent aussi être vues comme des streams:  
dans ce cas, plusieurs arrivées de messages qui devraient conduire "_à des update_" sont vus comme des "_séries d'insert_".

# III) Les partitions de topics et les groupes de consommateurs

## 1) Les partitions d'un topic

\tab Lorsque des sources distinctes peuvent produire de mêmes données rassemblables dans un même topic, elles sont regroupables dans une \definition{partition}.

  - Toutes les données deviennent accessibles par la même étiquette, mais \danger{l'ordre (timestamp) n'est garanti qu'au sein de chaque partition}, pas globalement.

\bigbreak

Le \definition{facteur de répartition} du cluster dit sur combien de machines sera une partition pour un topic particulier: 2 ou plus est avisé.  

   - Un des brokers est déclaré \definition{leader}, il gère les requêtes des producteurs/consommateurs. Les autres, \definition{followers}, se contentent de copier ses données.

   - Si le leader tombe, un autre broker est élu leader.

## 2) Les groupes de consommateurs

Les __topics__ peuvent être consommés par un:

  - \definition{consommateur unique} qui peut reprendre où il s'était arrêté, car il repère sa position dans un flux par un __offset__ que _Kafka_ stocke, pour se rappeler où il était.
  
  - \definition{groupe de consommateurs}, qui traitent ensemble les données d'un topic, pour répondre à des problèmes de charge typiquement.
  
     - Un broker peut rejoindre ce groupe de consommateurs et participer au travail. Il peut le quitter ou tomber en panne.  
     - Un des brokers est déclaré \definition{coordinateur de groupe} pour le gérer.  
     - Si un groupe de consommateurs est utilisé, \attention{un seul de ses membres} peut utiliser une partition donnée, au risque que certains soient inactifs.

