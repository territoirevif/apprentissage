---
title: "k01. Kafka: Installation et configuration"
subtitle: "deux programmations de flux, de haut et de bas niveau, se configurant différemment"
abstract: |  
  On crée un stream Kafka que l'on démarre, soit en haut niveau,  
  soit en bas niveau, à l'aide d'une Topology, ayant une Source et un Processor.   
  \linebreak
  Une expérimentation est possible depuis le répertoire  
  `apprentissageDev/mastering_kafka_streams/mastering-kafka-streams-and-ksqldb/chapter-01`  
  où se trouve une VM à lancer en `sudo docker-compose up`, puis `sudo docker exec -it kafka bash`.

keywords:
   - Kafka
   - Zookeeper
   - topology
   - source
   - processor
   - Stream
   - KStream
   - StreamsConfig
   - port 9092 
   - port 29092
   - port 2181
   - Serdes

author: "Marc Le Bihan"
---

---

# I) Installation de Kafka sous Linux

   \code

   > ```bash
   > # Récupération et installation
   > wget https://downloads.apache.org/kafka/3.4.0/kafka_2.12-3.4.0.tgz
   > sudo tar -xvf kafka_2.12-3.4.0.tgz -C /opt
   > ```

   \endgroup

\bigbreak

Définir `KAFKA_HOME` dans le `~/.profile` et le placer dans le `PATH`.

   \code

   > ```bash
   > # Démarrage de kafka
   > zookeeper-server-start.sh /opt/kafka_2.12-3.4.0/config/zookeeper.properties
   > kafka-server-start.sh /opt/kafka_2.12-3.4.0/config/server.properties
   > ```

   \endgroup


# II) Configuration d'une application Kafka

Le port habituel pour _Kafka_ est le __9092__.  
Parfois, il peut écouter aussi sur le __29092__ (Bootstrap).

Celui de _Zookeeper_, le __2181__.

## 1) Configuration d'une application de haut niveau (DSL)

   \code

   > ```java
   > public static void main(String[] args) {
   >    /**
   >     * Création d'une topologie.
   >     * @param args Arguments.
   >     */
   >    public static void main(String[] args) {
   >       // Création d'un flux sans clef et valeur : chaîne de caractères.
   >       StreamsBuilder builder = new StreamsBuilder();
   >       KStream<Void,String> stream = builder.stream("users");
   >       	
   >       // C'est un foreach de Kafka, pas de lambda java. Il est lazy.
   >       stream.foreach((key, value) -> {
   >       	System.out.println("(DSL) Hello, " + value);
   >       });
   >       		
   >       KafkaStreams streams = new KafkaStreams(builder.build(), config());
   >       streams.start();
   >          
   >       // Fermer le flux Kafka quand la VM s'arrêtera, en faisant appeler streams.close().
   >       Runtime.getRuntime().addShutdownHook(new Thread(streams::close));
   >    }
   >    	
   >    /** 
   >     * Propriétés pour le démarrage.
   >     * @return propriétés de configuration.
   >     */
   >    private static Properties config() {
   >       Properties config = new Properties();
   >       config.put(StreamsConfig.APPLICATION_ID_CONFIG, "dev1");
   >       config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:29092");
   >       config.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
   >       config.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.Void().getClass());
   >       config.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
   >       return config;
   >    }
   > }
   > ```

   \endgroup

   Le programme ci-dessus a été lancé par les instructions d'un livre:

   \code

   > ```bash
   > sudo docker-compose up
   > ./gradlew runDSL --info
   > kafka-console-producer --bootstrap-server localhost:9092 --topic users
   > ```

   \endgroup

   suivi de quelques entrées :

   \code

   > ```text
   > angie
   > guy
   > kate
   > mark
   > ```

   \endgroup 

   qui apparaissent dans le flux après un `Ctrl-D` ou `Ctrl-C` pour stopper la saisie.

   \code

   > ```text
   > (DSL) Hello, angie
   > (DSL) Hello, guy
   > (DSL) Hello, kate
   > (DSL) Hello, mark
   > ```

   \endgroup

## 2) Configuration d'une application de bas-niveau, par l'API Processor

Au lieu de la section précédente, c'est celle-ci que l'on lance:

   \code

   > ```java
   > Topology topology = new Topology();
   >
   > // UserSource : nom arbitraire pour la source de la topologie
   > // users : le topic source
   > topology.addSource("UserSource", "users");
   > 
   > // SayHello : le nom de ce processeur
   > // UserSource devient le parent de notre processeur SayHello
   > topology.addProcessor("SayHello", SayHelloProcessor::new, "UserSource");
   > [...]
   >
   > // Démarrage du flux Kafka
   > KafkaStreams streams = new KafkaStreams(topology, config);
   > streams.start();
   > ```

   \endgroup

\bigbreak

\tab Cette application délègue à une implémentation de d'API `Processor`, ici \fonction{SayHelloProcessor},  
issue de `org.apache.kafka.streams.processor.api`, le travail de cette topologie.

\tab Le processeur prend en argument `KIn`, `VIn`, `KOut`, `VOut` (dans notre cas, la clef est de type `Void`) et propose d'implémenter les méthodes \fonction{init}, \fonction{process}, \fonction{close}.

   \code

   > ```java
   > public class SayHelloProcessor implements Processor<Void, String, Void, Void> {
   >   @Override
   >   public void init(ProcessorContext<Void, Void> context) {
   >   }
   > 
   >   @Override
   >   public void process(Record<Void, String> record) {
   >     System.out.println("(Processor API) Hello, " + record.value());
   >   }
   > 
   >   @Override
   >   public void close() {
   >   }
   > }
   > ```

   \endgroup

