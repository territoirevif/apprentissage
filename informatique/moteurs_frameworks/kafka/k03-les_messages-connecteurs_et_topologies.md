---
header-includes:
title: "03. Kafka : les messages, connecteurs et topologies"
subtitle: ""
author: Marc Le Bihan
keywords:
- event
- kafka
- clef
- timestamp
- source
- stream
- sink
- topic
- Direct Acyclic Graph
- DAG
- map
- flatmap
- filter
- topologie
- tâche
- thread
- depth first processing
---

Le message transporté par _Kafka_ s'appelle l'\definition{event}.

\bigbreak

Il comporte :

   1. Un header
   2. Une clef (optionnelle)
   3. Un timestamp
   4. Une valeur (qui peut être encodée dans un byte array).

# I) Les connecteurs

\definition{source} lisent des \definition{topics} depuis des formats externes et en font des flux : _JDBC_ en a un.  
\definition{stream} lisent des topics et les traitent/transforment.  
\definition{sink} écrivent des topics _Kafka_ (pour les consommateurs).

\bigbreak

Un processus _Kafka_ se fonde sur un \definition{Direct Acyclic Graph (DAG)} (graphe sans cycle).

Un processeur :

   - __source__ lit un flux, 
   - __de flux (stream)__ le traite ou l'enrichit : opérations \fonction{map}, \fonction{flatmap}, \fonction{filter}...
   - __sink__ le renvoie enrichi, filtré dans un autre flux.

   > ![stream, source, sink](../images/kafka_stream_source_sink.jpg)

\bigbreak

Ces flux, DAG liés entre-eux sont une \definition{topologie} \nopagebreak

   - Le sink de l'un pourra être la source d'une sous-topologie, souhaitant un traitement plus avancé.

   - __Depth first processing__ : un enregistrement suit une topologie (ou une sous-topologie) entièrement avant que l'on ne passe à l'enregistrement suivant.

# II) Tâches et threads

Les tâches sont des découpages logiques.

Une topologie utilisant un topic ayant 16 partitions créera 16 tâches.

Les threads sont en nombre limités, réglés par `num.stream.threads`.

\bigbreak

   \begingroup \cadreAvantage

   > si l'on fixe ce nombre à 2, il y aura 2 threads de 8 tâches chacun.

   \endgroup

