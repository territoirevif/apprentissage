---
title: "t305. Les services publics"
subtitle: "Règles de financement et montages"

abstract: |
   Modes de financement publics, semi-publics et privés, et montages.  
   L'on fait appel aux SPIC et EPIC pour une Obligation de Service Public (OSP) réclamant un fort investissement ou expertise.  
   SPIC ou EPIC peuvent choisir FCTVA ou Impôt sur les Sociétés (IS).  
   Il faut des règles pour passer en DSP.

keywords:
   - Service Public Administratif (SPA)
   - Service Public Industriel et Commercial (SPIC)
   - Délégation de Service Public (DSP) : affermage, concession
   - Mode de gestion
   - Règle de financement
   - Subvention d'équilibre
   - Obligation de Service Public (OSP)
   - Budget Général ou Budget Annexe (BA)
   - Etablissement Public Industriel et Commercial (EPIC)
   - FCTVA ou Impôt sur les Sociétés
   - Régie directe
   - Régie indirecte
   - Régie intéressée (aux résultats)
   - Marché Public (MP)
   - Association
   - Partenariat Public Privé
   - Mise en concurrence
   - Société Publique Locale (SPL)
   - La Société d'Economie Mixte (SEM)
   - Groupe d'Intérêt Public (GIP)
   - Procédure de passation en Délégation de Service Public
   - Cahier des charges
   - Concurrence
   - Entretien des équipements
   - Risques réels justifiant une Délégation de Service Public (DSP)
   - Affermage
   - Concession

author: Marc Le Bihan
---

---

# I) La définition du service public

La définition du service public :

   - l'objet du service
   - son mode de financement
   - ses modalités de fonctionnement

\bigbreak

Les services publics peuvent être traités :

   - en \definition{Régie} par la collectivité locale
   - ou en \definition{Délégation de Service Public (DSP)} dans les cas où elle n'a pas : 

     - l'expertise
     - la technicité
     - la capacité de sécurisation
     - le savoir faire nécessaire, en général  

      C'est souvent le cas dans le/la :
      
      - environnement (déchets, eaux usées)
      - distribution d'eau potable
      - transport public
      - stationnement (extérieur ou souterrain)
      - restauration (écoles, crèches...)
      - équipements sportifs et culturels

# II) Les règles de financement : SPA, SPIC...

## 1) le Service Public Administratif (SPA)

est typiquement en régie :

   - financé par des taxes, subventions d'équilibre
   - depuis le budget général ou régie
   - par le contribuable
   - FCTVA

## 2) Le Service public industriel et commercial (SPIC)

### a) sa présence doit être justifiée

\tab Parce que d'être là, il pourrait faire concurrence à un commerce/entreprise privée qui voudrait vivre d'une activité rémunérée sur ce service/objet.

Il faut donc qu'il y ait :

  - une \avantage{Obligation de Service Public (OSP)} dû à un caractère d'\definition{Intérêt Général}, pour justifier de sa présence.  
    _Piscine_.  
  - ou un investissement considérable que l'usager ne peut supporter seul  
    _Passage au bus électrique_  
  - ou des circonstances exceptionnelles  
    _Tempêtes, inondations_

Le SPIC est souvent délégué ($\rightarrow$ Délégation de Service Public) : on peut dire qu'il fonctionne comme une entreprise.

### b) Financement

Pour constater qu'il \avantage{fonctionne à l'équilibre}, il impliquera soit :

   - un __Budget Annexe (BA)__, qui lui assure l'autonomie financière, 

   - la fondation d'un __Etablissement Public (EP)__ pour 

      - l'autonomie financière, 
      - juridique, 
      - posséder son propre dirigeant, 
      - posséder son conseil, 
      - fixer ses tarifs. 

     ainsi, il existe des \definition{Etablissement Pulic Industriel et Commercial (EPIC)}.
 
il se finance par :

   - les recettes des usagers
   - un Budget Principal peut contribuer à un SPIC ou BA déficitaire (subvention conditionnelle)

Il perçoit la \definition{TVA} du client

   - qu'il déduit des achats
   - et reverse à l'Etat

Mais il peut faire le choix de la \definition{FCTVA} si pour le patrimoine

   > qui permet qu'une dépense d'investissement (et quelques frais de fonctionnement) soient \donnee{compensés à 15\%} par l'état.

\bigbreak

Mais le fisc va juger du choix de la TVA et de l'asujetissement à \definition{l'Impôt sur les Sociétés (IS)}. Pour cela, il regardera si le SPIC agit comme une entreprise sur ces sujets :

   - Produits
   - Public
   - Publicité
   - Prix

\bigbreak

Dépenses directes et indirectes  
Exemple : remboursement car mise à disposition par la C ou IC d'un comptable.

# III) Les montages

## 1) La Régie ou "gestion directe" / Marché public (MP)

La collectivité locale assure le service public avec son matériel et personnel.

   > ![Régie et Budgets](../images/régies-et-budgets.jpg)

   - \underline{simple :} le maire, par son budget communal.
   - \underline{autonome :} budget annexe ou direction annexe. Autonomie financière.  
   - \underline{personalisée :} confié à un Etablissement Public (EP) (ils sont autonomes) distict de la collectivité locale : exemple, le C.C.A.S.

\danger{Dès le premier euro, je fais appel aux marchés publics !}

## 2) La gestion indirecte

### a) La régie intéressée

Le service public est confié à un tiers : à une structure privée, publique ou associative.

   - \underline{régie intéressée :} un exploitant, le \definition{régisseur}, assure le service public pour la collectivité.
   
      \attention{Il ne se rémunère pas lui-même} : il verse ses revenus à la collectivité locale qui le rémunère ensuite. Souvent avec une partie fixe et variable, pour intéresser l'exploitant.
   
### b) L'association

Risques de : 

   - Gestion de fait (association transparente)  
   - Délit de favoritisme (activité économique ou commerciale, mais elle touche de l'argent public...)
   - Conflit d'intérêt

### c) La Délégation de Service Public (DSP) : en affermage ou concession

Les intérêts de la \definition{Délégation de Service Public (DSP)} sont que les entreprises :

   - maîtrisent des :  
     - technologies,
     - savoir-faire,
     - mise en applications de normes exigeantes (exemple : sanitaire, environnemental)  
\    
     que les collectivités locales ne maîtrisent pas.
   
   - partagent le coût ou possèdera les équipements requis, qui peuvent être importants, pour assurer un service public, quand la collectivité locale ne les possède peut-être pas tous.
   
   - partagent le risque financier.

\bigbreak

Mais en contrepartie, il y a des dangers :

   - Le coût financier est plus élevé. Même si des conseillers aident à négocier.
   
   - Il faut que la concurrence joue réellement :
   
      - En certains lieux, il y a parfois peu d'entreprises spécialisées dans un domaine particulier.
      - Certaines, mêmes, se répartissent les régions.
      $\to$ __Il faut qu'il y ait des contrôles__.
   
   - Les coûts de structure sont difficiles à vérifier.

### \underline{Procédure de passation en Délégation de Service Public :}

Une délibération justifie et autorise la délégation en service public du service public désiré,  
des membres sont nommés, qui vont analyser les offres issues de la :

  1. Publicité, journaux, cahier des charges
  2. Négociation pour faire jouer la concurrence

(cf. marchés publics)

La délégation de service public se pratique en : 

   - \underline{affermage :} la collectivité confie au __fermier__ le soin d'exploiter un service public au moyen d'un \avantage{équipement déjà construit}.
   
      - Il se rémunère directement auprès des usagers,
   
      - mais ce n'est souvent pas suffisant pour ses dépenses : la collectivité locale octroie un financement en échange d'un cahier des charges.
   
       - Il y a, de fait, une relation contractuelle avec la CL qui porte le risque financier.
   
   - ou \underline{concession :} Exploitation et construction sont confiés à un tiers, qui en assume les risques complets.  
   (exemple : une usine d'incinération : plusieurs milliards)
   
      - la durée de la concession est de 10, 15 ou 20 ans.
      - l'entretien des équipements est requis.
      - ensuite, les équipements reviennent à l'Etat.

### d) Groupe d'Intérêt Public (GIP)

Rare et rigide.  
Créé par arrêté ministériel, et contrôlé par un acteur public.

### e) Le Partenariat Public Privé (PPP)

\danger{Peut contourner les marchés publics}

Intérêt comment public et privé.  
Exemple : un hôpital en partie privé.

Mais il faut que la partie privée soit __vraiment intéressée__ pour que cela fonctionne.  
Les collèges ou lycée en PPP, ça na jamais rien donné.

### f) La Société d'Economie Mixte (SEM)

\danger{Peut contourner les marchés publics}

Société Anonyme à Capitaux publics (50 à 85\%)

   - La mise en concurrence est systématique  
   - Si elle perd son mandat $\implies$ Marché Public ou Délégation de Service Public $\rightarrow$ \attention{elle n'est pas sûre de le retrouver !}

Variante : __SEMOP__ pour une opération unique.

   - Capital mixte, modulable

### g) La Société Publique Locale (SPL)

Cotnrôle public total.

\demitab Deux intercommunalités doivent y avoir un intérêt.
C'est une société anonyme privée contrôlée à 100\% par __deux__ collectivités locales.

Une Collectivité Locale y excerce le même contrôle que sur ses Services Publics

\definition{CJCE Jurisprudence Teckal}

\donnee{"in house"} : pas de mise en concurrence si elle travaille dans ces deux collectivités locales, à 5 ou 10\% près.

\attention{Mais pour cette raison aussi, elle peut être exangue !}

# IV) Comparaison de montages

## 1) La Délégation de Service Public par rapport à un Marché Public

Sa mise en concurrence est différente de celle des Marchés Publics.

   - C'est le maire ou président de l'intercommunalité qui prend la décision, en son nom propre.
   - Il y a un avis de la commission, mais un avis seulement.

Le délégataire du service public __est supposé prendre les risques__ :  
\attention{s'il n'y a pas de risques avérés, requalification en Marché Public !} Car c'en est un, déguisé.

Les sommmes perçues doivent venir à 30\% minimum, des usagers.

Une délégation de service public peut être arrêtée à tout moment, mais avec compensations, car il s'agit d'un contrat public.

Un Marché Public, en revanche, est aux prix du marché  
Régie intéressée (aux résultats)
gérance.

## 2) La Délégation de Service Public par rapport au Partenariat Public Privé

Le PPP prend moins de risques qu'un DSP, et la collectivité lui \avantage{paie un loyer}.

   - Mise en concurrence
   - Choix de l'exploitant

---

# Bibliographie et références

@cnam01
