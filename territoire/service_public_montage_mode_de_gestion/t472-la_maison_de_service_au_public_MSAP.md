---
title: "t472. La Maison de Service au Public (MSAP)"
author: Marc Le Bihan
---

Elle a été pensée pour améliorer l'accès à tous les services (Etat, EPCI, Collectivité Locale), surtout au sein des territoires ruraux :

   - Pôle Emploi
   - Caisse Nationale d'Assurance Maladie (CNAM)
   - Caisse Nationale d'Assurance Vieillesse (CNAV)
   - Caisse Nationale des Allocations Familiales (CNAF)
   - La Poste
   ...

En proposant :

   - Accueil, orientation, information
   - Postes informatiques en accès libre
   - Aide à l'utilisation des services en ligne : déclaration d'impôts
   - Aide scanner, impression...
   - Facilitation administrative
   - Aide à la prise de rendez-vous

\mauve{$1700$ Maisons de Service au Public} en 2019  
Financement : 50\% Collectivité Locale, 25\% Etat

Mais :

   - L'offre de service est hétérogène
   - Défaut, parfois, de professionalisation, de qualité de service
   - Déficit de notoriété
   - Les six opérateurs peinent à amener l'argent

