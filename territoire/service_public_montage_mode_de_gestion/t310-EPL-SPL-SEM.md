---
title: "t310. EPL, SPL, SEM"
subtitle: "Entreprises Publiques Locales (Sociétés P.L., Société d'Economie Mixe...)"
author: Marc Le Bihan
keywords:
- Entreprise Publique Locale (EPL)
- Société Publique Locale (SPL)
- Société d'Economie Mixte (SEM)
- capital
- droit privé
- conseil municipal
- externalisation d'emprunt
- concurrence
- marché public
- Société d'Economie Mixte à Opération Unique (SEMOP)
- partenariat
- opération
- Entreprise à Statut d'Utilité Sociale (ESUS)
---

Une \definition{Entreprise Publique Locale (EPL)}

- est une \definition{société anonyme} qui a un capital
- qui répond du __droit privé__ et a de la souplesse pour __passer des marchés__
- elle a une \avantage{logique publique et économique}
- et est sous le contrôle du Conseil Municipal.

\bigbreak

Il y a \donnee{1200 EPL en France}.

\bigbreak

\cadreAvantage

> Les EPL sont utiles si un projet n'intéresse pas le privé,
> ou qu'une collectivité locale veut fortement maîtriser un projet :
>
>    - patrimonial
>    - culturel
>    - bâti dégradé
>    - tourisme
>    - transport
>    ...

\bigbreak

Une collectivité locale peut aussi y externaliser ses emprunts, qui ne seront plus dans ses comptes.

Elles ne peuvent être fondées que \attention{s'il y a un motif d'Intérêt Général incontestable}.  
Une collectivité locale peut entrer dans une EPL de son périmètre géographique.

## 1) La Société d'Economie Mixte (SEM)

La collectivité locale détient \donnee{50 à 85\% du capital}.

Comme elle est \underline{soumise aux règles de la concurrence},  
les collectivités locales \danger{ne sont pas certaines de pouvoir lui confier ce qu'elles désirent !}

## 2) La Société Publique Locale (SPL)

Deux collectivités locales (ou plus) s'unissent pour la constituer, et la détiennent à \donnee{100\%}.

Elles sont souvent pauvres, recevant des projets peu rentables ou accaparants.

## 3) La SEM à Opération Unique (SEMOP)

Une SEM, mais pour une opération unique, \avantage{qui permet d'engager des partenariats}.

Soumise aux règles de la concurrence, l'actif et le passif sont reversés aux actionnaires en fin d'opération : \attention{elle n'a que la durée de vie de l'opération}.

Il faut deux actionnaires minimum, et la collectivité locale détient entre \donnee{34 et 85\%} du capital.

# Annexe : l'Entreprise à Statut d'Utilité Sociale (ESUS)

Elle n'est pas publique, et n'est pas un montage mais mérite d'être mentionnée, et suit ces règles.

   - Salaires encadrés
   - 95% des bénéfices réinvestis
   - interdiction de la séparation du capital social
