---
title: "t636 - E.U. : Evaluation d'une Politique Publique"
subtitle: ""
author: Marc Le Bihan
---

Il s'agit de mesure son éfficacité __en réintégrant les besoins__, en revenant à la source

   - "_Value for money_"
   - Qualité, coûts, délais
   - Benchmark vs. autres pays
   - Indicateurs de performances

\bigbreak

![Evaluation d'une politique publique](../images/evaluation_politique-publique.jpg)

\bigbreak

\underline{Exemple :} Un musée peu avoir peu de visiteurs, mais un impact social important.

Ecueils possibles du pilotage :

   - Pas d'objectifs spécifiques
   - Pas de ciblage
   - Pas d'objectifs chiffrés
   - Réalisé comme un audit... Avec parfois pour but de virer quelqu'un, d'ailleurs.

