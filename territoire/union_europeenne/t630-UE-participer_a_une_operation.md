---
title: "t630 - E.U. : Opérations et Programmes Opérationnels"
subtitle: "Comment y participer ?"
author: Marc Le Bihan
---

L'Union Européenne cherche à promouvoir des __opérations__ qui répondent à des __stratégies__
 
   \demitab où des résultats sont évalués selon leur \definition{éfficacité} (réussite réelle)  
   \demitab et \definition{éfficience} (rapport moyen / réalisation).

\bigbreak

À l'opposé des remboursements sur les coûts réellements engagés

   \demitab les __Subventions et aides remboursables__ peuvent être en \definition{Options de Coûts Simplifiés (OCS)}  
   \demitab en proposant des montants forfaitaires et des taux.

## 1) Comment se mettre en capacité de participer à une opération d'un programme opérationnel ?

1. On raissonne par \definition{opération} et pas par fonctionnement / investissement.

   - La comptabilité est basée sur des centres de coût.
   - On part des \definition{résultats} pour caler des \definition{réalisations}.
   - On identifie des réalisations __\underline{concrètes}__ : les \definition{livrables}.

\bigbreak

2. L'on constitue une \definition{équipe projet}

   - dont le responsable a une casquette de technicien, financier, projet.
   - exemple : dossier de subvention rempli collectivement.

\bigbreak

3. Travailler en \definition{réseau}

   - Association d'élus, de villes et agglomérations
   - Réseaux internationaux
   - Regarder les expériences des autres

\bigbreak

4. Mobiliser \definition{co-financeurs} et \definition{financements} pour ces opérations

   - \attention{\underline{Attention:} ce sont des \% de co-financement:}  
     pour 20\%, un co-financement donne 20 \underline{si on a dépensé 100}.

      > \definition{accompte} : co-financement du projet.  
      > \definition{avance} : (avant-projet) devenu très rare.

   - Il faut bien identifier les coûts par opération.
   - \danger{risque de \underline{double} financement} à bien gérer.

\bigbreak

5. On définit un \definition{calendrier précis} pour le \definition{dépôt} et la \definition{mise en oeuvre}.

   - Attention à la règle du \donnee{n+2 / n+3} :   
     deux à trois ans pour réaliser son opération ou \attention{dégagement d'office} : perte des crédits.

\bigbreak

6. Faire le point régulièrement, à partir d'un \definition{tableau de bord}

\bigbreak

7. Définir des \definition{indicateurs} clairs et lisibles
   en faisant apparaître les résultats en lien avec les objectifs des programmes et stratégies.

\bigbreak

8. Définir un \definition{plan de communication}

   - Sur les réalisations, livrables, logos...
   - Pour le soutien des co-financeurs

\bigbreak

9. Intégrer la dimension européenne dès le début

   - EU 2020 et ses objectifs thématiques
   - Investissements prioritaires déclinés par \definition{axes}, avec leurs résultats attendus.

\bigbreak

10. Anglais !

\bigbreak

Autres contingences : 

   - archivage, qui implique armoire inifugée, anti-inondation. 
   - comptabilité...

## 2) Comment l'initier ?

1. Créer, réunir et faire travailler un \definition{Comité Opérationnel}
2. Prendre rendez-vous avec la région et le service instructeur
3. Dépôt du projet

\bigbreak

\attention{Penser à la consultation des habitants (par exemple, si applicable) entre ces étapes !}

# Bibliographie et liens

[EUROPA : Call for proposals/tenders](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/home)  
[https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/home](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/home)

[CORDIS](https://cordis.europa.eu/)  
[https://cordis.europa.eu/](https://cordis.europa.eu/)

Liste tous les programmes régionaux :  
[Les programmes européens](https://www.europe-en-france.gouv.fr/fr/programmes-europeens)  
[https://www.europe-en-france.gouv.fr/fr/programmes-europeens](https://www.europe-en-france.gouv.fr/fr/programmes-europeens)
