---
title: "U.E. : Les modes de gestion des autres états-membres"
subtitle: ""
author: Marc Le Bihan
---

## 1) Les questions types à se poser

Comment fonctionnent les autres états-membres :

   - vis-à-vis de la concurrence ? 

       - Qu'est-ce qui y est soumis, 
       - quelles sont leurs zones grises ?

\bigbreak

   - quel rôle a leur état ? 

        - Règlementaire ?
        - Régulateur ?
        - Actionnaire ?

\bigbreak

   - comment le territoire s'amménage t-il ? 

        - Solidarité entre territoires. 
        - Comment se combinent public et privé ?

\bigbreak

   - solidarité :

        - qu'est-ce qui est à l'échelle locale, 
        - qu'est-ce qui est à celle européenne ?

## 2) En France

Communes : compétences générales  
Intercommunalités : EPCI

SPA / SPIC, PPP  
DSP : droit __intuitu personae__

MP / DSP

Socialisme municipal :  
   \demitab si carence du privé $\implies$ le public s'en mêle.

Problème de péréquation :  
   \demitab comment redistribuer entre collectivités ?

Deux supers régions, et les autres...

## 3) Modèle Rhénan (Allemagne, Belgique, Suède)

Intercommunalités : Kreis

Economie sociale de marché

   - __Stadtwerke :__ régie communale (entreprise pour gérer le marché, mais sous service public)
   - Solidarité entre services

Autonomie locale

   - Chaque Land s'organise : primauté au local
   - Délégation de compétence
   - Organisation fédérale

Péréquation : \donnee{95\% - 105\%}  
   
   - entre Länders
   - entre Länders et Etat

Inhouse : en Allemagne, mais aussi en Belgique (wateringue) et Suède

## 4) Etats régionalisés (Italie, Espagne)

Intercommunalités : Communes/villages portugais
Autonomie des régions

Solidarité régionale, sauf pour l'impôt

Espagne : provinces (Etat), region (autonomes)

## 5) Anglo-saxon

Initiative privée, sous licence

Centralisation administrative

Pas de compétence générale

Pas d'administration locale

__Performance Indicators :__ concurrence compulsive qui va parfois trop loin

__Big Society :__ l'état n'intervient qu'en urgence : pas de tutelle des collectivités locales

## 6) Les nouveaux entrants ont souvent choisi :

Service public passé du tout étatique au tout régulé

