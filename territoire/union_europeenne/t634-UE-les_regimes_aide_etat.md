---
title: "t634 - E.U. : Les Régimes d'Aides d'Etat"
subtitle: "Les situations faussant ou susceptibles de fausser la concurrence"
author: Marc Le Bihan
---

Ce sont les \definition{aides publiques} qui faussent ou menacent de fausser la concurrence  
   \demitab 1958 : Droit de la concurrence

\bigbreak

Quel que soit son statut (même associatif),  
c'est l'avantage, d'un point de vue économique, qui compte.

Pas de favoritisme, sauf à le justifier

Reporting par projet (et archivage)

Diagnostic SWOT : atouts, faiblesses, opportunités, menaces.

\danger{argent public + avantage + marché + état membre}

## 1) Bénéficiaire / partenaires

Sont les responsables de l'opération

## 2) Groupe ou territoire cible

Là où l'on veut avoir ses effets

   - \danger{attention à un groupe cible qui s'avèrerait être \underline{client !}}  
     \danger{Car un client doit payer le }\definition{prix du marché}

## 3) Prestataire

Fait les travaux avec obligation de moyens.

   - Mis en concurrence
   - Emet des factures
   - Reçoit des paiements
   - Peut sous-traiter, en le déclarant.
   - \attention{On ne peut pas réclamer qu les prestations soient d'une région}

      - mais dans un rayon (exemple: 10 km)
      - on peut aussi demander d'employer localement

   - \attention{on ne peut pas plus cerner un produit régional "qui n'existerait qu'ici"}

## 4) Le Budget

__dépenses directes__ : uniquement consacrées à cette opération

__dépenses indirectes__ : à proratiser. Elles sont limitées.

   \demitab Exemple : pour les \definition{dépenses simplifiées} 
   \demitab \attention{qui réclament d'avoir une vision très claire}   
   \demitab l'Europe dit : c'est 7\% de dépenses indirectes.

\bigbreak

\definition{\underline{Budget :}} (= dépenses)

   - Personnel
   - Services extérieurs : études, fournitures
   - Equipement : location, amortissement : \avantage{les favoriser}  
   - Infrastructures : elles dépassent la vie de l'opération, avec : achat, construction...  
     \attention{l'UE n'en veut plus}  
   - Dépenses indirectes
   - Publicité

Ce n'est parfois pas simple de classer : ordinateur = équipement ou infrastructure ?  
Présenter équipements et infrastructures au travers de leur __usage__.

## 5) Les ressources

Les recettes viennent d'un caractère économique, les établir et ensuite voir les besoins en ressources. Il n'y en a peut-être pas !

Les ressources n'ont pas de caractère commercial.  
On peut estimer les recettes à venir à partir du commerce.

Il faut établir un calendrier des dépenses et réalisations.

\bigbreak

\definition{\underline{Plan de financement :}}

Le \avantage{service instructeur} nous apprend que lors d'une opération,

   \demitab pour 100 dépensés,  
   \demitab l'UE, considérant l'intérêt général, la zone 75\% du PIB, etc.  
   \tab dit que si le contributeur \attention{trouve un cofinancement public} (l'UE ne veut pas faire seule),  
   \tab elle contribuera, remboursant une partie des dépenses,  
   \tab dès que le bénéficiaire se sera auto-financé pour \avantage{15 ou 20\%}.

\bigbreak

\underline{Attention :} le Régime des Aides d'Etat peut mener à un plafond de \underline{tous} les fonds :  
   \demitab UE + cofinanceurs

Calendrier des livrables, réalisations :

   \demitab Plan d'action à lier avec les livrables  
   \demitab Calendrier des dépenses justifiées.

\bigbreak

\danger{Seulement des indicateurs de résultats ? => Risque d'écrémage}

\danger{Seulement des indicateurs de réalisation ? => Risque d'inefficacité}

