---
title: "t632 - E.U. : Opérations et PO"
subtitle: "Monter un dossier de financement"
author: Marc Le Bihan
---

Une opération doit :

   - fonctionner de manière __autonome__ 
   - avoir des __tranches fonctionnelles__ distinctes  
     \demitab rattachées à des __réalisations concrètes__.

     \underline{Attention :} une tranche fonctionnelle, c'est différent d'une phase,  
     dont un contrôleur européen ne voudra pas.

\bigbreak

Les régions (ex : conseil régional) et l'Europe sont des \definition{autorités de gestion}.  

   \demitab elles peuvent déléguer des sous-domaines dans des \definition{organismes intermédiaires}  
   \demitab dans les départements ou intercommunalités.

\bigbreak

Les opérations doivent être terminées à $N+2$ ou $N+3$ : plus de fonds dormants.  
et le taux d'erreur doit être raisonnable.

On peut obtenir des fonds pour une opération démarrée, eventuellement...  
Mais jamais pour une opération terminée !

Ce n'est pas son statut qui compte, mais l'activité, l'opération : le résultat que l'on atteint :

   \demitab $\rightarrow$ la \avantage{contribution}, la \avantage{capacité à faire}.

## 1) Points d'alerte : ce dont il faut se prémunir !

\attention{Gare au privé !}

\attention{Gare aux refinancements !}  
et aux \attention{double financements} (même feuille de temps pour deux projets différents !).

\bigbreak

À ce sujet, \danger{De Minimis est \underline{TRÈS} dangereux !},  

   \demitab car il est applicable si le bénéficiaire ne touche \donnee{pas plus de 200 000 euros tous les trois ans}, 
   \demitab __toutes aides publiques confondues__ 
   \tab et __tous financeurs confondus !__

Bien connaître son passé, être bien informé.  
Soi-même, on peut avoir oublié ! Ne pas se rendre compte !

\bigbreak

\avantage{il existe des exceptions communautaires} qui rendent des dépenses éligibles.  

Mais dès qu'on détecte une situation d'\danger{Aides d'État},  
   \demitab c'est à dire une situation faussant où risquant de fausser la concrrence, \danger{STOP !}

\definition{"Stand Still"}  
__délit de favoritisme !__

   - Caractère intentionnel d'une erreur ? $\rightarrow$ fraude $\rightarrow$ juge

   - Corrigeable ? Oui $\rightarrow$ erreur  
     Non $\rightarrow$ irrégularité $\rightarrow$ correction financière.

## 2) Dépôt par l'outil Synergie

1. Identification du dossier

   Le porteur du projet est le \definition{bénéficiaire}  
   Localisation du projet.  
   Période d'exécution : 2 à 3 ans.

\bigbreak

2. Assujetissement à la TVA  
   Récupération TVA = Financement Hors-Taxes.  
   Mais une CL peut assujettir ses activités à la TVA (voir FCTVA).

\bigbreak

3. Description du projet

   Parler : Objectif Thématique, Investissement Prioritaire  
   Evoquer le programme
   Pas de phasage  
   aller chercher les indicateurs du projet

\bigbreak

4. Plan de financement

\bigbreak

5. Pièces à joindre

\bigbreak

6. Positionnement dans le Programme Opérationnel  
(commencer par cela !)

\bigbreak

7. Prise en compte des thématiques

   Répondre à __toutes__ les questions   
   Egalité Homme-Femme,  
   Egalité des chances...

## 3) Les contrôles

Pour chaque Programme Opérationnel, il y a :

   - Une autorité de gestion
   - une autorité de certification
   - Une autorité d'audit

\bigbreak

__Ex-ANTE__ : Contrôle lors de l'instruction  
__Ex-POST__ : Contrôle après sélection, durant le déroulement  
(qui, idéalement, devraient être faites par la même personne : être instructeur = être vérificateur).

\bigbreak

Les contrôles sont techniques, financiers et administratifs,  
Sur pièces \underline{et} \danger{sur place} de plus en plus.

Si l'on appartient à un groupe (ex: université ou commune...), les contrôles pourront avoir la portée de ce groupe.

responsable côté opération $\iff$ responsable côté autorité de gestion

Les obligations d'utiliser, bien sûr, les marchés publics.  
Publicité des fonds européens, et ce dès la première page.

### a) Côté instructeur, premier contrôle : l'autorité de gestion

- Diagnostic
- Objectifs
- Publics cibles
- Résultats (effet(s) sur un groupe cible)
- Réalisaitons
- Budget
- Plan de Financement
- Calendrier
- Bénéficiaires

\bigbreak

\definition{\underline{L'autorité de gestion :}}

La région est le gestionnaire du programme : 

   - instruction
   - signature du contrat
   - vérification
   - suivi
   - planification
   - sélection
   - évaluation

\avantage{il est recommandé de prendre rendez-vous avec son service instructeur}.

\bigbreak

\underline{Vérfications \definition{Ex-ANTE} :}  
(typiquement du service instruction)

   - __éligibilité de base__ : on s'assure que le bénéficiaire a les __capacité de faire__.  
     Sinon, il ne sera jamais un bon contributeur !

   - sélection d'après la contribution :

      - efficience : à moindre coût  
      - efficacité : en quelle proportion l'on contribue aux résultats.   
      - __contrat__ : la formalisation des conditions de paiement.  
        Liées à des indicateurs de résultats et de réalisation.  
      - respect des politiques horizontales :  
        environnement, égalité homme/femme, lutte contre la discrimination...

### b) L'autorité d'audit

C'est le deuxième niveau de contrôle.

   - contrôle des opérations
   - vérifie le système de contrôle de l'autorité de gestion, car son sujet n'est pas simple !

\bigbreak

L'UE attend son rapport avant de payer.

En France, c'est la \avantage{Commission Interministérielle de Contrôle (CICC)} : inspecteurs IGA, IGAS, IGF.

et \definition{Post-ANTE} 

   - On vérifie les réalisations (produits et services co-financés)
   - puis, on paie les bénéficiaires

\bigbreak

Point de contrôle : Contrôle de Service Fait (CSF).

### c) L'autorité de certification

Contrôle l'exactitude des comptes.  
La \avantage{Cour Européenne des Comptes} peut aussi contrôler les résultats.

\bigbreak

Mais il y a un principe de \textbf{\underline{l'audit unique}} :

   \demitab le contrôleur tient compte des contrôles précédents,  
   \demitab et ne refait pas ceux qui ont déjà été réalisés.

\bigbreak

Gestion des risques : contrôle d'assurance raisonnable : 2 ou 5\% d'erreurs ?

   \demitab Risques inhérents internes de non-détection.
   \demitab Examen statistique.

## 4) Les coûts simplifiés

   1. Classique : le remboursemment des coûts élibibles réellement engagés et payés.

      \underline{Remarques :}  
      
      - l'\definition{l'argent public} s'exprime en unités de caisse,  
        que seul le \definition{comptable du trésor} ou un mandaté peut manipuler.  
      - Dépenses : paiement par mandatement,  
        puis réception d'un état des dépenses effectuées, où l'on voit leur acceptation.  
      - Recettes : on émet un titre de recettes.  
      - Coûts réels : facture + état des dépenses acquittées et \definition{comptabilité engagée} :  
        bon de commande ou autre chose (contrat...).

\bigbreak

   2. Les \definition{barèmes standards de coûts unitaires} : réalisations ou résultats

\bigbreak

   3. \definition{Montant forfaitaires} : mais les résultats ne sont pas atteints => rien !

      (2) et (3) sont toujours choisis pour les \donnee{dossiers inférieurs à 100 000€}.  
      Et c'est souvent le cas pour ceux supérieurs aussi.

   4. Financement à \definition{taux forfaitaire}

      déterminé par l'application d'un pourcentage à une ou plusieurs catégories de coûts définis,  
      et destiné à déterminer d'autres dépenses,  
      comme des coûts indirects, par exemple.

