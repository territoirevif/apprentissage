---
title: "t202. Le droit européen"
subtitle: ""
author: Marc Le Bihan

keywords:
   - droit européen
   - hiérarchie des normes
   - traité
   - règlement
   - directive
   - décision
   - avis
   - loi
   - décret
   - arrêté
   - circulaire
   - livre blanc
   - livre vert
   - spill over
---

## 1) La Hérarchie des normes 

Le \definition{Traité} est équivalent à notre constitution.

Un \definition{Règlement} ou une \definition{Directive}, à une loi française.

La \definition{Décision} et l'\definition{Avis} sont plutôt en direction de ses institutions ou équivalent aux Décrets, Arrêtés ou Circulaires de notre droit national.

L'U.E. peut aussi faire de simples \definition{Communications}.

## 2) Le Spill Over

Par ailleurs, Règlements et Directives peuvent s'établir par consultation par :

   - Un livre blanc
   - Un livre vert

dans un processus appelé \avantage{Spill Over} où le contenu de ces livres sera jugé et incorporé aux règlements et directives votées.

