---
title: "t620. U.E. : Les fonds européens"
subtitle: ""
author: Marc Le Bihan
---

# I) Les différentes aides

## 1) Le financement international : deux ou trois nations

Sur EUROPA : aller sur ["Call for proposals" ou "Call for tenders"](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/home)

Il faut adhérer à un consortium (avoir des partenaires, [CORDIS](https://cordis.europa.eu/) peut y aider) :  
   \demitab on ne peut venir aux réunions de présentation seul.

La région se fait représenter à Bruxelles.

## 2) La coopération INTERREG

Transfrontalière, transnationale, transrégionale

On peut les trouver sur [Europe en France](https://www.europe-en-france.gouv.fr/fr/programmes-europeens) : 
   \demitab une case à cocher "_transfrontalier_" ou "_interrégional_" dans le sélecteur de liste des programmes.

## 3) Un Programme Opérationel régional

Regarder les \definition{axes prioritaires}, ils sont rattachés à des \definition{objectifs spécifiques}  
qui incluent des résultats et des réalisations.

   > L'Europe 2020, définit \definition{Objectifs Thématiques} et \definition{Investissements Prioritaires}  
   > et la région, les __Objectifs Spécifiques__.

# II) Les fonds européens

## 1) L'indicateur NUTS de comparaison

Un indicateur le \definition{NUTS} permet la comparaison des territoires dans l'UE selon trois niveaux :

   - l'état
   - la région (certains petits états = une région)
   - département et en dessous

\bigbreak

Les \definition{catégories de région} sont classées par \donnee{PIB / habitant},  
   \demitab sur la base que l'UE des 27 états a un indice moyen à 100 :

   - \donnee{75\%} : régions moins développées
   - \donnee{\ $\mathbf{\downarrow}$\ \ \ } : régions en transition, celles vers laquelle la France se dirige
   - \donnee{90\%} : régions plus développées : Pologne, Estonie, Lithuanie tirene leur épingle du jeu

## 2) Le cadre budgétaire FEDER, FSE, FEADER

### a) Les fonds annoncent leurs stratégies

L'Union Européenne annonce ses stratégies deux ans à l'avance par une Lettre de Cadrage, envoyée à l'Etat.

   - le \definition{Fond Européen de Développement Régional (FEDER)}
   - le \definition{Fond Social Européen (FSE)}
   - le \definition{Fond Européen Agricole de Développement Régional (FEADER)}

### b) Montants et répartitions des aides pour la France

\donnee{FEDER + FSE font 16 milliards d'euros} pour la France :

   - 25% Outremer
   - 27% 10 régions en transition
   - 40% 12 régions développées
   (il s'agit des anciennes régions, en 2014-2020)

\bigbreak

En France, les \attention{niveaux de programmation des opérations sont trop faibles} : 
   \demitab 25 à 50\% $\rightarrow$ cela est dû au redécoupage des régions.

Elle touchera davantage d'aides, car elle est passée en dessous de la moyenne du PIB européen.

## 3) Le Groupement d'Action Local (GAL)

(_Community-Led Local Development (CLLD)_ en anglais)

### a) Leur objectif est, notoirement, la Politique de la Ville

Créés parce que l'Europe n'a pas de compétences en Politique de la Ville, par exemple.  
Et surtout : pour intervenir dans les Quartiers Prioritaires de la Ville (QPV).

Aussi, elle fixe une méthode : le cadre urbain de développement durable dans l'U.E.,  
réclamant que soient intégrés conjointement, et pas de manière séparée :

   - Economique
   - Social
   - Environnement
   - Egalité des chances

### b) Constitution d'un GAL

Mis en place par l'Europe et la région, il faut \donnee{50\% d'acteurs privés} pour le constituer.

Stratégie de Développement Local (DLAL),  
   \demitab où le __FEADER__ apporte le \avantage{Programme LEADER} = DLAL __rural__.

L'approche est le pays ou le SCOT, et pas la commune ou intercommunalité.

## 4) Les Investissements Territoriaux Intégrés (ITI)

Il existe aussi depuis 2020 les \definition{Investissements Territoriaux Intégrés (ITI)}, sans GAL.  

Ce sont des stratégies spécifiques pour certaines régions.

Développement de synergies entre entrerprises, sur des thématiques et spécialisations territoriales.

## 5) Le Groupement Européen de Coopération Territorial

De type INTERREG, pour créer des zones territoriales.

# Bibliographie et liens

[EUROPA : Call for proposals/tenders](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/home)  
[https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/home](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/home)

[CORDIS](https://cordis.europa.eu/)  
[https://cordis.europa.eu/](https://cordis.europa.eu/)

Liste tous les programmes régionaux :  
[Les programmes européens](https://www.europe-en-france.gouv.fr/fr/programmes-europeens)  
[https://www.europe-en-france.gouv.fr/fr/programmes-europeens](https://www.europe-en-france.gouv.fr/fr/programmes-europeens)

