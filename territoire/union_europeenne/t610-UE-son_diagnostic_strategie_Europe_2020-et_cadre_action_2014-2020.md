---
title: "t610. U.E. : son diagnostic et cadre d'action 2014-2020"
subtitle: ""
author: Marc Le Bihan
---

# I) Le diagnostic de l'UE sur l'Union Européenne 

## 1) La population baisse

La population est en baisse (\donnee{450 millions d'habitants}), et elle vieillit :

  - \donnee{35 millions d'habitants auront 80 ans ou plus, en 2030},
  - il y a \donnee{2 actifs pour 1 inactif}.

## 2) Améliorer le Taux d'Emploi

Il faut améliorer le \definition{taux d'emploi} (différent du taux de chômage) :

   > Dans l'UE, le \donnee{taux d'emploi des 15-64 ans est de 64\%}  
   > avec \donnee{70 millions d'habitants en situation précaire}.

\bigbreak

   > Allemagne: davantage de migrants,  

   >   > $\implies$ meilleurs taux d'emploi,    
   >   > meilleur financement des retraites.

## 3) Les objectifs de chaque pays, sur Eurostat

Chaque pays a ses objectifs spécifiques à atteindre visibles sur [Eurostat](https://ec.europa.eu/eurostat/fr/home)

   > \listPlus Environnement  
   > \listPlus Education  
   > \listEgal Emploi : sous-emploi des femmes, toujours  
   > \listMoins Pauvreté : aucun progrès

# II) La stratégie Europe 2020 et Cadre d'Action 2014-2020

L'Union Européenne fonctionne par période : \definition{cadre}  
   \demitab autrefois, c'était un agenda, de \donnee{sept ans}.

## 1) La stratégie Europe 2020

\definition{Traité de Lisbonne (2005)} :  
   \demitab on lutte contre les _écarts de territoire_ (et plus contre les _écarts de richesse_) :   
   \demitab (ne pas confondre la _Stratégie de Lisbonne_ avec le _Traité de Lisbonne (2007)_  
   \demitab qui rassemble tous les traités précédents).

\bigbreak

L'U.E. fit le pari de la recherche et développement, et éjecta le reste :

   - culture 
   - sport 
   - infrastructures de base 
   - aménagement des villes...

\bigbreak

   1. \underline{\textbf{Innovation :}}
   
      - Recherche et développement
      - Société numérique
      - Nouveaux produits et services : sans eux, comment être compétitifs ?

   > 2014-2020 $\rightarrow$ \donnee{3\% du PIB} en R&D.

\bigbreak

   2. \underline{\textbf{Environnement, climat :}}

      - Energie trop fossile, polluante, gaspilleuse...
      - Efficacité énergétique des bâtiments

   > 2014-2020 $\rightarrow$ Réduction des gaz à effet de serre de \donnee{20\% par rapport à 1990}.

\bigbreak

   3. \underline{\textbf{Formation / éducation :}}

      Vers l'emploi, la mobilité professionnelle

   > 2014-2020 $\rightarrow$

   > - \donnee{75\% de la population de 20 à 64 ans}.
   > - \donnee{40\% de la population des 30 - 34 ans} diplômée de l'enseignement supérieur.
   > - Lutte contre la sortie précoce du système scolaire : \donnee{10\% sortent sans diplôme}

\bigbreak

   4. \underline{\textbf{Lutte contre la pauvreté et l'exclusion sociale :}}

      - Nouveaux emplois et nouvelles compétences
      - Lutte contre la pauvreté

   > 2014-2020 $\rightarrow$ 70 millions de personnes en pauvreté et exclusion sociale $\mathbf{-20\%}$

## 2) Les Objectifs Thématiques (O.T.) 2014-2020

\underline{\textbf{O.T. 1 :}}

   - Recherche
   - Développement technologique
   - Innovation

\bigbreak

\underline{\textbf{O.T. 2 :}}

   - Accès et qualité de l'accès aux technologiees de l'information

\bigbreak

\underline{\textbf{O.T. 3 :}}

   - Renforcer la compétititivé des Petites et Moyennes Entreprises (PME)

\bigbreak

\underline{\textbf{O.T. 4 :}}

   - Mutation vers une économie à faible teneur en carbone.

\bigbreak

\underline{\textbf{O.T. 5 :}}

   - Adaptation au changement climatique
   - Prévention des risques

\bigbreak

\underline{\textbf{O.T. 6 :}}

   - Protéger l'environnement 
   - Promouvoir un usage durable des ressources

\bigbreak

\underline{\textbf{O.T. 7 :}}

   - Transport durable
   - Suppression des goulets d'étranglement dans réseau principal d'infrastructures

\bigbreak

\underline{\textbf{O.T. 8 :}}

   - Promouvoir l'emploi
   - Promouvoir la mobilité au travail

\bigbreak

\underline{\textbf{O.T. 9 :}}

   - Inclusion sociale
   - Combat contre la pauvreté

\bigbreak

\underline{\textbf{O.T. 10 :}}

   - Investir dans l'éducation, les compétences, la formation tout au long de la vie

\bigbreak

\underline{\textbf{O.T. 11 :}}

   - Renforcer la capacité institutionnelle
   - Rendre l'administration publique plus efficace

# Bibiliographie, liens

- [Eurostat](https://ec.europa.eu/eurostat/fr/home)  
[https://ec.europa.eu/eurostat/fr/home](https://ec.europa.eu/eurostat/fr/home)
