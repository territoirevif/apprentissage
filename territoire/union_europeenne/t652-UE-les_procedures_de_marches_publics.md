---
title: "t652 - Les procédures de marchés publics"
subtitle: "gré à gré, Marché à Procédure Adaptée, Appel d'Offre"
author: Marc Le Bihan
---

# I) Les procédures de marchés publics : gré à gré, MAPA, AO

Un marché public est une \definition{mise en concurrence}

   \demitab qui raisonne sur des \definition{seuils},  
   \demitab mais aussi sur des __principes__.

La \definition{publicité} d'un marché public est requise !

### 1) Portée et durée du marché

Les marches publics sont conclus à titre onéreux,  
   \demitab saufs exceptions comme le mobilier urbain.

\bigbreak

Ils peuvent porter sur des services 

   - matériels 
   - ou immatériels (juristes, architectes...)

\bigbreak

Le durée d'un marché est souvent d'un à quatre ans.

## 2) Rester ouvert

\danger{On ne peut demander une marque} dans les spécifications techniques.

Il faut rester ouvert.

## 3) Les procédures de marché

Elles dépendent de son coût

|                         | gré à gré | Marché à Procédure Adaptée (MAPA) | Appel d'Offres |
|-------------------------|-----------|-----------------------------------|----------------|
| Fournitures et services | 25 000€ < | 221 000€ <                        | > 221 000€     |
| Travaux                 | 25 000€ < | 5 548 000€ <                      | > 5 548 00€    |


\bigbreak

\rouge{Attention aux études !} 

On les croit souvent indépendantes,  
   \demitab mais elles sont parfois une partie d'une opération plus vaste,  
   \demitab dont le coût global à considérer pour être dans la bonne procédure est plus grand.

### a) Marché de gré à gré

L'acheteur s'adresse à qui il veut, sans mise en concurrence.  
Mais il doit quand même demander plusieurs devis.

### b) Marché à Procédure Adaptée (MAPA)

\attention{On \underline{peut} négocier.}

C'est le cas si le _Dossier de Consultation des Entreprises (DCE)_ n'indique pas le contraire.

### c) Appel d'Offres 

   - Le délai minimal de remise des offres est de 30 jours.

   - Obligation de \definition{Suspension de Signature} pendant 11 jours en fin de parcours :  
     un concurrent peut attaquer en _référé précontractuel_.

   - \danger{Pas} de négociation.

\bigbreak

Dérogation possible aux Appels d'offres par :

la __Procédure Concurrentielle avec Négociation__  
et le __Dialogue Compétitif__ sous condition que :  

   1. Le marché implique des prestations de conception  
     (exemple : refaire un site Internet)  
   2. Il est impossible de définir les spécifications du marché  
     (exemple : station d'épuration)  
   3. __Concours__ pour choisir un plan ou un projet (avec prime aux participants)  
      Exemple : refaire l'hôtel de ville.  
   4. __Partenariat d'Innovation__ : il implique de la recherche et développement  
      car ce qui existe à ce moment ne peut satifaire l'acheteur.

\bigbreak

Remarque : toutes les entreprises de l'Union Européenne pourront candidater à l'appel d'offres :

   \demitab les marchés publics ne peuvent être discriminants.  
   \demitab mais l'on peut demander des offres qu'elles \avantage{favorisent le local}.

## 2) L'acheteur

Ils vont faire une offre, et juger de la capacité à faire.

L'acheteur peut :

   - Demander des précisions.  
   - Demander à corriger une étourderie sur un point précis  
     (mais seulement celui-là : le candidat ne peut changer son offre !)

### a) Les acheteurs publics

Les acheteurs (pouvoir __adjudicateur__) sont :

   - les communes, 
   - départements, 
   - régions, 
   - ministères, 
   - préfectures, 
   - établissements publics locaux, 
   - EPCI
   ...

### b) Autres acheteurs

   - les sociétés d'économie mixte (SEM),  
     sociétés publiques locales (SPL)  

   - et dans le cas de concessions, c'est l'usager qui paie.

## 3) L'offre de l'acheteur

\definition{Article 30, ordonnance du 23 Juillet 2015}

   \demitab L'acheteur doit définir en amont ses besoins, les prestations qu'il va acheter

   - pour encadrer sa dépense,  
     en permettant de la comparer avec celle des années précédentes (économique)
   - \rouge{saucissonage à moins de 25 000 euros interdit !}
   - Prix trop bas ? \rouge{se méfier des prix prédateurs}, et demander des justifications.

\bigbreak

\danger{L'enveloppe n'est \underline{pas communiquée} aux concurrents pour éviter les ententes}

## 4) Communication et échanges entre acheteurs et candidats

\definition{Article 4 du décret du 25 Mars 2016} 

   \demitab autorise les échanges avec les opérateurs économiques  
   \demitab avant le lancement d'un projet (sourcing),   
   \demitab et des _Assistants Mmaîtres d'ouvrage (AMO)_ peuvent aider la ville.

### a) Le Document de Consultation des Entreprises (DCE)

Il décrit toutes les règles :

   - Critères
   - Délais à respecter pour la remise des offres
   - Attestations demandées
   - Capacités professionnelles

### b) L'Acte d'Engagement

   - Nom de l'entreprise
   - Nom de l'acheteur
   - Prix proposé par l'entreprise

      - global et forfaitaire : elle s'engage à mettre en oeuvre tous les moyens nécessaires
      - prix unitaire : peut varier selon les quantités (?)

### c) Le Cahier des Clauses Administratives Particulières (CCAP)

Il définit les obligations et sanctions et peut se décliner en cahiers des clauses : 

   - techniques (CCTP)
   - administratives générales (CCAG)
   - fournitures et travaux (CFCS)

### d) Avis de Marché

Il est publié par la collectivité, qui donne le DCE à qui le demande.

### e) Dématérialisation des marchés supérieurs à 25 000 euros

Tous les marchés supérieurs à 25 000 euros sont dématérialisés  
et il y a obligation d'échanger par voie électronique, sur une plateforme Internet.

__Open Data__

Accroît la transparence. Les données essentielles du marché public sont publiées avec :

   - Le profil d'acheteur
   - puis, le moment venu, l'offre retenue (qui a gagné)
   - avec données sur les passations : 

        - prix, 
        - date de signature
        ...

   - données sur l'exécution, tous les avenants

# III) Déroulement de la procédure

## 1) Publicité

   - Pas d'obligation en dessous de 25 000€ (gré-à-gré).  
   - Publication sur le [Bulletin d'Annonces sur les Marchés Publics (BOMAP)](https://www.boamp.fr/),  
   et sur le [Journal d'Annonces Légales (JAL)](https://www.annonces-legales.fr/) à partir de 90 000€.  
   - et pour les Appels d'Offres, dans le [Journal Officiel de l'Union Européenne](https://eur-lex.europa.eu/oj/direct-access.html?locale=fr)

## 2) Mise en concurrence

En Appel d'Offres, une __Commission d'Appel d'Offre__

   \demitab est composée d'élus venus d'une assemblée délibérante,  
   \demitab certains venus du conseil municipal.

## 3) Sélection des Offres

L'offre est choisie considérant :

### a) La plus avantageuse, vis à vis du DCE

L'offre choisie est celle la plus avantageuse __vis à vis du DCE__ d'après 

   - son prix, 
   - technique, 
   - délai d'exécution
   ...

les critères étant pondérés et repris dans le rapport.

### b) Les informations que l'on a sur les candidats

Sur les candidats, l'acheteur prend des informations sur leurs :

   - moyens humains et matériels,
   - moyens financiers,
   - qualifications professionnelles, 
   - capacités, 
   - références (! ce qui normalement, ne devrait pas être le cas)

\bigbreak

\definition{Arrêté 26/03/2016} : liste les informations que l'on peut demander en phase de sélection aux candidats.

### c) Possibilités de rejet d'une entreprise

On peut rejeter une entreprise si :

   - risque de conflit d'intérêt acheteur / candidat  
   - mauvais précédent  
    (prouvé ! D'où beaucoup de notifications et prises de notes durant l'exécution de contrats) avec elle.

### d) Avenant possible

Un avenant est possible, une seule fois, sur présomption de légalité (?) avec preuve,

   \demitab 50% avec 15% travaux, 10% services.

# IV) Le contrat public

Un __contrat__ est un accord de volonté.  
Mais, par ses prérogatives de puissance publique, une administration impose 

   - délais
   - pénalités
   ...

\bigbreak

Un __accord cadre__, lui, permet à une administration d'adapter son contrat quand elle ne sait pas à l'avance toutes les prestations dont elle aura besoin. 

Besoins standards ou spécifiques :

   - stylos
   - papier... 

---

# Bibliographie et références

@cnam01
