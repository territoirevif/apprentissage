---
title: "t650 - Les marchés publics et les aides d'état"
subtitle: "Le droit de la concurrence européen"
author: Marc Le Bihan
---

## 1) Le droit de la concurrence Européen

Les \definition{Marchés Publics} transposent des __directives communautaires__   
qui ont remplacé le _Code des marchés publics_, qui existait auparavant.

### a) Les secteurs qui sont ouverts à la concurrence

Avec le temps, tous les service publics, ont été ouverts à la concurrence  
même dans les secteurs les plus étatiques :

   - eau
   - energie
   - transport
   - gaz
   - numérique
   - poste
   - communications électroniques
   ...

### b) Ce qu'impose les principes européens pour les services publics

Par ailleurs, les principes européens, qui valent aussi pour les services publics, imposent d'assurer :

   - la qualité,
   - la sécurité,
   - le caractère abordable
   - l'égalité de traitement
   - l'accès universel
   - le droit des utilisateurs

## 2) Empêcher ou contrôler les Aides d'Etat

### a) Ne pas fausser la concurrence

L'UE cherche à les empêcher, car elles faussent ou sont susceptibles de fausser la concurrence.

   > \donnee{Article 107:} [...] sont incompatibles avec le marché commun [...] les aides accordées __par les états__ ou au moyen de __ressources d'état__ \danger{sous quelque forme que ce soit} qui faussent ou \danger{menacent de} fausser la concurrence en __favorisant__ certaines __entreprises__ ou __productions__.

\bigbreak

De fait, se pose la question du \definition{mandatement} (mandat d'intérêt général) : 

   \demitab comment mettre de l'argent public dans le privé,  
   \demitab pour des obligations de service public,  
   \demitab sans fausser la concurrence ?

### b) Quelques exemples d'Aides d'Etat

Ces aides d'état peuvent être __directes__ ou __indirectes__

Un exemple d'aide indirecte, le favoritisme  
$\rightarrow$ Requalifier les abords d'une entreprise :

   - Voie refaite
   - Parking non public
   - Loyer de parking amorti sur 50 ans...

\bigbreak

Pas d'avantages à des commerces bien établis ! Exemples :

   - des subventions
   - Remise de dette
   - Abandon de créance
   - Exonération fiscale ou sociale (TVA, impôt sur les sociétés...)
   ...

### b) Les conditions de faussage de concurrence

Voici les conditions/situation cumulatives de faussage :

   1. \donnee{Public} : de l'argent public, ou quoi que ce soit d'autre.  
   2. \donnee{Avantage} : attention, \attention{financer un déficit}, c'est un avantage !  
      Pas de soucis si c'est ouvert à tout le monde.  
   3. \donnee{Marché} : il faut qu'il y ait marché.  
   4. \donnee{Etats-membres} : c'est à dire, différent du marché local.

\bigbreak

Il y a des fonds publics, directs ou indirects. 
   \demitab Avec un caractère sélectif présent : ils ne sont pas pour tout le monde.

On octroie un avantage à un opérateur économique 

   \demitab ("_les quatre P_" : marché Public, Produit, Prix, Publicité),  
   \demitab et cela affecte les échanges entre états-membres.

### c) Les erreurs à ne pas commettre

   - Caractère public du bénéficiaire
   - Caractère __non lucratif__ ou __non commercial__ : \danger{vous cassez le marché !}
   - Les aides indirectes
   - Les zones blanches

\bigbreak

Pour faciliter la détection des aides d'état, il faut raisonner par :

   - Bénéficiaire
   - Secteur
   - Localisation
   - un \gris{pourcentage plafond par rapport à l'environnement ?}

## 3) Le Stand Still et l'obligation de notification des Aides d'Etat

Il y a obligation de notifier les aides d'état, sauf régime d'exeption, et alors la commission décide.

\bigbreak

\definition{Arrêt Ferring} :

Dès lors qu'il y a une \avantage{Obligation de Service Public (OSP)} qui entraine un __surcoût__,  
l'argent touché __n'est pas une subvention__ mais une \definition{compensation} (c'est à dire une contribution de service public). À stricte hauteur de ce surcoût, bien sûr.

\bigbreak

\definition{Arrêt Altmark} :

Il y a :

   - des obligations de service public
   - des éléments de salaire
   - pas de surcompensation

\bigbreak

\definition{Directive Kroes} : \bleu{les quatre critères d'un mandat d'intérêt général}  

   - obligation de service public
   - définition explicite et préalable
   - calcul préalable dde la compensation
   - contrôle de surcompensation

c'est une dérogation aux aides d'état.

### a) Les régimes d'aides notifiés

Chaque __région__ a un \definition{registre des régimes d'aides notifiées}   
   \demitab issues de la négociation avec l'Union Européenne sur : 

   - le tourisme, 
   - l'économie  
   ...

Le \definition{Règlement Général d'Exemption par Catégorie (RGEC)}

### b) De Minimis

200 000 euros sur 3 ans par bénéficiaire.  
\rouge{Extrêmement dangereux, car très facilement et indiciblement dépassé}.

## 4) Les services publics européens qui peuvent déroger au régime des aides d'état

Il y a cependant des aides compatibles avec le marché européen.  
Elles sont souvent à caractère social.

   - octroyées aux consommateurs individuels,
   - et utilisées sans discrimination sur l'origine des produits

\bigbreak

Aides extraordinaires et désastres naturels

Peuvent aussi être compatibles :

   - Aides pour le développement de régions défavorisées (grave sous-emploi)
   - Projet d'Intérêt Européen
   - Culture et conservation du patrimoine : mais gare aux échanges et à la concurrence

\bigbreak

Pour les autres dérogations, il faut notifier la commission pour le savoir (paragraphe précédent).

### a) Le Service d'Intérêt Economique Général (SIEG)

Il faut, comme son nom l'indique, un intérêt général : 

   - gratuité pour les enfants, 
   - relais postal dans un commerce...

\avantage{\underline{C'est la fonction qui importe}}
\newline \attention{pas que ce soit public ou privé, local ou pas !}

C''est à dire qu'il y a derrière, par exemple, une _Obligation de Service Public (OSP)_.

\bigbreak

Un choix qui est souvent fait quand :

   - Un marché existe
   - Il y a rémunération du service
   - et contrepartie économique

(Variante le SNEIG, qui est "_régalien_" : pour la puissance publique, et exclusivement social)

### b) Entreprise Publique

Elle fournit un service d'intérêt général dont le fournisseur peut être public ou privé.

### c) Le Service d'Intérêt Général (SIG)

Couvre les services marchands ou non marchands

### d) Le Service Universel

"_Un ensemble minimal de services déterminés à tous les utilisateurs finaux, à un prix abordable_".

Chaque état y met ce qu'il veut. 

   \demitab Par exemple, pour les télécommunications, 
   
   \tab des pays choisiront de proposer la 4G à tous,  
   \tab quand d'autres fourniront l'annuaire papier gratuitement...

### e) Le Service Social d'Intérêt Général (SSIG)

Droits sociaux fondamentaux, couverture des risques sociaux, en général.

- soins de santé 
- garde d'enfants 
- accès et réinsertion sur le marché du travail
- logement social
- inclusion sociale des groupes vulnérables
...

\avantage{Jusqu'à 500 000€ sans appel d'offre pour les marchés publics}
\newline \avantage{Publicité possible}

## 5) Règles de sélection des entreprises : alotissement

26/02/2014. Ordonnance du 28/07/2015 et Décret du 25/03/2016

   1. Favoriser l'accès à la commande publique des PME en imposant __l'alotissement__
   2. Publicité suffisante du marché
   3. On ne peut rejeter une entreprise par manque de référence
   4. On ne peut fermer un cahier des charges en imposant une marque particulière

