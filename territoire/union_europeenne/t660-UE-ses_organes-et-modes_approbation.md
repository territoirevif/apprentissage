---
title: "t660.  U.E. : ses organes et ses modes d'approbation"
subtitle: ""
author: Marc Le Bihan
---

## 1) Organes

### a) Le Conseil Européen

Il réunit les chef d'états pour cinq ans.

### b) Conseil de l'Union Européenne

Il réunit les ministres et est présidé pour six mois.

### c) Le Parlement Européen (PE)

A des groupes politiques : PPE, S&D, ECR, ADLE

\avantage{Conseil de l'Union Européenne et Parlement Européen co-décident}

### d) Commission Européenne

Elle est la gardienne des traités  
Elle peut faire des propositions de lois

### e) La Cour de Justice de l'U.E.

### f) La Cour des Comptes Européenne

### g) La Banque Centrale Européenne (BCE)

### h) Eurogroupe

Conseil des Ministres de l'Economie

### i) Le Conseil de l'Europe (n'est pas une institution de l'UE)

Il réunit 46 pays.

## 2) Ses modes d'approbation

### a) La majorité qualifiée

\suspect{(et (dans quelles circonstances ?) par un ministre [français, pour nous] du thème requis ?)}

   - \donnee{55\% des membres} qui votent
   - \donnee{65\% de la population} représentée
   - minorité de blocage : 

      - \donnee{35\%} de la population
      - et \donnee{quatre} états-membres

### b) à l'unanimité

Sur :

   - la finance
   - l'immigration
   - la sécurité sociale

## 3) Représentation

La France a une \definition{Représentation Permanente (RP)}  
   \demitab avec des \definition{envoyés ministériels}.
 
Elle aide aux réunions préparatoires,  
   \demitab pour assister des ministres du \definition{Secrétariat Général des Affaires Européennes}  
   \demitab (en lien avec le gouvernement).

Exemple : Ministre de l'Agriculture.

\bigbreak

Le \definition{S.G. Affaires Régionales} existe, mais ce sont là encore des ministres, et pas des régions.

