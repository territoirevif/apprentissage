---
title: "t640. Comment un état ouvre ses services à la concurrence ?"
subtitle: "illustration dans les télécoms, l'énergie et le secteur ferroviaire"
author: Marc Le Bihan
---

L'Union Européenne a invité ses états à s'ouvrir à la concurrence sur certains secteurs,  
   \demitab à l'aide de \definition{paquets de directives}  
   \demitab (une directive doit être transposée en deux ans).

\bigbreak

En particulier, elle a eu une action sur :

   - les télécoms, 
   - l'énergie, 
   - le réseau de transport : dont celui ferroviaire.

\bigbreak

Le premier principe, c'est que :   
   \demitab \avantage{Celui qui règlemente ne peut pas être celui qui exploite}

Le deuxième, c'est que :  
   \demitab l'on passe de services où il y a des autorisations à obtenir pour s'en dire fournisseur,  
   \demitab à des \definition{services déclaratifs} avec :

  - des obligations
  - une contribution au __Service Universel__

et une nécéssité, par un organisme de contrôle, de \definition{réguler}.

## 1) Dans les télécommunications

   - L'\avantage{Autorité de Régulation des Télécommunications (ART)} est celle qui régule   
   - La contributon au Service Universel est l'annuaire papier, en France,  
     mais c'est la 4G dans d'autres pays...  
   - L'\avantage{Autorité Européenne de Marché} observe la gestion des données personnelles (RGPD).

\bigbreak

Une collectivité locale peut :

   - déployer un réseau
   - des infrastructures
   - des services (en logique SPIC)

   > e-learning, e-service, smart-cities,  
   > territoire numérique : consultation médicale par Internet

## 2) Dans l'énergie

Jusqu'à 1990, EDF était le délégataire obligé.

En France, les compétences d'électrification  
    \demitab sont portées par des \definition{Syndicats Départementaux d'Electricité}.  
    \demitab Nationaux, ils assurent une péréquation globale au niveau national.

Mais les métropoles les quittent, pour payer moins cher  
   \demitab $\rightarrow$ plus de péréquation départementale et nationale.

\bigbreak

L'ouverture à la concurrence et le _Grenelle de l'environnement II_ ont amené :

   - les tarifs spéciaux
   - achat de photovoltaïque
   - les droits à polluer, qui réduisent l'effet de serre, mais sont inégalitaires

Notre respect de l'environnement, du point de vue énergétique, est surtout dû au tout nucléaire.

## 3) Dans les transports et réseaux de transports

L'U.E. commence par ouvrir le fret à la concurrence,  
   \demitab qui est passé de 15 à 8\% en Europe, de 1970 à 2003.

Puis un réseau trans-européen casse le monopole.

\bigbreak

En 1997 : Création de \avantage{Réseau Ferré de France (RFF) (EPIC)} qui gère :

   - les capacités régionales,
   - nationales
   - le fret
   - les sillons

\bigbreak

En 2001, il est créé une \avantage{Licence d'Entreprise Ferroviaire}  
dont la règlementation est à l'échelle E.U.

Création d'une \definition{Agence Ferroviaire Européenne}.

Renforcement de l'interopérabilité (largeur des voies) et de la sécurité

Indemnisation des trains en retard

Le réseau ferré est ouvert à la concurrence sur les trajets internationaux,  
et depuis Décembre 2019, à la concurrence du trafic voyageur.

