---
title: "421. le Logement social"
subtitle: ""
author: Marc Le Bihan
---

La situation du logement, en France, est catastrophique.  
Il faudrait construire plus de $500000$ logements par an, quand on est à $100000$.  
Des bidonvilles, éradiqués dans les années 50, se reforment.  
$3\%$ de rotation des logements.

## Historique

\definition{1912 - Première loi de service public du logement}

\definition{Loi Quillot} : Logement pour tous  
Remise en état entre locataires

\definition{Solidarité Renouvellement Urbain (SRU)}  
$20\% \rightarrow 25\%$ (Voynet) de logements sociaux

\definition{1975 - Loi Barre} :  
Aide à la pierre remplacée par l'APL... que plus personne ne touche !

\definition{2007 - Droit au logement opposable (DALO)}

\definition{2009 - Loi Boutin / Molle} :  
Le loyer payé augmente jusqu'à $60\%$, d'après son salaire, ce qui entraîne des départs.  
Ceux qui arrivent sont plus pauvres que les précédents. 

## Situation des logements sociaux

Il y a $35$ à $38$ millions d'unités d'habitation, dont $5$ millions d'unités de logements sociaux.  

\rouge{Les barèmes font qu'un couple avec deux SMIC sans enfants ne peut plus accéder à un logement social.}

\rouge{Rejoignent et occupent des logements sociaux des personnes qui ont de moins en moins la capacité de les payer.}

\rouge{Problème du coût du foncier}, manque de promoteurs privés, étalement urbain.

Les immeubles mêlent locatif et propriétaires.

L'\definition{Agence Nationale Nationale pour la Rénovation Urbaine (ANRU)} réclame des rénovations énergétiques, 
et l'\definition{l'Agence Nationale pour l'Amélioration de l'Habitat (Anah)} agit, sans condition de ressources en copropriétés et individuel.

Les logements que l'on voit vides et inoccupés, sont souvent des logements qui sont en attente de recevoir des gens que l'on va reloger là, 
quand des travaux de rénovation auront lieux dans leurs habitations.  

## Financement et attribution d'un logement social

1. Le bailleur demande à la commune de quels terrains elle dispose.

2. S'enquiert des règles de constructibilité.  
\mauve{PLU} $\rightarrow$ hauteur, espacement...

3. La commune écrit à \definition{France Domaine} : le terrain vaut $100000 \pm 10\%$

4. La commune vend le terrain à $90 000$ si aménagements collectifs ou culturels...

5. La commune accepte de garantir le prêt du bailleur.  
   \rouge{Elle prend le risque de ne plus avoir de prêts}  
   Elle obtient un \underline{droit de réservation} sur $20\%$ des logements.  
   Elle propose des locataires, que le bailleur peut toujours refuser.

6. Département et région peuvent faire des subventions ou des prêts via la \definition{La Caisse des Dépôts et Consignations} (financé, entre-autres, par le Livret A).  
Organisée par les bailleurs sociaux, la \definition{Caisse Générale du Logement Social} peut les aider, s'ils n'ont eu aucun prêt.

---

### Bibliographie et références
@cnam01
