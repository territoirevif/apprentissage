---
title: "420. La Politique Locale du Logement"
subtitle: ""
author: Marc Le Bihan
keywords:
- logement social
- locataire
- propriétaire
- accédant
- crédit
- logement vacant
- transformation de bureau en logement
- dissémination
- leap frog
- étalement urbain (leap frog)
- encadrement des loyers
- bailleur
- ALUR
- Aides Personnalisées au Logement (APL)
- sous-location
- mal logement
- surpeuplement
- surpopulation simple
- habitat dégradé
- précarité énergétique
---

# I) Le parc et le besoin en logements

Il y a \donnee{36 millions d'unités} dont \donnee{5.5 de logement sociaux}

\donnee{350 000 logements} se construisent par an. \donnee{$110000$ sont sociaux}.

Le secteur du logement pèse \donnee{502 milliards d'euros} soit \donnee{22\% du PIB}.

## 1) Locataires et propriétaires

   - \donnee{44\% de collectif},
   - \donnee{56\% d'individuels}

et

   - \donnee{42\% de locataires}, dont \donnee{18\% de social}
   - \donnee{58\% de propriétaires}, dont \donnee{20\% d'accédants} (c'est à dire : avec un crédit en cours)

\bigbreak

$\rightarrow$ On achète avec le taux du crédit en cours : les crises empêchent l'emprunt.

## 2) Les habitats recherchés

### a) L'habitat trans-générationnel

   - les personnes âgées voudront plutôt des rez-de-chausée et de grands appartements,
   - les jeunes, petits ou moyens, au dessus.

### b) Logements vacants

   - Faciliter la transformation de bureaux en logements.
   - Dérogation possuble servitude
   - Bonus constructibilité sur PLU

## 3) Différentes populations et répartitions

Les populations et comportements sont diversifiés selon qu'on sera dans le :

   - rural, 
   - péri-urbain éparpillé, 
   - banlieue continue,
   - ville centrale : logements de plus chers et plus petits, revenus plus hauts, aussi.

\bigbreak

Phénomènes d'émiettement communal, de mobilité.

   - dissémination (_leap frog_)
   - étalement urbain (_urban sprawl_)

# II) La difficulté à se loger

Les logements suffisent en nombre, mais pas en type :

   > 2, 3 personnes par logement
   > ruptures familiales et vieillissement population

\bigbreak

\donnee{8\% des logements vacants} surtout en villes moyennes.

\bigbreak

Le logement coûte cher, c'est \donnee{26\% du budget des français}, qui atteint \donnee{40\%} pour les \donnee{8\%} des français les plus modestes.  
et, en date de 2020, le prix des terrains a augementé de +10\% en dix ans.


Surchauffe dans les pôles métropolitains : immobilier tendu,   
dans ceux moyens ou plus petits : mais déclin relatif des aires urbaines et quartiers centraux  
les zones rurales sont vieillissantes, surtout au Sud.

  - $\frac{2}{3}$ des logements occupés par une ou deux personnes.

## 1) La difficulté d'accès au logement

La politique du logement, c'est une \underline{politique lourde} : il faut très longtemps entre décider et avoir.

   - d'un côté, il y a une politique volontaire de l'état, budgétaire et fiscale, avec interventionisme, pour aider les ménages,  
   - d'un autre, ce secteur obéit aux règles du marché, avec des taux d'intérêts mondiaux, et un essort des entreprises de construction à assurer.

### a) l'Encadrement des loyers (ALUR)

Plus de \donnee{1400 communes sont en zone tendue}

   - si travaux d'amélioration,
   - un loyer de référence est fixé par \avantage{l'Observatoire Local des Loyers}
   - amende à l'encontre des bailleurs (5 000€, 15 000 pour une personne morale), et :

      - restitution des trop perçus
      - mise en demeure de baisser les loyers

\bigbreak

L'encadrement s'enclenche à __l'initiative de l'intercommunalité__, sous condition qu'elle :

   - soit compétente en matière d'habitat,
   - qu'il y ait un écart important entre les loyers moyens, privés et sociaux,
   - loyer médian élevé,
   - le taux de renouvellement du parc social est faible.

L'autorisation est pour 5 ans, par décret du préfet.

### b) Aides Personnalisées au Logement (APL)

Elle est versée aux occupants pour alléger la charge qui pèse sur eux. C'est une Aide à la Personne.

Elle participe à hauteur de 20 milliards pour :

   - \donnee{20\% des loyers sociaux}
   - \donnee{12\% des loyers privés}

\bigbreak

Ses bénéficiaires,

   - ont pour \donnee{19\% moins de 30 ans},
   - \donnee{22\% sont des familles monoparentales}
   - 2 sur 5 vivent sous le seuil de pauvreté.

et ces APL font chuter leur taux d'effort de 40 à 27\%.

\bigbreak

\underline{Remarque :} la loi ELAN a supprimé les APL aux accédants, et ce sont les organismes HLM qui compensent.

### c) Sous-location

Un locataire qui sous-loue conserve son APL,  
et celui qui loue, aussi.

## 2) Le mal logement

D'après la Fondation Abbé Pierre, \donnee{4 millions de personnes sont mal (ou non) logées} :

   - \donnee{0.9 sont sans logement} : abris, chambre d'hôtel
   
      > \donnee{0.15 sont SDF}

   - \donnee{0.6 hébergées par des tiers}

\bigbreak

   - \donnee{2.6 privées de confort} : eau, chauffage...

      > dont \donnee{0.9 en surpeuplement accentué}
      >
      >   - \donnee{33\% de familles nombreuses}
      >   - \donnee{59\% de familles modestes}
      >   - et \donnee{17\% du parc social le subit}.

      > et \donnee{5.1 en surpopulation simple}

\bigbreak

Selon comment on avise les chiffres, on peut aller jusqu'à \donnee{8.5 millions de personnes mal logées}.

\donnee{2 millions en habitat dégradé}

\donnee{12.1 millions de personnes sont en situation de fragilité} :

   - \donnee{1.2 ont eu des loyers impayés}
   - \donnee{3.5 ont eu froid $\implies$ précarité énergétique}

