---
title: "t506. Le Nouveau Programme de Renouvellement Urbain"
subtitle: "(NPNRU 2014-2024) "
author: Marc Le Bihan
keywords:
- territoire
- sociologie urbaine
---

## 1) Son contenu et son objectif

450 Quartiers d'intérêts généraux

   - 10 milliards d'euros  
      - 6.6 action logement
      - 2.4 \avantage{Caisse de Garantie de Logement Locatif et Social (CGLLS)}

\bigbreak

\underline{Objectifs :}

   - 80000 démolitions
   - 72000 reconstructions
   - 130000 requalifications

en articulation avec le \donnee{Contrat de Ville}.

## 2) A t-il réussi ?

La Cour des Comptes dénonce :

  - Le manque d'indicateurs de suivi
  - pas assez d'évaluations de résultats ou de performances

\bigbreak

Sur 600 quartiers :

|                   | 2003 | 2013 |
| ----------------- | ---- | ---- |
| Logements sociaux | 58\% | 61\% |
| Surface ($m^2$)   | 67.3 | 61.0 |

$15%$ du parc dégradé démoli
$40000$ logements privés construits

| Habitants en $\to$               | QPV  | hors QPV |
| -------------------------------- | ---- | :------: |
| En logement social               | 76\% | 24\%     |
| Pauvres                          | 42\% | 16\%     |
| Sans diplôme ou inférieur au Bac | 75\% | 55\%     |
| Emploi précaire                  | 21\% | 14\%     |

L'écart entre hors QPV et QPV reste important.  
Borloo dit que c'est un scandale : on a fait des annonces au lieu d'agir.

\bigbreak

Budget 2019 : 498 millions d'euros

   > cohésion sociale : 75\%  
   > revitalisation économique : 17\%  
   > ingénierie, projet : 5\%  
   > rénovation urbaine : 3\%

+ exonérations fiscales de $1.56$ milliards d'euros

et en 2020 : + $5.7$ milliards d'euros  
copropriétés dégradées : $2.7$ milliards d'euros
\bigbreak

47 quartiers en reconquête Police du Quotidien  
Emploi francs : mitigé. 12000 contre 40000 espérés  
Contrats aidés : 2017 : 320000 $\to$ 2019 : 13000

