---
title: "t507. Sociologie Urbaine : Stigmatisation"
subtitle: ""
author: Marc Le Bihan
keywords:
- territoire
- sociologie urbaine
---

## 1) Des personnes "repoussées" vers les quartiers

Rapport de domination : 

   - sociale
   - économique
   - ethnique

repoussés dans les quartiers.

## 2) Entretien d'un regard misérabiliste

Avec une vision homogénéisante :

   > publics (jeunes ou immigrés)  
   > territoires (quartiers)

\bigbreak

qui sont vus comme irrémédiablement différents,  
\demitab avec apparition d'une terminologie négative :

   - "_quartiers sensibles_"
   - "_ségrégation urbaine_"
   - "_territoires perdus de la république_"
   - "_violences urbaines_"

\bigbreak

qui, bien sûr, leur fait de la contre-publicité, et n'incite pas à s'y installer !

