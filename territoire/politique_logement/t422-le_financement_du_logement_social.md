---
title: "a422. Le financement du logement social"
subtitle: ""
author: Marc Le Bihan
category: territoire
keywords:
- logement social
- financement
- organisme HLM
- office public de l'habitat (OPH)
- entreprises sociales pour l'habitat (ESH)
- acquisition de terrain
- réhabilitation
- rénovation
- caisse des dépôts et consignation (CDC)
- prêt locatif aide intégration (PLAI)
- prêt locatif à usage social (PLUS)
- prêt locatif social (PLS)
- prêt locatif intermédiaire (PLI)
- aide à la pierre
- fond national des aides à la pierre (FNAP)
- action logement
- livret A
- livret d'épargne populaire
---

## 1) Organismes HLM et bailleurs sociaux

Les \definition{Offices Publics de l'Habitat (OPH)}, un EPIC, et  
les \definition{Entreprises Sociales pour l'Habitat (ESH)}, tous deux pour 41\% :

   1. Acquièrent du terrain et construisent
   2. Acquièrent et améliorent
   3. Réhabilitent : rénovent et relouent

## 2) Les prêts de la Caisse des Dépôts et Consignation (CDC)

Via le __Livret A__ et le __Livret d'épargne populaire__, \donnee{435 milliards d'euros} dont confiés à la CDC

   - 189 milliards sont proposés en prêts aux organismes HLM, qui panachent les prêts, car les immeubles sont rarement monobloc et ont plusieurs publics.  
   - 86 sont des actifs financiers

\bigbreak

\definition{Prêt Locatif Aide Intégration (PLAI)}

Pour ceux très éloignés du logement

   > Taux du Livret A (0.5\%) - 20 points de base = 0.3\% de taux d'intérêt

\bigbreak

\definition{Prêt Locatif à Usage Social (PLUS)}

Ménages modestes en besoin importants.

   > 1.1% de taux d'intérêt

\bigbreak

\definition{Prêt Locatif Social (PLS)}

Ménages modestes classiques.

   > 1.6\% de taux d'intérêt

\bigbreak

\definition{Prêt Locatif Intermédiaire (PLI)}

   > 1.9\% de taux d'intérêt

## 3) Les Aides à la pierre

Les \definition{Aides à la Pierre} sont des financements à destination des bailleurs, proposés par un \definition{Fond National des Aides à la Pierre (FNAP)}, qui facilitent la création de logements sociaux.

mais ils ne sont pas très élevés : 

   - Etat : 30 millions
   - Organismes HLM : 75 millions
   - Action Logement : 300 millions

\bigbreak

TVA sur les logements sociaux : 10\% (autrefois 5.5\%).

