---
title: "t505. La sociologie urbaine"
subtitle: "Histoire"
author: Marc Le Bihan
keywords:
- territoire
---

## 1) Moyen-âge

La ville apparaît au moyen-âge :  
ce sont ses remparts qui la distinguent des villages et hameaux.

   - sécurité,
   - commerce,
   - circulation,
   - convivialité,
   - loisirs,
   - salubrité

## 2) Révolution industrielle

La révolution industrielle est synonyme de progrès.

Essort de la vapeur et des machines qui l'utilisent.  
Les ouvriers arrivent en grand nombre.

Mais :

   - forte concentration,
   - misère,
   - rejet en périphérie

Grandes avenues : 

   - l'on cherche à faire rentrer de l'air et de la lumière.
   - faciliter le maintien de l'ordre

## 3) La déqualification des quartiers : de la ville rêvée à la désillusion vécue

\donnee{1950} : modèle de la \definition{cité idéale}

   - constructions modernes,
   - propres,
   - claires, confortables, chauffées...
   - avec des services communs

\bigbreak

\donnee{1960} : retournement du modèle

   1. "_l'île echantée_" : les ouvriers viennent : pour eux, c'est une progression sociale
   2. L'on fait venir de la main d'oeuvre étrangère (souvent des anciennes colonies) :

      - pas assez de logements : ils vont dans les mêmes habitats
      - ce sont deux groupes très différents, à la cohabitation imparfaite.  
        Et qui tire vers le bas.

\bigbreak

\donnee{1970} : \definition{déqualification}

Conséquence de la crise économique et migratoire.

   > \definition{1988 $\to$ 2015 : Conseil National des villes (CNV)}
   > 
   > Il rassemble :  
   > 
   >    - élus et personnalités politiques
   >    - acteurs économiques, 
   >    - associatifs, 
   >    - habitants des quartiers prioritaires
   >
   > et il émet des avis, fait des rapports sur des sujets.

\bigbreak

\donnee{1990} : naissance de la \definition{Politique de la Ville}

Fondée sur la discrimination positive.

   > \definition{1998 : le Contrat de Ville}
   >
   > Il fixe un cadre au partenariat entre :
   > 
   >    - Etat, 
   >    - Collectivité locale, 
   >    - Partenaires de développement
   >
   > \mauve{435 contrats de ville pour 5.5 millions d'habitants} \gris{(quelle date ?)}

\bigbreak

\donnee{2000} : la \definition{requalification urbaine} des quartiers

Reconstruire, rebâtir.

   > \definition{2004 : Agence Nationale de Renouvellement Urbain (ANRU)}  
   > et le \definition{Programme National de Renouvellement Urbain (PNRU)}
   >
   >   - diversification des quartiers  
   >      - fonctions offerte
   >      - natures
   >      - statuts logements  
   >   - désenclavement des quartiers
   >   - démolition
   >   - reconstitution de logements sociaux
   >   - réhabilitation
   >   - résidentialisation (domaine public, domaine privé)
   >   - aménagements
   >   - équimements publics

Mais, il est axé immobilier :

   - les gens restent relogés à proximité
   - pas de brassage
   - les équilibres sociaux ne changent pas

\mauve{12 milliards d'euros, 200000 logements démolis}

\bigbreak

\donnee{2010} : le \definition{renouvellement urbain}

Il ajoute une action sociale en intégrant habitants et services.

   - on spatialise la question sociale
   - on travaille par catégories de territoire

