---
title: "t700. Environnement : GEMAPI"
subtitle: "Gestion des Milieux Aquatiques et Prévention des Inondations"
author: Marc Le Bihan
---

## 1) Le but de la Gestion des Milieux Aquatiques et Prévention des Inondations (GEMAPI)

Gestion des fleuves, rivières, cours d'eau,  
problématique des inondations.

Cette compétence a été créée en 2014-2015,  
   \demitab car les responsabilités étaient auparavant éparpillées, alors que :

   - Ces phénonmènes sont répétitfs et leur acuité augmente chaque année,
   - L'entretien des cours d'eau pouvait être mal fait
   - et l'utilisation des sols, mal maîtrisée.

\bigbreak

Elle a pris effet au $1^{er}$ Janvier 2018.  
   \demitab C'est une \mauve{compétence communale devenue intercommunale en 2020}.

   - L'intercommunalité devient responsable de la gestion des bassins d'eaux.  
     Elle peut instaurer une taxe GEMAPI allant jusqu'à \donnee{40 euros par habitant et par an}.

   - La commune a aussi des responsabilités, dont en urbanisme  
     inquiétude dans les communes rurales, car cette responsabilité est lourde et son coût aussi.

## 2) Les structures qui organisent GEMAPI

   1. La structure opérationnelle, maître d'ouvrage :  
      l'\definition{Etablissement Public d'Amenagement et de Gestion de l'Eau (EPAGE)}.
   
   2. Coordonne les structures EPAGE et définissent  
      le \definition{Schema d'Elaboration et d'Aménagement de la Gestion d'eau (SAGE)}
   
      - Couvre tout le bassin d'eau
      - \definition{Etablissement Public Territorial de Bassin (EPTB)}  
        (Les bassins sont définis par des lignes de crète)

## 3) Le détail de la compétence GEMAPI

Les départements et régions peuvent apporter des financements,  
   \demitab mais ce sont les communes et intercommunalités qui sont responsables.

\rouge{les détenteurs de propriétés privées (personnes morales ou physiques) doivent entretenir leurs abords}.

### a) Aménagement d'un bassin

   - Gestion hydrolique
   - Bassin de rétention
   - Ecoulement
   - Bassins réservoirs
   - Barrages

### b) Entretien/aménagement d'un cours d'eau, canal ou plan d'eau

Les communes sont responsables de l'écoulement :

   - curage
   - bon état écologique
   - débris
   - engorgements

### c) Défense contre les inondations et la mer

Les intercommunalités sont responsables des :

   - ouvrages de protection contre la mer (digues)
   - servitudes sur propriétés et passages
   - lutte contre l'érosion des côtes

### d) Protection et restauration des systèmes aquatiques, des zones humides et boisées

   - Exemple : gestion des marais

## 4) Le rôle de l'Etat

L'état :

   - élabore les \avantage{cartes de zones inondables}
   - donne la prévision et alerte des crues
   - met en place des \avantage{plans de prévention des risques}

\definition{Voies Navigables de France (VNF)}

