---
title: "t704. Environnement : la gestion des déchets"
subtitle: ""
author: Marc Le Bihan
---

La collecte des déchets est une \donnee{compétence régionale}  
   \demitab partagée avec la commune et l'intercommunalité.

\definition{2014 : Loi contre l'Obsolescence Programmée}
appuyé par le pouvoir de police (de salubrité) du maire  
   \demitab (ou intercommunalité s'il a renoncé à ses pouvoirs de police).

## 1) Collecte

\underline{Communale (ou Intercommunale) :}

Dans les zones de plus de 2000 habitants  
   \demitab la collecte des \definition{Ordures Ménagères Résiduelles (OMR)} doit se faire au moins une fois par semaine.

   - \mauve{4000 déchèteries} en France.
   - collecte en porte à porte
   - problème du Fini-Parti causant ultérieurement des problèmes de santé (dos)

\bigbreak
 
Elles tendent à aller vers le secteur privé pour la collecte des déchets.

## 2) Traitement

   - tri coûteux (mais tous les français y participent à présent)
   - problèmes de maraude (polluants) et de pillage des déchêteries  
   - \mauve{40\% des déchets sont encore incinérés} et l'objectif est de 20% dans 10 ans.

\bigbreak

\underline{Intercommunal :}

   - Gestion des déchets électroménagers (mine urbaine)

## 3) Financement

   - La \definition{Taxe d'Enlèvement des Ordures Ménagère (TEOM)}  
     est liée à la valeur locative via la Taxe Foncière Bâti (TFB).  
   - La \definition{Redevance au lieu de Taxe (REOM)} est un SPIC  
     \demitab dont l'équilibre financier dépend du service rendu.  
   - La \definition{Redevance Incitative (RI)} : l'on ne paie que ce que l'on jette.  
     Elle monte en puissance, mais est dangereuse :  
     \demitab conflits car le voisin jette dans votre poubelle ou dans la nature.

