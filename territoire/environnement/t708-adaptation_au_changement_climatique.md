---
title: "Environnement : adaptation au changement climatique"
subtitle: "Exemple de quelques villes"
author: Marc Le Bihan

references:
   - id: lgp01
     author: Catherine Abou El Kahir
     title: "Adapter la ville au changement climatique"
     container-title: Le journal du Grand Paris
     issue: 65
     page: 4
     type: article-journal
     issued:
       year: 2024
       month: 11

   - id: lgp02
     author: Nacima Baron
     title: "Il faut réfléchir à différentes échelles spatiales et temporelles"
     container-title: Le journal du Grand Paris
     issue: 65
     page: 10
     type: article-journal
     issued:
       year: 2024
       month: 11

   - id: lgp03
     author: Catherine Bernard
     title: "Transition écologique : le défi des chantiers électriques"
     container-title: Le journal du Grand Paris
     issue: 65
     page: 10
     type: article-journal
     issued:
       year: 2024
       month: 11

   - id: lgp04
     author: Elena Jeudy-Ballini
     title: "Quand l'ONU incite les villes à accentuer leurs politiques environnementales"
     container-title: Le journal du Grand Paris
     issue: 65
     page: 16
     type: article-journal
     issued:
       year: 2024
       month: 11

---

## 1) Les collectivités locales de plus de 50 000 habitants doivent rédiger pour l'ONU un rapport sur leur développement durable

Communes ou Intercommunalités dépassant cette taille le doivent.  
Valent à l'horizon 2030, et portent sur le développement durable inclu dans ces objectifs généraux de l'ONU :

  1. Pas de pauvreté
  2. Faim zéro
  3. Bonne santé et bien-être
  4. Education de qualité
  5. Égalité entre les sexes
  6. Eau propre et assainissement
  7. Énergie propre à un coût abordable
  8. Travail decent et croissance économique
  9. Industrie, innovation et infrastructure
  10. Inégalités réduites
  11. Villes et communautés durables
  12. Consommation et production responsables
  13. Lutte contre le réchauffement climatique
  14. Vie aquatique
  15. Vie terrestre
  16. Paix, justice et institutions efficaces
  17. Partenariats pour la réalisation de ces objectifs

## 2) Quelques réalisations pour aider à transformer la ville en éponge

L'objectif est de transformer la ville en "_éponge_".

### a) Sols

   - Surfaces infiltrantes
   - Structures alvéolaires sur les sols

### b) Bâtiments recevant du public

Il s'agit de réduire la température tout en évitant les climatiseurs.

   - Stores à toiles transparentes réduisant la température de 7.5°C à 10°C (Poissy).

### c) Végétalisation

Elle permet d'apporter de la fraîcheur l'été, mais réclame de protéger les arbres contre le stress hydrique, ce qui suggère de :

   - travailler sur des fosses continues d'arbres
   - de choisir des espèces dont les racines se développent en profondeur

## 3) Les îlots de chaleur

### a) Impact des arbres

On ne peut résoudre le problème des îlots de chaleur en plaçant simplement des arbres dans un parc.  
Il faut penser jusqu'aux forêts urbaines.

  - végétalisation des cours d'écoles

### b) Impact des fontaines urbaines

On escompte qu'elles captent la chaleur de l'air ambiant, formant une zone de fraîcheur à la surface de l'eau (refroidissement de l'air par évaporation) et autour du bassin.  

Les aménageurs qui y songent doivent cependant penser aux nuisances nocturnes qu'elles peuvent générer.

## 4) Chantiers : le passage à l'électrique est à ses balbutiements

Le passage à l'électrique (allant jusqu'à exclure les biocarburants ou ceux de synthèse) a pour but de réduire les émissions en $\text{CO}_2$.

Mais il faut :

   - Planifier en amont les besoins de recharge électrique, et construire des points de charge temporaires  
   - Obtenir des engins de chantiers compatibles, qui ne sont pas si disponibles, et éventuellement : en rétrofiter certains 

---

### Bibliographie et références
@lgp01
@lgp02
@lgp03
@lgp04
