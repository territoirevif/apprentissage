---
title: "t703. La sobriété en eau"
subtitle: "Quelques exemples d'actions en ville"
author: Marc Le Bihan

references:
   - id: lgp01
     author: Catherine Abou El Kahir
     title: "Transition écologique : apprendre la sobriété en eau"
     container-title: Le journal du Grand Paris
     issue: 65
     page: 3
     type: article-journal
     issued:
       year: 2024
       month: 11
---

## Paris

### Risque

Le réchauffement climatique qui réduira d'ici à 2070 - 2100 :

  - le débit des cours d'eau de $30\%$
  - la recharge des nappes souterraines de $10\%$

### Objectif

La ville de Paris a réduit sa consommation en eau de $40\%$ entre 2019 et 2020.  
Elle veut encore réduire de $10\%$ les consommations en eau potable et non potable.

### Consommation de l'eau non potable

   - l'arrosage des jardins et des parcs
   - l'alimentation des rivières et des lacs dans le bois de Vincennes et de Boulogne
   - le nettoyage des rues  
   - le curage et l'entretien des égouts, qui représentait en 2024 près de $50\%$ de la consommation : ce sera la plus grande part de ses efforts  
   - les piscines municipales, qui font $20\%$ de la consommation. Elle voudrait réduire leur consommation de $35\%$  
   - fuites d'eau dans le réseau : surveillance par $3 000$ capteurs, installés depuis 2022 

---

### Bibliographie et références
@lgp01
