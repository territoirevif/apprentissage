---
title: "t706. Environnement : FREC"
subtitle: "Feuille de Route de l'Economie Circulaire"
author: Marc Le Bihan

references:
- id: lgp01
  container-title: Le journal du Grand Paris
  issue: 65
  type: article-journal
  issued:
    year: 2024
    month: 11
---

# I) La Feuille de Route sur l'Economie Circulaire (FREC) 

La Feuille de Route sur l'Economie Circulaire (FREC) est une \donnee{compétence régionale}.

Elle s'oppose à l'économie linéraire  
   \demitab qui prélève, transforme, utilise, élimine ($\implies$ déchets)  

\bigbreak

La \definition{Taxe Générale sur les Activités Polluantes (TGAP)}  
   \demitab est une fiscalité punitive sur les déchets incinérés ou enfouis.

## 1) La Filière Responsabilité Elargie des Producteurs (FREP)

Economie circulaire : \definition{Filière Responsabilité Elargie des Producteurs (FREP)}

   1. Approvisionnement durable
   2. Commande publique durable
   3. Eco-conception par l'industriel
   4. Economie de fonctionalité (on a l'usage, mais pas la propriété)
   5. Consommation responsable, collaborative (velib...). Digitalisation.
   6. Réemploi, Réutilisation, Réparation (RRR)
   7. Déchets (ressources, recyclage)

## 2) La Hiérarchie Européenne des Modes de Traitement 

S'en suit la \definition{Hiérarchie Européenne des Modes de Traitement}

   1. Prévention : éco-conception : éviter de concevoir le produit
   2. Réemploi/Réutilisation : allonger la durée de vie de l'équipement
   3. Recyclage
   4. Valorisation (énergétique, chaleur)
   5. Elimination (enfouissement, incinération sans valorisation énergétique)

La fillière de recyclage du BTP est très attendue  
   \demitab car il y a un problème de décharges ouvertes et d'acteurs mafieux.

## 3) Coût de la Feuille de Route

Problème des filières et de la gestion des déchets en général :  
   \demitab le coût a été bien supérieur à celui estimé.

La FREP coûte aux :

   - fabricants, \mauve{14 milliards d'euros par an}
   - marché des emballages, \mauve{700 millions d'euros}
   - Déchets d'Equipements Electriques et Electroniques (DEEE), \mauve{300 millions d'euros}

## 4) La Feuille de Route fait partie de la Responsabilité Sociale des Entreprises (RSE)

dans le cadre de la __Responsabilité Sociale des Entreprises (RSE)__,  
les entreprises doivent veiller au respect de l'économie circulaire. Par ailleurs :

   - L'association de notation extra-financière _VIGEO_ les note,  
     et un classement peut servir __lors de commandes publiques__ à départager des dossiers équivalents.
   
   - Le \definition{Pacte sur l'Améloration de la Compétitivité des Entreprises (PACTE)} définit les entreprises à mission. 
   
   - Les banques proposent souvent des _Investissements Socialement Responsables_.

# II) L'économie circulaire dans l'aménagement

En 2021, 

   - $53\%$ des matériaux consommés en France sont nécessaires au secteur du BTP
   - ils sont presque $75\%$ des déchets produits

## 1) Réhabiliter et renoncer à construire

En aménagement, l'économie circulaire doit amener à \definition{écoconcevoir}, ce qui peut vouloir dire préférer réhabiliter et renoncer à construire.

En particulier, lorsque l'on a utilisé du béton, mieux vaut ne pas le déconstruire pour réduire l'empreinte carbone.  
(on croit pouvoir recycler le beton, mais son granulat recyclé n'est pas d'excellente qualité, et l'on utilise tout de même les carrières : il faut quand même utiliser du ciment)

## 2) Prolonger la durée d'utilisation du bois

Dans l'absolu, si le bois est capable de perdurer 120 ans dans une construction, un arbre aura poussé et sera arrivé à maturité quand il faudra le remplacer.

Si l'on utilise des méthodes mécaniques pour l'assemblage (au lieu de la colle) on peut démonter et réutiliser le bois ailleurs.

## 3) La ville low-cost

Ce n'est pas un non-usage des technologies, mais un usage modéré avec économie de moyens.  
Fillières locales, lutte contre l'obsolescence, compose avec ce qui est déjà là, en dimensionnant au plus juste et en concevant pour durer.

---

# Bibliographie et références

@cnam01
@lgp01