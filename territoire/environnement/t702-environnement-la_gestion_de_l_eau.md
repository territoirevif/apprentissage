---
title: "t702. Environnement : la gestion de l'eau"
subtitle: ""
author: Marc Le Bihan

references:
- id: lgp01
  author: Catherine Bernard
  title: "Campus Cachan : le tout-circulaire"
  container-title: Le journal du Grand Paris
  issue: 65
  page: 13
  type: article-journal
  issued:
    year: 2024
    month: 11
---

Grand cycle de l'eau : nappe d'eau $\rightarrow$ évaporation $\rightarrow$ nuages $\rightarrow$ pluie

Petit cycle de l'eau : captage $\rightarrow$ rejet des eaux usées (épurées)

## 1) La compétence gestion de l'eau{#eau}

C'est une \mauve{compétence communale} et \mauve{intercommunale}.

   - La commune doit assurer la distribution de l'eau, et qu'elle soit potable
   - L'intercommunalité, le bon état des installations

\bigbreak

Assuré via des services
 
   - \definition{Service Public d'Assainissement Collectif (SPAC)} : égouts
   - \definition{Service Public d'Assainissement Non Collectif (SPANC)}

\bigbreak

En délégation de service public, l'affermage est fréquent, rétribué par des redevances de l'usager : "_l'eau paie l'eau_". 

## 2) Aménagement

Le PLUi intercommunal définit un \definition{Schéma d'Aménagement et de Gestion des Eaux (SAGE)},

   > qui découle du \definition{Schéma Directeur d'Aménagement et de Gestion des Eaux (SDAGE)} régional,  
   > défini d'après le Schéma de cohérence territorial (SCOT).

## 3) Partage et lutte contre la pollution

Le \definition{Programme de Maîtrise des Polluants d'Origine Agricole (PMOA)}  
   \demitab punit d'amende tous les contrevenants à lla règlementation et pollueurs.

Les \definition{Comités de Bassins} sont des minis parlements de l'eau,  
   \demitab car les agriculteurs, industriels, usagers en ont tous besoin.

Autres acteurs : 

   - Agence Française de la Biodiversité
   - Office National des Milieux Aquatiques (ONEMA)

\bigbreak

\definition{eaux grises} ou eaux ménagères des eaux usées domestiques faiblement polluées issues d’évacuations d'une douche, d'un lavabo, d'un lave-linge et d'un lave-vaisselle. 

\definition{eaux noires} ou eaux vannes sont plus fortement polluées, issues des toilettes.

Pour gérer en interne les eaux pluviales du Campus Cachan, il a fallu s'occuper des remblais pollués qui empêchaient qu'on infiltre ces eaux, qui doivent d'abord être traitées.

De la même façon, l'infiltration à la parcelle d'eaux de ruissellement continu depuis des parkings nécessite leur dépollution. 

---

### Bibliographie et références

@cnam01
@lgp01