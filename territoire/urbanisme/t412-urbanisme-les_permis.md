---
title: "t412. Les Permis"
subtitle: "de construire, aménager, démolir... Et la déclaration préalable"
author: Marc Le Bihan
keywords:
- permis de construire
- architecte
- ELAN
- construction de bâtiment
- rénovation de bâtiment
- transformation de bâtiment
- bâtiment particulier
- bâtiment industriel
- bâtiment agricole
- norme de construction
- pétitionnaire
- norme d'urbanisme
- norme de sécurité
- norme d'accessibilité
- habitat inclusif
- accessibilité personnes handicapées
- ascenceur
- droits à construire
- monument classé
- habitat indigne
- antenne-relais
- formulaire CERFA
- plan de raccordement au réseau
- image de mise en situation
- plan de situation
- cadastre
- carte
- parcellaire
- plan de masse
- parcelle
- emprise sur le sol
- plan de coupe
- hauteur du bâtiment
- esthétique
- plan de facade
- régulation thermique
- diagnostic de performance energie (DPE)
- gaz à effet de serre
- permis de faire
- résistance au feu
- accoustique
- aération
- caractéristique énergétique
- alternative innovante
- procédure d'obtention d'un permis de construire
- demande d'autorisation de permis de construire
- permis d'aménager
- terrain à bâtir
- taxe d'aménagement
- premis de démolir
- déclaration préalable
---

Le maire, autorité administrative, délivre quatre types de permis.

## 1) Le permis de construire

C'est un document officiel avec __CERFA__, pour la 

   - construction,
   - rénovation,
   - ou transformation

d'un bâtiment

   - particulier,
   - industriel
   - ou agricole.

### a) quelques normes de construction et d'urbanisme

Il vérifie que le __pétitionnaire__ respecte bien les normes de construction et d'urbanisme : normes de sécurité, d'accessiblité...

   - 10\% (\gris{ou 20\% ?} et non plus 100\%) des logements accessibles aux personnes handicapées. Les autres : évolutifs (ELAN). Notion d'habitat inclusif.
   - 3 étages et plus $\implies$ ascenceur

   - Davantage de droits à construire si libération de logements vacants (ELAN).
   - Les \avantage{Architectes des Bâtiments de France (ABF)} n'ont plus qu'un avis consultatif

      - sauf si un monument classé peut être impacté,
      - et encore peut-il être outrepassé lors d'un projet de réhabilitation d'un habitat indigne (ELAN),  
      - ou l'implantation d'une antenne-relais.

   - Les opérations de conception ou réalisation sont passables en un seul marché : les architectes devront s'associer aux acteurs du bâtiment (ELAN).

### b) Le contenu du permis de construire

(conforme au PLUi ou RNU s'il n'y en a pas), le permis de construire comprend :

   - Fiche standardisée (Formulaire CERFA)
   - Notice descriptive
   - Images de mise en situation : documents graphiques, et photographie
   - Plan de raccordement au réseau
   - Plan de situtation : carte, cadastre (parcellaire)

   - Plan de Masse. Il donne : 

       - la densité du bâtiment sur la parcelle
       - l'emprise au sol

   - Plan de coupe :

       - hauteur du bâtiment
       - angle
       - silhouette esthétique

   - Plan de facade : facades côté rue, arrière, de côtés.
   ...

### c) Régulation thermique et Diagnostic de Performance Energie (DPE)

Le permis de construire est remplacé par [une __déclaration préalable__ pour certaines constructions (ci-dessous)](#declaration-prealable).

\underline{Régulation thermique :}

(Loi Grenelle 1 du 3 Août 2009) Ensemble de règles à appliquer dans le domaine de la construction, afin d'augmenter le confort des résidents, toiut en réduisant la consommation énergétique des bâtiments.

   > $\implies$ RT 2012, qui veut diviser par 3 la consommation énergétique des bâtiments.

\underline{Diagnostic de Performance Energétique (DPE) :}

Evalue la consommation énergétique d'un bâtiment (en KW/h/m²)  
et son impact en gaz à effet de serre.

Il est obligatoire en vente et en location.

### d) Le Permis de faire

Déroge à une règle de construction :
 
   - résistance au feu
   - aération
   - accoustique
   - caractéristique énergétique...

si une alternative innovante, équivalente, est certifiée par un laboratoire agréé.

### e) La procédure d'obtention d'un permis de construire

![Procédure](../images/procedure_publique_permis_de_construire.jpg)

![Circuit interne](../images/circuit_interne_permis_de_construire.jpg)

Depuis 2022, il y a téléprocédure des demandes d'urbanisme.

à la fin, c'est le maire qui décide.

Les demandes d'autorisation (permis de construire ou autre permis) peuvent être __instruites__ par des prestataires privés (marché public de services).  
Mais sans délégation de signature possible.

Les travaux mentionnés dans un permis de construire doivent commencer sous trois ans.

## 2) Le permis d'aménager

Pour un projet d'aménagement affectant le sol d'un terrain donné.

Définit des terrains à bâtir.

__Taxe d'aménagement__ :

Valeur d'assiette des terrain, exemple : $750 \text{€} / m^2$

\ \ \ + Majoration jusqu'à 20%, si équipements spécifiques requis : voirie...  
\ \ \ + Les communes décident d'un taux, de 1 à 5%  
\ \ \ + Ceux intercommunaux, départementaux.  

Dans le cas d'un _Projet Urbain Partenarial_, au lieu du paiement d'une taxe, l'entrepreneur finance une part.

## 3) Permis de démolir (n'est plus obligatoire)

(préalable à la démolition totale ou partielle d'un bâtiment).

Il est remplacé par le dépôt d'une __déclaration préalable__, sauf certains secteurs ou constructions protégées.

## 4) La déclaration préalable{#declaration-prealable}

Vérifie qu'un projet respecte toutes les normes et règles d'urbanisme en vigueur,
(que le PLU ou RNU donneront).

Pour les constructions peu importantes :

   - moins de 5 mètres carrés : il n'est même pas obligatoire,  
     mais il est conseillé pour des raisons fiscales (taxes)

   - pour les constructions entre 5 et 20 mètres carrés, d'une hauteur inférieure à 12 mètres,  
     il remplace le permis de construire

