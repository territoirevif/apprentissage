---
title: "t408. Le Schéma de Cohérence Territorial (SCOT)"
subtitle: ""
author: Marc Le Bihan
keywords:
- Schéma de Cohérence Territorial (SCOT)
- orientation de développement
- site naturel
- espace urbanisé
- politique de transport
- politique d'habitat
- intercommunalité
- Projet d'Aménagement et de Développement Durable (PADD)
- Schéma Régional d'Aménagement et Développement Durable et d'Egalité des Territoires (SRADDET)
- Plan Local de l'Habitat (PLH)
- Plan de Déplacement Urbain (PDU)
- Schéma Régional de Cohérence Ecologique (SRCE)
- Trame Verte et Bleue
- Zone Naturelle d'Intérêt Ecologique, Faunistique et Floristique (ZNIEFF)
- GEMAPI
- Programme d'Action et de Prévention des Inondations (PAPI)
- Programme d'Action et de Prévention des Risques Naturels (PPRN)
- Programme d'Action et de Prévention des Risques d'Inondation (PPRI)
---

Le Schéma de Cohérence Territorial :

   - présente les orientations générales de développement
   - désigne les espaces urbanisés et ceux libres
   - protège les sites naturels
   - assure la cohérence des politiques de transport, d'habitat...

## 1) La portée d'un SCOT

Un SCOT est rarement pour moins de 50 communes et a souvent une portée intercommunale.

   > Ainsi, le SCOT Ile-de-France n'a pas pour MOA la région IdF, mais la Métropole du Grand Paris, le Syndicat Paris Métropole, le SDRIF, et la Société du Grand Paris.

## 2) La règlementation que suit un SCOT

   Un SCOT est typiquement élaboré par un EPCI et s'impose aux documents d'urbanisme locaux, mais n'est pas opposable aux tiers.

\bigbreak

Pour la règlementation, il s'appuie sur le \definition{Schéma Régional d'Aménagement et Développement Durable et d'Egalité des Territoires (SRADDET)}

\attention{Mais certains aménagements échappent à ces schémas car dirigés directement par l'Etat !}

   - Par exemple, dans des \definition{Opérations d'Intérêt National (OIN)} comme les zones prioritaires.  
   Il crée alors, typiquement, des \definition{Etablissements Publics d'Aménagement (EPA)} qui délivrent des permis de construire.
   
   - Les \definition{Projets d'Intérêt Général (PIG)} sont issus d'arrêtés préfectoraux. Exemple : _Disney_.

\bigbreak

Site Patrimonial Remarquable (SPR)  

## 3) Les documents issus de lui

Il présente le \definition{Projet d'Aménagement et de Développement Durable (PADD)} $\rightarrow$ qui mène au \definition{Plan Local d'Urbanisme (PLUi)} $\rightarrow$ qui mène au __Permis de Construire__.

\bigbreak

Après études et diagnostics, une \avantage{Carte de destination générale}, peut dire quelles routes et équipements seront nécessaire, peut être produite. Elle n'est pas obligatoire.

### a) Documents périphériques du SCOT

   - \definition{Programme Local de l'Habitat (PLH)}
   
   - \definition{Plan de Déplacement Urbain (PDU)
      - Evaluations environnementales
      - Schémas commerciaux

### b) Environnement : documents prescriptifs

   - \definition{Schéma Réginal de Cohérence Ecologique (SRCE)}
   - \definition{Trame Verte et Bleue}
   - \definition{Zone Naturelle d'Intérêt Ecologique, Faunistique et Floristique (ZNIEFF)}
   - \definition{GEMAPI}
   - \definition{Programme d'Action et de Prévention des Inondations (PAPI)}
   - \definition{Programme de Protection des Risques Naturels (PPRN)}
   - \definition{Programme de Prévention des Risques d'Inondation (PPRI)}

