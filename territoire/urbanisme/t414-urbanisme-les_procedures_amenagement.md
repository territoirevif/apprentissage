---
title: "t414. Urbanisme : les procédures d'aménagement"
subtitle: ""
author: Marc Le Bihan
keywords:
- équipement de terrain
- procédure d'aménagement
- projet partenarial urbain
- permis de construire valant division
- parcelle
- projet partenarial d'aménagement (PPA)
- lotissement
- zone d'aménagement concertée (ZAC)
- terrain
- plan local d'urbanisme (PLU)
- aménageur
- société publique locale d'aménagement (SPLA)
- établissement public foncier (SPF)
- droit de péremption urbain (DPU)
- grande opération d'urbanisme (GOU)
- zone d'acquisition différée (ZAD)
- contrat de projet partenarial d'aménagement (PPA)

references:
   - id: lgp01
     author: Mathias Doquet-Chassaing, Directeur de l'aménageur Sadev 94
     title: "Nous subissons à la fois une crise du logement et du tertiaire"
     container-title: "Le journal du Grand Paris"
     issue: 65
     page: 7
     type: article-journal
     issued:
        year: 2024
        month: 11
---

Une procédure d'aménagement permet à une collectivité locale de réaliser ou faire réaliser de __l'équipement de terrain__, notamment, ceux qu'elle a acquis, __pour les céder ultérieurement à des acteurs publics ou privés__.

   - Etude d'impact
   - Choix d'un concessionnaire (souvent un SPLA)
   - Induit souvent la modification du PLUi.
   - Dossier de réalisation avec éventuellement un __Dossier d'Utilité Publique (DUP)__

\bigbreak

\underline{Habitats :}

   __R à R+1__ : petites maisons  
   __R+2__ : maisons moyennes  
   __R+3__ : maisons hautes
   __P__ ; place, espace de rencontre

__Projet Urbain Partenarial (PUP)__ : proposition des propriétaires fonciers  

__Permis de Construire valant division__ : zones accordées divisées en parcelles, regroupées en une même unité foncière qu'on redécoupe.

\definition{Projet Partenarial d'Aménagement (PPA)} (ELAN)

__Lotissement__ : Souvent en zone __AU__  

__Zone d'Aménagement Concertée (ZAC)__ : pour les grandes opérations ambitieuses.

## 1) L'Aménagement Foncier

Il peut amener la révision du PLU : étude d'impact, bilan prévisionnel...

\underline{Dépenses :}

   - On achète un terrain
   - Un urbaniste fait un plan
   - Le PLU est révisé
   - Un aménageur agit
   - puis, mise en vente

\bigbreak

\underline{Recettes :}
 
On peut envisager les charges foncières (où le logement rapporte plus que le bureau), mais c'est assez peu rentable, parce qu'à très long terme.

## 2) Les acteurs de l'aménagement

Les intercommunalités le confient souvent à un aménageur :

- une \definition{Société Publique Locale d'Aménagement (SPLA)}, qui est une SPL, donc public.
- Un \definition{Etablissement Public Foncier (SPF)}, qui est un propriétaire foncier.
- Aménageurs publics ou privés.

\bigbreak

Par ailleurs, les \definition{domaines} aident à évaluer la valeur des terrains. En les surestimant souvent.

## 3) les actions foncières

Il n'y a pas de prix plafond, et peu de mécanismes de régulation.

Il existe un \definition{Droit de Péremption Urbain (DPU)} mais \danger{gare à la collectivité locale qui récupèrera un terrain dont elle ne sait pas quoi faire !}

\bigbreak

Les Etablissements Publics Fonciers (EPF) n'ont pas beaucoup de budget pour acheter du terrain.

Dans les \definition{Grandes Opérations d'Urbanisme (GOU)}, une \definition{Zone d'Aquisition Différée (ZAD)} bloque les prix, et évite la spéculation (ELAN).

## 4) Le Contrat de Projet Partenarial d'Aménagement (PPA)

Les acteurs de la :

   - commune
   - intercommunalité
   - département
   - région
   - société publique locale
   - SPLA (aménagement dédié au foncier)

peuvent se réunir dans ce contrat, si le territoire a une finalité de logements (pas forcément à 100\%)

## 5) Les Grandes Opérations d'Urbanisme (GOU)

Si la nature des enjeux est __supra-locale__, l'intercommunalité peut créer une \definition{Grande Opération d'Urbanisme (GOU)}.

Sauf si une commune, incluse dans le projet, y est défavorable.

## 6) Les aménageurs face à la crise du logement et celle du tertiaire

Le marché de l'immobilier bloqué, font qu'il y a moins de projets et qu'ils doivent la charge financière des projets plus longtemps.

\bigbreak

Au début de la crise, certains ont vendu en bloc à des opérateurs de _Logement Locatifs Intermédiaire (LLI)_.  
Lorsque le marché était vif, il n'était pas rare qu'ils demandent l'application de la Règlementation Énergétique (RE2020) à ses niveaux 2025 ou 2028, mais ils sont revenus en arrière.  

Le \definition{Coefficient de pleine terre} est le rapport entre la surface de la ou les parcelles et les espaces verts ; il est d'usage de l'imposer aujourd'hui, ainsi que la gestion de l'eau à la parcelle.  

La densification (liée au Zéro Artificialisation Nette) est de moins en moins acceptée par les habitants.

\bigbreak

De plus, si la demande de logement s'est arrêtée à cause du taux d'intérêt, elle peut repartir très vite, car elle existe toujours,  
autant, pour le tertiaire (bureaux, par exemple), le marché s'est contracté.

---

### Bibliographie et références
@cnam01  
@lgp01
