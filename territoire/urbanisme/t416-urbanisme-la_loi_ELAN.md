---
title: "t416. Urbanisme : la loi ELAN"
subtitle: "Loi portant Évolution du Logement, de l'Aménagement et du Numérique"
author: Marc Le Bihan
keywords:
- ELAN
- construction
- marchand de sommeil
- lutte contre les recours abusifs
---

\definition{23/11/2018 : Loi portant évolution du logement, de l'aménagement et du numérique (ELAN)} :

   - faciliter la construction
   - produire mieux et moins cher
   - réorganiser le logement social

## 1) Lutte contre les marchands de sommeil

   - Hôtels (trop de personnes dans une chambre)
   - Logements collectifs

\bigbreak

$\rightarrow$ Institution d'une \definition{présomption de revenus}, comme pour la drogue ou la prostitution, contrefaçon : avec __inversion de la charge de la preuve__, au profit de l'administration.

Amende élevée

## 2) Lutte contre les recours abusifs

Un \definition{Recours en excès de pouvoir (REP)} doit être fait devant un tribunal administratif :

   - un juge donne un délai,
   - où il dira sa décision en l'état (ELAN)

\bigbreak

$\rightarrow$ Il y a obligation \definition{de recours contre l'autorisation initiale} :

   - on ne s'attaque plus à un bâtiment un par un,
   - mais à l'ensemble du projet
