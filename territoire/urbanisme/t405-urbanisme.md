---
title: "t405. L'Urbanisme et le Code de l'Urbanisme"
subtitle: ""
author: Marc Le Bihan
keywords:
- terrains à bâtir
- construction de bâtiment
- recomposition urbaine
- réhabilitation
- logement insalubre
- code de l'urbanisme
- urbanisme
- parcelle
- règlement général des sols
- autorisation à bâtir
- zone particulière
- filacom
- sitadel
- marché immobilier
---

L'Urbanisme opérationnel, c'est la :

   - Fourniture de terrains à bâtir
   - Construction de bâtiments
   - Traitement de quartiers et immeubles (recomposition urbaine, réhabilitation, résorbtion des logements insalubres)

Les services de l'Etat sont inégalement présents, et quand il intervient, c'est en Maître d'Ouvrage.

## 1) Le Code de l'Urbanisme

Le \definition{Code de l'Urbanisme} définit les règles que les collectivités locales doivent suivre. C'est surtout de l'investissement privé.

   - Chaque __parcelle__ de terrain est régie par le \definition{Règlement général des sols} qui donne les \definition{Autorisations à bâtir}.
   
   - Ceraines zones : montagnes, littoral, outremer... sont des __zones particulières__.

## 2) Indicateurs de l'Urbanisme

   - démographie
   - qualité des emplois
   - revenus centre/périphérie
   - marchés immobilier (vacance)

## 3) Sources statistiques

   - infra communales (IRIS), 
   - Filacom (accès limité),
   - Sitadel (construction, logement...)

     - éléments de calcul de taxe d'urbanisme,
     - fiscalité locale
     - les communes doivent y répondre avant le 15 de chaque mois.

