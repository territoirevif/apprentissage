---
title: "t418. Autres actions sur le parc immobilier"
subtitle: ""
author: Marc Le Bihan

references:
   - id: lgp01
     author: Fabienne Proux
     title: "L'optimisation du parc immobilier en vue"
     container-title: "Le journal du Grand Paris"
     issue: 65
     page: 4
     type: article-journal
     issued:
       year: 2024
       month: 11
---

## Évaluer le potentiel d'intensification des bâtiments pour éviter de construire

Jauger par un score, l'\definition{intensi'Score}, si des bâtiments sont vides (1) ou très occupés (5), et donc si des bâtiments publics ou privés pourraient l'être davantage, selon leur usage par heure, par semaine ou par an.

   - Donner les compatibilités d'usage qu'il peut y avoir entre les différentes activités, selon les endroits
   - et s'adapter selon les règles relatives à la sécurité incendie, le PLU, l'assurance 

---

### Bibliographie et références
@lgp01
