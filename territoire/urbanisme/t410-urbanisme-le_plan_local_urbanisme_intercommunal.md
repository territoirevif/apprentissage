---
title: "t410. Le Plan Local d'Urbanisme intercommunal (PLUi)"
subtitle: ""
author: Marc Le Bihan
keywords:
- Plan Local d'Urbanisme (PLU)
- Plan Local d'Urbanisme Intercommunal (PLUi)
- parcelle
- orientation d'aménagement
- règlement de zone géographique
- Règlement National d'Urbanisme (RNU)
- Loi Aménagement Logement et Urbanisme Rénové (ALUR)
- mitage
- Loi Transition Écologique et de la Croissance Verte (TECV)
- Etablissement Recevant du Public (ERP)
- précarité énergétique
- isolation
- Plan d'Occupation des Sols (POS)
- bailleur social
- logement social
- ELAN
- Loi Solidarité Renouvellement Urbain (SRU)
- Règlement PLU
- unité foncière
- emplacement réservé
- zone de servitude
- droit de délaissement
- expropriation
- destination d'une construction
- espace boisé classé
- plantations obligatoires
- zone urbaine existante (U)
- zone à urbaniser (AU)
- zone naturelle (N)
- zone agricole (A)
- article de règlement PLU
---

# I) Le Plan Local d'Urbanisme intercommunal (PLUi)

## 1) Objectifs et organisation du PLU

Le plan local d'urbanisme est uen vision stratégique d'aménagement qui définit les orientations du projet communal. Il est conforme au \definition{Code de l'Urbanisme}.

En cas d'absence, c'est le \definition{Règlement National d'Urbanisme (RNU)}, un court texte de cinquante articles, qui se substitue.


\bigbreak

Il définit les __règles d'agencement__ : implantations et alignements possibles ou non, hauteurs possibles, obligation de parking...

Les lois fixent les grandes règles générales. 

   - La région a son \definition{Schéma de Développement Régional} et son \definition{Plan de Déplacement Urbain (PDU)}  
   - Les métropoles ou intercommunalités ont un \definition{Schéma de Cohérence Territorial (SCOT)}  
   - L'établissement Public Territorial fait le \definition{Plan de Développement}, le \definition{Programme Local de l'Habitat (PLH)}, le \definition{Schéma de Développement Commercial (SDC)}

\bigbreak

Le PLU réglemente toutes les parcelles du territoire.

   - il a aussi son PADD.
   - et dit les orientations d'aménagement.
   - Le règlement des zones géographiques
   - et les procédures d'orientation et d'action

\bigbreak

Ces documents affectent le PLUi, qui a plusieurs parties :

   - Le \definition{Plan d'Aménagement et de Développement Durable (PADD)} : "_Voici la ville que nous voulons_" $\rightarrow$ vision à $10$ à $15$ ans.  
   - Le \definition{Plan de Zonage} : à tel endroit plutôt des entreprises, ici les habitats, et là : préserver les forêts. "_Où l'on fait quoi_".  
   - Le \definition{Règlement} :  à tel endroit, l'on peut mettre des constructions en bois...

## 2) Chronologie de sa mise en place

\definition{24/03/2014 : Loi Aménagement Logement et Urbanisme Rénové (ALUR)}

   - Interdit le mitage, pour densifier et bâtir en hauteur
   - c'est elle qui dit que le PLU sera maintenant intercommunal

\bigbreak

\definition{17/08/2015 : Loi Transition Ecologique et de la Croissance Verte (TECV)}

   - Exemplarité des constructions publiques, \underline{dont les Etablissements Recevant du Public (ERP)}
   - Lutte contre la précarité énergétique :

      - Aide à l'isolation, double vitrage... Pour ceux qui le peuvent pas.

\bigbreak

La loi \definition{Solidarité Renouvellement Urbain (SRU)} du 13/12/2000  
   \demitab a supprimé le Plan d'Occupation des Sols (POS)  
   \demitab et les a remplacés par le \definition{Plan Local d'Urbanisme (PLU)}.

   \demitab elle dit aussi que doit avoir 20% (aujourd'hui 25%) de logements sociaux.

   - Un opérateur devra posséder 1 500 logements (ELAN).
   - Les bailleurs sociaux peuvent vendre leur parc à quelqu'un d'autre qu'à l'occupant (ELAN).

## 3) Le transfert des PLU des communes aux intercommunalités (PLUi)

Le PLU est Souvent communal, mais de plus en plus intercommunal. 
grâce à lui, une intercommunalité peut contracter, et \avantage{dévérrouiller dans une commune une implantation qui la bloque} (ELAN).

   > toutes les communes ne lui l'ont pas encore transféré. Car des minorités de blocage ont pu le laisser à la commune.  
   > Certaines n'ont pas encore de PLU : RNU $\rightarrow$ carte communale $\rightarrow$ PLU $\rightarrow$ PLUi)

## 4) Le règlement PLU : Zonage

### a) Vocabulaire : Unité foncière, emplacements réservés, servitudes...

Une \definition{unité foncière} est une propriété unique, non coupée par une voie.

Il y a des __emplacements réservés__ pour des écoles, logements sociaux.  
Et des \definition{zones de servitude} : passage de canalisation de gaz, par exemple.

\bigbreak

Un propriétaire qui ne peut plus jouir de son terrain comme il l'entend peut faire valoir son \definition{Droit de délaissement} (négociation à l'amiable) ou attendre l'\attention{expropriation}.

### b) Le règlement PLU

La règlementation organise la répartition du territoire en zones, en fixant pour chacune :

   - leur genre
   - les conditions d'utilisation du sol

\bigbreak

Destination des constructions  
Caractéristiques  
Equipement et réseaux

\bigbreak

Les \definition{prescriptions} sont des restrictions, des conditions :

   - espace boisé classé
   - plantations obligatoires

### U : zone urbaine existante

   > __UA__ : Zone urbaine dense  
   > __UB__ : Zone de pavillons  
   > __UC__ : Tissu lâche, et hameau

### AU : zone à urbaniser
 
   > __1AU__ : Zone d'habitat à court terme  
   > __2AU__ : Zone d'habitat à long terme

### N : Zone naturelle

### A : Zone agricole

### c) Le contenu des articles du règlement PLU

   > __1 & 2__ : Interdits ou soumis à condition  
   > 
   > __3, 4, 5__ : Desserte et caractéristique des terrains  
   > 
   > __6, 7, 8__ : Implantation des constructions par rapport aux voies, aux limites séparatives, aux autres constructions (prospects).  
   > 
   > __9__ : Emprises \avantage(Coefficient d'Emprise au Sol (CES)).  
   > 
   > __10__ : Hauteurs  
   > 
   > __11__ : Aspects  
   > 
   > __12__ : Stationnement  
   > 
   > __13__ : Espaces extérieurs  
   > 
   > __14__ : Coefficient d'Occupation des Sols (COS) : abrogé en 2014.

# II) La révision d'un plan local d'urbanisme

La révision du PLUi (différent d'une modification qui affecte une zone), se fait tous les $10$ - $15$ ans.

Elle rassemble (fait intervenir) les \definition{Personnes Publiques Associées}

   - Département
   - Région
   - Chambres consulaires : Chambre de Commerce et d'Industrie (CCI), CCM
   - Chambre des notaires
   - Service de l'Etat

\bigbreak

Elle prend aisément $3$ ans : état des lieux, diagnostic environnement, concertation du public...

   - Maîtriser l'évolution urbaine : production de logements (dont sociaux), besoins en équipements publics  
   - Maintenir les activités économiques
   - Lutter contre l'\definition{artificialisation des sols} : imperméables $\rightarrow$ inondations

## 1) Environnement

- Qualité de l'air
- Gestion des déchets
- Nuisances sonores
- Biodiversité et Agenda 21
- Fils verts (continuités végétales)

## 2) Logement

$2.2$ personnes / ménage, en moyenne : les divorces sont plus fréquents. Viellissement de la population

- Parcours résidentiel (logement social $\rightarrow$ locataire privé $\rightarrow$ propriétaire)
- Pas assez de petits et grands logements.
- \mauve{Clauses anti-spéculatives}, pour lutter contre le problème des prix.
- Actions contre l'habitat indigne.

## 3) Equipements publics

- Cinéma
- Nouvel équipement sportif
- Espaces verts...

## 4) Développement économique

- Friches urbaines à réaffecter
- Artisanat sur place à maintenir
- Rez-de-Chaussée sur rues pour commerces

## 5) Paysage urbain (architecture)

On parle de \mauve{polarités} : là où se passent les choses.

   - Valoriser les carrefours, places
   - Préserver les \mauve{franges communales} : près des voies ferrées, entrées et sorties de villes

## 6) Déplacements

- Pistes cyclables
- Rues piétonnes
- Question du stationnement
- Question du stationnement
