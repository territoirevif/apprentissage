---
title: "t470. Quelles politiques publiques hors des villes ?"
subtitle: "où ruralité signifie: rural + très petites villes"
author: Marc Le Bihan
---

La ruralité, c'est \mauve{41\% de la surface du territoire}, et environ \mauve{17\% de la population} dessus.

# I) Quelles politiques publiques pour les territoires ruraux ?

## 1) Le Schéma Départemental d'Amélioration de l'Accessibilité des Services Publics (SDAASP)

L'Etat et le département élaborent un \definition{Schéma Départemental d'Amélioration de l'Accessibilité des Services Publics (SDAASP)} (2015, issu de la Loi NOTRe).

   - Durée : six ans
   - Programme d'actions pour renforcer l'offre de services, et la mutualiser

Un service public est accesible, d'après :

   - son temps et facilité d'accès
   - coût, tarif
   - niveau de qualité... et ressenti de l'usager
   - choix entre opérateurs
   - information sur l'existence du service

exemple : mobilité des usagers, santé : mutualisation, numérique...

Mais ses diagnostics sont de qualité inégale, l'ensemble est peu lisible, et le SDAASP manque de moyens financiers.

## 2) La Maison de Service au Public (MSAP)

Elle améliore l'accès à tous les services (Etat, EPCI, Collectivité Locale)  
cf [t472. La Maison de Service au Public (MSAP)](./t472-la_maison_de_service_au_public_MSAP.pdf)

## 3) Dotation d'Equipement aux Territoires Ruraux (DETR)

(Reçue par le département, qui décide comment la répartir entre Commune et Intercommunalité)

Projets :

   - économiques
   - sociaux
   - environnementaux
   - sportifs
   - touristiques

favorisant / maintenant les services publics en milieu rural  
Financement : 1 milliards d'euros

## 4) Dotation de Soutien à l'Investissement Local (DSIL)

   - Zones blanches téléphoniques
   - Transition énergétique
   - Bâtiments scolaires

Financement : 570 millions d'euros

## 5) Dotation de Soutien à l'Investissement des Départements (DSID)

Projets en faveur de la cohésion des territoires

   - Couverture haut-débit
   - Protection de l'enfance
   - Accès service scolaire

Financement : 212 millions

## 6) Fond National d'Aménagement et de Développement du Territoire (FNADT)

Finances locales pour projets ayant plusieurs partenaires locaux :

   - Emploi
   - Production locale
   - Co-working
   ...

## 7) Dotation de Solidarité Rurale (DSR)

La Dotation Globale de Financement (DGF) la contient :

   - Maintenir la vie sociale
   - Compenser les insuffisances rurales
   - Charges des communes rurales

Financement : 1.5 milliards d'euros

# II) Contractualisation : les contrats de ruralité

(avec un engagement pluri-annuel de l'Etat)  
Contrat avec l'Etat sur des projets structurants, mobilisant Région, Europe (LEADER), banque des territoires, chambres consulaires comme financeurs.

## 1) Réalisations via le Pôle d'Equilibre Territorial et Rural (PETR)

Accès aux services publics, cohésion sociale et revitalisation  
(l'Association des Maires Rureaux de France (AMRF) demande de ne pas se limiter aux villes centres)

Exemples : 

  - piscine, qui va servir à beaucoup  
  - travaux touristiques sur les berges
  - itinéraires cyclables, reliant les villages

480 contrats de ruralité

## 2) Programme "Action Coeur de Ville"

cf. [t902. La revitalisation des centres bourgs](revitalisation/t902-la_revitalisation_des_centres_bourgs-anct.pdf)

