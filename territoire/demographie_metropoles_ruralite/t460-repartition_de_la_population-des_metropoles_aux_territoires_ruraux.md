---
title: "t460. Répartition de la population"
subtitle: "des métropoles aux territoires ruraux (ruralité = rural + très petites villes)"
author: Marc Le Bihan
---

Sur les \mauve{67 millions de français} :

   - \mauve{40 millions} vivent dans les pôles urbains
   - \mauve{12 millions} dans couronnes des grands pôles
   - \mauve{83\%} vivent dans une grande aire urbaine
   - \mauve{41\% de la surface du territoire} est couverte par elles.

# I) Emploi et services

## 1) Emploi

Les couronnes et grands pôles progressent, tandis que les campagnes et petites villes régressent.

Les régions Bretagne, Île-de-France, Auvergne Rhônes-Alpes progressent, tandis que le Grand-Est et la Bourgogne Franche-Comté régressent.

Les aires de plus de $\mauve{500000}$ habitants ont un meilleur dynamisme: Montpellier, Nantes, Toulouse, Rennes...

\underline{Distanciation emploi - habitat:}

On ne travaille plus là où l'on habite;

   - \mauve{81.5\%} des emplois sont dans les grandes aires urbaines, et ce sont les plus qualifiés
   - Périurbanisation de l'habitat

## 2) Services

La présence d'équipements publics ou privés, de commerces et de services, est ce qui vous décidera à vous installer quelque-part.

\definition{Services de proximité} : supermarché, boulangerie dans beaucoup de communes isolées

\definition{Services intermédiaires} : coiffeurs... Dans les villes et grands pôles. Moins dans les villes moyennes

\definition{Services supérieurs} : opticien... Villes centres et grands pôles urbains. Pas dans les communes isolées.

\bigbreak

\underline{Besoins exprimés:}
\nopagebreak[4]

|            | Population            | Besoin                     |
| ---------- | --------------------- | -------------------------- |
| Rural      | vieillissante         | mobilité, services publics |
| Littoral   | retraités             | santé                      |
| Périurbain | familles avec enfants | éducation, crèche          |

## 3) Métropoles : attractivité, mais concurrence

\definition{Métropole} : une intercommunalité (EPCI) regroupant plusieurs communes d'un seul tenant.  
compétences : développement économique, écologique, cohésion, compétitivité, développement équilibré...

Les métropoles peuvent avoir beaucoup de communes rurales, pas forcément importantes.  
La ville centre n'est pas toujours prépondérante.

Dans certaines régions, les métropoles peuvent se faire concurrence.

# II) Les territoires ruraux

\definition{unité urbaine} : Zone de bâti continu $\rightarrow$ pas de coupure de plus de \mauve{200 mètres}, et qui compte plus de \mauve{2000 habitants}.

Une commune rurale n'appartient pas à une unité urbaine.

## 1) Cractéristiques des territoires ruraux

   - \mauve{15\%} de la population
   - à l'écart des zones d'influence des grandes villes et bassins d'emplois
   - mauvaise desserte par les grandes structures de transport
   - faible densité de population
   - population âgée

## 2) Services publics dans les territoires ruraux

Ils restent denses, mais avec une réoganisation.

Regroupement des services de gendarmerie, La Poste, éducation nationale

Pôle-Emploi, Préfectures... moins accessibles et remplacés par des guichets dématérialisés  
$rightarrow$ manque d'anticipation et de concertation

Services médicaux, dépendance personnes âgées : insuffisants

