---
title: "t210. Le processus législatif : élaboration de loi"
subtitle: "les procédures simplifiées, accélérées, vote bloqué, responsabilité engagée, concililation"
author: Marc Le Bihan
keywords:
   - loi
   - décret
   - arrêté
   - circulaire
   - livre blanc
   - livre vert
   - spill over
   - projet de loi
   - proposition de loi
   - exposé des motifs
   - étude d'impact
   - commission
   - amendement
   - assemblée nationale
   - sénat
   - procédure accélérée
   - responsabilité engagée
   - procédure de conciliation
   - commission mixte paritaire (CMP)
   - vote bloqué
   - promulgation
   - mise en application d'une loi
   - journal officiel (J.O.)
   - décret d'application
   - conseil constitutionnel
---

## 1) Origines des lois

Un \definition{texte de loi} peut avoir deux origines possibles :

   - gouvernemental : \definition{Projet de Loi}
   - parlementaire : \definition{Proposition de Loi}

## 2) Structure d'une loi

Ses articles sont préparés avec :

   - Un \definition{exposé des motifs}
   - Une \definition{étude d'impact}

Le Conseil d'Etat évalue la solidité des articles. Il peut émettre des réserves.

## 3) Processus législatif

Il y a un va-et-vient entre l'Assemblée Nationale et le Sénat, sauf pour les textes concernant les Collectivités Locales, où c'est l'inverse.

Au cours du processus parlementaire :

   - Des \definition{commissions} étudient des articles, suggèrent des modifications.
   - Il y un vote avec des \definition{amendements} possibles à l'Assemblée Nationale, puis c'est au Sénat.
   - Fin de la première lecture => \avantage{Si les textes sont identiques : le processus s'arrête-là}.

## 4) Les procédures simplifiées

### a) La Procédure accélérée

Passage en seconde lecture, dernier mot à l'Assemblée Nationale

### b) la Responsabilité engagée (49-3)

On vote sur la responsabilité du gouvernement, et non sur le texte de loi.

### c) la Procédure de conciliation

Une \definition{Commission Mixte Paritaire (CMP)} est constituée de 7 députés et 7 sénateurs.

Ils s'entendent sur une rédaction commune, qui a des chances d'être approuvée en seconde lecture.

### d) Vote bloqué

On vote sur tout le texte, et non article par article.

## 5) Promulgation et mise en application

- Promulgation officielle : Le Président de la République signe le texte de loi.
- Publication au Journal Officiel (J.O.)
- des Décrets d'application sont parfois nécessaires (et longs à venir...)
- le Conseil Consitutionnel (CC) peut après coup modifier une loi.

