---
title: "t210. Responsabilité pénale" 
subtitle: ""
category: territoire
author: Marc Le Bihan

abstract: |  
  Favoritisme, octroi d'avantages injustifiés, gestion de fait

keywords:
  - marchés publics
  - gestion de fait
  - comptable de fait
  - transparence
  - favoritisme
  - conflit d'intérêt formel, potentiel ou réel
---

# I) Infractions

Le \definition{favoritisme} ou \definition{octroi d'avantages injustifiés}

   > exemple, pour les marchés publics : des informations délivrées, mais pas à tout le monde.

\bigbreak
\avantage{\underline{Transparence :}} on ne vous écrit pas sur votre mail personnel, ou alors vous le rediffusez à tous.
\bigbreak

La \definition{gestion de fait} : je me retrouve __comptable de fait__.

   > exemple : le maire qui dirige une association, que la mairie subventionne.

\bigbreak

Le \definition{conflit d'intérêt} : 

   - __formel__ : même patronyme $\implies$ je m'écarte
   - __potentiel__ : on se connaît ! $\implies$ je justifie tout ce qui a été fait
   - __réel__ : \attention{par défaut tant que l'on a pas montré le contraire !}

