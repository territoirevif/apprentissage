---
title: "t015. La décentralisation"
subtitle: ""
author: Marc Le Bihan
keywords:
   - décentralisation
   - tutelle
   - contrôle de légalité
   - collectivité locale
---

La \definition{décentralisation} est issue de la

#### \definition{2 Mars 1982 : Droîts et Libertés des Communes, Départements et Régions}

- Pas de tutelle d'une Collectivité Locale sur une autre
- Pas de contrôle à priori ou postériori de l'état, sauf de légalité

