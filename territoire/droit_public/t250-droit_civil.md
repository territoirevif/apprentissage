---
title: "t250. Droit Civil"
subtitle: "Arrêt Blanco"
author: Marc Le Bihan
keywords:
  - arrêt Blanco
  - arrêt
  - acteur public
  - pouvoir
  - droit civil
  - droit privé
---

### \definition{1873 Arrêt Blanco}

   > Les acteurs publics ont des pouvoirs définis par le droit civil, mais ils sont limités aussi par lui, et doit se concillier avec les droits privés.

