---
title: "t260. Les pouvoirs de police du Maire"
subtitle: ""
author: Marc Le Bihan
keywords:
- maire
- pouvoir de police
- officier de police judiciaire
- témoignage
- procès verbal
- interpellation
- prévention des délits
- Conseil Locaux de Prévention de la Délinquance (CLPD)
- sûreté
- accident
- signalisation
- baignade
- sécurité
- édifices
- vitesse limite autorisée
- piste cyclable
- sécurité activité de loisir
- Etablissement Recevant du Public (ERP)
- salubrité
- effondrement
- insalubrité logement
- maladie contagieuse
- marché aux comestibles
- tranquilité publique
- bon ordre
- urbanisme
- affichage publicitaire
- environnement
- arrêté
- police municipale
- procureur de la république
- surveillance vidéo
- préfet
- transfert de pouvoir de police à l'intercommunalité
---

## 1) Le maire est Officier de Police Judiciaire 

   - il peut dresser des procès verbaux,
   - collecter des témoignages,
   - agir en cas d'incident,
   - interpeller un suspect

\bigbreak

Mais sitôt celui-ci interpellé, il doit le remettre à la police nationale,  
et il ne peut débuter d'enquête de lui-même.

## 2) Il a des pouvoirs de Police Administrative

- il cherche à __prévenir__ les délits
- il coordonne les acteurs (état, ville, associations) dans des \avantage{Conseils Locaux de Prévention de la Délinquance (CLPD)}

\bigbreak

- \definition{Sûreté} : 
   - prévenir les accidents
   - signalisation sur la voie publique
   - baignades dangereuses

\bigbreak

- \definition{Sécurité} : 
   - risques liés aux édifices et aménagements publics
   - vitesse de circulation, pistes cyclables
   - activités de loisir publiques (ex : salles de concerts)
   - contrôle des \avantage{Etablissements Recevant du Public (ERP)}  
   - Non lié au pouvoir de police du maire, mais dans le cadre des Politiques Sociales :  
     une commune peut mettre en oeuvre des prestations spécifiques pour lutter contre les stupéfiants, par exemple.

\bigbreak

- \definition{Salubrité} : 
   - Enfondrement immeubles ou insalubrité des logements
   - Maladies contagieuses
   - Marché aux comestibles
   - Tranquilité publique

\bigbreak

- \definition{Bon Ordre} : cas particuliers, sujets à interprétation

## 3) et un pouvoir de police spécial

- Urbanisme : interrompre les travaux sans permis
- Régulation de l'affichage publicitaire
- Pouvoir environnementaux

## 4) Moyens d'actions du maire de ses pouvoirs de police

Il agit : 

- Au moyen d'__arrêtés__ (règlementaire)
- par la __Police Municipale__ (moyens humains et matériels) après agrément de celle-ci par le procureur de la république

- surveillance vidéo : dans le domaine strictement public, dans une zone signalée, accès par des personnels habilités par le préfet.

\bigbreak

\underline{Remarque :} \attention{Le maire peut transférer ses pouvoirs de police à l'intercommunalité, mais alors il les perd tous.}

