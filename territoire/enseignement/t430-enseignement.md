---
title: "t430. Enseignement"
subtitle: ""
author: Marc Le Bihan
category: territoire
keywords:
- enseignement
- éducation
- rectorat
- état
- rythme scolaire
- niveau scolaire
- programme scolaire
- pédagogie
- enseignant
- collectivité locale
- établissement scolaire
- école
- cantine
- sortie scolaire
- autocar
- centre de loisir
- périscolaire
- temps méridien
- agent technqiue des écoles maternelles (ATSEM)
- ouverture de classe
- fermeture de classe
- éducation nationale
- zone d'éducation prioritaire (ZEP)
- dédoublement de classe de CP
- temps d'activités périscolaires (TAP)
- service d'accueil minimum
- instruction obligatoire
- socle commun de connaissances
- maternelle
- carte scolaire
- école publique
- école privée
- sous contrat
- hors contrat
- école confessionnelle
---

L'éducation est une compétence de l'état : un service public national.

## I) La répartition des responsabilités

## 1) Etat

   - Il fournit le personnel (le \definition{rectorat} affecte les enseignants)
   - dit la pédagogie et les programmes
   - les rythmes et niveaux attendus

## 2) Collectivités locales

L'Etat confie la gestion des établissements aux collectivités locales :

   > c'est le deuxième poste d'investissement des communes  
   > deuxième des régions  
   > troisième des départements

Elles sont chargées de leur construction et entretien.

\bigbreak

Elles financent les \definition{Agents Techniques des Ecoles Maternelles (ATSEM)}, qui entre les temps de cours :

   - accueillent 
   - assurent la cantine
   - les sorties scolaires

\bigbreak

Elles organisent le transport scolaire :

   - Les cars viendront de la CL, si c'est l'enseignant qui décide d'une sortie

\bigbreak

Elles assurent aussi les temps d'__activités périscolaires__ : 

   - centres de loisirs en période scolaire
   - ou durant les vacances

## 3) Qui décide de fermer ou d'ouvrir des classes ?

L'Etat choisit où, car l'Education Nationale fournit l'enseignant, mais la collectivité locale : 

   - choisit la localisation
   - l'aménagement
   - le nombre de classes
   - et lance les marchés

\bigbreak

L'Education Nationale décide aussi de quand elle en ferme.

   - Manque d'enseignants !
   - En deçà d'un certain seuil, on ferme.  
     mais cela dépend aussi des _Catégories Sociaux Professionnelles (CSP)_ :

      - Politique de la Ville et Zones d'Educations Prioritaires : dédoublement de classes de CP.

# II) Instruction et rythme scolaire

## 1) Rythme scolaire

Le maire choisit __4__ ou __4.5__ jours, d'après le cadre légal.

Les communes ont l'obligation de mettre en place des \definition{Temps d'Activités Périscolaires (TAP)} durant les __temps méridiens__.

### Service d'accueil minimum

En cas de grève, les CL doivent mettre en place un service minimum d'accueil, si l'Education Nationale ne peut compenser les absences de professeurs.

   - si les grévistes ne dépassent pas 25\%.
   - et que le personnel de la ville n'est pas lui-même en grêve.

## 2) Instruction obligatoire

L'instruction (et non l'éducation) est obligatoire :

   - école publique ou privée (aux 6 ans de l'enfant, les parents doivent dire leur choix à la mairie).
   - personnes de son choix  
   - par les parents, mais alors ils doivent respecter un \definition{Socle Commun de Connaissances}. S'ils le font, ils doivent notifier aux 6 ans de l'enfant, leur choix à l'Education Nationale.

jusqu'à 16 ans et à partir de 3 ans (maternelle).

## 3) Ecoles publiques, écoles privées, et types de contrats

### a) Ecoles publiques (80\% des élèves) : Etat

\definition{Carte scolaire} : on a pas le choix de son école (publique), même si une demande de dérogation est possible.

Cela fait que pour des raisons de transport ou de facilité, les parents choisissent parfois le privé.

### b) Ecoles privées (20\%)

   - __sous contrat__ : l'état les contrôle fortement. Les collectivités locales leur versent un forfait annuel : le coût d'un enfant dans une école publique.  
   - __hors contrat__
   - __confessionnelles__ : problème de sectes qui ont pu s'introduire
   - __non confessionnelles__

\bigbreak

\underline{Remarque :}

Les collectivités locales n'ont alors pas d'obligation de construction ou d'entretien.

