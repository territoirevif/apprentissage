---
title: "t458. Le stationnement payant sur voirie"
subtitle: "et sa réforme"
author: Marc Le Bihan
---

Le but des frais de stationnement est de favoriser la rotations des véhicules.

\bigbreak

Depuis la loi MAPTAM, la contravention de 17 euros est remplacée depuis 2018 par une \definition{Redevance d'occupation du domaine public} : le \definition{Forfait Pour Stationnement (FPS)}.

   - Son montant est fixé par l'assemblée délibérante de la collectivité locale,
   - la redevance : c'est le coût du stationnement
   - le forfait : la contravention

\bigbreak

Mais les recours sont très fastidieux, ainsi que les procédures de paiement.

