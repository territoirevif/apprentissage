---
title: "t450. Le transport : statistiques"
subtitle: ""
author: Marc Le Bihan
---

[transport.data.gouv.fr](https://transport.data.gouv.fr/)

Le transport, c'est un véhicule, des infrastructures et des services...  
et un droit constitutionnel : la liberté d'aller et venir.

## 1) Statistiques sur le transport

On se déplace davantage chaque année, surtout par véhicules particuliers  
\demitab \mauve{nombre de voitures par foyer \underline{équipé} : 1.5}

### a) Transports intérieurs

   - voitures particulières
   - transports ferrés : \mauve{1/7}
   - transports en commun : \mauve{1/8}

### b) Transport de marchandises

   - le routier domine : français d'abord, puis étranger (transitant)
   - ferré très faible
   - fluvial, encore plus

### c) La répartition du réseau routier

Les collectivités sont au premier plan, car \mauve{le réseau routier fait 1.1 millions de kilomètres} en France.

   - $705000$ km sont gérés par les communes
   - $378000$ km par les départements
   - $11700$ km par l'état : routes nationales, autoroutes gérées par l'état
   - $9200$ km sont des autoroutes gérées par des opérateurs privés

## 2) Economie et impact du transport

### a) Impact énergétique

\mauve{33\% de l'énergie finale}

   - \mauve{80\% par la route}

     - \mauve{60\% individuel}
     - \mauve{20\% utilitaires}
     - \mauve{14\% camions}

   - \mauve{15\% par l'avion}

### b) Économie

   - \mauve{14\% du PIB (48 milliards de recettes fiscales)}
   - \mauve{2.2 millions d'emplois} \gris{(autres chiffres : 1 millions d'emplois et 20 milliards d'euros de masse salariale ?)}

