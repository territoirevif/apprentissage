---
title: "t456. Le transport marqueur des inégalités territoriales"
subtitle: ""
author: Marc Le Bihan
---

## 1) Rural

   > Charges : \mauve{21\%} pour \mauve{7000 euros/an}, dont \mauve{90\%} imputables à l'achat et utilisation.  
   > Il y a plus d'une voiture par foyer, régulièrement, en rural.

## 2) Urbain

   > Charges : \mauve{16\%} pour \mauve{6200 euros/an}, dont \mauve{80\%} imputables à l'achat et utilisation.

En agglomération :

   > \mauve{6 à 11\%} des gens ne se déplacement pas un jour donné, en QPV : \mauve{9 à 15\%}.  
   > \mauve{12 à 21\%} sont sans voiture, en QPV : \mauve{29 à 41\%}.

\bigbreak

Avec de manière générale, une moindre mobilité des femmes par rapport aux hommes.

