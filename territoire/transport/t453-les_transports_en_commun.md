---
title: "t453. Les transports en commun"
subtitle: ""
author: Marc Le Bihan
---

Un réseau de transport en commun urbain (bus...)

   - s'exploite toujours de la même manière,
   - coûte toujours trop cher...


## 1) Situation des transports publics

Les transports publics sont :

   - en augmentation dans les centres urbains de province,
   - stables ailleurs, ou en légère augmentation

### a) Utilisations

   - Bus : \mauve{79\%}
   - Métro : \mauve{49\%}
   - Tramway : \mauve{44\%}
   - RER : \mauve{23\%}
   - TER : \mauve{18\%}

### b) Coûts mensuels

   - Voiture : \mauve{505 euros}
   - Transport en commun :

     - en province : \mauve{31 euros}
     - en Île-de-France : \mauve{75 euros}

### c) Les limites des AOM

La limite des AOM, c'est qu'elles ont :

   - de plus en plus de communes à désservir : $\times 2$
   - de surface à désservir : $\times 2$
   - une population plus grande à transporter : +50\%

\bigbreak

$\implies$ l'augmentation des périmètres n'est pas rentable : les coûts d'exploitation s'accroissent, particulièrement sur les petits réseaux, et ne sont pas couverts par les recettes tarifaires.

## 2) Qui finance les réseaux des collectivités ?

### a) Les usagers (recettes)

Dans les communes de plus de \mauve{250000 habitants} : \mauve{37\%},  
celles inférieures à \mauve{100000 habitants} : \mauve{19 à 24\%}

car les petits réseaux attirent moins.

### b) Les recettes des entreprises

   - Prise en charge des salariés à 50\%

   - __versement mobilité__ si la collectivité locale lève cet impôt au titre d'un service de la mobilité.  
     Plafond à :  
        \demitab \mauve{0.55\% de la masse salariale}  
        \demitab ou \mauve{2\%} pour les communes touristiques, EPCI...

     il tourne le plus souvent autour de  0.45\%, car \danger{il peut réduire l'attractivité} pour les entreprises !

### c) Les recettes des collectivités

   - subventions d'exploitation (général)
   - subventions d'équilibre, si une exploitation est déficitaire : par exemple, pour certaines lignes de transport public.

   - compenser __l'abandon de recettes__ de l'opérateur au titre de la __tarification solidaire__ instaurée par l'AOM.

      - Tarifs réduits pour certains publics 

         - RSA $\to$ gratuité, ...
         - Jeunes $\to$ tarifs réduits

   - le Covid 19 fait souffrir les compagnies de transports :

      > \listMoins de recettes d'usagers  
      > \listMoins d'apports d'entreprises

Car le coût réel d'un abonnement de transport public, est de :

   - \mauve{827 euros} en Ile-de-France,
   - \mauve{382 euros} en régions

|               | Usagers   | Employeurs   | Collectivités Locales   |
| --------------| :-------: | :----------: | :---------------------: |
| Petit réseau  | 11\%      | 46\%         | 43\%                    |
| Ile-de-France | 38\%      | 40\%         | 22\%                    |

## 3) La gratuité ?

\danger{chère et peu d'effets}

Si ce ne sont pas les entreprises qui paient, ce seront les collectivités locales. Comment ?

   - augmentation des impôts ?
   - des emprunts $\implies$ dette ?

ce seront ceux qui n'utiliseront pas les transports en commun, qui paieront...

Dans les faits, la fréquentation n'augmente pas alors que c'est la qualité de service qui est recherchée par les usagers.

