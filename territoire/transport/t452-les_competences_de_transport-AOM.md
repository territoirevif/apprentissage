---
title: "t454. Les compétences de transport"
subtitle: "les Autorités Organisatrices de la Mobilité (AOM)"
author: Marc Le Bihan
---

# I) Les Compétences de transport 

## 1) De la région

La région est chef de file du transport, jusqu'à l'inter-urbain (sauf le bloc communal) et est \definition{Autorité Organisatrice de la Mobilité (AOM), 24/12/2019}, elle organise :

   - les lignes des transports inter-urbains si la commune et l'intercommunalité n'ont pas souhaité le faire (en milieu rural, c'est assez fréquent).  

   - Les modes de transport ruraux et la déserte des zones peu denses, en général.

   - trains TER et transports inter-régionaux

## 2) Du département

   - Le transport des élèves en handicap

## 3) Communes ou intercommunalités

Elles choisissent d'être Autorités Organisatrices de la Mobilité ou non.  
Si elles ne le veulent pas, c'est la région qui tiendra ce rôle à leur place.

## 4) L'état

L'Etat programme les opérations à horizon de 20 ans via un Etablissemnt Pulic Administratif (EPA) : l'\definition{Agence de Financement des Infrastructures de Transport en France (AFITF)}

   > \mauve{3 milliards d'euros par an}, dont :

   - \mauve{1.1 pour les infrastructures routières}
   - \mauve{0.3 pour les transports collectifs en aglomération}
   - \mauve{0.024 pour les mobilités actives et cyclables}

# II) Organisation

La \definition{Loi d'Orientation des Mobilités} organise les AOM :  
\demitab les communes ou EPCI qui en font le choix.

À défaut, c'est le conseil régional qui le devient.

## 1) Coordination régionale dans des bassins de mobilité

À l'aide de plans pluri-annuels, réalisés avec :

   - les gestionnaires de voirie
   - d'infrastructures
   - EPCI  
   ...

à l'échelle de \avantage{bassins de mobilité}, des \definition{Contrat Opérationnel de Mobilité} sont établis.  
Ils disent :

   - la forme de mobilité
   - desserte, horaire, tarification
   - points d'échanges multi-modaux
   - recherche des opérations capables d'améliorer la cohésion sociale et territoriale

## 2) Les compétences des AOM

   1. Service réguliers de transport public
   2. Transport à la Demande (TAD)
   3. Transports scolaires
   4. Services de mobilité active
   5. Services d'usage partagé des véhicules
   6. Services pour handicapés et mobilité réduite
   7. Conseils pour les employeurs qui génèrent des flux de transports importants
   8. Développement des services de transport de marchandises et de logistique urbaine

Ces AOM doivent :

   - Faire un compte rendu annuel de leur Contrat Opérationnel de Mobilité, à destination des employeurs, collectivités locales...
   - Consulter les usagers avant des évolutions majeures ou tarifaires
   - Permettre le stationnement sécurisé des vélos
   - Inclure des voies en site propre dans les itinéraires cyclables :  
     la création/réaménagement de voies impose une étude de faisabilité d'un itinéraire cyclable.
   - Créer des \definition{Plan de Mobilite (PDM)} dans les agglomérations de plus de 100000 habitants :  
     ils reprennent les \donnee{Plan de Développement Urbain (PDU)},  
     en y ajoutant l'auto-partage, les pistes cyclables.
   - Partager en opendata les données de mobilité sur [transport.data.gouv.fr](https://transport.data.gouv.fr/),  
     en y alimentant un \definition{Point d'Accès Numérique (PAN)}

      - infrastructures de recharche
      - auto-partage
      - covoiturage

\bigbreak

Dès qu'il y a __au moins__ un service de mobilité __constitué et actif__, il est possible de prélever comme ressource fiscale un \definition{versement mobilité}.

