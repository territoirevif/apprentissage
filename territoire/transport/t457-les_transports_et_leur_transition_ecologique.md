---
title: "t457. Les transports et leur transition écologique"
subtitle: ""
author: Marc Le Bihan
---

La Loi \definition{Transition Ecologique et Croissance Verte} (TECV) de 2015, s'applique aux flottes d'au moins \mauve{20 bus}.

## 1) Le renouvellement des véhicules propres

   - 50\% en 2020,
   - 100\% en 2025

\bigbreak

À l'occasion des renouvellements de parcs de véhicules, les collectivités locales doivent s'assurer \mauve{20 à 37\%} d'entre-eux soient être propres.

## 2) Les bus et leurs coût (achat, frais, énergie...)

   - Diesel norme "Euro 6" : 520 k€
   - Electrique : 1 à 1.2 millions d'euros
   - Hybrides : 650 k€
   - Hydrogène

\bigbreak

Mais ces nouveaux véhicules coûtent bien plus cher : où va t-on trouver l'argent pour en financer de nouveaux ?

