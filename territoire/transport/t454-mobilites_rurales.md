---
title: "t454. Le développement de nouvelles mobilités rurales"
subtitle: "Le Plan de Mobilité Simplifié"
author: Marc Le Bihan
---

## 1) La situation des communes

   - 91\% des communes ont moins de 3500 habitants, et font 32\% de la population,
   - 53\% des communes ont mons de 500 habitants, et font 6.4\% de la population.

## 2) Le Plan de Mobilité Simplifié

Un \definition{Plan de Mobilité Simplifié} tient compte des plans élaborés par :

   - les entreprises
   - les représentants locaux
   - les associations
   - la Chambre de Commerce de d'Industrie (CCI)
   - la Chambre d'Agriculture

et est soumis à la consultation du public.

\bigbreak

Tous les acteurs de la mobilité échangent leurs expériences et se concertent sur un site Internet la [Plateforme France Mobilité](https://www.francemobilites.fr/plateforme).

## 3) Le court-voiturage

Une sorte d'auto-stop organisé à l'avance, avec une application, sur un parcours défini.

