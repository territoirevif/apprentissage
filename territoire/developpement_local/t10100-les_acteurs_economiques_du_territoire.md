---
title: "t01000. Les acteurs économiques du territoire"
subtitle: "développement économique"
author: Marc Le Bihan
---

# I) Les principes d'actions

## 1) Faire naître

- Pépinières d'entreprises 
- réseau de micro-crédit 
- Zones d'Activité Économiques (où elles se placent).
- Conciergerie (La Poste, Crêche, Pressing)

## 2) Faire durer

- Club d'entreprises
- Politique de veille
- Les entreprises doivent se développer

## 3) Faire venir

- Plaquette décrivant les atouts du territoire
- Salon des entreprises
- Déjeuners avec les chefs d'entreprises

## 4) Empêcher de partir

- Enracinement des activités et des salariés
- Fiscalité
- Présence commerces

## 5) Faire plus avec l'existant

- Choyer les entreprises
- Clubs d'entreprises
- Synergies

## 6) Faire plus en élargissant

- Élargir les champs d'action

   - Chambre de Commerce et d'Industrie
   - Chambre des Métiers et de l'Artisanat (CMA)

- Aider en innovant

## 7) Mieux faire

- Formation de la main d'œuvre
- Sensibilisation et valorisation de la formation

# II) Les dispositifs existants

## 1) Les Conventions de Partenariat

Valoriser les commerces en échange de la génération d'emplois locaux.  
Exemple : arrêt de bus supplémentaire.

## 2) Les clauses sociales dans les marchés publics

Il est possible de favoriser les personnes en difficulté ($5\%$ des heures de travail pour elles si contrat Agence Nationale de Rénobvation Urbaine (ANRU)) ou les emplois locaux.

## 3) PACTE Entreprises

- \definition{Emplois francs} : entreprise aidée de $5000$ euros sur 3 ans (CDI), $2000$ euros (CDD) si __Quartier en Politique de la Ville__  
- Achats responsables
- Plateforme de stages élèves de $3^\text{ème}$
- __Plans Locaux d'Insertion et d'Emploi (PLIE)__

## 4) FEDER / FSE

__FSE__ pour formation, reconversion, suite aux plans de licenciement  
Financements

## 5) Apprentissage

Lutte contre :

   - l'illettrisme
   - l'illectronisme
   - les discriminations

## 6) Offre immobilière et foncière

Pour commerces, services et activités.

- Les commerces vont \rouge{réclamer des hauteurs de plafond de $3$ mètres}
- Les sociétés, des étages plutôt que des rez-de-chaussée

Gaines de ventilation

## Le Schema Directeur d'Urbanisme Commercial (SDUC)

- Définit une stratégie
- Comment créer l'accueil
- Les services aux salariés

---

# Bibliographie et références

@cnam01