---
title: "t902. La revitalisation des centres bourgs"
subtitle: "le programme Petite Ville de Demain (PVD)"
date: "Webinaire Leader France du 30 Juin 2022"
author: synthèse du webinaire rédigé par Marc Le Bihan
abstract: | 
   Donner des moyens d'ingénieurie aux communes centres, pour la rénovation de l'habitat, la revitalisation des commerces,  
   et réorganiser le tourisme en zones de montagne.
keywords:
- outil
- service
- collectivité
- relance
- montagne
- petite commune
- petite ville
- Groupe d'Action Local (GAL)
- programme leader
- Contrat de Plan État-région (CPER)
- Contrat de Relance et de Transition Écologique (CRTE)
---

Fiche de lecture du [Webinaire Leader France : Comprendre les dispositifs de Agence Nationale de la Cohésion des Territoires (ANCT)](https://www.youtube.com/watch?v=Tw9_5nPVbk4)
 
\begingroup \small

   \demitab le public qui le suivait : Groupes d'Actions Locaux (GAL), Pôle d'Equilibre Rural et Territorial (PETR).

\endgroup

---

Les programmes de l'ANCT portent sur :

  - la Politique de la Ville
  - le Numérique
  - Territoires et ruralité

\bigbreak

Deux dispositifs d'_Action Coeur de Ville_, __Petite Ville de Demain__ et __Plan d'Avenir Montagne__ vont aider à organiser de l'ingénieurie. 

_Action Coeur de Ville_ avise les petites villes en grande difficulté sociale, dont les petits commerces disparaissent.

Sur cinq ans et sur cinq axes :

   - Réhabilitation de l'habitat
   - Développement économique et commercial
   - Mise en valeur de l'urbanisme, du patrimoine
   - Accès aux équipements, services publics

Jusqu'à deux ans de diagnostic :

   - Un comité de projet identifie des pistes de développement, éventuellement en demandant aux associations des idées (pistes cyclables, végétalisation...),  
   - puis une équipe projet agit, en mobilisant des investisseurs privés.

---

# I) Petite Ville de Demain

[Petite Ville de Demain](https://agence-cohesion-territoires.gouv.fr/petites-villes-de-demain-45) a été lancé en Avril 2020, il fait suite à _Action Coeur de Ville_. C'est une offre de services à destination des élus locaux, en particulier sur les moyens d'ingénierie.

$3$ milliards d'euros sont alloués, pour aller au delà de l'Agenda Rural Français et Européen, et du Pacte Rural Européen. En complément du :

   - Contrat de plan État-région (CPER)
   - Contrat de relance et de transition écologique (CRTE)

## 1) Communes destinataires

### a) Situation démographique des communes

$1626$ communes ont été sélectionnées en Octobre 2020 (soit $10\%$ de la population)  
   \demitab par les préfets de région et de département.  

   - $50\%$ des communes PVD ont moins de $3500$ habitants
   - $12.6\%$ des habitants des communes PVD ont plus de $75$ ans $\perp 9.3\%$ France entière.

Organisation: 

   - $1319$ dépendent d'un GAL

### b) à destination des communes ayant des fonctions de centralité sur le territoire

Les communes qui y entrent, au moment de leur choix, ont des fonctions de centralité (sur un territoire plus large) par :

   - leurs équipements
   - leurs services 

## 2) Entrée dans le programme

### a) Examens et études initiales

   - vérifier qu'un diagnostic du _Contrat de Relance et de Transition Écologique (CRTE)_ de la collectivité est bien achevé.  
   - réaliser une pré-étude _Opération Programmée d'Amélioration de l'Habitat (OPAH)_ de _Renouvellement Urbain (OPAH-RU)_  
     (Accompagnement financier par l'_Agence Nationale des l'Habitat (ANAH)_ et la _Banque des Territoires_)

\bigbreak

Sous $18$ mois, la commune doit définir :

   - un secteur d'intervention prioritaire
   - son plan d'action (axes stratégiques, fiches actions...)
   - sa stratégie de revitalisation pour entrer dans la convention cadre ORT.

### b) Son aboutissement : l'entrée en Opération de Revitalisation de Territoire (ORT)

Dont les opérations types sont : 

   - l'Habitat 
   - les Commerces (vacance commerciale) 

Un ORT se déroule dans la période 2020 - 2026 : la durée du mandat.

# II) Le but du programme PVD : l'Assistance à l'ingénieurie

Mobiliser les acteurs $\rightarrow$ bâtir son projet $\rightarrow$ renforcer ses équipes $\rightarrow$ passer à l'opérationnel

## 1) Renforcer les équipes, financer les études

### a) Renforcer les équipes des collectivités

Aide de l'état de $15000$ euros pour le \definition{Volontariat Territorial en Administration (VTA)} :  
   \demitab les jeunes diplomés gagnent une première expérience en millieu rural.

### b) Point-clef : le cofinancement d'un Chef de projet
      
Le programme Petite Ville de Demain cofinance des postes de Chef de Projet.

Binôme de l'élu, il suit les porteurs de projets, qui ont une logique ascendante (comme les acteurs de terrains, en général),  
et répond aux pouvoirs publics qui veulent promouvoir une politique particulière, dans une logique descendante (comme les financeurs).

Il réalise des fiches d'action (faites de la même façon que celles CRTE car les deux programmes sont imbriqués)

et suit, évalue, et anime :

   - des réunions dont les acteurs ont des logiques différentes
   - un comité technique
   - un comité de projet : avec le préfet de département et ses principaux partenaires

### c) Financement d'études

Financement d'études et de diagnostics (AMO)

   - sur le plan stratégique
   - sur le plan opérationnel

\bigbreak

Peuvent prendre part aux études : 

   - Centre d'études et d'expertise sur les risques, l'environnement, la mobilité et l'aménagement (CEREMA)  
   - Agences d'urbanisme

### d) Les limites de ce financement

Les financements sont multiples :

   - sur des besoins spécifiques : pour financer des actions définis dans le plan d'action
   - financement par l'état ou ses partenaires
   - des fonds régionaux peuvent y participer : il faudra respecter leur fléchage
   - les Etablissements Publics d'Aménagement (EPA) peuvent abonder
   - par la Banque des territoires

\bigbreak

Il va falloir mettre en cohérence des :

   - aides apportées par l'ANCT
   - par la région
   - par l'Europe via le programme LEADER

\bigbreak
 
Mais \danger{ce financement est sous forme de prêts bancaires} :  
\demitab ce n'est pas une enveloppe pluri-annuelle comme dans le cas d'un programme LEADER.

Et il faut aller chercher cet argent "_loin_" :  
   \demitab Montpellier ou Toulouse, par exemple, quand on est en Occitanie  
   \demitab et l'on est en concurrence avec d'autres collectivités, pour ces demandes.

## 2) Les thématiques

### a) Redynamisation commerciale

Avec le cofinancement des managers de commerce  

### b) Habitat 

   - habitat (dont dégradé et problématiques énergétiques)
   - habitat inclusif (lié à l'accroissement des personnes âgées)
   - patrimoine

### c) Vivre ensemble

   - vivre et faire ensemble
   - grandir, bien vivre, et veillir en bonne santé
   - Bus "_Services_"
   - participation citoyenne

### c) Numérique

   - accès au numérique
   - accès aux démarches de la vie quotidienne
   - Conseillers numériques

### d) Transition écologique

Au cours des projets, veiller aux impacts de la _Loi Climat Résillience_, que le programme PVD réclame d'anticiper immédiatement

   \demitab comme le Zéro Artificialisation Nette, par exemple.  

Une question d'un participant :

   > Les collectivités se saisissent d'opportunités financières pour leurs projets,  
   > comment faire pour que les territoires s'inscrivent aussi dans un projet partagé, vers la transition écologique ?

### e) Exemples de sujets concrets

   1. Lors du covid, le monde rural a été très attractif.  
   \demitab Comment proposer des services à ces nouveaux habitants ?

   2. "_Campus connecté_" : l'université de Perpignan avec un lieu de formation local, de Font-Romeu

# III) Plan d'Avenir Montagne : un sous-ensemble de PVD focalisé sur le tourisme

Définir une stratégie touristique pour des communes moins denses que celles de _Petite Ville de Demain_  
   \demitab $9$ massifs, $8817$ communes (10 millions d'habitants, 15% de la population)  
   \demitab dans $32$ structures appliquant ce programme, dont 16 ont un GAL.

\bigbreak

Il faut diversifier les activités économiques, qui sont très centrées sur le tourisme et un peu sur l'agriculture.

Il faut un tourisme plus respectueux de l'environnement.  
Un tourisme durable, qui préserve la biodiversité.  
En montagne, parce que le changement climatique $\implies$ moins de neige $\implies$ station de ski

   - comment rénover les hébergements touristiques ?
   - comment mettre à disposition des moyens de locomotion moins polluants ?

# IV) Les aides accessibles

La page _Aides Territoires_ du site de l'[Agence Nationale de la Cohésion des Territoires](https://agence-cohesion-territoires.gouv.fr) présente pour chaque territoire les aides dont il peut profiter :

   \demitab celles du programme PVD ont un macaron bleu.  

\bigbreak

[Formulaire de recherche des aides PVD](https://aides-territoires.beta.gouv.fr/portails/petitesvillesdedemain/)  
À l'échelle nationale, les aides accessibles sont rassemblées sur [Aides Territoires](https://aides-territoires.beta.gouv.fr/)

[Aides Territoires par l'open data](https://www.data.gouv.fr/fr/datasets/aides-financieres-et-en-ingenierie-disponibles-pour-les-collectivites-territoriales-et-leurs-partenaires-locaux/)

\bigbreak

Les Chefs de Projet, les partenaires, ont une plateforme Internet de suivi :  
[OSMOSE](https://www.numerique.gouv.fr/outils-agents/osmose/)

\bigbreak

Il y a également un lien sur les aides LEADER.

