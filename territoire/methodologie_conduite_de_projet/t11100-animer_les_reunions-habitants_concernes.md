---
title: "Et les habitants ?"
subtitle: "Comment faire pour qu'ils se sentent concernés ? Comment animer les réunions ?"
author: Marc Le Bihan
---

Comment faire __pour__ et __avec__ les habitants ?  
Comment faire pour qu'ils se sentent concernés ?  
Comment animer les réunions ?  
Et faire que les gens reviennent ?

Comment atteindre ceux les plus éloignés de la chose publique ?

   - Ceux qui ne viennent pas quand on les appelle ?
   - Ceux qui sont là, mais qu'on ne voit pas ?

## 1) Les obstacles à surmonter

### a) Représentation / délégation du pouvoir

- députés : $75\%$ issus de la Fonction Publique Territoriale, et la FPT c'est 5 millions de personnes sur 40 millions pouvant députables.  
  CSP+, et pas assez de femmes.

- On laisse faire qui veut faire, on ne se sent pas concerné.

### b) Modèle du sachant et du non sachant. Reconnaissance par les diplômes

\bleu{Les citoyens doivent dire ce qu'ils savent, leur vision, même si elle imparfaite.}

### c) Défiance à l'égard des institutions

- $50\%$ ne vont pas voter?
- pensent que les fonctionnaires sont des nantis

### d) Société de consommation exacerbée

- Repli sur soi, individualisme
- Les gens veulent que ça aille vite
- zapping

Or, il y a le temps du projet.  
Les politiques publiques peuvent prendre du temps.

## 2) Faire ensemble pour vivre mieux

"_Le but de la société, c'est le bonheur en commun_"

Agir ensemble sur les décisions publiques :

  - est plus long, mais plus efficace.
  - crée des solidarités
  - aide à respecter les gens
  - donne le goût de débattre
  - $22\%$ des gens se méfient des autres, sont isolés.

### a) La démocratie participative

- Prendre part $\rightarrow$ être présent
- Donner sa part $\rightarrow$ contribuer
- Recevoir sa part $\rightarrow$ __1 + 1 = 3__
- Faire sa part $\rightarrow$ être acteur

### b) Participation institutionnelle

Politique de la ville, environnement, urbanisme...  
L'avis par un \definition{conseil citoyen} est demandé (il est consultatif) $\leftarrow$ roposition de loi de Développement et de la Promotion de la Démocratie Participative.

## 3) Les acteurs incontournables de la démocratie locale

![Acteurs de la démocratie participative](../images/acteurs_democratie_participative_habitants.png)

\begingroup \footnotesize
_Habitants, associations, professionnels et techniciens, élus de toutes collectivités locales, contrat politique, de service, de gestion_
\endgroup

## 4) Les quatre démons de la participation

1. La peur

2. Le sentiment d'inutilité

   - Vais-je être écouté ?
   - Est-ce que cela va être utile ?

3. Le sentiment d'impuissance

  - "_Tout a été décidé..._"

4. L'isolement

## 5) Animation

\underline{L'on doit être :} 

- Animateur de forme

- Animateur de fond

   - Prise de notes
   - L'on vérifie auprès de la personne qu'on a bien noté

\bigbreak

\underline{Autres pratiques :}

- Tables à thèmes $\rightarrow$ changement de tables
- Écrire des idées sur des post-its anonymes

\bigbreak

\underline{Trois temps :}

- Quelles sont vos colères ? Qu'est-ce qui ne va pas ?
- Quels sont nos rêves ? Nos souhaits ?

## 6) Échelle de la participation citoyenne

![Acteurs de la démocratie participative](../images/echelle_participation_citoyenne.png)

\begingroup \footnotesize
_co-élaboration, coopération (idéal), concertation, consultation individuelle (chacun donne son avis), information, échelle de démocratie participative,
diagnostic partagé : observer, écouter et analyser, étatpes : moments manquants, échéances : gestion du temps, objectifs : réalisations, orientation, visée_
\endgroup

## Références et bibliographie

@cnam01