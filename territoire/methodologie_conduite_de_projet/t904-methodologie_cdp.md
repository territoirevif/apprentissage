---
title: "t904. Méthodologie de conduite de projet public"
subtitle: ""
author: Marc Le Bihan
keywords:
---

Mobiliser les acteurs $\rightarrow$ bâtir son projet $\rightarrow$ renforcer ses équipes $\rightarrow$ passer à l'opérationnel

## 1) Définition du projet

### a) Plan d'action

   - Lire un cahier des charges

   - définir plan d'action

     - axes stratégiques 
     - fiches actions

   - un secteur d'intervention prioritaire

### b) Critères et indicateurs

   - construire les critères
   - construire les indicateurs

### c) Etudes et diagnostics

Financement d'études et de diagnostics (AMO)

   - sur le plan stratégique
   - sur le plan opérationnel

## 2) Faire se rencontrer les acteurs

Il est intéressant de se faire rencontrer des logiques différentes

   - les \definition{porteurs de projets} ont une \definition{logique ascendante} (comme les acteurs de terrains, en général)  
   - les \definition{pouvoirs publics}, eux, veulent promouvoir une politique particulière, dans une logique \definition{descendante} (comme les financeurs).

\bigbreak

La rencontre des acteurs, l'étude de leurs projets, éviter les redondances

L'animation de réunions publiques est aussi utile

## 3) Suivi du projet

   - comité technique
   - comité de projet : avec le préfet de département et ses principaux partenaires

## 4) Financement, recrutement, prestations

   - Renforcer des équipes des collectivités
   - Faire du Marchés publics
   - et des dossiers de subvention

