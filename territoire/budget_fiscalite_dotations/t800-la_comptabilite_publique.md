---
title: "t800. La comptabilité publique"
subtitle: ""
category: territoire
author: Marc Le Bihan
---

## 1) La comptabilité publique

Les plans comptables généraux et publics sont équivalents.

Il y a séparation entre \definition{ordonnateur} et \definition{comptable}.  
et les agents comptables ont une \danger{responsabilité personnelle}.

Ils travaillent avec le \definition{Trésor Public} en \definition{unités de caisse}.

Le délai de paiement est de 30 jours.

Il s'agit d'une \definition{comptabilité d'engagement} :

   - Bon de commande
   - Contrat préalable
   - délibération

sont préalables à tout ordre.

Un agent peut devenir \definition{régisseur} et recevoir des paiements : \definition{régie de recette}.

## 2) Section de fonctionnement 

### Recettes

- Fiscalité
- Dotation, dégrèvements
- Participation des usagers

### Dépenses

- Masse salariale  
   - qui augmente par le __Glissement Vieillesse Technicité__
   - la Fonction Publique Territoriale tend à préférer les contractels, à présent  
- Subventions aux associations
- Intérêts de la dette (frais financiers)

## 3) Section d'Investissement

### Recettes

- Subventions reçues
- Emprunt de la dette
- Autonfinancement

### Dépenses

- Dépenses d'équipement
- Subventions d'équipement (à destination de...)
- Remboursement de la dette (en capital)

