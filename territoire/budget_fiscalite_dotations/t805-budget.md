---
title: "t805. Budget"
subtitle: ""
category: territoire
author: Marc Le Bihan
---

Les collectivités locales ont un budget de \mauve{230 milliards d'euros}.

Une collectivité locale a un compte courant (de gestion) chez un comptable public, agent de l'état.  
Elle peut emprunter d'une la banque ou d'un organisme de crédit.

Si le budget d'une commune se dévoie, le préfet peut intervenir via la \avantage{Chambre Régionale des Comptes} qui le corrigera, souvent en augmentant la fiscalité.

Ce budget dépend de valeurs issues de la _Loi de Finances_ et n'est souvent établi qu'au 31 Mars.

## 1) Principes budgétaires

- Annualité budgétaire : la commune l'établit pour un an

   - Si une dépense est plus durable, elle va dans une \definition{autorisation de programme} : un __crédit de paiement__ qui est une provision.

- Unicité budgétaire : un seul document pour tous les budgets

   - Quelques budgets annexes sont autonomes et équilibrés.  
     Financés par leurs propres recettes.

- Principe d'universalisté : on affecte pas une dépense à une recette.

- Principe de l'équilibre budgétaire : les recettes et dépenses sont votées à l'équilibre.

   - Le remboursement des dettes doivent se faire par des __recettes propres__ : autofinancement, subventions, mais pas par une autre dette.


