---
title: "t830. Fiscalité locale"
subtitle: "Les sources de la fiscalité locale"
category: territoire
author: Marc Le Bihan
---

## 1) Les collectivités locales peuvent prélever l'impôt

Les collectivités locales peuvent prélever l'impôt, __mais pas en créer de nouveau__.  
Ce sont des impôts dont on peut établir la __territorialité__.

\bigbreak

La fiscalité (des contribuables)  
   \demitab s'ajoute aux dotations/subventions (Etat, Union Européenne, autres CL)  
   \demitab et aux recettes tarifaires (participations usagers).

Les impôts locaux dépendent du nombre de personnes présentes au $1^{\text{er}}$ Janvier de l'année.

## 2) Les sources de la fiscalité locale

### a) Les impôts ménage et professionnel

- Taxe d'Habitation : impôt ménage
- Taxe Foncière Bâti : impôt ménage et professionnel
- Taxe Foncière Non Bâtie : impôt ménage et professionnel

### b) Le calcul de l'impôt et le problème de la base fiscale

  \begingroup \large $\text{impôt} = \text{base fiscale} \times \text{taux imposition}$ \endgroup

  \demitab où la base fiscale est la __valeur locative__.  

Comme la base fiscale se fonde sur un recencement fait en 1970, et qui est déclaratif,  
il existe une \definition{Commission Communale des Impôts Directs (CCID)}

### c) Limites du taux d'imposition

Le taux d'imposition ne peut dépasser  
$2.5 \times \text{le taux national}$ ou départemental (le plus élevé),  
et tous les impôts doivent être prélevés dans les mêmes proportions.

### d) Exonérations et dégrèvements

- \avantage{exonérations}

\bigbreak

- \avantage{dégrèvements} : l'Etat se substitue  
  \rouge{mais alors les collectivités locales perdent leur autonomie},  
  et ne peuvent plus prélever régler les taux à leur guise.

### e) Les impôts indirects

Il y en a une liste finie

  - versement transport
  - taxe de séjour

## 3) Les ratios


## 4) Fiscalité locale des entreprises

### a) Autrefois : la Taxe Professionnelle

La Taxe Professionnelle, créée dans les années 70,  
\demitab s'est basée sur la masse salariale et les investissements des entreprises.

C'était un \definition{impôt de stock}  
\demitab qu'elles continuaient à payer même quand leurs recettes baissaient.

\bigbreak

Les collectivités locales voyaient cela comme une rente,  
\demitab mais c'était surtout celles ayant beaucoup d'entreprises qui en bénéficiaient...  
\demitab celles à vocation sociale, non.

### b) La Contribution Economique Territoriale (CET)

Contribution Economique Territoriale (CET) =  
\tab Contribution Foncière des Entreprises (CFE) + Cotisation Valeur Ajoutée Entreprises (CVAE)

La CFE est répartie sur les communes d'après la masse salariale qu'elles représentent localement.

### c) Autres

Imposition Forfaitaire Entreprises Réseau (IFER)


