---
title: "t810. Dotations, subventions, compensations"
subtitle: ""
category: territoire
author: Marc Le Bihan
---

## 1) Les types de dotations

Les dotations peuvent être :

### a) Budgédaires

Les dotations \definition{budgétaires} servent à :

- soutenir les collectivités

- au transfert de compétences :

   - mais souvent, l'évolution de leur montant n'est pas connu...

   \begingroup \cadreAvantage

   > Les départements (RSA, etc.) ont des dépenses qui augmentent,  
   > mais leur compensation, elles, non.

   \endgroup

\bigbreak

- fonctionnement courant, car les C.L. exercent des fonctions au nom de l'état 

   - Etat-civil
   - Police
   - Vote 

\bigbreak

- Aide à l'investissement : soutien à l'économie via des travaux publics  
  suscités par le \definition{Fond de Compensation de la TVA (FCTVA)}  
  qui rembourse la TVA dont se sont acquittés les C.L., qui n'en paient donc pas. 

\bigbreak

- $50\%$ du produit des amendes de police. (Paris, STIF : $25\%$)

### b) Fiscales

Les dotations \definition{fiscales} ($42\%$ des dotations) compensent des allègements de fiscalité.

## 2) Montants et parts dans le budget de l'Etat

\donnee{40\%} des budgets (soit \donnee{100} milliards euros) des collectivités locales viennent des dotations  
(Le budget de l'état est de \donnee{380} milliards d'euros)

\bigbreak

Sur ces 100 M³ euros de dotations :

   - \donnee{56 M³ €} sont des concours financiers

      - \donnee{37 M³ €} La __Dotation Globale de Fonctionnement (DGF)__  
        qui ne porte que sur les dépenses de fonctionnement des communes,  
        fait 20% des recettes des C.L.

      - le reste, des dégrèvements fiscaux.

\bigbreak

   - \donnee{21 M³ €} vont au bloc communal (communes et intercommunalités) :  
     \demitab \donnee{11 M³ €} vont aux communes,  
     \demitab et \donnee{10 M³ €} aux intercommunalités

      - Les intercommunalités n'en conservent que \donnee{6.5 M³ €} pour elles
      - \donnee{3.6 M³ €} sont en __péréquation__ pour les communes

\bigbreak

   - \definition{Dotation de Solidarité Urbaine (DSU)}

