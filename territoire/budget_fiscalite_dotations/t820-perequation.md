---
title: "t820. La péréquation"
subtitle: ""
category: territoire
author: Marc Le Bihan
---

## 1) La péréquation verticale

Les dotations de l'état visent à favoriser les collectivités locales qui ont peu de ressources fiscales.

## 2) La péréquation horizontale

On prélève aux collectivités locales aux ressources élevées pour redistribuer.

\bigbreak

Les critères de redistribution sont :

  - Le \definition{Potentiel Fiscal} : ce que l'impôt pourrait lever  
    $\text{base fiscalité} \times \text{taux moyens nationaux}$

\bigbreak

  - L' \definition{Effort Fiscal} : ce que l'impôt prélève réellement :  
   une commune pourrait avoir des taux volontairement bas.

