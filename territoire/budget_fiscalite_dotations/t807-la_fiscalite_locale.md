---
title: "t807. La fiscalité locale"
subtitle: ""
category: territoire
author: Marc Le Bihan
---

Les collectivités locales peuvent prélever l'impôt, mais pas en créer de nouveau.  
Des impôts dont on peut établir la __territorialité__.

La fiscalité (des contribuables) s'ajoute aux dotations/subventions (Etat, Union Européenne, autres CL), et recettes tarifaires (participations usagers).

Les impôts locaux dépendent du nombre de personnes présentes au 1er Janvier de l'année.

## 1) Ses sources

- Taxe d'Habitation : impôt ménage
- Taxe Foncière Bâti : impôt ménage et professionnel
- Taxe Foncière Non Bâtie : impôt ménage et professionnel

$impôt = base\ fiscale * taux\ imposition$

où la base fiscale est la __valeur locative__. Comme elle se fonde sur un recencement fait en 1970, et est déclaratif, il existe une \definition{Commission Communale des Impôts Directs (CCID)}

Le taux d'imposition ne peut dépasser $2.5 \times \text{le taux national}$ ou départemental (le plus élevé), et tous les impôts doivent être prélevés dans les mêmes proportions.

- \avantage{exonérations}

- \avantage{dégrèvements} : l'Etat se substitue
mais alors les collectivités locales perdent leur autonomie, et ne peuvent plus prélever régler les taux à leur guise.

impôts indirects : il y en a une liste finie

- versement transport
- taxe de séjour

## 2) Les ratios

## 3) La fiscalité locale des entreprises

La Taxe Professionnelle, créée dans les années 70, s'est basée sur la masse salariale et les investissements des entreprises.  
C'était un \definition{impôt de stock} qu'elles continuaient à payer même quand leurs recettes baissaient.

Les collectivités locales voyaient cela comme une rente, mais c'était surtout celles ayant beaucoup d'entreprises qui en bénéficiaient. Celles à vocation sociale, non.

Contribution Economique Territoriale (CET) = Contribution Foncière des Entreprises (CFE) + Cotisation Valeur Ajoutée Entreprises (CVAE)

La CFE est répartie sur les communes d'après la masse salariale qu'elles représentent localement.

Imposition Forfaitaire Entreprises Réseau (IFER)

