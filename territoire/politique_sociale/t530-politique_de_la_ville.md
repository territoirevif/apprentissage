---
title: "La Politique de la Ville"
subtitle: "et le Nouveau Programme de Rénovation Urbaine (NPRU)"
author: Marc Le Bihan
---

[Cartographie des contrats de ville](https://sig.ville.gouv.fr/)  
[https://sig.ville.gouv.fr/](https://sig.ville.gouv.fr/)

---

# I) Pour qui et comment ?

## 1) Les objectifs de la Politique de la Ville

\definition{Loi Politique de la Ville et Cohésion du Territoire (2014)}

La Politique de la ville a été lancée il y a 40 ans :

   - en réaction à la dégradation des quartiers  
    (rapidement construits en 1950-60, près d'anciennes industries aujourd'hui disparues, au moment du rapatriement des pieds-noirs, de l'arrivée de la main d'oeuvre étrangère, et de la construction de grandes tours pour résorber les bidons-villes)
   
   - pour lutter contre l'exclusion
   - éviter la ségrégation

\bigbreak

Elle couvre : 

   - l'urbanisme 
   - le social 
   - l'économie 
   - l'enseignement 
   - la sécurité...

\bigbreak

Territoire / Acteur / Développement : dynamique locale, vivre ensemble $\rightarrow$ économie, social et culturel

C'est un ensemble d'actions de l'Etat, en partenariat avec les collectivités locales (tous les niveaux de collectivités locales y sont engagées), ainsi que les associations (sur le terrain), mais pas toujours l'État,
sur une base contractuelle avec des \bleu{mesures législatives et règlementaires} : elle adapte des prinicpes de droit commun, puis met en oeuvre ses instruments.

Pendant \donnee{six ans}, Le temps d'un mandat de maire,  
   \demitab une politique lie ces collectivités locales par un __Contrat de Ville__, ainsi que des bailleurs.  

La politique de la ville est une politique de cohésion urbaine et de solidarité envers les plus défavorisés : elle vise à réduire les inégalités entre territoires.  
Il s'agit de mettre en oeuvre des Politiques Sociales et a des répercussions dans l'urbanisme.

## 2) Les critères d'entrée d'une commune en Politique de la Ville

Le critère d'entrée, aujourd'hui, est le \donnee{revenu des habitants}.

   \demitab Il doit être inférieur à 60\% du revenu médian par famille,  
   \demitab soit $\approx$ 600 euros / personne  
   \demitab et être dans une zone urbaine

Vers 2020, il y a 1514 quartiers, 5.5 millions de personnes, 702 communes.

\bigbreak

Aujourd'hui, la \definition{Loi Lamy (2014)} définit \donnee{1 514 quartiers pour 5.5 millions d'habitants},  
   \demitab avec pour critère unique de sélection __le revenu par habitant__.

Elle redéfinit la géographie prioritaire, nouveau programme de renouvellement urbain, encourage la participation des habitants. 

## 3) La participation des habitants aux projets qui les concernent

Les habitants sont associés en amont des projets qui les concernent,  
   \demitab par des \definition{conseils citoyens},  
   \demitab qui incluent un tirage au sort de ses membres.

   > \underline{$1^{er}$ droit : } être __informé__ de tous les sujets qui concernent leur quartier.
   >
   > \underline{$2^{ème}$ droit : } droit à __proposer__ et à __intervenir__ sur ces questions
   >
   > \underline{$3^{ème}$ droit : } __participer__ aux instrances et comités qui décident du pilotage
   >
   > \underline{$4^{ème}$ droit : } si un des droits n'est pas respecté, ils peuvent interpeller le préfet

\bigbreak

En effet, il y a l'inquiétude des habitants :

   - Veut-on les remplacer (si les loyers augmentent) ?

      - On crée un profil de locataire
      - Pas d'augmentations pour les anciens (personnes âgées)

\bigbreak

Des ateliers où l'on demande : "_\bleu{ce qui ne va pas}_", "_\bleu{ce dont vous rêvez}_", etc

   \demitab naît un __mémoire__ que les urbanistes, architectes  
   \demitab (qui sont invités à dialoguer avec la population)  
   \demitab vont étudier de près, pour s'en inspirer.

## 4) Les programmes, contrats et financements

Les \definition{Contrats de Développement Social des Quartiers (DSQ, 1989)} et \definition{Contrats de Ville (1994)} répartissent les rôles et les financements.

   \demitab Des sous-préfets sont chargés de mission  
   \demitab pour coordonner ces acteurs en suivant les dispositifs contractuels (1991).

\bigbreak

Le \definition{Nouveau Programme de Rénovation Urbaine (NPRU)} est associé au contrat de ville.

La \definition{Dotation de Solidarité Urbaine (DSU)},  
   \demitab qui fait partie de la Dotation Globale de Financement,  
   \demitab prélève aux villes riches pour reverser à celles en politique de la ville.

\bigbreak

La \definition{Loi Borloo de rénovation urbaine (2003)}  

   \demitab aidée de l'\definition{Agence Nationale de Rénobvation Urbaine (ANRU)}  
   \demitab qui concentre les financements de l'état,  
   \demitab rénove les logements et démolit des tours.

\bigbreak

\avantage{l'Agence Nationale pour la Rénovation Urbaine (2003)} perçoit :

   - pour 80\% une cotisation des entrerises de \donnee{0.45\%}
   - et de l'Etat, pour 20\%, dans \avantage{Action Logement}.
   - le __Livret A__ y aide aussi.

\bigbreak

Il faut programmer, au fil de l'eau, des __opérations FEDER__ (investissement)  
   \demitab pour l'équipement public ou FSE (humain).  
   \demitab $\rightarrow$ il faut les créer l'année d'avant pour qu'elles soient actives l'année d'après.

## 5) Les acteurs

### a) Étatiques

- Direction Départementale de la Cohésion Sociale (DDCS)
- Unité Territoriale de Direction Générale des Entreprises
- Direction Générale de la Concurrence, de la Consommation, du Travail de l'Emploi (DIRECCTE) 

### b) Collectivités

- Communes
- Départements
- Intercommunalités
- Régions
- ...

### c) Équipes projet

- Chefs de projets dédiés
- Délégués de préfet
- Professionnels
- Préfet
- Sous-préfecture
- ...

# II) Les actions entreprises en Politique de la Ville

Ses objectifs sont peu ou prou ceux décrits dans les objectifs thématiques. En particulier :

   1. Lutte contre les inégalités, fractures sociales, numériques et territoriales
   2. Droits, éducation, culture, services
   3. Développement économique, création entreprises, accès emploi
   4. Habitat
   5. Santé et accès aux soins
   10. Egalité Homme-Femme, non-discrimination (dont celle du lieu de résidence).s  

Partager diagnostics, financements, qui est MOA ? quels seront les acteurs sur le terrain ?

## 1) Habitat, routes, équipements publics

### a) Refaire le dur, les équipements et espaces publics

On refait des bâtiments, on refait des routes, on en détruit : c'est le dur.

Equipements publics : écoles, gymnases, commerces...

Espaces publics : espaces verts, voirie

  - Quartiers propres : ce qui est cassé est réparé ou jeté.  
    On désigne qui nettoie quoi, dans "_les zones qui n'appartiennent à personne_"

  - Problème de stationnement illicite : retrait des épaves

  - \avantage{Régie de quartier} : elle embauche des intervenants pour du 
     
     - sur-entretien
     - re-végétalisation

### b) Le logement des résidents

Renouvellement urbain, réhabilitation, rénovation, pour \donnee{450 QPV}  
   \demitab \donnee{8.5 millions de personnes mal logées}  
   \demitab dont \donnee{2 millions en habitat dégradé}

On peut y faire de la __résidentialisation__ qui clôture et fait un espace privé.  

Pour apporter de la mixité, l'on détruit des logements sociaux pour construire de petites maisons.

\attention{Mais gare à construire avant de détruire !}
\attention{Sinon, l'on sème la panique !}

Convention avec les bailleurs sociaux : leur taxe foncière est réduite de 30\%.

### c) Organisation

La maquette, les plans, le calendrier sont dans la \avantage{Maison du Projet}, obligatoire.

\definition{Convention de Gestion Urbaine de Proximité (GUSP)}

  - Relation de proximité avec les habitants
  - Charte de bon voisinage

## 2) Transports

Les voies fluidifient et créent du trafic.  
Et le trafic apporte la sécurité

## 3) Santé

Des problèmes spécifiques sont présents, par exemple : obésité

\avantage{Ateliers Santé Ville (ASV)}

   - Faire venir des médecins
   - Création de Maisons de la Santé
   - Travail sur les taxicomanies, 
   - sur les problèmes d'alimentation

   > $\rightarrow$ \definition{Contrat Local de Santé}

## 4) Education

Zone d'Education Prioritaire (ZEP)

Programme de Réussite Educative (PRE)

enseignement : soutien aux établissements scolaires  
   \demitab avec davantage de professeurs

Mais les enseignants manquent.

## 5) Emploi

### a) La clause d'insertion

Les entreprises ont une \definition{clause d'insertion}

   - de gens du quartier (10\% des heures, ou plus : 15\%... doivent être réalisées par eux)

      - des anciens professionnels, sans emploi
      - du personnel peu qualifié (manoeuvres)    

   - ou issus d'autres quartiers politique de la ville.

\bigbreak

On peut passer par une association qui fait de l'accompagnement.

\bigbreak

Mais il est bon de passer par un \definition{clauseur},  
   \demitab une personne extérireure (de la commune, ou de l'intercommunalité), qui va :

   - chercher les embauchables,
   - vérifier le travail qu'ils font

### b) Les Zones Franches Urbaines (ZFU) disparaissent

\definition{Zones Franches Urbaines (ZFU)} : s'éteignent progressivement

Leur but : faire venir des entreprises.  
Elles ne paieront pas d'impôts ou de contributions sociales, mais devront embaucher localement.

Pacte de Dijon / Territoire entreprendre

### c) Le Plan Local d'Insertion et d'Emploi (PLIE)

\definition{Plan Local d'Insertion et d'Emploi (PLIE)}

On se fixe un objectif sur 3 ans. Exemple : "_\bleu{Accompagner 250 personnes}_"

L'on crée une association

   - On cherche des personnes loin de l'emploi
   - Un conseiller d'insertion va suivre ces personnes

\bigbreak

Il y a des financements européens.

## 6) Prévention, sécurité
 
Prévention, sécurité, citoyenneté :  
   \demitab accès au droit et à la justice de proximité (maison de la Justice)  

\bigbreak

\definition{Contrat Local de Sécurité et de Prévention de la Délinquance (CLSPD)}

   - Sensibilisation aux règles et lois
   - Rencontre avec les policiers
   - Maison du droit et de la Justice : les magistrats font de la prévention
   - Travaux d'intérêt généraux, s'il y a des sanctions

## 7) Services publics de proximité

Services publics de proximité : culturel, sportif, médical, associations  

