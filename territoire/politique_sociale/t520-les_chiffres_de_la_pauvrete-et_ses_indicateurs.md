---
title: "t520. Les chiffres de la pauvreté"
subtitle: "et leurs indicateurs"
author: Marc Le Bihan
---

## 1) Les chiffres de la pauvreté et ses indicateurs

### a) Les revenus

En 2017, le revenu moyen est de \mauve{20 800 euros} par an  
   \demitab soit \mauve{1735 euros} par mois   
   \demitab mais les \mauve{10\%} les plus modestes  
   \demitab sont en dessous de \mauve{11190 euros par an}.

\bigbreak

Les chômeurs gagnent en moyenne \mauve{1170 euros}  
   \demitab mais \mauve{42\%} recoivent moins de \mauve{500 euros}.

\bigbreak

\mauve{SMIC : 1539 euros / mois}, soit \mauve{18468 euros par an}.

### a) Le seuil de pauvreté

\mauve{5 millions de pauvres}  
   \demitab perçoivent moins de \mauve{855 euros} par mois.

\bigbreak

\mauve{60\% du niveau de vie médian}  
\demitab soit \mauve{1041 euros pour une personne seule}.  

\bigbreak

\mauve{9 millions de personnes} sont sous ce seuil.  

   - \mauve{31.3\%} sont des inactifs (chômeurs, étudiants...)
   - \mauve{7.6\%} sont des retraités
   - \mauve{8.2\%} des actifs occupés (travailleurs pauvres)

### b) l'Intensité de la pauvreté

"_C'est un indicateur qui permet d'apprécier  
à quel point le niveau de vie de la population pauvre  
est éloignée du seuil de pauvreté_"

\bleu{niveau de vie de la population pauvre} :  
   \demitab \mauve{837 euros par mois}  
   $$ \rouge{\text{intensité de la pauvreté}} = \frac{\text{seuil de pauvreté} - \text{niveau de vie médian pop. pauvre}}{\text{seuil de pauvreté}} = \frac{204}{1041} = \mauve{19.6\%} $$

## 2) La pauvreté non monétaire

### a) La pauvreté en condition de vie

Il faut avoir 8 items sur 27 regroupés en 4 domaines :

   - consomation
   - insuffisance de ressources
   - retards de paiement
   - difficulté de paiements

### b) Le niveau de vie

   > \bleu{UC} : Unité de consommation
   >
   > - __1__ pour le premier adulte du ménage
   > - __0.5__ pour une autre personne de plus de 14 ans
   > - __0.3__ pour un enfant de moins de 14 ans

   $$ \rouge{\text{niveau de vie}} = \frac{\text{revenu disponible du ménage}}{\text{nombre d'unités de consommation}} $$

### c) Le degré d'inégalité de répartition (Gini)

C'est le degré d'inégalité de la population en niveaux de vie :

   > __0__ : égalité parfaite  
   > __1__ : une a tout, les autres : rien.

\bigbreak

En France, le Gini est à \mauve{0.3} : 

   \demitab Nous avons des amortisseurs sociaux :

   \tab quand l'économie se casse la figure  
   \tab le nombre de personnes pauvres n'explose pas.

