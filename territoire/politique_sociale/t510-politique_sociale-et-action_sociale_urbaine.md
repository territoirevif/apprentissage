---
title: "t510. Politiques sociales et action sociale urbaine"
subtitle: ""
author: Marc Le Bihan
---

## 1) Une action sociale pilotée par l'état et surtout le département

### a) Le modèle de l'état providence

Il assure la couverture plus par l'impôt que par les cotisations.

   - sécurité sociale
   - protection sociale complémentaire : mutuelles, caisses de retraite
   - \textbf{\mauve{aide} sociale} : c'est un droit \definition{subjectif} : 

      - droit pour l'individu 
      - obligation pour la collectivité

     Il est défini par la loi ou le règlement.  

   - \textbf{\mauve{action} sociale} : action complémentaire des collectivités, définies par leur promoteur.

\bigbreak

Les \definition{Politiques Sociales} relèvent de l'Etat,  
   \demitab mais sont souvent déconcentrées dans les collectivités locales. 

Elles agissent comme un __guichet__, accordant des droits à des personnes selon des critères.

### b) Le pilotage par le département

Les politiques sociales sont surtout l'apanage du département  
   \demitab qui en est chef de file depuis \definition{MAPTAM (2014)} :

   - il __coordonne__ l'application des politiques sociales  
       \demitab via des \avantage{Centre Locaux d'Information et de Coordination (CLIC)}  
   - mais un département peut aussi proposer ses propres politiques d'accompagnement.

\bigbreak

Il les pilote en définissant préalablement un :

   \demitab \definition{schéma départemental (social, médical...)}  
   \demitab comme la répartition des EHPAD, par exemple.

   - aide légale,
   - planification
   - tarification

\bigbreak

Et est chef de file dans :

   1. l'action sociale, le développement social, la résorption de la précarité énergétique
   2. l'autonomie des personnes
   3. les solidarités, la cohésion des territoires 

   \begingroup \cadreGris

   > \underline{Dépenses d'aides sociales obligatoires par les départements, en \mauve{2016 : 36.8 milliards d'euros}}
   >
   >    - insertion : 10 milliards d'euros.
   >    - personnes âgées : 6.9
   >    - handicap : 7
   >    - enfance : 6

   \endgroup \bigbreak

\underline{il agit selon trois axes :}

   1. Attributions de prestations en espèce ou en nature
   2. Avantages tarifaires pour l'accès aux Services Publics
   3. Structures collectives d'accueil pour certains publics

## 2) Les objectifs des politiques sociales

Facultatives ou obligatoires, elles ont deux buts "antagonistes" :

   - atténuer les inégalités,
   - favoriser la réussite individuelle

\bigbreak

Elles luttent contre les effets de/du :

### a) La pauvreté, du chômage : l'action sociale contre la pauvreté

   - accès au logement
   - soins
   - aides alimentaires, de transport, énergétiques. Prêt remboursable   
   - avantages tarifaires, basés sur le \definition{Quotient Familial} ou plus avancés encore  
       - cantines, 
       - centres de loisirs, 
       - culture, 
       - sport

\bigbreak

Il faut savoir que les prestations sociales font :

   - \mauve{40\%} des revenus des personnes du premier décile,  
   - quand celles du dernier décile ont surtout ceux de leur patrimoine.

### b) Vieillissement, personnes âgées (compétence département)

   - Versement de l'\definition{Allocation Personnalisée d'Autonomie (APA)}  
     \demitab de plus en plus lourde pour le département  
   - Maintien à domicile
   - Hébergement
   - Versement de l'\definition{Aide Personnalisée d'Autonomie (APA)}
   - Animation et accompagnement

### c) Handicap (compétence département)

   - Politiques d'hébergement et d'insertion sociale
   - Versement de la prestation de compensation  
   - Orientation, suivi [...]  
     \demitab dans la \avantage{Maison Départementale des Personnes Handicapées (MDPH)}  
   - Service de transport collectif  
     de personnes handicapées ou dépendantes  
     \demitab \avantage{Pour Aider à la Mobilité (PAM)}  
   - Versement de l'\definition{Allocation aux Adultes Handicapé (AAH)}
   - Tarifs Handicap.

\bigbreak

   > \underline{Remarque} : vieillesse ou handicap, il y a une recherche 
   >
   >    - d'autonomie des personnes,
   >    - de prévention/prise en charge des situations de fragilité
 
### d) Revenu minimum et Aide à l'Insertion (compétence département)

   - Verse le \definition{Revenu de Solidarité Active (RSA)}
   - tarifs RSA
   - prime d'activité

   \begingroup \cadreGris

   > \textbf{Le RSA vaut (aux alentours de 2019) :}\newline
   > 
   > \ \ \ \ \mauve{564€} pour une personne seule,  
   > \ \ \ \ \mauve{966€} pour un parent avec 1 enfant,  
   > \ \ \ \ \mauve{1184€} pour un couple avec 2 enfants

   \endgroup

   > C'est une dépense de \mauve{9.8 milliards d'euros} pour le département  
   > auquel l'état transfère \mauve{5.6 $M^3$ euros}.  
Cette dotation est étrangement indexéee sur une taxe des carburants...

\bigbreak

\danger{Effet ciseaux :}  

   > Les départements pauvres ont :  
   >
   >    - Beaucoup de personnes au RSA
   >    - Pas de fonds, puisqu'ils sont pauvres
   > 
   > \   
   > De sorte que l'argent est pris au dépend d'autres actions :  
   >
   > \ \ \ \ En 2009, les fonds réservés à l'insertion représentaient \mauve{14.5\%}   
   > \ \ \ \ en 2015, \mauve{7.7\%}

### e) Protection de l'Enfance (compétence département)

   - Enfance, adoption (\definition{Aide Sociale à l'Enfance (ASE)})
   - Assure la \definition{Protection Maternelle et Infantile (PMI)}
   - Enfance, général : animations

## 3) L'Action Sociale Communale

Une commune dispose obligatoirement d'un 
    \demitab \definition{Centre Communal [ou Intercommunal] d'Action Sociale (CCAS ou CIAS)}  
    \demitab dès qu'elle a plus de \mauve{1500 habitants}. Il est facultatif sinon.

C'est un _Etablissement Public Administratif (EPA)_, donc :  

   - personnalité juritique propre
   - budget propre 
   - conseil d'administration

Mais :

   - Son président est le maire  
     ou le président du conseil communautaire de l'EPCI.

et il a :

   - 8 membres du conseil municipal [communataire]
   - 8 représentants nommés par le maire [président du conseil communautaire]

### a) Les domaines d'action du CCAS

   - l'insertion locale contre l'exclusion
   - famille
   - retraités, personnes âgées
   - personnes handicapées

### b) Ses compétences

   - Les mêmes que celles du département, confiées à la commune/intercommunalité
   - Instruction des demandes d'aide sociale
   - Créer des services sociaux et médicaux-sociaux
   - Réaliser des enquêtes sociales
   - Tenir un fichier des bénéficiaires d'aides sociales

### c) Ses missions

\underline{Rôle d'ensemblier}, tout d'abord : il est le premier échelon pour coordonner les acteurs suivants.

Il a une grande latitude d'interventions :

  - prestations en espèces (bons de secours)
  - prestations en nature (colis alimentaires)
  - vêtements...

\bigbreak

C'est un guichet unique pour les aides sociales : RSA...

Ses autres actions sociales sont :

   - l'aide à domicile : 

      - aide ménagère
      - auxilliaire de vie
      - garde de jour

Et :

   - le portage des repas
   - EHPAD, accompagnement séniors
   - Factures énergie, 
   - soins, 
   - loyers, 
   - cantine, 
   - prériscolaire,
   - micro-crédit

### d) Les autres actions communales

   - Plan Canicule
   - Hébergement d'urgence
   - Domiciliation des SDF
   - Réalisation d'une enquête socialee

## Annexe : la société civile, au travers des associations

   - Handicap, avec : 

      - APF France handicap (APF), 
      - Union nationale des associations de parents, de personnes handicapées mentales et de leurs amis (UNAPEI)

\bigbreak

   - Pauvreté, avec :

      - ATD Quart-Monde
      - Secours Populaire

