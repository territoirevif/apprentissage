---
title: "t040. Les régions"
subtitle: ""
category: territoire
author: Marc Le Bihan
keywords:
- Schéma Régional de Développement Economique d'Innovation et d'Internationalisation (SRDEII)
- Programme Opérationnel (PO)
- FEDER
- FSE
- transport
- formation professionnelle
- lycée
- attractivité du territoire
- gestion des déchets
- emploi
- insertion professionnelle
- gestion de l'eau
- attractivité économique des entreprises
---

Elles apparaissent en 1982-1983 pour concurrencer les Länders.

   - Budget : \mauve{13\% du budget} des collectivités locales $\rightarrow$ 30 à 35 milliards d'euros (2021)
   - Investissment : les régions font \mauve{16\% des investissements} des collectiviités locales

\bigbreak

La région définit la vision stratégique du territoire. 

   - \definition{Schéma Régional de Développement Économique d’Innovation et d'Internationalisation (SRDEII)}.  
   - Prépare le \definition{Programme Opérationnel (PO) FEDER ou FSE} européen.
   - Seuls les projets qui y figurent sont subventionnables par elle.  

# I) Les compétences régionales

## 1) Le Développemenent économique du territoire (compétence)

## 1) Cas de l'Ile-de-France

L'Etat fait des interventions au travers
   
   - d'un \definition{Etablissement Public d'Aménagement (EPA)}.
   - de la \avantage{Société du Grand Paris (SGP)}.

## 2) Transports

[Chef de file des transports et Autorité Organisatrice de la Mobilité (AOM), cf. transports](t450-transport.pdf)

## 3) Formation professionnelle, en général. Lycées, en particulier (compétence)

## 4) Emploi et insertion professionnelle (compétence)

## 5) Action environnementale 

### a) Gestion des déchets

[cf. environnement, gestion des déchets](t700-environnement.pdf#dechets)

### b) Feuille de Route sur l'Economie Circulaire (FREC)

[cf. environnement, feuille de route sur l'économie circulaire](t700-environnement.pdf#frec)

### c) Gestion de l'eau

[cf. environnement, gestion de l'eau](t700-environnement.pdf#eau)

## 6) Action culturelle (compétence)

## 7) Attractivité du territoire (compétence)

### a) Attractivité Eonomique des entreprises

### b) Logement

## 8) Planification dans le domaine du Tourisme (compétence)

## 9) Traitement des ordures ménagères, pour les investissements dans les lourds équipements (compétence)

