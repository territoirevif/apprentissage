---
title: "t003. La fondation d'une commune nouvelle"
subtitle: ""
author: Marc Le Bihan
keywords:
- commune
- maire
- conseiller minicipal
- fondation d'une commune nouvelle
- commune déléguée
- maire adjoint
- état-civil
---

Il y a plusieurs motivations à ce que des communes fusionnent :

   - "_On pèse peu dans l'intercommunalité_"  
   - \underline{Protection :}
      - maintien de services publics, d'écoles.
      - un seul cadastre, un seul service postal receveur municipal...  
   - Projets de développement
   - Mutualisation financière
   - Incitation financière

## 1) Initiation et décision

Il y a plusieurs origines possibles à l'initiation d'une fusion de communes

   1. Les conseils municipaux en décident.
   2. L'intercommunalité le propose, et les communes sont d'accord.
   3. Le préfet le décide.

Il faudra que $\frac{2}{3}$ des communes faisant $\frac{2}{3}$ de la population acceptent (un référendum peut avoir lieu).  
Mais ce sont presque toujours des processus consensuels.

## 2) Conséquences

Lors d'une fusion, les anciennes communes disparaissent, même si elles peuvent devenir un temps des \definition{communes déléguées}, conservant quelques fonctions : 

   - état-civil, 
   - inscription dans les écoles...

\bigbreak

Avant le renouvellement municipal suivant :

   - Le conseil municipal se compose de toues les conseillers de toutes les communes,
   - Les anciens maires deviennent des \definition{maires adjoints},
   - Un nouveau maire est désigné.

Lorsque des communes issues d'intercommunalités différentes fusionnent, la commune nouvelle créée \avantage{choisit son intercommunalité}.

\bigbreak
 
Lorsque des communes issues de départements différents fusionnent, les limites départementales changent. Les habitants d'une ancienne commune peuvent même en changer de région...

\bigbreak

\underline{Conséquences financières :}

   - Il n'y a plus qu'un seul budget.  
   - On calcule un taux pondéré d'après la fiscalité (TH, TFB, TFNB) des communes qui existaient, et l'on a douze ans pour l'atteindre.  
   - Incitation Dotation Globale de Financement (DGF) :
      - Pas de baisse de dotation pendant 3 ans.
      - et \donnee{+ 5\% de DGF} la première année.

## 3) Conditions

- Les communes doivent être contigües.
- Cela ne peut se faire dans les \donnee{douze mois} qui précèdent une élection municipale.

