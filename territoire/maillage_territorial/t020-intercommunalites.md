---
title: "t020. Les intercommunalités"
subtitle: ""
category: territoire
author: Marc Le Bihan
keywords:
- intercommunalité
- EPCI
- communauté de commune
- communauté d'agglomération
- communauté urbaine
- métropole
- Loi Administration Territoriale de la République (ATR) (1992)
- Loi Chevènement (1999)
- Loi MAPTAM (2013)
- fiscalité propre
- fiscalité additionnelle
- fiscalité superposée
- Contribution Economique Territoriale (CET)
- Dotation Globale de Fonctionnement (DGF)
- potentiel fiscal
- coefficient d'intégration fiscal
- reversement intercommunalité aux communes
- Plan Local d'Urbanisme (PLUi)
- Métropole du Grand Paris (MGP)
---

   - communauté de communes
   - communauté d'agglomération
   - communauté urbaine
   - métropole.   

\bigbreak

leur territoire est d'un seul tenant, sans enclave, et elles ont pour but de :

   - Regrouper les communes rurales émiétées
   - Mieux répartir les entreprises sur le sol
   - et les équipements structurants
   - Mettre en commun des moyens (assainissement eaux usées, déchets, voirie, transports...)
   - Solidarité financière (charges, péréquation...)

#### 1992 : \definition{Loi Administration Territoriale de la République (ATR)}

### 1999 : \definition{Loi Chevènement}

#### 2013 : \definition{2013 Loi MAPTAM}

   - Organise les métropoles  
     Trois statuts particuliers pour Paris (qu'il rejette), Lyon et Marseille.

# I) Fonctionnement

   - Une commune y a au moins une voix
   - Aucune commune ne peut y avoir la majorité à elle toute seule.

# II) Fiscalité

Il existe encore des intercommunalités __sans fiscalité__, où les participations sont versées par les communes, mais elles se font plus rares.  

Elles sont maintenant à \definition{fiscalité propre} : 

## 1) Fiscalité propre, additionnelle ou superposée

   - le particulier paie une partie de sa TH, FB ; 

   - les entreprises

      - une partie de leur CET à la commune 
      - et une autre à l'intercommunalité.

## 2) Fiscalité propre, spécialisée 

   - la Contribution Economique Territoriale (CET) va à l'intercommunalité
   - TH, FB, FNB, à la commune

## 3) La Dotation Globale de Financement (DGF)

Elle est répartie d'après :

   1. La population
   
   2. Le \definition{Potentiel Fiscal} : les ressources mobilisables sur un territoire.
   
   3. Le \definition{Coefficient d'Intégration Fiscal (CIF)}
   $$ \rouge{\text{CIF}} = \frac{\text{Produit fiscal IC} - \text{Reversement IC aux communes}}{\text{Produit fiscal IC} + \sum \text{Produits fiscaux communes}}
   $$ 

# III) Compétences

## Plan Local d'Urbanisme (PLUi)

## Environnement (Compétence)

   - [Gestion des Milieux Aquatiques et Prévention des Inondations (GEMAPI)](territoire-13-environnement.pdf), qu'elle partage avec la commune.

## Transport

Elle peut décider d'être [Autorité Organisatrice de la Mobilité (AOM)](territoire-t22-transport.pdf)

# IV) Focus : Métropole du Grand Paris (MGP)

Issue de la Loi MAPTAM (2013), mais surtout NOTRe (2017), elle avise surtout la première couronne (75 + 92, 93, 94)

