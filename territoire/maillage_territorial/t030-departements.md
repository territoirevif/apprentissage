---
title: "t030. Les départements"
subtitle: ""
category: territoire
author: Marc Le Bihan
keywords:
- déconcentration
- enseignement (collège)
- aménagement rural
- action sociale
- handicap
- personnes âgées
- Etablissement Public Territorial (EPT)
---

La France s'est déconcentrée en départements lors de la Révolution.

\bigbreak

Ils sont un échelon particulièrement utile en milieu rural.

   - Budget : \mauve{33\% du budget des collectivités locales} (année 2017 ??)
   - Investissment : \mauve{24\% des investissements des collectiviités locales}.

# I) Les compétences des départements

   - Enseignement : Collège (compétence)
   - Aménagement : Rural (compétence)
   - Transport : Urbain, routes départementales (compétence), [transport des élèves en handicap](t450-transport.pdf)
   - Culture : Bibilothèque, Archives départementales (compétence)

   - Action sociale (voir chapitre dédié)
   - Personnes Handicapées (voir chapitre dédié)
   - Personnes Âgées (voir chapitre dédié)

# II) Focus : Ile-de-France

Les départements 92, 93 et 94 d'île-de-France sont divisés en \definition{Etablissement Public Territoriaux (EPT)}, financés par :

   - la CVAE issue de la CET, depuis la MGP
   - la CFE allant aux territoires
