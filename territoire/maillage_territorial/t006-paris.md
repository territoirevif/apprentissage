---
title: "t006. Paris"
subtitle: ""
author: Marc Le Bihan
keywords:
- commune
- Paris
- Etat
- ministères
---

Paris est une ville :

- de grand poids : population, économique, historique.

- Vitrine de la France, avec une tendance à éjecter tout ce qui ne lui plaît pas en périphérie.

- Présence très forte de l'Etat (beaucoup de bâtiments lui appartiennent), avec des ministères qui longtemps (et encore ?) se mêlent de sa politique.


