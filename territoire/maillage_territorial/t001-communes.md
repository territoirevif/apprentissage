---
title: "t001. Les compétences d'une commune"
subtitle: ""
author: Marc Le Bihan
keywords:
- compétence
- commune
- maire
- conseiller minicipal
- centre communal d'action social
- politique du logement
- ordures ménagères
- distribution d'eau
- gestion des milieux aquatiques et prévention des inondations
- GEMAPI
- petite enfance
- crêche
- halte garderie
- maison de famille
- école primaire
- école maternelle
- handicap accessibilité
- établissements pour personnes âgées
- culture
- sport
- transport
- autorité organisatrice de la mobilité
- AOM
---

Il y a \donnee{500 000 élus, maires et conseillers municipaux}.
 
Une commune est une _circonscription administrative de l'état_ : le maire devient agent de l'état dans des fonctions d'état-civil, sécurité, mariages...

\bigbreak

\underline{Remarque :} La commune met en oeuvre des __Politiques Sociales__ au travers du \avantage{Centre Communal d'Action Sociale (CCAS)} au budget autonome. [cf. chapitre départements](./t020-departements.pdf)

## 1) Clause de compétence générale

## 2) Politique du logement (compétence)

## 3) Environnement (compétence)

   - Ordures ménagères
   - Distribution d'eau  
   - [Gestion des Milieux Aquatiques et Prévention des Inondations (GEMAPI)](territoire-13-environnement.pdf), qu'elle partage avec l'intercommunalité.

## 4) Petite enfance et enfance (compétence)

   - Politiques Sociales : crêches, haltes garderies, relais d'assistance. Maison de la Famille.
   - Ecoles primaies, et maternelles

## 5) Handicap

Poltiques Sociales : obligation des communes :

   - Accessibilité de l'Hôtel de Ville, des écoles, théâtres, trottoirs.

## 6) Personnes âgées

Politiques Sociales : 

   - établissements  
   - animation et accompagnement (les communes chercheront surtout à faire de \definition{l'aide au maintien à domicile} via des __auxilliaires de Vie__.

## 7) Culture (compétence)

## 8) Sport (compétence)

## 9) Développement économique (compétence ?)

## 10) Transport

Elle peut décider d'être [Autorité Organisatrice de la Mobilité (AOM)](territoire-t22-transport.pdf)

