#!/bin/bash
export source=$1

# Le paramètre source doit être alimenté
if [ -z "$source" ]; then
   echo "Le nom du fichier markdown à convertir en pdf est attendu en paramètre." >&2
   exit 1
fi

# Il ne doit pas contenir d'extension
if [[ "$source" == *"."* ]]; then
   echo "Le nom du fichier markdown à convertir en pdf ne doit pas avoir d'extension (.md ou autre)." >&2
   exit 1
fi

conversion=$(pandoc -f markdown-implicit_figures --syntax-definition=$APPRENTISSAGE_HOME/java.xml --syntax-definition=$APPRENTISSAGE_HOME/java.xml --metadata-file=$APPRENTISSAGE_HOME/metadata.json --include-in-header=$APPRENTISSAGE_HOME/apprentissage-header-include.tex --include-in-header=$APPRENTISSAGE_HOME/apprentissage-include.tex --bibliography=$APPRENTISSAGE_HOME/bibliographie.bib --citeproc "${source}".md -o "${source}".pdf)

if $conversion; then
   echo "Markdown ${source}.md converti en ${source}.pdf"
fi;
