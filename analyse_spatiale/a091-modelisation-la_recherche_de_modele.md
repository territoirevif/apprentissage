---
title: "a091. La recherche d'un modèle"
subtitle: ""
author: Marc Le Bihan
keywords:
- modèle
- modèle descriptif
- modèle prédictif
- boite noire
- base physique
- sochastique
- explicatif
- processus
- résidu
- autocorrélation spatiale
- domaine
- processus d'émergence
- processus de diffusion
- vecteurs
- individus
- distribution spatiale d'un phénomène
- modèle linéaire généralisé
- modèle logistique
- évènement
---

## 1) Les différents types de modèles

Modèle descriptif vs. prédictif.

De manière générale, l'analyse explicative est liée à de la statistique inférentielle.

   - __boite noire__ : ses entrées et sorties suffisent à le décrire (ex : pluies et bassin versant)
   - __base physique__ : toutes les variables ont une dimension (une unité de mesure)
   - __stochastique__ : certains éléments sont aléatoires.  
   - __explicatif__ : impossible à simuler/appréhender entièrement parce que trop complexe, mais certaines variables contribuent à l'expliquer.

## 2) Un modèle prédictif 

### a) Expliquer une variable $y$ en fonction de variables $x_{n}$

Le modèle dépend de :

  - facteurs
  - du temps
  - de l'individu

On cherche idéalement un modèle ou \definition{processus} __M__ qui à partir de

   - $\textcolor{blue}{x_{1}, x_{2}, x_{3},...,x_{n}}$ \demitab les variables prédictives,
   - $\textcolor{blue}{t}$ \demitab \tab \tab le temps
   - __\avantage{P}__ \demitab \tab \tab l'individu

soit capable d'expliquer:

   > $\textcolor{red}{\varepsilon}$ \demitab le \definition{résidu}, qu'on espère finir par être aléatoire (un bruit spatial)
   \begingroup \large $$ \textcolor{OliveGreen}{y} = M(x_{1}, x_{2}, x_{3},...,x_{n}, P, t, \varepsilon) $$ \endgroup

par exemple, expliquer $\bleu{y}$ en fonction $\bleu{x_{1}, x_{2}, x_{3},...,x_{n}}$.

### b) auto-corrélation spatiale

Il reste une auto-correlation spatiale du résidu  

certains modèles spatiaux, auto-regressifs; l'appréhendent avec une matrice de poids spatiaux définie d'après des distances :

   > $\textcolor{blue}{Y}$ \demitab valeurs observées aux $N$ objets  
   > \   
   > $\textcolor{blue}{W}$ \demitab matrice $N * N$ des poids spatiaux :  
     \tab $\frac{d0 - d}{d0}$ ou $(\frac{d0 - d}{d0})^2$, avec minimisation de la variance
   \begingroup \large $$ \textcolor{OliveGreen}{y} = M(WY, X, P, t, \varepsilon, W\varepsilon) $$ \endgroup

Ce modèle __M__ est supposé être le même sur tout le domaine __D__.

## 3) Diviser le modèle en plusieurs parties en séparant les processus

Le modèle est très complexe à déterminer : mieux vaut le voir comme \avantage{une somme de processus indépendants} $M_{1}, M_{2}, M_{3},...,M_{n}$ :  

   - d'émergence
   - de diffusion
   - de ceux contenant des vecteurs
   - ou des individus

\bigbreak

L'analyse spatiale cherche à caractériser __F__  
\demitab la distribution spatiale d'un phénomène d'un de ces processus $M_{i}$.  

Ces modèles $\mathbf{M_{i}}$ vont varier dans l'espace (par leurs paramètres ou plus encore)  
\demitab en passant d'un domaine à l'autre.

\attention{On peut avoir à décrire le modèle avec des paramètres indépendants des valeurs observées}.

Les __modèles linéaires généralisés__ comme le __modèle logistique__ sont appréciés en santé, car il suffit d'en trouver les coefficients.

