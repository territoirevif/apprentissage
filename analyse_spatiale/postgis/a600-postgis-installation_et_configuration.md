---
title: "a600. Installation et configuration de PostGIS" 
subtitle: ""
category: "analyse spatiale"
author: Marc Le Bihan

abstract: |
   Packages Linux, installation de postgresql et postgis  
   PgAdmin 4 et DBeaver  
   Créer et se connecter à une base par psql.

keywords:
- linux
- package
- postgresql
- su -u postgres
- postgis
- pgadmin
- dbeaver
- sudo postgres
- database
- extension
- psql
- schema
- grant usage
- alter user
- shp2pgsql
---

---

`PostGIS` n'est pas la seule base de données géographique : il existe `MariaDB` ou `Spatialite`.

### 1) Installer les packages qui seront utilisés par les différents outils

   \code

   > ```bash
   > # optionnel : choisir la langue française pour le clavier
   > sudo dpkg-reconfigure keyboard-configuration   
   > sudo service keyboard-setup restart
   >
   > # upgrader les packages et rebooter
   > sudo apt-get update
   > sudo apt-get upgrade
   > sudo reboot
   >
   > # mettre à jour les certificats
   > sudo apt install gnupg2 wget ca-certificates
   > sudo apt-get install curl
   > ```

   \endgroup 

### 2) Installer Postgresql et Postgis

Sur l'exemple de Postgres 15 et PostGIS 3.x.

   \code

   > ```bash
   > sudo apt -y install postgresql-14 postgresql-client-15
   > sudo apt -y install postgis postgresql-15-postgis-3 postgresql-15-postgis-3-scripts
   > ```

   \endgroup

\bigbreak

\underline{Remarque :} c'est dans `postgresql-xx-postgis-3-scripts` (où `xx` est sa version) que se trouve la commande `shp2pgsql` de conversion de Shapefile en table.

### 3) Installation de PgAdmin4 ou de DBeaver

\underline{PgAdmin 4 :}

   \code

   > ```bash
   > sudo curl https://www.pgadmin.org/static/packages_pgadmin_org.pub | sudo apt-key add
   > sudo sh -c 'echo "deb https://ftp.postgresql.org/pub/pgadmin/pgadmin4/apt/$(lsb_release -cs) pgadmin4 main" > /etc/apt/sources.list.d/pgadmin4.list && apt update'
   > sudo apt-get upgrade
   > sudo apt install pgadmin4
  > ```

   \endgroup

\bigbreak

\underline{DBeaver :}

   \code

   > ```bash
   > echo "deb https://dbeaver.io/debs/dbeaver-ce /" | sudo tee /etc/apt/sources.list.d/dbeaver.list
   > curl -fsSL https://dbeaver.io/debs/dbeaver.gpg.key | sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/dbeaver.gpg
   > sudo apt-get update
   > sudo apt install dbeaver-ce
   > ```

   \endgroup

### 4) Placer l'utilisateur `postgres` dans le groupe des sudoers

   \code

   > ```bash
   > # un des moyens possibles
   > sudo vi /etc/sudoers
   > ```

   \endgroup

### 5) Se connecter en sudo en tant que `postgres` sous `bash`, puis se connecter à `psql` sans préciser la base de données.

   \code

   > ```bash
   > sudo su - postgres
   >
   > # si la commande précédente ne fonctionne pas bien, passer en deux temps :
   > # sudo - d'abord, pour passer root, puis sudo - postgres
   >
   > sudo -u postgres psql
   > ```

   \endgroup

### 6) Créeer la database

   \code

   > ```sql
   > CREATE DATABASE ma_database; -- Ne pas oublier le point-virgule sous psql !
   > 
   > # en profiter pour attribuer à postgres le mot de passe postgres (tant qu'on est en dev)
   > # sinon, c'est casse-pieds pour les authentification, sans mot de passe.
   > ALTER USER postgres WITH PASSWORD 'postgres'
   > \q  -- pour quitter psql
   > ```

   \endgroup

### 7) Se connecter à la base

   \code

   > ```bash
   > sudo -u postgres psql postgis_in_action
   > ```

   \endgroup

\bigbreak

   et exécuter ces commandes :

   \code

   > ```sql
   > # Création du schéma postgis
   > CREATE SCHEMA postgis;
   > GRANT USAGE ON SCHEMA postgis to public;
   > CREATE EXTENSION postgis SCHEMA postgis;
   > ALTER DATABASE postgis_in_action SET search_path=public,postgis,contrib; 
   > CREATE EXTENSION postgis_raster SCHEMA postgis -- En Postgis 3 et supérieurs
   >
   > CREATE EXTENSION postgis_topology
   > CREATE EXTENSION pgRouting
   > ```

   \endgroup

