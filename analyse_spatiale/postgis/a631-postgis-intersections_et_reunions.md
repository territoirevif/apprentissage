---
title: "a631. Intersections et réunions avec PostGIS"
subtitle: ""

keywords:
- intersection
- union
- réunion
- postgis
- st_within
- st_union
- st_buffer
- distance
- déduplication

author: Marc Le Bihan
---

Intersections et aires autour d'un point par `ST_DWithin` et `ST_Buffer`.

## 1) `ST_DWithin` et une déduplication à ne pas oublier !

\methode{ST\_DWithin}{(a.geom, b.geom, distance\_en\_mètres)} répond _true_ lorsque deux géométries sont à une distance inférieure à celle en argument.

\tab Dans notre exemple, des restaurants sur deux routes s'intersectant pourraient apparaître en double. Il faut les dédupliquer par leurs identifiants. 

   \bigbreak \code

   > \underline{Eléments s'intersectant, avec déduplication par COUNT(DISTINCT) :}
   > 
   > ```sql
   > SELECT f.franchise, COUNT(DISTINCT r.id) AS total -- dédupliquer les restaurants
   >    FROM ch01.restaurants As r 
   >       INNER JOIN ch01.lu_franchises As f ON r.franchise = f.id
   >       INNER JOIN ch01.highways As h 
   >    ON ST_DWithin(r.geom, h.geom, 1609) -- 1.609 mètres = 1 mile
   > GROUP BY f.franchise
   > ORDER BY total DESC; -- les franchises aux plus grand nombre de restaurants d'abord
   > ```

   \endgroup

   \begingroup \footnotesize

   |franchise|total|
   |---------|-----|
   |McDonald|5343|
   |Burger King|3049|
   |Pizza Hut|2920|
   |Wendys|2446|
   |Taco Bell|2428|
   |Kentucky Fried Chicken|2371|
   |Hardee|1077|
   |Jack in the Box|509|
   |Carl's Jr|224|
   |In-N-Out|44|

   \endgroup

\bigbreak

\underline{Remarque :}

Le nombre de restaurants est important, mais l'on demande 1 mile autour de __TOUTES__ les autoroutes de __tous les États-Unis__ !

   \bigbreak \code

   > \underline{ajout de restrictions supplémentaires à la requête précédente :}
   >
   > ```sql
   > -- Si l'on reprend la requête précédente en passant 1609 * 20 en arguments, c'est 20 miles qu'on avisera.
   > -- On restreint les résultats, pour l'exercice, aux restaurants "Hardee's" de la route "_US Route 1_" du Maryland.
   > SELECT COUNT(DISTINCT r.id) As total
   >    FROM ch01.restaurants As r 
   >      INNER JOIN ch01.highways As h 
   >      ON ST_DWithin(r.geom, h.geom, 1609*20)
   > WHERE r.franchise = 'HDE' 
   >    AND h.name  = 'US Route 1' AND h.state = 'MD';
   >
   > -- Les routes sont des MUTLILINESTRING
   > SELECT gid, name, geom 
   >    FROM ch01.highways
   > WHERE name = 'US Route 1' AND state = 'MD';
   > ```

   |gid|name |geom                            |
   |---|-----|--------------------------------|
   |16000|US Route 1|MULTILINESTRING ((2012782.8315602066 -295583.9743548482, [...]))|
   |16057|US Route 1|MULTILINESTRING ((2012139.7842248029 -296719.72822618077, 2012782.8315602066 -295583.9743548482))|
   |16173|US Route 1|MULTILINESTRING ((2007252.1405982107 -299651.62856319884, [...]))|

   \endgroup

## 2) `ST_Buffer` et `ST_Union`

D'un côté,  
avec les fonctions \fonction{ST\_Buffer} et \fonction{ST\_Union} ensemble, on va rassembler en un unique `POLYGON` l'aire qui est 20 miles autour des routes considérées :

   \bigbreak \code

   > \underline{rassemblement des polygones qui sont dans un certain rayon en un seul :}
   >
   > ```sql
   > SELECT ST_Union(ST_Buffer(geom, 1609*20))
   > FROM ch01.highways
   > WHERE name = 'US Route 1' AND state = 'MD';
   > ```

   |st\_union|
   |---------|
   |POLYGON ((2026155.226698877 -328548.4392001351, [...], 2026153.4979527208))|
 
   \endgroup

De l'autre, la localisation (en `POINT` géométriques) des restaurants _Hardee's_, 20 miles autour de la route _US Route 1_ dans le Maryland (qui parcoure 130 km/80 miles dans cet état).

   \bigbreak \code

   > ```sql
   > SELECT r.geom
   > FROM ch01.restaurants r
   > WHERE EXISTS 
   >  (SELECT gid FROM ch01.highways 
   >   WHERE ST_DWithin(r.geom, geom, 1609*20) AND 
   >   name = 'US Route 1' 
   >    AND state = 'MD' AND r.franchise = 'HDE');
   > ```

   \endgroup

\bigbreak

   Ils sont seulement trois...

   \begingroup \footnotesize

   |geom|
   |----|
   |POINT (1984951.4140166119 -368305.3645910054)|
   |POINT (1980307.7428061718 -360035.22259236954)|
   |POINT (1994954.3683215368 -367611.5673761975)|

   \endgroup

