---
title: "a621. Le chargement de données dans PostGIS"
subtitle: "depuis un fichier CSV ou Shapefile"

keywords:
- projection
- reprojection
- postgis
- géométrie
- index géométrique
- geometry
- cascade
- gist
- spgist
- brin
- on update cascade
- on delete restrict
- csv
- \copy
- psql
- st_point
- st_setsrid
- shapefile
- chargement
- shp2pgsql

author: Marc Le Bihan
---

# I) Champs et index géométriques

L'on peut préciser le type et la projection d'un champ géographique :

   \bigbreak \code

   > \underline{une géométrie de type point en EPSG:2163 :}
   >
   > ```sql
   > CREATE TABLE ch01.restaurants(
   >   id serial primary key,
   >   franchise char(3) NOT NULL,
   >   geom geometry(point,2163) 
   >   -- EPSG:2163 : US National Atlas Equal Area (déprécié)
   > );
   > ```

   \endgroup

## 1) Les types d'index géométriques

### a) `gist` : le plus souvent utilisé

   \code

   > ```sql
   > CREATE INDEX ix_code_restaurants_geom 
   >  ON ch01.restaurants USING gist(geom);
   > ```

   \endgroup

### b) `spgist`

### c) `brin`

# II) les cascades

Exemple d'emploi des cascades \fonction{ON UPDATE CASCADE} et \fonction{ON DELETE RESTRICT}

   \bigbreak \code

   > ```sql
   > ALTER TABLE ch01.restaurants 
   >   ADD CONSTRAINT fk_restaurants_lu_franchises
   >   FOREIGN KEY(franchise) 
   >   REFERENCES ch01.lu_franchises (id)
   >   ON UPDATE CASCADE ON DELETE RESTRICT;
   > ```

   \endgroup \bigbreak

\fonction{ON UPDATE CASCADE} : un identifiant de franchise changé, il sera mis à jour dans les restaurants.

\fonction{ON DELETE RESTRICT} : supprime l'identifiant de la table restaurants si la franchise disparaît, mais évite que les franchises disparaissent avec la suppression d'un restaurant.

# III) Chargement de CSV dans une table

## 1) Création d'une table adaptée au CSV

Ses types de données sont \fonction{text}, \fonction{double}...

   \bigbreak \code

   > ```sql
   > CREATE TABLE ch01.restaurants_staging(franchise text, lat double precision, lon double precision);
   > ```

   \endgroup

## 2) \\COPY (par psql) des données du CSV

\tab La commande de copie de CSV utilisée \attention{est de psql}, difficle d'accès sur _DBeaver_ ou _PgAdmin_.

Il faut se mettre sur un terminal, entrer dans une session `psql`, et provoquer la copie par un `\COPY`.

   \bigbreak \code

   > \underline{copie d'un fichier csv dans une table par /COPY :}
   >
   > ```bash
   > sudo su - postgres
   > sudo -u postgres psql postgis_in_action
   > \copy ch01.restaurants_staging FROM '/home/vagrant/ch01/data/restaurants.csv' DELIMITER as ','
   > ```

   \endgroup

\bigbreak
La table est alimentée avec des données :

   \begingroup \footnotesize

   |franchise|lat|lon|
   |---------|---|---|
   |BKG|25.8092|-80.24|
   |BKG|25.8587|-80.2094|
   |BKG|25.8471|-80.2415|
   |BKG|25.8306|-80.2661|

   \endgroup

## 3) Reprojection

L'on reprojette :

   1. un point que l'on crée à partir de \methode{ST\_Point}{(lon, lat)} 
   2. suivi de \methode{ST\_SetSRID}{(cePoint, 4326)} qui dit : "_je suis actuellement en WGS84_"
   3. en projection `EPSG:2163` qui convient pour les États-unis

   \bigbreak \code

   > \underline{Une rétroprojection de CRS A en B :}
   >
   > ```sql  
   > INSERT INTO ch01.restaurants(franchise, geom)
   > SELECT franchise
   >  , ST_Transform(
   >    ST_SetSRID(ST_Point(lon , lat), 4326)
   >    , 2163) As geom
   > FROM ch01.restaurants_staging;
   > ```

   \endgroup

# IV) Chargement d'un Shapefile avec shp2pgsql

## 1) Importation avec `shp2pgsql`

Après s'être loggué comme utilisateur `postgres` par un `sudo su -u postgres`  
taper le \fonction{shp2pgsql} qui va :

   1. déclarer que la projection du shapefile `roadtrl020.shp` est `EPSG:4269` : _Geodetic coordinate system for North America_  
   2. générer les instructions SQL pour alimenter une table `highways.stagging`.

   \bigbreak \code

   > ```bash
   > shp2pgsql -D -s 4269 -g geom -I '/home/vagrant/ch01/data/roadtrl020.shp' ch01.highways_staging | psql -h localhost -U postgres -p 5432 -d postgis_in_action
   > ```

   \endgroup

## 2) Reprojection

\tab Ce contenu est reprojeté en `EPSG:2163` (remarque : un \avantage{-s 4269:2163 provoquerait cette conversion par shp2pgsql depuis la version 3.0}),  
Nous filtrons les routes sur le critère d'être de type `Principal Highway%`,  
et transférerons le contenu de cette table `stagging` dans une table définitive, que l'on va analyser ensuite.

   \bigbreak \code

   > ```sql  
   > INSERT INTO ch01.highways (gid, feature, name, state, geom)
   >    SELECT gid, feature, name, state, ST_Transform(geom, 2163)
   >       FROM ch01.highways_staging
   >    WHERE feature LIKE 'Principal Highway%';
   > 
   > VACUUM ANALYZE ch01.highways;
   > ```

   \endgroup

