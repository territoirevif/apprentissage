---
title: "a623. Postgis: ses types de données"
subtitle: ""
author: Marc Le Bihan
keywords:
- geometry
- geography
- latitude
- longitude
- Set_SRID
---

\underline{Curiosité :} les types géographiques de _PostgreSQL_ ne sont pas pleinement compatibles avec ceux de _PostGIS_... On est parfois appelés à convertir des formes géométriques (`box`, `circle`...) de PostgreSQL en polygones, avant de pouvoir les transformer en géométries _PostGIS_.

---

Aux types des bases de données (`CHARACTER`, `NUMERIC`...), s'ajoute dans _PostGIS_ ceux géométriques.  
Une table "_géométrique_" sous _PostGIS_ se crée ainsi :

   \code
   
   > ```sql
   > CREATE TABLE ch02.my_geometries(id serial PRIMARY KEY,name text, geom geometry);
   > ```
   
   \endgroup

\bigbreak

Ainsi, elle est trop prosaïque. L'on précise, en général, deux choses :

   1. Le sous-type de la géométrie (c'est recommandé) : `POINT`, `POLYGON`, `TIN`...
   2. Le système de référence spatial (qu'on peut toujours fixer ultérieurement par \fonction{Set\_SRID()}.

\bigbreak

\demitab Le champ de type géométrique est apparu dans les SGBD spatiaux, mais le type \fonction{geography} est apprécié : il prend en compte la courbature de la terre, où les degrés de latitute et longitude ne sont pas tout à fait uniformes, en distances/surfaces, à l'échelle du globe.


