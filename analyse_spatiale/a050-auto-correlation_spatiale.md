---
title: "a050. L'auto-corrélation spatiale"
subtitle: ""
author: Marc Le Bihan
keywords:
- auto-corrélation spatiale
- autocorrélation
- interaction spatiale
- relation spatiale
---

## 1) L'auto-corrélation spatiale dit à quel point des valeurs sont influencées par des objets qui leur sont proches

Plus les valeurs des observations sont influencées  
\ \ \ \  par celles qui leur sont géographiquement proches,  
et plus l’\definition{autocorrélation spatiale} est élevée.

Et ce sont des \definition{indices d’autocorrélation spatiale} qui permettent de mesurer la force des interactions spatiales
entre les observations. L'on regardera si ces relations sont significatives.

## 2) la corrélation d’une variable avec elle-même

   - soit parce que les observations ont un décalage dans le temps (autocorrélation temporelle)
   - soit dans l’espace (autocorrélation spatiale). Positive ou négative alors, elle traduit:

       - des processus inobservés ou difficilement quantifiables qui créent:

          - des interactions (entre les décisions des agents par exemple)
          - une diffusion (comme les phénomènes de diffusion technologique) dans l’espace

       - des faiblesses d'un modèle: mauvaise spécification de variables, ou variables omises.

## 3) Calcul d'un indice d'auto-corrélation spatiale

L'on va calculer un \definition{indice d'autocorrélation spatiale} et pour cela, comparer: 

   - un indice observé dans la région d'étude
   - avec celui théorique que l'on trouverait dans une distribution aléatoire, exempte de toute organisation spatiale ou structure.

$\rightarrow$ si notre zone d'étude est décrite par une valeur \mauve{nominale},  
ce sera la \mauve{similitude} ou la \mauve{différence} des objets que l'on observera

si elle est décrite par une valeur \mauve{ordinale},  
ce sera par la différence entre deux \mauve{rangs}

si elle est \mauve{cardinale}, par la différence des \mauve{valeurs} des deux objets contigus.

