---
title: "a095. Evolution dans le temps : le mouvement"
subtitle: ""

keywords:
- temps
- mouvement
- spatio-temporel
- processus
- processus de diffusion
- processus de déplacement
- processus de propagation
- variable mono-temporelle
- variable multi-temporelle
- variable intrinsèque
- variable extrinsèque
- distance euclidienne
- espace isotrope
- espace homogène
- coefficient de friction
- diffulté de déplacement
- surface de friction
- distance pondérée

author: Marc Le Bihan
---

Dès qu'il y a \definition{spatio-temporel}, c'est à dire : 

   - localisation
   - et temps

il peut y avoir mouvement.  
Il est alors important de l'étudier.

Comment faire apparaître des processus :

   - de \definition{diffusion}
   - \definition{déplacement} 
   - ou de \definition{propagation} ?

# I) Les variables mono-temporelles, multi-temporelles

Dans les SIG, le temps, c'est une succession de couches.

Les variables sont soit :

   - extrinsèques : elles portent les données spécifiques à un objet

   - intrinsèques, __mono-temporelles__ : on ne prend la donnée/mesure qu'une fois, il n'y a qu'une seule date : routes, MNT, hydrologie...

   - intrinsèques, __multi-temporelles__ : à intervalles réguliers $\to$ plusieurs dates

# II) L'exemple d'un déplacement

Sur un espace plat et non accidenté (__isotrope__ et __homogène__), une distance euclidienne :
$$
d_{A,B} = \sqrt{(x_{A} - x_{B})^2 + (y_{A} - y_{B})^2}
$$

suffirait à mesurer des éloignements.

Mais il y a des obstacles, des reliefs, des forêts...  
Un \definition{coefficient de friction} rend compte de la difficulté de déplacement.  

Sa valeur, son amplitude, c'est celle que choisit l'analyse, mais certains logiciels attribuent :

   - une valeur comprise entre $\mathbf{0}$ et $\mathbf{1}$ pour une surface qui accélère/facilite le mouvement (force).
   - une valeur supérieure à $\mathbf{1}$ pour une surface qui la freine (friction).

Une _surface de friction_ présente leur répartition spatiale.

![Anisotropie et hétérogénéité](images/anisotropie_heterogeneite_espace.jpg)

Plus généralement, aller d'un point __A__ à un point __B__ sur le territoire, ne se fait pas en suivant une ligne droite (un seul segment), mais une en plusieurs segments $\bleu{d_{i}}$ menant à une \avantage{distance pondérée}.

   > $\textcolor{blue}{w_{i}}$ : la difficulté de chaque segment
   \begingroup \large $$ \rouge{d_{A,B}} = \sum{w_{i}d_{i}} $$ \endgroup

\attention{La friction n'est pas nécessairement liée aux obstacles géographiques !}  
Ca peut être des problèmes de sécurité pour traverser un lieu (_vais-je traverser à pied une autoroute ?_)

