---
title: "a154. L'interpolation de Delaunay"
subtitle: "adaptée aux TIN"
author: Marc Le Bihan
keywords:
- interpolation
- delaunay
---

Des triangles contigus sont formés, tels 

   \begingroup \small \demitab qu'un cercle tracé  
   \demitab à travers trois noeuds d'un triangle  
   \tab ne passe par aucun autre noeud. \endgroup

![Interpolation de Delaunay](images/tin_delaunay.jpg)

