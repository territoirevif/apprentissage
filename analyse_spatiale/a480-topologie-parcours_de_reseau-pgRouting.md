---
title: "a480. La topologie en parcours de réseau"
subtitle: "Avec pgRouting de PostGIS"
categorie: analyse spatiale
author: Marc Le Bihan
keywords:
- PostGIS
- topologie
- pgRouting
- parcours de réseau
- recherche d'itinéraire
- optimisation de parcours
- plus court chemin
- Djikstra
---

\fonction{pgRouting} est le module de _PostGIS_ qui observe un réseau pour les sujets de :

   - recherche d'itinéraires
   - optimisation de parcours

\bigbreak
\fonction{Topology} est, elle, plus générale.

## 1) La recherche du plus court chemin{#pcc}

Soit une table, `routes_gers`, qui contient toutes les routes du Gers.

\bigbreak

   0. L'extension \fonction{pgRouting} doit avoir été installée dans PostGIS.

      \code

      > ```bash
      > sudo apt install postgresql-12-pgrouting
      > ```

      > ```sql
      > CREATE EXTENSION pgRouting; -- Cette extension doit être installée
      > ```

      \endgroup

\bigbreak

   1. Pour transformer notre table des routes en graphe, nous lui ajoutons deux champs : \fonction{source} et \fonction{target} pour recevoir les noeuds de départ et d'arrivée.

\bigbreak

   2. Puis, nous créons la topologie __en fusionnant les noeuds distants de moins de 20 mètres__ (par exemple) pour corriger les petits défauts de numérisation  
  \linebreak
   Cette fonction créera une table \fonction{routes\_gers\_vertices\_pgr} qui contient la géométrie des noeuds, si on veut la visualiser.

      \code

      > ```sql
      > ALTER TABLE routes_gers ADD COLUMN source INTEGER;
      > ALTER TABLE routes_gers ADD COLUMN target INTEGER;
      >
      > SELECT pgr_createtopology('routes_gers', 20, 'geom', 'id', 'source', 'target');
      > ```

      \endgroup

\bigbreak

   3. L'on peut alors \avantage{calculer la longueur des tronçons} avec \fonction{ST\_Length} (les géométries de la table \fonction{routes\_gers} sont des lignes).

      \code
      
      > ```sql
      > ALTER TABLE routes_gers ADD COLUMN longueur double precision;
      > UPDATE routes_gers SET longueur = ST_Length(geom);
      > ```

      \endgroup

\bigbreak

   4. Lancement de l'algorithme de Dijkstra entre deux points (ici, sans prendre en compte le sens) :

      \code


      > ```sql
      > SELECT seq, node, edge, cost 
      >   FROM pgr_dijkstra('select id, source, target, longueur AS cost FROM routes_gers', 15, 16, false);
      > ```

      \endgroup

\bigbreak

   > ```txt
   > seq   node    edge     cost
   > 1	   15	   13982    24.429043644656456
   > 2	   16	   -1	    0
   > ```

\bigbreak

   5. Cette requête ajoutera les géométries parcourues, pour visualiser l'itinéraire parcouru :

      \code

      > ```sql
      > WITH dijkstra AS (
      > SELECT seq, node, edge, cost 
      >    FROM pgr_dijkstra('select id, source, target, longueur AS cost FROM routes_gers', 15, 16, false))
      > SELECT r.id, r.fclass, r.geom
      >    FROM routes_gers AS r CROSS JOIN dijkstra
      >    WHERE r.id = dijkstra.edge
      > ```

      \endgroup

\bigbreak

   Mais il y a plein de pièges !  
   Ici, il suffirait de tourner à droite depuis la route noire pour prendre le tronçon 392.

   ...mais hélas, par une erreur de topologie, la route noire et le tronçon 392 n'ont pas de noeud commun, et l'on ne peut tourner à droite...

   ![Route sans noeuds](images/routes_sans_noeuds.jpg)

