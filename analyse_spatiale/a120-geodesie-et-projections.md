---
title: "a120. Le système géodésique (datum)"
subtitle: ""
author: Marc Le Bihan
keywords:
- système géodésique
- géoïde
- représentation de la terre
- repère géographique
- méridien-origine
- Greenwitch
- angle par rapport à l'équateur
- latitude
- ellipsoïde
- datum
- Nouvelle Triangulation de France (NTF)
- Réseau Géodésique de France (RGF 93)
- World Geodetic System (WGS 84)
- type postgis Geography
- autres planètes
- système de coordonnées de référence (SCR)
- déformation d'une image
- rééchantillonage d'image
- méthode des plus proches voisins
- interpolation bilinéaire
- convolution cubique
---

# I) Système géodésique (datum)

## 1) La représentation de la terre par un géoïde

On représente la terre par un \definition{géoïde} ajusté au niveau de la mer. Mais il reste complexe.

   - Les unités sont des unités d'angles (degrés, radians ou grades)  
   
   - \underline{Repère géographique :}  
     \demitab angle du plan par rapport au plan du méridien-origine  
     \tab (arbitrairement choisi internationalement : c'est celui de _Greenwitch_),  
     \   
     \demitab angle par rapport à l'équateur, et altitude $\mathbf{(\lambda, \varphi, h)}$ :  
         \tab $\bleu{\lambda}$ longitude  
         \tab $\bleu{\varphi}$ latitude  
         \tab $\bleu{r}$ distance au centre de la terre

## 2) L'ellipsoïde, simplificateur

On préfère au géoïde un \definition{éllipsoïde} simplificateur. Mais il est imparfait : la mesure de l'altitude (qui se fait théoriquement par un fil à plomb au dessus d'un point) suivant la normale à l'éllipsoïde a une orientation un peu différente.

### a) Les systèmes géodésiques locaux (datum)

L'on peut réaliser des systèmes géodésiques (\definition{datum} en anglais) locaux pour apporter plus de précision. En voici quelques-uns, qui sont ou furent appliqués, basés sur des éllipsoïdes. IIls sont définis par :

   - leur demi-grand axe __a__, 
   - leur demi-petit axe __b__, 
   - leur coefficient d'applatissement __f__ ou le carré de l'exentricité __e²__)

### b) Quelques exemples de datum

   - La __Nouvelle Triangulation de France (NTF, 1880)__  
   - Le __Réseau Géodésique de France (RGF 93, 1980)__ : divisé en neuf zones (CC 42 à 50), il induit une déformation de 7 à 9 cm/km.  
   - ED 50
   - \avantage{World Geodetic System (WGS 84)} : le plus connu, qui résout le problème de l'altitude.

### c) d'autres planètes que la terre

Sous PostGIS, \avantage{le type \definition{Geography} est adapté aux données non projetées} :  
\demitab $\rightarrow$ les lignes sont en fait plutôt des courbes.  

et son _Geodetic Spatial Reference System_ permet de prendre pour référence d'autres planètes que la terre (Mars, par exemple).

# II) Passage d'un système géodésique à l'autre

## 1) Les conséquences d'un changement de système géodésique

Passer d'un système géodésique à l'autre se fait par une combinaison de :

   - translations, 
   - rotations, 
   - et mises à l'échelle. 

L'une pratiquée est la _transformation de Helmert_ [SIGGQ, p.32], par exemple.

\bigbreak

\underline{Remarque :} \avantage{Un changement de système de coordonnées de référence (SCR) ne change pas la topologie} : les vecteurs et lignes qui sont connectées restent connectées.

\danger{Mais pour une image, c'est autre chose : elle subit de lourdes déformations}.

## 2) Quelques méthodes de rééchantillonage d'images

Trois principaux rééchantillonage d'images sont applicables, mais chacun a ses inconvénients :

   - la __méthode des plus proches voisins__ mène à un effet d'escalier.  
     Des pixels peuvent être perdus, d'autres dupliqués, mais elle convient pour les _valeurs qualitatives (catégories)_.
   
   - l'__interpolation bilinéaire__ (un pixel fixé d'après les valeurs des quatre pixels les plus proches)  
    lisse l'image, mais convient aux Modèles Numériques de Terrain.
   
   - la __convolution cubique__ sur seize pixels, est adaptée aux photos satellites ou aériennes.
     lisse l'image et \underline{ne peut pas} être utilisée sur les variables qualitatives (catégories). Elle lisse l'image.

