---
title: "a030. La topologie avec PostGIS"
subtitle: "Passage à la topologie, opérations sur les topologies"
author: Marc Le Bihan
---

\underline{\textbf{Dans ce chapitre :}}

   - calculer l'aire de surfaces
   - obtenir les points les plus proches d'un autre point, du centre ou du côté d'une emprise (opérateurs \fonction{<->}, \fonction{<\#>})
   - ce qu'il y a autour d'un point : \fonction{ST\_Buffer}

# I) Calcul de périmètres, aires, distances avec PostGIS

_PostGIS_ stocke les noeuds et arêtes par des attributs qui contiennent \bleu{right} et \bleu{left} et marquent l'orientation du graphe. Parmi les attributs qui les définissent, les plus notables sont :

   > \fonction{start\_node}, \fonction{end\_node} : un noeud de l'arête, et celui qui le suit.  
   > \fonction{next\_left\_edge}, \fonction{next\_right\_edge} : l'arête suivante,

   > - dans le sens anti-horaire : le polygone situé à sa gauche (left)
   > - dans le sens anti-horaire toujours, le polygone situé à sa droite {right).

## 1) Périmètre, aire

\underline{Attention :} dans ces exemples, les unités de retour sont des mètres, m²... : c'est souvent le cas.
 
Mais cela dépend :

   - de l'unité de mesure inscrite dans le système de référence spatial défini dans la base de données pour des géométries basées sur un type _PostGIS_ \fonction{Geometry}  
   - et en mètres pour une \fonction{Geography}.

\underline{Rappel :} on peut créer une géométrie avec \fonction{ST\_GeomFromText} ou \fonction{ST\_MakePoint}, en précisant le système de coordonnées spatiales cible qu'on vise.

   - longueur d'une polyline (Length) avec `ST_Length`
   
   - Périmètre d'un polygone (Perimeter)
   
   - aire d'un polygone (Area) : \fonction{ST\_Area(geom)}, en $m^2$ :
  
      \begingroup \cadreGris \small 

      > Aire des polygones de forêts :
      >
      > ```sql
      > select id, fclass, ST_Area(geom)
      >    from landuse 
      >    where fclass = 'forest';
      >  ```
     
      \endgroup
   
      Remarque : en divisant le résultat (`ST_Area(geom) / 10000 as surface_Ha`) l'on aura une surface en hectares.
   
      > ```
      > id  fclass    ST_AREA(geom)       surface_Ha
      > 1   "forest"  464993.9356559594   46.49
      > 4   "forest"  473.729419395099    0.047
      > 8   "forest"  9960.415970652159   0.996
      > 10  "forest"  106294.81291852165  10.62
      > ``` 

## 2) Distance

   - Distance entre deux points (Distance) :  
     \methode{ST\_Distance}{(pointA, pointB)} => distance en __mètres\*__ entre __A__ et __B__.

     \begingroup \cadreGris \small

     > __Obtenir la liste des forêts les plus proches d'un point__
     >
     > ```sql
     > SELECT id, ST_Distance(geom, St_GeomFromText('POINT(317000 4813900)', 32631)) as distance 
     >                        -- WGS84 UTM zone 31 N 
     >                        -- 3°17'0" longitude, 48°13'9" latitude 
     >                        -- (un chemin rural à Villeneuve-la-Dondagre)
     >    FROM landuse
     >       WHERE fclass = 'forest' -- Les forêts
     >    ORDER BY distance         
     >       LIMIT 1;                -- Avoir la distance la plus courte
     > ```
  
     \endgroup
 
     > ```txt
     > id      distance (mètres)
     > 122709  1074.47
     > ```

     > \*exprimée selon le CRS/SCR de la couche

\ \ \ \ \ \ \ Pour limiter la durée de la requête \danger{qui porte là sur \underline{toute la France} pour faire sa sélection !}, on peut utiliser l'algorithme des \definition{K plus proches voisins} basé :

   - sur le \definition{centre des emprises} (la clause \fonction{ORDER BY} change), où l'opérateur \fonction{<->} apparaît :
  
      \begingroup \cadreGris \small
 
      > __Les forêts les plus proches d'un point, par rapport au centres des emprises__
      >
      > ```sql
      > SELECT id, ST_Distance(geom, St_GeomFromText('POINT(317000 4813900)', 32631)) as distance -- WGS84 UTM zone 31 N (3°17'0" longitude, 48°13'9" latitude => un chemin rural à Villeneuve-la-Dondagre)
      >    FROM landuse
      >       WHERE fclass = 'forest' -- Les forêts
      >          ORDER BY 
      >             -- par rapport au centre de l'emprise
      >             ST_GeomFromText('POINT(317000 4813900)', 32631) <-> geom  
      >          LIMIT 10;
      > ``` 

      \endgroup

   - ou sur le \definition{côté des emprises}, avec l'opérateur \fonction{<\#>} :
   
     \begingroup \cadreGris

     > __par rapport au côté des emprises__ 
     >
     > ```sql
     > ORDER BY ST_GeomFromText('POINT(317000 4813900)', 32631) <#> geom
     >  ```

     \endgroup

   - Tester si une distance est inférieure à un seuil (PtDistWithin)
   
   - Les objets qui sont à une certaine distance d'un autre : \methode{ST\_Buffer}{(geom, mètres)} $\rightarrow$ objets recensés dans ce __périmètre__.
   
      \begingroup \cadreGris

      > __Ce qu'il y a 500 mètres autour de certains sites__
      >   
      > ```sql
      > select id, fclass, ST_Buffer(geom,500)
      >    from landuse 
      >    where fclass = 'cemetery' or fclass = 'residential';
      > ```
   
      \endgroup

\   
\ \ \ \ \ \ Idéalement, le calcul d'une distance __doit considérer le relief__, en se fondant sur un __Modèle Numérique de Terrain__ le lui donnant.

