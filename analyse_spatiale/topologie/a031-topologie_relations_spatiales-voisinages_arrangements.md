---
title: "a031. Topologie : relations spatiales, voisinages, arrangements"
subtitle: ""

keywords:
- topologie
- relations spatiales
- voisinage
- arrangement
- étude des adjacences
- position
- dispersion
- proximité
- espace plan
- espace homogène
- espace isotrope
- hétérogénéité spatiale
- pondération par la distance
- reconstitution des objets depuis des mailles
- présence ou absence d'une propriété
- variable aléatoire
- jonction
- connexion
- adjacence
- connectivité
- inclusion
- intersection
- atténuation

author: Marc Le Bihan
---

   \begingroup \cadreAvantage \small

   > Les relations spatiales peuvent être de tout type :
   > 
   >    - La distance entre deux aéroports
   >    - Le prix d'un trajet entre deux lieux, sa durée...
   >    - La parcelle de __A__ jouxte t-elle celle de __B__ ?
   >    - Une maison est-elle à proximité d'une voie de chemin de fer ?
   >    - Une pollution (phénomène) ou nuisance sonore affecte t-elle un environnement ?
   > 
   > La distance, par exemple, n'a de sens qu'en considérant le thème abordé qui amènera selon le cas à prendre en considération : 
   > 
   >    - le moyen de transport utilisé, 
   >    - l'état de la chaussée, 
   >    - la pente...
   >

   \endgroup

\bigbreak

Etudier la topologie, c'est étudier les adjacences.

Un \definition{arrangement} est la disposition relative des objets entre eux,

   > On observe les distributions spatiales pour voir si elles ont des formes, des arrangements particuliers.  
   > Lors de tests d'hypothèses, $\bleu{H_{0}}$ signifiera: pas de structure discernable.

le \definition{voisinage} est la même chose, mais d'un point de vue géométrique ou topologique:
 
   - la distance minimale à un ou des objets
   - la contiguité des objets
 
qui suggèrent des interactions spatiales avec les objets,  
avec des notions de poids accordé à chaque voisin.

\avantage{$\implies$ Arrangements et voisinages rendent les objets dépendants entre-eux.}

et même: le voisinage pose un problème en statistiques:

  > ailleurs, deux observations \bleu{$i$} et \bleu{$j$} pourraient être dites indépendantes, \danger{mais si elles sont voisines, il y a des chances qu'elles ne le soient pas, parce qu'une interaction est susceptible d'exister entre-elles.}

On étudie leur __position__, la __dispersion__, la __proximité__ en deux étapes:

[On envisage d'abord l'espace comme plan, homogène et isotrope](./a032-topologie_espace_plan_homogène_isotrope-synthese_objets_et_dispersion.pdf), dony les dimensions sont géométriques.

[Puis on y intègre l'hétérogénité spatiale et la pondération par la distance](./a033-topologie_distribution_spatiale-et-pondération_par_la_distance.pdf) où:

   - Les _positions_ sont des centres moyens pondérés
   - La _dispersion_, une distance standard pondérée
   - La _proximité_, une distance pondérée, zones de plus grande proximité pondérées

Les relations __topologiques__ expriment comment les objets sont agencés entre-eux, comment nous les situons les uns par rapport aux autres: 

   \begingroup \cadreAvantage \small

   > \underline{Des relations topologiques :}
   >
   > - une rivière traverse un champ
   > - le sens de l'écoulement de l'eau

   \endgroup

Sans elles, impossible de déterminer des voisinages.

Si en mode objet, la topologie est explicite,  
\attention{en mode image, il faut reconstituer les objets}, déterminer leur voisinage à partir des mailles.


# I) Cas nominal: Présence ou absence d'une propriété dans une unité d'observation

Il s'agit de déterminer la distribution spatiale des unités qui ont une propriété.

   \begingroup \cadreAvantage

   > On veut connaître la distribution spatiale des communes qui possèdent :
   >
   >   - un hôpital
   >   - un service bancaire

   \endgroup

## 1) Etiquettage de la présence ou absence d'une propriété dans les unités

La \definition{variable aléatoire} que nous allons étudier est celle de la \definition{jonction}, qui quand on considère deux éléments, peuvent prendre __quatre valeurs possibles__, avec :  

   > __P__ qui signifie présence (de la propriété)  
   > __A__ absence

L'on a les jonctions possibles suivantes :

   > __PP__ présent des deux côtés  
   > __AA__ absent des deux côtés  
   > __AP__ absent d'un côté et présent dans l'autre  
   > __PA__ présent d'un côté et absent de l'autre

Avec :

$\bleu{n_A}$ : le nombre de zones où la propriété est absente  
$\bleu{n_P}$ : le nombre de zones où la propriété est présente  
$\bleu{n}$ : le nombre total de zones

$\bleu{C_i}$ : le nombre de connexions qu'a la zone $i$ avec les autres  
$\bleu{C}$ : le nombre total de connexions

## 2) Calcul de la moyenne et de la variance de $C_{PA}$

### a) Adjacence, connectivité (inclusion, intersection...)

- l'\definition{adjacence}  
- la \definition{connectivité} (l'adjacence pour les réseaux linéaires).  
Elle peut être orientée ou non (sens de circulation, par exemple)  

![Connectivité et matrice d'adjacence](images/connectivite_et_matrice_adjacence.jpg)

_Zones contigües avec la matrice d'adjacence/contigüité_

- l'\definition{inclusion} (cas particulier d'adjacence) : l'unité spatiale est entièrement à l'intérieur d'une autre.  
- l'\definition{intersection}

### b) Calcul de la moyenne et de la variance du nombre de connexions $C_{PA}$

C'est à dire celles qui ont une propriété tandis que celle à laquelle elle est connectée ne l'a pas,  
ou inversement, car $C_{AP} = C_{PA}$.

on va chercher :

   > $\rouge{\mu_{PA}}$ : moyenne du nombre de connexions entre zone à propriété et zone sans  
   > $\rouge{\sigma_{PA}}$ : variance de ce nombre de connexions

# II) Voisinage et atténuation

Quand dira t-on que quelque-chose n'est plus au voisinage d'autre chose ?  
Atténue t-on son effet sur son voisinage brutalement, linéairement, non linéairement ?

![Atténuations possibles](images/attenuation_voisinage.jpg){width=70%}

