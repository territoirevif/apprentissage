---
title: "a033. Topologie : distribution spatiale et pondération par la distance"
subtitle: "l'espace spatial est cette fois vu hétérogène"

keywords:
- topologie
- distribution spatiale
- pondération par la distance
- centre moyen pondéré
- distance standard pondérée
- zones de plus grande proximité pondérées
- arrangement spatial
- voisinage des objets
- tendance centrale
- variabilité
- arrangement d'objets ponctuels
- indice du plus proche voisin
- distance moyenne entre paires de points
- R
- distribution aléatoire
- arrangement d'objets linéaires
- taille moyenne
- indice de densité spatiale
- indice de connexité
- degré de complexité d'un réseau
- nombre de chaînes
- polygones de Thiessen
- arrangement d'objets zonaux

author: Marc Le Bihan
---

Cette fois:

   - Les _positions_ sont des centres moyens pondérés
   - La _dispersion_, une distance standard pondérée
   - La _proximité_, une distance pondérée, zones de plus grande proximité pondérées

L'on observe les objets en groupe, en définissant des indicateurs: 

   - de distribution spatiale, 
   - d'arrangement spatial, 
   - et de voisinage des objets

### La distribution spatiale se base sur :

   - des indices de position: tendance centrale
   - des indices de dispersion: variabilté

# I) Indices d'arrangement spatial

## 1) Arrangement d'objets ponctuels

Quelle est la représentativité spatiale de notre échantillonnage ?  
(On ne peut prélever tous les points: on en prend quelques-uns, qu'on juge représentatifs)

__R__ est un \definition{indice du plus proche voisin} qui compare 

   - une distribution observée 
   - à une distribution aléatoire.

   > \bleu{$d_i$} : la distance du point $i$ à __son plus proche voisin__  
   > \bleu{$S$} : l'aire de la zone d'étude  
   > \bleu{$n$} : le nombre de __points de mesure__
   > $$ \mauve{\overline{d}} = \frac{1}{n}\sum\limits_{i=1}^{n}{d_i} $$
   > $$ \mauve{d_{aléatoire}} = \frac{1}{2\sqrt{\frac{n}{S}}} $$

$$ \rouge{R} = \frac{\mauve{\overline{d}}}{\mauve{d_{aléatoire}}} = \frac{\text{distance moyenne entre les paires de l'échantillon}}{\text{distance moyenne pour une distribution aléatoire}} $$

![Distance Standard](images/R_distributions_points.jpg){width=70%}

## 2) Arrangement d'objets linéaires

Les indicateurs sont la __taille moyenne ($T_{moy}$)__ et l'__indice de densité spatiale ($D$)__.

   > \bleu{$L$} : la longeur totale des lignes  
   > \bleu{$n$} : le nombre d'arcs
   $$ \rouge{T_{moy}} = \frac{L}{n} $$

   > \bleu{$A$} : l'aire de la zone
   $$ \rouge{D} = \frac{L}{A} $$

![Arrangement de lignes](images/arrangement_lignes.png)

\underline{Indice de connexité :} le degré de complexité d'un réseau.  
C'est le nombre de chaînes : le nombre de suites d'arêtes ou arcs reliants $x$ à $y$ (sur la figure ci-dessus, il y en a 8).

   > \bleu{n} : le nombre de noeuds (ou sommets du graphe)  
   > \begingroup \footnotesize _\text{\ \ \ \ } dans l'exemple, il vaut 11_ \endgroup
   $$ \rouge{C} = 0.5n(n - 1) $$

\underline{Distance minimale et zones de plus grande proximité :}

Cette fois-ci, les polygones de Thiessen se font autour des chaînes.

![Arrangement de lignes](images/voisinages_lignes_polygones_thiessen.jpg)

## 3) Arrangement d'objets zonaux

(cf. chapitre dédié)

# II) Descripteurs de relations de voisinage des objets

   - distance aux objets
   - zones de plus grande proximité

## 1) Voisinage spatial de points

Les \definition{polygones de Thiessen} aident à représenter les zones  
\ \ \ \ \ \ dont les points sont __les plus proches__ d'un point,  
\ \ \ \ \ \  que tous les autres points.

![Voisinage spatial de points, polygones Thiessen](images/voisinages_points_polygones_thiessen.png)

## 2) Voisinage de lignes

On définit des indices de __connexité__, de __distance__ et de __proximité__

