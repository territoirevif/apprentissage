---
title: "a038. Topologie : les indicateurs d'arrangement spatial par région dans une zone"
subtitle: "quel est l'arrangement spatial, toutes classes (thématiques) confondues ?"

keywords:
- topologie
- arrangement spatial
- région
- objets zonaux
- landscape
- structure thématique
- polygone de Voronoï
- indice de majorité
- indice de mode
- indice médian
- région la plus proche des autres
- catégorie la plus présente dans une région
- indice de moyenne
- indice de diversité de Shannon
- indice d'entropie
- indice de Simpson
- indice de richesse
- SHDI
- SIDI
- PR
- indice de densité de richesse
- PRD
- indice de régularité de Shannon
- SHEI
- domination d'un type de catégorie ou de classe

author: Marc Le Bihan
---


\ \ \ \ \ \ Nous sommes toujours en train de considérer l'agencement des objets les uns par rapport aux autres. En particulier, des objets zonaux.
 
\ \ \ \ \ \ Cette fois, l'on veut étudier comment ils se placent par \definition{région (landscape)} quelle que soit leur thème (classe).

Une région, c'est un \definition{objet spatial} : on a pu le créer, par exemple, à partir d'une image décomposée en mailles et rassemblées ensuite (cf. chapitre dédié). Ce n'est \underline{pas encore} un \definition{objet géographique} avec ses attributs, qui nous servira réellement.

Ici, il est possible de calculer les valeurs CA, PE, ED, MSI, MPFD, NP, PD, MPS, MNN, NNSD, NNCV, valides pour les classes et décrites au chapitre précédent, mais aussi de détecter une structure thématique.

# I) Indices de structure géométrique et thématique

### \definition{Polygone de Voronoï} associé au point $\mathbf{x_{i}}$ :{#voronoi}

   > Il s’agit de la région qui est plus proche de $\bleu{x_{i}}$ que les autres points de l’ensemble d’étude $\bleu{E}$.
   >
   > \ \ \ \ \ \ \ \ \bleu{$E$} : la région d'étude  
   > \ \ \ \ \ \ \ \ \bleu{$x_{i}$} : un point dont on veut la région la plus proche  
   > $$ \rouge{C(x_{i} | E)} = \big\{ u \in \mathbb{R}^2, \lVert u - x_{i} \rVert = \min_{j=1}^{card(E)} \lVert u - x_{j} \rVert \big\} $$ 

## 1) Indice de majorité (de mode)

C'est la valeur (ou catégorie) la plus présente dans la région

   - En mode objet, celle dont l'aire est majoritaire,
   - En mode image, celle dont le nombre de cellules est le plus grand.

\ \ \ \ \ \ \ \paramLarge{$v_{fmax}$} : la valeur dont l'aire est majoritaire dans l'ensemble de la région (= mode)  
$$ \rouge{\text{MOD}} = v_{fmax} $$

## 2) Indice médian (MED) ordinal

C'est la valeur de l'unité d'observation positionnée au milieu des valeurs triées pour la région.

   - En mode objet, ce sont les valeurs triées qui sont considérées (et l'aire n'intervient pas)
   - En mode image, chaque unité d'observation a la même aire

\ \ \ \ \ \ \ \paramLarge{$v_{med}$} : valeur du rang central pour l'ensemble des unités d'observation de la région  
$$ \rouge{\text{MED}} = v_{med} $$

## 3) Indice de moyenne (AVG) cardinal

C'est la __moyenne pondérée__ par la surface relative de chaque unité.

En mode image, il faut combiner deux grilles:

   - celle qui définit les régions
   - celle qui porte les valeurs thématiques

   > \paramLarge{$v_{i}$} : valeur thématique de l'unité d'observation  
   > \paramLarge{$A_{i}$} : aire de l'unité d'observation

$$ \rouge{\text{AVG}} = \sum\limits_{i=1}^{n} \frac{V_{i}. A_{i}}{\sum\limits_{j=1}^{n} A_{j}} $$

## 4) Indice de diversité de Shannon ou entropie (SHDI), ou de Simpson (SIDI)

Dit si les aires tendent à s'unifier, ou au contraire sont diverses, riches, en fonction du nombre de types de classes qu'on y trouve.

Il est égal à zéro quand la zone est constituée d'un seul type de classe,  
et augmente en fonction du nombre de types,  
et que leurs aires tendent à l'uniformité.

Avec :

\ \ \ \ \ \ \ \bleu{$P_{i}$} : proportion de la région d'étude occupée par la classe (ou catégorie) \bleu{$i$}  
$$ \rouge{\text{SHDI}} = - \sum\limits_{i=1}^{n} (P_{i}.\ln{(P_{i})}) $$

$$ \rouge{\text{SIDI}} = - \sum\limits_{i=1}^{n} P_{i}^2 \text{\ \ \ \ \ \ \ \ \ \ \ \ : qui ira, lui, entre 0 et 1.} $$

## 5) Indice de richesse (PR)

C'est un indice de diversité, qui donne la richesse thématique d'une région.

\ \ \ \ \ \ \ \bleu{$c$} : le nombre de catégories ou de classes (types) dans la région d'étude
$$ \rouge{\text{PR}} = c $$

## 6) Indice de densité de richesse (PRD)

C'est la densité de richesse par unité de 100 hectares.  
Il permet de comparer des zones d'aires difféntes.

\ \ \ \ \ \ \ \bleu{$c$} : le nombre de catégories ou de classes (types) dans la région d'étude  
\ \ \ \ \ \ \ \bleu{$A$} : aire en $\mathbf{m^2}$ de la région d'étude,  
$$ \rouge{\text{PRD}} = \frac{c \times 10000 \times 100}{A} $$

## 7) Indice de régularité de Shannon (SHEI)

Il varie entre 0 et 1 :

   - $\mathbf{0}$ quand il y a un seul type de catégorie ou classe
   - proche de 0, quand les aires des classes sont très différentes ou qu'un type domine
   - $\mathbf{1}$ quand leurs aires sont parfaitement égales (régularité)

\ \ \ \ \ \ \ \bleu{$c$} : le nombre de catégories ou de classes (types) dans la région d'étude  
\ \ \ \ \ \ \ \bleu{$P_{i}$} : la proportion de la surface occupée par la catégorie ou classe dans la région d'étude  
$$ \rouge{\text{SHEI}} = \frac{\sum\limits_{i=1}^{c}\big(P_{i}.\ln (P_{i})\big)}{\ln c} $$
