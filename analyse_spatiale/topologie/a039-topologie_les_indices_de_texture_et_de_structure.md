---
title: "a039. Topologie: les indices de texture et de structure"
subtitle: ""
author: Marc Le Bihan

keywords:
- topologie
- texture
- structure spatiale
- forme
- fenêtre d'ausculation
- tendance centrale
- hétérogénéité
- centre-voisinage
- richesse relative
- fragmentation
- diversité
- dominance
- entropie
- pondération
- poids aux objets
- friction
- difficulté d'accès
- centre moyen pondéré
- distance moyenne pondérée
- interdépendance
- voisinage
- corrélation spatiale
- autocorrélation spatiale
- matrice de connectivité
- matrice de contiguité
- matrice d'adjacence
- adjacence
- arrangement groupé, dispersé, aléatoire
- H0
- hypothèse nulle
- distribution spatiale aléatoire
- échantillon
---

La \definition{structure spatiale}, c'est la distribution spatiale d'objets de __thématiques__ différentes.

   > zones de bâti  
   > de voirie
   > d'espaces verts
   > plans d'eau...

La \definition{texture}, la suggestion de forme que donnent des unités voisines de taille proche, localement.  
C'est une affaire de perception visuelle, que l'analyse spatiale tente de détecter par des indicateurs statistiques.

  > Une zone bâtie de villas ou industrielle se présente différemment.

Le principe est de parcourir la zone d'étude par une fenêtre mouvante: la \definition{fenêtre d'auscultation}, et de détecter les formes qu'on y trouve:

   ![Fenêtre d'auscultation de textures](images/textures_et_formes.jpg)

Il y a quatre types d'indices de structure, qui se fondent sur des notions de:

   - distance minimale entre objets zonaux,
   - et plus grande proximité entre objets zonaux (ci-dessous : distance entre les carrés jaunes (?))

   ![distances minimales et plus grande proximité](images/distance_minimale_et_plus_grande_proximité.jpg)

# I) Les indices de tendance centrale, variabilité, premier et second ordre

## 1) Indices de tendance centrale

   - Mode (nominal)
   - Moyenne (cardinal)
   - Médiane (ordinal)

## 2) Indices de variabilité

   - Hétérogénéité (NDC, nominal) $\approx$ Richesse (PR)
   - Centre-voisinage (CVN, nominal)
   - Fréquence centrale (CCF, nominal)
   - Richesse relative (R, nominal)
   - Fragmentation (F, nominal)
   - Diversité (H, nominal) $\approx$ Indice de Shannon (SHDI)
   - Dominance (D, nominal) $\approx$ Régularité (SHEI)
   - Paires différentes (BCM, nominal)

## 3) Indices de texture de premier ordre

Ils décrivent la variabilité et la dispersion des propriétés sur l'ensemble du voisinage.

   - Diversité (DIV, nominal) $\approx$ Richesse (PR)
   - Interquartile (IQ, ordinal)
   - Ecart-type (SD, cardinal)
   - Amplitude (RNG, cardinal)

## 4) Indices de texture de second ordre

Ils mettent en évidence les transitions de propriétés entre :

   > des paires de propriétés triées par distance  
   > ou par direction

   - Contraste (CONT, cardinal)
   - Entropie (ENT, cardinal)
   - $2^{\text{ème}}$ moment angulaire (2AM, cardinal)

# II) Pondération des arrangements et du voisinage spatial

Selon l'étude que l'on réalise, on accorde plus d'importance à des éléments qu'à d'autres.

   \begingroup \cadreAvantage
   
   > . un centre commercial est plus attractif qu'un immeuble d'habitation  
   > . pour un parcours routier, on observera état de la chaussée, largeur de la route, etc.

   \endgroup

Ce poids on considère qu'il affecte la distance plane. Mais on pondère aussi la dispersion.

On va attribuer un poids à des objets pour dire leur attractivité, influence, friction.  
Une friction, par exemple, pourra traduire une difficulté d'accès.

La conséquence d'attribuer des poids, c'est que les moyennes, la distance standard seront affectés.

Par exemple, le __centre moyen pondéré__ aura maintenant pour coordonnées:
   $$ \rouge{\overline{x_{\omega}}} = \frac{\sum\limits_{i=1}^{n}\omega_{i}x_{i}}{\sum\limits_{i=1}^{n}\omega_{i}} \text{\ \ \ \ \ \ et \ \ \ \ \ \ } \rouge{\overline{y_{\omega}}} = \frac{\sum\limits_{i=1}^{n}\omega_{i}y_{i}}{\sum\limits_{i=1}^{n}\omega_{i}} $$


et la \definition{distance standard pondérée} s'écrira:
$$ \rouge{DS_{\text{pond}}} = \rouge{DS_{\omega}} = \sqrt{\frac{\sum\limits_{i=1}^{n}\omega_{i}\big(x_{i} - \overline{x}\big)^2 + \sum\limits_{i=1}^{n}\omega_{i}\big(y_{i} - \overline{y}\big)^2}{\sum\limits_{i=1}^{n}\omega_{i}}} $$

## 1) Comment rapporter l'interdépendance, le voisinage, la corrélation spatiale... ?

\tab Il y a souvent effet de voisinage entre deux zones: le fait d'avoir tracé une frontière ne fait pas qu'il y a forcément discontinuité de valeurs.

Qu'on appelle cela effet de voisinage, interdépendance, corrélation spatiale, la détection à réaliser reste la même.

Il va falloir mesurer le __degré de dépendance__ entre unité voisines, l'__auto-corrélation spatiale__.

### a) L'indice d'autocorrélation spatiale

[cf. L'auto-corrélation spatiale](a050-auto-correlation_spatiale.pdf)

### b) Etablir la matrice de contiguité

Elle est aussi appelée matrice de connectivité pour les unités linéaires, et est utile quand les phénomènes sont discontinus (par opposition à l'altitude, une teneur en métaux lourds dans un sol, etc) et se traduit par des points, lignes ou zones éparses.

Dans le cas des lignes et zones, pour des phénomènes discontinus, la distance euclidienne n'est pas appropriée: l'on observe les adjacences (ou contiguïtés en vocabulaire SIG).

![Construire une matrice de contiguité](images/construire_matrice_contiguite.jpg)

On détermine les contiguités entre chaque zone, puis l'on fait la somme (ici, dans la ligne __V__).

## 2) Catégoriser l'arrangement spatial des unités

Un arrangement est dit "__groupé__" si les propriétés des unités voisines se ressemblent davantage que celles qui sont éloignées,  
"__dispersé__", quand elles se ressemblent, au contraire, moins que celles distantes.  
"__aléatoire__", quand elles ne sont ni groupées ni dispersées.

Le coéfficient de contiguité (à venir) et l'indice de Moran (à venir) diront le sens de la dépendance spatiale : groupée (positif), dispersée (négatif), aléatoire (zéro)...

![distributions spatiales](images/arrangement_groupe_disperse_aleatoire.jpg)

Statistiquement parlant, l'hypothèse nulle ($H_{0}$ : "_C'est le seul hasard qui intervient_"), est associé à l'arrangement/distribution spatiale aléatoire.

Mais là où nous sommes, nous ne disposons que d'un seul échantillon... Celui de là où nous sommes, avec ses valeurs observées.

Nous aurons une fonction aléatoire 
$$ \rouge{\mu_{0}} = \overline{x} \pm z_{0.05}.s\sqrt{n} \ \ \ \ \ \text{(pour un risque $\alpha$ à 5\%)} $$

si nous nous basons sur une loi normale centrée réduite $N(0,1)$.

Nous avons :

   > $\bleu{I_{obs}}$ : les indices/valeurs que nous connaissons, puisque nous les avons mesurés  
   > $\rouge{\overline{I_{th}}}$ : l'espérance théorique de la distribution aléatoire.  
   > $\rouge{\alpha_{th}}$ : l'écart-type théorique de la distribution aléatoire.
   \begingroup \large $$  z = \frac{I_{obs} - \overline{I_{th}}}{\alpha_{th}} $$ \endgroup
