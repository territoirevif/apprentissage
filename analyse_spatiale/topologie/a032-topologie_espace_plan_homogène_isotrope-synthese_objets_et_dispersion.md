---
title: "a032. Topologie : synthèse d'objets et dispersion"
subtitle: "Espace plan homogène isotrope"

keywords:
- espace plan
- espace homogène
- espace isotrope
- topologie
- point moyen
- centre moyen
- point médian
- centre médian
- centre de cercle
- cercle inscrit
- cercle circonscrit
- écart-type
- distance standard
- distance de Bachi
- écart-type de distance
- distance interquartile
- éllipse de dispersion
- orientation d'un nuage de points
- projection orthogonale sur un axe

author: Marc Le Bihan
---

On envisage l'espace comme plan, homogène et isotrope : les _dimensions_ sont géométriques.

# I) Résumés/synthèses d'objets : points centraux, médians...

## 1) Point moyen (ou centre moyen)

Il minimise \underline{le carré} de la somme des distances aux points de l'ensemble
$$ \overline{x} = \frac{1}{n}\sum_{i=1}^{n}{x_i}\text{\ \ \ \ \ \ }\overline{y} = \frac{1}{n}\sum_{i=1}^{n}{y_i} \text{\ \ \ pour le centre moyen} $$

   \code

   > \underline{Calcul d'un point moyen :}
   >
   > ```java
   > double xmoy = mean(x);
   > double ymoy = mean(y);
   > ```

   \endgroup

\bigbreak

\ \ \ \ \listMoins il dépend du référentiel (axes)  
\ \ \ \ \listMoins il est \attention{plus sensible} aux points qui lui sont éloignés que proches

Ex (dans le tableau ci-dessous) : $C_{\text{moy}}(567808, 177638)$

\ \ \ \ $\rightarrow$ on peut réduire ce phénomène en pondérant le point moyen par l'inverse de sa distance aux autres points.

![Centre moyen ou médian](images/centre_moyen_ou_median.jpg)

## 2) Point médian (ou centre médian)

Il minimise \underline{la somme des distances} aux points de l'ensemble.

   \code

   > \underline{Calcul d'un point médian :}
   >
   > ```java
   > double xmed = median(x);
   > double ymed = median(y);
   > ```

   \endgroup

\bigbreak

Ex: dans le tableau précédent, entre $A$ et $B$, $d(A,B) = 53100$

On peut déduire ce point facilement par la \definition{Distance de Manhattan}
$$ \rouge{d(P,Q)} = |x_{p} - x_{q}| + | y_{p} - y_{q} | $$

mais on ne peut que l'approximer pour une distance euclidienne.

\ \ \ \ \listPlus il ne dépend pas du référentiel  
\ \ \ \ \listPlus il est \avantage{moins sensible} aux points éloignés

## 3) Centres de cercles

D'autres constructions géométriques montrent la centralité dans un nuage de points :

   - le centre du cercle $\text{inscrit}^*$ dans l'enveloppe convexe,
   - centre du cercle $\text{circonscrit}^{**}$

![Cercle inscrit ou circonscrit à un polygone](images/cercle_inscrit_cercle_circonscrit_a_un_polygone.gif){width=40%}

$^*\text{inscrit}$ [dans un polygone] est un cercle qui est tangent à tous les côtés de ce polygone.  
C'est le cercle de plus grand rayon qu'on pourrait faire dans ce polygone.
Son centre est le point où ses bissectrices concourrent.

$^{**}\text{circonscrit}$ [dans un polygone] est celui qui passe par tous les sommets de ce polygone.  
Il n'existe pas toujours.  
Son centre est le point de concours des médiatrices de chaque côté.

# II) Indicateurs de dispersion

## 1) Écart-type

$$
\rouge{s_x} = \sqrt{\frac{\sum\limits_{i=1}^{n}(x - \overline{x})^2}{n - 1}}\text{\ \ \ \ \ \ }\rouge{s_y} = \sqrt{\frac{\sum\limits_{i=1}^n(y - \overline{y})^2}{n - 1}}
$$

   \code

   > \underline{Calcul d'écarts-types d'échantillon :}
   >
   > ```java
   > double sx = standardDeviation(x);
   > double sy = standardDeviation(y);
   > ```

   \endgroup

\bigbreak

Ex: dans le tableau précédent, $S_x = 10743.1$ et $S_y = 13985.8$

## 2) Distance standard (DS) ou distance de Bachi
     
   > \bleu{$s_x$} : écart type de $x$ (c.à.d. intervalle centré sur le centre moyen : $[C_{moy_x} - s_x, C_{moy_x} + s_x])$ 
   > \bleu{$s_y$} : $[C_{moy_y} - s_y, C_{moy_y} + s_y]$
   >
   > Ex: dans le tableau précédent: $[163653, 191624]$ pour $x$, et $[557065, 578551]$ pour $y$  
   > \$rightarrow$ $S_x = 10743.1$ et $S_y = 13985.8$

\bigbreak

   \code

   > \underline{Calcul d'une distance standard :}
   >
   > ```java
   > return Math.sqrt(populationVariance(x) + populationVariance(y));
   > ```

   \endgroup

\bigbreak

   Dans ces intervalles centrés sur $\mathbf{C_{moy}}$  
   s'inscrit un cercle qui est la distance standard. 
     $$ \rouge{\text{DS}} = \sqrt{\frac{\sum\limits_{i=1}^{n}{(x - \overline{x})^2} + \sum\limits_{i=1}^{n}{(y - \overline{y})^2}}{n}} $$  

$\rightarrow$ $\text{DS} = 16943$ pour les valeurs du tableau d'exemple.

   ![Distance Standard](images/distance_standard.png)

\bigbreak

ou encore, avec :

   > \bleu{$P_{i}$} : les points de l'ensemble,  
   > \bleu{M} :  le point moyen,  
   > \methode{$d^2$}{$(M$, $P_{i})$} : le carré de la distance d'un point $i$ au point $M$

$$ \rouge{\text{DS}} = \sqrt{\frac{1}{n}\sum\limits_{i=1}^{n}d^2(M, P_{i})} $$

## 3) Distance interquartile

On peut aussi la produire \definition{distance interquartile}, par rapport au centre \underline{médian}, $C_{med}$.

## 4) Ellipses de dispersion, pour connaître l'orientation du nuage de points

On cherche à connaître l'orientation du nuage de points. Pour cela, on crée une élipse, formée de deux axes.

   - Le premier axe maximise __la somme des distances__ entre : 
   
      - les points projetés sur cette droite [Projeter des points sur une droite](../mathematiques/m200-projeter_des_points_sur_une_droite.md), 
      - et le point qui est le centre moyen (ou médian, selon le choix) qu'on y projete aussi.

   - L'angle de dispersion est l'angle entre le premier axe et l'axe des abscisses.
   - on calcule aussi la distance standard entre les points sur cet axe, 
   - et après avoir déterminé le second axe, orthogonal,
   - on calcule aussi la distance standard secondaire, basée sur cet axe

   \gris{La méthode mathématique complète pour calculer une ellipse de dispersion, manque}

   \avantage{l'ellipse de dispersion (ou Standard Deviationnal Ellipse (SDE)) est un outil des SIG}

## 5) Indice R

## 6) proximité

   - [La proximité, la distance plane, les zones de plus grande proximité...] \gris{à venir}

