---
title: "a037. Topologie: les indicateurs d'arrangement spatial par classe dans une zone"
subtitle: "quelle est la prégnance des objets d'une thématique particulière ?"

keywords:
- topologie
- arrangement spatial
- classe
- prégnance des objets d'une thématique particulière
- agencement des objets
- forme
- taille
- thème
- paysage
- région d'étude
- entité
- patch
- postgis
- aire des objets de la même catégorie
- sql
- périmètre total de tous les objets d'une classe
- forme moyenne
- fractal moyen
- densité d'objets d'une même classe
- CA
- TE
- ED
- MSI
- MPFD
- NP
- PD
- MPS
- voisinage
- indice de distance moyenne aux voisins de même catégorie/classe
- indice de dispersion de la distance aux voisins de la même catégorie/classe
- MNN
- NNSD
- coefficient de variation de la distance aux voisins de la même catégorie/classe
- NNCV

author: Marc Le Bihan
---

Nous sommes en train de considérer l'hétérogénéité spatiale, et l'__agencement des objets les uns par rapport aux autres__. 

Ici, en particulier, pour les objets zonaux.  
À l'échelle zonale, les arrangements et les incides sont dits \definition{de structure}.
 
L'on veut étudier comment se placent ceux qui ont une thématique particulière \definition{classe} par rapport aux autres, en définissant des indicateurs pour décrire cela.

Dans les études de paysage, on aime catégoriser les arrangements d'objets par :

   - forme
   - taille 
   - thème...

Soit:

   - pour toute classe confondue
   - une classe (ou catégorie thématique) particulière

\definition{paysage} : la région d'étude.  
\definition{entité} ou \definition{patch} : les objets zonaux de la zone d'étude.  

__classe__ : les objets zonaux qui sont affectés à une même catégorie.

\avantage{la classe étant définie on va calculer des indices qui va présenter sa prégnance dans la région d'étude}. À partir de la géométrie des objets, notament leur :

# I) Les indices d'arrangement spatial pour une classe (zonal)

   > \bleu{$i$} index qui désigne un objet,  
   > \bleu{$j$} index qui désigne la classe à laquelle il appartient,

En début d'analyse, on aura donc les objets par classe pour pouvoir les parcourir, classe par classe.

   \code
 
   > \underline{Par exemple, on prend l'affectation du territoire dans le gers:}
   >
   > ```sql
   > WITH region_gers AS (
   >    SELECT l.id, l.osm_id, l.fclass, l.name, l.geom
   >       FROM landuse AS l
   >       CROSS JOIN departements AS d -- produit cartésien
   >    WHERE ST_Intersects(l.geom, d.geom)
   >       AND d.code_insee = '32')     -- département du Gers
   > ```
   >
   > et l'on en tire : les zones de forêts, de parcs, de fermes... comme autant de classes  
   > \demitab "recreation\_ground","retail","farmyard","nature\_reserve","scrub","park"...
 
   \endgroup

## 1) Les aires de tous les objets de la même catégorie

$\bleu{a}$ : aire  
$\rouge{\text{CA}(j)}$ : aire totale de la classe d'index $\bleu{j}$
   $$ \rouge{\text{CA}(j)} = \sum\limits_{i=1}^{n_{j}}{a_{i,j}} \demitab \annotation{les aires de tous les objets de la même catégorie} $$

   \code

   > ```sql
   > SELECT fclass, SUM(ST_Area(geom)) AS aire, 
   >        ROUND(CAST(SUM(ST_Area(geom)) / 1000000 AS numeric), 2) AS aire_km2 
   >    FROM region_gers GROUP BY fclass
   > ```

   ```
   j   fclass             aire                aire_km2
   ---------------------------------------------------
   1   recreation_ground  341620.5890249085     0.34
   2   retail             1629870.3841363457    1.63
   3   farmyard           755922.2429632602     0.76
   4   nature_reserve     1144430.887508519     1.14
   5   scrub              1538554.0094855826    1.54
   6   park               559970.4727429125     0.56
   ...
   ```

   \endgroup
 
   et la proportion (\fonction{\%Land}) qu'elle représente dans la région d'étude.  

   \code

   > ```sql
   > SELECT SUM(ST_Area(geom)) AS aireRegionEtude,
   >    ROUND(CAST(SUM(ST_Area(geom)) / 1000000 AS numeric), 2) AS aireRegionEtude_km2 
   > FROM region_gers;
   > ```

\bigbreak

   va retourner 4650665464.84617 $m^2$ = 4650.67 $km^2$,  
   inférieur aux 6301942631.361788 $m^2$ = 6301.94 $km^2$ du département du Gers

   \endgroup

## 2) Périmètre total de tous les objets individuels

$\bleu{p}$ : périmètre  
$\rouge{TE(j)}$ : périmètre total des objets de la classe d'index $\bleu{j}$
   $$ \rouge{\text{TE}(j)} = \sum{p_{i,j}} \demitab \annotation{le périmètre total de tous les objets individuels} $$
   $$ \rouge{\text{ED}(j)} = \frac{\sum\limits_{i}{p_{i,j}}}{\sum\limits_{i}{a_{i,j}}} \demitab \annotation{la densité des contours (périmètres sur aires)} $$

## 3) Forme

   > \underline{forme moyenne:}
   > $$ \rouge{\text{MSI}} = \sum\limits_{i=1}^{n}\frac{p_{i,j}}{n_{i}\sqrt{\prod\limits_{k=1}^{n}{a_{k,j}}}} $$
   > 
   > \underline{fractal moyen:}
   > $$ \rouge{\text{MPFD}} = \sum\limits_{i=1}^{n}\frac{2.\ln{p_{i,j}}}{n_{i}.\ln{a_{i,j}}} $$

## 4) Densité

   > \underline{nombre d'objets:} \bleu{NP}
   >
   > \underline{densité d'objets:}
   > 
   >   > \bleu{$a_{R}$} : aire de la région  
   > $$ \rouge{\text{PD}} = \frac{\text{NP}}{a_{R}} $$
   >
   > \underline{taille moyenne:}
   > 
   >   > \bleu{$a_{C}$} : aire de la classe  
   > $$ \rouge{\text{MPS}} = \frac{a_{C}}{\text{NP}} $$

## 5) Voisinage

   > avec $\bleu{h_{i,j}}$ la distance \attention{la plus courte} entre la \underline{limite} d'un objet $i$ \attention{(attention : ce n'est pas le centre !)} et l'objet $j$ de \underline{de même catégorie}, le plus proche.
   > 
   > \underline{Indice de distance moyenne aux voisins de même catégorie/classe:}
   $$ \textcolor{red}{\text{MNN}} = \sum{h_{i,j}} / n_{i} $$
   >
   > \underline{Indice de dispersion de la distance aux voisins de la même catégorie/classe:}
   $$ \textcolor{red}{\text{NNSD}} = \sqrt{\frac{\sum{[h_{i,j} - (\sum{h_{i,j} / n_{i}})]^2}}{n_{i}}} $$
   >
   > \underline{Coefficient de variation de la distance aux voisins de la même catégorie/classe:}
   $$ \textcolor{red}{\text{NNCV}} = 100 \frac{\text{MNN}}{\text{NNSD}} $$
  
   > \attention{Attention : ici, \bleu{$i$} désigne un objet, et \bleu{$j$} un autre objet voisin, mais pour \bleu{$h_{i,j}$} seulement}.

