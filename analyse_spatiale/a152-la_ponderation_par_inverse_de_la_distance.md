---
title: "a152. La pondération par l'inverse de la distance"
subtitle: "adaptée aux GRID"
author: Marc Le Bihan
keywords:
- pondération
- distance
- inverse de la distance
- estimation
- point estimé
- point interpolé
- interpolation de delaunay
- GRID
---

La \definition{pondération par l'inverse de la distance} pondère les points interpolés  
\demitab \avantage{en fonction de la distance qui les sépare}  
\demitab du point où la valeur est estimée.

Elle est adaptée au GRID.

## 1) Le point connu et le point candidat, et un paramètre de puissance

   > \bleu{$x_k$} est un point connu
   >
   >  > \bleu{$u_k$} la valeur qu'il porte,  
   > \   
   >
   > \bleu{$x$} est le point candidat, interpolé
   >
   >   > \rouge{$u(x)$} : sa valeur, qu'on veut estimer  
   > \   
   >
   > \mauve{$p$} est un paramètre de puissance choisi :
   >
   >    \begingroup \small \demitab plus il sera grand  
   >    \demitab plus il donnera plus d’influence  
   >    \tab aux points les plus proches de celui interpolé \endgroup  

## 2) Une fonction de calcul de poids

   > \mauve{$w_k(x)$} : un calcul intermédiaire de poids 
   > 
   >    \begingroup \small \demitab que l'on va attribuer en fonction de la distance  
   >    \tab entre le point connu  
   >    \tab et celui interpolé \endgroup
   >
   >   > \begingroup \Large 
   >   > $\mauve{w_k(x)} = \frac{1}{d(x, x_k)^p}$ 
   >   > \endgroup

## 3) L'interpolation

\begingroup \large $$ \textcolor{red}{u(x)} = \frac{\sum\limits_{k=0}^{n}(w_k(x) . u_k)}{\sum\limits_{k=0}^{n}w_k(x)} $$ \endgroup

![Application Numérique pondération inverse de la distance](images/an_ponderation_inverse_distance.jpg)

