---
title: "a092. Analyses locales et globales"
subtitle: ""
author: Marc Le Bihan

keywords:
- analyse
- analyse locale
- analyse globale
- premier ordre
- second ordre
- analyse absolue
- analyse relative
- autocorrélation spatiale
- hétérogénéité
- surdispersion
- tendance
- centralité
- taille
- forme
- excès de risques
- aggrégat
- point chaud
- point froid
- regroupement
- attraction
- répulsion
---

La \definition{distribution spatiale} est aussi appelée \definition{arrangement}.  

Les analyses sont: 

   - globales
   - locales 
   - absolues 
   - ou relatives

et ont plusieurs buts.

\bigbreak

Par exemple, étendre les propriétés d'un lieu où l'on a des informations à un autre lieu où l'on en a pas, comme l'occupation d'un sol, \avantage{discontinue}.

## 1) Les analyses globales

Les analyses globales sont dites de premier ordre:

   - [l'autocorrélation spatiale](a050-auto-correlation_spatiale.pdf)
   - hétérogénéité
   - surdispersion 
   - tendance
   - centralité
   - taille
   - forme (observée par des mesures et des indices)

## 2) Les analyses locales

Les analyses locales sont dites de second ordre.

Ce sont les variations locales de la distribution spatiale:

   - excès de risques
   - aggrégats
   - points chauds
   - points froids

### a) La détection des lieux particuliers

Des points ou surfaces particuliers:

   - regroupement
   - centralité
   - attraction
   - répulsion

### b) Capacité d'estimation, d'interpolation, d'un point quelconque de l'espace

C'est à dire, d'après la distribution spatiale des objets ou leurs valeurs.

