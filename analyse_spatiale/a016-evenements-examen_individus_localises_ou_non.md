---
title: "a016. Evènements, individus"
subtitle: "Etude des évènements, individus sans et avec leur localisation"
author: Marc Le Bihan
keywords:
- évènement
- fréquence d'apparition d'un évènement
- booléen
- vérifier une hypothèse
- échantillon de non-cas
- échantillon témoin
- variance dans un rayon
- interpolation
- individu
- évènement aléatoire
- loi de Poisson homogène
- évènements indépendants
- loi de Poisson non homogène
- Cox
- loi de Markov
- loi de Gilles
- évènement générateur d'autre évènement
- Neyman-Scott
- évènements générés et distribués uniformément dans un rayon
- évènements générés et distribués de façon gaussienne
- Thomas
- Mattern
- domaine spatial continu
- intensité continue
- surface de tendance
- analyse temporelle
- création de groupes
- comparaison de groupes
- gradient
- continuité
- direction
- forme
- regroupement
- dispersion
- indice de synthèse
- distribution spatiale
- dépendance spatiale
- mesure de l'autocorrélation spatiale
- tendance globale
- modélisation
- centres
- lieux sources
- aggrégat
- exclusion
- points chauds
- points froids
- cas index
- reconstruction de parcours
- modèle de diffusion
- modèle d'extinction
- évènement provoquant/rapportant un changement d'état
- analyse de série chronologique
- fluctuation saisonnière
- cycle
- calcul de taux impose aggrégat
- attention au risque de désaggréger une donnée aggrégée
---

\underline{Rappels:}

   - Une variable peut être une \avantage{fréquence d'apparition d'un évènement} bâtie à partir d'un booléen qui dit si l'évènement a eu lieu ou non. 

   - Lorsqu'on a des données sur lesquelles on veut \definition{vérifier une hypothèse}, il faut créer en regard un échantillon de __non-cas__, qu'on appelle __témoins__ et servent de référence.

### Examen de la variance dans un rayon

   1. Observer la variance des valeurs dans un certain rayon autour d'un point __P__.

   2. Déplacer ce point __P__, augmenter ou diminuer __R__, pour qu'elle évolue.

   3. Et à chaque point __P__ associer une variable aléatoire __Y__ dite \avantage{régionalisée}, en faisant l'hypothèse qu'elle suit une même Loi sur tout ce domaine d'étude, où l'on suppose le phénomène identique.

### Autres manipulations fréquentes

   - Il peut être utile de définir une courbe à partir des points étudiés, par \donnee{interpolation}.
   - \avantage{Une agrégation transforme les objets mobiles en immobiles}. Centroïdes.

# I) Etude des évènements dans un sous-ensemble du domaine d'étude

Les évènements, localisés, affectent des individus soit :

   - instantanément
   - durant un certain temps

## 1) Evènements aléatoires et indépendants
   
   - de même probabilité en tous points : \definition{Loi de Poisson Homogène}
   - de probabilité variable : \definition{Loi de Poisson non Homomgène}
   - ou avec une densité qui varie au fil du temps : \definition{Cox}
   
## 2) Evènements non indépendants
   
exemple : une contagion, où la possibilité d'évènements s'accroît au voisinage du point où elle est survenue.

Loi de \definition{Markov} ou \definition{Gilles}.
   
## 3) Evènements générateurs d'autres évènements
   
Un évènement génère :
   
   - un \avantage{autre processus} de même loi que lui : \definition{Neyman-Scott}
   
   - un \avantage{nombre aléatoire d'autres évènements distribués uniformément} dans un rayon __R__ : \definition{Mattern}
   
   - un \avantage{nombre aléatoire d'autres évènements distrués de façon gaussienne} et __isotrope__ (mêmes propriétés dans toutes les directions) autour de l'évènement : \definition{Thomas}
   
# II) Analyse des individus

\underline{Remarque :}
   
   > Beaucoup de modèles théoriques reposent sur des domaines spatiaux \attention{continus},
   >
   >   > Or, on mesure souvent des __points__ (individu, ferme, habitat...)   
   >   > un évènement dépend souvent de plusieurs variables.
   >
   > Alors on résume les évènements et leur distribution spatiale par une \definition{intensité} continue.

## 1) Examen des individus indépendamment de leur localisation

   1. On \donnee{étudie les individus en univarié} (moyennes, fréquence...)  
   à partir de données individuelles ou regroupées.
   
   2. on \donnee{donne un nom} aux éléments importants pour les identifier.
   
   3. L'on fait une première analyse visuelle, pour observation.

      - \donnee{cartographie}
      - \donnee{représentation graphique}
      - \donnee{surface de tendance} (la distribution spatiale, sans les variations et le bruit)

   sans en tirer de preuves pour l'instant.
   
   4. Si les données comportent une date, \donnee{l'on fait une analyse temporelle} (cf. plus bas).
   
   5. Puis, l'on \donnee{crée des groupes}

      - à partir d'un attribut qualitatif (ex: cas témoins, sexe, classe d'âge...) 
      - ou spatial (appartenance à un lieu, découpage géographique...)

   6. Et l'on \donnee{compare ces groupes entre-eux}.

## 2) Examen des individus d'après leur localisation

Cette fois-ci, l'on étudie la distribution spatiale des individus

   1. En examinant leur/le/la : 
   
      - \donnee{gradient} 
      - \donnee{continuité} 
      - \donnee{direction} 
      - \donnee{forme}
   
   2. L'on \donnee{caractérise la distribution spatiale par des indices de synthèse} :
   
      - regroupement/dispersion
      - dépendance spatiale
      - mesure de l'autocorrélation spatiale
   
   3. Puis l'on recherche la forme globale du phénomène :
   
       - tendance globale du phénomène
       - distribution spatiale théorique...
       - ...ou processus permettant de le modéliser.
   
   4. Et l'on recherche les lieux singuliers
   
      - Centres et lieux sources
      - Aggrégats
      - Exclusions
      - Points chauds
      - Points froids
      - Etude des relations spatiales au niveau des individus

## 3) Quand le temps est disponible, l'on réalise des analyses spatio-temporelles

   - Recherche des \avantage{cas index} (exemple en santé : le patient zéro)
   - Reconstruction de parcours
   - Apparition-déplacement-disparition d'agrégat
   - Modèles de diffusion
   - Modèles d'extinction

Ls \definition{évènements} qui provoquent un changement d'état sont souvent sans données, mais avoir leur date est important : présente, elle permet des __analyses de séries chronologiques__ : 

   - tendance globale
   - périodicité
   - fluctuations saisonnières
   - cycle

## 4) Agrégats et groupes

Les données en agrégats, ou groupes, sont moins localisables. $rightarrow$ les évènements qui affectent les individus sont confondus avec l'unité géographique agrégatrice.  
\danger{On ne peut désagréger des données sans terribles risques (et précautions !)}

\avantage{Reste que les agrégats sont indispensables !}  
Un \avantage{calcul de taux} ne peut se faire que sur la base d'un effectif agrégé quelque-part !  
Si les données sont regroupées (par exemple, par âge, commune...) on aura souvent des moyennes, taux, fréquences.
