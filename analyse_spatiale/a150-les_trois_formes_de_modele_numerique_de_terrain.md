---
title: "a150. Les trois formes de Modèle Numérique de Terrain"
subtitle: "GRID, TIN et courbes d'altitude"
author: Marc Le Bihan
keywords:
- relief
- topographie
- modèle numérique de terrain (MNT)
- modèle numérique d'élévation (MNE)
- modèle numérique d'altitude
- GRID
- Triangulated Irregular Network (TIN)
- courbes d’altitude
- isoligne
- altimétrie
- photogrammétrie
---

Le \definition{relief} désigne l'ensemble des irrégularités en creux ou en saillie de la terre.  
Son étude est la \definition{topographie} (\attention{différente de la topologie !})

Le relief intervient dans les écoulements, la détection de parcelles à l'ombre ou ensoleillées.

## 1) Le modèle numérique de terrain et celui d'élévation

Le \definition{Modèle Numérique de Terrain} porte sur le terrain.

Le \definition{Modèle Numérique d'Élévation (MNE)} encore appelé \definition{Modèle Numérique d'Altitude (MNA)} inclut la hauteur de la végétation et des bâtiments.  

   > le satellite ne sait que rapporter celui-ci, et il faut le travailler pour en tirer un MNT.

## 2) Le GRID

   ![Formes de MNT](images/grid_tin_courbes-niveaux.jpg)
   
Le \definition{GRID} est un raster dont la couleur des pixels disent l'altitude.  
il est du monde Raster.  
et subit fortement l'effert de reprojections successives.  
\avantage{il est le plus apprécié, car proche des rasters}
 
## 3) Le Triangulated Irregular Network (TIN)

Le \definition{Triangulated Irregular Network (TIN)}, en triangles contigus, pourvu d'une coordonnées $\bleu{z}$ représentant l'altitude.

il est du monde vectoriel,  
et n'est pas affecté par des reprojections successives.  
\avantage{il est apprécié pour sa précision}

## 4) Les courbes d'altitude   

Les \definition{courbes d'altitudes} sont des isolignes, qui présentent le relief sur les cartes topographiques,  
elles sont du monde vectoriel.

\underline{Attention :} elles ne peuvent servir à calculer une pente, une orientation, un ombrage directement :  
\demitab $\rightarrow$ il faut les convertir en GRID ou TIN avant.

\bigbreak

l'\definition{altimétrie} détermine l'altitude ou la différence de hauteur entre deux points en utilisant des mesures de :

   - de distance
   - de durée de trajet d'une onde
   - d'angle

Ces opérations reposent sur des calculs trigonométriques.  

   > Instruments : 
   >
   > - GPS 
   > - altimètre radar 
   > - altimètres lidar 
   > - interfèromètres radars

\bigbreak

La \definition{photogrammétrie} utilise la \definition{parallaxe} obtenue entre des images acquises selon des points de vue différents de position et de formes des objets dans l'espace.

Exploitation de couples \definition{stéréoscopiques} d'images, en particulier.  

   > Instruments : 
   > 
   > - images satellites 
   > 
   >    - SPOT 
   >    - Pleiades

## 5) Ce qu'apportent les SIG aux modèles numériques de terrain

La précision d'un MNT s'évalue par :

   - la précision de la localisation d'un point dans le cas d'un TIN ou du centre du pixel (GRID) : __planimétrique__   
   - la qualité de l'estimation de l'altitude : __altimétrique__
   - la résolution __géométrique__

\bigbreak

Les SIG intègrent des fonctions pour construire des MNT à partir de :

   - semis de points disposés régulièrement selon un carroyage spatial,
   - ou situés en des points remarquables (sommet d'une montagne)
   - lignes de cassures
   - combinaisons d'autres MNT sous forme de GRID, TIN, courbes de niveaux

\bigbreak

Leurs algorithmes d'interpolation sont améliorés pour prendre en compte :

   - des contraintes liées aux ruptures dans le relief :

      - falaise
      - effondrement
      - surcreusements dus à l'érosion...

\bigbreak

\underline{Sources de MNT et MNE :}

   - __GTOP30__ (ou SRTM-30 ?) est un MNE global
   - __GDEM Aster__
   - __BD ALTI__ de l'IGN

