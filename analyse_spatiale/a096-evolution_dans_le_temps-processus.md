---
title: "a096. Evolution dans le temps : les processus"
subtitle: "et les évènements qui les font apparaître, se diffuser, disparaître"

keywords:
- temps
- processus
- processus d'émergence
- processus de diffusion
- processus d'extinction
- probabilité d'évènement
- apparition d'un évènement
- durée d'un évènement
- individu
- situation initiale des individus
- intéractions entre individus
- intéractions avec l'environnement
- facteur régionalisé
- facteur environnemental
- facteur de vulnérabilité
- cartographie rétroactive d'un évènement

author: Marc Le Bihan
---

## 1) Les processus

Par la modélisation spatio-temporelle, on analyse les processus :

   - d'émergence
   - de diffusion
   - d'extinction

Nous allons modéliser l'influence de chaque variable sur un processus.

## 2) Etablir une probabilité d'évènement

### a) Un facteur influe sur l'apparition ou la durée d'un évènement

Un facteur, c'est une variable qui va participer à l'apparition ou à la durée d'un évènement. Par exemple, en santé : un facteur de risque qui peut provenir :

   - de la situation initiale des individus eux-mêmes :

      - âge
      - caractérisitiques physiques
      - ou socio-économiques

   - de l'intéraction entre individus :

      - contact
      - contagion
      - relations de proximité

   - l'intéraction avec l'environnement :

      - appartenance à un espace  
        et dans celui-ci, la probablitité de présence d'un évènement

### b) Déterminer les variables du système

Il faut déterminer les variables du système pour donner à chaque individu une probabilité d'évènement, d'après :

   - des facteurs individuels
   
      - La plupart sont régionalisés, et souvent considérés par une moyenne.
   
   - des facteurs environnementaux : 

      - mobiles 
      - ou immobiles

   - des facteurs de vulnérabilité
   - des intéractions entre individus

## 3) La cartographie spatio-temporelle

Lorsque le temps est engagé, on aime produire la cartographie rétroactive d'un phénomène. Soit par :

   - des positions, valeurs ou indices (ou des synthèses de ceux-ci),  
   - des flèches qui montrent :  
      - un déplacement
      - ou des évènements qui se suivent dans le temps  
   - des flèches qui montrent des évènements qui affectent des individus

