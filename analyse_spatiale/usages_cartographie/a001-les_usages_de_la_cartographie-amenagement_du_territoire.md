---
title: "a001 - La cartographie en aménagement du territoire"
subtitle: "Usages et données utiliséees"
author: Marc Le Bihan
keywords:
- aménagement du territoire
- cartographie
- zone d'affectation
- cadastre
- adduction
- evacuation d'eau
- canalisation
- réseau
- équipements
---

En aménagement du territoire, les usages de la cartographie sont :

   - Aménagement du territoire
   - Zone d'affectation
   - Cadastre foncier
   - Planification communale
   - Adduction
   - Evacuation d'eau (canalisation, réseau)
   - Equipements

