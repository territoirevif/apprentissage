---
title: "a008. La cartographie en géologie"
subtitle: "son usage et les données habituelles qu'elle manipule"
author: Marc Le Bihan
keywords:
- cartographie
- géologie
- prospection minière
- analyse du sol
- métaux lourds
- granulométrie
- géomorphologie
- glissement de terrain
---

En géologie, les usages de la cartographie sont :

   - Prospection minière
   - Analyse du sol pour échantillonage (métaux lourds, granulométrie)
   - Géomorphologie (glissement de terrain)
   - Indicateur géomorphologique

