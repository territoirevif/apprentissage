---
title: "a000 - Les usages de la cartographie"
subtitle: "les analyses qu'on réalise et les données habituelles qu'on manipule"
author: Marc Le Bihan
keywords:
- cartographie
- obsevation
- analyse
- source de données
- évènement
- processus
- indicateur
---

Dans les documents de la série relative aux usages de la cartographie, sont indiqués si l'on connait dans leur contexte métier, les :

   - Observations types et objectifs 
   - Analyses types 
   - Sources de données
   - indicateurs, états (attributs observés d'un individu)
   - évènements
   - processus 
   - données externes souvent mises en relation

que l'on fait, observe ou utilise.

