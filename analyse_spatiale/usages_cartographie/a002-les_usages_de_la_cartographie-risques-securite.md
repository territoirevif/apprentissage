---
title: "a002. La cartographie en analyse des risques et sécurité"
subtitle: "son usage et les données habituelles qu'elle manipule"
author: Marc Le Bihan
keywords:
- risque civil
- risque naturel
- crue
- ouragan
- épidémie
- feu de forêt
- accidentologie
- sécurité
- cartographie
- sécurité 
---

En analyse des risques et sécurité, les usages de la cartographie sont :

   - Risques civils
   - Risques naturels : crues, ouragans, épidémies
   - Protection civile : modélisation propagation d'un feu de forêt
   - Accidentologie

