---
title: "a012. La cartographie pour le tourisme"
subtitle: "son usage et les données habituelles qu'elle manipule"
author: Marc Le Bihan
keywords:
- cartographie
- tourisme
- lieu touristique
- terroir
- analyse de l'attractivité
---

Pour le tourisme, les usages de la cartographie sont :

   - Lieux touristiques
   - Terroirs
   - Analyse de l'attractivité

