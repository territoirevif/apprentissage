---
title: "a009. La cartographie en géographie"
subtitle: "son usage et les données habituelles qu'elle manipule"
author: Marc Le Bihan
keywords:
- cartographie
- géographie
- zone inondable
- hydrologie
- bassin versant
---

En géographie, les usages de la cartographie sont :

   - Zone inondable
   - Hydrologie de micro-bassins versants
   - Hydrologie de bassin versant

