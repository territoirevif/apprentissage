---
title: "a010. La cartographie en environnement"
subtitle: "son usage et les données habituelles qu'elle manipule"
author: Marc Le Bihan
keywords:
- cartographie
- environnement
- pollution
- polluant
- biotope
- écoulement
- ombre
- ensoleillement
---

En environnement, les usages de la cartographie sont :

   - Détermination d'une zone affectée par un polluant
   - Zone de protection d'un biotope
   - Ecoulements
   - Détection de parcelles à l'ombre ou ensoleillées

