---
title: "a014. La cartographie, autres usages"
subtitle: "son usage et les données habituelles qu'elle manipule"
author: Marc Le Bihan
keywords:
- cartographie
- phénomène
- planification régionale
---

D'autres usages de la cartographie interviennent autour de/des/de la :

   - Représentation contextuelle d'un phénomène
   - Étude paysagère
   - Planification régionale
   - Adéquation de l'offre à la demande

