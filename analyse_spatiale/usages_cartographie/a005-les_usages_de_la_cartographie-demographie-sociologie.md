---
title: "a005. La cartographie en démographie, sociologie"
subtitle: "son usage et les données habituelles qu'elle manipule"
author: Marc Le Bihan
keywords:
- cartographie
- démographie
- sociologie
- habitat
- lieu de travail
- criminalité
- stratification sociale
---

En démographie et sociologie, les usages de la cartographie sont :

   - Études démographiques
   - Relation habitat - lieu de travail
   - Criminalité
   - Stratification sociale

