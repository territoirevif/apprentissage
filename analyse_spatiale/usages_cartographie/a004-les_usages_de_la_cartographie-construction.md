---
title: "a004. La cartographie en construction"
subtitle: "son usage et les données habituelles qu'elle manipule"
author: Marc Le Bihan
keywords:
- construction
- ouvrage
- implantation
- antenne téléphonique
- emplacement
---

En construction, les usages de la cartographie sont :

   - Impact des constructions
   - Implantation d'un ouvrage
   - Recherche d'emplacement pour la pose d'une antenne téléphonique

