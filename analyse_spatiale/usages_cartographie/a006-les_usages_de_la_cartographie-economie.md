---
title: "a006. La cartographie en économie"
subtitle: "son usage et les données habituelles qu'elle manipule"
author: Marc Le Bihan
keywords:
- cartographie
- geomarketing
- geopolitique
- mouvements migratoires
---

En économie, les usages de la cartographie sont :

   - Géomarketing
   - Socio-économie
   - Géopolitique
   - Mouvements migratoires

