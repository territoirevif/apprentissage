---
title: "a003. La cartographie en santé"
subtitle: "son usage et les données habituelles qu'elle manipule"
author: Marc Le Bihan
keywords:
- sexe
- âge
- taille
- consommation de soins
- consommation de médicaments
- santé
- cartographie
- statut immunitaire
- exposition à un facteur de risque
- hôpital
- nombre de lits
- nombre de médecins
- pharmacie
- Programme de Médicalisation des Systèmes d’Information (PMSI)
- open data PMSI
- open data CPAM Ameli
- open data Score Santé
- acte de soin
- infection
- affection de longue durée (ALD)
- patientèle
- territoire de santé
- épidémiologie
- pathogène
- maladie
- médecin
---

En santé, les usages de la cartographie et les données habituellement utilisées sont :

## 1) Attributs régulièrement étudiés

   - sexe, taille, âge, indice de masse corporelle...
   - profession
   - données génétiques
   - consommation de soins et de médicaments
   - statut immunitaire
   - symptômes (et sévérité de ceux-ci)
   - exposition à un facteur de risque  
   - hôpital (en tant qu'individu) :
      - nombre de lits, de médecins, d'infirmiers, spécialité, volume des actes  
   - dans une commune on pourra recenser : 
      - nombre d'hôpitaux, de médecins, de pharmacies, laboratoires d'analyse

### a) données externes
 
   - environnement naturel : température, humidité, état et type de la végétation...
   - urbain, démographique, environnement social
   - infrastructures, réseau de transport  

### b) Sources

   - hôpitaux, réseaux de pharmacies, Ministère de la Santé, assurance maladie, centres de santé  
   - des réseaux de surveillance suivent les évolutions spatio-temporelles, médecins sentinelles  
   - vétérinaires
   - acteurs de la prévention

### c) Open data

   - Base de données du [Programme de Médicalisation des Systèmes d'Information (PMSI), en accès contrôlé  
https://www.atih.sante.fr/bases-de-donnees/commande-de-bases](https://www.atih.sante.fr/bases-de-donnees/commande-de-bases)       

   - PMSI sur [Scan santé  
https://www.scansante.fr/opendata](https://www.scansante.fr/opendata)

   - [CPAM (Ameli)  
https://assurance-maladie.ameli.fr/etudes-et-donnees/donnees/bases-de-donnees-open-data/liste-bases-de-donnees-open-data](https://assurance-maladie.ameli.fr/etudes-et-donnees/donnees/bases-de-donnees-open-data/liste-bases-de-donnees-open-data)

   - [Score santé](https://www.scoresante.org/)
     [https://www.scoresante.org/](https://www.scoresante.org/)

## 2) Evènements observés

   - acte de soin
   - entrée à l'hôpital
   - infection ; Affection de Longue Durée (ALD)
   - expression de l'effet d'une maladie
   - changement de statut immunitaire

\bigbreak

Remarque : certaines maladies sont à déclaration obligatoire.

## 3) Analyses réalisées

   - relation entre variable de santé et risque supposé
   - incidence, prévalence, densité, risque relatif, _odd ratio_, par de risque attribuable...
   - accessibilité, inégalités, patientèles, construction de territoires de santé  
   - Épidémiologie
   
     - présence d'un pathogène, d'une maladie

