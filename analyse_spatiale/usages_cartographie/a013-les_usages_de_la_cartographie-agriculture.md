---
title: "a013. La cartographie en agriculture"
subtitle: "son usage et les données habituelles qu'elle manipule"
author: Marc Le Bihan
keywords:
- cartographie
- agriculture
- agroalimentaire
- pléiade
- landsat
- sentinel-2
- registre parcellaire
---

En agriculture, les usages de la cartographie sont :

[à compléter]

## Sources de données

[Data science et modélisation pour l'agriculture et l'agroalimentaire](http://www.modelia.org)  

[Le Registre Parcellaire Graphique (RPG)](https://geoservices.ign.fr/rpg#tab-3) d'IGN / Geoservices

[Theia/Muscate CNES](https://theia.cnes.fr/)  
Pléiades, Spot World Heritage, Spirit, Landsat L2A, Sentinel-2  

