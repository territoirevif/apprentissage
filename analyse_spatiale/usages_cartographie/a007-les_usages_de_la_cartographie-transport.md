---
title: "a007. La cartographie dans les transports"
subtitle: "son usage et les données habituelles qu'elle manipule"
author: Marc Le Bihan
keywords:
- cartographie
- gestion de réseaux
- suivi en temps réel de véhicules
- trafic urbain
- transport
- analyse des déplacements
---

En transport, les usages de la cartographie sont :

   - Gestion de réseaux
   - Suivi en temps réel des véhicules
   - Trafic urbain
   - Transport (en général)
   - Trafic régional
   - Analyse des déplacements
   - Bruit produit par le trafic routier


