---
title: "a201. Extraction, modélisation et définition d'objets"
subtitle: ""
author: Marc Le Bihan
--

__Actions concrètes :__

1. Définir des \definition{objets spatiaux} qui ont plusieurs informations (nos futures couches).
2. Extraire des \definition{objets géographiques} qui ont un seul thème, mais plusieurs attributs.

__Comment :__

   - Ajouter un champ géométrique dans sa bonne projection, à une table qui n'en a pas encore
   - Découper en mailles une images
   - Former des régions depuis une image

__Outils :__

   - PostGIS : [Ajout d'une géométrie à une table](#ajout-geometrie)
   - PostGIS : [Reprojection](#reprojection)
   - PostGIS : [Déterminer la projection d'une couche](#determiner-projection)

---

Outre les : 

   - mesures 
   - observations 
   - sondages 

venus d'études sur le terrain, en géographie ce sont surtout des :

   - photographies aériennes 
   - images (et images satellite) 
   - cartes papier numérisées

qui servent à créer les formes auxquelles vont appartenir des données dans le SIG.

   > \underline{Exemple :} la forme de _Rouen_, là où est _Rouen_,  
   > pour qu'on y associe : sa population : 110 000 habitants, température moyenne en été : 19.8°C, en hiver : 5.2°C...

Cette forme s'appelle une \definition{géométrie}.

# I) Déclarer une géométrie dans une table PostGIS

## 1) Le champ géométrique

Quand on désire localiser quelque-chose, par exemple : des commerces

\begingroup 
\cadreAvantage
\small

>   {\textcolor{gray}{nom:} "Fruits du Pays", \textcolor{gray}{type:} MARAICHER, \textcolor{gray}{ouverture:} 9h30, \textcolor{gray}{fermeture:} 19h00}  
>   {"Bijoux fantaisie", BIJOUTERIE, 10h00, 19h00},  
>   {"Maison de la presse", LIBRAIRIE, 8h00, 19h00}

\endgroup

\bigbreak

on place une géométrie pour dire où ils sont,  
en créant un nouveau champ, qu'on appelle \definition{\emph{geom}} : sa projection cartographique (en anglais : `SRS`, cf. [géodésie et projections](analyse_spatiale-a120-géodésie-et-projections.pdf)) cette géométrie utilise.

\bigbreak

   - __WGS84__ que _PostGIS_ connaît sous le code `4326`.  
   et on lui dira : `SRID` (c.à.d. identifiant du Système de Référence (SR)) = 4326  
   - __Lambert93__ que _PostGIS_ connaît par le `SRID = 2154`, pour la __France Métropolitaine__
   - __UTM40S__ de `SRID 32740`, pour __La Réunion__
   - __UTM20N__, `SRID 32620`, pour la __Martinique__ et __Guadeloupe__
   - __UTM22N__, `SRID 32622`, pour la __Guyane__
   - __RGM04__, `SRID 4471`, pour __Mayotte__
   ...

\bigbreak

Leur nom est souvent préfixé par `EPSG` (_European Petroleum Survey Group_), les premiers à avoir informatisé ces projections, et qui leur ont associés les codes 4326, 2154, 4471...

\bigbreak

   \begingroup \cadreAvantage \small

   > Pour des commerces __en France Métropolitaine__, on peut choisir une projection :
   >
   >   - __Lambert93__ (`SRID = 2154` que _QGIS_ nommera `EPSG:2154`), et alors on pourra présenter les commerces sur une carte, \attention{mais cette carte ne pourra pas montrer en même temps des commerces d'outremer, aux projections différentes},
   >
   >   - la projection __WGS84__ (`SRID = 4326` ou `EPSG:4326` dans _QGIS_), mais qui n'est pas cartographique : elle laisse des coordonnées en latitude, longitude, se rapportant au globe terrestre.  
   > Elle dit : "_Je ne m'occupe pas de projeter cartographiquement maintenant_", \danger{mais il faudra s'en occuper plus tard, sinon la carte ne vaudra rien : présentée à l'écran ou sur papier (qui sont plats), elle sera déformée}.

   \endgroup

## 2) Le champ géométrique dans PostGIS

### a) Ajouter un champ géométrique à une table qui n'en a pas{#ajout-geometrie}

En créant une nouvelle colonne géographique `geom` (en 2D : Polygon) dans une table, `SRID = 4326 (WGS84)`, puis en alimentant celle-ci :

\bigbreak

   \begingroup \code
   
   > \underline{Ajouter un champ géométrique à une table qui n'en a pas encore :}
   >
   > ```sql
   > SELECT AddGeometryColumn('public', 'landuse', 'new_geom', 4326, 'MULTIPOLYGON', 2);
   > 
   > SELECT ST_SRID(geom) from landuse; -- Pour connaître sa projection
   > ```

   \endgroup

\bigbreak

   \danger{\underline{Remarque :} Eviter de faire porter deux géométries au même enregistrement !}

Sous _PostGIS_, il est possible de créer une géométrie avec \fonction{ST\_GeomFromText} ou des fonctions plus spécialisées comme \fonction{ST\_MakePoint}  
en précisant le \definition{système de coordonnées spatiales} (cf. [géodésie et projections](analyse_spatiale-a120-géodésie-et-projections.pdf)) voulu.

### b) Reprojection{#reprojection}

La reprojection se fait avec \methode{ST\_Transform}{(Geometry, SRID)} où \bleu{SRID} = l'EPSG du système cible.

   \begingroup \code

   > \underline{Reprojeter un champ géométrique :}
   >
   > ```sql
   > UPDATE landuse SET new_geom = ST_Transform(geom, 4326);
   > ```

   \endgroup

### c) déterminer la projection d'une couche{#determiner-projection}

Découvrir quel est la projection d'une table.

   \begingroup \code
   
   > \underline{Déterminer le SRID d'un champ géographique :}
   >
   > ```sql
   > SELECT ST_SRID(geom) from landuse; -- Pour information
   > ```

   \endgroup

# II) Extraction de données depuis une image

Pour extraire des informations d'une image, on la découpe en \definition{mailles} (quadrillage de l'image en petits carrés).

Mais aussi fine qu'on fasse cette maille, elle peut encore porter à la fois :

   - une route 
   - un bâtiment 
   - une végétation ou culture

\bigbreak

__Ce sont autant de couches géographiques__ qu'il va falloir séparer pour chaque catégorie d'éléments.

Il faut donc \donnee{énumérer toutes les catégories d'éléments au sein d'une image} pour __déterminer le nombre et la nature des couches__ que l'on aura à produire.

\bigbreak

Un agrégat de mailles devient une \definition{région} : notre objet géographique.

   - une dernière opération, le \definition{géoréférencement} place l'image sur le globe, en lui attribuant des __coordonnées géographiques__ : \attention{sans, l'objet aurait une géométrie (la forme de l'agrégat des mailles), mais pas de localisation spatiale}.

\bigbreak

\underline{Remarque :}  
Une image devient un \definition{raster}. Issues de photographies aériennes ou images satellites, elles sont stockées en arborescence (_Quadtree_ ou _Octree_ pour la 3D), pour faciliter le choix de l'échelle et son affichage progressif durant les chargements longs.

![Le Quadtree](images/raster_tree_pyramide.jpg)

# III) L'espace géographique, le territoire et les objets

Pour modéliser géographiquement, il faut aussi considérer __les arrangements des objets entre-eux__.

Ils sont dans un \definition{espace géographique} fait de lieux et de relations entre eux, et ont cinq usages fondamentaux :

   - Appropriation
   - Exploitation ou mise en valeur
   - Habitation
   - Echange ou communication
   - Gestion

\bigbreak

Le \definition{territoire} est une portion de l'espace géographique, circonscrite par des limites politico-administratives, qu'on définit pour y étudier les activités humaines :

   - municipalité
   - département
   - canton
   - région
   - pays... 

\bigbreak

Les __objets__ sont une portion de cet espace. Vus en terme d'attributs et de localisation, forme, ou de relations entre-eux, ils sont rarement indépendants et forment un __système__.

   - Un objet __géographique__ a une localisation (position), des attributs, mais aussi un \definition{voisinage}
   - Un objet __géométrique__ est un point, ligne, vecteur : une forme géométrique
   - Un objet \definition{topologique} est vu en __noeuds__ et __arcs__ (polylines, polygones)
   - Ils sont aussi thématiques ou temporels : date de création, durée...

\bigbreak

Une unité d'observation est indivisible, et ses propriétés sont homogènes sur l'ensemble de l'unité.

L'espace géographique se décompose en trois ensembles :

   - un __géométrique__, avec une métrique de longueur et une métrique de surface.  
   - un d'__objets spatiaux__, ponctuels, linéaires ou zonaux, dont les métriques décrivent leur forme, arrangements et relations.  
   - et un rassemblant les __propriétés thématiques__ des objets, dont les métriques aux échelles de mesures nominales, ordinales ou cardinales, sont les opérateurs statistiques.

## 1) Transformer un phénomène spatial discret en objet géographique

Un phénomène spatial __discret__ a des limites et fait un objet géographique (point, ligne, vecteur).

\bigbreak

Les \definition{objets spatiaux} correspondent à une \avantage{portion d'espace clairement délimitée} et sont définis à priori. C'est l'analyste qui les choisit et les identifie.

   - \attention{ils peuvent être liés à plusieurs thématiques à la fois !} (une commune, par exemple : il y a tant à en dire)

   - Un objet spatial peut changer d'un niveau de zoom à l'autre : un élément zonal (vecteur) va apparaître comme un point à certaine échelle

      > Une ville (vecteur) peut devenir un point (son centroïde)

   - l'échelle de la source d'information est ce qui limite le détail d'un objet spatial

## 2) Transformer un phénomène spatial continu modèle discret, via un raster

Un phénomène spatial __continu__ (altitude, humidité, température...) est __pris en quelques points__, et représenté, par exemple, par des isolignes.

L'on fait d'un __phénomène continu__ un __modèle discret__.

### a) Transformation en mailles

La \definition{maille} est l'unité d'observation initiale, indivisible. Ses propriétés considérées homogènes sur son ensemble.
Elle aura \definition{une valeur discrète} : \attention{attention, ça ne veut pas dire entière !} mais fini, dénombrable : il y aura \underline{\textbf{un}} réel.

Les mailles faites, on fera de tout cela une image raster.

### b) Pour cela, on discrétise des valeurs continues

\underline{\textbf{En classes :}}

   On discrétise les valeurs continues en en faisant des classes.
   
   > Pour l'altitude, par exemple.
   
   Ces classes provoquent l'aparitition d'\definition{objets spatiaux}

   > Quand on transforme en classes une valeur continue, l'on passe d'une __échelle cardiale__ à __ordinale__.  

\bigbreak

\underline{\textbf{Par des moyennes, ou d'autres calculs :}}

Par une moyenne, écart-type ou autre indicateur synthétique, on calcule une valeur unique (discrète), nouvel attribut de l'objet.

Les valeurs continues (autour d'une commune, par exemple) sont ainsi résumées en une seule valeur, qui devient un de ses attributs.

### c) Face à une image, nous devons l'échantillonner pour la discrétiser

Un phénomène continu ne peut jamais être observé complètement : il est toujours échantillonné.  
Discrétisé, au moins par les pixels

Dans une image, des unités d'observation arbitraires sont découpées : ce sont des \definition{mailles} ou \definition{cellules}.

\avantage{Quand ces cellules portent un attribut de même valeur}, elles deviennent une \definition{région}, et \definition{ce sont ces régions qui deviennent les objets géographiques}, définis à postériori.

Les mailles, rassemblées en régions, font elles aussi des zones, lignes ou points. 
Une région est définie à la condition que les mailles portent la même thématique.
