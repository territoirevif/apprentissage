---
title: "a191. Grass, QGIS : Manipulations générales"  
subtitle: ""
author: Marc Le Bihan
---

Illustration en transformant par GRASS des polygones d'un Shapefile en raster.

# I) Création d'un secteur puis d'un jeu de cartes

## 1) Créer un secteur et lui donner une projection

1. Créer un secteur, 

   - lui donner un nom, 
   - et projection : ici, là `EPSG:32648` (WGS84)

   ![Créer un secteur puis un jeu de cartes](images/grass_qgis_ex01_choix_secteur_jeu_de_carte.jpg){width=60%}

\bigbreak

Une région est une zone géographique d'étude. Elle a :

   - une extension géographique (limites Nord, Sud, Est, Ouest)
   - taille/résolution de pixels (N-S et E-O : qui ont souvent les mêmes tailles).

## 2) Créer un jeu de cartes

   \danger{Ne jamais travailler dans "PERMANENT"}, qui contient les caractéristiques du SCR : toujours en créer un nouveau.

## 3) Démarrer et vérifier la projection

   - Appuyer sur démarrer

   - Vérifier la projection, en allant sur le menu  
   \menuTrois{Paramètres}{Projection de cartes}{Affiche la projection de cartes (g.proj-p)}  

   \code

   ```
   (Mon Sep 26 19:54:38 2022)                                                      
   g.proj -p                                                                       
   -PROJ_INFO-------------------------------------------------
   name       : WGS 84 / UTM zone 48N
   datum      : wgs84
   ellps      : wgs84
   proj       : utm
   zone       : 48
   no_defs    : defined
   towgs84    : 0.000,0.000,0.000
   -PROJ_EPSG-------------------------------------------------
   epsg       : 32648
   -PROJ_UNITS------------------------------------------------
   unit       : meter
   units      : meters
   meters     : 1
   (Mon Sep 26 19:54:38 2022) La commande s'est terminée (0 sec) 
   ```

   \endgroup 

# II) Opération sur les vecteurs et les rasters

## 1) Importer un Shapefile

Aller dans le menu :  
\menuTrois{Fichier}{Importer des données vectorielles}{Importation de vecteurs de format commun (v.in.ogr)}

   ![Importer un shapefile](images/grass_qgis_ex01_importer_shapefile.jpg)  
   \legende{et importer le fichier `LU2007.shp`}

## 2) Importer un Raster

### a) Définir une résolution de 30 mètres en 2D

\menuQuatre{Région}{Région calculée}{Définir la région}{Onglet : Résolution}

   > Placer $30$ (pour 30 mètres) dans le champ __Résolution 2D__.

ou

   \code

   > ```
   > g.proj -p
   > ```

   \endgroup

   ![Importer un shapefile](images/grass_region_resolution.jpg)

### b) Provoquer la conversion en Raster

\menuTrois{Fichier}{Conversion type de carte}{Vecteur vers raster}

ou

   \code

   > ```
   > v.to.rast input=LU2007@laos output=LU2007 use=attr attribute_column=ID3 label_column=LU3
   > ```

   \endgroup

