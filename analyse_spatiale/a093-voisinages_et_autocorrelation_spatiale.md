---
title: "a093. Les voisinages et l'autocorrélation spatiale" 
subtitle: ""

keywords:
- distribution spatiale
- arrangement
- autocorrélation spatiale
- analyse de Moran
- Local Indicators of Spatial Association
- LISA

author: Marc Le Bihan
---

La \definition{distribution spatiale} est aussi appelée \definition{arrangement}.  

l'\definition{autocorrélation spatiale} ou dépendance spatiale est étudiée par l'\definition{l'Analyse de Moran} et le \definition{Local Indicators of Spatial Association (LISA)}  

