---
title: "a094. Inférences, généralisations, corrélations"
subtitle: "existe t-il des relations qui ne sont pas dues au hasard ?"
author: Marc Le Bihan
keywords:
- inférence
- généralisation
- corrélation
- phénomène spatial
- modèle
- géostatistique
- mobilité
- transport
- accessilité de ressources
- relations fonctionnelles
- situations géométriques particulières
---

L'analyse spatiale décompose un phénomène spatial en ses éléments essentiels, pour former un modèle d'ensemble.

En comprenant et en généralisant ce qui se trouve sur une carte,  
   \demitab ou ce qui y apparaît ou disparaît,

l'on tente d'__\avantage{établir un modèle}__ expliquant :

   - les dispositions 
   - les relations
   - et les phénomènes qu'on y voit

\bigbreak

Grâce aux statistiques descriptives et inférentielles (explicative), la \definition{géostatistique} analyse des caractères spatiaux pour définir des indicateurs dédiés. De (par exemple) : 

   - de mobilité
   - transport
   - accessiblité de ressources...

\bigbreak

en réalisant des :

   - observant des \definition{corrélations} entre variables
   - définissant \definition{relations} fonctionnelles entre entités ou phénomènes
   - calculant des \definition{indices synthéthiques}, résumant une information

\bigbreak

à l'aide

   - d'__attributs thématiques__ : où sont les zones de forêts, quel est leur nombre ?
   - __dimension géographique__ : quelle est la zone 50 mètres autour d'une autoroute ?
   - __dimension spatiale__ : la réunion des deux. Quelles sont les zones inondables de plus de 1 ha ?

\bigbreak

\underline{C'est que sur une carte, une tendance globale est rarement due au hasard !}

$\rightarrow$ l'arrivée des points en 2D ($x$, $y$) élève au carré le nombre de situations possibles  
\demitab rendant la probabilité de hasard très faible.

Et les situations géométriques particulières sont repérées par l'oeil humain.

