---
title: "a061. Valeurs significatives et correction de taux"
subtitle: "mettre en évidence les valeurs qui dérogent à l'hypothèse nulle  \ncorriger les taux trop imprécis"
author: Marc Le Bihan
keywords:
- valeurs significatives
- correction de taux
- hypothèse nulle
- H0
- processus inobservés
- faiblesse d'un modèle
- modèle
- variable
- distribution théorique
- individus rejettant leur hypothèse nulle
- taux faibles avec des effectifs faibles
- estimateurs baysiens empiriques
- EBE
- lissage spatial
- autocorrélation spatiale
---

# I) Confronter une valeur à sa distribution théorique pour éclairer sur celles significatives

Présenter une valeur ou un taux ne suffira pas toujours à éclairer le lecteur.

Le confronter à sa distribution (test d'hypothèse) davantage :  
   \demitab est-il __significatif__, c'est à dire : déroge t-il à sa distribution théorique ?

\bigbreak
\avantage{\underline{Exemple :} On peut ainsi marquer sur une carte}
\avantage{les individus (ou unités) qui rejettent leur hypothèse nulle}.

\bigbreak

\gris{[voir p106. significativité d'une hypothèse globale et correction de Bonferroni (diviser le risque d'erreurs par le nombre de tests effectués)]}

\bigbreak

Par ailleurs, quand l'estimation est à la fois faible et a une forte variation aléatoire,  
   \demitab commme pour les évènements rares,

l'objectif d'un calcul de taux n'est plus d'estimer avec précision une valeur réelle du risque, 
   \demitab mais de déterminer si cette valeur est réellement différente du risque global.

   > $\rightarrow$ en donnant un résultat sous la forme d'une classification du risque.

# II) Les Estimateurs Baysiens Empiriques (EBE)

\underline{Le problème de la significativité des taux, quand ils sont très faibles ou basés sur des effectifs faibles :}

En traitant nombreuses zones, l'on calcule les taux sur des effectifs différents 

   - parfois sur des effectifs grands,
   - \rouge{mais parfois sur un effectif faible, et la précision s'en ressent :}  
     \rouge{$\rightarrow$ un taux très faible appliqué sur un effectif faible est très imprécis}.

\bigbreak

L'idée est alors de prendre de l'information à l'extérieur d'une unité pour augmenter la précision de celle-ci : 

on fait l'hypothèse que la zone étudiée possède les mêmes caractéristiques qu'une zone plus large.

\bigbreak

Les \definition{Estimateurs Baysiens Empiriques (EBE)} sont utilisés en cartographie et dans l'analyse spatiale des taux (pour les indices d'autocorrélation), et proposent une manière de les ajuster.

## 1) Estimer un taux moyen global sur toute la population

On estime un taux moyen global et sa variance sur l'ensemble de la population,  
   \demitab toutes unités géographiques confondues.

\bigbreak

Ce qui implique de choisir une distribution pour cela :  
\demitab $\rightarrow$ celle qui nous semble la plus probable,  
\demitab en choissant ses paramètres comme on peut... !

## 2) Ajuster par une combinaison linéraire entre la valeur observée et la valeur à priori

Puis l'on fait un ajustement par une __combinaison linéaire__   
   \demitab entre la \definition{valeur observée}  
   \demitab et la \avantage{valeur à priori}

\bigbreak
   
   - Les __coefficients__ sont fonction de :
   
      - la taille relative de la population 
      - la variance de la \definition{valeur à priori} de chaque unité observation

\bigbreak
   
   - plus l'effectif de l'unité est faible (la valeur du \avantage{taux} éloigné de celui \definition{global}),  
     plus l'ajustement est important.

   > \bleu{$i$} l'individu = la zone observée (unité d'observation),  
   > \   
   > \demitab \bleu{$\mathbf{n_{i}}$} \demitab la taille de sa population,  
   > \demitab \bleu{$\mathbf{y_{i}}$} \demitab l'effectif observé,  
   > $\rightarrow$ \begingroup \large $\mauve{r_{i}} = \frac{y_{i}}{n_{i}}$ \endgroup le taux observé de chaque unité d'observation
   > 

\bigbreak

Et dans notre calcul :

   > \rouge{$\lambda_{i}$} \demitab est le taux inconnu dans l'unité d'observation $i$ qu'on veut écouvrir.
   >
   > \rouge{$\hat{\lambda_{i}}$} \demitab est sa \definition{meilleure estimation baysienne}

\bigbreak
 
   \begingroup \cadreGris 

   > __On fait l'hypothèse que chaque unité/individu est régi par une distribution de probabilité__  
   > \   
   > \demitab de moyenne \mauve{$\mu_{i}$}  
   > \demitab et de variance \mauve{$\phi_{i}$}  
   > \   
   > \demitab d'où l'on tirerait le coefficient :
   >
   > \tab \begingroup \Large $\mauve{\omega_{i}} = \frac{\phi_{i}}{\phi_{i} + \frac{\mu_{i}}{n_{i}}}$ \endgroup \   
   > \   
   > \demitab pour calculer :  
   > \tab \begingroup \large $\rouge{\hat{\lambda_{i}}} = \omega_{i}r_{i} + (1 - \omega_{i})\mu_{i}$ \endgroup \   
   > \   
   > \tab $\mauve{\omega_{i}}$ et $\mauve{(1 - \omega_{i})}$ dépendant de la population et de la variance de la distribution à priori.  

   \endgroup

\attention{Mais on ne peut pas calculer directement ces valeurs !}  
\demitab $\rightarrow$ parce que la moyenne et la variance de chaque unité, on ne la connaît pas.

\bigbreak

   \begingroup \cadreGris

   > __Ainsi, si notre hypothèse c'est : notre distribution est une distribution Gamma__  
   > (de paramètres $a$ et $b$, pour une moyenne \begingroup \large $\mu = \frac{a}{b}$ \endgroup et une variance \begingroup \large $\psi = \frac{a}{b^2}$\endgroup)  
   > \   
   > on peut \avantage{estimer} :
   > $$ \rouge{\hat{\mu}} = \frac{\sum{y_{i}}}{\sum{n_{i}}} $$  
   > $$ \rouge{\hat{\psi}} = \frac{\sum(r_{i} - \hat{\mu})^2}{\sum{n_{i}}} - \frac{\hat{\mu}}{\overline{n}} $$
   > \   
   > et calculer l'estimateur baysien  
   > \demitab qui ajustera/corrigera le taux \bleu{$r_{i}$ observé}  
   > \demitab (potentiellement incorrect des unités d'observations/individus \bleu{$i$}) avec :
   > \begingroup \large $$ \rouge{\hat{\lambda}} = \hat{\mu} + \frac{\hat{\psi}(r_{i} - \hat{\mu})}{\hat{\psi} + \frac{\hat{\mu}}{n_{i}}} $$ \endgroup

   \endgroup

## 3) Quand les valeurs ne permettent même pas d'estimation : recherche de la significativité

Quand la qualité de l'estimation est si frêle  
(par exemple, pour les évènements rares)  
on ne peut même plus chercher à rectifier un taux...

$\rightarrow$ On va juste détecter s'il est réellement différent de la valeur globale :  
\avantage{classer un risque} plutôt qu'une valeur réelle.

## 4) Autres sélections d'unités d'observations/individus possibles

   - zones voisines (distance ou contigüité. cf. ci-dessous)
   - groupes (urbain - rural, ou autre critères géographiques)

      - en calculant des paramètres par classe,
      - et en considérant uniquement les unités de cette classe

# III) Le lissage spatial

avec une hypothèse d'autocorrélation spatiale
(chapitre 5 du livre, à venir)
en associant cette technique à l'estimateur baysien, l'on peut rectifier des taux sur des voisinages seulement,
et minimiser l'autocorrélation spatiale.

# IV) L'interpolation par noyau

où l'on attrape des unités dans un rayon variable

