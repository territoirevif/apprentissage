---
title: "a500. Les protocoles des services géographiques"
subtitle: "et les avantages de ceux tuilés"
category: analyse spatiale
abstract: WFS, WFS-T, WMS, WMTS, WCS, CSW, WPS, OWS 

keywords:
- Carte
- WFS
- Web Feature Service
- WFS-T
- WMS
- Web Map Service
- WMTS
- WCS
- Web Coverage Service
- CSW
- Catalogue Service for the Web
- tile
- tuile
- JPG
- JPEG
- HTTP
- HTTP/2
- Niveau de zoom
- PNG
- WPS
- Web Processing Service
- OWS
- Open Geospatial Consortium Web Service
- OGC Web Service
- Modèle Numérique de Terrain
- MNT
- orthoimage
- entité

author: Marc Le Bihan
---

---

## 1) Web Map Service (WMS)

### a) Classique (WMS)

\demitab WMS renvoie une portion de carte correspondant à \definition{une fenêtre englobante},  
sous forme d'__image raster__ : souvent \fonction{PNG} ou \fonction{JPEG}.

  - Il est rarement possible de changer la symbologie que le fournisseur propose.
  - Il transporte une masse de données longue et volumineuse à récupérer.

### b) Tuilé (Tile, WMTS)

`WMTS` accélère la récupération de carte, en distribuant des \definition{tuiles},

   > de $\mauve{256 \times 256}$ ou $\mauve{512 \times 512}$ pixels, souvent, et plus une portion de carte.

On définit à ces tuiles des niveaux de zooms, qui vont de :

   > $\mathbf{0}$ : terre entière (et même plus vaste),  
   > à $\mathbf{20}$ (en général) : plusieurs milliers de tuiles pour une rue.

\bigbreak

__Chaque niveau de zoom divise les tuiles par quatre, par rapport au précédent__.

L'intérêt du chargement par tuiles est que :

   1. Elles peuvent être affichées au fur-et-à-mesure qu'elles arrivent, sans les attendre toutes.
   2. On peut redemander celles qui ne seraient pas arrivées, car chaque tuile a sa propre requête

      - HTTP permet jusqu'à six requêtes simultanées \gris{(d'où ça vient, ça ?)}
      - HTTP/2 en permet davantage

## 2) Web Feature Service (WFS)

### a) Classique (WFS)

\demitab `WFS` permet d'obtenir des entités stokées sous forme de points, lignes, vecteurs.

  - L'on peut faire varier la symbologie, côté client.
  - Mais il renvoie des résultats volumineux : on lui préfère les tuiles vectorielles.

### b) Transactionnel (WFS-T)

\demitab `WFS-T` permet de créer, modifier, supprimer des entités, sous conditions.


## 3) Web Coverage Service (WCS)

   - Modèle numérique de terrain
   - orthoimages.

## 4) Catalogue Service for the Web (CSW)

\demitab Catalogue de données

## 5) Web Processing Service (WPS)

## 6) Open Geospatial Consortium Web Service (OWS)

\demitab Peut produire du WFS, WMS, WMTS...

