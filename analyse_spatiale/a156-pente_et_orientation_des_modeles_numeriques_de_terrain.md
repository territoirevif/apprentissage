---
title: "a156. Pente et orientation des Modèles Numériques de Terrain"
subtitle: "modèle de terrain, d'élévation et hydrologique"
author: Marc Le Bihan
keywords:
- pente
- modèle numérique de terrain (MNT)
- orientation
- dénivellation
- axe
- TIN
- exposition
- aspect
- azimut
- altitude
- ligne de plus grande pente
- GRID
---

## 1) La pente

La \definition{pente} est l'angle d'inclinaison d'une surface par rapport au plan horizontal.  
Elle s'exprime en unités d'angles.

la \definition{pente et son orientation} se calculent  
   \demitab à partir de la dénivellation par mètre de l'axe des $x$ et de celle des $y$.

\bigbreak

La pente \danger{$\delta$} d'un point __M__ est l'angle entre

   - la direction de la plus grande pente
   - l'horizontale du plan passant par le point __M__.

\bigbreak

   > \definition{$b$} : la dénivélation par mètre sur l'axe des $x$  
   > \definition{$c$} : la dénivélation par mètre sur l'axe des $y$  
   > \danger{$\delta$} : la pente que l'on veut obtenir
\begingroup \large $$ \tan(\rouge{\delta}) = \sqrt{b^2 + c^2} = \frac{\text{dénivelé}}{\text{avancement horizontal}} $$ \endgroup

\avantage{\underline{Attention :} pour comprendre cette formule}  
\avantage{il faut l'imaginer dans un repère $(x,y,z)$ en 3D}

   > car autant :  
   > \demitab le rapport dénivelé / avancement horizontal de la première formule  
   > \demitab peut s'observer sur un repère $(x,y)$ : c'est une coupe
   > 
   > autant la seconde réclame  
   > \demitab qu'on puisse voir la progression des axes dans les deux directions $x$ et $y$,  
   > \demitab en plus du dénivelé.
   >
   > On peut le comprendre sur un _TIN_.

## 2) L'orientation

- L'\definition{orientation} (ou exposition ou aspect) indique la direction horizontale de la plus grande pente.  
Exprimée :

   - soit en azimut par rapport à l'est (origine du cercle trigonométrique)  
   - soit en unité géographique par rapport au Nord  
     et est alors codée selon les quatre cadrants cardinaux : N, E, S, W, ou avec plus de précision : N-E, S-E, S-O, N-O.

\bigbreak

L'orientation \danger{$\omega$} est l'angle de la ligne de plus grande pente par rapport à l'axe des $x$.
\begingroup \large $$ \tan{\rouge{\omega}} = \frac{b}{c} $$ \endgroup

### a) Calcul de la ligne de plus grande pente et de l'orientation depuis un GRID

   > ![Calcul de la ligne de plus grande pente et orientation d'après un GRID](images/calcul_pente_orientation_grid.jpg)

à partir de : 

   > \definition{$z_{i}$} : l'altitude d'un point _i_  
   > \definition{$d_{x}$} : dénivelé selon $X$  
   > \definition{$d_{y}$} : dénivelé selon $Y$

\bigbreak

l'on calcule trois altitudes :

   > \rouge{$a$} : altitude au centre de la matrice
   > 
   >   > \begingroup \Large \rouge{$a$} = $\frac{z_{1} + z_{2} + z_{3} + z_{4}}{4}$ \endgroup \tab \demitab $a = 7.25$\ mètres sur l'illustration
   > \   
   >
   > \rouge{$b$} : dénivélation en $X$, avec :
   >
   >   > \bleu{$d_{x}$} : la longueur d'une case du GRID ($d_{x} = 10$\ mètres sur l'illustration)
   >   >
   >   > \begingroup \Large \rouge{$b$} = $\frac{(z_{2} - z_{1}) + (z_{4} - z_{3})}{2d_{x}}$ \endgroup \tab $b = -0.25$\ mètres
   > \   
   >
   > \rouge{$c$} : dénivélation en $Y$, avec :
   >
   >   > \bleu{$d_{y}$} : la hauteur d'une case du GRID ($d_{y} = 10$\ mètres)
   >   >
   >   > \begingroup \Large \rouge{$c$} = $\frac{(z_{3} + z_{4}) - (z_{1} + z_{2})}{2d_{y}}$ \endgroup \tab $c = 0.15$\ mètres

\bigbreak

\underline{Remarque : }

   > Pour le calcul de $b$ et $c$, la division par $2$ vient que l'on calcule d'après deux informations (deux lignes de GRID ou deux colonnes de GRID).
   > 
   > et la division par $d_{x}$ ou $d_{y}$ fait revenir à une mesure par rapport au mètre.

\bigbreak

$\rouge{\delta}$ : la pente, vaut alors :  
$$ \rouge{\delta} = \frac{\text{arctan}(\sqrt{b^2 + c^2}) \times 180}{\pi} $$

   > \emph{\fonction{arctan} c'est \fonction{$tan^{-1}$}, l'inverse de la tangente}  
   > \emph{et la tangente \fonction{tan}, c'est $\text{tan }\hat{A} = \frac{BC}{AC}$ ou $\frac{\text{opposé}}{\text{adjacent}}$ et $\text{tan }{\theta} = \frac{\text{sin }\theta}{\text{cos }\theta}$}

   > $\rouge{\delta} = 16.25$°
   >
   > ou, en pourcentage : $100 \times{tan(\delta)} = 29\%$

\bigbreak

Et l'orientation :
$$ \rouge{\omega} = \frac{\text{arctan2}(c,b) \times 180}{\pi} $$

   > \emph{\fonction{arctan2} ou \fonction{atan2} est la variante à deux arguments d'arc tangente}  
   > et partant de $x$, $y$, \methode{atan2}{(y,x)} c'est à dire \methode{atan}{(c,b)} dans notre cas, vaut :  
   > $$ \omega = \text{atan2}(y,x) = 2 \text{ arctan }\frac{y}{\sqrt{x^2 + y^2} + x} $$
   > 
   > $\resolu{\boldmath{\omega}} = 149$°

\bigbreak

\definition{Cet angle est mesuré par rapport à l'axe des $x$ dans le sens anti-horaire},  
et il désigne \attention{le point de coordonnées $(c,b)$ opposé à la pente}.

La pente est orientée \avantage{par rapport à l'est} de : $180^\circ - 149^\circ = \mathbf{31^\circ}$.

![la pente et son orientation, d'après un grid](images/pente_et_orientation_grid.jpg)

### b) Depuis un Triangulated Irregular Network (TIN)

Le calcul se fait à l'aide de trigonométrie.

La ligne de plus grande pente est calculée par l'angle d'inclinaison entre  
   \demitab la surface du triangle 
   \demitab et le plan horizontal.
 
   > (cela peut poser des problèmes dans des cas exceptionnels de pentes négatives, dont il ne peut pas rendre compte : parois de grottes ou de falaises).

\bigbreak

Leur orientation se déduit de l'angle entre :

   - la projection sur le plan horizontal de la normale à la surface du triangle,
   - et la direction nord.

![la pente et son orientation, d'après un triangle de TIN](images/pente_et_orientation_tin.jpg)

## 3) L'ombrage

L'\definition{ombrage} simule l'intensité de l'éclairement. Il est utile pour les études paysagères : 

   - zones vallonées 
   - répartition ddes espèces végétales.
   - \definition{adret} : ensolleillé, \definition{ubac} : plus ombragé.

### a) Calcul de l'ombrage (cas général)

   > \bleu{$\delta$} : angle de la pente  
   > \bleu{$\omega$} : angle de l'orientation  
   >  
   > \bleu{elev} : élévation solaire  
   > $\implies$ \mauve{$\text{zénith} = 90^{\circ} - \text{elev}$} : angle zénithal. __à convertir en radians__  
   >  
   > \bleu{azimut} : angle azimutal choisi.

\bigbreak

Les valeurs sont multipliées par $255.0$ :

   \demitab pour valoir $0$ dans une zone totalement à l'ombre,  
   \demitab et de $1$ à $255$ sinon
$$ \rouge{\text{ombrage}} = 255 \times [\cos(\text{zénith}).\cos(\delta) + \sin(\text{zénith}).\sin(\delta).\cos(\text{azimut} - \omega)] $$

avec les valeurs précédentes :

   - $\delta = 16.25^{\circ}$
   - $\omega = 149^{\circ}$
   - un angle d'élévation solaire à $45^{\circ}$ menant à $\text{zénith} = 45^{\circ}$
   - et un azimut choisi à $135^{\circ}$

$\rightarrow$ l'ombrage vaut $222$

### b) Calcul de l'ombrage depuis un TIN

On se base sur :

   - l'angle entre la direction du soleil et la normale à la surface du triangle
   - l'angle azimutal
   - la pente
   - l'orientation

