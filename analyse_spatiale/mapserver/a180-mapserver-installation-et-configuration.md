---
title: "a180. Installation et configuration de MapServer"
subtitle: ""
author: Marc Le Bihan
---

MapServer ne s'accordera qu'avec Apache2, qui peut exécuter les CGI.

   \code

   > ```bash
   > # Arrêter et désactiver nginx qui est concurrent à Apache2 et lui mangerait le port 80
   > sudo systemctl stop nginx.service 
   > sudo systemctl disable nginx.service 
   > 
   > # Installation d'Apache 2 (s'il manque)
   > # de mapserver, son module CGI, et liaison avec Apache
   > sudo apt install -y apache2 libapache2-mod-fcgid cgi-mapserver mapserver-bin
   > a2enmod authnz_fcgi
   > sudo a2enmod authnz_fcgi
   > sudo a2enmod cgi
   > 
   > # Création d'un lien vers l'exécutable mapserv dans /var/www/cgi-bin
   > # (recommandé par le livre "Géomatique WebMapping en Opensource)
   > cd /var/www
   > sudo mkdir cgi-bin
   > sudo ln -s /usr/lib/cgi-bin/mapserv mapserv
   > 
   > # Redémarrer Apache 2, et demander son démarrage automatique, par la suite
   > sudo systemctl restart apache2
   > sudo systemctl enable apache2
   > ```

   \endgroup

\bigbreak

En allant sur un navigateur et en tapant l'URL :  
[http://localhost/cgi-bin/mapserv?](http://localhost/cgi-bin/mapserv?)

On reçoit le message 'No query information to decode. QUERY\_STRING is set, but empty.", signe que l'installation a réussi (il reçoit une requête, vide).

