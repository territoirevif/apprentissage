---
title: "a181. La structure d'un mapfile"  
subtitle: ""
author: Marc Le Bihan
---

# I) La structure d'un mapfile

\tab Le \definition{mapfile} contient des informations pour la confection d'une carte. Il est délimité par les instructions \fonction{MAP}...\fonction{END} :

   \code

   > ```
   > MAP
   > NAME 'Le nom du mapfile'
   >    LAYER   # La définition d'une couche géographique
   >       NAME 'couche1'  # Le nom de la couche
   >       
   >       CLASS      # Une entité qu'on peut trouver dans la couche
   >           NAME 'routes'
   > 
   >           STYLE  # Son style, pour la sémiologie
   >           END
   >       END
   >    END
   > 
   >    LAYER
   >       NAME 'couche2'
   >       
   >       CLASS
   >           NAME 'forêts'
   > 
   >           STYLE
   >           END
   >       END
   >    END
   > 
   >    LEGEND    # Légende de la carte
   >    END
   > 
   >    SCALEBAR  # Echelle de la carte
   >    END  
   > END
   > ```

   \endgroup

\bigbreak

## 1) Propriétés de l'image de sortie (section MAP)

C'est aussi dans le MapFile, dans sa section \fonction{MAP}, que l'on donne __les propriétés de l'image de sortie__ :

   - son format, directive \fonction{IMAGETYPE} : `JPEG`, `PNG`, `TIF`...
   - couleur d'arrière plan, par \methode{IMAGECOLOR}{(R, G, B)}
   - taille en pixels, avec \fonction{SIZE}.
   - l'emprise de la zone cartographiée, avec \fonction{EXTENT} (xmin, ymin, xmax, ymax)

## 2) Déclaration de la projection d'une couche (section MAP)

Egalement, il faudra déclarer la projection d'une couche géographique donnée par \fonction{PROJECTION} : MapServer accepte que chacune ait la sienne, et peut les reprojeter à la volée.

   \code

   > ```
   > MAP
   >      IMAGETYPE	PNG
   >      EXTENT	321660 4812100 332400 4818600 # de (3.32166, 48.121) à (3.324, 48.186).
   >      SIZE		1000 1000
   >      IMAGECOLOR	255 255 255 # Blanc
   >
   >      PROJECTION
   >          "init=EPSG:32631" # WGS 84 / UTM zone 31N
   > ``` 

   \endgroup

# II) Création d'un mapfile

\tab Ici, nous travaillons sous serveur Apache. Il faudra créer un fichier `.map`, par exemple : `routes.map` (pour y placer une couche de routes), dans \fonction{/var/www/html} (ou `c:\ms4w\Apache\htdocs` sous Windows).

