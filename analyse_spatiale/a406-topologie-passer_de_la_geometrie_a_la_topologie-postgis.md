---
title: "a406. Passer de la géométrie à la topologie avec PostGIS"
subtitle: ""
category: analyse_spatiale
author: Marc Le Bihan
keywords:
- géométrie
- topologie
- PostGIS
- noeud
- arête
- face
- polygone
- polyline
- centroïde
- ligne
- connexion de lignes
- création de noeuds aux intersections
- seuil de tolérance
- ajouter une couche dans PostGIS
- index spatial
- clef primaire
- shp2pgsql
---

## 1) Les transformations dues au passage d'une géométrie à une topologie

Dans une topologie, il y a :

   - des noeuds (les points)
   - des arêtes (lignes orientées)
   - les faces (constituées d'arêtes).

\bigbreak

En passant de la géométrie à la topologie :

Un polygone devient un __polyline__

   - s'il est adjaçent à un autre polygone, leurs arcs communs ne sont pas dupliqués
   - il se dote d'un \definition{centroïde}

\bigbreak

Il faudra s'assurer que les __lignes__ se connectent entre-elles par des __noeuds__.  
Redresser la topologie, s'il le faut :

   - en rallongeant des lignes pour qu'elles se connectent, 
   - en en raccourcissant d'autres,
   - en créant des noeuds à des intersections.

\bigbreak

les vecteurs en seront corrigés, du même coup.

\bigbreak
 
Il faut préciser un \avantage{seuil de tolérance} pour permettre l'opération. 

## 2) Ajouter une couche dans PostGIS

### a) ajout de couche depuis un shapefile (QGIS)

### b) ajout de couche depuis PostGIS

   1. Se connecter à Postgis depuis QGIS (user/password postgres)

      ![Connexion à Postgis depuis QGIS](images/connexion_postgis_webmapping.png){width=70%}

      (_Si QGis refuse de se connecter à Postgresql, vérifier que ce n'est pas un problème de master password qu'il faut saisir sous PgAdmin 4_)

\bigbreak

   2. Importer la couche depuis PostGIS

      - \danger{Ne pas oublier l'index spatial !}
      - \danger{Ne pas oublier la clef primaire}  
      - Noter le changement de SCR cible (pour être dans le même référentiel que les données _Sentinel_)

      ![Importer une couche QGIS dans Postgis](images/qgis_importer_couche_dans_table.png){width=60%}

      (C'est la commande \fonction{shp2pgsql} qui est utilisée en tâche de fond).

