---
title: "a062. Interpolations et surfaces de tendance"
subtitle: ""

keywords:
- interpolation
- surface de tendance
- estimation
- lissage
- interpolation par noyau
- Kernel Estimation
- noyau
- fonction de distance
- objets voisins
- poids
- densité d'objets
- densité d'évènements
- Kernel Density Estimation
- NKDE
- interpolation par potentiel
- interpolation barycentrique
- Voronoï
- interpolation par surface de Bézier
- polynomiale par morceaux
- spline
- interpolation géostatistique
- Krigeage

author: Marc Le Bihan
---

\mauve{Une} \donnee{interpolation} \mauve{:}

   - \mauve{en mathématique, c'est :}

      - \mauve{construire une courbe à partir des données d'un nombre fini de points}
      - \mauve{ou une fonction à partir de données d'un nombre fini de valeurs}

   - \mauve{en imagerie numérique, c'est redimensionner l'image en diminuant ou en augmentant la matrice de l'image initiale, et donc le nombre de pixels}.

---

Parfois, ce ne sont pas des objets que l'on veut représenter par des symboles ou des applats de couleur,  
mais une \definition{tendance globale} :

  - L'on cherche à représenter une variable numérique
  - ou des effectifs (si la variable est booléenne)

par une surface continue obtenue :

  - par interpolation
  - estimation
  - ou lissage

sur tout l'espace étudié.

\bigbreak

Il y a alors une représentation ("_une couleur_") même là, où originellement, il n'y a pas de points/individus.

\attention{De fait, cela a ses limites...}

# I) Interpolation et représentation continue

On va interpoler, estimer ou lisser, d'après une grille de points uniformément répartis dans l'espace étudié, en fonction : 

  - de la valeur
  - de la présence ou absence de l'objet

et la finesse va dépendre du __pas de la grille__.

![Tendance globale par interpolation](images/tendance-globale-par-interpolation.jpg)  
\begingroup \small _Carte du Gabon.  
Les symboles proportionnels ne seraient pas aussi parlants ces surfaces de tendance, calculées par interpolation,  
mais qui, elles, placent des valeurs dans des esapces forestiers où il n'y a pas eu d'enquêtes_ \endgroup

## 1) Pour les valeurs numériques : interpolation par noyau (Kernel Estimation)

Lorsque la valeur est numérique, l'\definition{interpolation par noyau (Kernel Estimation)} effectue:

   > En chaque point de la grille

   >  > pour tous les objets situés à une distance $\mauve{d} \leq \bleu{d_{0}}$
   >  > 
   >  >  > la __moyenne__ pondérée(\{valeurs voisines $\to$  
   >  >  > \demitab filtrées(\mauve{fonction de distance} (appelée \definition{noyau}))\})

\bigbreak

où la fonction de distance (__noyau__) est ici soit :

   - linéaire : \begingroup \Large $\mauve{\frac{d_0 - d}{d_0}}$ \endgroup
   - quadratique : \begingroup \Large $\mauve{(\frac{d_0 - d}{d_0})^2}$ \endgroup
   - gaussienne : \begingroup \Large $\mauve{e^{\frac{-1}{2}(\frac{d}{d_0})^2}}$ \endgroup

\bigbreak

$\rightarrow$ La manipulation revient à "étaler" la valeur d'un point $\bleu{P}$ sur ses objets voisins.

\bigbreak

   \begingroup \cadreAvantage

   > En épidémologie, l'interpolation par noyau est très utilisée, avec les surfaces quadratiques,  
   > pour s'affranchir d'un découpage géographique particulier lors d'une agrégation.   
   > \   
   > En utilisant un disque de rayon $\bleu{d_0}$ autour de chaque point de l'espace,  
   > on enrichit le processus d'agrégation en affectant à chaque objet un __poids__  
   > en fonction de sa distance au point à estimer.

   \endgroup

## 2) Pour les valeurs qualitatives OU densité d'objets OU d'évènements (Kernel Density Estimation)

Lorsque la valeur est:

   - qualitative 
   - une densité d'objets
   - ou d'évènements

\bigbreak

   > En chaque point de la grille 

   >  > pour tous les objets situés à une distance $\mauve{d} \leq \bleu{d_{0}}$
   >  > 
   >  >  > la __somme des effectifs__ pondérée (\{valeurs voisines $\to$  
   >  >  > \ \ \ \ filtrées(\mauve{fonction de distance} (appelée \definition{noyau}))\})

\bigbreak

Choisir pour fonction de distance le noyau gaussien, \begingroup \large \mauve{$e^{\frac{-1}{2}(\frac{d}{d_0})^2}$}, \endgroup permet de s'assurer que  
\demitab la contribution totale d'un objet est égale à $1$ dans chaque direction

le paramètre $\bleu{d_{0}}$ influe sur la vitesse de décroissance, c'est à dire :  
\demitab la contribution de l'objet en foncton de la distance.

\bigbreak

Cette technique permet aussi le calcul de taux en chaque point

   > en divisant les effectifs pondérés du phénomène  
   > par les effectifs pondérés de la populaton totale

En utilisant le même noyau.

Il est possible ensuite d'ajuster ces taux avec des estimateurs baysiens, si nécessaire.

### Possibilité supplémentaire, pour l'estimation, si le support est linéaire :

Lorsque le support est linéaire (réseau routier, par exemple),

   > il est possible de prendre en compte pour l'estimation la longueur du support dans le disque de recherche des évènements (NKDE).

# II) Autres méthodes d'interpolation

## 1) Interpolation par potentiel

Calculer en chaque point de la grille, 

   > la moyenne des valeurs des objets pondérées par une fonction de l'inverse  
   > de la distance à des objets au point de la grille

## 2) Interpolation barycentrique

Calculer en chaque point de la grille, la moyenne

   > des valeurs des objets voisins - au sens de [Voronoi](a038-topologie_les_indicateurs_arrangement_spatial_par_région-objets-zonaux.pdf#voronoi) -  
   > pondérées par l'inverse de la distance de l'objet au point de la grille

## 3) Interpolation par la surface de Bézier, polynomiale par morceaux, les Spline

Les coefficients des polynomes étant calculés à partir de la valeur des objets

## 4) Interpolation géostatistique

\definition{Krigeage} : méthode d'interpolation linéaire, qui garantit le minimum de variance.

