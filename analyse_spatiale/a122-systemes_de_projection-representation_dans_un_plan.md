---
title: "a122. Système de projection"
subtitle: "Représentation dans un plan"
author: Marc Le Bihan
keywords:
- système de projection
- orientation
- projection conforme
- surface
- projection équivalente
- distance
- projection équidistante
- repère cartographique
- qualité d'une projection
- indicatrice de Tissot
- projection polaire
- projection équatoriale
- projection oblique
- projection planaire
- projection azimutale
- projection cyclindrique
- projection conique
- universal transverse mercator (UTM)
- Lambert
- Lambert93
- WGS84
- Pseudo-Mercator
- Spherical Mercator
- RGF93
- Lambert coniques conforme
- Nouvelle Triangulation de France (NTF)
---

## 1) Les propriétés des projections 

Une projection doit conserver :
 
   - L'orientation (ou direction) : respectée par les projections \definition{conformes} : ce que sont les projections topographiques, qui respectent les angles.   
   - La surface, respectée par les projections \definition{équivalentes}
   - Les distances, respectées par les projections \definition{équidistantes}
   - Les mesures sont en mètres.
   - Repère cartographique : abscisse, ordonnée, altitude $(x, y, z)$

\bigbreak

\avantage{L'indicatrice de Tissot} (points placés sur un globe) aide à déterminer la qualité d'une projection.

## 2) Les types de projections

Sous PostGIS, \avantage{le type \definition{Geometry} est adapté aux surfaces planes, c'est à dire projetées.}

### a) Projection planaire ou azimutale
   
Polaire, équatoriale ou oblique.
   
### b) Projection cylindrique
   
\avantage{Universal Transverse Mercator (UTM)} est faite 3° d'angle autour d'un méridien donné.  
Il y a 60 fuseaux (d'Ouest en Est), le méridien de Greenwitch séparant les fuseaux 30 et 31.

### c) Projection conique

L'une des plus connues est celle de \avantage {Lambert} pour la France

   - 3 pour la métropole, 
   - 1 pour la Corse.

## 3) Les systèmes de référence de coordonnées

Ils sont nombreux, et surtout : ils ont évolué dans le temps...  
Ce sont les héritiers de la navigation maritime et aérienne.

Un objet pourra avoir pour coordonnées : 

   - $45.96^{\circ}$ de latitude et $5.82^{\circ}$ de longitude en `WGS84`,
   - et $x = 918178.491$ mètres et $y = 6544063.607$ mètres en `Lambert93`.

### a) WGS84

La terre étant un globe, dès le début de la navigation, il a été fait des mesures d'angle.  
Le WGS84 est celle qui représente sans déformation le globe terrestre. Il est souvent assimilé à "_GPS_".

Ses unités sont __sexadécimales__, DMS : degrés, minutes, secondes.

Son code `EPSG` est le __EPSG:4326__.

### WGS 84 / Pseudo-Mercator / Spherical Mercator

Popularisé par _Google Maps_, _OpenStreetMap_, _Bing_, _ArcGIS_, _ESRI_,  
il convient bien aux sites web.

Son code `EPSG` est le __EPSG:3857__.

### b) RGF93 et Lambert93

Le `RGF93` a été créé pour rendre compatible avec le système GPS l'ancienne _Nouvelle Triangulation de France_, qui datait du $19^{\text{ème}}$. Il est aussi exprimé en unités d'angles.

Le `Lambert93` est centré sur le parallèle $46.5^{\circ} N$, qui est bien, mais insuffisant quand on désire une grande pécision :

   > $\to$ Il découpe la France \underline{Métropolitaine} en 9 projections `Lambert` __Coniques Conformes (CC 9)__, basés sur plusieurs parallèles : neuf zones qui se chevauchent.

