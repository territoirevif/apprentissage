---
title: "060. Cartographie"
subtitle: "Les règles pour présenter une carte lisible"

keywords:
- cartographie
- carte
- tendance centrale
- aggrégat
- contraste
- lisibilité
- sémilogie graphique
- cercles proportionnels
- hiérarchie
- similitudes
- titre
- sous-titre
- copyright
- échelle
- orientation
- nord géographique
- nord magnétique
- boussole
- carte marine
- localisation
- système de coordonnées
- variable visuelle
- forme
- orientation
- grain
- couleur
- taille
- aplats de couleur
- ratio
- densité
- parts
- typologie
- données bilocalisées
- flux
- oursins
- flèches proportionnelles
- résultantes vectorielles
- symboles
- dégradés de couleurs
- représentation spatio-temporelle
- représentation 3D
- carte chloroplète
- taux
- camemberts
- déplacements
- matrice origine - destination
- légende
- discrétisation d'attribut qualitatif en quantitatif
- découpage en classes
- classes d'amplitude égales
- classes par quantiles, d'effectifs égaux
- méthode de Jenks
- méthode k-means
- variance
- effet de seuil
- nombre de classes
- règle de Sturges

author: Marc Le Bihan
---

La cartographie existe pour :

   - se repérer 
   - détecter des __éléments importants__ d'une __carte__ ou d'une __image__. 

Repérer:

   - des tendances globales: gradients, direction, centralité,

   - situations locales particulières (aggrégats, constrastes, situation "anormale" par rapport à une situation "normale"...).

Des points qui se regroupent sur une carte aux alentours des mêmes $(x, y)$ sont rarement le hasard. Il faut deux dimensions qui "s'accordent" en même temps.

# I) Lisibilité, sémiologie graphique

Le but d'une carte est de mettre en évidence, de manière visuelle, le contenu de variables qui peuvent être :

   - des quantités, souvent représentées par des cercles proportionnels
   - une hiérarchie: une série ordonnée de valeurs relatives, comme des densités de population
   - des différences entre entités représentées. Exemple: entre l’industrie et le tourisme
   - des similitudes, en regroupant dans un seul ensemble des objets de même thème.

## 1) Ce qui doit figurer sur une carte

   - titre et sous-titres explicites

   - copyright

   - L'échelle : son choix dépend de quand l'objet d'étude devient visible
   
   - L'orientation
   
      - Le __Nord Géographique__ est celui où aboutissent tous les méridiens. Il est virtuel.  
      - Le __Nord Magnétique__ de la boussole, a \donnee{0°8' d'angle} d'écart. Il varie en fonction du temps: une date est requise sur la carte.  
      - Certaines cartes sont orientées différemment: celles marines sont perpendiculaires au trait de côte.

      $\implies$ il y a parfois deux flèches nord sur une carte
      
      - La localisation
      - Le système de coordonnées

## 2) Les variables visuelles

   - __forme__ : éléments ponctuels ou surfaciques exprimés par des variables nominales  
   expression figurative, évocatrice ou conventionnelle.
   
   - __orientation__ (dans les motifs utilisés dans une représentation) sert à marquer des différences entre zones, et se combine avec des représentation en grain.
   
   - __grain__ : pour marquer les variations de valeur et de rapport entre quantités, 
   
   - __couleur__ :
   
     - il est usage pour les variables quantitatives relatives (signées) d'utiliser une couleur chaude pour les valeurs positives, froide pour les négatives.
   
   - __taille__ : ex: cercles de plus en plus grands, adapté aux variables quantitatives.

      Elle repose sur un calcul, avec :   
      \ \ \ $\textcolor{red}{R_{i}}$ est le rayon du cercle qui sera représenté  
      \ \ \ $\textcolor{blue}{Q_{i}}$ la quantité que ce rayon représentera  
      \ \ \ $\textcolor{blue}{R_{ref}}$ un rayon de référence choisi pour ne pas masquer les voisins  
      \ \ \ $\textcolor{blue}{Q_{ref}}$ la quantité maximale observable.
      $$ R_{i} = \frac{\sqrt{Q_{i} \times R_{ref}}}{\sqrt{Q_{ref}}} $$  

# 3) Selon que l'on veut représenter...

La première question à se poser est de savoir ce que l’on veut représenter. 

   - Pour une variable en __volume__ ou un __chiffre absolu__, on utilise des \definition{ronds proportionnels}  
   - Pour des __ratios__, __densités__, __évolutions__, __parts__ et __typologie__, on utilise une carte en \definition{aplats de couleurs} (c'est à dire : qui ne varie pas en densité ou luminosité)

       - Dans le cas d’une carte en aplats de couleurs

            - appelée aussi : analyse en classes 
            - ou carte choroplète

       les valeurs :

       - positives sont dans des teintes chaudes (rouge, orange) 
       - alors que les valeurs négatives sont dans des teintes froides (bleu, vert).
\    
   - Les __données bilocalisées__ ou les __flux__ par des \definition{oursins}, \definition{flèches proportionnelles} ou \definition{résultantes vectorielles}  
   - La __localisation__ (par exemple : d’équipements) par des cartes à \definition{symboles}.

Par ailleurs, à une __hiérarchie de valeurs__ correspond un \definition{dégradé de couleurs} dont les couleurs les plus foncées (ou les plus claires) correspondent aux valeurs extrêmes.

## 3) Autres considérations

Les représentations spatio-temporelles évoluent dans le temps, qu'une vidéo peut montrer.

Si l'on présente des évènements de natures différentes par des points, leurs symboles peuvent se superposer. Les agréger les rendra plus lisibles.  
\attention{attention : une mauvaise sémiologie graphique influencera le lecteur}.

Le choix de dégradés est important. L'on peut en choisir deux : un pour les valeurs supérieures à la moyenne, un pour celles inférieures.

\underline{Remarque :} Les représentations 3D sont utiles, notamment en urbanisme, \attention{mais elles exacerbent certains caractères et il faut les observer selon \underline{plusieurs} angles de vue}.

\attention{ÉViter les applats de couleur (cartes chloroplètes) pour présenter des taux}: la surface des unités géographiques influence la lecture. Préférer des symboles proportionnels.

\attention{Les camemberts sont à proscrire}, car peu lisibles.

Les relations spatiales (flux, déplacements...) encombrent vite : se limiter à ceux importants. Par des :

   - sélections, 
   - organisation, 
   - matrice origine-destination pour produire des représentations comme les __cartes en oursins__.

Ne pas faire du territoire une île : son voisinage peut être utile pour le comprendre.

Quand c'est possible, reprendre les mêmes légendes et indicateurs, pour que les cartes restent comparables.

# II) La discrétisation d'un attribut quantitatif en qualitatif

La transformation d'un attribut __quantitatif__ en attribut __qualitatif__ pour sa présentation sur une carte se fait par la \definition{discrétisation}.  
Chaque méthode a ses avantages et inconvénients.

## 1) découpage en classes d'amplitudes égales 

Ne pas dépasser dix classes.

   > \listPlus simple à comprendre  
   > \listMoins s’adapte très rarement à la distribution  
   > \listMoins certaines classes peuvent ne contenir aucune valeur 

## 2) par quantiles (classes d'effectifs égaux)

   > \listPlus produit une carte facile à lire, les couleurs se répartissant à parts égales

   > \listMoins elle ne s’adapte pas toujours à la distribution des données.

## 3) Les méthodes de __Jenks__ et __k-means__ 

Elles créent des classes homogènes :

   - en maximisant la variance entre les classes,
   - et en minimisant la variance au sein de chacune d’entre elles.

   \listPlus elles s’adaptent parfaitement aux données en éliminant les effets de seuil

   \listMoins le temps de calcul de _Jenks_ est long, et _k-means_ peut être préféré

   \listMoins _k-means_ peut être instable et donner des classes différentes pour un même jeu de données. On le répéte plusieurs fois pour garder la meilleure répartition.

## 4) Seuils naturels 

Prise en compte des discontinuités dans la distribution des valeurs

## 5) Selon une progression (arithmétique ou géométrique)

## 6) Selon des moyennes emboitées

## 7) Par écart à la moyenne (en fonction de l'écart-type)

## 8) Arrangement manuel (l'on fixe soi-même les bornes des classes)

   \listPlus utile pour faire apparaître :

   - des valeurs significatives (borne à zéro ou autour de zéro, moyenne...) 
   - pour améliorer le positionnement de certains seuils en fonction de la distribution locale
   - elle peut rendre comparables des cartes entre elles, en fixant des bornes identiques de classes. 

Cette méthode réclame d’analyser d'abord la distribution des données, avec la méthode de Jenks ou de k-means, pour avoir des classes homogènes, puis d'ajustaer les bornes des classes manuellement pour éviter les effets de seuil.

\textbf{\underline{Nombre de classes :}}

Le nombre de classes se calcule en fonction du nombre d’observations. 

La règle de _Sturges_ propose: $1 + 3.3 log_{10}(\bleu{n})$, avec $\bleu{n}$ le nombre d’observations.

   - pour moins de 50 observations : 3 classes
   - de 50 à 150 : 4 classes
   - plus de 150 : 5 classes...

