---
title: "a410. Les opérations sur les topologies"
subtitle: ""
author: Marc Le Bihan
keywords:
- topologie
- opération
- postgis
- intersection (ST_Intersects, ST_Relate)
- objet à l'intérieur d'un autre (ST_Within)
- frontière d'un objet
- intérieur d'un objet
- polyline
- extérieur d'un objet
- modèle d'intersection (DE-9IM, RCC8)
- déconnexion (DC)
- connexion extérieure (Touches, EC)
- chevauchement (PO)
- égal (EQ)
- inclusion tangentielle (TPP)
- inclusion tangentielle inverse (TPPi)
- inclusion non tangentielle (Within, NTPP)
- inclusion non tangentielle inverse (Contains, NTPPi)
- différence symétrique
- différence géométrique
- st_clip
- vérification de bonnes intersections
---

   - un objet qui est à l'intérieur d'un autre objet : \fonction{ST\_Within}
   - savoir s'il y a intersection : \fonction{ST\_Intersects}, et laquelle : \fonction{ST\_Intersection}
   - éterminer les intersections au modèle  _DE-9IM_ : (F, 0, 1, 2) : \fonction{ST\_Relate}

---

La \definition{frontière} d'un objet (son contour) a une dimension inférieure : 

   - une polyline pour un polygone
   - deux points (début et fin) pour une ligne
   - et rien pour un point

\bigbreak

L'\definition{intérieur} d'un objet exclut sa frontière, son __extérieur__ : tout ce qui n'est __pas__ lui.

\bigbreak

Ci-dessous, les intersections sont noircies :

![Intersections](images/topologie_intersections.png)

## 1) Les modèles d'intersections pour les requêtes spatiales

Le modèle \fonction{DE-9IM} aider à réaliser des corrections.

Ici, l'on compare ls intersections de lignes (pontons) et d'un polygone (lac) :

   - __$\varnothing$__ s'il n'y a aucune intersection.
   - __0__ si c'est un point (ou noeud)
   - __1__ si c'est une ligne
   - __2__ si c'est un plan

![Intersections](images/topologie_modeles_DE-9IM_et_RCC8.png)

- Le modèle \fonction{RCC8} propose huit relations, que l'on retrouve dans les requêtes spatiales

   - DC : Déconnexion
   - EC : Connexion extérieure
   - PO : Chevauchement
   - EQ : Egal
   - TPP : Inclusion tangentielle (le bord d'un objet touche un coté de l'autre objet)
   - TPPi : Inclusion tangentielle inverse
   - NTPP : Inclusion non tangentielle
   - NTPPi : Inclusion non tangentielle inverse

\bigbreak

\underline{Remarque :} Les fonctions de distance fonctionnent le plus souvent sur la sphère, mais l'on peut choisir l'élipsoïde.   

## 2) Apparition de nouvelles géométries par les opérations topologiques

Dans les fonctions réalisant des opérations topologiques, interviennent les notions de :

   - Frontière : les lignes communes à deux polygones, les points communs à deux lignes, etc. (Boundary)
   - Rectangle encadrant l'objet (Enveloppe)
   - Intersection, Union
   - Différence symétrique (opposée de l'intersection) : tout ce qui n'est pas en commun.
   - Différence géométrique entre deux objets

## 3) Test de relations géométriques entre deux objets

### a) Equalité entre deux objets (Equals)

### b) Objets disjoints (Disjoint, DC du RCC8)

### c) Objets avec une intersection non vide (Intersects)

   > \methode{ST\_Intersects}{(geometry A, geometry B):boolean}
   >
   > \methode{ST\_Intersects}{(raster A, geometry B):boolean}

\bigbreak

Par exemple, pour \avantage{découper une zone d'intérêt dans un raster} _PostGIS_

  1. l'on crée un polygone, 
  2. on détermine avec quoi il intersecte dans l'image avec \fonction{ST\_Intersect}  
  3. et l'on découpe cette partie pour que les traitements dans cette zone d'intérêt soient moins longs : \fonction{ST\_Clip}.

   \code

   > __Intersection d'un raster et d'un polygone__ (output : une table)
   >
   > ```sql
   > CREATE TABLE quinze022017_b4_b8_reduite AS
   > SELECT r.rid, ST_CLIP(r.rast, ST_GeomFromText('POLYGON((321660 4812100, 321660 4818600, 332400 4818600, 332400 4812100, 321660 4812100))', 32631)) as rast
   >    FROM quinze022017_b4_b8 as r
   >    WHERE ST_Intersects(r.rast, ST_GeomFromText('POLYGON((321660 4812100, 321660 4818600, 332400 4818600, 332400 4812100, 321660 4812100))', 32631));
   > ```

   \endgroup

\bigbreak

   \code

   > \textbf{Obtenir les routes qui s'intersectent entre-elles dans le département du \emph{Gers (32)}}
   >
   > ```sql
   > CREATE TABLE routes_gers AS (
   >    SELECT r.id, r.osm_id, r.fclass, r.geom
   >       FROM routes AS r
   >          CROSS JOIN departements AS d -- produit cartésien
   >    WHERE ST_Intersects(r.geom, d.geom)
   >       AND d.code_insee = '32' -- département du Gers
   >       AND (r.fclass = 'primary' OR r.fclass = 'secondary' OR r.fclass = 'tertiary')); 
   >    -- routes pricipales, secondaires, tertiaires
   > ```

   \endgroup

### d) Intersection incluant une partie des deux objets sans être égale à l'un d'eux (Crosses)

### e) Les objets se touchent (Touches, EC du RCC8)
   
### f) Un objet est à l'intérieur d'un autre (Within, NTPP de RCC8)
   
   > \methode{ST\_Within(Geometry A, Geometry B)} répond vrai si __A__ est totalement inclu dans __B__
   
   \code

   > __Trouver les forêts F entièrement incluses dans les communes C__
   >
   > ```sql
   > SELECT f.id, f.fclass, c.nom
   >    FROM landuse AS f
   >       CROSS JOIN communes as c
   >    WHERE
   >       f.fclass = 'forest'
   >       AND ST_Within(f.geom, c.geom) -- F inclu dans C
   > ```
         
   \endgroup

\bigbreak

   ```log
   id      fclass       nom
   1       "forest"     "Tournefeuille"
   4       "forest"     "Labège"
   96      "forest"     "Plaisance-du-Touch"
   81301   "forest"     "Sariac-Magnoac"
   ...     ...          ...
   ```

\bigbreak
   
   \underline{Remarque :} si l'on place un \fonction{ST\_Intersects}, on aura chacune des forêts communales :
      
   \code

   > ```sql
   > SELECT f.id, ST_Intersection(f.geom, c.geom) as foret_communale, c.nom
   > ```

   \endgroup

### h) Un objet en contient un autre (Contains, NTPPi de RCC8)
   
### i) L'intersection est de même dimension que les objets évalués, mais n'est aucun d'entre-eux (Overlaps)

### j) Test des matrices d'intersection du Modèle DE-9IM

\underline{Remarque :} quand les requêtes deviennent complexes, il est possible d'utiliser les \fonction{Common Table Expression (CTE)}, qui créent des vues temporaires.

   \code

   > ```sql
   > WITH forets AS
   >   (SELECT id, fclass, ST_Area(geom) FROM landuse
   >   WHERE fclass = 'forest')
   > ```
  
   \endgroup

## 4) Récapitulatif des intersections

| formes | mots-clefs |
| ------ | ---------: |
| ![Intersections de formes](images/intersections_formes.png){width=55%} | ![Fonctions sur géométries](images/operations_intersections.jpg){width=40%} |

## 5) Exemple d'une vérification de bonnes intersections 

Voici des intersections de pontons et berges qui nous conviendraient, sous forme de matrice :

![Bonnes intersections](images/relations_matrice_intersection_ok.png){width=25%}

   \code

   > ```sql
   > SELECT ponton.*, berge.*
   > -- FROM ...
   > WHERE ST_Relate(ponton.geom, berge.geom, '1FF00F212');
   > ```

   \endgroup

