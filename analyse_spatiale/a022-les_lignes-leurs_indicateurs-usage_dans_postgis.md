---
title: "a022. Les lignes"
subtitle: "Leurs indicateurs. Leur usage dans PostGIS"
author: Marc Le Bihan
keywords:
- ligne
- taille
- forme
- orientation
- sinuosité
---

Une \definition{Ligne} a une localisation, taille, forme, orientation.

## 1) Remarque sur la définition d'une ligne

Une valeur est associée à la ligne, mais dans la pratique, elle n'a été déterminée que sur un certain nombre de points pour être attribuée.
 
La géostatistique peut souhaiter prédire une valeur en un point qui n'a pas été échantilloné.

## 2) Les indicateurs d'une ligne

### a) Sinuosité

La \avantage{\underline{sinuosité}} d'une ligne c'est sa longueur sur la distance de ses deux extrêmités.
     $$ \begingroup \large \rouge{S} = \frac{L}{D} \endgroup $$

   > ![Sinuosité d'une ligne](images/sinuosite_ligne.jpg)

   > _La sinuosité calculée en objet (à gauche), image (à droite) : les valeurs sont similaires_

