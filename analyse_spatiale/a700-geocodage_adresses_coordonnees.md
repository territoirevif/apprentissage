---
title: "a700. Le géocodage : adresses vers/depuis des coordonnées"
subtitle: ""
category: analyse spatiale
author: Marc Le Bihan
keywords:
- géocodage
- adresse
- adresse postale
- Base Adresse Nationale
- BAN
- coordonnées
- géolocalisation
- Etalab
---

Si l'on possède un code commune, l'INSEE donne sa position par le Code Officiel Géographique (COG).

\bigbreak

Mais si l'on a qu'une adresse postale, Il faut la géocoder.

   - [Etalab](https://guides.etalab.gouv.fr/apis-geo/1-api-adresse.html#les-donnees-d-adresses) est un géocodeur gratuit allant jusqu'à 10000 adresses,

   - La [Base Adresse Nationale (BAN)](https://www.data.gouv.fr/fr/datasets/base-adresse-nationale/)

