---
title: "a650. Le raster, support de l'image satellite"
subtitle: "et ses bandes spectrales"
author: Marc Le Bihan
keywords:
- raster
- image satellite
- lumière
- réflectance
- altimétrie
- modèle numérique de terrain (MNT)
- modèle numérique d'élévation (MNE)
- GeoTIFF
- JPEG2000
- ASC
- DTED
- bande spectrale
- bande de raster
- spectre visible
- bande visible
- spectre invisible
- image spectrale
- résolution spatiale
- résolution spectrale
- infra-rouge
- proche infra-rouge
- BD ORTHO (IGN)
- capteur
- Light and Detection and Ranging (LIDAR)
---

Un raster peut contenir tout type de données,  
   \demitab il n'est pas toujours lié à de la lumière ou de la réflectance.  
   \demitab il peut porter des données d'altimétrie pour un Modèle Numérique de Terrain ou d'Elévation.

Les formats d'images raster les plus connus sont les `GeoTIFF`, `JPEG2000`, `ASC` et `DTED`.

## 1) Les bandes d'un raster

Un raster a souvent de 3 à 15 bandes : 

   - certaines sont visibles (RGB) ou "spectre visible", 
   - et d'autres non : on parle alors d'\definition{image spectrale}.

## 2) Sa résolution

Un raster est défini par : 

   - __sa résolution spatiale__ : la taille que représente un pixel dans la réalité.
 
     > Exemple : la BD ORTHO propose des résolutions de 50 cm et 5 m.

\bigbreak

   - __la résolution spectrale__ : c'est l'étroitesse du faisceau électromagnétique qu'un capteur perçoit. Infra-rouge seulement, ou proche-infrarouge et autres, les capteurs les plus performants séparent en centaines de bandes de longueur variable.

## 3) Un raster particulier : le Light and Detection and Ranging (LIDAR)

Ce n'est pas un raster habituel :

\demitab c'est un ensemble de points connus en trois dimensions dans un système de coordonnées de référence.

