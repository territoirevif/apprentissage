---
title: "a656. Postgis : extraction de raster depuis une image satellite"
subtitle: ""
author: Marc Le Bihan
abstract: |
   Télécharger une image satellite, pour en prendre des bandes et les placer dans PostGIS.  
   [\fonction{raster2pgsql}](#raster2pgsql){.underline} ou [\fonction{ST\_AddBand}](#addBand) ou [\fonction{gdal}](#gdal) font d'une image un raster PostGIS  
keywords:
- raster
- postgis
- image satellite
- bande spectrale
- mono-bande
- multi-bande
- importer une bande spectrale
- projection du raster
- enveloppe convexe (ST_ConvexHull)
- gdal
- gdalbuildvrt
- gdal_translate
- raster2pgsql
- tiff
- ST_UpperLeftX, ST_UpperLeftY
---

Nous allons importer deux nouvelles bandes d'image satellite :  

   - PIR
   - Rouge

qui feront deux nouveaux rasters.

\bigbreak

   \begingroup \cadreGris

   > Un \definition{raster} est une grille multi-bandes  
   > c'est à dire que sur chaque cellule, il n'y a pas une valeur, mais un tableau de valeurs.

   \endgroup

## 1) Importation d'un raster mono-bande

Elle réclame l'[initialisation d'une base de données PostGIS](a600-postgis-installation_et_configuration.pdf) avec une extension \fonction{postgis\_raster}.

### a) Par raster2pgsql{#raster2pgsql}

   \code

   > \underline{Importer une bande d'image satellite :}
   > 
   >  ```bash
   >  # Importe la bande n°4 de l'image satellite en WGS84, puisque la photo est sur la sphère de la terre, UTM 31 (proche Greenwitch)
   >  # datée du 15/02/2017.
   >  raster2pgsql -I -C -s 32631 -t 50x50 T31TCJ_20170215T105121_B04.jp2 quinze022017_b4 | psql -U postgres -d webmapping -h localhost -p 5432
   >   
   >  # Modifier B04 en B08 (et b4 en b8) pour importer la bande n°8 également.
   >  ```

   \endgroup

\bigbreak

   > \textbf{-I} indexe spatialement le raster
   >
   > \textbf{-t largeur x hauteur} des tuiles si demandé,  
   > _le tuilage évite de charger l'ensemble de la carte_
   >
   > \textbf{-s} projection du Raster  
   > \textbf{-C} pour appliquer des contraintes (projection, taille du pixel) 

### b) Par SQL sous PostGIS{#addBand}

\methode{ST\_AddBand}{(rasterCible, rasterOrigine, indexBandeOrigine, indexBandeArrivée)}

   \code

   > \underline{Ajouter une autre bande d'image satellite}  
   > \demitab en ne sélectionnant que les tuiles qui ont la même emprise
   > 
   > ```sql
   > -- Ajouter à un raster la bande 8 de Février.
   > CREATE TABLE fusion_fast_fev AS
   >    SELECT ST_AddBand(quinze022017_b4.rast, quinze022017_b8.rast, 1, 2) AS rast
   >    FROM quinze022017_b4 CROSS JOIN quinze022017_b8
   > WHERE
   >    -- Ne conserver que les tuiles qui ont la même emprise
   >    ST_UpperLeftX(quinze022017_b4.rast) = ST_UpperLeftX(quinze022017_b8.rast) AND
   >    ST_UpperLeftY(quinze022017_b4.rast) = ST_UpperLeftY(quinze022017_b8.rast);
   >   
   > -- Créer une séquence puis ajouter une clef primaire à la table
   > CREATE SEQUENCE fusion_fast_fev_gid_seq;
   > 
   > ALTER TABLE fusion_fast_fev ADD COLUMN gid integer NOT NULL
   >    DEFAULT nextval('fusion_fast_fev_gid_seq'::regclass);
   >      
   > ALTER TABLE fusion_fast_fev ADD CONSTRAINT fusion_rast_pkey PRIMARY KEY(gid);
   >   
   > -- Puis créer un index dessus
   > CREATE INDEX fusion_fast_fev_st_convexhull_idx ON fusion_fast_fev
   >    USING gist -- Utiliser un index "Generalized Search Tree (GIST)"
   >    (ST_ConvexHull(rast)) -- Enveloppe convexe de la forme :
   >    -- la plus petite géométrie qui clos toutes les géoméries
   > ```

   \endgroup

\bigbreak

\underline{Remarque :}

La fonction \fonction{ST\_ConvexHull} retourne l'\avantage{enveloppe convexe} :  
la plus petite géométrie qui englobe toutes les géométries.

## 2) Importation de rasters multi-bandes par gdal et raster2pgsql{#gdal}

   Nous créeons un raster multibande pour l'image satellite de Février 2017 \nopagebreak

   \code

   > \underline{Créer directement un raster multibandes :}
   >
   >  ```bash
   >  # Crée un raster virtuel au format XML
   >  gdalbuildvrt -separate sentinel_4_8.vrt T31TCJ_20170215T105121_B04.jp2 T31TCJ_20170215T105121_B08.jp2
   >   
   >  # Convertir en tiff
   >  gdal_translate -of GTiff sentinel_4_8.vrt sentinel_4_8.tif
   >   
   >  # et importer le tiff en raster multibandes
   >  raster2pgsql -I -C -s 32631 -t 50x50 sentinel_4_8.tif quinze022017_b4_b8 | psql -U postgres -d webmapping -h localhost -p 5432
   >   ```

   \endgroup

\bigbreak

   Respectivement, pour Juillet et Août 2017 : \nopagebreak

   \code
   
   > ```bash
   > # Pour Juillet 2017 (20170705 Indique 05/07/2017)
   > gdalbuildvrt -separate sentinel_4_8.vrt rasters/S2A_MSIL1C_20170705T105031_N0205_R051_T31TCJ_20170705T105605.SAFE/GRANULE/L1C_T31TCJ_A010630_20170705T105605/IMG_DATA/T31TCJ_20170705T105031_B04.jp2 rasters/S2A_MSIL1C_20170705T105031_N0205_R051_T31TCJ_20170705T105605.SAFE/GRANULE/L1C_T31TCJ_A010630_20170705T105605/IMG_DATA/T31TCJ_20170705T105031_B08.jp2
   > 
   > gdal_translate -of GTiff sentinel_4_8.vrt sentinel_4_8.tif
   > 
   > raster2pgsql -I -C -s 32631 -t 50x50 sentinel_4_8.tif cinq072017_b4_b8 | psql -U postgres -d webmapping -h localhost -p 5432
   > 
   > # Pour Août 2017
   > gdalbuildvrt -separate sentinel_4_8.vrt rasters/S2A_MSIL1C_20170824T105031_N0205_R051_T31TCJ_20200817T060840.SAFE/GRANULE/L1C_T31TCJ_A011345_20170824T105240/IMG_DATA/T31TCJ_20170824T105031_B04.jp2 rasters/S2A_MSIL1C_20170824T105031_N0205_R051_T31TCJ_20200817T060840.SAFE/GRANULE/L1C_T31TCJ_A011345_20170824T105240/IMG_DATA/T31TCJ_20170824T105031_B08.jp2
   > 
   > gdal_translate -of GTiff sentinel_4_8.vrt sentinel_4_8.tif
   > 
   > raster2pgsql -I -C -s 32631 -t 50x50 sentinel_4_8.tif vingt_quatre082017_b4_b8 | psql -U postgres -d webmapping -h localhost -p 5432
   ```
   
   \endgroup

