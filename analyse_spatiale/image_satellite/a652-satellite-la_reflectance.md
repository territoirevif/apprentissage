---
title: "a652. La réflectance"
subtitle: "des végétaux, de l'eau..."
author: Marc Le Bihan
keywords:
- image satellite
- végétation
- infrarouge
- PIR
- réflectance
- Normalized Difference Vegetation Index (NVDI)
- signature spectrale
- feuillus
- résineux
- saison
- hiver
- été
- printemps
- eau
---

## 1) La végétation

### a) Ses caractéristiques spectrales

   - Son infrarouge est haut
   - son rouge et vert sont bas
   - on utilise souvent la composition en fausses couleurs (PIR, rouge, vert) pour la faire apparaître.  

\bigbreak

Sa réflectance montre des sauts caractéristiques :

   - dans le domaine visible (0.4 à 0.7 µm), 
   - dans et le proche infrarouge (entre 0.7 et 1.1 µm), surtout 
   - avec trois sauts caractéristiques.  

![Signatures spectrales des végétaux](images/longueur-ondes_vegetaux.jpg)  
_Les signatures spectrales des végétaux_

### b) L'indice Normalized Difference Vegetation Index (NVDI)

L'indice \definition{Normalized Difference Vegetation Index (NVDI)} met en évidence la signature spectrale d'un végétal.

   > \bleu{$R$} : la mesure radiométrique d'une bande __visible__  
   > souvent le rouge, centrée entre $0.4$ et $0.7$ µm.
   >
   > \bleu{$\emph{\text{PIR}}$} : celle d'une bande __proche infrarouge__, comprise entre $0.7$ et $1.1$ µm  
   > elle aide à distinguer les feuillus des résineux.
$$
\rouge{\emph{\text{NVDI}}} = \frac{\emph{\text{PIR}} - R}{\emph{\text{PIR}} + R}
$$

L'on prend plusieurs images séparées dans le temps pour mieux discriminer la végétation :

   - __En hiver__, les champs de cultures automnales, les prairies ont une forte activité photosynthétique quand les feuillus n'en ont plus.  
   - __En Juillet__, les feuillus sont en pleine activité, quand les cultures d'automne sont moissonnées.
   - Il faut aussi discriminer les feuillus des cultures de printemps (maïs, tournesol...).

## 2) L'eau

   - Basse pour toutes les bandes :  elle dépend des matières en suspension qu'elle transporte.


![Signatures spectrales combinées](images/signatures_spectrales_combinees.png){width=70%}  
_Image : Signatures spectrales combinées_

