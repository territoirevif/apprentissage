---
title: "a658. Postgis : calcul de NDVI sur la végétation"
subtitle: "et confrontation avec des données de l'Open Data"
author: Marc Le Bihan
abstract: |
   Déterminer la végétation depuis une image satellite  
   joindre un raster et des vecteurs par _PostGIS_
keywords:
- raster
- image satellite
- postgis
- vecteurs
- ST_CLIP
- ST_MapAlgebra
- ST_PixelAsPolygons
- ST_Within
- ST_Histogram
- ST_DumpAsPolygons
- ST_Dump
- ST_Dimension
- ST_Raster
- ST_BandPixelType
- ST_Value
- NVDI
- combinaison de bandes raster
- zone de forêt
- intersection des données saisonnières
- dimension d'une géométrie (point, ligne, polygone)

---

   > \bleu{\emph{La carte des forêts de l'Open Data}}\   
   > \bleu{\emph{est-elle exacte si on la compare avec une image satellite d'aujourd'hui ?}}

---

   - [ST\_Clip](#clip) pour prendre une zone d'étude plus réduite dans une image satellite
   - [ST\_MapAlgebra](#mapAlgebra) pour calculer un indice NVDI à partir des bandes du raster  
   - [ST\_PixelAsPolygons](#pixelAsPolygons) transforme les pixels d'un raster en polygones
   - [ST\_Within](#within) détermine si une forme est totalement incluse dans une autre
   - [ST\_Histogram](#histogram) donne la répartition en classes avec leurs effectifs des valeurs d'un raster
   - [ST\_DumpAsPolygons](#dumpAsPolygons) convertit des pixels de rasters en polygones
   - [ST\_Dump](#dump-dimension) qui sépare une `GEOMETRYCOLLECTION` en ses élements simples.
   - [ST\_Dimension](#dump-dimension) donne la dimension (point, ligne, polygone) d'une géométrie
   - [ST\_Raster](#raster) transforme en raster un polygone unique
   - `ST_BandPixelType` : dit le type des pixels dans une bande (float, double...)
   - `ST_Value` : donne la valeur d'un point donné dans un raster.

---

En joignant rasters et vecteurs par PostGIS, nous allons :

   1. calculer un indice `NVDI` à partir des bandes de l'image satellite et le stocker dans un raster
   2. reconstituer les polygones `forest` en prenant ces valeurs `NVDI`
   3. et éjecter les polygones qui ne sont pas des forêts, d'après ce `NVDI`

## 1) Combiner les niveaux de couleurs en un indice NVDI

### a) Réduction de la zone d'étude dans l'image satellite par un ST\_Clip{#clip}

L'on réduit la zone géographique d'étude dans l'image satellite, par un \fonction{ST\_Clip} :

   \code

   > \underline{Restriction de la zone géographique d'étude dans l'image satellite :}
   >
   > ```sql
   > CREATE TABLE quinze022017_b4_b8_reduite AS
   >    SELECT r.rid, ST_Clip(r.rast, ST_GeomFromText('POLYGON((321660 4812100, 321660 4818600, 332400 4818600, 332400 4812100, 321660 4812100))', 32631)) as rast
   >       FROM 
   >         quinze022017_b4_b8 as r WHERE ST_Intersects(r.rast, ST_GeomFromText('POLYGON((321660 4812100, 321660 4818600, 332400 4818600, 332400 4812100, 321660 4812100))', 32631))
   > ```

   \endgroup

\bigbreak

   Respectivement, pour Juillet at Août :

   \begingroup \cadreGris \tiny
   
   > ```sql
   > CREATE TABLE cinq072017_b4_b8_reduite AS
   >    SELECT r.rid, ST_Clip(r.rast, ST_GeomFromText('POLYGON((321660 4812100, 321660 4818600, 332400 4818600, 332400 4812100, 321660 4812100))', 32631)) as rast
   >    FROM cinq072017_b4_b8 as r WHERE ST_Intersects(r.rast, ST_GeomFromText('POLYGON((321660 4812100, 321660 4818600, 332400 4818600, 332400 4812100, 321660 4812100))', 32631))
   > 
   > CREATE TABLE vingt_quatre082017_b4_b8_reduite AS
   >    SELECT r.rid, ST_Clip(r.rast, ST_GeomFromText('POLYGON((321660 4812100, 321660 4818600, 332400 4818600, 332400 4812100, 321660 4812100))', 32631)) as rast
   >    FROM vingt_quatre082017_b4_b8 as r WHERE ST_Intersects(r.rast, ST_GeomFromText('POLYGON((321660 4812100, 321660 4818600, 332400 4818600, 332400 4812100, 321660 4812100))', 32631))
   ```
   
   \endgroup

### b) Combinaison des bandes du raster en un NVDI par ST\_MapAlebra{#mapAlgebra}

L'on combine les niveaux de couleurs de l'image satellite en un indice `NDVI` pour être certain d'être devant une forêt.
    
   - On part d'un raster `xxx_b4_b8`, créé précédement

     \ \ \ \ \ \ \ \ il a deux bandes, `b4` et `b8` : Rouge et Proche Infra Rouge (PIR)
  $$ \text{et l'on va lui appliquer la formule : } \rouge{\text{\emph{NDVI}}} = \frac{\text{\emph{PIR}} - R}{\text{\emph{PIR}} + R} $$

   - \fonction{ST\_MapAlgebra} applique une formule mathématique sur deux rasters et en produit un autre.

\bigbreak

   \code

   > \underline{Calculer un indice NVDI}  
   > \ \ \ \ un raster source étant PIR, l'autre rouge  
   > \ \ \ \ et stocker ce résultat dans un nouveau raster
   > 
   > ```sql
   > CREATE TABLE ndvi_15fev17_reduite AS
   >    SELECT ST_MapAlgebra(
   >       a.rast, 2, -- rast2 sera Rouge
   >       a.rast, 1, -- rast1 sera Proche Infra Rouge (PIR)
   >       '([rast1] - [rast2]) / ([rast1] + [rast2])::float', -- NDVI
   >       '32BF' -- Type des pixels : 32 bits, type : Float
   >    ) as rast
   >  FROM
   >     quinze022017_b4_b8_reduite AS a;
   > ```

   \endgroup

   ![Raster résultat](images/raster_resultat_map_algebra.png){width=80%}

   _Raster résultat de \fonction{ST\_MapAlgebra} : le NDVI_

\bigbreak

   Et pour juillet et août :\nopagebreak

   \begingroup \cadreGris \tiny
   
   > ```sql 
   > CREATE TABLE ndvi_05jui17_reduite AS
   >    SELECT ST_MapAlgebra(
   >      a.rast, 2, -- rast1 sera Proche Infra Rouge (PIR)
   >      a.rast, 1, -- rast2 sera Rouge
   >      '([rast1] - [rast2]) / ([rast1] + [rast2])::float', -- NDVI
   >      '32BF' -- Type des pixels : 32 bits, type : Float
   >    ) as rast
   >    FROM
   >       cinq072017_b4_b8_reduite AS a;
   > 
   > CREATE TABLE ndvi_24aou17_reduite AS
   >    SELECT ST_MapAlgebra(
   >      a.rast, 2, -- rast1 sera Proche Infra Rouge (PIR)
   >      a.rast, 1, -- rast2 sera Rouge
   >      '([rast1] - [rast2]) / ([rast1] + [rast2])::float', -- NDVI
   >      '32BF' -- Type des pixels : 32 bits, type : Float
   >    ) as rast
   >    FROM
   >       vingt_quatre082017_b4_b8_reduite AS a;
   > ```

   \endgroup

\bigbreak

\underline{Remarque technique SQL :}\nopagebreak

La constante \mauve{\texttt{32BF}} déclare les données du raster en \mauve{float 32 bits}.  
 
D'autres choix sont possibles, et un \methode{ST\_BandPixelType}{(raster rast, integer bandnum=1)} donne le codage des pixels dans une bande d'un raster :
 
   \begingroup \footnotesize

   > `1BB`   - 1-bit boolean  
   > `2BUI`  - 2-bit unsigned integer  
   > `4BUI`  - 4-bit unsigned integer  
   > `8BSI`  - 8-bit signed integer  
   > `8BUI`  - 8-bit unsigned integer  
   > `16BSI` - 16-bit signed integer  
   > `16BUI` - 16-bit unsigned integer  
   > `32BSI` - 32-bit signed integer  
   > `32BUI` - 32-bit unsigned integer  
   > `32BF`  - 32-bit float  
   > `64BF`  - 64-bit float

   \endgroup

## 2) Déterminer quels pixels du raster appartiennent à des zones de forêt

La couche `landuse` énumère parmi ses zones, des forêts.  
Pour mettre en regard nos NVDI, il faut d'abord en faire des polygones. 

### a) Transformation de pixels de raster en polygones{#pixelAsPolygons}

\fonction{ST\_PixelAsPolygons} transforme en polygone chaque pixel du raster : \nopagebreak

   \code

   > \underline{Transformer des pixels en polygones :}
   >
   > ```sql
   > select ST_AsText(a.geom) from (
   >    SELECT 
   >      (ST_PixelAsPolygons(rast,1)).* FROM ndvi_15fev17_reduite
   >    ) as a;
   > ```

   \endgroup

   \begingroup \footnotesize

   > ```
   > geom, val, x, y
   > "POLYGON((321660 4818600,321670 4818600,321670 4818590,
   >           321660 4818590,321660 4818600))"
   >           0.7097999453544617
   >           1 1
   > "POLYGON((321670 4818600,321680 4818600,321680 4818590,
   >           321670 4818590,321670 4818600))"
   >           0.7075257897377014
   >           2 1
   > ```

   \endgroup

\underline{Remarque :}\nopagebreak

   \fonction{(ST\_PixelAsPolygons(rast,1)).*} sépare l'enregistrement en tous ses champs.  
   Voici le raster retourné alors :  
   _(Remarque : un `ST_AsText` a été ajouté sur geom : il n'est pas dans la requête)_
   
   \begingroup \footnotesize

   > ```txt
   > geom                                                                                   val                 x  y
   > POLYGON((321660 4818600,321670 4818600,321670 4818590,321660 4818590,321660 4818600))  0.7097999453544617  1  1 
   > POLYGON((321670 4818600,321680 4818600,321680 4818590,321670 4818590,321670 4818600))  0.7075257897377014  2	 1
   > ```

   \endgroup

   \fonction{ST\_PixelAsPolygons(rast,1)} seul ne présente pas le même découpage, et est moins exploitable. \nopagebreak 

   \begingroup \footnotesize

   > ```txt
   > st_pixelsaspolygons
   > (0103000020777F0000010000000500000000000000F0[...],0.7097999453544617,1,1)
   > (0103000020777F000001000000050000000000000018[...],0.7075257897377014,2,1)
   > ```

   \endgroup

### b) Déterminer quels pixels "polygonés" appartiennent à des zones de forêt{#within}

\tab Ces polygones créés, on ne retient que ceux rapport avec zone de forêt dans la couche `landuse`, grâce à l'opérateur \fonction{ST\_Within}.

   \code

   > \underline{Sélectionner les pixels du raster qui sont dans une forêt}  
   > \ \ \ \ d'après les polygones `landuse` open data  
   > \ \ \ \ et calculer leur minimum, maximum, moyenne, écart-type
   >  
   > ```sql
   > -- minimum, maximum, moyenne et écart-type du NDVI de forêts.
   > WITH pixels AS
   >    (SELECT (ST_PixelAsPolygons(rast,1)).val,     -- valeur du pixel
   >       (ST_PixelAsPolygons(rast,1)).geom AS geom  -- polygone
   >    FROM ndvi_15fev17_reduite)
   >
   > -- Tous les pixels du raster qui sont dans un polygone forest de landuse
   > -- forment une grande série, dont on prend les bornes et l'écart-type.
   > SELECT 
   >    min(p.val) AS min, max(p.val) AS max, -- valeur min et max
   >    avg(p.val) AS moyenne,                -- moyenne
   >    stddev(p.val) AS ecart_type           -- et écart-type
   > FROM pixels AS p
   >    CROSS JOIN landuse
   > WHERE
   >    landuse.fclass = 'forest'
   >    AND ST_Within(p.geom, landuse.geom);
   >    -- où p (les pixels en polygones) sont entièrement inclus dans une forêt
   > ```
 
   \endgroup

\bigbreak

Dans cette requête, chaque pixel de l'image satellite (qu'un `ST_Clip` a restreint à notre zone d'étude, précédemment) est devenu un polygone le temps de l'opération.

\avantage{Nous connaissons maintenant les NVDI de ce que l'open data déclare être une forêt}
\avantage{$\rightarrow$ nous allons pouvoir vérifier si c'est vrai.}

\attention{on voit des valeurs extrêmes (min, max) qui ne sont pas des forêts} :\nopagebreak 

\bigbreak

   \begingroup \cadreAvantage \small

   > ```
   > min         max       moyenne   ecart-type
   > -0.016042   0.78732   0.39966   0.06517
   > ```

   \endgroup

\bigbreak

leurs NVDI sont normalement limités à l'intervalle $\mathbf{[0.23, 0.57]}$ : nous allons pouvoir en éjecter.

## 3) Placer les valeurs NVDI dans les polygones forest

### a) Créer des sous-rasters NVDI de la forme des polygones forest de landuse{#union}

\tab Nous rassemblons dans un nouveau raster les `NVDI` de 0 à 1 de notre zone d'étude, pour les seules zones où il y a des forêts, d'après landuse.

   \code

   > \underline{Répartir les pixels d'un raster en plusieurs sous-rasters}  
   > chacun correspondant à un polygone de forêt,  
   > puis, les re-rassembler ensemble dans un vaste raster
   >
   > ```sql
   > CREATE TABLE ndvi_forests_fev17 AS
   > WITH 
   >    ndvi_fev_union AS -- réunit tous les pixels d'un raster
   >       (SELECT ST_Union(r.rast) AS rast
   >          FROM ndvi_15fev17_reduite AS r),
   >
   >    forests AS -- les forêts qui intersectent notre raster
   >       (SELECT f.geom
   >          FROM landuse f, ndvi_fev_union
   >          WHERE f.fclass = 'forest'
   >             AND ST_Intersects(f.geom, ndvi_fev_union.rast))
   >   
   > -- Réunir les clippings du raster avec des zones de forêt
   > SELECT ST_Union(ST_Clip(ndvi_fev_union.rast, f.geom)) AS rast
   >    FROM ndvi_fev_union
   >    CROSS JOIN forests AS f
   > ```

   \endgroup

   ![Raster résultat: forêts](images/raster_resultat_forets.png){width=70%}

   _La restriction de notre raster avec ce que `landuse` dit être des forêts_

## 4) Restriction du raster à ce qui est réellement une forêt{#histogram}

### a) Observation des NVDI des pixels du raster dans chaque polygone de forêt supposée

La fonction \fonction{ST\_Histogram} va répartir les valeurs du raster en classes, dont il va nous donner l'effectif.

   > \bleu{ndvi\_forests\_fev17} : le raster des forêts théoriques (d'après `landuse`) dans la portion d'image satellite de Février 2017.

   \code

   > \underline{Répartition en classes des valeurs NVDI des zones de forêt}
   >
   > ```sql
   > SELECT (ST_Histogram(ndvi_forests_fev17.rast, 1)).*
   >    FROM ndvi_forests_fev17;
   > ```
   >
   > \underline{Remarque :}  
   > La fonction \fonction{ST\_Histogram} ne sait travailler que sur un raster.

   \endgroup

   \begingroup\footnotesize

   > ```
   >  min                  	max                    count     percent
   > -0.17820657789707184	-0.12069186743567972	2		5.903361964638862e-05
   > -0.12069186743567972	-0.06317715697428761	0		0
   > -0.06317715697428761	-0.0056624465128954846	1		2.951680982319431e-05
   > -0.0056624465128954846	0.05185226394849664		6		0.00017710085893916585
   > 0.05185226394849664	0.10936697440988877		62		0.0018300422090380471
   > 0.10936697440988877	0.16688168487128088		227		0.006700315829865108
   > 0.16688168487128088	0.224396395332673		272		0.008028572271908853
   > 0.224396395332673		0.28191110579406514		694		0.02048466601729685
   > 0.28191110579406514	0.33942581625545726		3128	0.0923285811269518
   > 0.33942581625545726	0.3969405267168494		13342	0.3938132766610585
   > 0.3969405267168494		0.4544552371782415		11439	0.3376427875675197
   > 0.4544552371782415		0.5119699476396337		2937	0.08669087045072169
   > 0.5119699476396337		0.5694846581010258		935		0.02759821718468668
   > 0.5694846581010258		0.6269993685624179		427		0.01260367779450397
   > 0.6269993685624179		0.68451407902381		250		0.007379202455798577
   > 0.68451407902381		0.7420287894852021		129		0.003807668467192066
   > 0.7420287894852021		0.7995434999465942		28		0.0008264706750494407
   > ```

   \endgroup

   Les points du raster (des pixels individuels) sont évalués. L'on voit qu'ils ne sont pas tous des forêts : 

   \begingroup\footnotesize

   > ```
   > [-0.178, -0.121]    2 NDVIs   0.00005%
   > [0.742, 0.7995]    28 NDVIs   0.00083%
   > ```

   \endgroup

### b) Créer le masque de ceux qui sont réellement des forêts{#dumpAsPolygons}

\tab Ce masque va être une couche géographique de polygones pour chaque point (pixel) du raster [converti par la fonction \fonction{ST\_DumpAsPolygons}], qui portera une valeur indicatrice :  

   > __1__ : si on considère que c'est une forêt (nvdi compris entre ]0.23, 0.57[)  
   > __0__ : sinon.

   \code

   > \underline{Placer dans une table les pixels dont le NVDI est compris dans ]0.23, 0.57[}  
   > en tant que polygones, pour en faire une couche géographique
   >
   > ```sql
   > CREATE TABLE geom_mask_fev17 AS
   >    -- Ne retenir que les pixels de forets reelles, d'après leur NVDI
   >    WITH mask_fev17 AS
   >       (SELECT ST_MapAlgebra(a.rast, 1, '32BF', -- Depuis la bande 1, produire des float 32 bits
   > 	      'CASE WHEN [rast1] > 0.23 AND [rast1] < 0.57 THEN 1 ELSE 0 END', -1) as rast
   >          FROM ndvi_15fev17_reduite as a)
   > 	-- Creer la table geom_mask_fev17 avec ces pixels convertis en polygones
   > 	SELECT (ST_DumpAsPolygons(r.rast)).* FROM mask_fev17 r
   > ```

   \endgroup

\bigbreak

\underline{Remarque :}

   > Dans les familles \fonction{ST\_Dump} (`ST_Dump`, `ST_DumpAsPoints`, `ST_DumpAsPolygons`, `ST_DumpAsLines`), si la fonction revoie un \mauve{geometry\_dump}, il faut mettre le champ entre parenthèses pour extraire sa géométrie :
   >
   > ```sql
   > ST_AsText((a).geom) 
   > ```

## 5) Résumer/industrialiser les requêtes précédentes

Cette requête va utiliser trois CTE communiquant par des variables internes :

   1. \mauve{pixels}, qui transforme les pixels en polygones

   2. \mauve{intervalle\_admissible} qui détermine les bornes $min, max$, la moyenne et l'écart-type des `NVDI` calculés sur l'image satellite, aux endroits où la couche `landuse` pense qu'il y a des forêts.

   > $\rightarrow$ d'où le \mauve{$2.58 \times \text{écart-type}$} qu'on voit, qui retient \mauve{98\%} de ce qui est gaussien.

   3. \mauve{mask} marque les pixels de forêts confirmées : ceux dans la borne $\mathbf{]min, max[}$.

   \code

   > \underline{Automatisation du calcul des bornes min, max des pixels à retenir, d'après leur NVDI}
   > 
   > ```sql
   > CREATE TABLE geom_mask_jui17 AS (
   >    -- Récupération des NVDI des points classifiés 'forêts' par landuse
   >    WITH pixels AS (
   > 	   SELECT 
   > 	      (ST_PixelAsPolygons(rast,1)).val, (ST_PixelAsPolygons(rast,1)).geom AS geom
   > 	   FROM ndvi_05jui17_reduite),
   > 		  
   >    -- Déterminer les valeurs min, max de l'intervalle des valeurs NVDI admises
   >    -- pour exclure, plus tard, celles extrêmes
   >    intervalle_admissible AS (
   > 	   SELECT 
   > 	      avg(p.val) - 2.58 * stddev(p.val) AS min, -- borne min intervalle 
   > 	      avg(p.val) + 2.58 * stddev(p.val) AS max, -- borne max intervalle
   > 	      avg(p.val) AS moyenne,
   > 	      stddev(p.val) AS ecart_type
   > 	   FROM pixels AS p
   > 	      CROSS JOIN landuse
   > 	   WHERE landuse.fclass='forest' AND
   > 	      ST_Within(p.geom, landuse.geom)),
   > 	
   > 	-- Création du masque, pour le filtrage, les bornes min et max étant passées
   > 	-- depuis le CTE intervalle_admissible ci-dessus
   > 	mask AS (
   > 		SELECT
   > 		   ST_MapAlgebra(a.rast, 1, '32BF', 'CASE WHEN [rast1] > '||i.min||' AND [rast1] < '||i.max||' THEN 1 ELSE 0 END', -1) AS rast
   > 		FROM ndvi_05jui17_reduite AS a
   > 		   CROSS JOIN (
   > 			   SELECT intervalle_admissible.min, intervalle_admissible.max FROM intervalle_admissible) AS i)
   > 
   >     -- Requête principale d'alimentation de la table créée
   > 	SELECT (ST_DumpAsPolygons(r.rast)).* FROM mask r
   > )
   > ```

   \endgroup

Là, il suffit de substituer `geom_mask_aou17` à `geom_mask_jui17` et `ndvi_24aou17_reduite` à `ndvi_05jui17_reduite`, et de re-exécuter la requête.

## 6) Réaliser l'intersection des couches des trois saisons

Voici ce que donnent ces trois masques saisonniers sur les forêts, quand on les superpose :

   > ![Forêts selon les saisons](images/forets_trois_saisons.png){width=80%}   
   > _vert : février, mauve : juillet, rose : août_

Nous allons prendre leur intersection : là, nous serons sûrs que ce seront des forêts.

### a) Mettre un identifiant unique aux polygones des masques

En générant un champ de clef unique \mauve{gid} :\nopagebreak

   \code

   > ```sql
   > ALTER TABLE geom_mask_fev17 ADD COLUMN
   >    gid SERIAL PRIMARY KEY;
   >    
   > ALTER TABLE geom_mask_jui17 ADD COLUMN
   >    gid SERIAL PRIMARY KEY;
   >    
   > ALTER TABLE geom_mask_aou17 ADD COLUMN
   >    gid SERIAL PRIMARY KEY;
   > ```

   \endgroup

### b) Réaliser les intersections entre forêts selon leur mois, en éjectant du résultat des intersections ce qui n'est pas un polygone{#dump-dimension}

\attention{car l'intersection de polygones ne renvoie pas nécessairement des polygones !}
 
\demitab \attention{$\rightarrow$ parfois seulement des lignes, des points, si leur frontière est ténue}

\bigbreak

\underline{Mise en évidence du problème sur un exemple :}

Pour illustration, les géométries issues de l'intersection de deux couches :

   \code
 
   > ```sql
   > SELECT f.gid, ST_AsText((ST_Intersection(f.geom, j.geom))) as geom
   >    FROM geom_mask_fev17 as f, geom_mask_jui17 as j
   > WHERE ST_Intersects(f.geom, j.geom)
   > ORDER BY f.gid
   > ```

  \endgroup

\bigbreak

renvoient toutes sortes d'objets hétéroclites :

   \begingroup \small
 
   > ```txt
   > gid  geom
   > 1    "POLYGON((321910 4818590,321920 4818590..."
   > 2    "POLYGON((321810 4818600,321810 4818590..."
   > 2    "GEOMETRYCOLLECTION(POINT(321830 4818590),POINT(321840 4818580),LINESTRING(321990 4818540,321980 4818540)"
   > 3    "LINESTRING(322000 4818540,322000 4818530)"
   > 5    "POINT(322500 4818530)"
   > ```

   \endgroup

Ces géométries-là, il faut les exclure. Elles ne nous intéressent pas.

\bigbreak

\underline{Correction du problème :}

Pour cela, nous utilisons :

   1. \fonction{ST\_Dump} qui sépare des collections de géométries (`GEOMETERYCOLLECTION`) en éléments simples
   2. \fonction{ST\_Dimension} qui renvoie la dimension d'une géométrie :

      > __0__ : c'est un point  
      > __1__ : une ligne  
      > __2__ : un polygone

      et nous ne retiendrons que les polygones.

\bigbreak

   \code

   > \underline{Ne retenir que les polygones à l'issue d'une intesection de deux couches}
   >
   > ```sql
   > WITH inter1 AS (
   > 	-- Séparer les collections de géométries
   >    SELECT 
   > 	  f.gid AS gid_f, j.gid AS gid_j,
   > 	  (ST_Dump(ST_Intersection(f.geom, j.geom))).geom as geom
   >    FROM 
   > 	  geom_mask_fev17 as f, geom_mask_jui17 as j
   >    WHERE 
   > 	   ST_Intersects(f.geom, j.geom))
   > -- Ne reternir que les polygones
   > (SELECT i.gid_f, i.gid_j, i.geom
   >    FROM inter1 AS i
   > WHERE
   >    ST_Dimension(i.geom) = 2)
   > ```

   \endgroup

\bigbreak

\underline{Mise en application concrète et complète de la solution :}

Voici la requête réalisant les trois intersections, avec :

   > \bleu{f} : forêts en Février  
   > \bleu{j} : forêts en Juillet  
   > \bleu{a} : forêts en Août  
   > \rouge{i} : intersection à réaliser

\bigbreak

   \code
   
   > \underline{Réalisation des trois intersections et restrictions à leurs polygones :}  
   > 
   > ```sql
   > CREATE TABLE resultat AS
   > WITH inter1 AS (
   > 	-- Séparer les collections de géométries Février - Juillet
   >    SELECT 
   > 	  f.gid AS gid_f, j.gid AS gid_j,
   > 	  (ST_Dump(ST_Intersection(f.geom, j.geom))).geom as geom
   >    FROM 
   > 	  geom_mask_fev17 as f, geom_mask_jui17 as j
   >    WHERE 
   > 	   ST_Intersects(f.geom, j.geom)),
   > 
   >     -- Ne retenir que les polygones de l'intersection Février - Juillet
   >    inter1_clean AS
   >        (SELECT i.gid_f, i.gid_j, i.geom
   >           FROM inter1 AS i
   >        WHERE
   >           ST_Dimension(i.geom) = 2),
   > 		  
   >     -- Séparer les collections de géométrie de l'intersection précédente avec celles d'Août
   >    inter2 AS
   >       (SELECT 
   > 	     i.gid_f, i.gid_j, a.gid AS gid_a,
   > 	     (ST_Dump(ST_Intersection(i.geom, a.geom))).geom as geom
   >       FROM 
   > 	     geom_mask_aou17 as a, inter1_clean as i
   >       WHERE 
   > 	     ST_Intersects(i.geom, a.geom))
   > -- Et créer la table résultat en restreignant aux polygones une dernière fois
   > SELECT 
   >    i.gid_f, i.gid_j, i.gid_a, i.geom
   > FROM 
   >    inter2 AS i
   > WHERE
   >    ST_Dimension(i.geom) = 2
   > ```

   \endgroup

> ![Intersection forêts saisonnières](images/intersection_forets_saisonnieres.png){width=80%}   
> _L'intersection des couches forêts de Février, Juillet, Août_

## 7) Annexe

### a) convertir des vecteurs en raster{#raster}

\attention{Un raster ne peut se créer qu'à partir d'un seul polygone}

Il faut rassembler tous les polygones par un \fonction{ST\_Union} avant,  
pour les soumettre à \fonction{ST\_AsRaster}, qui ne prend qu'une seule géométrie en entrée.

\bigbreak

   \code
 
   > ```sql
   > CREATE TABLE forets_rasterisees AS
   >    -- Réunion des tuiles dans un raster unique
   >    WITH raster AS (
   >       SELECT ST_Union(ndvi_15fev17_reduite.rast) AS rast
   >          FROM ndvi_15fev17_reduite),
   > 	  
   >    -- Réunion des forêts dans un unique multi-polygone
   >    foret_union AS (
   >       SELECT ST_Union(geom) AS geom 
   >          FROM landuse 
   > 	        CROSS JOIN raster
   >        WHERE 
   >          landuse.fclass='forest' AND
   >          ST_Intersects(raster.rast, landuse.geom))
   > 		 
   >     -- Création du raster :
   > 	SELECT ST_AsRaster(f.geom, raster.rast,'32BF') as rast
   > 	    FROM foret_union as f 
   > 		    CROSS JOIN raster
   > ```

   \endgroup

### b) ST\_Value : obtenir la valeur d'un point donné

   \code

   > ```sql
   > SELECT ST_Value(r.rast, ST_GeomFromText('POINT(325000 4815000)', 32631)) AS valeur
   >    FROM ndvi_15fev17_reduite as r
   > WHERE
   >    ST_Intersects(r.rast, ST_GeomFromText('POINT(325000 4815000)', 32631))
   > ```

   \endgroup

   > ```txt
   > 0.6198185086250305
   > ```

