---
title: "a654. Le satellite Sentinel 2A ou B"
subtitle: "et son accès par le site Copernicus EU"
author: Marc Le Bihan
keywords:
- satellite
- image satellite
- sentinel 2A
- sentinel 2B
- site web Copernicus
- type de satellite
- zone visée
- couverture nuageuse
- post-traitement
- bande spectrale
- résolution spatiale
- projection spatiale
- emprise
- orbite
---

Le satellite __Sentinel 2A__ ou __2B__ est accessible par le site web [Copernicus EU](https://dataspace.copernicus.eu/)  
[https://dataspace.copernicus.eu/](https://dataspace.copernicus.eu/)

Ce site permet des requêtes par :

   - type de satellite
   - zone visée
   - date
   - couverture nuageuse
   - qualité du post-traitement

## 1) Caractéristiques du satellite Sentinel

   - 13 Bandes spectrales du visible à l'infrarouge court (SWIR)  
     en passant par le proche infrarouge (PIR).  
   - Résolution spatiale : 10 à 60 mètres
   - Emprise de 290 kilomètres de large
   - Projection UTM 31 / EPSG 32631 (WGS 84, UTM 31)
   - Fréquence de balayage : tous les cinq jours

## 2) La récupération de données satellite

1. Se créer un compte sur [Copernicus EU](https://dataspace.copernicus.eu/)  

2. Télécharger en tapant dans la barre de recherche :

   ```
   S2A_MSIL1C_20170705T105031_N0205_R051_T31TCJ_20170705T105605.SAFE
   S2A_MSIL1C_20170215T105121_N0204_R051_T31TCJ_20170215T105607.SAFE
   S2A_MSIL1C_20170824T105031_N0205_R051_T31TCJ_20170824T105240.SAFE
   (ce dernier est offline)
   ```

3. Utiliser la fonction de recherche avancée (en haut à gauche de la zone de recherche), avec :

   - Plateforme satellite : `S2A`
   - Numéro de produit : `MSIL1C`
   - Numéro d'orbite relative : `51` => (R051)
   - Faire une restriction par date d'ingestion, sinon les données seront trop nombreuses
   - Chercher dans la liste des résultats `T31TCJ`

