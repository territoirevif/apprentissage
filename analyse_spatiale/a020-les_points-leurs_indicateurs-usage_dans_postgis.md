---
title: "a020. Les points"
subtitle: "Leurs indicateurs. Leur usage dans PostGIS"
author: Marc Le Bihan
keywords:
- point
- pointz
- pointm
- pointzm
---

## 1) Remarque sur l'étude des points

Spatialement parlant, on étudiera pas nécessairement les valeurs des points : parfois, ce sont juste leurs localisations qui nous intéressent.

\bigbreak

   \begingroup \cadreAvantage

   >
   >   - Le lieu d'apparition d'une maladie
   >   - La répartition de certaines espèces d'arbres
   >

   \endgroup

\bigbreak

Dans ce cas, l'analyse spatiale voudra quantifier l'écart entre :

   >   - la distribution que nous observons,
   >   - et une qui serait totalement aléatoire.

$\rightarrow$ si les points paraissent plus regroupés qu'ils ne le seraient aléatoirement, l'on peut commencer à étudier des \definition{clusters}.

---

## 2) Les points et leurs indicateurs

Un \definition{point} a des coordonnés $x$, $y$, [$z$].

## 3) Dans PostGIS

Le \fonction{POINT} est le premier type disponible : il a une coordonnée $(X,Y)$  
   \demitab son pendant, \fonction{POINTZ}, les coordonnées $(X,Y,Z)$

et une catégorie de points, \fonction{POINTM} [respectivement POINTZM], associe à ses coordonnées  
   \demitab \definition{M}, \avantage{une mesure additionnelle, de l'unité que l'on veut}.

   \code
   
   > ```sql
   > -- Crée les types de points vus précédement
   > CREATE TABLE ch02.my_points(
   > 	id serial PRIMARY KEY,
   >     p geometry(POINT),
   >     pz geometry(POINTZ),
   >     pm geometry(POINTM),
   >     pzm geometry(POINTZM),
   >     p_srid geometry(POINT,4269)
   > );
   > 
   > -- et les alimente
   > INSERT INTO ch02.my_points(p, pz, pm, pzm, p_srid)
   > VALUES(
   > 	ST_GeomFromText('POINT(1 -1)'),
   > 	ST_GeomFromText('POINT(1 -1)',4269), -- variante avec SRID
   > 	
   > 	ST_GeomFromText('POINT Z(1 -1 1)'),
   > 	ST_GeomFromText('POINT M(1 -1 1)'),
   > 	ST_GeomFromText('POINT ZM(1 -1 1 1)')
   > );
   ```
   
   \endgroup

\bigbreak

\underline{Attention :} `POINT Z` et `POINTZ` sont acceptés, mais `POINTZ` passe mieux dans les requêtes : lors de définition de colonnes, les espaces peut être refusés: autant ne pas en mettre.

