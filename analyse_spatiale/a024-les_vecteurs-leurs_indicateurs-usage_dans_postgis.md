---
title: "a024. Les vecteurs"
subtitle: "Leurs indicateurs. Leur usage dans Postgis"
author: Marc Le Bihan
keywords:
- vecteur
- indice de compacité
- centre de gravité géométrique
- centre de masse
- cercle circonscrit
- cercle inscrit
- aire
- forme
- indice fractal
---

\bleu{($\bar{x}$, $\bar{y}$)} sont les coordonnées du __centre de gravité géométrique__ ou __centre de masse__ d'un vecteur.

## 1) Les indices de compacité

Les indices de compacités comparent un vecteur à un cercle ou à un carré.

### a) Rapport entre aire d'un objet et son cercle circonscrit
     
   - Rapport de l'aire d'un objet __A__ avec celle de son cercle circonscrit __C__ :  
     $$ \begingroup \large \rouge{S_{A,C}} = \frac{A}{C} \endgroup $$  

### b) Rapport entre l'aire d'un objet et celle d'un cercle de diamètre égal à l'axe majeur L

   - Rapport entre l'aire d'un objet __A__  
     et celle d'un cercle de diamètre égal à la longueur de l'axe majeur __L__ :  
     $$ \begingroup \large \rouge{S_{A,L}} = \frac{A}{\pi(0.5)L^2} = \frac{1.27A}{L^2} \endgroup$$  

### c) Rapport entre le cercle inscrit et celui circonscrit

   - Rapport entre le cercle inscrit __I__ et celui circonscrit, __C__ :  
     $$ \begingroup \large \rouge{S_{I,C}} = \frac{I}{C} \endgroup $$  

### d) Shape

Avec \bleu{$P$} : le périmètre
  $$ \begingroup \large \rouge{\text{Shape}} = G = \frac{P}{\sqrt{\pi A}} \endgroup $$

### e) Indice fractal

  $$ \begingroup \large \rouge{\text{Indice fractal}} = \frac{2\ ln(P)}{ln(A)} \endgroup $$  
  \demitab il varie entre $1$ et $2$ : $2$ est très complexe.


