---
title: "a158. Le Modèle Hydrologique"
subtitle: ""
author: Marc Le Bihan
keywords:
- modèle numérique de terrain (MNT)
- bassin versant
- ruissellement
- pluie
- réseau hydrographique
- flux d'écoulement
- GRID
- TIN
- réseau de drainage
- canal
- modèle hydrologique
---

On utilise les MNT pour modéliser les écoulements et délimiter les __bassins versants__  
ou caractériser le ruissellement des pluies.

\bigbreak

Un \definition{bassin versant} est une zone qui conduit ses substances (exemple : eau) jusqu'à un exutoire qui est le point d'écoulement le plus bas.

On modélise le \definition{réseau hydrographique} sous la forme d'un arbre  
   \demitab dont la base du tronc sera l'exutoire
   \demitab et les branches : les canaux d'écoulement

   > $\rightarrow$ depuis tout point d'un réseau d'écoulement, il est possible d'observer les zones en amont dont les flux arrivent sur lui, pour définir l'aire de drainage.

## 1) Recherche du modèle de flux d'écoulement, à partir d'un GRID

![Le modèle de flux d'écoulement, à partir d'un GRID](images/modèle_flux_écoulement_grid.jpg)

   1. Pour chaque cellule du GRID, la direction est déterminée : on désigne la direction par un chiffre, comme si l'eau pouvait faire "_un mouvement de la Reine sur un échiquier_".

   2. Puis, on regarde vers autre cellule va le flux d'une cellule de GRID.  
   On somme ses trois cellules supérieures (exemple pour attribuer la valeur de la ligne 3, colonne 3, on calcule depuis ligne 2, colonne 2 à 4 : 2 + 4 + 1, et on ajoute +1 encore, pour soi-même (ligne 3, colonne 3), ce qui fait un total de 8)

   Une matrice des poids intervient, ici à 1 1 1 1 :   
   \demitab elle peut représenter la répartition spatiale de la pluie, par exemple.

   3. Puis, l'on applique un __seuil__ à partir duquel on dit qu'il y a effectivement écoulement.

## 2) à partir d'un TIN

Les TIN se prêtent aussi à la recherche des flux d'écoulement, et des réseaux de drainage.

   - Les directions d'écoulement sert à délimiter les bassins versants
   - La zone de drainage, la zone dont les flux convergent vers un point donné.

![Le modèle de flux d'écoulement, à partir d'un TIN](images/modèle_flux_écoulement_tin.jpg)

## 3) Modéliser un bassin versant ou une aire de drainage

Il faut :

   1. Créer un MNT sans dépression  
      il ne doit pas contenir de cuvettes ou de zones d'où aucun flux ne sortirait

   2. Identifier le réseau hydrographique
   3. Délimiter le contour du bassin versant ou de la zone de drainage

\bigbreak

On peut surcreuser le MNT aux endroits où le modèle théorique ne correspond pas aux emplacements du réseau hydrographique naturel, mais :

   \demitab comme la pluie n'est pas homogène
   \demitab et qu'il faut prendre en compte son interception
   \demitab ainsi que l'évapotranspiration
   \demitab et que l'infiltration n'est pas homogène

   \demitab $\rightarrow$ il faut vite avoir recours à des modèles hydrologiques.

