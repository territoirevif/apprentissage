---
title: "a160. Le modèle de Paysage"
subtitle: ""

keywords:
- modèle
- modèle de paysage
- tâche
- corridor
- matrice
- lisière
- noyau
- patch-corridor-matrix
- bordure
- isolement / proximité
- forme
- contraste
- connectivité

author: Marc Le Bihan
---

Le modèle de paysage le décrit par des métriques.

# I) Le paysage : tâches - corridors - matrice

Un \definition{paysage} est composé de trois éléments :

   1. les \definition{tâches} qui constituent les unités fonctionnelles

      - elle a un \definition{noyau} et une \definition{lisière}
      - plus sa forme est complexe, plus la tâche est allongée et plus sa lisière est importante
      - la lisière intéragit avec la \bleu{matrice} de sa tâche voisine
      - alors que le noyau n'est en relation qu'avec la lisière de sa tâche

   2. Les \definition{corridors} sont des unités de forme linéaire

      - il assurent les connexions, les flux entre tâches.

   3. La \definition{matrice} englobe tâches et corridors

Le choix des éléments du modèle tâche-corridor-matrice (en anglais : \bleu{patch-corridor-matrix}) dépend du problème traité : 

   ![tâches-corridors-matrice](images/modele_de_paysage_taches_corridors_matrice.jpg)

   \begingroup \cadreAvantage

   > __Exemple : une étude__
   >
   > - de clairières considèrera les clairières comme des tâches, et la forêt comme matrice  
   > - sur le développement d'îlots forestiers : les îlots comme tâches, les espaces ouverts comme matrice

   \endgroup

# II) Les métriques paysagères

Leur analyse est fréquente. En écologie, par exemple.  

   \begingroup \cadreAvantage

   > structure paysagère d'espèces occupant des zones naturelles (programme européen Bioasses, 2004)
   > 
   >    - pour caractériser leurs habitats
   >    - quantifier la fragmentation des forêts 
   >   
   > \bigbreak 
   > dans son cadre, sont observées les métriques : aire, densité, bordure, forme, noyau, voisinage, diversité, contagion.

   \endgroup

\bigbreak

Il y a huit groupes de métriques paysagères.

## 1) Métriques de tâches et de noyau

### a) Surface/densité/mesure

\underline{Surface de la tâche}

   \bleu{$i$} : index d'une tâche du modèle  
   \bleu{$a_{i}$} : l'aire de cette tâche  
   $$ \rouge{\text{AREA}} = \frac{a_{i}}{10000} \text{\ \ \ \ en hectares} $$

\bigbreak

\underline{Périmètre de la tâche}
   $$ \rouge{\text{PERIM}} = p_{i} \text{\ \ \ \ en mètres} $$

### b) Forme

\underline{Rapport périmètre/surface}
   $$ \rouge{\text{LSI}} = \frac{p_{i}}{a_{i}} $$

### c) Noyau (épaisseur de la bordure choisie par l'utilisateur)

\underline{Surface du noyau}

   \bleu{$a^{c}_{i}$} : surface du noyau de la tâche
   $$ \rouge{\text{CORE}} = \frac{a_{i}}{10000} \text{\ \ \ \ en hectares} $$

### d) Isolement/proximité

\underline{Distance euclidienne du plus proche voisin entre deux tâches}
   $$ \rouge{\text{ENN}} = \mauve{h_{i,j}} $$

### e) Contraste

\underline{Contraste de bordure}

   \bleu{$p_{i}$}\  : le périmètre de la tâche $i$  
   \bleu{$p_{i,k}$} : la longueur de la bordure tâche $i$ adjacente à la tâche $k$  
   \bleu{$d_{i,k}$} : dissimilarité entre les catégories de la tâche $i$ et la tâche $k$
   $$ \rouge{\text{ECON}} = \frac{\sum\limits_{k=1}^{m}p_{i,k}.d_{i,k}}{p_{i}} \times 100 $$

## 2) Métriques de classes

### a) Surface/densité/mesure

\underline{Pourcentage du paysage occupé par une classe de tâches}

   $\bleu{a_{i,j}}$ : surface de la tâche $j$ appartenant à la classe $i$  
   $\bleu{A}$ : surface totale du paysage ($m^{2}$)
   $$ \rouge{\text{PLAND}} = \frac{\sum\limits_{j=1}^{n}a_{i,j}}{A} \times 100 $$

### b) Forme

\underline{Indice de forme}

   $\bleu{e_{i,j}}$ : longueur de la bordure de la tâche $j$ appartenant à la classe $j$  
   $\bleu{T}$ : surface totale des tâches
   $$ \rouge{\text{LSI}} = \frac{0.25 \times \sum\limits_{j=1}^{n}e_{i,j}}{\sqrt{T}} $$

### c) Noyau (épaisseur de la bordure choisie par l'utilisateur)

\underline{Pourcentage du paysage occupé par les noyaux une classe de tâches}

   $\bleu{a_{i,j}^{c}}$ : surface du noyau de la tâche $j$ appartenant à la classe $i$  
   $\bleu{A}$ : surface totale du paysage ($m^{2}$)
   $$ \rouge{\text{CPLAND}} = \frac{\sum\limits_{j=1}^{n}a_{i,j}^{c}}{A} \times 100 $$

### d) Contraste

\underline{Contraste de la bordure de classe pondérée par la surface}

   \bleu{$e_{i,k}$}\  : longueur de la bordure de la tâche $i$ adjacente à la tâche $k$  
   \bleu{$d_{i,k}$} : dissimilarité entre les catégories de la tâche $i$ et la tâche $k$  
   \bleu{$A$} : surface totale du paysage ($m^{2}$)
   $$ \rouge{\text{CWED}} = \frac{\sum\limits_{k=1}^{m}e_{i,k}.d_{i,k}}{A} \times 10000 $$

### e) Agrégation/subdivision

\underline{Division du paysage}

   $\bleu{a_{i,j}}$ : surface de la tâche $j$ appartenant à la classe $i$  
   $\bleu{A}$ : surface totale du paysage ($m^{2}$)
   $$ \rouge{\text{DIVISION}} = 1 - \sum\limits_{j=1}^{n}\big(\frac{a_{i,j}}{A}\big)^{2} $$

### f) Connectivité

\underline{Connexion entre deux tâches d'une même classe}

Deux classes sont considérées comme connectées si la distance qui sépare leur centre est inférieure à une valeur définie par l'utilisateur.

   $\bleu{c_{i,j,k}}$ : __0__ = disjoint, __1__ = joint  
   $\bleu{n_{i}}$ : le nombre de tâches de la classe $i$
   $$ \rouge{\text{CONNECT}} = \frac{\sum\limits_{j \neq k}^{n}c_{i,j,k}}{\frac{n_{i}(n_{i} - 1)}{2}} \times 100 $$

## 3) Métriques du paysage

\underline{Nombre de tâches du paysage}

   $\bleu{n_{i}}$ : le nombre de tâches de la classe $i$
   $$ \rouge{\text{NP}} = \sum\limits_{i=1}^{n}n_{i} $$

\bigbreak

\underline{Densité de tâches du paysage}

   $\bleu{n_{i}}$ : le nombre de tâches de la classe $i$  
   $\bleu{A}$ : surface totale du paysage, en $m^{2}$.
   $$ \rouge{\text{PD}} = \frac{\sum\limits_{i=1}^{n}n_{i}}{A} \times 100 $$

# III) Distribution statistique de métriques

\tab Additionner des métriques pour en faire des métriques de classes ou de paysage est possible, mais la moyenne, médiane, écart-type et coéfficient de variation sont parfois plus faciles à interpréter.

Comme la moyenne pondérée par la surface, qui est très adaptée à toutes les métriques qui se réfèrent à une surface.

## 1) Métriques de classes résumant des métriques de tâches

\underline{Moyenne de métrique}

   \bleu{métrique} : une métrique parmi celles du chapitre précédent  
   $\bleu{X_{i,j}}$ : valeur de métrique de la tâche $j$ appartenant à la classe $i$  
   $\bleu{n_{i}}$ : le nombre de tâches de la classe $i$
   $$ \rouge{\text{MN}} = \frac{\sum\limits_{j=1}^{n}X_{i,j}}{n_{i}} $$

\bigbreak

\underline{Moyenne pondérée par la surface}

   \bleu{métrique} : une métrique parmi celles du chapitre précédent  
   $\bleu{a_{i,j}}$ : surface de la tâche $j$ appartenant à la classe $i$  
   $\bleu{X_{i,j}}$ : valeur de métrique de la tâche $j$ appartenant à la classe $i$  
   $\bleu{n_{i}}$ : le nombre de tâches de la classe $i$
   $$ \rouge{\text{AM}} = \sum\limits_{j=1}^{n}X_{i,j} \frac{a_{i,j}}{\sum\limits_{j=1}^{n}a_{i,j}} $$

## 2) Métriques de paysage résumant des métriques de tâches

\underline{Moyenne de métrique}

   \bleu{métrique} : une métrique parmi celles du chapitre précédent  
   $\bleu{X_{i,j}}$ : valeur de métrique de la tâche $j$ appartenant à la classe $i$  
   $\bleu{n_{i}}$ : le nombre de tâches de la classe $i$  
   $\gris{m}$ : ?
   $$ \rouge{\text{MN}} = \frac{\sum\limits_{j=1}^{m} \sum\limits_{j=1}^{n}X_{i,j}}{n_{i}} $$

\bigbreak

\underline{Moyenne pondérée par la surface}

   \bleu{métrique} : une métrique parmi celles du chapitre précédent  
   $\bleu{a_{i,j}}$ : surface de la tâche $j$ appartenant à la classe $i$  
   $\bleu{X_{i,j}}$ : valeur de métrique de la tâche $j$ appartenant à la classe $i$  
   $\bleu{n_{i}}$ : le nombre de tâches de la classe $i$  
   $\gris{m}$ : ?
   $$ \rouge{\text{AM}} = \sum\limits_{j=1}^{m} \sum\limits_{j=1}^{n}X_{i,j} \frac{a_{i,j}}{\sum\limits_{j=1}^{n}a_{i,j}} $$

# IV) Les protocoles d'échantillonnage

Nous devons choisir une zone géographique où calculer nos métriques. C'est une \definition{zone d'échantillonnage}.  
Elle peut être :

   - circonscrite à la zone d'étude
   - calée sur une variable mesurée, comme l'abondance d'une espèce
   - définie d'après une structure spatiale pré-existante :

      - bassin versant ou zone de drainage
      - subdivision ville/campagne
      - toute partition _à priori_ de l'espace
      - dictée par un protocole d'échantillonnage, comme l'outil \donnee{GRASS} le propose

      ![les protocoles d'échantillonnage de GRASS](images/grass_protocoles_echantillonnage.jpg)

## 6) Agrégation/subdivision

## 7) Connectivité

## 8) Diversité

---

# Bibliographie et références

@auda
