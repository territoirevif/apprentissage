#!/bin/bash
export source=$1
export index=$2
export entree=$3

# Le paramètre source doit être alimenté
if [ -z "$source" ]; then
   echo "Le nom du fichier pdf à indexer dans Elastic est attendu en paramètre." >&2
   exit 1
fi

# Si le fichier source n'a pas d'extension, lui rajouter celle .pdf
if [[ "$source" != *"."* ]]; then
   source=$source.pdf
fi

# Il doit avoir l'extension pdf
if [[ "$source" != *".pdf" ]]; then
   echo "Le fichier à indexer dans Elastic doit avoir l'extension .pdf" >&2
   exit 1
fi

# Si l'index n'est pas alimenté, lui assigner apprentissage
if [ -z "$index" ]; then
  index=apprentissage
fi

# Si l'entrée n'est pas alimentée, lui assigner le nom du fichier sans extension
if [ -z "$entree" ]; then
  entree=$(basename "${source%.*}")
fi

host="http://localhost:9200"
user="elastic"
pwd="Ox-JoQT0po=pdOT-LeE*"

json_file=$(mktemp)
cur_url="$host/$index/_doc/$entree?pipeline=attachment"

echo '{"data"  : "'"$( base64 "$source" -w 0    )"'"}' >"$json_file"
# echo "transfert via $json_file vers $cur_url"

if ! ingest=$(curl -s -X PUT -H "Content-Type: application/json" -u "$user:$pwd" -d "@$json_file" "$cur_url"); then
  echo "Echec de l'ingestion dans Elastic de $source dans l'index $index, entrée $entree : $ingest" >&2
  exit $?
fi

rm "$json_file"
echo "$source indexé dans l'entrée $entree de l'index $index d'Elastic"
