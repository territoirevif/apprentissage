---
title: "m350. Divisiblité et congruences"
subtitle: ""
author: Marc Le Bihan
keywords:
- congruence
- congru
- modulo
- reste
- divisibilité
- résidu
- classe
- reste
- addition
- soustraction
- élévation à la puissance
- multiple
- divisibilité par 2
- divisibilité par 4
- divisibilité par 5
- divisibilité par 3
- divisibilité par 9
- divisibilité par 6
---

Dire que l'entier $\mauve{a}$ est \definition{congru} à l'entier $\mauve{b}\ \donnee{modulo}\ \mauve{n}$,  
\tab signifie que $\mauve{a}$ et $\mauve{b}$ ont \definition{le même reste}  
\tab dans leur \definition{division par l'entier} \donnee{n}.

### Ecriture

   > $a \equiv b\ (mod\ n)$  
   > $a \equiv b\ [n]$  
   > $a \underset{n}\equiv b$

\bigbreak

Les entiers \textbf{congrus} à $\mauve{r\ modulo\ n}$ constituent une \definition{classe de congruence}

   > $\mauve{r}$, le reste, est appelé \definition{le résidu}.  
   > et chaque $r$ produit une classe.

# I) Additions, soustractions, élévation à la puissance

Soient $\bleu{a \underset{n}\equiv b}$ et $\mauve{c \underset{n}\equiv d}$,  
(avec par exemple : $a = 7, b = 2, c = 18\text{ et }d = 3$. et $n = 5$).

  > $\bleu{(a - b)}$ et $\mauve{(c - d)}$ sont des multiples de \textbf{n}  
  > \gris{$(7 - 2)$ et $(18 - 3)$ sont des multiples de 5}

  > $a + c \underset{n}\equiv b + d$  
  > \gris{$7 + 18 \underset{5}\equiv 2 + 3 \iff 25 \underset{5}\equiv 5$}

  > $a - c \underset{n}\equiv b - d$  
  > \gris{$7 - 18 \underset{5}\equiv 2 - 3 \iff -11 \underset{5}\equiv -1$, qui ont tout deux pour reste : 4 quand divisés par 5}

  > $a . c \underset{n}\equiv b . d$  
  > \gris{$7 \times 18 \underset{5}\equiv 2 \times 3 \iff 126 \underset{5}\equiv 6$, qui ont tout deux pour reste : 1 quand divisés par 5}

  > $a^{k} \underset{n}\equiv b^{k}$ \tab avec $\mauve{k}$ entier positif  
  > \gris{$7^{3} \underset{5}\equiv 2^{3} \iff 343 \underset{5}\equiv 8$, qui ont tout deux pour reste : 3 quand divisés par 5}
   

# II) Cherche d'une congruence d'un nombre élevé à la puissance

Quel est le reste de $\mauve{178^{13}}$ divisé par \rouge{11} ?

   1. On cherche d'abord à quoi 178 est congru, modulo \rouge{11} :

   > $\mauve{178} = \rouge{11} \times 16 + \bleu{2}$ \tab il est congru à \bleu{2}.

\bigbreak

   2. On cherche maintenant un $\bleu{2}^{p}$, en augmentant $p$ jusqu'à ce qu'on ait $\bleu{2}^p \underset{\rouge{11}}\equiv 1$ ou $\bleu{2}^p \underset{\rouge{11}}\equiv -1$ :

  > $\bleu{2}^1 \underset{\rouge{11}}\equiv 2$,  
  > $\bleu{2}^2 \underset{\rouge{11}}\equiv 4$,  
  > $\bleu{2}^3 \underset{\rouge{11}}\equiv 8$,  
  > et enfin $\bleu{2}^5 \underset{\rouge{11}}\equiv 10 \underset{\rouge{11}}\equiv -1$

\bigbreak

  3. On exprime $\bleu{2}^{\mauve{13}}$ à partir des puissances de \bleu{2} obtenues :

  >  $\bleu{2}^{\mauve{13}} = (2^{5})^{\rouge{2}} \times 2^{3}$  
  > \gris{(l'élévation à la puissance d'une puissance la multiplie, sa multiplication, l'ajoute)}

\bigbreak

  4. Et on remplace les facteurs ($2^5$ et $2^3$), mais pas les puissances $(...)^{2}$, par leurs congruences :

  > $2^{13} = (-1)^{\rouge{2}} \times 8 = 8$

\bigbreak \begingroup \large
$\to \mathbf{178^{13} \underset{11}\equiv 8}$
\endgroup

# III) Critères de divisibilité dans le système décimal

\underline{Le principe :}

Soustraire d'un nombre à tester plusieurs multiples du diviseur.

   > Exemple : $28735$ est divisible par $7$ parce que $(28000 + 700 + 35)$ le sont chacun.

## 1) Divisiblité par 2 et par 5 

Un entier dont le chiffre des unités se termine par 0,2,4,6 ou 8 est divisible par 2.  
Un entier dont le chiffre des unités se termine par 0 ou 5 est divisible par 5.

   > Comme 10 est multiple de 2 et de 5,  
   > tout entier est congru à son chiffre des unités modulo 2 et modulo 5  
   > \gris{c'est à dire que divisés par 2 (ou divisés par 5) tout nombre va donner le même reste}

   >  > $8526 = 852 \times 10 + 6 = 852 \times 5 \times 2 + 6$  
   >  > 6 est multiple de 2, mais n'est pas multiple de 5  
   >  > il en est de même pour 8526.

## 2) Divisiblité par 4

Un entier est divisible par 4 si le nombre formé ses deux derniers chiffres l'est.  
Un entier est divisible par 25 si le nombre formé par ses deux derniers chiffre l'est : 25, 50, 75 ou 00.

   > La division de l'entier $N$ à tester par 100 s'écrit :  
   > \demitab $\mauve{N = 100c + r}$ avec $\mauve{r \in [0, 100[}$ (c'est le reste)  

   >  > $100$ est un multiple de $4$ et de $25$, $100c$ l'est donc aussi,  
   >  > donc tester la divisibilité de $N$ revient à tester celle de $r$  

   >  > (puisqu'on peut tester un nombre en soustrayant plusieurs parties de lui : 
   >  >
   >  >  \demitab 1. on a soustrait 100c, on a vu que ça marchait (forcément...),
   >  >  \demitab 2. et il ne reste plus qu'à tester $r$).

## 3) Divisibilité par 3 et par 9

Tous les nombres $10^n - 1$ (avec $n > 1$) sont divisibles par 9, parce que ce sont des séries de 9...

   > par là, $10^n \underset{9}\equiv 1$

Si l'on prend un nombre, par exemple $384267$,

   > Il se décompose en puissances de 10 :  
   > \demitab $3.10^5 + 8.10^4 + 4.10^3 + 2.10^2 + 6.10^1 + 7.10^8$  

et chaque puissance de 10 a pour modulo 1, comme dit plus haut. Alors :

   > $384267 \underset{9}\equiv 3 + 8 + 4 + 2 + 6 + 7 = 30 \underset{9}\equiv 3$  
   > \gris{(il n'est pas divisible par 9)}.

Un nombre est divisible par 9 si la somme de ses chiffres l'est,  
et le reste de sa division est la somme de ses chiffres divisés par 9.

![Preuve par neuf](images/preuve_par_neuf.jpg){width=60%}

Même principe pour la division par 3 :

   - si la somme de ses chiffres est divisible par 3, le nombre l'est,
   - et son reste est celui de la somme de ses chiffres divisés par 3.

## 4) Divisibilité par 6

Un entieer est multiple de 6 s'il est divisible par 2 et par 3.

## 5) Divisibilité par 11

Quel que soit la puissance de 10, sa division par 11 a pour reste 10 ou 1 ;

   > $\frac{10}{11} \equiv -1 = 10$

   > $\frac{10^2}{11} \equiv 1$  

   > $\frac{10^3}{11} \equiv -1 = 10$

   > $\frac{10^4}{11} \equiv 1$  
   > ...

Les puissances __paires__ sont congrues à __+1__  
et les puissances __impaires__, à __-1__.

