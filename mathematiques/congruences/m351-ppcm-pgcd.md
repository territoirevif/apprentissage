---
title: "m351. Plus Grand Commun Divisieur (PGCD), Plus Petit Commun Multiple (PPCM)"
subtitle: ""
author: Marc Le Bihan
keywords:
- pgcd
- plus grand commun diviseur
- ppcm
- plus petit commun multiple
- diviseur
- multiple
- nombre premier
- partie commune
- facteur commun
---

## Le Plus Grand Commun Diviseur (PGCD)

Le \definition{Plus Grand Commun Diviseur} de \bleu{$a$} et de \bleu{$b$}  
est le __plus grand__ nombre avec lequel on peut diviser $a$ et $b$.

   \begingroup \cadreGris

   > Les \underline{diviseurs} de 24 : $\text{Div}(24) = \{\mauve{1,2,3,4,6},8,\donnee{12},24\}$  
   > Les diviseurs de 36 : $\text{Div}(36) = \{\mauve{1,2,3,4,6},9,\donnee{12},18,36\}$  
   > dans notre cas, c'est 12.  
   > \newline
   > ou bien l'on peut aussi les mettre sous forme de puissances de nombres premiers,  
   > et prendre la partie commune : les facteurs qui se répètent avec la plus petite puissance :  
   > \tab $24 = 2^3 \times 3$  
   > \tab $36 = 2^2 \times 3^2$  
   > et la partie commune, c'est $2^2 \times 3 = 12$ 

   \endgroup

## Le Plus Petit Commun Multiple (PPCM)

Le \definition{Plus Petit Commun Multiple} de \bleu{$a$} et de \bleu{$b$}  
est le __plus petit__ nombre qui est à la fois multiple de $a$ et de $b$.

   \begingroup \cadreGris

   > Les \underline{multiples} de 24 : $\text{Mult}(24) = \{0,24,\donnee{72},96,120...\}$  
   > Les multiples de 36 : $\text{Mult}(36) = \{0,36,\donnee{72},144,180...\}$  
   > dans notre cas, c'est 72.  
   > \newline
   > ou bien l'on peut aussi les mettre sous forme de puissances de nombres premiers,  
   > et prendre tous les facteurs communs __ou non__ elevés à la plus grande puissance :  
   > \tab $24 = 2^3 \times 3$  
   > \tab $32 = 2^5$  
   > \tab $42 = 2 \times 3 \times 7$  
   > et la partie commune, c'est $2^5 \times 3 \times 7 = 672$ 

   \endgroup


