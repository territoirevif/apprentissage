---
header-includes:
  \input{apprentissage-header-include}
  \input{apprentissage-include}

title: "Mathématiques (MVA005)  \nCalcul différentiel et intégral"
subtitle: ""

category: mathematiques

author: Marc Le Bihan
geometry: margin=2cm
fontsize: 12pt
output: pdf
classoption: fleqn
urlcolor: blue
---

   1. Dans pdf : résumé cours en `category:`, `abstract:` et `subject:`
   2. Réalisation exercices de cours
   3. Recopie exercices des cahier dans pdf
   4. Recherche et réalisation annales
   5. Fiche A4

## 1) Suites numériques

   - Séries
   - Suites numériques, arithmétiques : m150
   - Suites géométriques : m151
   - Suites monotones
   - Suites convergentes

      -  Limite d'une suite

   - Théorème du point fixe
   
## 2) Fonctions réelles d'une variable réelle

   - Limite, continuité
   - Fonction réciproque

     - notamment  Arcsin, Arccos, Arctan

   - Dérivabilité

       - Théorème de Rolle
       - Accroissements finis

   - Formule de Taylor

       - Développements limités
       - équivalents de fonctions
       - Etude asymptotique

   - Fonctions usuelles

       - exponentielle
       - logarithme
       - puissance
       - trigonométrie hyperbolique

## 3) Nombres complexes

   - Représentation cartésienne

     - Calculs sur les complexes

   - Représentation géométrique

     - forme trigonométrique

   - Exponentielle complexe

## 4) Polynômes et fractions rationnelles

   Les polynômes sont une catégorie de fonctions

   - Racines d'un polynôme

       - multiplicités

   - Décomposition en éléments simples des fractions rationnelles simples
     
## 5) Calcul intégral

   - Intégrale d'une fonction continue

      - primitive d'une fonction continue
      - développement limités + fractions rationnelles

   - Calcul des intégrales et primitives classiques
   - Intégration par parties
   - Intégration par changement de variable

## 6) Equations différentielles

   - Équations du premier ordre (à une seule variable) Ordinaires (EDO)

      - Problème de Cauchy

   - Equations aux dérivées partielles (EDP) (à plusieurs variables)

   - Résolution des équations différentielles linéaires du premier ordre
   - Résolution des équations différentielles linéaires du deuxième ordre à coefficients constants

      - Méthode des combinaisons

   - Méthode de la variation de la constante

