---
title: "m200. Projeter des points sur une droite"
subtitle: ""
author: Marc Le Bihan
---

__Projeter orthogonalement des points sur une droite__   
\ \ \ \ \ \ \bleu{$A(1, 0, 2)$} et \bleu{$B(-1, 2, 1)$} : deux points qui définissent la droite (AB)  
\ \ \ \ \ \ \bleu{$C(0, 1, -2)$} : le point qu'on veut projeter sur la droite (AB) en un point \rouge{H}.  
\   
   1. La droite (AB) se représente paramétriquement par son point \bleu{$A(x_{A}, y_{A}, z_{A})$} et un __vecteur directeur__ \mauve{$\vec{u}$}, avec :  
   \ \ \ \ \ \ \mauve{$\vec{u}(a,b,c)$} : le vecteur directeur de (AB)  
   \ \ \ \ \ \ \mauve{$t$} : un nombre réel qui va rester sous forme de variable pour le moment
   $$ \left\{ \begin{array}{ll}
      x = x_{a} + a\mauve{t} \\
      y = y_{a} + b\mauve{t} \\
      z = z_{a} + c\mauve{t}
   \end{array} \right. $$

   Le vecteur directeur de notre droite, c'est $\mauve{\overrightarrow{\text{AB}}} = B - A = \overrightarrow{\text{AB}}(-2, 2, -1)$  
   Appliqué avec les coordonnées du point $A$ et celles correspondantes de $\overrightarrow{\text{AB}}$, cela donne :
   $$ \left\{ \begin{array}{ll}
      x = 1 - 2t \\
      y = 2t \\
      z = 2 - t
   \end{array} \right. $$
\   
   2. Comme le point projeté $\rouge{H}$ que nous recherchons est sur la droite (AB), il vérifie ce système.  

  ![Projection sur une droite ](images/projection_sur_une_droite.png){width=25%}  
  c'est à dire qu'on aura H sous la forme : $\text{H}(1-2t, 2t, 2-t)$.  
\   
  3. On projete le point C en H sur la droite (AB).  
  de fait, (AB) et [CH], avec $\mauve{\overrightarrow{\text{CH}}} = H - C =  \overrightarrow{\text{CH}}(1-2t, 2t-1, 4-t)$, sont orthogonaux.  
  $\implies$ __leur produit scalaire est égal à zéro__ : $\overrightarrow{\text{CH}}.\overrightarrow{\text{AB}} = 0$  
  on va donc faire la somme des produits des coordonnées respectives de $\overrightarrow{\text{CH}}$ et $\overrightarrow{\text{AB}}$ :
  $$ -2(1-2t) + 2(2t-1) - (4-t) = 0 \iff \mauve{t} = \frac{-8}{9} \implies \rouge{\text{H}\bigg(\frac{-7}{9}, \frac{16}{9}, \frac{10}{9}\bigg)} $$

![Un exemple de projection](images/projections_points_sur_droite.jpg){width=65%}  
_Une projection sur une droite quelconque (ne maximise pas les distances entre points projetés)_

