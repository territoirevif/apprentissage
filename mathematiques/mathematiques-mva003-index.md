---
header-includes:
  \input{apprentissage-header-include}
  \input{apprentissage-include}

title: "Mathématiques (MVA003)  \nOutils mathématiques pour l'informatique"
subtitle: "Combinatoire, probabilités, ordre, calcul booléen"

category: mathematiques

author: Marc Le Bihan
geometry: margin=2cm
fontsize: 12pt
output: pdf
classoption: fleqn
urlcolor: blue
---

## 1) Généralités

   - Ensembles

      - éléments
      - parties d'un ensemble
      - fonctions
      - opérations sur les ensembles

## 2) Dénombrements

   - Cardinal d'un ensemble
 
      - ensemble fini
      - ensemble dénombrable

   - Arrangements

     - combinaisons
     - permutations
     - formule du binôme

## 3) Probabilités combinatoires

   - Épreuves

     - événements
     - lois de probabilité
     - probabilités conditionnelles
     - indépendance
     - essais répétés

## 4) Relations

   - Relation d'équivalence
   - Relation d'ordre

      - diagramme de Hasse
      - éléments maximaux, minimaux
      - plus grand et plus petit élément

## 5) Calculs booléens

   - Treillis

     - algèbre de Boole
     - théorème de Stone

   - Fonctions booléennes

     - forme canonique disjonctive

   - Systèmes d'équations booléennes

   - Synthèse : 
      - chaînes de contacts
      - portes

   - Simplification des formules

      - méthode de Karnaugh
      - méthode des consensus

## 6) Arithmétique

   - Division euclidienne

     - nombres premiers
     - PGCD
     - PPCM
     - identité de Bézout

## 7) Logique

   - [Logique des propositions (m091)](./mathematiques-m091-la-logique-des-propositions.pdf)

      - raisonnement par contraposition : dans [ce cours](./mathematiques-m091-la-logique-des-propositions.pdf#contraposition), dans [celui des raisonnements](./mathematiques-m093-les-raisonnements.pdf#contraposition)
      - [raisonnement par l'absurde (m093)](./mathematiques-m093-les-raisonnements.pdf#absurde)

   - Calcul propositionnel
   - Propositions

     - connecteurs
     - formes propositionnelles

   - Prédicats

     - quantificateurs

   - Récurrences

     - définitions récursives

