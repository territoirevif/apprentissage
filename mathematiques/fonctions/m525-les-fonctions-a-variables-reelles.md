---
title: "m525. Les fonctions à variables réelles"
subtitle: ""
author: Marc Le Bihan
---

## 1) Les fonctions de référence

Les fonctions de référence sont :

  - la fonction constante
  - l'identité $\bleu{f(x) = x}$
  - racine carrée
  - valeur absolue
  - exponentielle
  - logarithme
  - sinus, cosinus, tangente, arcsinus, arccosinus

## 2) Les opérations sur fonctions

Les opérations sur fonctions :

  - $\bleu{f + g}$
  - $\bleu{f \times g}$

  - $\bleu{\frac{f}{g}}$, à la condition que $g(x) \neq 0$

## 3) Domaine de définition

## 4) Courbes représentatives

Il y a autant de pentes que de degrés : dans une fonction d'ordre 3, il y a 3 pentes.

## 5) Fonctions [strictement] croissantes, [strictement] décroissantes

