---
title: "m346. Image directe et image réciproque"
subtitle: ""
category: mathématiques
author: Marc Le Bihan
keywords:
- MVA003
- ensemble
- fonction
- application
- image directe
- image réciproque
- ensemble de départ
- ensemble d'arrivée
---

## 1) L'image directe

L'image directe d'une partie de E ce sont les images des élements de E.  

   > Soit __A__ une partie de __E__.  
   > on appelle __image (directe)__ de A par $f$,  
   > le sous-ensemble de __F__ noté \definition{$\mathbf{f(A)}$} (ou $f$\<A\>), défini par :
   > $$
   > f(A) : \{f(x), x \in A\} \\
   > $$
   > ou bien
   > $$
   > f(A) : \{y \in F, \exists x \in A, y = f(x)\} \\
   > $$

Ou encore, pour $f$ définie sur E, $Im(f) = f(E)$ : l'image de $f$ c'est $f(E)$

$A \subset B \implies f(A) \subset f(B)$  
_si A est un sous ensemble de B, alors les images de A seront aussi un sous-ensemble de celles de B._

$f(A \cup B)$ \avantage{=} $f(A) \cup f(B)$  
_L'union de A et de B a pour image la réunion des images de A et des images de B._

\attention{mais}  

$f(A \cap B)$ \danger{$\mathbf{\subset}$} $f(A) \cap f(B)$  
_L'intersection d'ensembles de départ a pour image un ensemble d'arrivée \attention{\underline{inclu}} dans l'intersection de chacun des ensembles d'arrivées de ces ensembles de départ._

\underline{Remarques :} 

   - si l'on voulait être bien stricts dans les écritures, si l'on a $f(2) = 4$ par exemple, il faudrait écrire sous forme d'ensembles : $f(\{2\}) = \{4\}$.
   
   - $f(\varnothing) = \varnothing$

## 2) L'image réciproque

L'image réciproque d'une partie de F ce sont les antécédents des éléments de F.

   > Soit B une partie de $F$, on appelle \definition{image réciproque} de $B$ par $f$, le sous-ensemble noté $f^{-1}(B)$, défini par :
$$ f^{-1}(B) = \{x \in E, f(x) \in B\} $$
(_tous les éléments de l'esemble de départ qui ont se sont trouvé une image dans l'ensemble d'arrivée)._

   > $f^{-1}(\varnothing) = \varnothing$  
   > $f^{-1}(F) = E$ : de l'ensemble d'arrivée on retrouve l'ensemble de départ, __mais :__
   > 
   > $f^{-1}\{(y\}) = \{x \in E, f(x) = y\}$ : pour un $y$, ce sont \attention{tous} les $x$ de l'ensemble de départ qui ont su mener à cet $y$. \attention{Il n'y en a pas nécessairement qu'un seul !}

   - $A \subset B \implies f^{-1}(A) \subset f^{-1}(B)$ : si A est inclu dans B, alors l'ensemble de départ de A est inclu dans celui de B.
   - $f^{-1}(A \cup B) = f^{-1}(A) \cup f^{-1}(B)$ : les antécédents de la réunion de deux ensembles d'arrivée, c'est la réunion des deux ensembles de départ.  
   - $f^{-1}(A \cap B) = f^{-1}(A) \cap f^{-1}(B)$ : les antécédents d'une intersection de deux ensembles d'arrivée, viennent de l'intersection de leurs ensembles sources respectifs.

