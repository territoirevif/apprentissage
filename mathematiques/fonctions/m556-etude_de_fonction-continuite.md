---
title: "m556. Etude de fonction"
subtitle: "Continuité"
author: Marc Le Bihan
keywords: 
- mathématiques
- continuité
---

## 1) Continuité en un point

Une fonction est continue au point $\bleu{a}$
 
   \demitab si $\mauve{y}$ s'approche de $\bleu{b = f(a)}$   
      \tab quand $\mauve{x}$ s'approche de $\bleu{a}$.
$$ \forall \varepsilon > 0, \exists \tau(x,a \in \tau) \implies |f(a) - f(x)| \leq \varepsilon) $$

On peut imposer à l'intervalle d'être \definition{symétrique} autour de $\bleu{a}$  
   \demitab en le choisissant de la forme : $]a - h, a + h[$

## 2) Les fonctions continues

Ces fonctions sont continues sur tout leur domaine de définition : 

   - cosinus
   - sinus
   - constantes
   - identité
   - exponentielles
   - logarithmes

\bigbreak

Les fonctions issues d'opérations sur fonctions ou compositions sont continues :

   - $f + g$
   - $f \times g$
   - $\frac{f}{g}$
   - $f \circ g$

