---
title: "m340. Correspondances, fonctions, applications"
subtitle: "ensemble de définition, images..."
category: mathématiques
author: Marc Le Bihan
keywords:
- MVA003
- ensemble
- correspondance
- fonction
- application
- image
- image directe
- image réciproque
- ensemble de définition
- ensemble de départ
- ensemble d'arrivée
- antécédent
---

## Les ensembles sont liés entre-eux par des correspondances

Une correspondance va relier :

   - \textbf{des} éléments de l'ensembles de départ 
   - à \textbf{des} éléments de l'ensemble d'arrivée.

   \begingroup \cadreGris

   > \underline{Définition :} \newline
   > 
   > Soit $\mathbf{E}$ et $\mathbf{F}$ deux ensembles,  
   > \ \ \ on appelle \definition{correspondance} tout triplet \mauve{$(E, F, \Gamma)$}  
   > \ \ \ où \mauve{$\Gamma$} (gamma) est une __partie__ du \definition{produit cartésien $\mauve{E \times F}$}.  
   > \  
   >
   > L'ensemble $\mathbf{\Gamma}$ est appelé le \definition{graphe de la correspondance}.

   \endgroup

## Une fonction fait communiquer des ensembles

Les ensembles communiquent entre-eux à l'aide de \mauve{fonctions}.

   - certaines fonctions sont définies par des \definition{formules} qui disent le calcul à effectuer,
   - d'autres n'ont que leur courbe représentative : $\mauve{G = f(t)}$, par exemple.

   \begingroup \cadreGris

   > \bleu{$A$} : ensemble de départ __source__  
   > \bleu{$B$} : ensemble d'arrivée, __but__  
   > \   
   > $f : A \to B$

   \endgroup

## Ensemble de définition

Le sous-ensemble __A__ formé des éléments $\mathbf{x}$ auxquels sont associés un élément de __B__ s'appelle l'\definition{ensemble de définition},
 
et une \definition{application} est encore plus stricte qu'une fonction : son domaine de définition est __A__ tout entier. C'est à dire qu'elle est partout définie.

et l'élément associé à $x$ par \mauve{$f$} s'appelle l'\definition{image} de $x$.

# I) Correspondance, fonction, application

Une \definition{application} \avantage{définit les correspondances entre les éléments de \underline{deux} ensembles}

   - L'ensemble de départ
   - L'ensemble d'arrivée

## 1) Les correspondances

Les applications et les \definition{fonctions} sont des types de correspondances, où de chaque élément de E \avantage{il part au maximum une seule flèche}.

   > \underline{Exemple :} si sur un graphe sont présentes deux courbes telles qu'en certains $x$ on peut trouver deux valeurs de $y$, alors ça n'est ni une application ni une fonction que l'on voit, mais une correspondance.

## 2) Les fonctions

Une \definition{fonction} est une correspondance $(E, F, \Gamma)$ vérifiant :

   > $\forall x \in E, \forall\ y, y' \in F$ \ \ \ \ \gris{(pour tout $x$ de E, pour tous $y$ et $y'$ de F)}  
   > $(x,y) \in \Gamma$ et $(x,y') \in \Gamma \implies y = y'$  
   > \gris{(deux éléments qui ont le même $x$ auront forcément le même $y$)}

C'est à dire : dans une fonction, un élément de l'ensemble de départ ne peut être relié qu'a un seul élément de l'emsemble d'arrivée.

   > Soit $f$ une application $(E,F,\Gamma)$ et soit $x \in E$,  
   > \ \ \ s'il existe $y \in F$  
   > \ \ \ \ \ \ on dit que \definition{$\mathbf{f}$} est \definition{définie} en \definition{$\mathbf{x}$},  
   > \ \ \ \ \ \ et que \definition{$\mathbf{y}$} est l'\definition{image} de $x$, que l'on note \definition{$\mathbf{f(x)}$}.

inversement,

   > Si $y$ est l'image de $x$,  
   > \ \ \ on dit que \definition{$\mathbf{x}$} est l'\definition{antécédent} de \definition{$\mathbf{y}$}.

$E \to F$

   > L'ensemble __E__ est appelé \definition{source}  
   > celui __F__, appelé \definition{but}

Deux fonctions sont dites __égales__ si elles ont :

   - Le même ensemble de départ
   - Le même ensemble d'arrivée
   - Le même graphe.

__\underline{L'ensemble de définition d'une fonction :}__

\definition{$\mathbf{D_{f}}$} est l'\definition{ensemble de définition} de $\mathbf{f}$ : l'ensemble des éléments $x$ de E qui ont une image dans F. C'est donc un sous-ensemble de E.

## 3) Les applications

Si $\mathbf{E = D_{f}}$, alors on dit que \avantage{$\mathbf{f}$ est une application}.  
(à l'opposé d'une fonction, où des éléments de l'ensemble de départ peuvent n'être pas reliés à l'un de ceux de l'ensemble d'arrivée).

   > Exemple : l'application identité, $\mathbf{Id_{E}}$, définie par :
   > $$ 
   > \begin{aligned}[t]
   > \mauve{Id_{E}} : E &\to F \\
   > x &\mapsto x
   > \end{aligned} 
   > $$

\mauve{$F^{E}$} ou $F(E, F)$ est l'ensemble des __applications__ de E dans F. 

et cette notation est choisie parce que s'ils sont tous deux des ensembles finis, alors  
$\text{Card }F^{E} = (\text{Card }F)^{(\text{Card }E)}$

\begingroup \cadreGris

   > \underline{Propriétés :} \newline
   > 
   > Dans une application, les éléments de l'ensemble d'arrivée peuvent avoir :
   > 
   >    - Un unique antécédent,  
   >    - Pas d'antécédent du tout :  
   >      "_En quel mois des familles partent-elles en vacances ($F \to M$) ?"  
   >      $\rightarrow$ "Peut-être aucune en Novembre !_"  
   >    - Plusieurs antécédents :  
   >      "_Beaucoup partent en vacances en Juillet_"
   > 

\endgroup

Remarques :

   - Comme un élément source est toujours relié à une image par une application, il n'existe pas d'application qui aurait pour ensemble de départ un ensemble non vide, et pour ensemble d'arrivée, l'ensemble vide.  
     "_Si une application reçoit quelque-chose, elle produit quelque-chose._"

