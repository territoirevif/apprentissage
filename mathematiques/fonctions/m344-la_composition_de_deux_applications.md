---
title: "m344. La composition de deux applications"
subtitle: ""
category: mathématiques
author: Marc Le Bihan
keywords:
- MVA003
- composition de fonctions
- composition d'applications
---

\underline{Définition :}

   > Soient $f : E \to F$ et $g : F \to G$, deux __applications__.

   > On appelle \definition{composée} de __f__ et de __g__, l'\definition{application $h : E \to G$}, notée  \definition{$\mathbf{g \circ f}$}, telle que :
   > $$
   > \begin{aligned}[t]
   > h : E &\to G \\
   > x &\mapsto g(f(x))
   > \end{aligned} 
   > $$

Remarque :

   > \avantage{l'identité est l'élément neutre} pour la relation de composition :  
   > $f \circ Id_{E} = Id_{E} \circ f = f$

La composition est __associative__, \attention{mais elle n'est pas commutative}.

Par ailleurs, pour qu'une composition tienne debout, il faut que :

   - l'ensemble d'arrivée de la première application $f$, c'est à dire $F$,
   - soit aussi un sous-ensemble de l'ensemble de départ de la deuxième application, $g$.

