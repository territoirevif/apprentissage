---
title: "m342. La fonction indicatrice"
subtitle: ""
category: mathématiques
author: Marc Le Bihan
keywords:
- MVA003
- fonction indicatrice
- fonction caractéristique
---

On appelle \definition{fonction indicatrice de A} ou \definition{fonction caractéristique} l'application 

   > qui fait correspondre aux éléments de __E__  
   > $\mathbf{0}$ ou $\mathbf{1}$ selon que $\mathbf{x}$ appartient à __A__ ou pas,

et on la note \definition{$\mathbf{1_{A}}$}.  
   $$
   \begin{aligned}[t]
   1_{A} : E &\to \{0,1\} \\
   x &\mapsto 1\text{ si }x \in A \\
   &\ \ \ \ \ 0\text{ sinon}
   \end{aligned} 
   $$

\danger{\underline{Théorème :}}

Soient __A__ et __B__ deux __parties__ de __E__ :
   $$
   \begin{aligned}[t]
   1_{A \cap B} &= 1_{A} \times 1_{B} \\ 
   1_{A \cup B} &= 1_{A} + 1_{B} - 1_{A} \times 1_{B} \\ 
   1_{\overline{A}} &= 1 - 1_{A} \text{\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ (le complémentaire)}
   \end{aligned} 
   $$

