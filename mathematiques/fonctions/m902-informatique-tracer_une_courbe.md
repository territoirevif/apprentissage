---
title: "m402. Tracer une courbe avec les outils informatiques"
subtitle: "et les langages de programmation"
author: Marc Le Bihan
keywords:
- courbe
- python
- tracer une courbe
- ensemble de définition
- graduer des axes
- axe 
---

  - [Python : tracer une courbe](#courbe-python)

---

Tracer \begingroup \large $\bleu{\frac{-x^2 + 2x}{x^2 - 1}}$ \endgroup sur son ensemble de définition $\bleu{\mathcal{D} = \mathbb{R} - \{-1, 1\}}$ en graduant les axes, et en sauvegardant la figure dans un fichier image.

\bigbreak

L'ensemble de définition $\mathcal{D} = \mathbb{R} - \{-1, 1\}$ doit être converti en $\mauve{\mathcal{D} = [-4, -1[\ \cup\  ]-1, 1] \  \cup \  ]-1, 4]}$ :

  - en créant trois ensembles distincts : $D_{f1}, D_{f2}, D_{f3}$
  - et en les rassemblant : $D_{f} = [D_{f1}, D_{f2}, D_{f3}]$

\bigbreak

## 1) Tracer une courbe en Python{#courbe-python}

Avec les packages \fonction{pylab} et \fonction{numpy}.

   - réglage des axes avec \methode{axis}{(xmin, xmax, ymin, ymax)}  
   - graduation des axes avec \methode{gca}{.minorticks\_on()} et \methode{gca}{.spines['bottom']} et \fonction{'left'} pour placer le zéro sur chaque axe  
   - la méthode \methode{plot}{(x,y, [color='...'])} va placer chaque point
   - Appel requis de `legend([r'$\mathscr{C}_f$'], fontsize = 18)` pour que la légende s'affiche.
   - sauvegarde de l'image avec \methode{savefig}{(fichier\_image)}

\bigbreak

\code

> ```python
> import pylab as pl
> from numpy import *
> 
> pl.style.use('bmh')
> 
> Df1 = [-4 + k/10 for k in range(28)] # [-4, -1[ par pas de 0.1
> Df2 = [-0.8 + k/10 for k in range(18)] # ]-1, 1] par pas de 0.1
> Df3 = [1.1 + k/10 for k in range(30)] # ]-1, 4] par pas de 0.1
> Df = [Df1, Df2, Df3] # Df = R - {-1, 1}
> 
> for D in Df:
>    Y = [(-(x ** 2) + 2 * x) / (x ** 2 - 1) for x in D] # Pour tout x ∈ D, calculer y = f(x)
>    pl.plot(D, Y, color='red')
> 
> pl.legend([ r'$\mathscr{C}_f$'], fontsize = 18)
> pl.axis(xmin=-4, xmax=4, ymin=-6, ymax=6) # axe des x : de -4 à 4, axe des y : de -6 à 6
> 
> pl.gca().minorticks_on()
> pl.gca().spines['bottom'].set_position('zero')
> pl.gca().spines['left'].set_position('zero')
> 
> pl.savefig("/tmp/analyse_9.14.png")
> ```

\endgroup

