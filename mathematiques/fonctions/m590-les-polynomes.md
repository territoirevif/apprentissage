---
title: "m590. Les polynômes"
subtitle: ""
author: Marc Le Bihan
keywords:
- mathématiques
- polynôme
- Horner
---

La méthode de \definition{Horner}  
   \demitab substitue à des mises à la puissance  
   \demitab des multiplications moins nombreuses.

## 1) Algorithme de Horner

\underline{Python :} Evaluer un polynôme $\bleu{f}$ en une valeur $\mauve{x}$

   \code

   à partir de $\bleu{a}$ la liste de ses coefficients  
      \demitab et $\bleu{x}$ une valeur où évaluer la fonction,  
      \demitab retourner $\rouge{f(x)}$.

   > ```python
   > def horner(a: list[float], x: float):
   >     if len(a) == 1:
   >         return a[0]
   > 
   >     a[-2] += x * a[-1]
   >     return horner(a[: -1], x)
   > ```

   \bigbreak

   \underline{Exemple :} $f(x) = 2x^3 - 5x^2 + 4x + 1$
 
   > ``` python
   > a = [2, -5, 4, 1]
   > print("f(x) = 2x³ - 5x² + 4x + 1")
   > print("\tf(-1) = ", horner(a, -1))
   > print("\tf(0) = ", horner(a, 0))
   > print("\tf(1) = ", horner(a, 1))
   > ```
   
   ```log
   f(x) = 2x³ - 5x² + 4x + 1
      f(-1) =  10
      f(0) =  2
      f(1) =  1
   ```
   
   \endgroup
