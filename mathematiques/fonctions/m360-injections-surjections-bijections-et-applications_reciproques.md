---
title: "m360. Injections, Surjections, Bijections et applications réciproques"
subtitle: ""
category: mathématiques
author: Marc Le Bihan
keywords:
- MVA003
- fonction
- injection
- surjection
- bijection
- application injective
- application surjective
- application bijective
- composition
- application réciproque
- réciproque
---

# I) Injection, surjection, et bijection d'application

## 1) Les applications injectives

Une application $f$ est dite \definition{injective} si tout élément de $F$ possède __au plus__ un antécédent dans $E$.
_Tous les éléments de l'ensemble d'arrivée recoivent une unique flèche, ou n'en reçoivent pas._
$$ \forall x, x' \in E, $$
$$ x \neq x' \implies f(x) \neq f(x') $$

ou dit autrement :
$$ \forall x, x' \in E, $$
$$ f(x) = f(x') \implies x = x' $$

\underline{Propriétés :}

- La composée de deux applications injectives est injective.  
Si $g \circ f$ est injective, alors $f$ est injective.

- soit $f : E \to F$ une application avec $E \neq \varnothing$,  
il existe une application $g : F \to E$ telle que $\mathbf{g \circ f = Id_{E}}$ si et seulement si $f$ est injective.  
(_c'est à dire : il existe une fonction $g$ inverse de $f$, car $g(f(x))$ renvoie à l'antécédent de $f(x)$ : $x$_)  
On dit que l'injection est une application "_inversible à gauche_".

- soit $f : E \to F$ une application injective, et soient $A$ et $B$ deux parties de $E$, alors
$$ f(A \cap B) = f(A) \cap f(B) $$
(et non plus seulement $f(A \cap B) \subset f(A) \cap f(B)$, comme dans le cas général).

- Toute application strictement monotone de $\mathbb{R}$ dans $\mathbb{R}$ est injective.

- Soit $A$ une partie de $E$ et $f : E \to F$,

   > $A \subset f^{-1}(f(A))$ dans le cas général  
   > $A = f^{-1}(f(A))$ si l'application est injective.

## 2) Les applications sujectives

Soit $f : E \to F$ une application. Elle est \definition{surjective} si tout élément $y$ de $F$ possède __au moins__ un antécédent dans $E$.
$$ \forall y \in F, \exists x \in E ; y = f(x) $$
(_on trouve toujours un antécédent $x$ à un $y$ qu'une fonction aura donné)_

\underline{Méthode :}

Pour démontrer qu'une application est surjective, on résoud l'équation :
$$ f(x) = y $$
Si l'on trouve au moins une solution, alors $f$ est surjective.

\underline{Propriétés :}

- La composée de deux applications surjectives est surjective.
- Egalement, si $g \circ f$ est surjective, alors $g$ est surjective.
- $f$ est surjective si et seulement si $\text{Im}(f) = F$ (dans le cas général, $\text{Im}(f) \subset F$).  
- Soit $f : F \to E$ une application : il existe une application $g : F \to E$ telle que $f \circ g = Id_{F}$ si et seulement si $f$ est surjective.  
On dit qu'une surjection est "_une application inversible à droite_"

Soit $B$ une partie de $E$, et $f : E \to F$

   - Si $f$ est surjective, alors $f(f^{-1}(B)) = B$
   - Si elle ne l'est pas (cas général), $f(f^{-1}(B)) \subset B$

## 3) Les applications bijectives

Soit $f : E \to F$ une application, on dit qu'elle est \definition{bijective} si elle est à la fois __injective__ et __surjective__.

__Tous__ les éléments de $E$ sont reliés à un __unique__ élément de $F$ :
$$ \forall y \in F, \exists! x \in E ; y = f(x) $$

\underline{Méthode :}

Pour montrer qu'une application est bijective, on résoud l'équation
$$ f(x) = y $$
et si l'on trouve une \underline{\textbf{unique}} solution, l'application est bijective.

\underline{Propriétés :}

- La composée de deux applications bijectives est bijective.
- L'ensemble des bijections d'un ensemble $A$ dans lui même est $n!$ où $n$ est le nombre de ses éléments.  

- Soit $E$ et $F$ deux ensembles __finis \underline{de même cardinal}__, et $f : E \to F$ une application, alors les propropositions suivantes sont équivalentes :

   - $f$ est injective
   - $f$ est surjective
   - $f$ est bijective

- Si deux ensembles finis sont en bijection, alors ils ont même cardinal.

# II) Les applications réciproques

On ne peut trouver une application réciproque $F \to E$ à une application $E \to F$ que si l'application en question est bijective. Faute de quoi :

   - si l'application est seulement injective, lors de la réciproque, des éléments n'auront pas d'image
   - si l'application est seulement surjective, lors de la réciproque, des éléments auront plusieurs images

Soit $f : E \to F$ une bijection, alors
$$ g : F \to E $$
$$ \forall y \in F, g(y) = x \iff f(x) = y $$

est une __application bijective__ appelée \definition{bijection réciproque}, qu'on note \definition{$\mathbf{f^{-1}}$}.

application réciproque et bijection réciproque sont des termes synonymes.

\underline{Propriétés :}

Soit $f : E \to F$ une bijection. Son application réciproque de $F \to E$ est __la seule__ qui vérifie :
$$ f \circ f^{-1} = Id_{F} $$
$$ f^{-1} \circ f = Id_{E} $$ 

\underline{Corrolaire :}

Une application $f : E \to F$ est bijective si et seulement si il existe une application $g : F \to E$ telle que :
$$ f \circ g = Id_{F} $$
$$ g \circ f = Id_{E} $$

et alors, $g$ est l'application réciproque : $g = f^{-1}$

\danger{\underline{Théorème :}}
Soient $f : E \to \textcolor{blue}{F}$ et $g : \textcolor{blue}{F} \to G$ deux bijections, alors :

   - $g \circ f$ est une bijection
   - $(g \circ f)^{-1} = f^{-1} \circ g^{-1}$ \ \ \ \attention{attention à l'ordre !}

