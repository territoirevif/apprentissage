---
title: "m348. Restriction et prolongement"
subtitle: ""
category: mathématiques
author: Marc Le Bihan
keywords:
- MVA003
- fonction
- restriction
- prolongement
---

\underline{Restriction :}

   > Soit $f : E  \to F$ une application, et __A__ une partie de __E__,  
   > l'application \definition{$\mathbf{f|A} : A \to F, x \mapsto f(x)$} est appeléee \underline{\textbf{la}} \definition{restriction} de $f$ à $A$.  
   > __La__, parce qu'elle est unique : il ne peut y en avoir qu'une seule.

\bigbreak

\underline{Prolongement :}

   > Soit $f : E \to F$ une application, et __E'__ un ensemble \attention{\underline{contenant}} __E__,  
   > $g : E' \to F$ est  __un__ \definition{prolongement} de $f$ à __E'__ si $g|E = f$
 
   >   > _C'est à dire si quand on restreint cette fonction $g$, qui utilise un surensemble de E, à l'ensemble source de E, on retrouve bien les images de $f$._

