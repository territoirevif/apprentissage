---
author: Marc Le Bihan

title: "m157. Les dérivées"
subtitle: "leur utilisation pour l'étude de fonctions"
category: mathématiques
abstract: Nombre dérivé, maximum/minimum locaux, tangente horizontale, calcul de tangente, tableau des dérivées, combinaison de fonctions, composition de fonctions
---

# I) La dérivée d'une fonction

![taux de variation](images/taux_de_variation.jpg)

Le \definition{taux de variation} entre deux abscisses \bleu{$x_{1}$} et \bleu{$x_{2}$} d'une fonction \mauve{f} est :
$$ \frac{f(x_{2}) - f(x_{1})}{x_{2} - x_{1}} $$

## 1) La dérivée d'une fonction en un point

On introduit le \definition{nombre dérivé} en faisant \donnee{tendre} $x_{2}$ vers $x_{1}$.

   > c'est que, __pour peu que l'intervalle soit défini et continu__ (et contenant $x$...),  
   >
   > \ \ \ \ \ \ \ \ $f'(x)$ en sera la limite
   > $$ \rouge{f'(x)} = \lim_{h \to 0} \Big(\frac{f(x + h) - f(x)}{h}\Big) $$

\pagebreak

\code 

Le calcul d'un nombre dérivé en java, avec \fonction{UnivariateFunction} de \fonction{org.apache.commons.math3.analysis} 

> ```java
> public double getNombreDerive(UnivariateFunction f, double x, double h) {
>    if (h == 0)
>       throw new IllegalArgumentException("L'écart pour calculer le nombre dérivé d'un polynôme ne peut pas valoir zéro.");
>
>    return (f.value(x+h) - f.value(x)) / h;
> }
> ```
>
> ---
>
> ```log
> Un appel de getNombreDerive(f, 3.0, 0.5) 
>    avec UnivariateFunction f = (x -> (5 * x - 2)/(1 - 3 * x)); répond :
>
> Le nombre dérivé en 3.0 est 5.0 pour p(x) = -2 + 5 x, mesuré avec h = 0.5
> ```

\bigbreak

Avec \fonction{UnivariateDifferentiableFunction} de \fonction{org.apache.commons.math3.analysis.differentiation} :  

Dans l'appel de \fonction{DerivativeStructure}:  
   \demitab paramètre libre: 1, ordre: 1, index du paramètre: 0,   
   \demitab valeur de la variable: $x$, ordre de la dérivée partielle: 1  

> ```java
> public double getNombreDerive(double x) {
>    return f.value(new DerivativeStructure(1, 1, 0, x)).getPartialDerivative(1);
> }
> ```

\endgroup

## 2) La pente et la tangente à une droite

![Pente et tangente à une droite](images/pente_et_tangente_à_une_droite.jpg)

Le rapport \begingroup \large \mauve{$\frac{f(x+h) - f(x)}{h}$} \endgroup mesure la \definition{pente} de la droite.

En plaçant les points:

   > $\bleu{P(\mauve{x}, f(\mauve{x}))}$
   >
   > $\bleu{M(\mauve{x+h}, f(\mauve{x+h}))}$

plus \mauve{h} est petit,  
   \demitab et plus $M$ se rapproche de $P$

et plus la droite (PM)  
   \demitab est \definition{tangente} (en vert sur le schéma) à la courbe $C$.

\gris{(est-ce que quand on a trouvé une tangente en un point, on a trouvé une fonction qui simplifie la nôtre en ce point ?)}

# II) Calcul des dérivées

## 1) Tableau des dérivées

   | \textbf{fonction}   | \textbf{dérivée}           | \textbf{ensemble de définition} |
   | ------------------- | -------------------------- | ---------------------- |
   | $y = a$             | $y' = 0$                   | $x \in \mathbb{R}$     |
   | $y = x$             | $y' = 1$                   | $x \in \mathbb{R}$     |
   | $y = ax+b$          | $y' = a$                   | $x \in \mathbb{R}$     |
   | $y = x^2$           | $y' = 2x$                  | $x \in \mathbb{R}$     |
   | $y = ax^2 + bx + c$ | $y' = 2ax+b$               | $x \in \mathbb{R}$     |
   | $y = x^3$           | $y' = 3x^2$                | $x \in \mathbb{R}$     |
   | $y = \frac{a}{x}$   | $y' = \frac{-a}{x^2}$      | $x \in \mathbb{R^*}$   |
   | $y = \sqrt{x}$      | $y' = \frac{1}{2\sqrt{x}}$ | $x \in \ ]0, +\infty[$ |
   | $y = x^n$           | $y' = nx^{n-1}$            | $x \in \mathbb{R}$     |

## 2) Combinaison de fonctions

\gris{(combinaison et composition de fonction (plus bas) ne sont pas exactement la même chose).}

   | \textbf{fonction}          | \textbf{dérivée}               |
   | -------------------------- | ------------------------------ |
   | $y(x) = k.u(x)$            | $y' = k.u'$                    |
   | $y(x) = u(x) + v(x)$       | $y' = u' + v'$                 |
   | $y(x) = u(x) . v(x)$       | $y' = u'v + uv'$               |
   | $y(x) = \frac{u(x)}{v(x)}$ | $y' = \frac{u'v - uv'}{v^{2}}$ |
   | $y(x) = \sqrt{u(x)}$       | $y' = \frac{u'}{2\sqrt{u}}$    |

![exemple de combinaison de fonctions](images/analyse_ex8-5.jpg)  
_Emploi de la combinaison de fonctions division de deux polynômes_

## 3) Composition de fonctions

Si $\mauve{u(x)}$ est une fonction, et $\mauve{y}$ une fonction de $\mauve{u}$, alors :
$$ \rouge{y'(x)} = y'(u) . u'(x) $$

ou dit autrement :
$$ \rouge{[f \circ g]'(x)} = f'(g(x)).g'(x) $$

![exemple de composition de fonctions pour deux polynômes](images/analyse_ex8.6-composition-fonctions.png)  
_Emploi de la composition de fonctions_

# III) Utilisation des dérivées pour l'étude de fonctions

## A) Recherche de maximum et de minimum locaux

Si une dérivée s'annule en un point (dans un intervalle où une fonction $f$ est dérivable), c'est à dire \mauve{$f'(x_{0}) = 0$}, alors la tangente en $x_{0}$ est en ce point horizontale, et il y a trois cas :

### 1. visuellement, la tangente est "_au dessus de la courbe, localement_"

   ![maximum local](images/tangente_horizontale_maximum_local.jpg)  
   $f$ est croissante avant $x_{0}$, décroissante ensuite, et la dérivée change de signe après s'être annulée en $x_{0}$ :
 
   > $\to$ elle passe d'un signe positif à un signe négatif.

   nous sommes en présence d'un \definition{maximum local}.  
   \attention{\underline{local}, parce qu'on ne peut pas affirmer que c'est le maximum pour toute la fonction !}

### 2. la tangente est "_au dessous de la courbe, localement_"

   ![minimum local](images/tangente_horizontale_minimum_local.jpg)  
   $f$ est décroissante avant $x_{0}$, croissante ensuite, et la dérivée change de signe après s'être annulée en $x_{0}$ :

   > $\to$ elle passe d'un signe négatif à un signe positif.

   nous sommes en présence d'un \definition{minimum local}.

### 3. la tangente n'est "_ni au au dessus, ni en dessous de la courbe, localement_"

   ![point d'inflexion à la tangente horizontale](images/point_inflexion_tangente_horizontale.jpg)  
   $f$ ne change pas de signe en s'annulant en $x_{0}$ : elle reste croissante ou décroissante, comme elle l'était, ensuite,  
   et la dérivée __ne change pas__ de signe après s'être annulée en $x_{0}$ :   
   $\to$ elle conserve celui qu'elle avait.  
   nous sommes en présence d'un \definition{point d'inflexion \underline{à la tangente horizontale}}.  
   \gris{(car il existe d'autres points d'inflexion, mais pour des $f'(x_{0}) \neq 0$)}.

## B) Trouver l'équation de la tangente à une courbe

Quelle est l'équation de la tangente en un point $\bleu{M}$ d'abscisse $\bleu{x_{0}}$ ?

\underline{Il faut deux points pour faire la tangente :}

   > $\bleu{M}$ : le point de coordonnées $\bleu{(x_{0}, f(x_{0}))}$ là où l'on veut la tangente à ce point
   >
   > $\rouge{T}$ : Un autre point sur la tangente : la pente entre $\bleu{M}$ et $\rouge{T}$ __sera le nombre dérivé__ $f'(x_{0})$

   1. On choisit donc une deuxièmee abscisse sur le graphe : $\bleu{X}$  
      la pente a pour formule :  
      $$ \frac{Y - f(x_{0})}{X - x_{0}} \ \ \text{et c'est } f'(x_{0}) \gris{\text{\ en fait, on recalcule le nombre dérivé comme avec la limite}} $$

![Tangente à une courbe en un point quelconque où elle est dérivable](images/tangente_a_une_courbe_en_un_point_derivable.jpg)  
\legende{tangente à la courbe en un point quelconque où $f(x)$ est dérivable}

   2. $\frac{Y - f(x_{0})}{X - x_{0}} = f'({x_0})$, ce qui fait que :  
   \begin{eqnarray*} 
   \gris{Y - f(x_{0}) = f'(x_{0})(X - x_{0})} \\
   \rouge{Y} = f'(x_{0}).(X - x_{0}) + f(x_{0})
   \end{eqnarray*}

   et l'on aura le second point de la tangente : $T(X,Y)$.

   \bigbreak

   \begingroup \cadreGris

   > \textbf{Quelle est l'équation de la tangente à} $y = x^{2}$ \textbf{au point d'abscisse } $x_{0} = 3$ ?  
   > on va la chercher avec une unité d'abscisse d'écart
   >
   > 1. la dérivée de $f(x) = x^2$ est $f'(x) = 2x$
   > \    
   > 2. en $x_0 = 3$, la dérivée $f'(3) = 2x_0 = 6$ : notre pente vaut __6__.  
   > \  
   > 3. $Y - f(x_{0}) = f'(x_{0})(X - x_{0}) \iff Y - f(3) = f'(3)(X -3) \iff Y - 9 = 6(X - 3)$  
   > \  
   > 4. On ne résoud pas, on simplifie juste, et cela donne :
   > $Y = 6X - 9$

   \endgroup
