---
author: Marc Le Bihan
title: "m530. Les inéquations réelles"
subtitle: ""
---

Une inéquation est une inégalité dans laquelle figure une ou plusieurs inconnues.

Contrairement aux équations, qui ont des solutions en général limitées, les inéquations ont souvent un nombre infini de solutions.

\bigbreak

$a < b$ ou $a > b$ est une inégalité stricte,  
$a \le b$ ou $a \ge b$ est une inégalité large

\bigbreak

On peut ajouter ou soustraire un même nombre aux deux membres d'une inéquation

Si $\mauve{a < b}$, alors

   > $a + c < b + c$  
   > $a - c < b - c$

\bigbreak

On peut multiplier ou diviser le même nombre __positif__ des deux membres d'une inéquation 

Si $\mauve{a < b}$ \bleu{et} $\mauve{c > 0}$, alors

   > $a \times c$ < $b \times c$
   > $\frac{a}{c} < \frac{b}{c}$

\bigbreak

Mais en la multipliant par un nombre négatif, elle changera de sens :

Si $\mauve{a < b}$, alors 

   > $-a > -b$  

