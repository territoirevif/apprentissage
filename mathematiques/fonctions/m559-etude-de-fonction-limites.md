---
title: "m559. Etude de fonction"
subtitle: "Limites"
author: Marc Le Bihan
keywords:
- mathématiques
- limite
- majoration
- minoration
---

## 1) Etude d'une limite autour d'un point

Autour d'un point, on étudie le comportement d'une fonction.

### a) Limite à gauche, limite à droite

Quand une limite à droite et à gauche de $\bleu{a}$ sont inégales  
   \demitab la fonction est \definition{discontinue}.

### b) Majorations, minorations, et limites

Une fonction \underline{\danger{croissante}} et \underline{\danger{majorée}} sur $\bleu{]a,b[}$

   \demitab admet une limite à gauche de $\bleu{b}$  
   \demitab telle que $\mauve{\lim\limits_{x \to b^{-}} \leq M}$

\bigbreak

Une fonction \underline{\avantage{décroissante}} et \underline{\danger{majorée}} sur $\bleu{]a,b[}$

   \demitab admet une limite à droite de $\bleu{a}$  
   \demitab telle que $\mauve{\lim\limits_{x \to a^{+}} \leq M}$

\bigbreak

Une fonction \underline{\danger{croissante}} et \underline{\avantage{minorée}} sur $\bleu{]a,b[}$

   \demitab admet une limite à droite de $\bleu{a}$  
   \demitab telle que $\mauve{\lim\limits_{x \to a^{+}} \geq m}$  

\bigbreak

Une fonction \underline{\avantage{décroissante}} et \underline{\avantage{minorée}} sur $\bleu{]a,b[}$

   \demitab admet une limite à gauche de $\bleu{b}$   
   \demitab telle que $\mauve{\lim\limits_{x \to b^{-}} \geq m}$

