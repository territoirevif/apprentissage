---
title: "m320. L'ensemble des parties"
subtitle: ""
category: mathématiques
author: Marc Le Bihan
keywords:
- MVA003
- ensemble
- ensemble des parties
- inclusion
- ensemble vide
---

L'\definition{ensemble des parties}, $P(E)$, est l'ensemble de tous les \attention{ensembles} inclus dans E.

$P(E) = \{A|\ A \subset E\}$

   > $\varnothing \in P(E)$  
   > $E \in P(E)$

\bigbreak

$A \in P(E) \iff A \subset E$ : le fait qu'un ensemble fasse partie des ensembles d'un autre indique qu'il est inclus dedans.

   > $P(\{1\}) = \{0,\{1\}\}$  
   > $P(\{1,2\}) = \{\varnothing,\{1\},\{2\},\{1,2\}\}$  
   > $P(\{1,2,3\}) = \{\varnothing,\{1\},\{2\},\{3\},  
   >              \{1,2\},\{1,3\},\{2,3\},
   >              \{1,2,3\}\}$

\bigbreak

Si \definition{E} est un ensemble fini à \definition{$\mathbf{n}$} éléments, alors l'ensemble des parties contient \definition{$\mathbf{2^n}$} éléments.

   > L'ensemble des parties n'est jamais vide, car même si E est vide, $P(E)$ contient toujours l'ensemble vide, $\varnothing$, $\implies 2^0 = 1$.

