---
title: "m330. Les opérations sur les ensembles"
subtitle: "intersection, union, inclusion..."
category: mathematiques
author: Marc Le Bihan
keywords:
- MVA003
- ensemble
- égalité entre deux ensembles
- intersection
- union
- inclusion
- ensemble complémentaire
- complémentaire
- Morgan
- Loi de Morgan
- cardinal
- sous-ensemble
- distributif
- associatif
- commutatif
---

## 1) Egalité entre deux ensembles

Deux ensembles sont \definition{égaux}  
\demitab s'ils ont exactement les mêmes éléments  
\demitab (pas nécessairement dans le même ordre)

et différents dans le cas contraire.

\bigbreak

   \begingroup \cadreGris

   > Monter que $\mauve{E = F}$ équivaut à montrer que \mauve{$(E \subset F)$ et $(F \subset E)$}.

   \endgroup

## 2) Intersection et union

L'ensemble des parties d'un ensemble est doté des opérations \definition{intersection} et \definition{union}.

  - L'union de A et de B : l'ensemble des éléments appartenant à A ou à B : $\mauve{A \cup B}$  
    (équivalent au $\mauve{\lor}$ logique),  
  - L'intersection de A et de B : l'ensemble des éléments appartenant à A et à B : $\mauve{A \cap B}$  
    (équivalent au $\mauve{\land}$). 

\bigbreak

Ces opérations sont __commutatives__ et __associatives__.

Elles sont aussi __distributives__ :

   - Par rapport à l'union :

     > $A \cap (B \cup C) = (A \cap B) \cup (A \cap C)$  

   - Par rapport à l'intersection :

     > $A \cup (B \cap C) = (A \cup B) \cap (A \cup C)$  

## 3) L'inclusion

\underline{Propriétés de l'inclusion :}

  > $\varnothing \subset E$  
  > $E \subset E$  
  > si E et F sont des ensembles \attention{finis} et que $E \subset F$, alors $Card(E) \le Card(F)$

\bigbreak

E est \definition{inclus} dans G si tous les éléments de E sont des éléments de G. On dit alors que c'est un \definition{sous-ensemble} de G.

## 4) L'ensemble complémentaire

Soit A un élément de P(E), le \definition{complémentaire} de A dans E, noté $E \setminus A$ ou $\overline{A}$, est l'ensemble des éléments de E qui ne sont pas dans A :

   > $E \setminus A = \overline{A} = \{x \in E|\ x \notin A\}$

## 5) Les Lois de Morgan

   > $\overline{A \cup B} = \overline{A} \cap \overline{B}$

   > $\overline{A \cap B} = \overline{A} \cup \overline{B}$

\bigbreak

\danger{\underline{Théorème :}}

Soient A et B deux éléments de P(E), alors

   > $A \subset (A \cup B)$  
   > $(A \cap B) \subset A$

\bigbreak

La différence de A et B, notée $A \setminus B$ est l'ensemble des éléments de A qui ne sont pas dans B :

   > $A \setminus B = \{x \in A$ et $x \notin B\}$

Enfin,

   > $Card(A \cup B) = Card(A) + Card(B) - Card(A \cap B)$

