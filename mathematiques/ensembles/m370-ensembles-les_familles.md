---
title: "m370. Les familles d'ensembles"
subtitle: ""
author: Marc Le Bihan
---

Une \definition{suite d'éléments} c'est une application de $\mathbb{N}$ vers $E$  
   \demitab qui permet de numéroter les éléments de $E$.

ce qui est utile si rien ne repère les éléments de l'ensemble entre-eux.

## 1) Une famille d'éléments

On appelle \begingroup \large \bleu{$E^{\mathbb{N}}$} \endgroup l'ensemble des suites indexées de \bleu{$E$} par \bleu{$\mathbb{N}$}.  

\bigbreak

Mais on peut aller plus loin, et choisir d'autres éléments que des nombres pour servir d'\definition{indices}.

Au lieu de repérer les éléments de $E$ par $\{e_{\mauve{1}}, e_{\mauve{2}}, e_{\mauve{3}}, ...\}$,  
   \demitab on pourra choisir de le faire par : $\{e_{\bleu{a}}, e_{\bleu{b}}, e_{\bleu{c}}, ...\}$

\bigbreak

   > \definition{\underline{Définition :}}  
   > On appelle \definition{famille d'éléments} de $\mathbf{E}$ indexée par un ensemble $\mathbf{I}$,  
   > \demitab toute application de $I$ dans $E$,  
   > \demitab et on la note : $(x_{i})_{i \in I}$
   >
   >    - Si $I = \varnothing$, alors $(x_{i})_{i \in I}$ est appelée __famille vide__,
   >    - Si $I = E$, alors $(x_{i})_{i \in I}$ est appelée la __famille canoniquement associée à E__

\bigbreak

En permettant d'itérer sur les éléments d'un ensemble  
   \demitab avec par exemple $\bleu{\mathbf{\sum}}$ ou $\bleu{\mathbf{\prod}}$,

la famille est souvent utilisée en \definition{algèbre linéraire}.

\bigbreak

Plus avant, la famille finie généralise la notion de liste (= une suite finie),  
    \demitab et la famille infinie, celle de suite.

## 2) Les familles d'ensembles

Cette fois-ci, ce ne sont pas des éléments d'un ensemble que l'on va numéroter  
   ou sur lesquels on va mettre un indice,

mais sur des ensembles.

\bigbreak

Les familles d'ensembles \definition{$E_{i}$} indexés par un ensemble \definition{$I$}, qu'on note : \definition{$(E_{i})_{i \in I}$}.

### a) Généralisation de la réunion

Dans le cas particulier où les $E_{i}$ seraient des parties d'un même ensemble $E$,  
   \demitab ($\forall i, E_{i} \in P(E)$), alors :

   > \definition{\underline{Définition :}}  
   > On appelle \definition{réunion de la famille} $\mauve{\mathbf{(E_{i})_{i \in I}}}$, l'ensemble :  
   > $$ \bigcup_{i \in I}E_i = \{x \in E, \textcolor{OliveGreen}{\mathbf{\exists}} i \in I, x \in E_{i}\} $$  
   > qui généralise l'union de deux ensembles.

\bigbreak

Exemple (avec trois mêmes façons de l'écrire)  :
   $$ \bigcup_{i \in \{1,2\}}E_i = \{x \in E, \exists i \in \{1,2\}, x \in E_{i}\} $$  
   $$ \bigcup_{i \in \{1,2\}}E_i = \{x \in E, x \in E_1 \text{ ou } x \in E_2\} $$  
   $$ \bigcup_{i \in \{1,2\}}E_i = \{x \in E, E_1 \text{ ou } E_2\} $$  

### b) Généralisation de l'intersection

Il existe de la même façon la définition de l'intersection :
   $$ \bigcap_{i \in I}E_i = \{x \in E, \textcolor{red}{\mathbf{\forall}} i \in I, x \in E_{i}\} $$  

\bigbreak

   >  \definition{\underline{Définition :}}  
   > On appelle __recouvrement d'un ensemble E__  
   > toute famille de parties E, telle que :  
   > $\bigcup_{i \in I}E_i = E$.

\bigbreak

   > \definition{\underline{Définition :}}  
   > on appelle \definition{partition} __d'un ensemble E__  
   > tout __recouvrement__ vérifiant :

   > \mauve{$\forall i \in I, E_{i} \neq \varnothing$}  
   > (_aucun des sous-ensembles n'est vide_)
   >
   > \mauve{$\forall i,j \in I, (i \neq j), E_i \cap E_j = \varnothing$}  
   > (_chaque sous-ensemble est distinct des autres_)

