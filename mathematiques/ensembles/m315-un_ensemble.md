---
title: "m315. La définition d'un ensemble"
subtitle: "en extension, compréhension..."
category: mathématiques
author: Marc Le Bihan
keywords:
- MVA003
- ensemble
- définition en extension
- élément
- définition en compréhension
- tiers exclu
- paradoxe de Roussel
- diagramme de Venn
- Venn
- singleton
- paire
- couple
- ensemble vide
- prédicat
---

Un \definition{ensemble} est une collection d'objets appelés \definition{éléments}.

## 1) Sa définition en extension, pour présenter tous ses éléments

Quand on veut présenter __tous__ les éléments d'un ensemble (les énumérer) on dit qu'on le définit en \definition{en extension}.

Par exemple, avec une \mauve{table de valeurs}.

   \begingroup \cadreAvantage

   > $E = \{1, 2, 3\}$  
   > \ \ \ \ $2 \in E$, $4 \notin E$

   \endgroup

## 2) Définition en compréhension avec une propriété, un prédicat

La propriété __P__ servant à définir un ensemble en compréhension s'appelle un \definition{prédicat}.

   \begingroup \cadreAvantage

   > $E = \{n \in \mathbb{N}|\ 1 \leq n \leq 3\}$

   \endgroup

\bigbreak

\underline{principe du tiers exclu :} soit un élément est dans l'ensemble, soit il n'y est pas,  
il ne peut pas y être et ne pas y être en même temps.

## 3) Definition par un Diagramme de Venn, pour le représenter graphiquement

Le \definition{Diagramme de Venn} c'est "_la patate_" dont on a l'habitude.

## 4) Le cardinal d'un ensemble

Si l'ensemble ne contient aucun élément, c'est l'\definition{ensemble vide}, noté : $\varnothing$ ou {}.

On appelle \definition{cardinal} (noté `Card()`) d'un ensemble E, son nombre d'éléments.

   > $Card(\varnothing) = 0$  
   > $Card(\{\varnothing\}) = 1$  
   > $Card({1, 2, 3}) = 3$

Un ensemble qui n'a qu'un seul élément est appelé un \definition{singleton}.  

Un ensemble qui a deux éléments est appelé une \definition{paire} :

\demitab \attention{et une paire \{a, b\} n'est \underline{pas} triée} et mise entre accolades
\demitab à l'opposé du \definition{couple} $(a, b)$, qui lui l'est, et mis entre parenthèses

\bigbreak

\underline{Le paradoxe de Russel :} quand on essaie de dire qu'un élément ne s'appartient pas.

