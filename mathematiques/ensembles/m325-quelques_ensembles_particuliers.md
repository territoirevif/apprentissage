---
title: "m325. Quelques ensembles particuliers"
subtitle: ""
category: mathématiques
author: Marc Le Bihan
keywords:
- ensemble
- entiers naturels
- nombres relatifs
- nombres décimaux
- rationnels
- réels
- nombres complexes
- produit cartésien
---

## 1) Les ensembles de nombres

\bleu{$\mathbb{N}$} est l'ensemble des nombres naturels : $0, 1, 2, 3...$  
\bleu{$\mathbb{Z}$} des nombres relatifs, positifs ou négatifs : $\mathbb{Z} = \mathbb{N} \cup \{-1, -2, -3...\}$  
\bleu{$\mathbb{D}$} des nombres décimaux : $1.52$, $-0.54$, ($27$ ou $-514$ aussi (= 27.0, 514.0)  
\bleu{$\mathbb{Q}$} des rationnels : toute fraction positive ou négative : $\frac{1}{3}, \frac{-1}{7}...$  
\bleu{$\mathbb{R}$} des réels : tous les nombres ou suites décimales limitées ou illimitées : $\frac{-1}{2}, 0.32, 1.5555...$  
\bleu{$\mathbb{C}$} des nombres complexes

![Les ensembles de nombres](images/les-ensembles-de-nombres.jpg)

## 2) Le produit cartésien

Le \definition{produit cartésien} (appellé aussi : _ensemble produit_) de deux ensembles E et F est noté $E \times F$, et est l'ensemble :

   > $E \times F = \{(x,y)|\ x \in E$ et $y \in F\}$

ou pour généraliser plus, si $n \geq 2$,

   > $E_{1} \times E_{2} \times ... \times E_{n} = \{(x_{1}, x_{2}, ..., x_{n}|\ x_{1} \in E_{i})$  
   > (tous les n-uplets qu'on peut faire à partir des ensembles E)

$Card(E \times F) = Card(E) \times Card(F)$, et :

   > $E \times F = \varnothing \iff E = \varnothing \lor F = \varnothing$  
   > $E \times F \neq \varnothing \iff E \neq \varnothing \land F \neq \varnothing$

Quelques exemples :

   > $E = \{1\}$ et $F = \{a\}$ : $E \times F = \{(1,a)\}$  
   > $E = \{1,2\}$ et $F = \{a,b\}$ : $E \times F = \{(1,a), (1,b), (2,a), (2,b)\}$


