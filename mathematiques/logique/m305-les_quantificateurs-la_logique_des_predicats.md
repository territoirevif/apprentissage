---
title: "m305. Les quantificateurs - logique des prédicats"
subtitle: "quelque-soit, il existe"
category: mathématiques 
author: Marc Le Bihan
keywords:
- logique
- prédicat
- paramètre
- tautologie
- propriété
- lettre muette
- pseudo-quantificateur
- quantificateur
- il existe
- il existe un unique
- négation d'un quantificateur
- suffixer des quantificateurs
- quelque-soit
---

Un \definition{prédicat} est une assertion qui dépend d'un ou plusieurs __paramètres__.

   > \definition{$\mathbf{a}$}$x^2$ + \definition{$\mathbf{b}$}$x$ + \definition{$\mathbf{c}$} = 0  

   > a pour paramètres __a__, __b__ et __c__.

\bigbreak

et une \definition{tautologie} est un prédicat qui est toujours vrai : $ax = ax$, par exemple.

## 1) Le quantificateur "QUELQUE-SOIT", \definition{$\forall$} 

\begingroup \large \definition{$\forall$}$x \in A, P(x)$ : \endgroup

   > "\bleu{\emph{Quel que soit $x$ appartenant à $A$, la propriété $P(x)$ est vraie}}"  
   > ou "_Soit $x$ un élément [quelconque] de A..._"  
   > ou "_Soit $x$ un réel quelconque..._"

\bigbreak

\underline{Remarque :} $x$ est une lettre __muette__ : on pourrait la remplacer par $y$ ou $z$

   \begingroup \cadreAvantage

   > $\forall x \in \mathbb{R}, x^2 \geq 0$ : le carré d'un réel est toujours positif ou nul.

   \endgroup

## 2) Le quantificateur "IL EXISTE", \definition{$\exists$}

\begingroup \large \definition{$\exists$}$x \in A, P(x)$  \endgroup

   > ou $\exists x \in A; P(x)$   
   > ou $\exists x \in A| P(x)$
   >
   > "\bleu{\emph{Il existe un élément de $A$ pour lequel la proposition $P(x)$ est vraie}}"  
   > ou "_Posons x = ..._"

\bigbreak

et le pseudo-quantificateur \begingroup \large \definition{$\exists!$} \endgroup

   > "\emph{Il existe un élément \bleu{\textbf{unique}} pour lequel $P(x)$ est vraie.}"

   \begingroup \cadreAvantage

   > __La définition de la fonction bijective__  
   > $\forall y \in \mathbb{R}, \exists! x \in A, y = f(x)$  
   > "_pour tout $y$ de $\mathbb{R}$, il existe un unique $x$ de $A$, tel que : $y = f(x)$_"

   \endgroup

\bigbreak

\underline{Remarque :} Quand plusieurs quantificateurs interviennent dans une propriété, on gagne à suffixer ceux qui dépendent les uns des autres :

   > $\forall x \in A, \exists y \in y_{x} \in B| P(x, y)$  
   > "_Pour tout $x$ appartenant à $A$, il existe un $y$ dépendant de $x$, tel que $P(x,y)$._"

## 3) La négation d'un quantificateur

on change le quantificateur et on nie la proposition.

\begingroup \large

$\bleu{\lnot}[\forall x \in A, P(x)] \equiv \exists x \in A, \lnot P(x)$  
$\bleu{\lnot}[\exists x \in A, P(x)] \equiv \forall x \in A, \lnot P(x)$

\endgroup

\bigbreak

   \begingroup \cadreAvantage

   > \underline{Exemple :} une suite majorée  
   > \   
   > $\exists M \in \mathbb{R}| \forall n \in \mathbb{N}, u_{n} \leq M$,  
   > \demitab "_Il existe un réel $M$, qui pour tout entier $n$, majore la suite $u_{n}$_"
   > \   
   > Sa négation (une suite non majorée) :  
   > $\forall M \in \mathbb{R}| \exists n \in \mathbb{N}, u_{n} > M$,  
   > \demitab "_Pour tout réel $M$, on trouvera un entier $n$, que $u_{n}$ dépasse_"

   \endgroup

\bigbreak

La négation d'une proposition avec deux quantificateurs suit la même règle :

   > $\forall x \in A, \exists y \in y_{x} \in B| P(x, y)$
 
et se nie en :  

   > $\exists x \in A, \forall y \in y_{x} \in B| \lnot P(x, y)$  

