---
title: "m310 : Les raisonnements"
subtitle: "contre-exemple, contraposition, par l'absurde, analyse-synthèse, disjonction de cas, équivalence, récurrence"
category: mathématiques
author: Marc Le Bihan
keywords: 
- logique
- raisonnement
- disjonction de cas
- contre-exemple
- analyse-synthèse
- équivalence
- récurrence
- récurrence simple
- récurrence forte
- récurrence faible
- hérédité
- absurde
---

Les raisonnements sont la boite à outils des démonstrations.

## 1) Démontrer par un contre-exemple

Si l'on veut démontrer que quelque-chose est vrai,  
il est parfois plus facile de montrer que \definition{son contraire} est faux.

\bigbreak

   > Par exemple, pour démonter que \bleu{$\forall x, P(x)$} est faux,  
   > l'on peut démontrer que \bleu{$\exists x, \lnot P(x)$} est vrai.

## 2) Démonstration par contraposition{#contraposition}

S'il est difficile de démonter que \bleu{$P(x) \implies Q(x)$}  
   \demitab alors utiliser la \definition{contraposition}  
   \demitab \bleu{$\lnot Q(x) \implies \lnot P(x)$} (ou $\overline{Q} \implies \overline{P}$)  
   \demitab peut être plus simple :   

   \demitab $\rightarrow$ elle a la même table de vérité.

\danger{Attention à bien inverser les deux propositions, en les niant !}

\bigbreak

   \begingroup \cadreGris

   > Pour démonter que $n^2$ impair $\implies n$ impair (ce qui est difficile)  
   > on peut démontrer que $n$ pair $\implies n^2$ pair.

   \endgroup

## 3) Le raisonnement par l'absurde{#absurde}

Si l'on dispose d'une relation $\bleu{R}$ à la fois vraie et fausse,  
\demitab alors toute relation $\bleu{S}$ est à la fois vraie et fausse.

$\bleu{R}$ est dite "contradictoire", et de fait, $\bleu{S}$ aussi.

\bigbreak

Une démonstration par l'absurde voa démontrer que $\bleu{R}$ est vraie :

   > on ajoute ($\mauve{non\  R}$) aux axiomes de la théorie $\to$ on suppose que $\mauve{(non\  R)}$ est vraie.  
   > et l'on montre que l'on aboutit à une contradiction, c'est à dire qu'on trouve : 
   > \    
   \demitab une relation $S$ à la fois vraie et fausse.  
   \demitab $S$ et $(\non S)$ sont vraies

\bigbreak

Alors toute relation est vraie ou fausse. Et $\bleu{R}$, en particulier :

   > $((\text{\non } R) \implies R)$ est vraie ; cf. axiome 4 : $(R \ou R \implies R)$  

\bigbreak

   \demitab Car le problème, c'est qu'on avait dit $\mauve{R}$ fausse (on avait posé $\mauve{non\ R}$),  
   \demitab et on reçoit le résultat que $\mauve{R}$ est vraie (parce qu'elle est vraie ou fausse en même temps),  
   \demitab et c'est pour cela que ça ne marche pas.

   > on a montré que $\mauve{R \implies (\non R)}$, ce qui est faux.

---

\avantage{On va utiliser le fait que dans la table de vérité de $\mathbf{P(x) \implies Q(x)}$}\   
\avantage{le seul cas faux est "\emph{P vrai tandis que Q est fausse}", pour mener ce raisonnement.}

   \begingroup \cadreGris

   > Pour montrer que \bleu{$x = \sqrt{2}$} est un irrationnel  
   > \begingroup \footnotesize (c'est à dire : qu'il peut s'écrire sous la forme $\frac{m}{n}$ où $m$ et $n$ sont des entiers relatifs ($\mathbb{Z}$) : c'est la définition de l'ensemble $\mathbb{Q}$) \endgroup
   > \    
   > pour montrer, donc, que \bleu{$x = \sqrt{2} \implies x \in \mathbb{R} \setminus \mathbb{Q}$}  
   > on va monter que \rouge{$x = \sqrt{2} \implies x \in \mathbb{Q}$} ne tient pas debout : la proposition est fausse.

   \endgroup

## 4) Le raisonnement par analyse / synthèse

Il sert à prouver l'existence d'un objet dont on ne connaît pas la valeur.

Par exemple, pour montrer qu'une fonction $\bleu{f}$  
\demitab est la somme d'une fonction paire et d'une fonction impaire,  
\demitab en analyse, on montrera que ces deux fonctions seraient candidates en les définissant par rapport à f,  
\demitab et en synthèse, que $f_{p}$ est bien paire, et $f_{i}$, impaire.

## 5) Le raisonnement par disjonction de cas

On énumère tous les cas possibles, et on les démontre un par un.

## 6) Démonstration d'une équivalence

Pour démonter $P \iff Q$, on démontre souvent :

- $P \implies Q$
- puis $Q \implies P$, sa réciproque.

## 7) Le raisonnement par récurrence 

### a) par récurrence simple

Soit $\bleu{P(n)}$ une proposition qui dépend d'un entier naturel $n$.

La \definition{récurrence simple} vérifie :

1. Initialisation : que $\bleu{P(n_{0})}$ est vraie,
2. Hérédité : que pour tout entier $\mauve{n \geq n_{0}, P(n) \implies P(n+1)}$.

### b) Par récurrence double

Même prémisses, mais :

1. Initialisation : $\bleu{P(n_{0})}$ et $\bleu{P(n_{0} + 1)}$ sont vraies,
2. Hérédité : pour tout entier $\mauve{n \geq n_{0}, P(n) \land P(n+1) \implies P(n+2)}$.

### 3) Par réccurence forte

1. Initialisation : $\bleu{P(n_{0})}$ est vraie,
2. Hérédité : pour tout entier $\mauve{n \geq n_{0}, P(n_{0}) \land P(n_{0} + 1) \land ... \land P(n) \implies P(n+1)}$.

