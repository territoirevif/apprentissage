---
title: "m301 : La logique des propositions"
subtitle: "Proposition, conjonction-disjonction, implication et équivalence"
category: mathématiques
author: Marc Le Bihan
keywords: 
- MVA003-01
- logique
- relation
- prédicat
- tautologie
- opérateur
- équivalence
- conjonction
- et
- disjonction
- ou
- négation
- not
- non
- proposition
- implication logique
- implication
- réciproque
- tiers exclus
- contraposition
- axiome
- théorème
- hypothèse
- proposition
- assertion
- déduction logique
- si ... alors
- modus ponens
- si et seulement si
- transitivité des déductions
- tiers exclu
- double négation
- contraposition
- formalisation du ET
- disjonction des cas OU
- Loi de Morgan
- nier une conjonction
- nier une disjonction
- commutativité
- associativité
- distributivité du ET sur le OU
- distirbutivité du OU sur le ET
---

# I) La logique : l'art de bien raisonner

La logique, c'est l'art de bien raisonner (Aristote).

Un raisonnement incorrect :  
\tab "_Tous les chats sont mortels, Socrate est un chat, donc Socrate est mortel_".

Un raisonnement correct :  
\tab "_Les hommes sont mortels, Socrate est un homme, donc Socrate est mortel_".

Notion de __Vrai__ et de __Faux__ :

   > c'est l'art de savoir déduire sans erreur __quelque-chose de vrai, à partir d'hypothèses supposées vraies__.

## 1) Les relations. Une relation pose une vérité

Une \definition{relation} est quelque-chose qui pose une vérité,  

   > à la différence des \definition{hypothèses} et \definition{propositions} qui peuvent se réveler vraies ou fausses.  
   >
   > (et les \definition{assertions} peuvent aussi être vraies ou fausses, même si souvent ce mot appuie l'idée qu'on les pense vraies).

   \begingroup \cadreAvantage
 
   > $5 + 4 = 8$ est __faux__  
   > $19 = 9 + 10$ est __vrai__  
   > $4 + 8$ __n'est pas une proposition__

   \endgroup

\bigbreak

Parmi les relations qui sont vraies, sont :

  - L'\definition{axiome}, une propriété supposéee vraie, qu'on ne démontre pas.
  - Le \definition{théorème} : des propriétés vraies obtenues par des déductions logiques

On les écrit avec des lettres majuscules : __P__, __Q__...

  > $P \equiv \mathbf{V}$ la proposition est vraie  
  > $P \equiv \mathbf{F}$ la proposition est fausse

# II) Les opérateurs fondamentaux

Des relations naissent les \definition{opérations fondamentales} : 

   - négation, 
   - conjonction "ou"

## 1) Négation d'une proposition "non"

Relation $R$ : $\non R$ exprime la négation de cette relation : "_Socrate n'est pas un homme_".

On l'écrit :  
\tab $\mathbf{\non P}$   
\tab $\mathbf{\lnot P}$  
\tab $\mathbf{\overline{P}}$  

\bl $(\non (\non P) \equiv P)$ \endgroup

   > $\non(x \in A) \equiv x \notin A$  
   > $\non(x \leq 1) \equiv x > 1$

## 2) Disjonction "ou"

Le "__ou__", logique de \definition{disjonction} :

((_Socrate est un homme_) ou (_6 n'est pas un nombre premier_))

| P | Q | $P \ou Q$ 
|---|---| :--------:
| V | V | V          
| V | F | V
| F | V | V
| F | F | F

\avantage{Ces deux opérateurs suffiraient à faire toute la logique...}

# III) Les autres opérateurs logiques

   - disjonction (et), 
   - implication logique ($\implies$),
   - équivalence logique {$\iff$, ssi}

## 1) Conjonction "et"

Dans la table de vérité de "__et__", logique de \definition{conjonction} :

| P | Q | $P \et Q$ 
|---|---| :----------:
| V | V | V          
| V | F | F
| F | V | F
| F | F | F

## 2) L'implication logique "$\implies$"

$(R \implies S)$ désigne la relation $((\non R) \ou S)$.

   > "_P \definition{implique} Q_"

   > "_\definition{Si} P \definition{alors} Q_"  

(remarque : $(A \implies B)$ signifie aussi $((\non B) \implies (\non A))$).

\bigbreak

\danger{Une proposition fausse donne toujours une valeur de vérité vraie},  
par l'implication $R \implies S$.

\bigbreak

   \begingroup \cadreGris

   > \danger{\emph{Si vous partez de quelque-chose de faux, vous pouvez en déduire n'importe quoi}}.  
   >
   > Deux relations, $R$ et $S$ : 
   > \   
   > \tab Si $R$ est fausse, alors l'implication $R \implies S$ est vraie,  
   > \   
   > \tab De fait, peut-on déduire que $S$ est vraie... ?  
   > \tab \textbf{Non !} Car autant pour $S=V$ que $S=F$, $R \implies S$ est vrai :  
   > \tab \danger{on ne peut rien décider sur $S$ !}

   \endgroup

\bigbreak

(par contre, si R est vraie, $R \implies S$ ne sera vraie que si S est vraie).

   \begingroup \cadreAvantage

   > L'implication, c'est le raisonnement à partir de situations où R est vraie.

   \endgroup

\bigbreak

\avantage{On peut voir l'implication comme l'\definition{inclusion} d'un ensemble dans un autre : P dans Q.}

$P \implies Q \equiv (\non P)\ou Q$

\attention{Attention à la réciproque !} La \definition{réciproque} de l'implication $P \implies Q$,  
$Q \implies P$ \attention{\underline{n'a pas}} la même table de vérité que $P \implies Q$.

   > _Il pleut $\implies$ le sol est mouillé_, n'implique pas que sa réciproque :  
   > _Le sol est mouillé $\implies$ il pleut_ soit vraie.

\bigbreak

   \begingroup \cadreGris
   
   > \textbf{\underline{Le modus ponens :}} \newline
   >
   > Si $\mauve{R}$ est vrai et $(\mauve{R} \implies \bleu{S})$ est vrai,  
   > \tab alors $\bleu{S}$ est vrai

   \endgroup

## 3) L'équivalence logique ("$\iff$")

Par définition, $(R \iff S)$ désigne la relation :
   $$ ((R \implies S) \et (S \implies R)) $$

les propositions sont \definition{équivalentes} si elles ont la même table de vérité.

"_\definition{Si et seulement si}_"

   > $(P \iff Q) \equiv (P \implies Q) \et (Q \implies P)$

### \underline{Remarque :}

   > \begingroup \large $(R \iff S) \iff ((R \et S) \ou ((\non R) \et (\non S)))$ \endgroup

# IV) La logique mathématique et ses six axiomes

Une théorie mathématique peut avoir des \definition{axiomes} : des propriétés supposées vraies, sans démonstration.

\begingroup \cadreAvantage

> Par exemple, la géométrie euclidienne, dit que pour une droite $(D)$,  
par un point $P$ qui n'appartient pas à $(D)$ on peut faire passer une unique parallèle.  
>   \
> C'est un axiome : on n'a jamais réussi à le démontrer.  
> l'on peut trouver des géométries non eclidiennes, qui ne se fondent pas sur cet axiome.

\endgroup

\bigbreak

D'après les __axiomes__, avec \bleu{$R$}, \bleu{$S$} et \bleu{$T$} des relations : 

   1. $((R \ou R) \implies R)$ est vraie
   2. $((R \implies (R \ou S))$ est vraie
   3. $((R \ou S) \implies (S \ou R))$ est vraie
   4. $((R \implies S) \implies ((R \ou T) \implies (S \ou T)))$ est vraie

On tire six théorèmes.  
Les \definition{théorèmes} sont eux des propriétés vraies, obtenues par déductions logiques des __axiomes__.

## Th 1 : Transitivité des déductions

Si $(R \implies S) \et (S \implies T)$ sont vraies, alors

   > $(R \implies T)$ est vraie

## Th 2 : Tiers exclu

Les relations :

   > $(R \implies R)$  
   > $((\non R) \ou R)$ 

sont vraies.

| R                | V | F |
| ---------------- | - | - |
| (non $R$)        | F | V |
| (non $R$ ou $R$) | V | V |

Quelque-chose est soit vrai, soit faux.

   - Mais __pas les deux à la fois__,
   - __ni l'un, ni l'autre à la fois__.

## Th 3 : Double négation

La relation :

   > $(R \mauve{\iff} \non (\non R))$ est toujours vraie

$R$ et $(\non (\non R))$ ont toujours la même valeur de vérité.

## Th 4 : Contraposition{#contraposition}

[voir aussi : les raisonnements](./mathematiques-m093-les-raisonnements.pdf#contraposition)

La relation $((R \implies S) \mauve{\iff} ((\non S) \implies (\non R)))$ est vraie.

|                            |   |   |   |   |
| -------------------------- | - | - | - | - |
| $R$                        | V | V | F | F |
| $S$                        | V | F | V | F |
| -------------------------- | - | - | - | - |
| $\non S$                   | F | V | F | V |
| $\non R$                   | F | F | V | V |
| -------------------------- | - | - | - | - |
| $(\non S \implies \non R)$ | V | F | V | V |
| $(\non R \implies \non S)$ | V | F | V | F |

(\attention{attention ! L'ordre des propriétés, R et S, sont inversées !})

   > $(x > 3 \implies x > 1) \equiv (x \leq 1 \implies x \leq 3)$

\avantage{Il peut être plus utile de se servir de la contraposée pour démontrer quelque-chose.}

   > \underline{Exemple :} pour démontrer que si "_$n^2$ est \bleu{impair}, alors $n$ est impair également_", ce qui est difficile, mieux vaut démonter que "_si $n$ est \bleu{pair}, alors $n^2$ est pair_".

## Th 5 : Formalisation du "et"

Le "et" logique formalise la locution usuelle "et" (ce qui n'est pas le cas du "ou" : le serveur qui demande "_fromage ou dessert_" ne souhaite pas que vous preniez l'un et l'autre).

$R$ et $S$ sont deux relations :

   > $((R \text{ et } S) \implies R)$  
   > $((R \text{ et } S) \implies S)$

sont vraies, et si $R$ et $S$ sont toutes deux vraies, alors $(R \text{ et } S)$ est vrai.

### \underline{Prorpriété :}

   > Symétrie du "et" :

   >  > $(R \et S) \iff (S \et R)$

### [Le raisonnement par l'absurde](mathematiques-m093-les-raisonnements.pdf#absurde)

## Th 6 : Théorème de disjonction des cas "ou"

$R$, $S$ et $T$ sont trois relations. Si :

   > $(R \text{ ou } S)$  
   > $(S \implies T)$  
   > $(R \implies T)$

sont vraies toutes les trois, alors $T$ est vraie.

### \underline{Prorpriété :}

   > Symétrie du "ou" :

   >   > $(R \ou S) \iff (S \ou R)$

# IV) Autres propriétés et lois

## 1) Les lois de De Morgan (19ème siècle)

### a) Nier une disjonction

   > \begingroup \large $\non(P \ou Q) \iff \non P \et \non Q$ \endgroup

(ce qui est la définition du "et").

### b) nier une conjonction : 

   > \begingroup \large $\non(P \et Q) \iff \non P \ou \non Q$ \endgroup 

## 2) Propriété : commutativité

   > du "et" : $P \et Q \iff Q \et P$  
   > du "ou" : $P \ou Q \iff Q \ou P$

## 3) Propriété : associativité

   > du "et" : $P \et (Q \et R) \iff (P \et Q) \et R$  
   > du "ou" : $P \et (Q \ou R) \iff (P \ou Q) \ou R$

## 4) Propriété : distributivité du ET sur le OU, et du OU sur le ET

   > du "et" sur le "ou" : $P \et (Q \ou R) \iff (P \et Q) \ou (P \et R)$  
   > du "ou" sur le "et" : $P \ou (Q \et R) \iff (P \ou Q) \et (P \ou R)$

\bigbreak

   \begingroup \cadreAvantage
   
   > La multiplication est distributive par rapport à l'addition :  
   > \   
   > \tab $2 \times (5 + 6) = (2 \times 5) + (2 \times 6)$  
   > \   
   >     
   > le \textbf{$\times$} joue le rôle du \textbf{et},  
   > le \textbf{+} joue le rôle du \textbf{ou}  
   
   \endgroup

\tab \gris{l'addition \underline{n'est pas} distributive par rapport à la multiplication :}

\tab \gris{$2 + (5 \times 6) \neq (2 + 5) \times (2 + 6)$ : l'un donne 32 et l'autre 56.}

## 5) La logique peut se faire piéger ! Don Quichotte (Cervantès)

_Le voyageur dit la phrase de son choix :  
\tab - si sa phrase est vraie, il sera pendu,  
\tab - si sa phrase est fausse, il sera noyé._

Comment fait-il pour sauver sa peau ?

   > Il répond : "_je serai noyé_".

C'est une contradiction : une des phrases du langage qui anmène le paradoxe (le tiers exclu).

Autre exemple : "_je mens_" (en le disant : je mens, donc je dis la vérité, et si je dis la vérité : je mens).

