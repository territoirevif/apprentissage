---
title: "m604. Trigonométrie : les arcs associés par symétrie ou par rotation"
author: Marc Le Bihan
---

![Arcs associés par symétrie 1](images/arc_associes_rotation_symetrie_1.jpg){width=80%}

![Arcs associés par symétrie 2](images/arc_associes_rotation_symetrie_2.jpg){width=80%}

