---
title: "m605. Trigonométrie : les principales relations entre cosinus, sinus et tangente"
subtitle: ""
author: Marc Le Bihan
abstract: |
   Formules d'addition et de duplication,  
   Linéariser $\cos^2(x)$ et $\sin^2(x)$,  
   Formules de transformation de produits en sommes et de sommes en produits
---

## 1) Relations principales

   > $\arccos{(x) + \arcsin{(x)}} =$ \bl $\frac{\pi}{2}$ \endgroup

   > $\tan(\alpha) =$ \bl $\frac{\sin(\alpha)}{\cos(\alpha)}$ \endgroup

   > $\text{cotan}(\alpha) =$ \bl $\frac{1}{\tan(\alpha)}$ \endgroup

## 2) Autres formules

$$ 
\rouge{\cos^2 \alpha} = \frac{1}{1 + tan^2 \alpha} \\
\resolu{\sin^2 \alpha} = \frac{\tan^2 \alpha}{1 + \tan^2 \alpha}
 $$

Mais attention, \attention{ce sont des carrés !}  
et selon le quadrant, le signe des $\mauve{\cos \alpha}$ et $\mauve{\sin \alpha}$ diffère :

\bigbreak

\demitab \underline{dans les quadrants I et IV :}  
   $$ 
   \rouge{\cos \alpha} = \frac{1}{1 + \sqrt{\tan^2 \alpha}} \\
   \resolu{\sin \alpha} = \frac{\tan \alpha}{\sqrt{1 + \tan^2 \alpha}}
   $$

\bigbreak

\demitab \underline{dans les quadrants II et III :}  
   $$ 
   \rouge{\cos \alpha} = \frac{-1}{1 + \sqrt{\tan^2 \alpha}} \\
   \resolu{\sin \alpha} = \frac{-\tan \alpha}{\sqrt{1 + \tan^2 \alpha}}
   $$

## 1) Doublement d'angle ($2x$)

   > $\cos(2x) = 2 \cos^2(x) - 1$  
   > $\cos(2x) = 1 - \sin^2(x)$  
   > $\cos(2x) = \cos^2(x) - \sin^2(x)$

   > $\sin(2x) = 2\sin(x) \cos(x)$

## 2) Additions et soustractions d'angles

   > $\cos(u + v) = \cos(u) \cos(v) - \sin(u) \sin(v)$  
   > $\cos(u - v) = \cos(u) \cos(v) + \sin(u) \sin(v)$

   > $\sin(u + v) = \sin(u) \cos(v) + \cos(u) \sin(v)$  
   > $\sin(u - v) = \sin(u) \cos(v) - \cos(u) \sin(v)$

## 3) Multiplications (de cosinus, de sinus, de cosinus et de sinus)

   > $2\cos(u) \cos(v) = \cos(u + v) + \cos(u - v)$  
   > $2\sin(u) \cos(v) = \sin(u + v) + \sin(u - v)$  
   > $2\sin(u) \sin(v) = \cos(u - v) - \cos(u + v)$

## 4) Mise au carré de cosinus et de sinus

   > $\sin^2(\alpha) + \cos^2(\alpha) = 1$

   > $\cos^2(x) =$ \bl $\frac{1 + \cos(2x)}{2}$ \endgroup

   > $\sin^2(x) =$  \bl $\frac{1 - \cos(2x)}{2}$ \endgroup

## 5) Addition et soustraction de cosinus et de sinus

   > $\cos{(u)} + \cos{(v)}=$ \bl $2 \cos{(\frac{u + v}{2}) \cos{(\frac{u - v}{2}})}$ \endgroup  

   \begingroup \small

   > > $\gris{\cos{(90)} + \cos{(30)} = 2 \cos{(\frac{90 + 30}{2})} \cos{(\frac{90 - 30}{2})}}$  
   > > \    
   > > \demitab $\gris{\iff \cos{(120)} = 2 \cos{(\frac{120}{2})} \cos{(\frac{60}{2})} = 2 \cos{(60)} \cos{(30)}}$  
   > > \demitab $\gris{\iff -0.5 = 2(-0.5 \times 0.5) = -0.5}$

   \endgroup
   
   > $\cos{(u)} - \cos{(v)}=$ \bl $-2 \sin{(\frac{u + v}{2}) \sin{(\frac{u - v}{2}})}$ \endgroup

   > $\sin{(u)} + \sin{(v)}=$ \bl $2 \sin{(\frac{u + v}{2}) \cos{(\frac{u - v}{2}})}$ \endgroup  

   > $\sin{(u)} - \sin{(v})=$ \bl $2 \sin{(\frac{u + v}{2}) \cos{(\frac{u - v}{2})}}$ \endgroup  

