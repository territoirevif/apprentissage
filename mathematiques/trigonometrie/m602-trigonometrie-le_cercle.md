---
title: "Le cercle trigonométrique"
subtitle: "m602. Trigonométrie"
author: Marc Le Bihan
---

## 1) Le cercle trigonométrique

Un cercle trigonométrique a un centre O, un rayon de longueur $\mathbf{1}$. Ses angles se mesurent en radians :

  \demitab \avantage{un arc mesure un radian si sa longueur est celle du rayon du cercle.}

![Longueur des sinus, arc, tangente](images/tailles_sin_cos_tan_arc_cercle.jpg){width=80%}

   - $\mathbf{\rouge{A}}$ est le point sur le cercle dont on veut la mesure
   - $\mauve{\alpha}$ la mesure de l'angle orienté $\widehat{(\overrightarrow{\text{OD}}, \overrightarrow{\text{OM}})}$
   - $\mauve{x}$ est la longueur de l'arc qui y mène : celle de la portion de cercle (AD)

       sa longueur est $\rouge{L} \text{ ou } \rouge{AD} = R . \alpha$
 
## 2) Le sinus et le cosinus, qui sont-ils, d'où viennent-ils ?

\bleu{Pourquoi les sinus et cosinus sont-ils là ?}

Ils sont là à cause des angles.  
Et les angles, on en a besoin pour des problèmes de direction. (vers où va t-on ?)

Sur le cercle trigonométrique, 

  \demitab \mauve{$\cos(x)$} est l'abscisse d'un point $\mathbf{A}$ sur le cercle  
  \demitab \mauve{$\sin(x)$} son ordonnée

### a) Définition des sinus, cosinus, tangente

Les \definition{sinus}, \definition{cosinus} et \definition{tangente} sont définis à partir de rapports entre deux côtés d'un triangle rectangle.

![La définition des cosinus, sinus, tangente](images/rapports_côtés_dans_triangles_cosinus_sinus_tangente.jpg)
$$ \sin(\alpha) = \frac{\text{côté opposé à }\alpha}{\text{hypothénuse}} $$
$$ \cos(\alpha) = \frac{\text{côté adjacent à }\alpha}{\text{hypothénuse}} $$
$$ \tan(\alpha) = \frac{\text{côté opposé à }\alpha}{\text{côté adjacent à } \alpha} $$

### b) Pythagore et le triangle rectangle lient la valeur du sinus à celle du cosinus

\definition{Pythagore} dit que dans un triangle rectangle : $\mauve{OM^2 = HM^2 + OH^2}$  
   \demitab \gris{(avec, pour notre dessin, $A$ qui est le point $M$, et $C$ qui est $H$ : $OA^2 = CA^2 + OC^2$)}

De là, vient que :

   \tab $\mauve{\sin^2(\alpha) + \cos^2(\alpha) = 1}$

   \tab \demitab \gris{$\mathbf{1}$ est la longueur de notre rayon, et $M$ étant sur le cercle, OM a pour longueur $1$}  
   \tab \demitab \gris{$\text{HM}$ ou $\text{CA}$ ont la valeur du $\sin(x)$}  
   \tab \demitab \gris{$\text{OH}$ ou $\text{OC}$ ont la valeur du $\cos(x)$}

C'est cette formule qui lie les valeurs des sinus et des cosinus entre elles  
   \tab et fait que pour un point donné, elles ne sont pas indépendantes.

## 3) Utilisation pratique des sinus et cosinus en s'inspirant du cercle

### a) De combien j'avance en x et y, quand je vais dans une direction une certaine distance ?

Les sinus et cosinus sont un moyen pratique de répondre à cette question :  
"\emph{\bleu{Si j'avance de 5 mètres, à 25° vers l'est (0° étant le Nord), de quelle distance en $\mauve{x}$ et en $\mauve{y}$ vais-je avancer ?}}"

\demitab en $x$ : $\cos(25^{\circ}) \times 5 = 4.53$

\demitab en $y$ : $\sin(25^{\circ}) \times 5 = 2.11$

\bigbreak

\demitab avec : $\sqrt{4.53^2 + 2.11^2} = 5$  
\tab $\rightarrow$ on a bien le rayon de $5$ mètres.

### b) Et si j'avance en x et en y, quelle distance ai-je parcourue, dans quelle direction ?

"\emph{\bleu{Si j'avance de 3.28 mètres à l'est, et 5.70 mètres au nord, quelle distance ai-je parcourue dans quelle direction ?}}"

\demitab la distance parcourue est $\sqrt{(3.28)^2 + (5.70)^2} = 6.58\text{ mètres}$

\demitab alors, on a :

   \tab $\cos(\theta) \times 6.58 = 3.28\text{ mètres} \rightarrow \cos(\theta) = 0.498757$  
   \tab $\sin(\theta) \times 6.58 = 5.70\text{ mètres} \rightarrow \sin(\theta) = 0.866742$

\demitab ce qui donne : $\theta = 60.08^{\circ}$

\bigbreak

On a avancé de $6.58$ mètres, direction : $60.08^{\circ}$ Nord-Est.

## 4) Les valeurs des cosinus et sinus

### a) Pourquoi les sinus et les cosinus valent-ils entre -1 et 1 ?

Parce que si je décide de me promener 500 mètres,  
\demitab je peux au mieux aller $500$ mètres à droite, $-500$ mètres (à gauche),  
\demitab $500$ mètres en haut ou $-500$ mètres (en bas).

Sinon, je dépasse la distance que j'ai dite que j'allais me promener...

Je peux faire une combinaison : je vais $-433$ mètres (à gauche) et $250$ mètres en haut.  
\demitab (c'est à dire : dans une direction de $150^\circ$)

  > On a l'impression de parcourir plus que 500 mètres dans ce cas-là, non ?  
  > Eh bien, non ! On fait bien 500 mètres de promenade, exactement !  
  > \linebreak
  > \gris{$500 \text{ mètres} \times \cos^2(150^\circ) + 500 \text{ mètres} \times \sin^2(150^\circ)$}  
  > \demitab \gris{$=500 \text{ mètres} \times (\frac{-\sqrt{-3}}{2})^2 + 500 \text{ mètres} \times ({\frac{1}{2}})^2$}  
  > \demitab \gris{$=500 \text{ mètres} \times 0.75 + 500 \text{ mètres} \times 0.25 = 500 \text{ mètres}$}

### b) Les principales valeurs des cosinus, sinus, tangentes

![Principaux cosinus, sinus, tangentes](images/principaux_cosinus_sinus_tangentes.jpg){width=60%}

|        $x$        |    $0$    | $30^\circ$ ou $\frac{\pi}{6}$ | $45^\circ$ ou $\frac{\pi}{4}$ | $60^\circ$ ou $\frac{\pi}{3}$ | $90^\circ$ ou $\frac{\pi}{2}$ |
|:-----------------:|:---------:|:-----------------------------:|:-----------------------------:|:-----------------------------:|:-----------------------------:|
| $\mauve{\sin(x)}$ | \mauve{0} |     $\mauve{\frac{1}{2}}$     | $\mauve{\frac{\sqrt{2}}{2}}$  | $\mauve{\frac{\sqrt{3}}{2}}$  |       \mauve{1}\newline       |   
| $\rouge{\cos(x)}$ | \rouge{1} | $\rouge{\frac{\sqrt{3}}{2}}$  | $\rouge{\frac{\sqrt{2}}{2}}$  |     $\rouge{\frac{1}{2}}$     |       \rouge{1}\newline       |   
| $\bleu{\tan(x)}$  | \bleu{0}  |  $\bleu{\frac{1}{\sqrt{3}}}$  |          $\bleu{1}$           |       $\bleu{\sqrt{3}}$       |      \bleu{Non définie}       |   

- Se rappeler $12$, le nombre du marchand, pour les valeurs de $x$.

### c) Tangente et cotangente

On définit la tangente par $\rouge{\tan(x)} = \frac{\sin(x)}{\cos(x)}$  
et la cotangente par $\rouge{cotan(x)} = \frac{\cos(x)}{\sin(x)} = \frac{1}{\tan(x)}$

### d) Conversions

- De degrés en radians

- De radians en degrés

La \definition{mesure principale d'un angle} est $]-\pi, \pi]$
