---
title: "m001. Entiers, entiers relatifs, rationnels, complexes"
subtitle: ""
category: mathématiques

keywords:
- ensemble
- nombre
- ordinal
- cardinal
- entier naturel
- Peano
- axiomes de Peano
- nombre décimal
- nombres décimaux
- nombre entier
- nombres entiers
- nombre rationnel
- entier
- rationnel
- complexe
- nombre complexe
- entier relatif
- ensemble dense
- suite décimale

abstract: | 
   Les nombres entiers, relatifs, rationnels, complexes, et leur inclusion les uns dans les autres.   
   Les axiomes de Peano. Les cardinaux et ordinaux.

author: Marc Le Bihan
---

Les ensembles de nombres sont arrivés par besoin :

   - d'ordonner des choses (ordinaux)
   - de les numéroter
   - les compter (cadinaux), quand ils sont dans une collection

\bigbreak

## 1) L'ensemble des nombres naturels

\bleu{$\mathbb{N}$} est l'ensemble des nombres naturels : $\mauve{0, 1, 2, 3...}$

   \begingroup \cadreGris

   > \textbf{Les axiomes de} \definition{Peano}
   >
   > 1. Tout entier naturel a un successeur et un seul
   > 2. Deux entiers distincts ne peuvent avoir le même successeur
   > 3. Tout entier autre que zéro, a un précédent
   > 4. Axiome de récurrence

   \endgroup

## 2) L'ensemble des nombres relatifs

\bleu{$\mathbb{Z}$} celui des nombres relatifs, positifs ou négatifs : $\mauve{\mathbb{Z} = \mathbb{N} \cup \{-1, -2, -3...\}}$

   \begingroup \cadreGris

   > \textbf{Le troisième axiome de Peano devient :}
   >
   > 3. Tout entier relatif a un précédent

   \endgroup

## 3) L'ensemble des nombres décimaux

\bleu{$\mathbb{D}$} des nombres décimaux : \mauve{$1.52$, $-0.54$},  
\tab ($27$ ou $-514$ aussi car on peut leur ajouter un $\mathbf{.0}$ : $27.0$, etc.)

## 4) L'ensemble des rationnels

\bleu{$\mathbb{Q}$} des rationnels : toute fraction positive ou négative : \begingroup \large $\mauve{\frac{1}{3}, \frac{-1}{7}...}$ \endgroup

   - Il permet de considérer des ensembles \definition{denses} : pourvus d'une infinité de valeurs

   > \textbf{Le premier axiome de Peano disparaît :}
   >
   > 1. \gris{"Tout entier relatif a un précédent"} n'est plus applicable

## 5) L'ensemble des réels

\bleu{$\mathbb{R}$} les réels : tous les nombres ou suites décimales limitées ou illimitées :  
\tab$\mauve{\frac{-1}{2}, 0.32, 1.5555...}$

   > ou approcher des nombres comme \mauve{$\sqrt{2}$} 

## 6) L'ensemble des nombres complexes

\bleu{$\mathbb{C}$} des nombres complexes :

   > pour déterminer la valeur de nombres comme \mauve{$\sqrt{-1}$}

   > - Ils font qu'une équation algébrique de degré $\bleu{n}$ a toujours $\bleu{n}$ solutions dans $\bleu{\mathbb{C}}$, ce qui n'est pas le cas dans $\mathbb{R}$.  
   > - Graphiquement, un nombre complexe est représenté dans un plan, et dans ce plan se trouve aussi la droite des réels.

   > ![Les ensembles de nombres](images/les-ensembles-de-nombres.jpg)

