---
title: "m002. Commutativité, associativité des opérations"
subtitle: ""
author: Marc Le Bihan
keywords:
- addition
- multiplication
- commutativité
- associativité
- distributivité
- priorité
- division
- opération
---

## 1) L'addition

est \definition{commutative}

   > $a + b = b + a$

\bigbreak

\definition{associative}

   > $(a + b) + c = a + (b + c)$

\bigbreak

et l'on peut faire une somme de termes sans ambiguïté :

   > $S = a + b + c + d + e$

## 2) La multiplication

est \bleu{commutative} et \bleu{associative}

\bigbreak

elle est \definition{distributive} par rapport à l'addition (ou la soustraction)

   > $a \times (b + c) = (a \times b) + (b \times c)$

\bigbreak

La multiplication et la division sont prioritaires sur l'addition et la soustraction.
