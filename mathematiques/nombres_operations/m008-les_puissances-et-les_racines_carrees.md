---
title: "m008. Les puissances et les racines carrées" 
subtitle: ""
abstract: |
   Inverse ou quotient de puissances, puissance à la puissance, propriétés des racines carrées
author: Marc Le Bihan
keywords:
- puissance
- racine carrée
- exposant
- inverse d'une puissance
- puissance de 10
---

# I) Les puissances

### Par convention

   - Pour tout réel non nul, \begingroup \large $\bleu{a^{0} = 1}$ \endgroup 
   - $0$ à toute puissance, vaut toujours $0$, \begingroup \large $\bleu{0^{n} = 0}$ \endgroup  
   - $\rouge{0^{0}}$ n'est pas défini.

### Propriétés

Quand :  

   > on multiplie deux mêmes termes à la puissance, leurs exposants s'additionnent :  
   > \begingroup \large $\bleu{a^{n} \times a^{m} = a^{n + m}}$ \endgroup \linebreak
   > \   
   > on élève à la puissance un terme à la puissance, les exposants se multiplient :  
   > \begingroup \large $\bleu{(a^{n})^{m} = a^{n.m}}$ \endgroup  

### Inverse d'une puissance

Pour $a$ non nul, $a^{n}$ existe, et : 

   > \begingroup \large $\bleu{(a^{-n}) = \frac{1}{a^{n}}}$ \endgroup

### Quotients

   > \begingroup \large $\bleu{\frac{a^{n}}{a^{m}} = a^{n - m}}$ \endgroup \linebreak
   > \  \linebreak
   > et par là : $\bleu{(a^{n})^{-m} = a^{-nm}}$  
   > \demitab et de fait : $\bleu{(ab)^{-m} = a^{-m}b^{-m}}$

### Puissances de dix

On écrira souvent :

   > $10^{3}$ pour $1000$  
   > ou $1,2.10^{2} = 120$  
   > et aussi $0.03 = 3.10^{-2}$

## Ce qu'on trouve dans les exercices

   - Simplifier des puissances
   - Ecrire un nombre sous la forme : $a.10^{b}$

# II) Les racines carrées

\underline{Définition :}

   > Si $\bleu{a}$ est un nombre positif, on appelle __racine carrée de $a$__ 
   > le nombre $A$ noté $\bleu{\sqrt{a}}$, positif, qui élevé au carré, donne $a$ :
   $$ \bleu{A = \sqrt{a} \iff A \geq 0 \text{ et } A^2 = a} $$

   > $\sqrt{0} = 0$,  
   > $\sqrt{1} = 1$

\bigbreak

Si $\bleu{a}$ est un nombre positif, il existe \attention{deux nombres} dont le carré vaut $a$ :

   > $\bleu{\sqrt{a}} \text{ et } \bleu{-\sqrt{a}}$

## 1) Propriétés

   - Si $a$ et $b$ sont positifs ou nuls : $\sqrt{ab} = \sqrt{a}\sqrt{b}$
   - Si $a \geq 0$ et $b > 0$ : $\sqrt{\frac{a}{b}} = \frac{\sqrt{a}}{\sqrt{b}}$

   - $\sqrt{a^{n}} = (\sqrt{a})^{n}$, avec $n$ : entier
   - Si $a \geq 0$, $\sqrt{a}$ peut être noté $a^{\bleu{\frac{1}{2}}}$   
     \gris{car ${(a^{\frac{1}{2}}})^{2} = a$}
   - Si $a \geq 0$, $\sqrt{a} \times \sqrt{a} = a$
