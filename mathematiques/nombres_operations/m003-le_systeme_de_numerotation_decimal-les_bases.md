---
title: "m003. Le système de numérotation décimal et les bases"
subtitle: ""
author: Marc Le Bihan
keywords:
- système de numérotation
- comparaison
- ordres
- base
- binaire
- hexadécimal
- duodécimal
---

Les systèmes de numérotation ont pour but :

   - d'écrire tous les entiers avec seulement dix symboles
   - de permettre la comparaison rapide entre deux entiers
   - de simplifier les calculs entre entiers

## 1) Le comptage en base dix

Consiste à créer des \definition{ordres} successifs :  
   \demitab __unités__, __dizaines__, __centaines__, __milliers__...

où chaque unité d'un ordre vaut 10 du précédent.

\bigbreak

Le __0__ marque l'absence d'un ordre, sans modifier la position des autres.

   > __135__ \tab Cent, trente, cinq

## 2) Les autres bases

   - binaire (base 2)
   - hexadécimal (base 16)
   - duodécimale (base 12)
   - chiffres romains...

## 3) Programmation

   \code

   > ```java
   > String nombre = Integer.toString(Integer.parseInt(nombre, baseDepart), baseArrivee);
   > ```

   \endgroup

\bigbreak

\underline{Exemple: $103$ base $16$ ($259$ en décimal) converti en base $29$:}

   \demitab Résultat attendu : $259 = 8 \times 29 + 27 = \text{8R}$  
	  \begingroup \small \gris{\demitab ($A = 10$, $B = 11$, ..., $R = 27$)} \endgroup

\bigbreak

   \code

   > ```java
   > assertEquals("8r",  Integer.toString(Integer.parseInt("103", 16), 29), 
   >    "Mauvaise conversion de base 16 en base 29");
   > ```

   \endgroup

## 4) Application numérique

\begingroup \cadreAvantage
   
> \textbf{Combien de chiffres utilise t-on pour écrire les nombres de $1$ à $1387$ ?}  
> \   
> $1$ à $9$ : \ \ \ \ \ \ \ \ \ 1 chiffre \ $\times$ 9 = 9 chiffres  
> $10$ à $99$ : \ \ \ \ \ \ 2 chiffres $\times$ 90 = 180 chiffres  
> $100$ à $999$ : \ \ \ 3 chiffres $\times$ 900 = 2700 chiffres  
> $1000$ à $1387$ : 4 chiffres $\times$ 388 = 1552 chiffres  
> \   
> soit 4441 chiffres au total
 
\endgroup

