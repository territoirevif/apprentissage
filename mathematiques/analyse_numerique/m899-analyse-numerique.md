---
title: "m899 : Analyse Numérique"
subtitle: "Fiche de Lecture (à re-répartir)"
category: mathématiques
author: Marc Le Bihan
---

La plupart des phénomènes

   - physiques
   - chimiques
   - biologiques

sont régis par des systèmes complexes d'équations aux dérivées parielles

\bigbreak

   \demitab \definition{Dérivée partielle} : la dérivée partielle d'une fonction de plusieurs variables  
   \tab est sa dérivée par rapport à l'une de ses variables,  
   \tab les autres étant gardées constantes.

# Introduction

## 1) Les types d'équations 

   - élliptique : décrivent des phénomènes de diffusion stationnaire
   - parabolique : de diffusion évolutive
   - hyperbolique : de transport à vitesse finie

## 2) Méthodes numériques

Les méthodes numériques sont classées par catégories

   - différences finies
   - éléments finis
   - volumes finis
   - méthodes spectrales   
   [...]

### a) Outils de base requis

   - interpolation polynomiale
   - dérivation
   - intégration numérique d'une fonction

# Chapitre 1 : Interpolations

Définir des polynômes ou des courbes à partir de quelques points

[Méthode de Lagrange](./m805-interpolation-par-la-methode-de-lagrange.pdf)


