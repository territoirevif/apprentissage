---
title: "m805 : Interpolation par la méthode de Lagrange"
subtitle: ""
category: mathématiques
author: Marc Le Bihan
keywords: 
- interpolation
- points
- valeurs
---

## 1) Les interpolations et les problèmes qu'elles posent

Une \definition{interpolation}, c'est :

   - construire une courbe à partir des données d'un nombre fini de points
   - ou une fonction à partir de données d'un nombre fini de valeurs

### a) Objectif : trouver un polynôme à partir de queqlues points

\underline{But:} trouver un polynôme \bleu{$p$} du degré \bleu{$n$} que l'on veut, qui : 

   \demitab pour ses valeurs\  : $x_{0} = t_{0}$, $x_{1} = t_{1}$, $x_{2} = t_{2}$, ..., $x_{n} = t_{n}$   
   \demitab vaut \tab \tab : $v_{0}$, $v_{1}$, $v_{2}$, ..., $v_{n}$

   \demitab \underline{Exemple :}  
      \tab pour un polynôme de degré $\bleu{2}$, je voudrais que :  
      \tab \demitab $\mauve{p(-1) = 5}$ \demitab \gris{$t_{0} = -1$, $v_{0} = 5$}  
      \tab \demitab $\mauve{p(0) = -3}$  
      \tab \demitab $\mauve{p(1) = 2}$

### b) Il faut un système avec n+1 [n = degré du polynôme] équations pour définir ce polynôme

Il faut une relation de plus que de degré que ce polynôme a, c.à.d. $n + 1$, pour définir ce polynôme  
   \demitab parce qu'une équation $\mauve{y = ax + b}$ de degré $\mauve{1}$

   - réclame deux points $\mauve{(x_{0}, y_{0})}$ et $\mauve{(x_{1}, y_{1})}$ pour être définie 
   - et demande à trouver $\mauve{a}$, $\mauve{b}$ pour être résolue

   \demitab Ce qui fait bien \mauve{$2$} relations : une de plus que le degré $\mauve{1}$ qu'il a.

### c) mais tenter de résoudre un système d'équations pour l'obtenir devient vite trop ardu

   \demitab on pourrait résoudre un système d'équations, comme ça ou avec une matrice.  
   \demitab mais ce serait très fastidieux

## 2) Comment fonctionne l'interpolation de Lagrange ?

Il faut vérfier $\bleu{n+1}$ relations pour définir le polynôme \bleu{p}

   1. Dans un premier temps, on va définir $\mauve{n+1}$ polynômes $\varphi_{\mauve{k}}, \mauve{k} \in [0,n]$  
      que l'on va appeler \definition{bases de Lagrange}, et qui auront la propriété que :   

      - pour le paramètre $t_{\mauve{k}}$, $\varphi_{\mauve{k}}$ vaudra $\mathbf{1}$  
      - mais que pour tous les autres $t_j, j \in [0,n], j \neq k$, $\varphi_{\mauve{j}}$ vaudra $\mathbf{0}$

        \rouge{attention : il ne vaudra $\mathbf{0}$ \underline{QUE} pour les autres $t_{j}$ passés en paramètre !}  
           \demitab Les autres valeurs de $\mathbb{R}$ passées en paramètre, pourront donner n'importe quoi !  
           \demitab Ou même n'être pas définies !

\bigbreak

   2. La somme de ces polynômes $\varphi_{\mauve{k}}$ avec $\mauve{k} \in [0,n]$   
      fera le polynôme : 
      $$ \rouge{p(t)} = \sum\limits_{\mauve{k} = 0}^{\mauve{k} = n}\bleu{v_{\mauve{k}}}.\varphi_{\mauve{k}}(t) = v_{0}.\varphi_{0}(t) + v_{1}.\varphi_{1}(t) + ... + v_{n}.\varphi_{n}(t) $$

      où c'est la multiplication d'un polynôme $\varphi_{\mauve{k}}$ qui vaut $\mathbf{1}$  
      \demitab par la valeur $\bleu{v_{k}}$ que l'on désire avoir pour $\bleu{t_{k}}$   
      \demitab qui donnera à $\bleu{p(t_{k})}$ la valeur que l'on recherche, $\rouge{p(t_{k}) = v_{k}}$  

      alors que dans le même temps, pour ce $t_{k}$,  
      \demitab les autres bases de lagrange $\varphi({t})$ qui n'ont pas été prévues pour lui,  
      \demitab vaudront $\mathbf{0}$ et n'interfèreront pas.

\bigbreak

   3. Il y a autant de bases de Lagrange qu'il y a de relations $p(t_{k}) = v_{k}$ que l'on veut vérifier.

## 3) La Base de Lagrange

### a) Comment fonctionne une base de Lagrange (établie pour chaque relation) ?

On observe tour à tour chaque relation. Et pour chaque relation $\bleu{k}$ parmi les $n+1$ à déterminer

   - cette relation \bleu{$k$} crée un polynôme $\rouge{\varphi_{\bleu{k}}(t)}$ plus petit :
     $$
     \begin{aligned}[t]
     \rouge{\varphi_{k}} : \mathbb{R} & \longrightarrow \mathbb{R} \\
     \bleu{k} &\in [0, n] \\
     t  &\longmapsto \frac{(t - t_{0})(t - t_{1})...(t - t_{\bleu{k - 1}})(t - t_{\rouge{k + 1}})...(t - t_{\mauve{n}})}{(t_{k} - t_{0})(t_{k} - t_{1})...(t_{k} - t_{\bleu{k - 1}})(t_{k} - t_{\rouge{k + 1}})...(t_{k} - t_{\mauve{n}})} \\
     \end{aligned}
     $$

     Remarque: au numérateur comme au dénominateur, on esquive $k$ : 

        - au numérateur, il n'y a pas de $(t - t_{k})$ : pour $k = 0$, on n'y trouvera pas $(t - t_{0})$
        - au dénominateur, il n'y a pas de $(t_{k} - t_{k})$, qui provoquerait une division par zéro.

     \bigbreak

     ou (plus facile à comprendre ?) :
     $$
     \begin{aligned}[t]
     \rouge{\varphi_{k}} : \mathbb{R} & \longrightarrow \mathbb{R} \\
     \bleu{k} &\in [0, n] \\
     \mauve{j} &\in [0, n] \text{ et }\mauve{j} \ne \bleu{k} \\[3pt]
     t  &\longmapsto \frac{\prod(t - t_{\mauve{j}})}{\prod(t_{\bleu{k}} - t_{\mauve{j}})} \\
     \end{aligned}
     $$

### b) Pourquoi cela fonctionne t-il ?

\underline{Pourquoi $\varphi_{\bleu{k}}(t_{\bleu{k}})$ vaut-il 1} ?

Pour la valeur $\bleu{t_{k}}$, ce plus petit polynôme vérifie l'équation : $\bleu{\varphi_{k}(t_{k}) = 1}$

   - car le numérateur est sous la forme de produits : $(t - t_{0})(t - t_{1})...(t - t_{j})...(t - t_{n})$   
     et pour $\varphi_k(t_{k})$, il vaut :  
     $(t_{k} - t_{0})(t_{k} - t_{1})...(t_{k} - t_{j})...(t_{k} - t_{n})$
     
   - et le dénominateur est aussi sous forme de produits :  
     $(t_{k} - t_{0})(t_{k} - t_{1})...(t_{k} - t_{j})...(t_{k} - t_{n})$   
     \bleu{il vaut alors exactement la même chose !}, ce qui fait que $\varphi_k(t_{k}) = \frac{\text{numérateur}}{\text{dénominateur}} = 1$

\bigbreak

\underline{Pourquoi $\varphi_{\bleu{k}}(t_{\mauve{j}})$ vaut-il 0} ?

Pour toutes les autres valeurs $t_{\mauve{j}}$ avec $\mauve{j} \in [0,n] \text{ et } \mauve{j} \neq \bleu{k}$,  
le numérateur est sous forme de produits : $(t - t_{0})(t - t_{1})...(t - t_{\mauve{j}})...(t - t_{n})$

   \demitab ce qui fait que pour chaque \mauve{j} tour à tour, un de ces facteurs $(t - t_{\mauve{j}})$ va s'annuler.

   \demitab car quand on passera $t_{\mauve{j}}$ le moment venu dans $\varphi_{\bleu{k}}$ : $\varphi_{\bleu{k}}(t_{\mauve{j}})$  
   \demitab \bleu{un des facteurs vaudra $(t_{j} - t_{j})$, c'est à dire $\mathbf{0}$},  
      \tab \bleu{et comme notre numérateur est un produit de facteur, il annulera tout}. 

## 4) Implémentation

.   
.  page suivante   
.

\pagebreak

\code

   - Chaque instance de la classe `Relation` représente une relation $y = f(x)$ que le polynôme doit vérifier.
   - On construit l'interpolation en passant les valeurs $\{t_{0}, t_{1}...t_{n}\}$ d'un côté, les valeurs $\{v_{0}, v_{1}...v_{n}\}$ de l'autre.

> ```java
> public class InterpolationLagrange {
>    private final List<Relation> relations;
>    private final int n;
> 
>    public static class Relation {
>       private final double x;
>       private final double y;
> 
>       public Relation(double x, double y) {
>          this.x = x;
>          this.y = y;
>       }
>    }
> 
>    /**
>     * Construire une interpolation de Lagrange.
>     * @param vx Valeurs de x recherchées.
>     * @param vy Valeurs de y recherchées.
>     */
>    public InterpolationLagrange(List<Double> vx, List<Double> vy) {
>       this.relations = new ArrayList<>();
> 
>       for(int index = 0; index < vx.size(); index++) {
>          Relation r = new Relation(vx.get(index), vy.get(index));
>          this.relations.add(r);
>       }
> 
>       this.n = this.relations.size();
>    }
> 
>    /**
>     * Interpolation
>     * @param x La valeur x qu'on recherche par p(x)
>     * @return La valeur y = p(x)
>     */
>    public double interpolation(int x) {
>       double result = 0;
> 
>       for (int k = 0; k < this.n; k++) {
>          result += terme(k, x);
>       }
> 
>       return result;
>    }
> ```

(suite page suivante)

\endgroup

\code

> ```java
>    private double terme(int k, double x) {
>       double term = this.relations.get(k).y;
> 
>       for (int j = 0; j < this.n; j++) {
>          if (j != k) {
>             term = term * baseDeLagrange(j, k, x);
>          }
>       }
> 
>       return term;
>    }
>
>    private double baseDeLagrange(int j, int k, double x) {
>       if (j != k) {
>         return (x - this.relations.get(j).x) / (this.relations.get(k).x - this.relations.get(j).x);
>       }
> 
>       return 1.0;
>    }
> }
```

\endgroup

\bigbreak

\code

\underline{Recherche d'un polynôme tel que :}

   \demitab $p(-1) = 8$, $p(0) = 3$, $p(1) = 6$


> ```java
> @Test
> void test() {
>    InterpolationLagrange p = 
>       new InterpolationLagrange(List.of(-1.0, 0.0, 1.0), List.of(8.0, 3.0, 6.0));
> 
>    assertEquals(17.0, p.interpolation(2), 0.001, "mauvaise interpolation pour y = 2");
>    assertEquals(36.0, p.interpolation(3), 0.001, "mauvaise interpolation pour x = 3");
> }
> ```

\endgroup

