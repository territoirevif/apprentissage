---
title: "m430. Les relations d'équivalence"
subtitle: ""
author: Marc Le Bihan
---

Une __relation d'ordre__ permet de structurer un ensemble  
   \demitab en définissant des liens (d'ordre) entre ses éléments.

   \demitab $\rightarrow$ une \definition{relation d'équivalence} va nous permettre de le structurer  
   \tab en __sous-ensembles__ appelées \definition{classes d'équivalence}.  

## 1) La classe d'équivalence

   > \definition{\underline{Définition :}}
   
   >   > Soit $\mathcal{R}$ une relation binaire sur $E$
   >   > On dit que $\mathcal{R}$ est une relation d'équivalence, si elle est :
   >
   >   > - réflexive
   >   > - symétrique ($x \mathcal{R} y \implies y \mathcal{R} x$)
   >   > - transitive

\bigbreak

Exemple : dans l'ensemble des droites du plan  
   \demitab la relation de __parallélisme__ est une relation d'équivalence.

La congruence $\mauve{a \equiv b(n)}$ est une relation d'équivalence dans $\mauve{\mathbb{Z}}$  
\rouge{\emph{ne pas oublier de mettre l'ensemble cible sur lequel porte une relation !}}

\bigbreak

   > \definition{\underline{Définition :}}

   >   > Soit $\mathcal{R}$ une relation d'équivalence sur un ensemble $E$.  
   >   > On appelle \definition{classe d'équivalence} d'un élément $x$ de $E$, l'ensemble :
   >   > $$ \dot{x} = \{y \in E, x \mathcal{R} y\} $$
   >   >
   >   > Remarque : $x \in \dot{x}$

\bigbreak

$\dot{v}$ une classe qui contiendra toutes les viandes de mon ensemble $F$ (frigo).  
$\dot{v} = \{"steak", "lard", "poulet"\}$

## 2) L'ensemble des classes d'équivalence : l'ensemble quotient

   > \definition{\underline{Définition :}}
   
   >   > Soit $\dot{x}$ une classe d'équivalence  
   >   > On appelle \definition{représentant} de $\dot{x}$ tout élément de $\dot{x}$.
   
   > \definition{\underline{Définition :}}
   
   >   > Soit $E$ un ensemble et $\mathcal{R}$ une relation d'équivalence sur $E$.  
   >   > \textbf{L'ensemble} des classes d'équivalence est appelé \definition{ensemble quotient}, il est noté \definition{$\mathbf{E/R}$}.


Il va être l'ensemble de toutes les classes d'équivalences que j'ai dans mon frigo.  
Par exemple, les quatre classes d'équivalence : viandes, fruits, légumes, desserts.

### a) Chaque élément de l'ensemble quotient est une partition

   > \textbf{Proposition :}
   >
   > Les éléments $C_i$ de l'ensemble quotient $E / \mathcal{R}$ forment une partition de $E$.  
   > (_partition : chaque partie est distincte des autres,  
   > et la réunion de toutes les parties forme l'ensemble lui-même_).
  
\bigbreak

   > \definition{\underline{Définition :}}
   >  > 
   >  > L'application
   >  > $$ 
   >  > \begin{aligned}[t]
   >  > \phi : E &\to E / \mathcal{R} \\
   >  > x &\mapsto \phi(x) = \dot{x}
   >  > \end{aligned} 
   >  > $$  
   >  > est appelée \definition{surjection cannonique}.

\bigbreak

Elle ne fait qu'attribuer à chaque élément $x$ sa classe $\dot{x}$.  
(exemple : les éléments "poulet", "oeuf" vont être associés à la classe d'élement "viande".

### b) Deux éléments sont équivalents, s'ils ont la même classe d'équivalence

   > __Proposition :__
   >
   > Deux éléments sont équivalents, s'ils ont la même classe d'équivalence.
   > $$ \forall x,y \in E, x \mathcal{R} y \implies \dot{x} = \dot{y} $$

   > \definition{\underline{Définition :}}
   >  > 
   >  > Soit $\dot{x}$ une classe d'équivalence.  
   >  > On appelle \definition{ensemble de représentants} de $\dot{x}$  
   >  > toute partie $A$ de $E$  
   >  > qui contient exactement un représentant par classe.

\bigbreak
 
C'est à dire qu'il existe un ensemble de représentants $\mauve{\mathcal{T}}$  
   \demitab qui est une partie de $\bleu{E}$, telle que :
   $$ \text{1) } \forall x \in E, \exists y \in \mathcal{T}, \dot{x} = \dot{y} $$
   $$ \text{2) } \forall x, y \in \mathcal{T}, x \neq y \implies \dot{x} \neq \dot{y} $$

\bigbreak

Par exemple, si l'on classe dans un ensemble $\mathbb{Z}$ les nombres en nombres impairs et pairs,  
   \demitab on aura deux classes :

   - $\dot{0}$ : la classe des nombres pairs : $\{0, 2, 4, 6, 8...\}$
   - $\dot{1}$ : la classe des nombres impairs : $\{1, 3, 5, 7...\}$

On pourra dire que $\mathcal{T} = \{0,1\}$ est l'ensemble des représentants. Il a un représentant des nombres pairs : 0, et un représentant des nombres impairs : 1.

### c) Si l'on a une partition de E, alors une unique relation d'équivalence la donne

   > \textbf{\underline{Proposition :}}

   > Soit $E$ un ensemble et $(A_i)_{i \in I}$ une partition de $E$.
   > 
   > Alors, il existe une unique relation d'équivalence $\mathcal{R}$ sur $E$  
   > \ \ \ \ dont toutes les classes d'équivalence sont les $A_i$ :
   > $$ x \mathcal{R} y \iff \exists i \in I, (x,y) \in A_i \times A_i $$

### d) Nombre de relations d'équivalence dans un ensemble : le nombre de Bell 

   > \definition{\underline{Définition :}}
   >  >
   >  > Soit $E$ un ensemble fini de cardinal $n$.
   >  > 
   >  > Le nombre de relations d'équivalence sur $E$ est égal au nombre de partitions de $E$.
   >  > Ce nombre est appelé \definition{nombre de Bell} et est noté \definition{$B_n$}.

