---
title: "m420. Le trellis"
subtitle: "un ensemble partiellement ordonné, où toute paire d'éléments a une borne inférieure et une borne supérieure"
category: mathematiques
author: Marc Le Bihan
keywords:
- treillis
- ensemble réticulé
- lattis
- ensemble
- ensemble partiellement ordonné
- borne inférieure
- borne supérieure
- PPCM
- PGCD
- commutatif
- associatif
- distributif
- relation
- relation d'ordre
- complémenté
---

   \begingroup \cadreAvantage

   > \underline{Définition :} \newline
   >   
   > On appelle \definition{trellis} ou _lattis_ ou _ensemble réticulé_,  
   > \tab un ensemble __partiellement ordonné__  
   > \tab dans lequel, pour toute paire d'éléments,  
   > \tab existent une __borne inférieure__ et __borne supérieure__

   \endgroup

## 1) Exemple de treillis

Par exemple, si un ensemble est fait de nombres entiers  
   \demitab et qu'ils sont ordonnés par la relation $\mauve{x | y}$

   > si l'on considère $30 = 2^1 \times 3^1 \times 5^1$  
   > \tab qui montre dans son treillis les nombres $\{1,2,3,5,6,10,15,30\}$

   > ![le treillis des diviseurs de 30](images/treillis_30.jpg)

### a) sa borne inférieure, sur l'exemple de la relation x | y

Sa borne inférieure (b.i.) __est son Plus Grand Commun Diviseur (PGCD)__ :  
\tab $5 \land 6$ \gris{(deux valeurs d'exemple prises dans cet ensemble)} $= 1$

### b) sa borne supérieure

Sa borne supérieure (b.s.) sont __Plus Petit Commun Multiple (PPCM)__ :  
\tab $5 \lor 6 = 5 \times 2 \times 3 = 30$

### c) son élément universel et son élément nul

Son élément universel est $30$ (on l'appelle souvent \bleu{$U$})  
son élément null est $1$ (on l'appelle $\bleu{n}$).

## 2) Définition d'un trellis à partir des propriétés de commutativité, associativité, indempotence, absorbtion

Si on a $T$ un ensemble aux propriétés de, 
   \demitab pour n'importe quels $\mauve{x,y,z \in T}$ :

   1. commutativité : $\mauve{x \lor y = y \lor x}$ et également : $\mauve{x \land y = y \land x}$
   2. associativité : $\mauve{x \lor (y \lor z) = (x \lor y) \lor z}$ et $\mauve{x \land (y \land z) = (x \land y) \land z}$
   3. idempotence : $\mauve{x \lor x = x}$ et $\mauve{x \land x = x}$
   4. d'absorbtion : $\mauve{x \lor (x \and y) = x}$ et $\mauve{x \land (x \lor y)}$

\bigbreak

alors $T$ est un ensemble ordonné par une relation $\mauve{\leq}$ où

   > $[x \leq y] \iff [x \land y] = x \iff [x \lor y] = y$  
   > ($x \leq y$ a pour borne inférieure $x$ et pour borne supérieure $y$)

\bigbreak

et c'est un treillis.

## 3) Un treillis complémenté

Un trellis $T$ est \definition{complémenté} s'il :

   - a un elément universel $\bleu{U}$ et un élément nul $\bleu{n}$
   - si à tout élément $\bleu{x}$ de $T$, on peut associer __au moins__ un élément $\mauve{\overline{x}}$ de $T$, tel que :
   $$ \left\{
   \begin{array}{ll}
       x \lor \overline{x} = U \\
       x \land \overline{x} = n \\
   \end{array}
   \right. $$

\bigbreak

Le treillis de notre figure des diviseurs est complémenté, car :

   - $1 \lor 30 = 30$, $1 \land 30 = 1$
   - $2 \lor 15 = 30$, $2 \land 15 = 1$
   - $3 \lor 10 = 30$, $3 \land 10 = 1$
   - $5 \lor 6 = 30$, $5 \land 6 = 1$

\bigbreak

\underline{Propriétés :}

   > $x \land U = x$  
   > $x \lor n = x$

## 4) Un treillis peut être distributif

Si aux axiomes 1 à 4 (commutativité, associativité, idempotence, absorbtion) s'ajoute que si,  
   \demitab quels que soient $x$, $y$ et $z \in T$ :

   > $x \land (y \lor z) = (x \land y) \lor (x \land z)$   
   > $x \lor (y \land z) = (x \lor y) \land (x \lor z)$  

alors ce treillis est distributif.

\bigbreak

Et dans un treillis distributif, la complémentation est unique.

