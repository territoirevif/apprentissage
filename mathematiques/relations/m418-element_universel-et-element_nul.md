---
title: "m418. L'élément universel et l'élément nul"
subtitle: "d'un ensemble ordonné"
author: Marc Le Bihan
---

Quand on a une \donnee{relation d'ordre} dans un ensemble, qu'elle soit \donnee{totale} ou \donnee{partielle}, on peut comparer ses éléments entre-eux.

Et par là, définir ceux qui sont particuliers.

\bigbreak

L'élément universel de $X$ est le plus grand élément de $X$.

Respectivement, l'élément null de $X$ est le plus petit élément de $X$.

