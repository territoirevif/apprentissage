---
title: "m404. Le diagramme de Hasse"
subtitle: ""
author: Marc Le Bihan
---

Le \definition{digramme de Hasse} permet de représenter une relation binaire de manière simplifiée.
 
   - les éléments reliés à eux-mêmes ne sont pas représentés.
   - on part du bas et l'on progresse vers le haut

\bigbreak

   le diagramme de Hasse de ($P(E), \subset$), lorsque $E = \{a, b\}$  
   \demitab et donc $P(E)= \{\varnothing, \{a\}, \{b\}, \{a, b\}\}$

   ![Diagramme de Hasse](images/diagramme-hasse.png){width=120px}  

\bigbreak

   Le diagramme de Hasse de (E, |)   
   avec $E = {1,2,3,4,5,6}$
   
   ![Diagramme de Hasse](images/diagramme-hasse_1-2-3-4-5-6_divise-par.png){width=120px}  
   On n'a pas besoin ici, de marquer le trait "_1 divise 4_", parce que 1 est indirectement relié à 4.

