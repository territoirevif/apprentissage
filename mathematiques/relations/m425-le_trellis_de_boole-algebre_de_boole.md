---
title: "m425. Le trellis de Boole et l'algèbre de Boole"
subtitle: "générateur, hypercube"
category: mathematiques
author: Marc Le Bihan
keywords:
- treillis
- ensemble partiellement ordonné
- commutatif
- associatif
- distributif
- relation d'ordre
- complémenté
- treillis de Boole
- Boole
- algèbre de Boole
---

Quand un treills est distributif \underline{et complémenté}, il s'appelle \definition{Trellis de Boole}.

## 1) Algèbre de boole

Une \definition{algèbre de Boole $B$} est

   > un ensemble __partiellement ordonné__  
   > doté d'un élément nul, $n$  
   > d'un élement universel, $U$  
   > qui vérifie les aximomes de : 
   >    - commutativité
   >    - associativité
   >    - idempotence
   >    - absorbtion
   >    - complémenté
   >    - distributivité  
 
\bigbreak

et sa relation d'ordre $\bleu{x \leq y}$ est définie par l'une des quatre relations suivantes :
$$ x \leq y \iff (x \land y = x) \ou (x \lor y = y) \ou (x \land \overline{y} = 0) \ou (\overline{x} \land y = 1) $$

![Algèbre de Boole, avec un générateur a](images/algebre_boole_treillis_generateur.jpg)  
\legende{une algèbre de boole, avec un générateur \bleu{a} (différent de 0 et de 1) a quatre éléments}

![](images/algebre_boole_treillis_hypercube.jpg)
![](images/algebre_boole_treillis_régions.jpg)

