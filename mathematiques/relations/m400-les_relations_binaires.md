---
title: "m400. Les relations binaires"
subtitle: "(celles d'un ensemble vers une sous-partie de son produit cartésien)"
author: Marc Le Bihan
---

Une \definition{relation} établit une vérité.  
Et à partir des relations, on peut établir des \definition{opérateurs fondamentaux}.

## 1) Une relation binaire

Considérons qu'au lieu d'observer une application de $E$ dans $F$,  
   \tab nous en considérons une de $E$ dans $E$ seulement.  
   \tab c.à.d. que la mise en correspondance des éléments conduit à une sous-partie de $E \times E$.

\bigbreak

S'il y a relation entre deux éléments $\mauve{x}$ et $\mauve{y}$ de $\mauve{E}$, on le note $\mauve{x\ \mathcal{R}\ y}$.  
   \demitab $\rightarrow$ l'ordre de la relation est important, et une \definition{relation binaire} opère sur un même ensemble.

   \begingroup \cadreAvantage

   > - $x = y$
   > - $x \leq y$
   > - $A \subset B$
   > - $n | p$\ \demitab \ \ ($n$ divisible par $p$)
   > - $d\ //\ d'$\ \ \ \ (droites parallèles)
   > - $d \perp d'$\ \ \ \ (droites perpendiculaires)

   \endgroup

\bigbreak

   \begingroup \cadreGris

   > \definition{\underline{Definition :}}
   > Soit $E$ un ensemble,  
   > \  
   >
   > \demitab on appelle \definition{relation binaire} sur $E$, toute partie de $E \times E$  
   > \demitab (_du produit cartésien, donc_)

   \endgroup

  
   \begingroup \cadreAvantage

   > avec $E = \{a, b\}$,   
   > \demitab si sur le graphe on a : $a$ relié à lui même et $a$ relié à $b$,  
   > \  
   >
   > \demitab alors on aura :  
   > \tab $\mathcal{R} = \{(a,a), (a,b)\}$

   \endgroup

## 2) La relation diagonale

Dans un produit cartésien $E \times E$, où l'on a :

|   | x     | y     | z     |
| - | :---: | :---: | :---: | 
| x | __(x,x)__ | (x,y) | (x,z) |
| y | (y,x) | __(y,y)__ | (y,z) |
| z | (z,x) | (z,y) | __(z,z)__ |

$\Delta = \{(x,x), (y,y), (z,z)\}$ est la relation \definition{diagonale}.

