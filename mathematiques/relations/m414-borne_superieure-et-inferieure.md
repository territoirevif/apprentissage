---
title: "m414. La borne supérieure et la borne inférieure"
subtitle: "d'un ensemble ordonné"
author: Marc Le Bihan
---

Quand on a une \donnee{relation d'ordre} dans un ensemble, qu'elle soit \donnee{totale} ou \donnee{partielle}, on peut comparer ses éléments entre-eux.

Et par là, définir ceux qui sont particuliers.

\bigbreak

La notion de borne supérieure ou inférieure palie le fait qu'il n'y a pas toujours de plus grand ou plus petit élément dans un ensemble.

\bigbreak

\definition{\underline{Définition :}}

   > Soit $P$ un ensemble ordonné par une relation $\leq$  
   > et soit $A$ une partie de $P$  
   >
   > \demitab Si l'ensemble des majorants de $A$ admet un plus petit élément  
   > \demitab on l'appelle la \definition{borne supérieure (b.s)}  
   > \demitab et on la note \definition{sup(A)} "_suppremum_".

\rouge{$sup(A)$ n'appartient pas forcément à $A$}

\bigbreak

Si sup(A) existe et appartient à $A$, alors c'est le plus grand élément.

==> La borne supérieure est d'un emploi plus souple que le plus grand élément

## a) La borne supérieure n'existe pas toujours

La borne supérieure n'existe pas toujours. Mais dans $\mathbb{R}$, oui : sous certaines conditions.  
C'est la \definition{propriété de la borne supérieure}

   > Toute partie non vide et majorée de $\mathbb{R}$ possède une borne supérieure.

## b) Si elle existe pour une paire (x, y) d'un ensemble ordonné, c'est $x \lor y$ [$x \land y$]

Pour une partie réduite à deux éléments (paire) ${x, y}$ d'un ensemble ordonné,  
il _peut_ exister une borne inférieure ou une borne supérieure.

  > dans ce cas, la borne supérieure est notée \mauve{$x \lor y$},  
  > la borne inférieure, \mauve{$x \land y$}

