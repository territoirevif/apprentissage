---
title: "m405. Les relations d'ordre"
subtitle: "Les relations de pré-ordre, ordre, d'équivalence, d'égalité"
author: Marc Le Bihan
---

## 1) Relation de pré-ordre

Une \definition{relation de pré-ordre} est \mauve{réflexive} et \mauve{transitive}

   > Exemple : $\mathcal{R}$ "_Avoir un rang aussi bon ou meilleur que_" où \mauve{(A,B)} signifie \mauve{$A \geq B$}  
   > n'est pas symétrique : $A \geq E$ mais $E \ngeq A$  
   > n'est pas assymétrique : $A \geq D$ et $D \geq A$ n'entraînent pas $A \equiv D$ : qu'on puisse confondre $A$ et $D$

## 2) Relation d'ordre

Une \definition{relation d'ordre} est \mauve{réfexive}, \mauve{antisymétrique} et \mauve{transitive}.  
On dit alors que l'ensemble $E$ est ordonné par $\mathcal{R}$.  

Une relation d'ordre \definition{strict} (\mauve{$<$}) est un ordre partiel.  
Remarque : une relation d'ordre $\leq$ peut aussi être partielle !  

### a) Ordre total

Dans $\mathbb{R}$ ($\mathcal{N}$, $\mathcal{Z}$, $\mathcal{Q}$) la relation $\leq$ est une relation d'ordre total,  

### b) Ordre partiel

Exemple : l'__inclusion__ dans $P(E)$ est bien une relation d'ordre, mais d'\definition{ordre partiel}. Car pour deux parties $A$ et $B$ de $E$, on a pas forcément $A \subset B \land B \subset A$

la relation de divisibilité dans $\mathbb{N}$ est une relation d'ordre partiel,  

mais attention ! Cette relation de divisibilité dans $\mathbf{\mathbb{Z}}$, elle,   
\textbf{\underline{n'est pas}} une relation d'ordre !

en effet, elle n'est pas antisymétrique ! 2|-2 et -2|2, et cependant: $-2 \neq 2$

## 3) Relation d'équivalence

Une \definition{relation d'équivalence} est \mauve{réflexive}, \mauve{symétrique} et \mauve{transitive}.

## 4) Relation d'égalité

La \definition{relation d'égalité} est la seule relation qui est à la fois d'__équivalence__ et d'__ordre__, car :

   - $\forall x, y \in E, x = y \implies y = x$
   - $\forall x, y \in E, x = y \text{\ et \ } y = x \implies x = y$

## 5) Application croissante (isotone)

\definition{\underline{Définition :}}

   > Soient \definition{$(E, \leq)$} et \danger{$(F, \leq)$} deux ensembles munis d'un ordre.  
   > Une application $f$ est dite \definition{croissante (isontone)} si elle conserve l'ordre.  
   > C'est à dire : 
   > $\forall x, \exists y \in E, x$ \definition{$\leq$} $y \implies f(x)$ \danger{$\leq$} $f(y)$

## 6) Relation forte, relation faible

On dit que \textbf{$\mathcal{R'}$ est plus faible que $\mathcal{R}$} si $\forall x,y \in E, x \mathcal{R} y \implies x \mathcal{R'} y$.  
 (réciproquement, on dit que on dit que $\mathcal{R}$ est plus forte que $\mathcal{R'}$ si $\forall x,y \in E, x \mathcal{R} y \implies x \mathcal{R'} y$).

   - La relation la plus faible est celle dont le Graphe est $E \times E$ : tous les éléments sont reliés entre-eux, et il n'apporte aucune information.

   - à l'inverse, la relation la plus forte est celle dont le Graphe est vide. Aucun élément n'est relié à lui-même ni à un autre élément.

\bigbreak

Qu'une relation soit $\mathcal{R'}$ soit plus faible que $\mathcal{R}$ implique que le graphe $\Gamma_\mathcal{R} \subset \Gamma_\mathcal{R'}$
   (_le graphe de la relation forte est inclu dans celui de la relation plus faible_).

### Intersection de relations

Soit $(R_i)_{i \in I}$ une famille indexée de relations binaires.  
On peut définir l'intersection de ces relations : $\bigcap_{i \in I} R_i$  
  
  Dont le graphe est donné par :
  $$ \Gamma_{\cap_{i \in I} R_i} = \bigcap_{i \in I} \Gamma_{\mathcal{R}_{i}} $$
  (_c'est à dire : l'intersection de tous les graphes_).

\bigbreak
 
Remarque : Parmi toutes les relations qui sont plus fortes que $\mathcal{R}_{i}$,  
  la plus faible d'entre-elles est celle des intersections des $\mathcal{R}_{i}$.  
   
  On écrit cette relation alors :
  $$ x \mathcal{R} y \iff \forall i \in I, x\ \mathcal{R}_{i}\ y $$

__Proposition :__
 
   \demitab L'intersection des relations binaires  
   \demitab conserve leurs propriétés de réflexivité, symétrie, assymétrie, transitivité

