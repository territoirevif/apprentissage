---
title: "m410. Les majorants et minorants"
subtitle: "d'un ensemble ordonné"
author: Marc Le Bihan
---

Quand on a une \donnee{relation d'ordre} dans un ensemble, qu'elle soit \donnee{totale} ou \donnee{partielle}, on peut comparer ses éléments entre-eux.

Et par là, définir ceux qui sont particuliers.

\bigbreak

\definition{\underline{Définition :}}

   > Soit $P$ un ensemble ordonné par une relation \mauve{$\leq$}  
   > 
   > \demitab On dit que \definition{M} $\in P$ est un \definition{majorant} de $A \subset P$, si
   > $$ \forall x \in A, \mauve{x \leq \donnee{M}} $$

$\rouge{\mathbf{M}}$ \rouge{n'appartient pas forcément à $A$} : il majore $A$, mais ne lui appartient pas toujours,  
et il n'est pas nécessairement unique.

\bigbreak

   \begingroup \cadreAvantage

   > l'intervalle $[2,8[$ est majoré par 8, 12, 20...  
   > en fait, par $[8, +\infty[$.

   \endgroup

