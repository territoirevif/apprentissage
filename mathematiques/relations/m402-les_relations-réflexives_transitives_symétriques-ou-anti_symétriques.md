---
title: "m402. Les relations réflexives, transitives, symétriques ou anti-symétriques"
subtitle: ""
author: Marc Le Bihan
---

Une relation binaire est :

\definition{réflexive} si pour tout élément $x$ de $E$
$$ x \mathcal{R} x $$

  - L'égalité dans $\mathbb{R}$
  - ou l'inclusion ($\subset$} dans $P(E)$, le sont.

\bigbreak

\definition{transitive} si pour tout élément $x, y, z$ de $E$ :
$$ x \mathcal{R} y \land y \mathcal{R} z \implies x \mathcal{R} z $$
$$ \text{exemple : l'inclusion dans\ }P(E) : (A \subset B) \land (B \subset C) \implies (A \subset C) $$

\bigbreak

\definition{symétrique} si pour tout élément $x, y$ de $E$ :
$$ x \mathcal{R} y \implies y \mathcal{R} x $$
$$ \text{avec pour exemples l'égalité ou l'orthogonalité de droites} $$

\bigbreak

\definition{antisymétrique} si pour tout élément $x, y$ de $E$ :
$$ x \mathcal{R} y \land y \mathcal{R} x \implies x = y $$
$$ \text{exemples : l'inclusion dans \ }P(E) : (A \subset B) \land (B \subset A) \implies A = B $$
$$ \text{ou la divisibilité dans }\mathbb{N} : (n | p) \text{ et } (p | n) \implies n = p $$

