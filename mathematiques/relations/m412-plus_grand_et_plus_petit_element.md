---
title: "m412. Plus grand et plus petit élément"
subtitle: "d'un ensemble ordonné"
author: Marc Le Bihan
---

Quand on a une \donnee{relation d'ordre} dans un ensemble, qu'elle soit \donnee{totale} ou \donnee{partielle}, on peut comparer ses éléments entre-eux.

Et par là, définir ceux qui sont particuliers.

## 1) Définition

   > Soit $P$ un ensemble ordonné par une relation $\leq$  
   >
   > On appelle \definition{plus grand élément} de $A \subset P$,  
   > un élément \definition{E} \textbf{\underline{de $A$}}  
   > plus grand que tous les autres :
   > $$ \forall x \in A, x \leq E $$

$E$ appartient à $A$, et il est unique.
 
\bigbreak

La différence ici, par rapport au majorant, c'est que :

   - $M$ est défini dans $P$
   - alors que $E$ l'est dans $A$.

__Un intervalle à bornes ouvertes, ne possède pas :__

  - de plus grand élément, s'il est ouvert à droite,
  - de plus petit élément, s'il est ouvert à gauche.

\pagebreak

## 2) Exemples

Exemple, sur $\mathbb{N}$ : 

   > ![Diagramme de Hasse](images/diagramme-hasse-N-divise.png){width=150px}

   - 0 est le plus grand élément (il est __divisible__ par tout le monde)
   - 1 est le plus petit élément (il __divise__ tout le monde)

\bigbreak

Ici il n'y a pas de plus grands éléments :

   > ![Diagramme de Hasse sans plus grand élément](images/diagramme-hasse-sans-plus-grand-élément.png){width=120px}  
   > $A = \{3,5\}$ est majoré par tous les multiples de 15.  
   > $sup(\{3,5\}) = 15$

