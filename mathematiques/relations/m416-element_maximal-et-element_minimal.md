---
title: "m416. L'élément maximal et l'élément minimal"
subtitle: "d'un ensemble ordonné"
author: Marc Le Bihan
---

Quand on a une \donnee{relation d'ordre} dans un ensemble, qu'elle soit \donnee{totale} ou \donnee{partielle}, on peut comparer ses éléments entre-eux.

Et par là, définir ceux qui sont particuliers.

\bigbreak

L'élément maximal prend tout son sens dans un __ensemble partiellement ordonné__.

\bigbreak

\definition{\underline{Définition :}}

   > Soit $E$ un ensemble ordonné par une relation $\leq$.  
   > On appelle \definition{élément maximal} un élément \definition{$a$} tel  
   > \ \ \ qu'il n'existe aucun autre élément de $E$ qui lui soit supérieur.
   > $$ \forall x \in E, a \leq x \implies x = a $$

\bigbreak

\underline{Exemple :} considérons l'ensemble $E = \mathbb{N}$ et la relation __|__ de divisibilité  
   \demitab $\mauve{(\mathbb{N}, |)}$ n'est pas totalement ordonné, car 3 ne divise pas 5, et 5 ne divise pas 3  
   \demitab à la différence de $\mauve{({\mathbb{N}, \bleu{\leq}})}$, qui lui est totalement ordonné

\bigbreak

Si l'on prend l'ensemble $\mauve{A = \{2, 4, 7, 8\}}$ (une partie de $\mathbb{N}$),  
   \demitab la relation $\mauve{\leq}$ va donner un \definition{diagramme de Hasse} droit  
   \demitab avec 2 en bas, puis 4, 7 et 8.

\bigbreak

Mais si l'on prend la relation de divisibilité, c'est différent  
   \demitab car dans le diagramme de Hasse, il se forme deux groupes dans notre cas :

![Diagramme de Hasse](images/diagramme-hasse-éléménts-maximaux.png){width=250px}  

\avantage{Un plus grand élément est un élément maximal} \attention{mais la réciproque est fausse}.

Dans un ensemble $E$ ordonné, il n'y a pas forcément de plus grand élément.  
Mais, si $E$ est fini, il existe au moins un élément maximal.

