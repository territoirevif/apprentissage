---
title: "m700. Algèbre Linéaire - Le langage matriciel"
subtitle: ""
author: Marc Le Bihan
---

## 1) Le format des matrices

Les matrices sont au format :

   > $\bleu{m}$ lignes  
   > $\bleu{n}$ colonnes
\begingroup \large $$ [A_{ij}]_{\bleu{m} \times \bleu{n}} $$ \endgroup 

Une matrice \begingroup \large $[A_{ij}]_{\donnee{1} \times n}$ \endgroup est une \definition{matrice ligne}  
une matrice \begingroup \large $[A_{ij}]_{m \times \donnee{1}}$ \endgroup est une \definition{matrice colonne}  

## 2) Le terme général d'une matrice : la formule qui la définit

Le \definition{terme général} est la __formule__ qui régit la matrice, s'il est connu. 

   \begingroup \cadreAvantage

   > $a_{ij} = 3i - 5j$

   \endgroup  

\bigbreak

Une matrice \definition{nulle} a tous ses éléments qui valent $\mathbf{0}$.

## 3) Les matrices carrées et leur diagonale

\rouge{Ce qui suit ne s'applique qu'aux matrices carrées}  
Une \definition{matrice carrée} d'\definition{ordre} \mauve{$n$}

   > a $n$ lignes, $n$ colonnes.

### a) La diagonale principale
   
   La \definition{diagonale principale} va  
   \demitab du premier nombre en haut à gauche \mauve{$a_{1,1}$}  
   \demitab au dernier en bas à droite \mauve{$(a_{n,n})$}.

   c'est à dire : tous ses éléments \mauve{$a_{i,i}$}.  

### b) Matrice diagonale, scalaire, identité

   Si ses élements au dessus et en dessous de sa diagnonale principale valent $\mathbf{0}$,  
   on l'appelle \definition{matrice diagonale}.
   
   - si toute cette diagonale a la même valeur, on l'appelle \definition{matrice scalaire}
          $$ A = \begin{bmatrix}
   	       \donnee{8} & 0 \\
   	       0 & \donnee{8}
             \end{bmatrix} $$
   - et s'ils valent tous __1__, \definition{matrice identité}
          $$ A = \begin{bmatrix}
   	       \donnee{1} & 0 \\
   	       0 & \donnee{1}
             \end{bmatrix} $$
   \ 
   
   Attention, donc :  
   \demitab la diagonale principale d'une matrice  
   \demitab et une matrice diagonale

   ce ne sont pas les mêmes choses.

### c) Matrice triangulaire supérieure [inférieure]

   Une matrice \definition{triangulaire supérieure} a ses éléments  
      \demitab __en dessous__ de sa diagonale principale  
      \demitab qui valent $\mathbf{0}$
 
   \tab $\rightarrow$ c'est à dire que l'on peut trouver de l'information "au dessus" de sa diagonale.  
   \  
   
   une matrice \definition{triangulaire inférieure}  
      \demitab ce sont ceux __au dessus__  
      \demitab qui valent $\mathbf{0}$

   \tab $\rightarrow$ c'est à dire que l'on peut trouver de l'information "au dessous" de sa diagonale.

### d) Trace d'une matrice carrée

On peut calculer la \definition{trace} d'une matrice carrée :

   \begingroup \cadreGris

   > La \definition{trace} d'une matrice carrée est la somme des éléments de sa \definition{diagonale principale}

   \endgroup  

   \begingroup \cadreAvantage

   > Pour :
   > $$ A = \begin{bmatrix}
   > 	\donnee{2} & 3 \\
   > 	0 & \donnee{5}
   > \end{bmatrix} $$
   > 
   > $$ \rouge{Tr(A)} = \sum_{i=1}^{n}a_{i,i} = 7 $$

   \endgroup  

### e) Matrice symétrique et anti-symétrique

   \begingroup \cadreGris

   > Une matrice est \definition{symétrique} si $\mauve{a_{i,j} = a_{j,i}}$,  
   > et \definition{antisymétrique} si $\mauve{a_{i,j} = - a_{j,i}}$
   > 
   >  $$ A = \begin{bmatrix}
   >     0 & -4 \\
   >     4 & \ \ 0
   >  \end{bmatrix} \text{ est anti-symétrique} $$

   \endgroup

## 4) Pivot et matrice échelonnée [réduite]
 
Le __pivot d'une ligne__ est le premier élément d'une ligne non nul.  

Une __matrice échelonnée__ est une matrice dont :

   - Les lignes nulles sont toutes situées en dessous des lignes ayant des éléments non nuls.
   - Le pivot de chaque ligne vaut 1.  
   - Le pivot de chaque ligne est toujours situé à droite  
     (mais avec autant d'indices d'écarts en _j_ que l'on veut) 
     par rapport à la ligne précédente.

\bigbreak

Une __matrice échelonnée réduite__ assure aussi  
   \demitab que dans toute __colonne__ qui contient un pivot  
   \demitab les autres valeurs de la colonne sont nulles.

## 5) Egalité de matrices

   \begingroup \cadreGris

Deux matrices sont égales si :
    \   

   - elles ont le même format :   
        \demitab c'est à dire que si  
           \tab $\mathbf{A = [a_{ij}]_{\bleu{m} \times \mauve{n}}}$ et $\mathbf{B = [b_{ij}]_{\bleu{p} \times \mauve{q}}}$  
        \   
        \demitab il faut que  
           \tab $\mathbf{\bleu{m} = \mauve{p}}$ et $\mathbf{\bleu{n} = \mauve{q}}$  

\bigbreak
  
   - tous les éléments correspondants soient égaux :  
        \demitab \mauve{$a_{i,j} = b_{i,j}$}  
        \demitab pour tout $i$ et tout $j$.

   \endgroup

## 6) Mises en application concrètes

### Transformation d'un graphe (à sommets, arcs...) en matrice

![graphe migration interprovinciales](images/algebre-lineaire_manitoba_1.jpg)

![graphe migration interprovinciales](images/algebre-lineaire_manitoba_2.jpg) 
