---
title: "m705. Algèbre linéaire - Le produit matriciel"
subtitle: ""
author: Marc Le Bihan
---

## 1) Somme de matrices

\rouge{Une somme de matrices réclame qu'elles aient le même format.}

Si $\bleu{A = [a_{i,j}]_{m \times n}}$ et $\bleu{B = [b_{i,j}]_{m \times n}}$ alors la somme est :
\begingroup \large $$ \rouge{A + B} = [a_{i,j} + b_{i,j}]_{m \times n} $$ \endgroup

## 2) Produit matriciel

  $$ C_{n \times p} = A_{n \times m}\ \bleu{\times}\ B_{m \times p} $$
  $$ c_{ij} = \sum\limits_{k=1}^{m}(a_{ik} . b_{kj})$$

### a) Produit de Hadamard ou de Shur

C'est la multiplication  
   \demitab terme à terme   
de deux matrices __de même dimension__

  $$ C_{n \times m} = A_{n \times m}\ \bleu{\land}\ B_{n \times m} \text{\demitab les deux matrices doivent avoir les mêmes dimensions} $$
  $$ c_{ij} = \sum\limits_{k=1}^{m}(a_{ij} . b_{ij})$$

### b) Produit de Kronecker

Produit direct de deux matrices.  
Il n'est pas commutatif.
  $$ C_{m.p \times n.q} = A_{m \times n}\ \bleu{\otimes}\ B_{p \times q} $$

Son cas pratique est :   
   \demitab la concaténation d'une matrice $\bleu{A}$ avec une matrice $\bleu{B}$   
   \demitab de mêmes formats.
  $$ (1 0) \otimes A + (0 1) \otimes B $$ 

plus généralement, sa formule est :

  ![produit de kronecker](images/produit_de_kronecker.jpg)

\bigbreak

\underline{autres propriétés :}

Si $\mauve{f}$ est une fonction d'une variable réelle ($\mathbb{R}$)

   > alors $f\mauve{(A)}$ est une matrice dont les coefficients  
   > \demitab [c'est à dire : les nombres du tableau]  
   > sont les $\mauve{f(a_{i,j})}$.

(ça va de soi : $\mauve{f(x) = 3x + 1}$ appliqué à une matrice $\mauve{(1, 7, 2.4,-3)}$ en fera $\mauve{(4, 22, 8.2, -8)}$)

\bigbreak

Si $\mauve{f}$ est de plus dérivable,  
   \demitab et que la matrice $\mauve{A}$ l'est aussi \gris{(qu'est-ce qu'une matrice dérivable ?)}, alors :
$$ d[f(A)] = (df)(A) \land dA $$

