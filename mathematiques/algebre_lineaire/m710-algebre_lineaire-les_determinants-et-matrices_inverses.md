---
title: "m710. Algèbre Linéaire - les déterminants et matrices inverses"
subtitle: ""
author: Marc Le Bihan
---

\donnee{Le déterminant nous permet de savoir} \newline 
\donnee{si une matrice inverse existe} \newline 
\donnee{pour une matrice carrée donnée} 

\bigbreak

Une __matrice inverse__ $\mathbf{A^{-1}}$ de la matrice \danger{carrée} $A$, est celle qui est telle que :
$$ \bleu{AA^{-1}}\ \text{ ou }\ \bleu{A^{-1}A} = \mauve{I_{n}}\ \text{ la matrice identité} $$
elle permet, par exemple, de résoudre un système d'équations linéaires.

## 1) Résolution d'un système d'équations avec une matrice inverse

Soit le système d'équations à résoudre :
$$
\left\{
    \begin{array}{ll}
       2x + 3y = 13 \\
       3x + 4y = -6
    \end{array}
\right.
$$

La matrice $\mathbf{A}$ porte ses coefficients :
$$ \bleu{A} = \begin{bmatrix}
   2 & 3 \\
   3 & 4
\end{bmatrix} $$

\bigbreak  

et la matrice $\mathbf{A^{-1}}$ est sa matrice inverse :
$$ \mauve{A^{-1}} = \begin{bmatrix*}[r]
   -4 & 3 \\
   3 & -2
\end{bmatrix*} $$

\bigbreak  

La matrice __B__ est celle des solutions :
$$ \bleu{B} = \begin{bmatrix*}[r]
   13 \\
   -6
\end{bmatrix*} $$

\bigbreak  

et la matrice __X__ celle des variables qu'on veut calculer :
$$ \rouge{X} = \begin{bmatrix*}[r]
   x \\
   y
\end{bmatrix*} $$

\bigbreak  

$$ AX = B \iff A^{-1}AX = A^{-1}B \text{\ \ \ \ $\rightarrow$ \emph{ on multiplie des deux côtés par $A^{-1}$}} $$
$$ \ \ \ \ \tab \iff \mathbf{X = A^{-1}B} \text{\tab $\rightarrow$ \emph{ le terme de gauche, avec $A^{-1}A$, se simplifie en $I_{2}$}} $$
$$ \ \ \ \ \tab \iff \begin{bmatrix*}[r]
   x \\
   y
\end{bmatrix*} = 
\begin{bmatrix*}[r]
   -4 & 3 \\
   3 & -2
\end{bmatrix*} 
\begin{bmatrix*}[r]
   13 \\
   -6
\end{bmatrix*} = \begin{bmatrix*}[r]
   -70 \\
   51
\end{bmatrix*} $$

\bigbreak

   > c'est à dire :   
   > \demitab $\rouge{x} = -70$ et $\rouge{y} = 51$  
   > \demitab les solutions du système d'équations

\bigbreak

Mais peut-on garantir  
   \demitab l'existence d'une \definition{matrice inverse}  
   \demitab et la trouver ?

Il faut le vérifier en calculant un \definition{déterminant}.

## 2) Le déterminant

Un \definition{déterminant} est un __nombre réel__   
   \demitab associé à une __matrice carrée__ d'ordre $\bleu{n}$   
   \demitab (il est d'\definition{ordre $n$} lui-même)

Il est noté :

   > $\det(A)$  
   > $\det A$  
   > \text{ou} $|A|$

\bigbreak

Si $\mauve{\det(A) \neq 0}$  
   \demitab la matrice admet une inverse :  
   \demitab et elle s'appelle \definition{matrice régulière} ou __non singulière__.

\bigbreak

Si $\mauve{\det(A) = 0}$  
   \demitab la matrice n'en admettra pas :  
   \demitab et on la dira \definition{matrice singulière}

### a) Son algorithme de calcul est récursif

Il se fonde sur le fait qu'on sait calculer un déterminant d'ordre 2 (et d'ordre 1 aussi, d'ailleurs) :

\bigbreak

\underline{Déterminant d'ordre 1 :}
$$ \rouge{\det{|a_{11}|}} = a_{11} \text{\ \ \ \ et son inverse sera } \rouge{A^{-1}} = \Big[\frac{1}{a_{11}}\Big] $$
\tab \gris{(car $a_{11}\Big[\frac{1}{a_{11}}\Big] = [1] = \bleu{I_{1}}$)}

\bigbreak

\underline{d'ordre 2 :}
$$ \rouge{\det{\begin{vmatrix*}[r]
   a_{11} & a_{12} \\
   a_{21} & a_{22}
\end{vmatrix*}}} = a_{11}.a_{22} - a_{21}.a_{12} $$
(_soustraction en croix en partant de $a_{11}$ : $... - ...$_ )

   \begingroup \cadreGris

   > $$ \det{\begin{vmatrix*}[r]
   > 2 & 3 \\
   > 1 & 4
   > \end{vmatrix*}} = 2(4) - 1(3) = 8 - 3 = 5 \ \ (\neq 0 : \text{ cette matrice est régulière)}$$
   
   \endgroup

### b) Le déterminant d'ordre 2 a une interprétation géométrique : l'aire d'un parallélogramme

Le déterminant d'ordre 2, $\mauve{\det{\begin{vmatrix*}[r]
   a & b \\
   c & d
\end{vmatrix*}}}$, __pris en valeur absolue__, est l'\avantage{aire du parallélogramme} $(0,0), (a,b), (c,d), (a + c, b + d)$,  
c'est à dire : $| ad - bc |$.

![interprétation géométrique](images/algebre_lineaire_parallelogramme_determinant_ordre_2.jpg)  
(_Le parallélogramme s'inscrit dans un rectangle dont les côtés ont pour longueurs ${a+c}$ et ${b+d}$_)

\gris{Calculer l'aire d'un parallélogramme réclame de prendre sa hauteur relative à son côté :}\    
\gris{\demitab $((c,d), (a+c, b+d))$}

\gris{Ce n'est pas un rectangle où l'on calculerait $L \times \ell$ !}\   
\gris{$\to$ on pourrait imaginer un parallélogramme si effilé que son côté serait très très long, alors que son aire globale serait faible !}

Par là, l'aire du triangle (0,0), (a,b), (c,d) est la moitié de cette valeur absolue.

### c) La matrice inverse d'un déterminant d'ordre 2

Si le déterminant n'est pas nul, alors :
$$ A^{-1} = \frac{1}{ad - bc}\begin{vmatrix*}[r]
   d & -b \\
   -c & a
\end{vmatrix*} $$

\begingroup \cadreGris

> $\mauve{B^{-1}} = \begin{bmatrix*}[r]
>   2 & 3 \\
>   1 & 4
> \end{bmatrix*}$ \newline
> \newline
> \newline
> $\mauve{B^{-1}} = \frac{1}{5}\begin{bmatrix*}[r]
>   4 & -3 \\[6pt]
>   -1 & 2
> \end{bmatrix*} = \begin{bmatrix*}[r]
>   \frac{4}{5} & \frac{-3}{5} \\[6pt]
>   \frac{-1}{5} & \frac{2}{5}
> \end{bmatrix*}$

\endgroup
