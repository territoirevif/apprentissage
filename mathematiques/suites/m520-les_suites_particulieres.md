---
title: "m520. Les suites particulières"
subtitle: ""
author: Marc Le Bihan
---

## 1) Les suites adjacentes

   \begingroup \cadreAvantage

   > \underline{\textbf{Théorème :}}  
   > Soit $\mauve{(u_n)}$ et $\mauve{(v_n)}$ deux suites de nombres réels tels que :
   > 
   > 1. La suite $\mauve{(u_n)}$ est croissante
   > 2. La suite $\mauve{(v_n)}$ est décroissante
   > 3. $\mauve{(u_n - v_n)}$ tend vers zéro
   > \   
   > \   
   > Alors les suites $(u_n)$ et $(v_n)$  
   >    \demitab convergent toutes les deux vers la même limite  
   >    \demitab et elles sont dites \definition{adjacentes}. 

   \endgroup

\bigbreak
 
Lorsqu'elles le sont, elles vérifient :
$$ \forall m \in \mathbb{N}, \forall n \in \mathbb{N}, u_m \leq v_n $$
autrement dit, chaque terme $u_i$ de la suite $(u_n)$  
   \demitab est inférieur au terme $v_i$ de la suite $(v_n)$.

## 2) La suite de Cauchy

Il y a des cas où je ne connais pas la limite (éventuelle !) d'__une__ suite.

\bigbreak

Pour démontrer qu'elle est convergente,  
   \demitab on peut montrer qu'elle croissante \gris{[respectivement décroissante]}   
   \demitab et majorée \gris{[respectivement minorée]}

en utilisant la \definition{suite de Cauchy}.

\bigbreak

\demitab \begingroup \large $\forall \varepsilon > 0, \exists n \in \mathbb{N}, \text{tel que :}$ 

\tab $\forall m \in \mathbb{N}, \forall n \in \mathbb{N}, \{(m > N) \et (n > N)\} \implies |u_m - u_n| \leq \varepsilon$ \endgroup

équivalent à :

\demitab \begingroup \large $\forall \varepsilon > 0, \exists N \in \mathbb{N}, \text{tel que :}$ 

\tab $\forall n \in \mathbb{N}, \forall p \in \mathbb{N}, (n > N) \implies |u_{n+p} - u_n| \leq \varepsilon$ \endgroup

\bigbreak

Cela signifie qu'aussi petit que soit $\varepsilon$,  
   \demitab à partir d'un certain rang,  
   \demitab tous les éléments de la suite se trouvent dans une distance inférieure ou égale à $\varepsilon$.

__Toute suite convergente est une suite de Cauchy__

### \underline{Théorème fondamental :}

   > Une suite de nombres réels est convergente si c'est une suite de Cauchy.

## 3) Les suites récurrentes

