---
title: "m510. Les suites géométriques"
subtitle: "définition directe et par récurrence, somme, convergence, limite"
author: Marc Le Bihan
---

   \begingroup \cadreAvantage
 
   > - Le rebond d'une balle, qui s'atténue
   > - Un capital placé à 5%, plusieurs années

   \endgroup

\bigbreak

Une suite $(u_n)$ est géométrique si l'on passe d'un terme au suivant en multipliant toujours par un même nombre réel.

Une suite est géométrique si sa raison est un quotient \begingroup \large \definition{$\mathbf{\frac{u_{n+1}}{u_{n}} = q}$} \endgroup

# I) Définition directe ou par récurrence{#definition}

## 1) Définition par récurrence

\begingroup \large $u_{n+1} = u_n \times q$ \endgroup
   $$ \left\{
   \begin{array}{ll}
       u_{0} = 3 \\
       u_{n+1} = \frac{-1}{2}u_n \\
   \end{array}
   \right. $$

est une suite géométrique de raison $\frac{-1}{2}$

Par définition directe :  
\tab \begingroup \large $u_{n} = u_0.q^n$  \endgroup

ou

\tab \begingroup \large $u_{n} = u_1.q^{n-1}$  \endgroup

## 2) Détecter qu'une suite est géométrique

Une suite est géométrique si \begingroup \large $\mauve{\frac{u_{n+1}}{u_{n}}}$ \endgroup est constant, et cette constante est la raison.

\bigbreak

   \begingroup \cadreGris

   > La suite $\mauve{u_n = 5 \times 3^{n+2}}$ est-elle géométrique ?  
   > $$ \frac{u_{n+1}}{u_{n}} = \frac{5 \times 3^{n+3}}{5 \times 3^{n+2}} = \frac{3^{n+3}}{3^{n+2}} = 3 $$  
   > elle est donc géométrique.

   \endgroup

## 3) Déterminer si une suite géométrique est croissance ou décroissance :

   - si $q > 1$, la suite est croissante si $u_0 > 0$, décroissante si $u_0 < 0$
   - si $0 < q < 1$, la suite est décroissante si $u_0 > 0$, croissante si $u_0 < 0$
   - si $q = 1$, la suite est __stationnaire__   
   - si $q = -1$, la suite devient une succession de termes opposés : $\{3, -3, 3, -3...\}$  
    et de manière générale, quand $q$ est négatif, ses termes changent de sens :  
    on dit que la suite et __alternée__.

## 4) Exprimer une suite géométrique en fonction de $n$

Soit $(u_n)$ une suite géométrique de raison $q$,
$$ u_n = u_0 \times q^{n} $$

### Une suite est une fonction exponentielle{#exponentielle}
   
$$ y = a.q^x $$
où $x$ serait un entier positif.

### \underline{Remarque}

   > Plus généralement, si $(u_n)$ est une suite géométrique de raison $q$,  
   > et si $\bleu{n}$ et $\bleu{p}$ sont deux entiers naturels, on a :  
   >
   > \begingroup \large \avantage{$u_n = u_p \times q^{n - p}$} \endgroup

# II) La somme d'une suite géométrique (finie){#somme}

## 1) Finie

   > \underline{Rappel :} Si $\mathbf{x \neq 1}$, pour une \definition{identité usuelle} (pas une suite géométrique, mais juste la somme de $1 + x + x^2 + x^3 + x^4 + ... + x^n$) c'est :
   > $$ \sum^{n}_{i=0} x^i = 1 + x + x^2 + x^3 + x^4 + ... + x^n = \frac{1 - x^{n+1}}{1 - x} $$

   \begingroup \cadreGris

   > $$ \text{exemple pour } x=3 \text{ et } n=3 : \sum^3_{i=0} 3^i = 1 + 3 + 9 + 27 = \frac{1 - 3^4}{1 - 3} = \frac{-80}{-2} = 40 $$

   \endgroup

   \begingroup \cadreAvantage

   > La \definition{somme d'une suite géométrique} finie est :  
   > \tab \definition{\begingroup \large $\mathbf{S = u_0\Big(\frac{1 - q^{n+1}}{1 - q}\Big)}$ \endgroup} 
   > \   
   > ou  
   >
   > \tab \definition{\begingroup \large $\mathbf{S = u_1\Big(\frac{1 - q^{n}}{1 - q}\Big)}$ \endgroup}

   \endgroup

\bigbreak

\underline{Démonstration :}

![Démonstration du calcul de la somme d'une suite géométrique](images/démonstration_somme_suite_géométrique.jpg){width=80%}

## 2) Infinie et convergente{#convergente}

Cela implique que $\mathbf{q \in ]-1, 1[}$ sinon elle ne convergerait pas (elle atteindrait l'infini ou pour $q=1$, serait stationnaire) 

Dans ce cas, l'identité usuelle se simplifie, parce que $x^{n+1}$ ne vaut quasiment plus rien :
$$ \sum^{n}_{i=0} x^i = \frac{1}{1 - x} $$

   > et du coup, la somme de la suite, toujours pour $\mathbf{q \in ]-1, 1[}$ vaut :  
   > \tab \definition{\begingroup \large $\mathbf{S = u_0\Big(\frac{1}{1 - q}\Big)}$ \endgroup}  
   >
   > ou  
   >
   > \tab \definition{\begingroup \large $\mathbf{S = u_1\Big(\frac{1}{1 - q}\Big)}$ \endgroup}

# III) La limite d'une suite géométrique{#limite}

   - Si \donnee{$\mathbf{q \in ]-1, 1[}$} alors 
     $$ \lim_{n \to \infty}{q^n} = 0 $$
   
      > __La suite converge vers zéro__
   
   - Si \donnee{$\mathbf{q > 1}$} alors
     $$ \lim_{n \to \infty}{q^n} = +\infty $$
   
      > __La suite diverge__ : car $q^n$ ne se fixe sur aucune valeur finie.
   
   - Si \donnee{$\mathbf{q = -1}$} alors il n'y a pas de limite, puisque la valeur des termes oscille entre $1$ et $-1$.
   
   - Si \donnee{$\mathbf{q < -1}$} alors il n'y a pas de limite non plus, car les termes alternent aussi en signes.

