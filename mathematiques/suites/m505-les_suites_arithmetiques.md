---
title: "m505. Les suites arithmétiques"
subtitle: "définition directe ou par récurrence, somme d'une suite"
author: Marc Le Bihan
---

## 1) Définition par récurrence d'une suite arithmérique {#definition} 

   \begingroup \cadreAvantage

   > ### \underline{Définition :}
   > Une suite $(u_n)$ est arithmétique si l'on passe d'un terme au suivant en __ajoutant toujours le même__ réel $\mathbf{r}$.  
   > \   
   > \tab \definition{$\mathbf{u_0}$} donné,  
   > \tab puis $\forall n, u_{n+1} = u_n +$ \definition{$\mathbf{r}$}  
   > \    
   > ou bien :  
   > \tab $u_n = u_1 + (n-1)r$  
   > \tab ou
   > \tab $u_n = u_0 + nr$

   \endgroup

\bigbreak

$\bleu{n}$ s'appelle l'\donnee{indice} : il permet de repérer le __rang__ d'un terme.
   $$ \left\{
   \begin{array}{ll}
       u_{0} = 10 \\
       u_{n+1} = u_n - 3 \\
   \end{array}
   \right. $$

est une suite arithmétique de raison $r = -3$.

   \begingroup \cadreGris

   > La suite des entiers naturels est une suite arithmétique de raison 1,  
   > celle des nombres pairs, de raison 2...

   \endgroup

\bigbreak

Dans une suite arithmétique :

   - La raison \definition{$r$} est constante : $\mathbf{u_{n+1} - u_{n} = r}$ 
   - C'est une fonction linéraire, avec $n \in \mathbb{N}$

## 2) Suite arithmétique croissante, décroissante, stationnaire{#croissance}

Cela dépend de la différence entre les termes $u_{n+1} - u_{n}$, donc de $r$.  

   \begingroup \cadreAvantage

   > ### \underline{Théorème}
   > Si $(u_n)$ est une suite arithmétique de raison $r$,  
   > 
   >    - Si $r > 0$ est positif, la suite sera __croissante__,
   >    - $r < 0$, elle sera __décroissante__.
   >    - $r = 0$ elle sera __stationnaire__.

   \endgroup

## 3) Expression en fonction de $n$

   \begingroup \cadreAvantage

   > ### \underline{Théorème}
   > Soit une suite arithmétique $(u_n)$ de raison $r$, on a :
   > $$ u_n = u_0 + nr $$

   \endgroup

\avantage{Une suite arithmétique est la même chose qu'une fonction affine} $\mathbf{y = ax + b}$,  
     \ \ \ \ où $\mathbf{a}$ joue le rôle de $\mathbf{r}$.  

### \underline{Remarque :}

   > Plus généralement, si $(u_n)$ est une suite arithmétique de raison $r$,

   > si $n$ et $p$ sont deux entiers naturels, on a :

   > \begingroup \large \avantage{$u_{n} = u_{p} + (n - p)r$} \endgroup

## 4) La somme des termes d'une suite arithmétique{#somme}

Somme des termes d'une suite : 
   $$ \textcolor{blue}{\sum^{n}_{i=0}u_{i}} = u_{0} + u_{1} + u_{2} + ... + u_{n} = (n+1)\frac{u_{0} + u_{n}}{2} $$
   $$ \textcolor{blue}{\sum^{n}_{i=1}u_{i}} = u_{1} + u_{2} + ... + u_{n} = n\frac{u_{1} + u_{n}}{2} $$

### \underline{Démonstration 1 :}

![Démonstration de la somme d'une suite arithmétique](images/demonstration_somme_suite_arithmétique.jpg){width=80%}

### \underline{Démonstration 2 :}

Pris deux à deux, les termes $u_{0}$ et $u_{n}$, $u_{1}$ et $u_{n-1}$, $u_{2}$ et $u_{n-2}$ sont équidistants  
\ \ \ \ car en passant de $u_{0}$ à $u_{1}, u_{2}...$ on ajoute $r$  
\ \ \ \ et en passant de $u_{n}$ à $u_{n-1}, u_{n-2}$ on retranche $r$.

De sorte que la somme $u_{0} + u_{n} = u_{1} +  u_{n-1} = u_{2} + u_{n-2}...$

C'est comme cela que l'on démontre que $\sum\limits^{n}_{i=0}u_{i} = (n+1)\frac{u_{0} + u_{n}}{2}$

   > Comme on est entrain de considérer (pour une suite débutant à $u_{0}$)  
   > $n+1$ fois une somme de __deux__ termes $u_{x}$ et $u_{y}$ qu'on dit constante (équidistante),  
   > utiliser cette somme dans une autre somme doublerait son total. 
   > 
   > C'est pour cette raison qu'il y a une _division par deux_.

