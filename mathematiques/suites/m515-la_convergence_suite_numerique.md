---
title: "m515. La convergence d'une suite numérique"
subtitle: ""
author: Marc Le Bihan
---

# I) Qu'est-ce que la convergence ?

## 1) Definition 

On dit qu'une suite $(u_n)$ converge (ou qu'elle est convergente)  
\demitab s'il existe un réel $\bleu{\ell}$, appelé \definition{limite}, tel que, avec :

   > $\bleu{\varepsilon}$ : un très petit nombre  
   > $\bleu{N}$ : un très grand nombre entier  
   > $\bleu{A}$ : un très grand nombre reél
$$ \forall \varepsilon > 0, \exists N \in \mathbb{N}, \forall n \in \mathbb{N}, (n > N) $$
\gris{$n$ ici, est appelé un \emph{seuil (cut off)}}

\begingroup \large $\implies{| u_n - l | < \varepsilon}$ \endgroup

\bigbreak

\avantage{c'est à dire que $| u_n - \ell |$ tend vers zéro}

\bigbreak

On dit que $u_n$ tend vers $\ell$, et l'on écrit :
$$ \ell = \lim\limits_{n \to \infty}(u_n) $$

## 2) La divergence

Si $(u_n)$ n'est pas convergente, on dit qu'elle diverge :  
\danger{pas forcément en allant vers l'infini !} $(-1)^n$ diverge déjà !

## 3) une suite tend vers l'infini, si...

Une suite $(u_n)$ tend vers l'infini si
$$ \forall A \in \mathbb{R}^+, \exists N \in \mathbb{N}, \forall n \in \mathbb{N}, (n > N) \implies (u_n > A) $$
$$ \forall A \in \mathbb{R}^+, \exists N \in \mathbb{N}, \forall n \in \mathbb{N}, (n > N) \implies (u_n > -A) $$

\underline{Remarque :}

  > $(u_n) \to \ell$ ssi une nouvelle suite $(v_{n} = u_{n} - \ell) \to 0$  
  > \gris{ce qui signifie que $(u_n) \to 0$}

On peut changer un terme ou démarrer la suite par un indice supérieur à 0 sans changer sa nature.

\bigbreak

La limite d'une suite convergente est __unique__.  
Et c'est pour cela que $(-1)^n$ n'est pas convergente.

# II) Une suite convergente

## 1) Suite majorée

\begingroup \cadreAvantage

   > ### \underline{Définition :}
   >
   > Une suite $(u_n)$ est dite majorée \gris{[respectivement minorée]} s'il existe un réel $M$ \gris{[respectivement $m$]}, tel que : 
   $$ \forall n \in \mathbb{N}, u_n \leq M \demitab \gris{[\text{resp.} m \leq u_n]} $$

\endgroup

\bigbreak

\attention{Un majorant n'est pas une limite !!!} Rien ne dit que la suite tend vers $M$ !

Une suite est dite \definition{bornée} si elle a à la fois un __majorant__ et un __minorant__.

### \underline{Théorème :}

Toute suite __croissante et majorée__ est convergente.  
si elle converge vers une limite $\ell$, alors quelque-soit $n, u_n \leq l$

Toute suite __décrosssante et minorée__ est décroissante.

## 2) Opérations sur les limites 

la somme de deux suites (attention : pas la somme de tous les termes d'une suite), s'écrit :

   > $s_n = u_n + v_n$  
   > $s_n$ s'appelle le terme général.

### \underline{Théorèmes :}

Soit $(u_n) et (v_n)$ deux suites convergentes, tendant vers $\ell_1$ et $\ell_2$.

  > alors la limite de $\lim\limits_{n \to \infty}(u_n + v_n) = \ell_1 + \ell_2$  
  > $\lim\limits_{n \to \infty}(\lambda u_n) = \lambda \ell_1$  
  > $\lim\limits_{n \to \infty}(u_n . v_n) = \ell_1 . \ell_2$ 

\bigbreak

Soit $(v_n)$ une suite convergente vers 0 et $(u_n)$ une suite bornée,

   > la suite $(u_n.v_n) \to 0$, elle aussi.

\bigbreak

Si $(u_n)$ et $(v_n)$ sont deux suites convergentes, avec des limites $\ell_1$ et $\ell_2$,  
si $(v_n)$ est une suite :

   - dont les \attention{termes $v_n$ sont non nuls}
   - la \attention{limite $\ell_2$ est non nulle}

alors 

   > \begingroup \large $\lim\limits_{n \to \infty}(\frac{u_n}{v_n}) = \frac{\ell_1}{\ell_2}$ \endgroup

\bigbreak

Si $(u_n)$ est une suite qui tend vers l'infini, alors :

  > $\forall n, u_n \neq 0, \frac{1}{u_n} \to 0$

\bigbreak

\attention{Piège classique !} $\lim\limits_{n \to +\infty}(\frac{1}{u_n})$ est l'indétermination

   \demitab car pour $n = 0$,  
      \tab $\frac{1}{n} = \infty$

   \demitab même si pour les grandes valeurs de $n$, il tend vers 0 !

