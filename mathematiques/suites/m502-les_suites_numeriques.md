---
title: "m502. Les suites numériques"
subtitle: "Les suites numériques sont l'entrée en matière pour l'étude des fonctions"
author: Marc Le Bihan
---

Une \definition{suite numérique} est une suite indexée de nombres :

   > une suite de nombres rangée dans un certain ordre,  
   > et une suite arithmétique en est un type.

Elle a un premier terme, un deuxième terme, etc.

\bigbreak

Une suite numérique est une \definition{fonction} qui à \bleu{tout entier naturel $n$} associe un nombre noté $\bleu{u_n}$ ou \bleu{$u(n)$}.

\bigbreak

\underline{Notations :}

   > lettres $\mauve{u, v, w}$, pour caractériser une suite  
   > $u_n$ est appelé \mauve{terme d'indice $n$} ou \mauve{de rang $n$}  
   > la suite, dans sa globalité, est notée $\mauve{u}$ ou $\mauve{u(n)}$

## 1) Génération de la suite

On peut générer la suite soit par une formule explicite, soit par un procédé.

### a) Par une formule explicite

   \begingroup \cadreGris

   > $u_n = -n^2 +n - 2$  
   > \tab $\implies u_{0} = -2, u_{1} = -2, u_{2} = 4...$

   \endgroup

\underline{Remarque :}

   > La suite est de la forme $\mauve{y = f(n)}$ où $\mauve{f}$ est une fonction.

### b) À l'aide d'un procédé

\definition{Procédé :} j'ai une __condition initiale__ et un __procédé__ pour avancer.

   > Soit $v(n)$ la suite de premier terme $v_{0} = 5$.
   >
   > le terme suivant est obtenu en ajoutant $\mathbf{3}$, puis en divisant par $\mathbf{2}$ :
   >
   > \tab \begingroup \large $v_{1} = \frac{v_0 + 3}{2} = \frac{5+3}{2} = 4$, $v_{2} = \frac{7}{2}$ \endgroup

\bigbreak

Dans ce cas, le terme d'indice n est calculé à partir du terme précédent, et on le note :
  \begingroup \large $$ \left\{
   (v_n) = \begin{array}{ll}
       v_{0} = 5 \\
       v_{n+1} = \frac{v_n + 3}{2} \\
   \end{array}
   \right. $$ \endgroup

## 2) Sens de variation d'une suite

\underline{Definition:}

   \begingroup \cadreAvantage

   > Une suite $(v_n)$ est croissante si pour tout entier naturel $n$, 
   > $$ \mathbf{u_{n+1} \geq u_{n}} $$
   > 
   > elle est décroissante si :
   > $$ \forall n \in \mathbb{N}, \mathbf{u_{n+1} \leq u_{n}} $$
   > 
   > strictement croissance avec \>,  
   > strictement décroissante avec \<,  
   > constante avec $u_{n+1} = u_{n}$

   \endgroup

### \underline{Propriété 1}

Les variations de $(u_{n})$ peuvent être étudiées par le signe de $\mauve{(u_{n+1} - u_{n})}$.

   \begingroup \cadreAvantage

   > Si $\forall n \in \mathbb{N}, u_{n+1} - u_{n} \geq 0$,
   > \tab alors $(u_{n})$ est croissante.
   >
   > Si $\forall n \in \mathbb{N}, u_{n+1} - u_{n} \leq 0$,
   > \tab alors $(u_{n})$ est décroissante.

   \endgroup

\bigbreak

   \begingroup \cadreGris

   > Etudier si $u_{n} = \frac{3}{n + 2}$ est croissante :  
   > \    
   > \tab $u_{n+1} - u_{n} = \frac{3}{(n+1) + 2} - \frac{3}{n + 2} = \frac{3}{n + 3} - \frac{3}{n + 2} = \frac{-3}{(n+3)(n+2)}$  
   > \linebreak
   > \tab $n \in \mathbb{N}$, donc $n+3$ et $n+2$ sont positifs  
   > \tab donc $u_{n+1} - u_{n} \geq 0$

   \endgroup

### \underline{Propriété 2}

Soit $(u_n)$ définie par $u_n = f(n)$

   - si la fonction $f$ est croissante sur $[0, +\infty[$, alors la suite $(u_n)$ est croissante.
   - si la fonction $f$ est décroissante sur $[0, +\infty[$, alors la suite $(u_n)$ est décroissante.

