---
title: "s530. k-means"
subtitle: "Méthode des centres mobiles"
author: Marc Le Bihan
---

\donnee{classification $\rightarrow$ supervisée}

---

La méthode \definition{k-Means} est appelée \definition{la méthode des centres mobiles}.

## 1) Le principe du k-means : la méthode des centres mobiles

C'est un algorithme de regroupement (\definition{cluster})  
   dans lequel $\mauve{k}$ centres  
   sont aléatoirement assignés au sein des données.

\bigbreak

   1. Les points les plus proches de ce point  
   sont assignés à une classe,  
   et le centre de ces points est calculé : on l'appelle le \definition{centroïde}.

\bigbreak

   2. On met l'étiquette "_\bleu{je suis dans cette classe centroïde}_"  
     aux les points les plus proches de ce centroïde.  

\bigbreak

   3. On calcule le nouveau centre qu'ont ces points   
      (puisqu'on en a resélectionnés)  
      et l'on déplace le centroïde à ce nouveau centre.

\bigbreak

   4. On réitère cette opération un nombre fini de fois,  
      ou bien jusqu'à ce qu'il y ait __convergence__  
      ($\rightarrow$ nos points centraux cessent de bouger).

## 2) Mise en oeuvre par le Machine Learning

Nous allons entraîner modèle par un algorithme _K-Mean_

   \demitab appliqué dans le cadre d'un pipeline de transformation  
   \demitab créé, par exemple, [comme proposé ici](../informatique/big_data/sp15-machine_learning-le_one_hot_encoding.pdf)

\bigbreak

\underline{Scala :} Enclenchement d'un _K-Mean_  

   \code

   L'algorithme __KMeans__ aura ce nom et utilisera cette classe tant qu'il ne sera pas entraîné,  
   puis en aura une, suffixée par \fonction{Model} quand il sera entraîné : \fonction{KMeansModel}  
   (Code Spark 2.0 à migrer en 3.x)
   
   > ```scala
   > val kmeans = new KMeans()
   >   .setK(20)
   >   .setSeed(1L)
   >   
   > val kmModel = kmeans.fit(transformedTraining)
   > kmModel.computeCost(transformedTraining) 
   > ```

   \endgroup

