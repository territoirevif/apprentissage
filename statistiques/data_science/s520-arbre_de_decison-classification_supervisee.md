---
title: "s520. Arbre de décision et leur extension Random Forest"
subtitle: ""
author: Marc Le Bihan
---

\donnee{classification $\rightarrow$ supervisée $\rightarrow$ analyse discriminante}

---

\underline{Son but :} Identifier les attributs pertinents dans un ensemble de données.
   
Chaque noeud de l'arbre fait référence à :  
   \demitab \bleu{\underline{une variable}}  
   \demitab et \bleu{\underline{donne une condition}}

En y associant :
   
   - Le nombre d'individus pour laquelle elle est vraie   
     (on place les noeuds enfants en dessous, à gauche)
   
   - Le nombre d'individus pour laquelle elle est fausse 
     (on place les noeuds enfants en dessous à droite).

\bigbreak

\underline{Exemple :} partie de l'arbre où les tests sont "vrais" : 
   
   ![la partie de l'arbre où les tests sont vrais](./statistiques/machine_learning_fig1-8.jpg)

