---
title: "s560. Autres apprentissages non supervisés"
subtitle: ""

keywords:
- prédiction
- classification
- apprentissage non supervisé
- random forest
- arbre de décision
- xgboost
- catboost
- ressemblance
- clustering
- kmean
- lda
- mixture de gaussiennes
- algorithme par renforcement
- tester en marchant
- sarsa
- sarsamax
- deep q network
- pas de variables à prédire
- structure sous-jaçente
- modélisation

author: Marc Le Bihan
---

Notre objectif : \definition{prédire} ou \definition{classer} des données à venir,

   > en se basant sur des données, mais sans exemples de réponse.

# I) Question type : "Quel est l'âge de mon internaute ?"

\underline{Prédiction :} trouver une donnée manquante : \emph{\bleu{Quel est l'âge de mon internaute ?}}

Utile aussi quand on considère que le futur ressemblera au passé...

   > $\rightarrow$ pour la prévision de pannes de matériel

   - Prédiction  
   - Random forest : permet de déterminer, par exemple, les attributs pertinents dans un ensmeble de données  
   - Arbre de décision
   - XgBoost
   - CatBoost

# II) Question type : "Quels internautes se ressmblent ?"

\underline{classification, segmentation :} \emph{\bleu{Quels internautes se ressemblent ?}}

Utile quand on pense que de mêmes causes produisent les mêmes effets.

   - Clustering
   - Kmean
   - LDA
   - mixture de gaussiennes

# III) Question type : "Si je propose dix publicités à mon internaute, laquelle préfère t-il ?"

\underline{Test de réactions :} \emph{\bleu{Si je propose dix publicités à mon internaute, laquelle préfère t-il ?}}

Utilise un algorithme par renforcement

   - Tester en marchant
   - Sarsa, SarsaMax
   - Deep Q network

# IV) Pas de variables à prédire, mais quelle est la structure sous-jaçente ?

Découvrir \definition{des structures sous-jacentes} dans un jeu de données.

Il n'y a __pas__ de __variables à prédire__.

Exemple d'usages :

   - Détection d'anomalies : repérer un évènement non standard.
   - Segmentation d'utilisateurs : déterminer quels attributs ils partagent.  
   - Modélisation de sujets : à partir de documents, analyser les mots qui s'y trouvent et trouver des relations entre-eux.  

