---
title: "s540. Les réseaux de neurones"
subtitle: ""
author: Marc Le Bihan
---

\donnee{classification $\rightarrow$ supervisée $\rightarrow$ non linéaire}

---

# I) Qu'est-ce qu'un réseau de neurones ?

## 1) Le but d'un réseau de neurones

Les réseaux de neurones classent des données  
   \demitab ou prédisent des valeurs futures ou manquantes. 

\bigbreak

Ils servent aussi à :
   
   - l'analyse d'images
   - la reconnaissance de formes

## 2) Comment sont-ils définis ?

### a) Un réseau de neurones est une fonction

Un \definition{neurone} est __une fonction__ qui 

   - transforme \definition{\underline{des}} \bleu{entrées}
   - en \definition{\underline{une}} \bleu{valeur de sortie}.

### b) Ce qui définit un réseau de neurones

Un réseau de neurones est un réseau ordonné par couches,  
   \demitab  qui se définit par trois informations :

   1. Le nombre de neurones sur la couche d'entrée  
      et sur celle de sortie

   2. Le nombre de neurones par couche,  
      et le nombre de couches

   3. Les interconnexions entre les neurones

![Un réseau de neurones](./statistiques/machine_learning_fig1-4.png)  
_Image : un réseau de neurones_ 

## 3) Principe de l'entraînement et de la prédiction

Les réseaux de neurones entrent dans la famille des classificateurs,  
   \demitab et les classificateurs portent sur des valeurs __qualitatives__.

\bigbreak

On l'entraine sur une base d'apprentissage  
   \demitab où chaque individu désigne la classe à laquelle il appartient,  

et le but de la prédiction   
   \demitab c'est de prédire pour un futur individu aux caractéristiques données,  
   \demitab $\rightarrow$ la classe probable à laquelle il appartiendra.

# II) Qu'est-ce qu'un neurone ?

## 1) Principe

\underline{Un neurone :}

![Un neurone avec ses entrées](statistiques/un_neurone.jpg)

   1. reçoit plusieurs valeurs $\bleu{\{x_1, x_2, ..., x_n\}}$  
     \demitab correspondant aux variables d'un individu,  
   2. sur lequelles il applique des \definition{poids} (ou coefficients de pondération) $\rouge{\{w_1, w_2, ..., w_n\}}$  
      \demitab pour résumer toutes ces valeurs d'entrée en une seule : $\mauve{a}$.  
      \demitab \rouge{mais ces poids $w_1, w_2...$, on ne les connait initialement pas...}   
   3. puis, il applique une \definition{fonction de \underline{transfert}} $\bleu{f(a)}$  
      \demitab qui renvoie une valeur dans $\mathbf{[0,1]}$

## 2) La fonction de transfert

### a) C'est n'importe laquelle de notre choix

La fonction de transfert $\bleu{f(a)}$ est choisie comme on veut  
    \demitab (mais on aime __la choisir dérivable__),  
    \demitab et renvoie une valeur \rouge{$o$} dans $\mathbf{[0,1]}$.

### b) Couramment choisie : la fonction de transfert sigmoïde ou fonction logistique

La fonction de transfert choisie, est souvent celle :

   \demitab \mauve{sigmoïde} (ou \mauve{fonction logistique}). 

\bigbreak

Elle est définie par :

   > $\rouge{o}$ : une valeur de sortie du neurone  
   > $\mauve{a}$ : une valeur résumée de toutes les valeurs en entrées du neurone
   $$ \rouge{o = f(a)} = \frac{1}{1 - e^{\mauve{-a}}} $$

   dont la dérivée est :
   $$ f'(a) = o(1-o) \demitab =f(a).[1-f(a)] $$

## 2) Trouver les poids

Les poids ne sont pas initialement connus.

Il faut :

   > 1. Trouver des poids initiaux  
   >    que l'on ajuste ensuite.   
   >    $\implies$ c'est une phase d'apprentissage,  
   >
   > 2. que l'on fait suivre d'une phase de prédiction  
   >    sur d'autres données.

### a) Rôles de la base d'apprentissage et de celle de prédiction

Il faut deux séries de vecteurs et de matrices pour créer un réseau de neurones :

   1. La première, pour régler les poids des matrices  
      à partir de la base d'apprentissage.  
      \demitab $\rightarrow$ elle minimisera l'erreur entre le résultat souhaité et celui obtenu.

   2. La seconde, servira à prédire de nouvelles données.

![Une fois réglé](./statistiques/machine_learning_fig1-5.png)  
_Image : une fois réglé_

### b) La base d'apprentissage

Pour entraîner un(des) neurone(s), il faut une \definition{base d'apprentissage}.

   > $\bleu{x_{1}...x_{n}}$ : valeurs d'entrées  
   > $\bleu{t}$ : résultat attendu pour $\mauve{o}$ \gris{(c'est à dire qu'on dit que $o = t$)}  
   > \    
   > $\rouge{\varepsilon}$ : l'erreur entre le $\bleu{t}$ souhaité  
   >     \demitab et le $\rouge{o}$ obtenu en sortie du neurone.

![Un ensemble d'individus transmis à un neurone](./statistiques/une_serie_individus_transmis_à_un_neurone.jpg)  
_Ici, pour 4 vecteurs d'individus : 3 variables dans chaque_

Cela lui fait régler les poids $\mauve{W^{t}}$

\rouge{\underline{Attention !} le $t$ de $W^{t}$ signifie : matrice transposée}  
   \demitab \rouge{et n'a rien à voir avec l'autre $t$ !}

\bigbreak

Dit autrement :

   \demitab nos $\bleu{x_{1}...x_{n}}$  
   \demitab vont être pondérés par une matrice $\mauve{W^{t}}$, à l'aide de poids $w$,  
   \demitab pour devenir un $\mauve{x}$ d'entrée à notre fonction $\bleu{f(x)}$ (ou plutôt : notre $\bleu{f(a)}$ de tout à l'heure).

\bigbreak

On aura alors :
\begingroup \large $$ \rouge{o} = \frac{1}{1 + e^{-a}} $$ \endgroup

et l'erreur :
\begingroup \large $$ \rouge{\varepsilon} = t - o $$ \endgroup

### c) Mise à jour des poids par itérations successives

Mais on a un \underline{problème d'oeuf et de poule},  
puisque $\mauve{W^{t}}$ on ne l'a pas :  
   \demitab les poids $w$, on ne les connaît initialement pas.

\bigbreak

Il va falloir les mettre à jour, __par itérations successives__,   
\demitab et pour cela, il faut calculer la dérivée
$$ f'(a) = o(1-o) $$
ce qui peut être fait immédiatement, si l'on fixe une première valeur de $\bleu{w}$.

### d) Résumé de la procédure d'évaluation d'un neurone

Son pseudo code serait :
   $$ a = \sum\limits^{n}_{i=1}x_{i}.w_{i1} \text{\gris{(rappel le 1 à côté du $i$ signifie : neurone n°1)}} $$ 
   $$ o = \frac{1}{1 + e^{-a}} $$
   $$ f'(o) = o(1 - o) $$
   $$ \varepsilon = t - o $$

## 3) La descente de gradient

Un coefficient $\mauve{\mu}$ va servir à \definition{descente de gradient}, pour

   - éviter les oscillations,
   - ou une convergence lente

   \begingroup \cadreGris

   > Typiquement, on fixe $\mauve{\mu = 0.9}$  
   > \demitab puis, on le fait décroître de $0.001$ (un millième),  
   > \demitab mais en veillant à ce qu'il ne descende pas en dessous de $\mu = 0.1$

   \endgroup

\bigbreak

De là :

   > $\mauve{\Delta_{1}}$ est la direction du gradient (ici, du neurone n°1)
   $$ \rouge{\Delta_{1}} = -\varepsilon_{1}.f'(a) $$

   \code \small

   > Pour i = 1 à `nombre_entrées_du_neurone` (nombre d'arcs)  
   > \ \ \ $\Delta W_{i1} = \mu.x_{i}.\Delta_{1}$ \demitab # 1 est le numéro du neurone, ici.  
   > \ \ \ $W_{i1} = W_{i1} + \Delta W_{i1}$  
   > FinPour

   \endgroup

