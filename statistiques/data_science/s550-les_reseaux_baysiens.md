---
title: "s550. Les réseaux baysiens"
subtitle: ""
author: Marc Le Bihan
---

## 1) Objectif : mettre en évidence les relations entre les données d'un problème

### a) C'est un graphe

Le réseau baysien aide à mettre en évidence les corrélations entre variables.

\bigbreak

C'est un __\textcolor{blue}{graphe}__ où :

   - un __sommet__ est une __\textcolor{blue}{variable aléatoire}__   
   - un __arc__ une __relation de dépendance__  
       \demitab entre la variable à l'origine de l'arc  
       \demitab et celle finale

\bigbreak

qui permet :

   - d'identifier
   - et de comprendre 

   \demitab les relations entre données d'un problème.

### b) Où l'on peut injecter de nouvelles données

Une fois construit,
 
   \demitab une nouvelle donnée peut être injectée dans le réseau,  
   \demitab et l'on peut répondre à une question :   
   \demitab _\bleu{Sachant que ... quelle est la probablité que ... ?}_

   ![exemple de reseau](./statistiques/reseau_baysien.jpg){width=90%}  
   _Un exemple de réseau baysien, en médecine, où l'on détecte les pathologies  
selon la bonne (B) ou mauvaise (M) alimentation et niveau d'activité physique d'une personne  
(ici, le tableau montre la probabilité d'une personne d'avoir du cholestérol (colonne `O`)_

\bigbreak

ou des questions plus avancées : 

   \demitab "\bleu{\emph{Sachant qu'un individu a une mauvaise alimentation et des problèmes cardiaques}}  
   \demitab \bleu{\emph{quelle est la probabilité qu'il ait \textbf{en plus} du diabète ?}}"

\attention{Il ne peut être construit que par des experts}

\bigbreak

Il faut :

   1. Déterminer les tables de probabilité et la structure du réseau,
   2. puis, il s'utilse avec une nouvelle donné pour laquelle il faut une prédiction

