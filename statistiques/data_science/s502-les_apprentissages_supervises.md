---
title: "s502. Les apprentissages supervisés"
subtitle: "Regression linéaire, régression multiple"
author: Marc Le Bihan
category: statistiques
keywords:
- modèle
- prédiction
- classement
- apprentissage
- base d'apprentissage
- fonction de coût
- fonction d'erreur
- variable quantitative
- variable qualitative
- variable ordinale
- moyenne du carré des écarts
- RMSE
- précision
- rappel
- AUC
- jeu d'apprentissage
- jeu de test
- fit
- transform
- régression linéaire
- régression multiple
---

# I) Principe

\demitab On entraîne un modèle avec des variables "historiques" pour __prédire__ ou __classer__ ensuite les valeurs que ces variables auront, en les affinant à chaque itération.

\demitab Il est basé sur des données passées avec des exemples de réponse. Sur une \definition{base d'apprentissage} où sont des informations utiles.

## 1) Fonction de coût ou d'erreur
 
Dans ces algorithmes, on se fonde sur une erreur (au carré entre la valeur obtenue et celle que l'on aurait dû obtenir, par exemple), grâce à une \definition{fonction de coût ou d'erreur}. Pour une variable :

   - quantitative (un nombre) : moyenne du carré des écarts (RMSE)
   - qualitiative (catégorie) : précision, rappel, AUC
   - ordinal (un classement) : mix des deux

Il est sage de juger cette fonction de coût d'un point de vue métier (exemple : lui donner un sens d'un point de vue économique).

## 2) Jeu d'apprentissage, jeu de test

Il y a un :

   - __jeu d'apprentissage__ : souvent 70 ou 80% des enregistrements
   - __jeu de test__ : souvent 20% ou 30%

\bigbreak

\danger{Veiller à ne pas reprendre dans le jeu de test des lignes du jeu d'apprentissage :} \linebreak
\danger{\demitab cela peut arriver si l'on prend deux échantillons distincts, chacun totalement aléatoire,} 
\danger{\demitab parce que l'un prendra des enregistrements de l'autre !}

\bigbreak

C'est la fonction \fonction{fit} de _Spark_ qui détermine un modèle à partir des données d'apprentissage,   
et \fonction{transform} qui tente une prédiction avec le jeu de données de test.

# II) Régression : pour prédire des valeurs quantitatives

Pour prédire des valeurs quantitatives :

   - de ventes
   - taille des personnes
   - détermination du nombre de personnes qui iront voir un spectacle

## 1) Régression Linéaire

## 2) Régression Multiple

