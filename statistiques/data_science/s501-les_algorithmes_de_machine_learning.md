---
title: "s500. Les manières de faire de l'Intelligence Artificielle"
subtitle: "Supervisés, non supervisés. Regression, classification"
author: Marc Le Bihan
---

# I) Qu'est-ce qu'on en fait ? Pourquoi en a t-on besoin ?

Voici des interrogations que l'on peut avoir, que l'on soit devant des données ou pas :

## 1) Je sais ce que je cherche

   - __Combien ça devrait me coûter, vu ma situation, de faire installer ceci chez moi ?__  
     → régression linéaire (droite $y = ax + b$), régression logistique (sinusoïde) : __je l'estime par calcul mathématique sur les prix qu'ont payés les autres, selon leurs propres situations__

   - __À quel groupe appartiennent les communes que j'ai devant moi ?__  
     À celles de montagne, d'île, de plaine ? Les règlements pour chacune ne sont pas les mêmes ; bien-sûr, il vaudrait mieux prendre une carte... :  
     → k-nearest neighbors : __je le présage d'après celles qui sont les plus proches de moi.__

## 2) Je ne sais pas ce que je cherche

   - __Quelles activités principales trouve-t-on dans les communes que j'ai devant moi ?__  
     Il y a des centaines d'activités qui existent (professions, industries, agriculture, enseignement...).  
     Je sais combien de groupes je veux faire, mais je ne sais pas comment il faut les faire et les trouver :  
     est-ce que quand il y a des cultures, il y a de l'élevage et ça ira dans le même groupe ?

     → k-means : je le présage aussi d'après celles qui sont les plus proches de moi.   
     Il va falloir que je découvre après ce qu'il peut bien y avoir dans ces groupes : comment ils ont été constitués. Parce que l'algorithme les a mises ainsi "_Parce qu'elles ont un air de famille_". Mais quel air de famille ? Ça, il ne nous le dit pas. S'il faut que j'explique ce qu'il y a dedans, comment je fais ? Ça ne va pas forcément être facile, mais au moins, ils sont faits)

kk
## Inclassées

   - Quelles autres compétences ont pris/n'ont pas pris la plupart des intercommunalités, alors que moi, je ne les ai pas/ou je les ai ?

![les domaines du Machine Learning](../images/domaines_du_machine_learning.png)
