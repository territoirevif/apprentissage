---
title: "s510. Les classifications"
subtitle: ""
author: Marc Le Bihan
---

Les classifications permettent de prédire des valeurs de variables qualitatives.

   - Prédiction de maladies
   - Prédiction d'achats

\bigbreak

   [ACP](./s350-ACP-ses_objectifs.pdf)  
   [k-Means](./s530-k-Means-classification.pdf)

## 1) Recommandation

En observant :

   - les préférences de personnes  
     à travers des notes qu'elles attribuent, par exemple,

   - ou celles implicites  
     à travers de leurs comportements  

l'on étudie les similarités entre objets ou utilisateurs.

\bigbreak

Et l'on s'en sert pour faire des recommandations  
   \demitab d'autres choses qui pourraient leur plaire. 

   - Recommandation de produits ou de films.

## 2) Support Vector Machine (SVM)

