---
title: "s600. La fouille de données"
subtitle: ""
author: Marc Le Bihan
category: statistiques

keywords:
- fouille de données
- data mining
- pattern
- motif
- classe
- ensemble d'individus
---

Il s'agit de créer des __classes__ pour analyser ces données.  
Ces méthodes nécessitent beaucoup d'interprétations manuelles.

# I) Pattern mining

Un __pattern__ (motif) peut être identifié dans une série :

\demitab $\rightarrow$ l'on cherche alors une série de __motifs__ qui reviennent fréquemment,  
\demitab et les __règles__ qui relient les attributs entre-eux.

Car un ensemble de patterns peut être représentatif d'un ensemble d'individus formant une classe.
