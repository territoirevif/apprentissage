---
title: "s052. Distribution"
subtitle: ""
author: Marc Le Bihan
keywords:
- distribution
- valeurs distintes
- comptage
- fréquence
- rangement en ordre
- trier
- ordonner
- tri sur variable
- clef composée
- paramètre de position d'une distribution
---

## 1) Distribuer, c'est ranger

Une \definition{distribution} existe quand on :  

   - __compte les valeurs distinctes__ qui sont dans une série __ou leur fréquence__
   - et qu'on les __range en ordre__. 

\bigbreak

\avantage{Distribuer = Ranger}

\bigbreak

| Valeur   | A  | B  | C
| ---      | -- | -- | --
| Effectif | 7  | 3  | 2

série de données $\rightarrow$ valeurs aggrégées avec un effectif $\rightarrow$ puis rangées par ordre : une distribution

## 2) On range par rapport à une variable

\danger{une distribution, c'est par rapport à quelque-chose : par rapport à une variable}.  

\bigbreak

   \begingroup \cadreAvantage

   > Pour un ensemble de personnes qu'on observe : 
   >
   > - la répartition en âges va donner une certaine distribution : beaucoup de gens entre 0 et 80 ans, mais pas beaucoup après  
   > - la région où il vivent (si l'on en fait des modalités) sera une autre distribution  
   > - qui pend le bus, la voiture, le vélo... pour aller au travail, une autre encore.

   \endgroup

\bigbreak

\avantage{Dans la pratique on met toujours une série sous la forme d'une distribution :}

   > - pour ne pas être noyé sous des milliers de valeurs en désordre

   > - et parce que certaines distributions sont typiques, et permettent de dire des choses de nos données, de faire des détections.

\bigbreak

Alors, quand  une série est composée de :

   - de valeurs "primitives" (entier, réel, modalité)  
     $\{3,8,1,5,3,1,2,2,1,2,3\}$ par exemple,  
     on la regroupera et on la triera : $\{(1,3), (2,3), (3,2), (5,1), (8,1)\}$.
   
   - d'entités / éléments, c'est à dire d'objets avec plusieurs champs dedans :
   
      - un seul de ces champs servira de regroupement
      - ou plusieurs, en clef composée (qu'on gagnera à remplacer par une valeur les résumant)

---

Un \definition{paramètre de position} donne la position d'une distribution.

