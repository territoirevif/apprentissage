---
title: "s054. Effectifs ou fréquences cumulées" 
subtitle: ""
category: statistiques
author: Marc Le Bihan
keywords:
- effectif
- fréquence
- fréquence cumulée
- regroupement
- agréger
- valeur distincte
- effectif cumulé à gauche
- effectif cumulé à droite
- fréquence cumulée à gauche
- fréquence cumulée à droite
- python
- r
---

[Python : Calculer l'effectif et les fréquences cumulées](#effectif-frequences-cumulees-python)  
[R : Calculer l'effectif et les fréquences cumulées](#effectif-frequences-cumulees-r)

---

Les fréquences ont un sens quand on regarde le nombre d'individus qui ont une valeur particulière.
 
   > elles impliquent qu'on ait fait un regroupement avant, pour avoir les valeurs distinctes de la variable qui nous intéresse.

\bigbreak

   \begingroup \cadreAvantage

   > Exemple : "\bleu{\emph{Combien de lettres recevez-vous par semaine ?}}"
   >
   > - $\mathbf{n = 10}$ personnes interrogées : $\{2,5,3,3,4,3,4,2,3,4\}$ : $i$ variera de 1 à 10.   
   > - Regroupement par nombre de lettres reçues : $\{2, 3, 4, 5\}$
   >  
   >   \ \ \ \ $j$ (index des valeurs distinctes) variera de 1 à 4.  
   > \   
   >
   > \bleu{$n_{j}$} : effectif de la valeur distincte d'index \bleu{$j$} que peut prendre la variable dans la série
   >
   > \ \ \ \ $n_{1}$, c'est à dire $n_{"\emph{\text{nombre de lettres reçues = 2}}"} = 2$ [personnes ont reçu 2 lettres]  
   > \ \ \ \ $n_{2}$, c'est à dire $n_{"\emph{\text{nombre de lettres reçues = 3}}"} = 4$ [personnes ont reçu 3 lettres]  
   > \ \ \ \ $n_{3}$, c'est à dire $n_{"\emph{\text{nombre de lettres reçues = 4}}"} = 3$ [personnes ont reçu 4 lettres]  
   > \ \ \ \ $n_{4}$, c'est à dire $n_{"\emph{\text{nombre de lettres reçues = 5}}"} = 1$ [personnes ont reçu 5 lettres]

   \endgroup

# I) Les effectifs et les fréquences cumulées

## 1) Effectifs cumulés

\bleu{$N_{j}$} : effectif __cumulé__ croissant (avec une flèche vers le bas : décroissant)  
\bleu{$N(x)$} : effectif __cumulé à gauche__ : nombre d'observations __inférieures ou égales__ à une valeur  
\bleu{$N^*(x)$} : effectif __cumulé à droite__ : nombre d'observations __supérieures ou égales__

## 2) Fréquences cumulées

\bleu{$f_{i}$} : fréquence : définie d'après l'effectif total 
$$ \rouge{f_{i}} = \frac{n_{i}}{n} $$

\bleu{$F_{i}$} : fréquence __cumulée__ croissante

\bigbreak

### Calculer l'effectif et les fréquences cumulées (Python){#effectif-frequences-cumulees-python}

   \code
 
   > ```python
   > if __name__ == '__main__':
   >     emploi_par_activite = [21143585, 35197834, 69941779, 64298386, 368931820]
   > 
   >     # Effectif cumulé
   >     effectif_cumule = np.cumsum(emploi_par_activite)
   >     print("effectif cumulé : ", effectif_cumule)
   >
   >     # [-1] sur un tableau donne son dernier élément
   >     print("effectif total  : ", effectif_cumule[-1])  
   > 
   >     # Fréquences cumulées
   >     frequences_cumulees = effectif_cumule / effectif_cumule[-1]
   >     
   >     # [-1] sur un tableau donne son dernier élément   
   >     print("fréquences cumulées  : ", frequences_cumulees)
   > ```
  
\bigbreak
 
   Résultat :
    
   > ```txt
   > effectif cumulé :  [ 21143585  56341419 126283198 190581584 559513404]
   > effectif total  :  559513404
   > fréquences cumulées  :  [0.03778924 0.10069717 0.22570183 0.34062023 1]
   > ```

   \endgroup

### Calculer l'effectif et les fréquences cumulées (R){#effectif-frequences-cumulees-r}

   \code
   
   > ```r
   > # Chargement et affectation des données brutes
   > data(Personnes_Foyer)
   > xi <- Personnes_Foyer$xi
   > ni <- Personnes_Foyer$ni
   > 
   > weighted.mean(xi,ni) # répond : 2.191815
   > cumsum(ni/sum(ni))   # répond : 0.3637253 0.6910106 0.8271677 0.9422720 0.9840092 1.0
   > print(cumsum(ni/sum(ni)),digits=2) # répond : 0.36 0.69 0.83 0.94 0.98 1.00
   > ```
   
   \endgroup

