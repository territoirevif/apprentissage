---
title: "s055. Les représentations graphiques des effectifs ou fréquences" 
subtitle: ""
category: statistiques
author: Marc Le Bihan
keywords:
- effectif
- fréquence
- fréquence cumulée
- regroupement
- agréger
- tri
- valeur distincte
- python
- représentation graphique
- diagramme en bâtons
- camembert
- diagramme en secteurs
- histogramme
- courbe des fréquences cumulées
---

# I) Un prérequis : regrouper et trier les données

Le regroupement et le tri sont nécessaires pour :

  - lire 
  - rechercher dans des données
  - mais aussi les représenter graphiquement

\bigbreak

tant que l'on a pas trié des valeurs, il n'est pas possible/facile de dire :  
"\bleu{\emph{60\% d'entre-elles sont inférieures à 18}}" !

## a) Le placement des valeurs sur les axes, suivant la nature de la variable

Sur l'axe des $\bleu{x}$ on placera, en général :

\tab Pour les variables __quantitatives__ ou __ordinales__ : 

  \tab \tab les __valeurs__ triées __de la plus petite__ à __la plus grande__ 

\bigbreak

\tab Pour les variables qualitatives :

  \tab \tab les __valeurs__ triées __par leur effectif__, du __plus grand__ au __plus petit__

# II) Les représentations graphiques des effectifs et des fréquences

## 1) La courbe des fréquences cumulées

   - Courbe des fréquences cumulées (cette fréquence cumulée est souvent notée \bleu{$F$} majuscule).  

## 2) Diagrammes

### a) Diagramme en bâton des effectifs ou des fréquences

   - Diagramme en bâtons (valeurs discrètes) 

### b) Histogramme des fréquences

   - Histogrammes des fréquences, où la surface est égale à la fréquence de la classe
   - ou histogramme (d'après des classes, pour les valeurs continues)  

### c) Camembert

\avantage{camemberts (secteurs)}  

### d) le diagrame feuille-branche

Un diagramme feuille-branche est un histogramme de valeurs qu'on a regroupées par un critère, et qui les présente toutes les valeurs (il ne les résume pas).  

   > \underline{exemple :} si l'on a des valeurs comprises entre 71 et 142, on aura un histogramme avec des blocs : 7,8,9,10,11,12,13,14, et au dessus les unités de ces nombres : 1 pour 71, 2 pour 142.

   - Il faut trier les valeurs avant !

   - \avantage{Intérêt : on peut créer un histogramme à partir d'une liste de valeurs, pour les présenter comme une distribution} (cf. ci-dessous).

## 4) Polygone des effectifs


