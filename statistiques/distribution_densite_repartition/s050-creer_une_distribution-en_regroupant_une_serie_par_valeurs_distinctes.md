---
title: "s050. Créer une distribution en regroupant une série par valeurs distinctes"
subtitle: "compter et ordonner des valeurs distinctes, crée une distribution"
category: statistiques
author: Marc Le Bihan
keywords:
- donnée
- analyse descriptive
- tableau individu x caractères
- effectif
- regroupement par valeurs distinctes
- ordonner
- trier
- distribution
- agréger
- GROUP BY
- ORDER BY
---

Faire connaissance avec les données, 

   - en les rangeant, 
   - en les \avantage{représentant graphiquement} 

pour voir comment elles se situent les unes par rapport aux autres.

\bigbreak

C'est là que débute \definition{l'analyse descriptive} :

   - l'on va faire des \avantage{tableaux}
   - des représentations graphiques, 
   - dans une représentation graphique, si la série est ordonnée, suivre cet ordre sur l'axe des $x$.
   - tableau individus $\times$ caractères.
   - donner des \definition{indicateurs de tendance centrale}...

## 1) Les effectifs et les regroupements par valeur

   \begingroup \cadreAvantage

   > __Regrouper__ les valeurs d'une série (qui sont nombreuses et en désordre)
   >
   >   - __en valeurs distinctes__,
   >   - et les __ordonner__,
   > \bigbreak
   >
   > ce sera en faire une __distribution__.  
   > Et alors, on s'appercevra que celles-ci suivent une __Loi__...

   \endgroup

\bigbreak

Lorsque l'on étudie une donnée particulière d'un individu, on va l'avoir en vrac, en désordre :

   > $\mathbf{\{A,A,A,C,B,A,B,A,A,C,A,B\}}$\ \ \ \ \bleu{$n$}, l'\definition{effectif} de la série vaut __12__.

\bigbreak

\attention{Cette série n'est pas facile à lire du tout !}  
Combien de __A__, de __B__, de __C__ dedans ?

## 2) Agréger, pour obtenir des valeurs distinctes

Nous allons __agréger__ (rassembler) ces valeurs pour pouvoir mettre un effectif à côté, à la place de longues répétition des mêmes données.

   > \avantage{$\mathbf{\{(A,7), (B,3), (C,2)\}}$} est plus facile à lire.  
   > $\rightarrow$ nous avons toujours $7 + 3 + 2 = 12$ individus, mais l'on sait qu'on a 7 __A__, 3 __B__, 2 __C__.

\bigbreak

On continue à parcourir les objets (ou individus) de __1__ à __12__, avec la variable d'index $\bleu{i}$,

mais les valeurs \textbf{\underline{distinctes}} qu'on peut observer, et qu'on a regroupées, elles on les les parcoure avec un autre index. De modalité/caractère/regroupement : $\bleu{j}$ ou $\bleu{k}$.

C'est à dire que cet index __parcoure les 3 valeurs distinctes : A, B, C__

\bigbreak

\underline{Informatique :}

   > De là vient la nécessité dans les notebook (`Zeppelin`, `Jupyter`...) et programmes, d'appliquer un \fonction{GROUP BY} sur la variable d'étude des individus, pour commencer une analyse.
   >
   > Cette clause, associée à un \fonction{ORDER BY} fera du résultat une distribution (cf. ci-dessous).

