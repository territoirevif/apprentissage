---
title: "s056. La fonction de densité et la fonction de répartition"
subtitle: "(celle de densité est la dérivée de celle de répartition)"
author: Marc Le Bihan
keywords:
- fonction de densité f
- fonction de probabilité
- fonction de répartition F
- distribution
- proportion de valeurs
- modélisation
- quantile
---

## 1) La fonction de répartition F

Notre distribution a des valeurs distinctes (ou des classes) triées, et l'on peut visuellement compter :

   \demitab Le nombre de valeurs dans notre série qui sont inférieures à une valeur $x$  
   \demitab (et par là \rouge{$p$}, leur proportion, qui dans une distribution de probabilité sera une probabilité)

### a) F(x) la proportion

Cette proportion est donnée par :

   > \bleu{$\mathbf{F(x)}$}, à interpréter $\mathbf{\bleu{F(X \leq x)}}$ : la \definition{fonction de répartition}.  
   > "\emph{les individus dont la valeur de leur variable aléatoire \bleu{$X$} est inférieure ou égale à \bleu{$x$}}"  
      
\bigbreak

toute distribution en a une, même si parfois on ne la connaît pas  
(et le but alors, c'est de modéliser pour la trouver)

\bigbreak
 
\begingroup \large \avantage{distribution = fonction de répartition : c'est elle qui la définit} \endgroup

### b) Quantile, son inverse

   - le \definition{quantile}, c'est la __fonction inverse__ de la répartition : 
     $$ \rouge{Q_{p}} = F^{-1}(p) $$

## 2) f(x), sa dérivée, est la fonction de densité

La fonction de répartition $\bleu{F(x)}$ travaille sur une aire  
("_Un ensemble de valeurs qui sont inférieures à une valeur_")

$\rightarrow$ \avantage{elle est donc dérivable}

\bigbreak

C'est elle qui est souvent présentée et qui donne ces belles courbes en cloche, par exemple.

   > Mais souvent, on ne la connaît pas...  
   > Qu'importe ! Il suffit de dériver sa fonction de répartition pour l'obtenir.

\bigbreak

$f(x)$ donne une probabilité pour une valeur précise :  
$\bleu{\mathbf{f(x)}}$ c'est $\bleu{\mathbf{F(X = x)}}$. C'est la même chose que $P(X = x)$.

C'est pour cela qu'elle est aussi appelée \definition{fonction de probabilité}.

\bigbreak

   > Représentée, sa \avantage{courbe de répartition} sera
   >
   >   - en escalier, si $x$ est réel, 
   >   - un diagramme en bâton, s'il est entier. 

\bigbreak

que la fonction de densité soit une dérivée s'explique :

À chaque valeur qui arrive, on peut se poser la question :  
"\emph{Est-ce qu'il y en a de plus en plus de ces valeurs qui arrivent, ou de moins en moins ?}"

## 3) Retrouver des probabilités à partir de ces fonctions

### a) Une probabilité dans un intervalle, ou inférieure à une valeur

\bleu{$P(X \in [a,b])$}
   
   - sa surface est sa \avantage{fréquence}, c'est à dire : sa probabilité.
   - $P(x \leq X \leq x + dx) = f(x).dx$, bien sûr : $\sum f(x).dx = 1$ 

### b) Une probabilité que la valeur cherchée soit inférieure ou égale à une autre

\bleu{$P(X \le x)$}, d'après une fonction de \definition{répartition} \bleu{$F(X)$} : 
   
   - derrière, ce sont des intégrales (on fait des sommes d'aires).
   $$ \rouge{P(x_{1} \leq x \leq x_{2})} = F(x_{2}) - F(x_{1}) $$

