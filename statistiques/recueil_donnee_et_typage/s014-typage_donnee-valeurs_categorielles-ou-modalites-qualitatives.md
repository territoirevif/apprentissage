---
title: "s014. Le typage des données : les valeurs catégorielles ou modalités : qualitatives"
subtitle: "Nominales, ordinales, cardinales, quantitatives, qualitatives"
author: Marc Le Bihan
keywords:
- modalité
- catégorie
- caractère
- énumération
- modalités incompatibles
- modalité exhaustive
- modalité sans ambiguïté
- modalité hiérarchisée
- modalité recodée numériquement
- variable indicatrice
- valeur ordinale
---

Les valeurs \definition{qualitatives} s'appellent aussi des 

   - \definition{modalités}, et ce sur quoi ces modalités portent s'appelle un \definition{caractère}
   - catégories 

ce sont des énumérations.

## 1) Les règles de définition d'une modalité

- il faut que les \definition{modalités} d'un caractère soient :

    - __incompatibles__ : on ne peut appartenir qu'à l'une d'entre-elles
    - __exhaustives__ : prévoir tous les cas (éventuellement avec une modalité "_autres_")
    - __sans ambiguïté__ : pour ne pas faire d'erreur de classement

## 2) Finesse de la définition d'une modalité

La finesse des modalités doit être choisie 

   \begingroup \cadreAvantage

   > Un statut matrimonial peut être :
   >
   > - marié/non marié
   > - ou marié/non marié/veuf/divorcé
   
   \endgroup

## 3) Les modalités hiérarchisées

Exemple : catégories socio-professionnelles INSEE

## 4) Traitement informatique : par une énumération

L'\fonction{enum} est adapté aux variables modales

et pour faciliter le recodage numérique des modalités en variable indicatrice, qui est fréquent, on peut associer à chaque valeur d'enum une valeur numérique.

   > 1=Homme, 2=Femme. 

