---
title: "s010. Le typage des données : les valeurs nominales"
subtitle: ""
author: Marc Le Bihan
keywords:
- valeur nomimale
- identifiant
---

Les \definition{valeurs nominales} servent à nommer des élements pour permettre de les : 

   - repérer, 
   - identifier

\bigbreak

   \begingroup \cadreAvantage

   > quelle culture il y a sur un sol : "_Blé_".

   \endgroup

\bigbreak

On ne peut pas faire grand-chose d'autre avec, sinon.

   - elles ne permettent que les comparaisons d'égalité (égal à ou différent de)
   - Statistiques : mode, diversité
   - Informatique : le type \fonction{String} est le plus utilisé pour ces données.

