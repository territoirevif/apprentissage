---
title: "s016. Le typage des données : les valeurs ordinales"
subtitle: ""
author: Marc Le Bihan
keywords:
- valeur ordinale
- valeur hiérarchique
- critère de tri
- comparaison par une donnée ordinale
---

Les valeurs \definition{ordinales} (ou __hiérarchiques__) trient les données.

## 1) Comment le type de donnée ordinale permet un tri

Ce sont souvent des nombres entiers, qui servent de critère de tri des données. Mais pas toujours.

Voici des exemples de tris que l'on peut faire, selon le type de donnée ordinale que l'on a devant soi :

- __discret__ : Une note sur une copie scolaire va de 0 à 20, et permet de les classer en qualité.  
- __continue__ : la taille d'un humain est un nombre réel, mais il peut être considéré comme ordinal, si on le désire.  
- __qualitatif__ : la fertilité d'un sol peut être jugée :  `Faible`, `Moyenne`, `Elevée`.

## 2) Les comparaisons permises par les données ordinales

les valeurs ordinales permettent les comparaisons d'égalité, supérieur, inférieur.  

   - pour celles numériques, c'est évident  
   - et pour celles qualitatives (= modalités = catégories), je peux définir un ordre et déclarer :  
"_la fertilité `Elevée` est supérieure à `Moyenne`_"

## 3) Les opérations statistiques souvent appliquées aux données ordinales

médiane, inter-quantile

## 4) Représentation informatique

- \fonction{integer}
- \fonction{double} parfois
- \fonction{enum} pour les qualitatives qui sont ordinales

