---
title: "s003. Les méthodes de recueil de la donnée"
subtitle: ""
author: Marc Le Bihan
keywords:
- recueil de la donnée
- observation
- relevé
- capture
- mesure de terrain
- enquête
- recensement
- sondage
- échantillon représentatif
- méthode par quota
- méthode probabiliste
- échantillonage aléatoire simple
- échantillonage aléatoire stratifié
- échantillonage en grappes
- échantillonage systématique
- échantillonage d'un continuum de temps
- échantillonage de surface ou de volume
- conception de questionnaire
- expérimentation
- plan d'expérience
- protocole expérimental
- réplication
- randomisation
- contrôle local de la variabilité
- blocage
- plans en blocs complets
- plans en blocs incomplets
- carrés latins
- carrés gréco-latins
- split-plots
- cross-over
- carry-over
- étude de traces
- moissonnage du web
- données massives
- origine de la donnée
- donnée mesurée
- donnée primaire
- donnée dérivée
- donnée interprétée
---

Comment receuillir de l'information pour se faire son stock de \definition{données} pour un sujet d'étude ?

# I) Les méthodes de recueil

## 1) observation, relevé, capture ou mesure de terrain

## 2) enquête

   - __recensement__ : une enquête complète, exhaustive sur la population totale.

      - Ensemble d'être humains
      - Stocks d'objets
      - Flux : l'ensemble des commandes d'un mois
      - Non concrets : intentions de vote

\bigbreak

   - __sondage__ : une enquête partielle sur un __échantillon représentatif__  
     où il faut déterminer les questions à poser selon l'information dont on a besoin.

     > \attention{Attention, une question aussi peut-être mal posée} : "_\textcolor{blue}{Êtes-vous pour l'écologie ?}_"  
     > Oui, bien sûr, tout le monde est pour ! Mais tout le monde agit-il pour autant de manière accordée ?

\bigbreak
 
   - méthode par __quotas__ : on connaît, par exemple, la proportion d'hommes et de femmes dans la population française, alors on va choisir un échantillon qui respectera ces quotas.

   - méthode __probabiliste__ : au hasard dans une grande base.

\bigbreak

   \danger{il est difficile de savoir si notre échantillon est bon...} il peut y avoir de nombreux biais.  
   Si l'on interroge seulement dix personnes, on sera très imprécis...  
   $\rightarrow$ Plus tard, l'on \avantage{estimera les erreurs}.

   (à classer : échantillonage aléatoire simple, aléaltoire stratifié, en grappes, systématique, d'un continuum de temps, de surface ou de volume).

   (à classer : estimation d'un ratio, par régression, de la taille d'une population ; conception d'un quesitonnaire ; évaluation de l'erreur dans les enquêtes).

## 3) Expérimentation

Elle provoque une observation. On provoque volontairement ce que l'on veut étudier.

Mais il faut la préparer, et un plan d'expérience est recommandé ou un protocole expérimental :

   - afin d'éviter une expériementation trop vaste, qui coûterait très cher.
   - ou trop petite, et qui ne serait pas significative.

Les facteurs que l'on voudra étudier.

\bigbreak

Il faut assurer :

   1. La réplication : qu'on peut reproduire l'expérience avec les mêmes résultats  
   2. La randomisation : modifier aléatoirement les conditions de l'expérience permet une estimation non biaisée de la variabilité résiduelle et des effets estimés  
   3. Le contrôle local de la variabilité ou blocage, pour augmenter la précision de l'expérience

      - plans en blocs complets ou incomplets
      - carrés latins
      - carrés gréco-latins
      - split-plots
      - cross-over
      - carry-over
      - ...

## 4) étude de traces

Typiquement, du passé : documents historiques...

## 5) Le moissonnage du web

Associé à une approche participative ou tout internaute est l'expert dans son milieu.

_OpenStreetMap_ en est un exemple, qui a permis de détecter certains bâtiments aux caractéristiques méconnues.

## 6) L'internet des objets

Dans les villes, véhicules, logements, les objets connectés sont une source de données massives.  
Les statistiques temps réel, par exemple, ont aidé le secteur du tourisme.

# II) L'origine de la donnée

   - __mesurée (ou primaire)__ : par un instrument de mesure ou une observation visuelle
   - __dérivée__ : obtenue par une combinaison d'informations primaires

      > débit d'une rivière ou une pente

   - __interprétée__ : estimée par des connaissances, une expertise, parce qu'elle n'est pas mesurable ; ou bien estimée à partir d'autres élements/évènements autour d'un point, par exemple.

