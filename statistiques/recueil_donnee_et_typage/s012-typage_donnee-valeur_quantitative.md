---
title: "s012. Le typage des données : les valeurs quantitatives"
subtitle: "cardinale, discrète ou continue"
author: Marc Le Bihan
keywords:
- valeur quantitative
- valeur cardinale
- valeur quantitative absolue
- valeur quantitative relative
- taux
- densité
- valeur discrète
- valeur continue
- classe
- modalité
- intervalle
- nombre réel
- amplitude d'une classe
- valeur bouchon d'une classe
- nombre de classes
---

Une valeur \definition{quantitative} est numérique.

# I) Une variable entière (discrète)

On peut y chercher une valeur précise.
  
# II) Une variable réelle (continue)

Enumérer les valeurs distinctes dans une série pour les compter est utile, mais comment faire devant des nombres réels ?

## 1) Il n'y a pas de valeurs distinctes avec les nombres réels !
 
Un $1.85$ cotoiera un $1.85125$ ou $1.84951$ : impossible que cela reste stable.   
\attention{On doit toujours considérer une valeur réelle dans un intervalle}, car on ne peut pas compter les valeurs qui valent $1.8264$, puis celles qui valent $1.82642$, puis $1.8265$...

   - quand on dit : "\bleu{\emph{Une personne mesure 1,82m}}", elle mesure en fait entre $[1,815; 1,825[$...
   - et l'âge qu'on a, à un instant, ce n'est pas $43$ ans, mais $43.841$ (l change à chaque instant !)

\bigbreak

On ne peut traiter ces valeurs qu'après les avoir mises en classes.

Donc, une variable continue (réelle) réclame de créer des \definition{classes} (ou modalités) pour pouvoir être distribuée : sinon, on ne peut pas la regrouper par quelque-chose.  

## 2) Comment faire une bonne classe ?

On peut souhaiter décrire le nombre d'enfants dans une famille ainsi :

   - 0 frères et soeurs, 
   - 1 ou 2 frères et soeurs
   - 3 ou plus : un peu délicate, c'est un __bouchon__. Que signifie t-elle ? Quel est son maximum ?

\bigbreak

Chaque rubrique devient une modalité.  

   - C'est plus facile si les classes ont des \definition{amplitudes} constantes... mais pas toujours avisé !

      \begingroup \cadreAvantage

      > Une étude de taille des humains n'a pas grand-chose à dire sur les $[1.30m, 1.60m]$ (amplitude: 0.3m) qui sont rares,  
      > ...mais plus sur les $[1.60m, 1.80m]$ (amplitude : 0.2m) très fréquents !

      \endgroup

\bigbreak

   - l'amplitude \attention{ne doit pas être trop grande}, sinon l'information sera imprécise
   - l'on peut avoir des \avantage{classes bouchon}/voiture balai : "_plus de 1.80m_", par exemple)  

\bigbreak

La convention, c'est que l'intervalle est fermé à gauche et ouvert à droite : $\mathbf{[130, 160[}$

## 3) Combien de classes ? 

   - \textcolor{blue}{$k$} classes, avec $2^{\textcolor{blue}{\mathbf{k}}} < n$ est une possibilité.  
   - $\textcolor{blue}{k} = \sqrt{n}$ aussi, pour un échantillon.  
   - choisir un nombre impair de classes est pratique, si l'on veut une classe centrale dans notre répartition.

## 4) Les mesures, indicateurs associés à une classe

Avec \bleu{$i$} l'indice qui parcoure les classes que l'on crée, et :

   > \bleu{$a_{i}$} : l'amplitude de la classe $i$  
   > \   
   > \mauve{\begingroup \large $\frac{f_{i}}{a_{i}}$ \endgroup} : la densité de la classe $i$  
   > \   
   > \bleu{$c_{i}$} : le centre de la classe  
   > \demitab \ remarque : on retrouve la moyenne $\bar{x}$ en faisant : $\bar{x} = \sum(f_{i}.c_{i})$   

# III) La valeur d'une variable quantitative

## 1) absolue, relative, taux...

Une variable quantitative peut porter une valeur :

   - __absolue__ (une quantité d'argile dans un sol, une distance ou température...)  
     On dit qu'elle est __cardinale__ si elle compte quelque-chose

   - __relative__ 
   - un __taux__, une __densité__, exprimé au moyen d'un rapport.

## 2) Les statistiques que l'on peut faire sur une variable quantitative

   - Toutes les opérations de comparaisons ou arithmétiques sont permises. 
   - Statistiques : moyenne, variance écart-type, corrélation, etc.

## 3) Représentation informatique

   - le type \fonction{Integer} ou \fonction{Long} (`int`, `long`) convient pour les variables discrètes (entières)
   - et le \fonction{double}, \fonction{float} ou \fonction{BigDecimal} pour les valeurs continues (réelles)

