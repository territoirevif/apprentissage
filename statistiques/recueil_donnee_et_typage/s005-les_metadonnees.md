---
title: "s005. Les métadonnées"
subtitle: ""
author: Marc Le Bihan
keywords:
- métadonnée
---

Les \definition{métadonnées} expliquent d'où vient chaque entité ou information  
et sont définies par une norme ISO.

\bigbreak

![Les métadonnées ISO](../analyse_spatiale/analyse_spatiale/metadonnees_iso.jpg)

_Les metadonnées ISO_

