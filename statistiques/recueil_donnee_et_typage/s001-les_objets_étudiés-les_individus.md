---
title: "s001. Les objets d'étude (individus)"
subtitle: ""
author: Marc Le Bihan
abstract: On observe ce qu'on appelle un objet, unité d'étude ou individu. Une série de ces objets.
keywords:
- individu
- variable
- variable aléatoire
- donnée
- donnée fausse
- donnée mal recueillie
- donnée mal interprétée
- série (non triée) d'individus
---

Les \definition{données} acquises sur un sujet d'étude vont faire des \definition{variables}.  
pas forcément aléatoires : une variable ne l'est que s'il est impossible de prévoir la valeur qu'elle aura.

# I) Notre sujet d'étude : des \definition{objets} qu'on observe

## 1) On étudie un individu

L'unité d'étude, c'est un \definition{objet}. On va l'appeler :

- objet, en informatique,
- élément, en mathématiques
- entité
- unité d'observation, en géographie
- \definition{individu}, en statistiques
- l'observation, du résultat d'une expérience

\bigbreak

On peut étudier des personnes, des oranges, des voitures...

## 2) En observant leurs attributs

   - parfois réduites à leur âge seulement, pour une personne

       > on a 5 individus, par exemple, ayant 8, 41, 54, 28 et 25 ans.

\bigbreak

   - les observant au travers de leur âge, taille, ville et leur état de santé général __ensemble__

      `{8, 1.05, Rennes, BONNE_SANTE}`, // 8 ans, 1m05, vivant à Rennes et en bonne santé  
      `{41, 1.75, Orléans, MALADE}`, `{54, 1.82, Fougères, MEDIOCRE}`...

## 3) Mais attention aux données que l'on prend !

Il faut faire attention, car ces données peuvent être :

   - fausses

   > On demande à des personnes de dire leur alimentation quotidienne $\rightarrow$
   >
   >   - ça les ennuie : elles ne notent pas tout.
   >   - elles sous estiment les quantités qu'elles mangent.
   > 
   > $\implies$ une manière de résorber le problème : demander un nombre de portions dans douze catégories

   - mal receuillies
   - et par la suite... mal interprétées

\bigbreak

Vérifer qu'on est capable d'identifier la donnée... en \underline{mettant un commentaire au dessus !}

### \underline{Conséquences en informatique}

Un objet d'étude (ou individu) sera souvent un objet (_Java_, _Python_, _C++_...) :

   - et nos données d'étude seront les variables membres qu'on aura définies dans sa `class`.

      \code

      > ```java
      > /**
      >  * une personne (notre individu statistique)
      >  */
      > public class Personne {
      >    /** Age */
      >    private int age;
      >      
      >    /** Taille */
      >    private double taille;
      >      
      >    /** Ville */
      >    private String ville;
      >      
      >    /** Etat de sante general */
      >    private EtatSante etatSante;
      > }
      > ```

   \endgroup

\bigbreak

   - parfois, quand on aura qu'une seule donnée à étudier : un nombre, par exemple, on ne fera même pas de `class`. 

# II) L'ensemble des objets (ou individus) étudiés : \definition{la série}

L'ensemble de données qu'on rassasse pour chaque objet ou individu s'appelle une \definition{série} :

   - \danger{elle n'est pas triée}

   - chaque élément est de même nature : si on étudie des oranges, on a une série [ensemble] d'oranges.

   - En informatique, __la liste d'éléments typés__ est idéale pour représenter une série :  
\fonction{List<E>} où __E__ est l'individu, qui sera toujours de même structure par la suite.

   \code

   > ```java
   > List<Orange> serieOranges = new ArrayList<>();
   > serieOranges.add(new Orange(0.05, MURE, 0.30));
   > serieOranges.add(new Orange(0.04, MURE, 0.30));
   > serieOranges.add(new Orange(0.06, PEU_MURE, 0.25));
   > ...
   > ```

   \endgroup

