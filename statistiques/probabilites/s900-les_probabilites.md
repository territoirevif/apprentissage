---
title: "s900 - Les probabilités"
subtitle: ""
author: Marc Le Bihan
---

\avantage{Une probabilité est l'idéalisation d'une fréquence}

   \tab si l'on répétait une expérience un grand nombre de fois.

   \begingroup \cadreAvantage

   > Les chances que l'ascenceur soit au rez-de-chaussée quand je rentre,  
   > et je rentre dans mon immeuble des milliers de fois.

   \endgroup

## 1) Les évènements

### a) Portent sur les caractéristiques d'un individu

Les \definition{évènements} ($\bleu{E_{1}}, \bleu{E_{2}}...$) portent sur

   \demitab une ou plusieurs \definition{caractéristiques}  
   \demitab d'un \definition{individu} : 

   - Un âge (en années),  
     taille (en centimètres),  
     prix de revient (en euros),  
     espèce d'arbre (_Hêtre_, _Chêne_...),  
     utilisation du vélo ou pas (booléen)... 

\bigbreak

Un évènement $\textcolor{blue}{E_{1}}$ peut être un _fait_  
(exemple : un individu a une caractéristique)

   \demitab $\textcolor{blue}{E_{2}}$ être un autre fait,  
   \demitab $\textcolor{OliveGreen}{E_{3} = E_{1} \cap E_{2}}$ leur intersection,  
   \demitab $E_{4} = \overline{E_{1} \cup E_{2}}$...

### b) Ils sont issus d'expériences
    
Les évènements sont attendus lors  d'\definition{expériences}   
et leur __probabilité__ suit une \definition{Loi}

## 2) Les variables aléatoires

Les variables aléatoires permettent d'étudier les caractéristiques d'un \textcolor{blue}{\bf{individu}}.  
   \demitab \gris{(pour qu'elle puisse suivre une loi}   
   \demitab \gris{la somme des probabilités d'avoir chacune de ses valeurs doit valoir $\mathbf{1}$)}

\bigbreak
 
Pour éviter les lourdes formules d'évènements :  
   \demitab $(A \cap B) \cup (C \cap \overline{D})$

on transforme les probabilités en \definition{variables aléatoires}.

