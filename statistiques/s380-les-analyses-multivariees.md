---
title: "s380. Les analyses multivariées"
subtitle: "L'Analyse en Composantes Multiples (ACM)"
author: Marc Le Bihan
---

L'Analyse des Correspondances Multiples (ACM)  
   \demitab traite de tableaux  
   \demitab croisant des individus et des variables __qualitatives__

\bigbreak

quand l'Analyse en Composantes Principales (ACP)  
   \demitab traite ceux des individus et des variables __quantitatives__,   

et l'Analyse Factorielle des Correspondances (AFC)  
   \demitab des tableaux de fréquences.




