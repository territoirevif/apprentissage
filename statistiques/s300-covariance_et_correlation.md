---
title: "s300. Covariance et corrélation"
subtitle: ""
author: Marc Le Bihan
category: statistiques
keywords:
- covariance
- matrice de variance-covariance
- corrélation
- Pearson
- Bravais-Pearson
---

## 1) La covariance : dépendante de l'unité, elle monte très haut

La \definition{covariance} adapte la formule de la variance  
   \demitab qui "_utilise deux fois la différence entre une même variable $x$ et la moyenne_"

   \tab $s^2 = E(V) = \sum_{i=0}^n\limits\frac{(x_{i}-m)^2}{n-1}$

\bigbreak

pour la \bleu{faire porter sur deux variables $x$ et $y$} :
$$ \rouge{s_{xy}} = \rouge{cov_{xy}} = \sum_{i=0}^n\limits\frac{(\textcolor{blue}{x_{i}}-m_{\textcolor{blue}{x}})(\textcolor{OliveGreen}{y_{i}}-m_{\textcolor{OliveGreen}{y}})}{n-1} $$

Le nombre obtenu donne le sens de la relation (positive, négative, ou nul : pas de relation)  

### a) Dépendante des unités

La covariance est dépendante des unités :

   - des $x$ et $y$ en mètres entre $0$ et $1$ donneront une covariance de $-2$, par exemple,
   - mais les mêmes en centimètres : $-20000$ !

\bigbreak

\demitab Pour l'utiliser, il faut être sûr que toutes les variables sont dans la même unité,  
\demitab ...et même ainsi, ce n'est pas commode.

### b) La matrice de variance-covariance

$$ \rouge{V} = \begin{pmatrix}
	s^{2}_{x} & s_{xy} \\
	s_{xy} & s^{2}_{y}
\end{pmatrix} $$

## 2) La corrélation : comprise entre -1 et 1. Bravais-Pearson

Il faut une mesure plus stable : la \definition{corrélation}  
   \demitab qui divise la covariance par le produit des écarts-types  
   \demitab des deux variables $\bleu{x}$ et $\bleu{y}$

\bigbreak

C'est le \definition{coefficient de corrélation de Bravais-Pearson (ou Pearson)} ou de \definition{Pearson}
  
   \demitab défini par la lettre $\bleu{r}$ pour les échantillons ou $\bleu{\rho}$ pour les populations  
   \demitab et qui a la même formule, dans les deux cas :

\begingroup \cadreAvantage \large

   > $$ \rouge{r_{xy}} = \rouge{\rho_{xy}} = \frac{cov_{xy}}{s_{x} . s_{y}} = \frac{s_{xy}}{s_{x} . s_{y}} $$

\endgroup

\bigbreak

ou bien, si l'on considère les __valeurs centrées réduites__ de chaque $\bleu{x_{i}}$ et $\bleu{y_{i}}$ :

   - \begingroup \Large $\textcolor{blue}{u_{i}} = \frac{x_{i} - m_{\overline{x}}}{s_{x}}$ \endgroup

   - \begingroup \Large $\textcolor{blue}{v_{i}} = \frac{y_{i} - m_{\overline{y}}}{s_{y}}$ \endgroup

   \begingroup \large $$ \rouge{r} = \frac{1}{n} \sum_{i=1}^{n}u_{i}v_{i} $$ \endgroup

\bigbreak

   \tab $\mathbf{1}$\ \ \ dira une relation positive parfaite  
   \tab $\mathbf{-1}$ une négative parfaite  
   \tab $\mathbf{0}$\ \ \ une absence de relation (ou cas tordu : une relation avec une composante monotone nulle)

\bigbreak

Ce $\bleu{r}$ traduit des situations entre variables $\bleu{x}$ et $\bleu{y}$ (ici $y$ en ordonnée et $x$ en abscisse) :

   > ![les relations types](images/relation-types-remarquees-par-les-correlations.jpg)
   >
   > > __a__ : relation linéaire positive ($r > 0$)  
   > > __b__ : relation linéaire négative ($r < 0$)  
   > > __c__ : relation monotone, positive, non linéaire  
   > > __d__ : relation positive, non linéaire  
   > > __e__ : relation globalement ni croissante, ni décroissante  
   > > __f__ : absence de relation

\bigbreak

\underline{Matrice de corrélation :} \nopagebreak
$$ \rouge{R} = \begin{pmatrix}
	1 & r \\
	r & 1
\end{pmatrix} $$

\underline{Exemple :} calcul du coefficient de correlation de Bravais-Pearson (compris entre $-1$ et $1$)

Entre les variables : 

   - _population d'une intercommunalité_
   -  et _le nombre de compétences qu'elle exerce_
 
\bigbreak

   \code

   > ```java
   > // (avec Spark)
   > double correlation = intercommunalites().stat()
   >    .corr("populationGroupement", "nombreDeCompetencesExercees", "pearson");
   > ```

   \endgroup

\bigbreak

   résultat : $\mathbf{0.3}$

