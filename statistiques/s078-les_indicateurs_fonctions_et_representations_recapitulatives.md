---
title: "s078. Les indicateurs de tendance centrale et distribution"
subtitle: "Fonctions récapitulatives et représentations"
author: Marc Le Bihan
keywords:
- indicateur de tendance centrale
- indicateur de distribution
- peigne
- quartile
- valeur extrême
- variance population totale
- variance sur échantillon
- écart-type sur population totale
- écart-type sur échantillon
- indicateur de position
- indicateur de dispersion
- summary
---

## 1) Représentation textuelle : le Peigne

| Paramètres |     $x_{\frac{1}{2}}$                   | Dispersion                           | Position
| ---        | ---                                     | ---                                  | ---
| Quartiles  | $x_{\frac{1}{4}}$     $x_{\frac{3}{4}}$ |  $x_{\frac{3}{4}} - x_{\frac{1}{4}}$ | $\frac{x_{\frac{3}{4}} + x_{\frac{1}{4}}}{2}$  
| Extrêmes   | $x_{(1)}$   $x_{(n)}$                   | $x_{(n)} - x_{(1)}$                  | $\frac{x_{(1)} + x_{(n)}}{2}$

## 2) Programmation : fonctions récapitulatives de statistique univariée{#recap}

\underline{Java : Récupération des valeurs des quantiles et d'autres valeurs de statistiques univarée}

Les fonctions qui suivent sont présentes dans `org.apache.spark.sql.functions.*` :

   > \methode{var\_pop}{(champ)} \demitab Variance sur la population totale  
   > \methode{var\_samp}{(champ)} \demitab Variance sur échantillon  
   > \methode{stddev\_pop}{(champ)} \demitab Ecart-type sur la population totale  
   > \methode{stddev\_samp}{(champ)} \demitab Ecart-type sur l'échantillon

\bigbreak

Sur un dataset `ds` :

   > \methode{describe}{(colonne)}  
   > \methode{summary}{()} \demitab 

\bigbreak

\underline{R : sur une liste de valeur \texttt{HospitFull} :}

   \code

   > ```r
   > summary(HospitFull)
   > ```
   >
   > \bigbreak \footnotesize
   >
   > ```
   >    Région            Tous.âges            X0.9            X10.19           X20.29        
   >  Length:19          Min.   :   12.0   Min.   : 0.000   Min.   : 0.000   Min.   :  0.00 
   >  Class :character   1st Qu.:   80.5   1st Qu.: 0.000   1st Qu.: 0.000   1st Qu.:  4.00
   >  Mode  :character   Median : 1361.0   Median : 1.000   Median : 3.000   Median : 10.00
   >                     Mean   : 2946.8   Mean   : 4.211   Mean   : 8.105   Mean   : 24.11
   >                     3rd Qu.: 2739.5   3rd Qu.: 2.500   3rd Qu.: 7.000   3rd Qu.: 17.50
   >                     Max.   :27995.0   Max.   :40.000   Max.   :77.000   Max.   :229.00
   > ```

   \endgroup

