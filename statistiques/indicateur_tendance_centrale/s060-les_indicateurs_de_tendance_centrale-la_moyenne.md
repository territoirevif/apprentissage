---
title: "s060. La moyenne et l'espérance"
subtitle: "les indicateurs de tendance centrale"
author: Marc Le Bihan
keywords:
- moyenne
- espérance
- centre de la Loi
- moment d'ordre
- spark
---

[Spark : calcul de la moyenne sur un Dataset (groupé)](#moyenne-spark)  

---

On l'appelle la moyenne ou espérance le \definition{moment d'ordre 1}.

## 1) Le calcul d'une moyenne

### a) Les propriétés de l'Espérance

L'espérance, c'est le centre d'une Loi.  
Le moment d'ordre 1.

   > $E(b) = b$  
   > $E(aX) = aE(X)$  
   > $E(X + b) = E(X) + b$    
   > 
   > $E(X - \mu) = 0$ ce qui __centre__ la variable.  

### b) Cas particulier : on connaît la fonction de densité d'une distribution

   > | $\mathbf{x}$     | __1__ | __2__ | __3__ | __4__ | __5__ | __6__ |
   > | :--------------: | :---: | :---: | :---: | :---: | :---: | :---: |
   > | $\mathbf{P_{x}}$ |  0.1  |  0.4  |  0.2  |  0.1  |  0.1  |  0.1  |
   > 
   > Résolution mathématique:
   > $$ E(X) = \sum_{i=1}^{6}P_{x} \times x $$
   > $$ E(X) = 1 \times 0.1 + 2 \times 0.4 + 3 \times 0.2 + 4 \times 0.1 + 5 \times 0.1 + 6 \times 0.1 = 3 $$

## 2) Obtention par Spark{#moyenne-spark}

Deux méthodes renvoient la moyenne :

(il faut qu'il y ait eu une demande de regroupement, avant, pour que ce calcul puisse avoir lieu).

   \code

   > ```java
   > relationalGroupedDataset.mean(champ)
   > relationalGroupedDataset.avg(champ)
   > ```
   
   \endgroup

