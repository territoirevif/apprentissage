---
title: "s064. La moyenne harmonique"
subtitle: "les indicateurs de tendance centrale"
author: Marc Le Bihan
keywords:
- moyenne harmonique
- indicateur de tendance centrale
---

La moyenne \definition{harmonique} aide à \avantage{corriger les effectifs inégaux entre les groupes}.
\begingroup \large
$$ \rouge{H} = \frac{n}{\sum{\frac{1}{x}}} $$
\endgroup

