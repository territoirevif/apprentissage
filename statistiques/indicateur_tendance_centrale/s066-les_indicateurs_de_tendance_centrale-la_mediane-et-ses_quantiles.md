---
title: "s066. La médiane et les quantiles"
subtitle: "les indicateurs de tendance centrale"
author: Marc Le Bihan
keywords:
- médiane
- spark
- quantile
- quartile
- décile
- écart interquartile
- écart interdécile
- valeur extrême
- pivot
- graphique de quantiles
---

\attention{Pas de calcul de médiane possible tant qu'on a pas ordonné (trié) les valeurs de la série !}

## 1) La relation de la médiane avec les quantiles

\underline{Quantile $\rightarrow$ quartile, décile...}

   - Quartiles : 25%, 50%, 75%
   - Déciles : 10%, 20%, 30%... 90%

\bigbreak

On repère la valeur voulue dans la liste \attention{triée} en calculant un index à partir de :

   > \bleu{$n$} : l'effectif de la série que l'on observe  
   > le pourcentage ou la proportion de valeurs __inférieures ou égales__ que l'on veut : 

\bigbreak
   
   \begingroup \cadreAvantage
    
   > 25\% (= 0.25) d'une série de $\mathbf{n = 60}$ valeurs sont inférieures ou égales à :  
   > \    
   > \ \ \ \ $\text{index} = 60 \times 0.25 = 15$ : à la $\mathbf{15^{\text{ème}}}$ valeur de cette liste (série), __triée__.
 
   \endgroup

\bigbreak

$\mathbf{Q_{2}}$, le deuxième quartile, c'est la médiane.  
$Q_{3} - Q_{1}$, l'\definition{interquartile}, est une valeur souvent calculée.

## 2) Le calcul détaillé des quantiles et des quartiles 

$Q_{1}$, $Q_{2}$, $Q_{3}$...
   
   > "\bleu{\emph{En 2019, 10\% des communes ont moins de 100 habitants ($x_{0.1} = 100$)}},  
   > \ \ \ \ \ \bleu{\emph{25\% ont moins de $x_{0.25} = 200$}},  
   > \ \ \ \ \ \bleu{\emph{50\% de 450, 75\% de 1200 et 90\% de 3200.}}"

  \bigbreak
    
Lorsque l'on trie les observations $x_{i}$ par ordre croissant  
   \demitab un _quantile_ d'ordre \bleu{$p$} est la valeur d'une observation \bleu{$x_{p}$} :

  - Qui vaut entre __0__ et __1__ : 
   d'aucune à la totalité des observations faites,  
   ce nombre d'observations étant cumulé de la plus petite à la plus grande jusqu'à \bleu{$x_{p}$}

      - "_Je vois 3 fois la valeur 5, puis 8 fois la valeur 2, puis 6 fois la valeur 7_"  
        $\implies$ {2 (vu 8 fois), 5 (vu 3 fois), 7 (vu 6 fois)}.  
      \bleu{$N(x_{p})$} : pourra aller de 0 à 17  
      \demitab avec $x_{i}$ = {2, 2, 2, 2, 2, 2, 2, 2, 2, 5, 5, 5, 7, 7, 7, 7, 7, 7}.  
     - \bleu{$N$} est le nombre cumulé d'observations :  
       si $n_{2} \text{(nombre de valeurs qui valent 2)} = 8$, $n_{5} = 3$, $n_{7} = 6$,  
       en revanche : $N(2) = 8$\text{\ (nombre cumulé de valeurs valant jusqu'à 8 inclu)},  
       $N(5) = 11$, et $N(7) \text{ à lire : } N(x \leq 7) = 17$.  

  \bigbreak

 - Telle que la proportion des valeurs observées est _inférieure ou égale_ à $\bleu{x_{p}}$

     - Exemple $p = 0,25$ signifie : 25% des valeurs observées sont inférieures où égales à 2.
     - Puis, $p = 0.5$ : 50% des valeurs observées sont inférieures où égales à 5.
     - $p = 0.75$ : 75% des valeurs observées sont inférieures où égales à $x_{0.75} = 7$.  

## 3) Intérêt des quantiles, et des écarts inter[quartiles, décile...]

### a) Les quartiles, pour représenter 25\%, 50\%, 75\% des valeurs présentes

Si la distribution est gaussienne

   - Ecart interquartile \mauve{(50\% des valeurs, si gaussien)} : $Q_{3} - Q_{1}$ ou $x_{0.75} - x_{0.25}$  
    (c'est à dire qu'entre $Q_{1} = 0,25$ et $Q_{3} = 0,75$, il y a 50\% des valeurs)

   - Ecart interdécile \mauve{(80\% des valeurs, si gaussien)} : $x_{0.9} - x_{0.1}$

   - On les présentera dans une _boite à moustaches (boxplot)_
   
### b) Ecarter les valeurs extrêmes
 
En s'affranchissant :

   - des valeurs en deçà de $\rouge{x_{0.1}}$
   - et au delà de $\rouge{x_{0.9}}$

\bigbreak

Les écarts interquartiles permettent de calculer des pivots pour détecter des valeurs aberrantes :
   
   - le pivot gauche : $\rouge{p_{g}} = x_{\frac{1}{4}} - 1.5(x_{\frac{3}{4}} - x_{\frac{1}{4}})$.
   - le pivot droit : $\rouge{p_{d}} = x_{\frac{3}{4}} + 1.5(x_{\frac{3}{4}} - x_{\frac{1}{4}})$.   

\bigbreak
     
Et ces pivots secondaires peuvent renforcer la présomption d'aberration :

   - le pivot gauche : $\rouge{p_{g}} = x_{\frac{1}{4}} - 2(x_{\frac{3}{4}} - x_{\frac{1}{4}})$.
   - le pivot droit : $\rouge{p_{d}} = x_{\frac{3}{4}} + 2(x_{\frac{3}{4}} - x_{\frac{1}{4}})$.  

### c) Graphique de quantiles
   
  - En abscisse : les ordres des quantiles, allant de 0 à 1 (0% à 100% des observations) 
  - En ordonnée : les valeurs $\bleu{x_{(i)}}$

    - et l'on marque sur l'axe celles $\rouge{x_{(1)}}$ et $\rouge{x_{(n)}}$ extrêmes (considérant qu'on les a ordonnées),
    - et $x_{0.25}$, $x_{0.5}$, $x_{0.75}$, les quartiles.  

## 3) Informatique

Dans `Spark`, la récupération des quantiles se fait par [DataFrameStatFunctions](https://spark.apache.org/docs/2.4.4/api/java/org/apache/spark/sql/DataFrameStatFunctions.html),  
\demitab depuis le `Dataset` / `Dataframe` par un \fonction{ds.stat()}.

