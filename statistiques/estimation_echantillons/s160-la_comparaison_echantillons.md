---
title: "s160. La comparaison d'échantillons"
subtitle: ""
category: statistiques
author: Marc Le Bihan
keywords:
- comparaison
- échantillon
- comparaison d'échantillons
- comparaison de moyenne d'échantillon
- Student
- Fisher
- risque d'erreur alpha
- degré de liberté
- table de Student
- intervalle de confiance d'une différence de moyennes
- appariement
- échantillons appareillés
- moyenne des différences
- dispersion des différences
- analyse de la variance à un facteur
- dispersion des moyennes
- variance iter-groupe
- variance intra-groupe
- Loi de Fisher
- Loi de Student
---

La comparaison d'échantillons met en oeuvre les tests statistiques.

# I) La comparaison de moyenne

\avantage{La loi de Student est appropriée pour les petits effectifs : n < 30}.

_Student_ fait l'hypothèse qu'un petit échantillon suit, malgré tout, une loi normale.
\begingroup \large $$ \rouge{\mathbf{t}} = \frac{|m_{1} - m_{2}|}{\sqrt{\frac{s^2_{1}}{n_{1}} + \frac{s^2_{2}}{n_{2}}}} $$ \endgroup

\bleu{$t$} : plus il sera élevé, et plus la probabilité obtenir cette différence par hasard sera faible.

\bigbreak

> \underline{Recherche de \bleu{$t$} dans une table :} il dépend du __nombre de variables catégorielles__,  
> rapportée par une notion de \definition{degrés de liberté} \definition{$\mathbf{\nu}$}
>
>   \tab où, pour une comparaison de deux groupes d'effectifs $\bleu{n_{1}}$ et $\bleu{n_{2}}$  
>   \tab \demitab $\rouge{\nu} = (\bleu{n_{1}} - 1) + (\bleu{n_{2}} - 1)$
>
> et d'un risque d'erreur \definition{$\mathbf{\alpha}$}  
> \tab de "_\rouge{croire que notre hypothèse opérationnelle est en oeuvre, alors que ce n'est pas vrai : c'est le hasard qui a joué.}_"

\bigbreak

\underline{Retrouver une valeur dans la table de \emph{Student} avec \emph{R} :}

   > Avec _R_, on peut recalculer la table de _Student_   
   > Par exemple, pour un test 
   > 
   >    - \bleu{bilatéral}
   >    - \bleu{$\nu$} : le degré de liberté, $\nu = 9$
   >    - \bleu{$\frac{\alpha}{2}$} : c'est à dire $\frac{p}{2}$,  
   >      \demitab avec \bleu{$p$} : la probabilité que le résultat observé soit dû au hasard.  
   >      \tab $p = 0.05 \implies \frac{\alpha}{2} = 0.025$
   >
   > \fonction{abs(qt($alpha/2$, $\nu$))}

\code

   > ```R
   > abs(qt(0.025, 9))
   > ```

\endgroup

renvoit le croisement sur la table de Student de $\bleu{\nu = 9}$ et $\bleu{\alpha = 0.05}$.

   > ```txt
   > 2.262157
   > ```

## 1) L'intervalle de confiance d'une différence de moyennes

$$ \mauve{\mu_{m_1 - m_2}} = m_1 - m_2 $$
$$ \mauve{s_{m_1 - m_2}} = \sqrt{\frac{s^2_1}{n_1} + \frac{s^2_2}{n_2}} $$

\bigbreak

l'intervalle de confiance d'une différence de moyenne, vaut :
$$ \begingroup \large \rouge{\text{IC}_{0.95}} = \Big[\mauve{\mu_{m_1 - m_2}} - \bleu{t_{(\text{0.05 ; ddl})}} \times \mauve{s_{m_1 - m_2}} ; \mauve{\mu_{m_1 - m_2}} + \bleu{t_{(\text{0.05 ; ddl})}} \times \mauve{s_{m_1 - m_2}}\Big] \endgroup $$

# II) L'appariement (les échantillons appareillés)

à la différence de groupe témoins, l'\definition{appariement} s'avise avec le même groupe, avant et après une expérience.

   1. On fait l'expérience, et la différence avant, après pour chaque sujet

   2. On calcule :

      - La moyenne des différences (éventuellement en valeur absolue) : \definition{$\mathbf{|m_{d}|}$}
      - La dispersion des différences : \definition{$\mathbf{s_{d}}$}
      - La racine carrée du groupe de différences (unique) : \definition{n}

   3. le $\bleu{t}$ de Student (_plus il est grand, en valeur absolue, moins le hasard est susceptible de jouer_) vaut :
      \begingroup \large $$ \rouge{t} = \frac{|m_{d}|}{\frac{s_{d}}{\sqrt{n}}} $$ \endgroup

# III) L'analyse de la variance à un facteur

\underline{Problème :} Si l'on compare deux échantillons, on a le risque \definition{alpha} d'un faux significatif : on pense par la comparaison que le facteur agit, alors qu'il n'agit pas.

   > $\rightarrow$ \attention{Si on multiplie le nombre d'échantillons comparés entre-eux, le nombre de faux va augmenter, et cela devient peu solide}  
   > observer la différence de moyenne ne suffit pas.

\bigbreak

\demitab On va se fonder sur la \avantage{\underline{dispersion} des moyennes} : en disant qu'elle devrait être à __zéro__ si nos groupes ne bougent pas.  
\demitab Et à l'opposé, que plus elle augmentera, et plus l'on pourra penser que notre facteur a un effet.

   > \underline{Remarque :} on passe de l'étude :
   >
   >   - de niveau _"un groupe"_ (c'est à dire deux groupes comparés) avec $\mathbf{\overline{m}}$,
   >   - à _"ensemble de groupes"_ avec l'examen de $\mathbf{\overline{\overline{m}}}$

\bigbreak

Dans l'analyse de la variance, on s'apperçoit qu'il y a :

   - Une variance __inter-groupe__ qui quand elle augmente, tend à dire qu'un __facteur agit__,
   - et une variance __intra-groupe__ qui quand elle augmente, tend à dire qu'il __n'agit pas__.

## 1) Calcul de la dispersion des moyennes (entre groupes, donc)

\textcolor{blue}{$\overline{m_{i}}$} : moyenne d'un groupe

\textcolor{blue}{$\overline{\overline{m}}$} : moyenne de l'ensemble des moyennes des groupes

\textcolor{blue}{$k$} : le nombre de groupes  
\textcolor{blue}{$n_{i}$} : le nombre d'éléments d'un groupe  
\begingroup \large $$ \rouge{s^2_{\overline{m}}} = \frac{\sum{[n_i (\overline{m_i} - \overline{\overline{m}})^2]}}{k - 1} $$ \endgroup

## 2) Calcul de la moyenne de la variance des individus au sein de chaque groupe

\textcolor{blue}{$k$} : le nombre de groupes  
\textcolor{blue}{$n$} : le nombre d'individus par groupe  
\textcolor{blue}{$x$} : la valeur de chaque individu  
\begingroup \Large $$ \rouge{s^2_{g}} = \frac{\sum[\frac{\sum(x - \overline{m})^2}{n - 1}]}{k} $$ \endgroup

remarque : $\bleu{\frac{\sum(x - \overline{m})^2}{n - 1} = E(V)}$ est l'__estimation__ de la __variance__ de la population dont chaque groupe est extrait.

## 3) Calcul du rapport entre les deux, pour obtenir F, qui s'il est haut, dit un effet du facteur

Ce $\bleu{F}$ ressemble au $\bleu{t}$ de _Student_, mais qui suit une loi de \definition{Fisher}.   
S'il augmente, la chance que ce que nous constatons soit lié au hasard diminue.  
\begingroup \large $$ \rouge{F} = \frac{s^2_{\overline{m}}}{s^2_{g}} $$ \endgroup

\bigbreak

Cette loi de _Fisher_ fonctionne avec des degrés de liberté.  

   > Exemple : examiner 4 groupes de 12 individus  
   > \demitab $\implies ddl_{1} = 3 \text{ et } ddl_{2} = 44$
   >
   > (d'après la formule des degrés de liberté, vue plus haut : $\nu = (n_{1} - 1) + (n_{2} - 1)$)

