---
title: "s220. Le calcul de la p-value : Monte-Carlo ou (bootstrap, permutation)"
subtitle: "choisies selon qu'on connaît la distribution ou non"
author: Marc Le Bihan
---

On calcule la \definition{p-value} par rééchantillonage.

> Ce qu'on l'on veut savoir, c'est si la valeur absolue de la différence entre :
>
>    - la moyenne d'un groupe qu'on suppose avoir un facteur impactant
>    - et la moyenne de celle d'un autre qui ne l'a pas

> qu'on a observé lors d'une expérience,  
>
>    - est vraiment liée au facteur impactant que l'on croit (car la __p-value__ est faible) 
>    - ou possiblement due au hasard.

\bigbreak

Le problème, c'est comment trouver cette _p-value_ ?

## 1) Si l'on connait la loi de distribution : méthode de Monte-Carlo 

### a) Différence en valeur absolue entre la moyenne de notre échantillon et celle du groupe témoin

On retient la différence \underline{en valeur absolue} de la moyenne entre :

   - notre échantillon escompté avec un facteur agissant : $\mauve{m_{f}}$
   - et celui témoin : $\bleu{m_{t}}$ 
   $$ \bleu{\mathbf{d_{observation}}} = |\mauve{m_{f}} - \bleu{m_{t}}| $$

### b) On génère beaucoup d'autres groupes témoins avec la méthode de distribution qu'on connaît

L'on génère des milliers/millions d'autres groupes "_témoins_"  
   \demitab de même effectif que les nôtres,  
   \demitab aléatoirement,  
   \demitab en utilisant la loi de distribution qu'on connaît

### c) Calcul de la moyenne et des différences entre-eux, de ces groupes

On calcule la moyenne $\mauve{m_{i}}$ de chacun de ces groupes.

Puis, l'on calcule des différences $\mauve{d_{j}}$ en valeur absolue entre leurs moyennes.

   > $\mauve{d_{1}} = |m_{2} - m_{1}|, \mauve{d_{2}} = |m_{3} - m_{2}|, ...$  
   > (ou $d_{1} = |m_{145} - m_{28}|, d_{2} = |m_{4120} - m_{578}|$)

\bigbreak

À la fin de cette boucle, on aura \definition{$n_{\text{groupes alétatoires}}$}  
   \demitab différences de moyennes de groupes "_témoins_" aléatoires.

### d) Calcul de la p-value en faisant le rapport de combien de fois une différence de moyenne a dépassé la nôtre

On compte combien de fois  
   \demitab une différence de moyennes en valeur absolue  
   \demitab a atteint ou dépassée celle de notre $\mauve{d_{observation}}$.

\bigbreak

C'est à dire qu'on compare juste chaque $\bleu{d_{i}}$ avec $\mauve{d_{observation}}$ :

   \demitab $\to$ elle aura été dépasséee un certain nombre de fois : \definition{$n_{\text{difference\_observée\_atteinte\_ou\_depassée}}$}

\bigbreak

Et notre __p-value__, c'est le rapport entre  
   \demitab le nombre de fois qu'elle a été dépassée  
   \demitab sur le nombre de différences de groupes témoins qu'on a calculé
\begingroup \large $$ \bleu{\textbf{p-value}} = \frac{n_{\text{difference\_observée\_atteinte\_ou\_depassée}}}{n_{\text{groupes alétatoires}}} $$ \endgroup

## 2) Si l'on ne connaît pas la loi de distribution : méthode bootstrap (avec remise) et celle des permutations (sans remise)

Cette méthode est utilisée quand on ne connaît pas la loi de distribution.

### a) Différence en valeur absolue entre la moyenne de notre échantillon et celle du groupe témoin

On calcule $\bleu{d_{observation}}$ comme auparavant.

### b) On génère beaucoup d'autres groupes témoins avec des données piochées dans les deux groupes (le nôtre, le témoin)

On constitue des groupes aléatoires comme avec l'autre méthode  
   \demitab de même effectifs que les nôtres  
   \demitab mais leurs données viennent de celles de nos deux groupes de départ (notre échantillon, le témoin)  
   \demitab que l'on prend aléatoirement pour constituer des groupes de tout contenu.

### c) Variante avec remise (bootstrap) et sans remise (permutation)

Dans la méthode \definition{bootstrap}, le tirage des données pour constituer ces groupes est fait \avantage{avec remise}.   
C'est celle que l'on préfère, car il garantit l'indépendance.

Dans celle des \definition{permutations}, le tirage est \avantage{sans remise}.

### d) Calcul de la moyenne, des différences entre-eux de ces groupes, puis de la p-value

On procède alors de la même manière qu'avec la méthode Monte-Carlo  
   \demitab pour calculer des différences de moyennes en valeur absolue entre ces groupes "_témoins_" aléatoires  
   \demitab et de la même manière, on aura $n_{\text{groupes alétatoires}}$  
   \demitab et on calcule $n_{\text{difference\_observée\_atteinte\_ou\_depassée}}$  
   \demitab et par leur rapport, l'on obtient la __p-value__

