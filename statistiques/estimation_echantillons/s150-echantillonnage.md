---
title: "s150. Echantillonnage"
subtitle: "Choisir les individus qui iront dans un échantillon"
category: statistiques
author: Marc Le Bihan
keywords:
- échantillonnage
- échantillon
- dataset
- population complète
- valeur extrême
- échantillonnage aléatoire
- échantillonnage stratifié
- échantillonnage systématique
- tirage avec remise
- tirage sans remise
- sample
- spark
- r
---

[Spark : Extraire un échantillon depuis un dataset existant](#extraire-echantillon-spark)  
[R : Tirer un échantillon qui suit une loi uniforme](#echantillon-uniforme-r)

---

L'échantillonnage, c'est choisir les individus qui vont aller dans l'échantillon.

Car le niveau de précision ou de confiance n'est pas le même, selon qu'on sait étudier des chiffres de la population complète ou ceux d'un échantillon tirée d'elle.

En particulier, quand on tire plusieurs échantillons, leurs valeurs diffèrerent.  
Certaines valeurs sont même extrêmes.

\bigbreak
 
   \begingroup \cadreGris

   > Un échantillon est \definition{représentatif} s'il a :  
   > \demitab - la même moyenne  
   > \demitab  - et la même dispersion que la population
   > \  
   > $\rightarrow$ seul son effectif est plus petit.

   \endgroup

# I) Les types d'échantillonages

L'échantillonnage peut être :

   - aléatoire
   - stratifié
   - systématique

# II) Obtention d'échantillons

## 1) Depuis un dataset existant, par Spark{#extraire-echantillon-spark}

   \begingroup \cadreGris

   > \bleu{fraction} : la proportion d'enregistrements à extraire, entre 0 et 1.  
   > Il n'y a pas de garantie d'extraction d'un nombre exact d'éléments.   
   > \   
   >
   > \bleu{seed} : graine pour le générateur aléatoire.  
   > \  
   >
   > \bleu{withReplacement} (booléen) : tirage avec remise (_true_) ou non (_false_).  
   > \	
   > ```java
   > ds.sample(withRemplacement, fraction, seed)
   > ```

   \endgroup

## 2) Un échantillon suivant une loi uniforme, par R{#echantillon-uniforme-r}

   \code

   > ```r
   > # 10 valeurs, comprises entre [1,12[
   > runif(10, min=1, max=12)
   > 
   > # Les mêmes, mais entières (floor = arrondir à l'inférieur)
   > floor(runif(10, min=1, max=12))
   > ```

   \endgroup

\bigbreak

les 10 valeurs tirées par runif :

   > ```log
   > 3.398175  2.311946 11.660116  8.734312  2.916065  
   > 4.758134  8.975491  6.556699  3.175433  8.914450
   > ```

