---
header-includes:
   \input{apprentissage-header-include}
   \input{apprentissage-include}

title: "Statistiques  \n07. Estimation d'une moyenne, variance, proportion"
subtitle: ""
author: Marc Le Bihan
geometry: margin=2cm
fontsize: 12pt
output: pdf
classoption: fleqn
urlcolor: blue
---

# I) Estimation d'une moyenne à partir d'un échantillon

## 1) Rappel sur la moyenne et la variance d'un échantillon

Si la population était bien définie, nous aurions : \mauve{$E(\overline{X}) = m$} et \mauve{$V(\overline{X}) = \frac{\sigma^2}{n}$}.

mais comme nous sommes sur un échantillon,
 
  > $$\overline{x} = \frac{1}{n}\sum_{i=1}^{n} x_i$$  
  > \definition{$\mathbf{\overline{x}}$} : moyenne de l'échantillon. Elle ne change pas de sa définition habituelle.  
  > $$s^2 = \frac{1}{n-1}\sum_{i=1}^{n}(x_i - \overline{x})^2$$
  > \definition{$\mathbf{s^2}$} : variance empirique de l'échantillon : __elle se calcule sur un élément de moins__.

## 2) Cas où la population est gaussienne

\underline{Note :} dans les formules qui suivent, \mauve{u} et \mauve{t} désignent des valeurs obtenues dans les tables normales, student ou khi-deux à partir du risque  \mauve{$\alpha$} que l'on veut).  

voici l'estimation de la moyenne par \definition{intervalle de confiance} suivant les différents cas d'effectifs, __et__ selon que la variance est connue ou pas.

### a) $n$ petit, $\sigma$ connu
$$X \sim N(m, \sigma)$ et $\overline{X} \sim N(m, \frac{\sigma}{\sqrt{n}})$$
$$m = \overline{x} \pm u.\frac{\sigma}{\sqrt{n}}$$
  
> C'est à dire qu'il y a 95% de chances que $m = \overline{x} \pm 2.\frac{\sigma}{\sqrt{n}}$

   \code

   > \textbf{Calcul d'un intervalle de confiance à 95\% du poids moyen d'un échantillon}  
   > \textbf{de 8 boites de chocolats par la loi normale ($\sigma$ connu, $n$ petit)} \newline 
   >
   > ```java
   > public void unConfiseurVendDesChocolats() {
   >    LOGGER.info("==> Exercice 9 : Un confiseur vend des boites de chocolats de poids normalement distribué.");
   >    LOGGER.info("Les pesées de 8 boites donnent un poids moyen de 0.995 et un écart-type de 0.015.");
   > 
   >    // a) Donner un intervalle de confiance de niveau 1 - α = 95% de m, le poids moyen d'une boite
   >    // la moyenne suit XBarre ∼ N(µ, σ/√n) : variance σ divisée par la racine de l'effectif de l'échantillo
   >    double ecartTypeEchantillon8 = 0.015 / Math.sqrt(8);
   >    LOGGER.info("\tLe poids moyen d'un l'échantillon de 8 boites de chocolats,XBarre, suit une loi N(0.095, {})", ecartTypeEchantillon8);
   > 
   >    // pour trouver un intervalle à 95% de confiance, m = xbarre ± u.(σ / √n), il faut déterminer u
   >    // l'inverse de 0.95 sur N(0,1) donnerait la probabilité d'une aire de 0.95, mais non centrée sur zéro, alors que c'est ce dont on a besoin.
   >    // ce que nous cherchons, c'est P(-t < X < t) = 2ϕ(t) - 1, c.à.d 2ϕ(t) - 1 = 0.95 => ϕ(t) = 0.975
   >    // et ϕ(t) c'est "la fonction inverse de la normale N(0,1)" à appliquer
   >    double phi = (1 + 0.95) / 2;
   >    NormalDistribution NCentreeReduite = new NormalDistribution(0, 1);
   >    double u = NCentreeReduite.inverseCumulativeProbability(phi);
   > 
   >    // maintenant on peut appliquer m = xbarre ± u.(σ / √n)
   >    double borneInfIntervalleConfianceMoyenneBoiteChocolat = 0.995 - u * ecartTypeEchantillon8;
   >    double borneSupIntervalleConfianceMoyenneBoiteChocolat = 0.995 + u * ecartTypeEchantillon8;
   >    LOGGER.info("\ta) En résolvant par une loi normale, l'intervalle à 95% de confiance du poids moyen d'une boite de chocolats d'un échantillon de huit boites");
   >    LOGGER.info("\test m = 0.995 ± 0.015.{}, c'est à dire [{}, {}]", u, borneInfIntervalleConfianceMoyenneBoiteChocolat, borneSupIntervalleConfianceMoyenneBoiteChocolat);
   >    assertEquals(0.982, borneInfIntervalleConfianceMoyenneBoiteChocolat, 0.01, "Mauvaise borne inféreure de l'intervalle à 95% de confiance du poids moyen d'un échantillon de huit boites de chocolats, par un calcul via la loi Normale");
   >    assertEquals(1.008, borneSupIntervalleConfianceMoyenneBoiteChocolat, 0.01, "Mauvaise borne supérieure de l'intervalle à 95% de confiance du poids moyen d'un échantillon de huit boites de chocolats, par un calcul via la loi Normale");
  > }
   > ```

   \endgroup

### b) $n$ petit, $\sigma$ inconnu

Suit une loi de Student $T(n - 1)$ : \avantage{cette loi est adaptée aux petits échantillons}.
$$m = \overline{x} \pm t.\frac{s}{\sqrt{n}}$$

   \code

   > \textbf{Calcul d'un intervalle de confiance à 95\% du poids moyen d'un échantillon}  
   > \textbf{de 8 boites de chocolats par la loi de Student ($\sigma$ inconnu, $n$ petit)} \newline
   >
   > ```java
   > public void unConfiseurVendDesChocolats() {
   >    LOGGER.info("==> Exercice 9 : Un confiseur vend des boites de chocolats de poids normalement distribué.");
   >    LOGGER.info("Les pesées de 8 boites donnent un poids moyen de 0.995 et un écart-type de 0.015.");
   > 
   >    // a) Donner un intervalle de confiance de niveau 1 - α = 95% de m, le poids moyen d'une boite
   >    // On va adresser une distribution de Student de 7 degrés de liberté (effectif de 8 boites dans l'échantillon)
   >    // avec phi = (1 - 0.95) / 2 = 0.25
   >    TDistribution Student = new TDistribution(7);
   >    // phi = (1 + 0.95) / 2; reste le même
   >    double t = Student.inverseCumulativeProbability(phi);
   >    LOGGER.info("\tEn résolvant par une loi de Student (n petit, sigma inconnu), l'intervalle à 95% de confiance du poids moyen d'une boite de chocolats d'un échantillon de huit boites");
   >    borneInfIntervalleConfianceMoyenneBoiteChocolat = 0.995 - t * ecartTypeEchantillon8;
   >    borneSupIntervalleConfianceMoyenneBoiteChocolat = 0.995 + t * ecartTypeEchantillon8;
   >    LOGGER.info("\test m = 0.995 ± 0.015.{}, c'est à dire [{}, {}]", t, borneInfIntervalleConfianceMoyenneBoiteChocolat, borneSupIntervalleConfianceMoyenneBoiteChocolat);
   >    assertEquals(0.982, borneInfIntervalleConfianceMoyenneBoiteChocolat, 0.01, "Mauvaise borne inféreure de l'intervalle à 95% de confiance du poids moyen d'un échantillon de huit boites de chocolats, par un calcul via la loi de Student");
   >    assertEquals(1.008, borneSupIntervalleConfianceMoyenneBoiteChocolat, 0.01, "Mauvaise borne supérieure de l'intervalle à 95% de confiance du poids moyen d'un échantillon de huit boites de chocolats, par un calcul via la loi de Student");
   > }
   > ```

   \endgroup

## 3) Cas où la population est quelconque

(Incluant gaussienne, avec $n$ grand).

### a) n grand, $\sigma$ connu

\avantage{Théorème Central Limite (TCL)}

Si $n$ est suffisamment grand (n $\geq 30$), la moyenne $\overline{X}$ suit une loi normale.  
$$m = \overline{x} \pm u.\frac{\sigma}{\sqrt{n}}$$

### b) n grand, $\sigma$ inconnu
$$m = \overline{x} \pm u.\frac{s}{\sqrt{n}}$$

   > __Si__ n $\geq 30$  
   > __alors__ $$\overline{X} \approx N\big(m, \frac{\sigma}{\sqrt{n}}\big)$$

# II) Estimation d'une variance à partir d'un échantillon

\attention{On ne sait faire cette estimation que sur une population gaussienne}

Si l'on tirait tous les échantillons possibles jusqu'à faire toute la population, l'espérance de la variablité qu'on aurait finirait par être celle de la population : $E(s^2) = \sigma^2$.

## 1) Si l'on connait la moyenne de l'échantillon

$\rouge{S'^{2}}$ : estimateur de la variance \mauve{$\sigma^{2}$} qui nous est inconnue
   $$ \rouge{S'^{2}} = \frac{1}{n}(X_{i} - m)^2 $$

et alors $\frac{nS'^{2}}{\sigma^2}$ va suivre une loi du Khi-Deux.

   > \bleu{n} : taille de l'échantillon
   > $$ \frac{nS'^{2}}{\sigma^2} \sim \chi^2(n) $$

### a) Déterminer un intervalle de confiance

\underline{La manip mathématique que l'on cherche à faire, c'est :}  

On cherche à sortir $\rouge{\sigma^2}$ de l'expression $\frac{nS'^{2}}{\sigma^2}$,  

   > dont on sait que la probabilité d'être comprise entre \mauve{$a$} et \mauve{$b$} dépend d'un :  
   > un risque \bleu{$\alpha$} (par exemple : 5\%),  
   > c'est à dire que c'est une confiance de $1 - \alpha$ (par exemple : 95\%)  
   >
   > que l'on a que :  
   > \ \ \ $p(\{a \leq \frac{nS'^{2}}{\sigma^2} \leq b\}) = 1 - \alpha$

pour l'encadrer dans un intervalle :
$$\sigma^2 \in \bigg[\frac{nS'^{2}}{b} ; \frac{nS'^{2}}{a}\bigg] \text{\ \ \ \ \ \ \ \ si l'on veut considérer la variance} $$

$$\sigma \in \bigg[\sqrt{\frac{nS'^2}{b}} ; \sqrt{\frac{nS'^2}{a}}\bigg] \text{\ \ \ \ si l'on veut considérer l'écart-type} $$

Pour cela :

   >  ![un intervalle de confiance dans une loi du Khi-Deux](statistiques/intervalle_confiance_khi_deux_bornes.jpg)
   >
   > $\mauve{a}$ : borne inférieure de la courbe de distribution du $\chi^2(n)$,  
   > \ \ \ \ telle que $p(\{\frac{nS'^{2}}{\sigma^2} \leq a\}) = \frac{\alpha}{2}$  
   > \ \ \ \ c'est à dire qu'on récupère cette valeur $a$ par une inverse Khi-Deux.
   >
   > $\mauve{b}$ : borne supérieure de la courbe de distribution du $\chi^2(n)$,  
   > \ \ \ \ telle que $p(\{\frac{nS'^{2}}{\sigma^2} \leq b\}) = 1 - \frac{\alpha}{2}$  
   >
   > \begingroup \footnotesize (puisque $p(\{a \leq \frac{nS'^{2}}{\sigma^2} \leq b\}) = 1 - \alpha$) \endgroup  
   > \   

et c'est de là qu'on sortira $\rouge{\sigma^2}$ :
$$ \frac{nS'^{2}}{b} \leq \rouge{\sigma^2} \leq \frac{nS'^{2}}{a} $$  
\ \ \ \ \ \ au risque de $\bleu{\alpha}$  
\ \ \ \ \ \ ou à $\bleu{1 - \alpha}$ de confiance.

---

Mais d'ici-là, pour un échantillon :

__Si__ la population est gaussienne,  
__alors__ $$\frac{(n-1)s^2}{\sigma^2} \sim \chi^2(n-1)$$
__et__ $$\frac{\overline{X} - m}{\frac{s}{\sqrt{n}}} \sim T(n-1)$$

La première formule réclame de connaître $\sigma$, l'autre non.

Son estimation par intervalle de confiance : 

(C'est à dire que $\frac{(n-1)s^2}{\sigma^2} \sim \chi^2(n-1)$, mais il nous reste à dire avec quel niveau de confiance on veut connaître ces résultats).

   > \bleu{$\alpha$} : le risque que l'on désire. Pour 95\%, $\alpha = 0.05$  
   > \bleu{$a$} : $\frac{\alpha}{2}$ : la borne inféfieure de l'intervalle de confiance dans la courbe $\chi^2(n-1)$  
   > \bleu{$b$} : 1 - $\frac{\alpha}{2}$ : la borne supérieure de l'intervalle de confiance dans la courbe $\chi^2(n-1)$

La variance est comprise entre :
$$\sigma^2 \in \bigg[\frac{(n - 1)s^2}{b} ; \frac{(n - 1)s^2}{a}\bigg]$$

ou si l'on préfère, l'écart-type est copmpris entre :
$$\sigma \in \bigg[\sqrt{\frac{(n - 1)s^2}{b}} ; \sqrt{\frac{(n - 1)s^2}{a}}\bigg]$$

au risque $\alpha$ donné.

### b) Estimation d'une variance à partir d'un échantillon
$$ E(V) = s^2 = \sum\frac{(x - m)^2}{n - 1}$$

### c) Le coefficient de variation (c.v.)
$$ \text{c.v.} = \frac{s}{\mu}$$

### d) L'erreur type (pour information : son usage est discutable) (SE)

Le Standard Error (SE) ou Standard Error Mean (SEM) se calcule ainsi :
$$ SE = \frac{s}{\sqrt{n}} $$
il est critiqué pour trop dépendre de l'éffectif de l'échantillon.

# III) Estimation d'une proportion (d'individus ayant une certaine caractéristique)

__Si__ n est suffisamment grand,  
__alors__ $$F \sim N\bigg(p, \sqrt{\frac{p(1-p)}{n}}\bigg) \text{\ : loi approchée} $$
$$nF \sim N\big(np, \sqrt{np(1-p)}\big)$$

Son estimation par intervalle de confiance :

## 1) Cas d'une population gaussienne
$$p = f \pm u.\sqrt{\frac{f(1 - f)}{n}}$$
