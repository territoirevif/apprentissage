---
title: "s156. Estimation d'une moyenne à partir d'un échantillon"
subtitle: ""
author: Marc Le Bihan
category: statistiques
keywords:
- estimation
- moyenne
- population gaussienne
- échantillon variance connue
- échantillon variance inconnue
- intervalle de confiance
- échantillon
- java
---

\underline{Rappel sur la moyenne et la variance d'un échantillon :}

Si la population était bien définie, nous aurions : \mauve{$E(\overline{X}) = m$} et \mauve{$V(\overline{X}) = \frac{\sigma^2}{n}$}.

mais comme nous sommes sur un échantillon,

  \tab \definition{$\mathbf{\overline{x}}$}, la moyenne de l'échantillon, ne change pas de sa définition habituelle.  
  $$ \tab \overline{x} = \frac{1}{n}\sum_{i=1}^{n} x_i $$

  \tab \definition{$\mathbf{s^2}$}, variance empirique de l'échantillon __se calcule sur un élément de moins__.
  $$ \tab s^2 = \frac{1}{n-1}\sum_{i=1}^{n}(x_i - \overline{x})^2 $$

---

## 1) Cas où la population est gaussienne

\underline{Note :} dans les formules qui suivent  
   \demitab \mauve{u} et \mauve{t} désignent des valeurs obtenues dans les tables normales, student ou khi-deux  
   \demitab à partir du risque \mauve{$\alpha$} que l'on veut.  

\bigbreak

Voici l'estimation de la moyenne par \definition{intervalle de confiance} selon 

   - les différents cas d'effectifs
   - __et__ que la variance est connue ou pas.

### a) petit effectif, variance de l'échantillon connue ($n$ petit, $\sigma$ connu)
$$X \sim N(m, \sigma)$ et $\overline{X} \sim N(m, \frac{\sigma}{\sqrt{n}})$$
$$m = \overline{x} \pm u.\frac{\sigma}{\sqrt{n}}$$
  
> C'est à dire qu'il y a 95% de chances que $m = \overline{x} \pm 2.\frac{\sigma}{\sqrt{n}}$

   \code

   > \textbf{Calcul d'un intervalle de confiance à 95\% du poids moyen d'un échantillon}  
   > \textbf{de 8 boites par la loi normale ($\sigma$ connu, $n$ petit)} \newline 
   >
   > ```java
   > public void unConfiseurVendDesChocolats() {
   >    LOGGER.info("==> Exercice 9 : Un confiseur vend des boites de poids normalement distribué.");
   >    LOGGER.info("Les pesées de 8 boites donnent un poids moyen de 0.995 et un écart-type de 0.015.");
   > 
   >    // a) Donner un IC de niveau 1 - α = 95% de m, le poids moyen d'une boite
   >    double ecartTypeEchantillon8 = 0.015 / Math.sqrt(8);
   >    LOGGER.info("\tLe poids moyen d'un l'échantillon de 8 boites,XBarre, suit une loi N(0.095, {})", ecartTypeEchantillon8);
   > 
   >    // pour trouver un intervalle à 95% de confiance, il faut déterminer u
   >    double phi = (1 + 0.95) / 2;
   >    NormalDistribution NCentreeReduite = new NormalDistribution(0, 1);
   >    double u = NCentreeReduite.inverseCumulativeProbability(phi);
   > 
   >    double borneInfICMoyBoite = 0.995 - u * ecartTypeEchantillon8;
   >    double borneSupICMoyBoite = 0.995 + u * ecartTypeEchantillon8;
   >
   >    LOGGER.info("\ta) En résolvant par une loi normale, l'intervalle à 95% de confiance du poids moyen d'une boite d'un échantillon de huit boites");
   >    LOGGER.info("\test m = 0.995 ± 0.015.{}, c'est à dire [{}, {}]", u, borneInfICMoyBoite, borneSupICMoyBoite);
   >    assertEquals(0.982, borneInfICMoyBoite, 0.01, "Mauvaise borne inféreure de l'intervalle à 95% de confiance du poids moyen d'un échantillon de huit boites, par un calcul via la loi Normale");
   >    assertEquals(1.008, borneSupICMoyBoite, 0.01, "Mauvaise borne supérieure de l'intervalle à 95% de confiance du poids moyen d'un échantillon de huit boites, par un calcul via la loi Normale");
  > }
   > ```

   \endgroup

### b) petit effectif, variance de l'échantillon inconnue ($n$ petit, $\sigma$ inconnu)

Il suit une loi de Student $T(n - 1)$ : \avantage{cette loi est adaptée aux petits échantillons}.
$$m = \overline{x} \pm t.\frac{s}{\sqrt{n}}$$

   \code

   > \textbf{Calcul d'un intervalle de confiance à 95\% du poids moyen d'un échantillon}  
   > \textbf{de 8 boites par la loi de Student ($\sigma$ inconnu, $n$ petit)} \newline
   >
   > ```java
   > public void unConfiseurVendDesChocolats() {
   >    LOGGER.info("==> Exercice 9 : Un confiseur vend des boites de poids normalement distribué.");
   >    LOGGER.info("Les pesées de 8 boites donnent un poids moyen de 0.995 et un écart-type de 0.015.");
   > 
   >    // a) Donner un IC de niveau 1 - α = 95% de m, le poids moyen d'une boite
   >    TDistribution Student = new TDistribution(7);
   >    double t = Student.inverseCumulativeProbability(phi);
   >
   >    LOGGER.info("\tEn résolvant par une loi de Student (n petit, sigma inconnu), l'intervalle à 95% de confiance du poids moyen d'une boite d'un échantillon de huit boites");
   >    borneInfICMoyBoite = 0.995 - t * ecartTypeEchantillon8;
   >    borneSupICMoyBoite = 0.995 + t * ecartTypeEchantillon8;
   >
   >    LOGGER.info("\test m = 0.995 ± 0.015.{}, c'est à dire [{}, {}]", t, borneInfICMoyBoite, borneSupICMoyBoite);
   >    assertEquals(0.982, borneInfICMoyBoite, 0.01, "Mauvaise borne inféreure de l'intervalle à 95% de confiance du poids moyen d'un échantillon de huit boites, par un calcul via la loi de Student");
   >    assertEquals(1.008, borneSupICMoyBoite, 0.01, "Mauvaise borne supérieure de l'intervalle à 95% de confiance du poids moyen d'un échantillon de huit boites, par un calcul via la loi de Student");
   > }
   > ```

   \endgroup

## 2) Cas où la population est quelconque

(Incluant gaussienne, avec $n$ grand).

### a) grand effectif, variance connue (n grand, $\sigma$ connu)

\avantage{Théorème Central Limite (TCL)}

Si $n$ est suffisamment grand (n $\geq 30$), la moyenne $\overline{X}$ suit une loi normale.  
$$m = \overline{x} \pm u.\frac{\sigma}{\sqrt{n}}$$

### b) grand effectif, variance inconnue (n grand, $\sigma$ inconnu)
$$m = \overline{x} \pm u.\frac{s}{\sqrt{n}}$$

   > __Si__ n $\geq 30$  
   > \demitab __alors__ $$\overline{X} \approx N\big(m, \frac{\sigma}{\sqrt{n}}\big)$$

