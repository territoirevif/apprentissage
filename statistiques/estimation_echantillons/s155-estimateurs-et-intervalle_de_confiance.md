---
title: "s155. Estimateurs et Intervalles de confiance"
subtitle: "Estimation de paramètres d'une distribution, intervalles de confiance, propriétés d'un bon estimateur"
category: statistiques
author: Marc Le Bihan
keywords:
- distribution
- estimation
- paramètre
- échantillon
- prédiction
- estimation d'une moyenne
- estimation d'une variance
- estimation d'une proportion
- intervalle de confiance
- intervalle de fluctuation
- estimation ponctuelle
- theta
- biais
- estimateur
---

# I) Pourquoi estimer, et quoi ?

\tab \avantage{Si on a éclairé une distribution en découvrant quelle loi elle suit avec quels paramètres, on a "gagné" parce qu'on peut prédire les valeurs qu'on est enclin à recevoir d'elle}.


## 1) On pense que notre échantillon suit une distribution : trouvons ses paramètres !

On tente d'obtenir ces paramètres depuis un échantillon,  
\demitab d'une distribution qui dit suivre une loi avec ces paramètres.

On verra bien plus tard, par calcul d'erreurs, si on a trouvé les bons  
\demitab en faisant de l'entraînement et de la prédiction.

## 2) Les paramètres que l'on peut extraire d'un échantillon

Parmi les paramètres que l'on peut calculer depuis un échantillon, il y a :

   - la \definition{moyenne}, 
   - la \definition{variance} 
   - et la \definition{proportion}

qui seront plus ou moins affirmés selon la taille de l'échantillon.  
et c'est pour cette raison, qu'un \definition{intervalle de confiance} les accompagne.

## 3) L'estimateur est l'outil qui fait l'estimation

L'outil qui fait l'\definition{estimation} de ces paramètres s'appelle un \definition{estimateur} :  
\demitab il est lui-même une variable aléatoire (à valeurs réelles),  
\demitab il et possède aussi une distribution de probabilité.

# II) L'intervalle de confiance et celui de fluctuation

Etant sur un échantillon, et pas la population totale,  
\demitab nous ne pouvons pas affirmer : $\rouge{\overline{x} = 5}$  
\demitab mais $\bleu{\overline{x} = 5 \pm 0.2}$ ou $\bleu{\overline{x} \in [4.8, 5.2]}$

Nous donnons un __intervalle de confiance__  
\demitab qui est une \avantage{estimation fiable}  
\demitab de paramètres \underline{inconnus} de notre population.

## 1) Intervalle de confiance

La précision de son estimation dépend de l'effectif de notre échantillon.

   \begingroup \cadreGris

   > \bleu{$t$} : valeur de student (pour un test bilatéral, par exemple)
   > $$ \rouge{\text{IC}_{0.95}} = \Big[m - t_{(0.05 ; ddl)} \times \frac{\sigma}{\sqrt{n}}\ ;\ m + t_{(0.05 ; ddl)} \times \frac{\sigma}{\sqrt{n}}\Big] $$
 
   \endgroup

à ne réaliser que si $n > 30$ __ou__ qu'il est plus petit mais que l'on sait qu'il suit une loi normale.

## 2) Intervalle de fluctuation

L'\definition{intevalle de fluctuation}, lui, donne la probabilité qu'une valeur de paramètre  
\demitab tombe dans ses limites  
\demitab dans un échantillon d'effectif connu.

S'il tombe hors limites, on peut considérer la valeur non représentative  
\demitab (provient d'une population différente, par eexemple).

   \begingroup \cadreGris

   > $$ \rouge{\text{IF}_{0.95}} = \Big[\mu - 1.96 \times \frac{\sigma}{\sqrt{n}}\ ;\ \mu + 1.96 \times \frac{\sigma}{\sqrt{n}}\Big] $$

   \endgroup

## 3) Estimation ponctuelle

Une estimation ponctuelle veut fournir à $\Theta$ (qui est donc : une moyenne, une variance ou une proportion) une valeur approchée.

# II) Ce que doit respecter un estimateur

L'\definition{estimateur} va approcher la moyenne, variance, médiane... de la population  
On le désigne par $\mathbf{\overline{\Theta}_n}$.

## 1) Qualités d'un bon estimateur

La fonction qui va servir d'estimateur de paramètre, et qui évalue / donne :

   > $\rouge{\Theta}$ : la valeur du paramètre qu'on ne connaît pas, et qu'on aimerait bien approcher,  
   > $\mauve{\overline{\Theta}_n}$ : l'estimation ponctuelle de $\Theta$ 

### a) Il converge

Un bon estimateur doit :

   - converger vers $\Theta$

### b) Il est précis

Un estimateur doit être précis :  
    \demitab On le mesure par sa propre variance.

\bigbreak

\begingroup \cadreGris

> \mauve{$s^2$} (variance empirique) est l'estimateur de \rouge{$\sigma^2$} (variance de la population totale).  
> La précision de $s^2$, dans le cas d'une population gaussienne, est
  \large $$ \rouge{V(s^2)} = \frac{2\sigma^4}{n-1} \tab \gris{\text{c'est la variance... de la variance !}} $$

\endgroup

\bigbreak

On considère qu'un estimateur $\overline{\Theta}_{n}$  
\demitab est plus efficace que $\overline{\Theta}'_{n}$ si :
$$ V(\overline{\Theta}_{n}) \leq V(\overline{\Theta}'_{n}) $$

### c) Il est sans biais

   - être sans biais : $E(\mauve{\overline{\Theta}_{n}}) = \rouge{\Theta}$
   - ou asymptotiquement sans biais : $\lim\limits_{n \to +\infty}E(\mauve{\overline{\Theta}_n)} = \rouge{\Theta}$
   - convergent sans biais ou asymtotiquement sans biais :  $\lim\limits_{n \to +\infty}V(\mauve{\overline{\Theta}_n)} = 0$

## 2) Intervalle de confiance

Il faut un intervalle de confiance autour de $\mauve{\overline{\Theta}_{n}}$  
\demitab pour transformer une estimation ponctuelle $\rightarrow$ en quelque-chose de vraiment utile.

\demitab car __c'est l'intervalle où il est vraissemblable que la vraie valeur $\Theta$ se trouve.__  
avec une probabilité [niveau de confiance] de $(1 - \alpha)$.

Si on dit que $E$ est la marge de l'estimation,

   > déterminée par ce niveau de confiance,  
   > et par l'écart-type,


la valeur de $\rouge{\Theta}$ sera encadrée par 
\bl $$ \mauve{\overline{\Theta}_{n}} - E \leq \rouge{\Theta} \leq \mauve{\overline{\Theta}_{n}} + E $$ \endgroup

---

\danger{Attention à ne pas confondre :}

   - \danger{paramètre à estimer dans la population totale, à partir d'un échantillon : moyenne, variance, proportion}
   - \danger{et paramètre d'une Loi (de Poisson, Binomiale...) !}

