---
title: "s070. Les indicateurs de distribution : la variance et l'écart-type"
subtitle: ""
author: Marc Le Bihan
keywords:
- variance
- indicateur de distribution
- écart-type
- fonction de densité
---

## 1) La Variance

La variance est le \definition{moment d'ordre 2}.

### a) Les propriétés de la variance

   \begingroup \large
   $$ \rouge{\sigma^2_{x}} = \frac{1}{n}\sum(x_{i} - \bar{x})^2 = \frac{1}{n}\sum x_{i}^2 - \bar{x}^2 $$
   \endgroup

   > $V(b) = 0$ \tab \demitab c'est normal, puisqu'une constante ne varie pas  
   > $V(aX) = a^2V(X)$
   > \   
   > $V(X - \mu) = V(X)$

### b) Cas particulier : calcul d'une variance quand on connaît seulement sa fonction de densité

   \begingroup \small \cadreGris

   > | $\mathbf{x}$     | __1__ | __2__ | __3__ | __4__ | __5__ | __6__ |
   > | :--------------: | :---: | :---: | :---: | :---: | :---: | :---: |
   > | $\mathbf{P_{x}}$ |  0.1  |  0.4  |  0.2  |  0.1  |  0.1  |  0.1  |
   > \    
   > __Résolution mathématique__
   > $$ V(X) = \sum_{i=1}^{6}P_{x}(x - \bar{x})^2 $$
   > $$ V(X) = 0.1 (1 - 3)^2 + 0.4 (2 - 3)^2 + 0.2 (3 - 3)^2 + 0.1 (4 - 3) ^2 + 0.1 (5 - 3)^2 + 0.1 (6 - 3)^2  $$
   > $$ V(X) = 0.4 + 0.4 + 0 + 0.1 + 0.4 + 0.9 = 2.2 $$
   
   \endgroup

## 2) l'écart-type

   \begingroup \large
   $$ \rouge{\sigma_{x}} = \sqrt{\frac{1}{n}\sum(x_{i} - \bar{x})^2} $$
   \endgroup

