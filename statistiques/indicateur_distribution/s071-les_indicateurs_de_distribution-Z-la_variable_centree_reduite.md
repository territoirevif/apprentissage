---
title: "s071. Z la variable centrée réduite"
subtitle: ""
author: Marc Le Bihan
keywords:
- centrage-réduction
- écart-type
- variance
- centrer une variable
- réduire une variable
---
  
\begingroup \large $\rouge{Z} = \frac{X - E(X)}{\sqrt{V(X)}}$ \endgroup _centre_ (au nominateur) et _réduit_ (au dénominateur) une variable.

\bigbreak

La conséquence, c'est que $\mathbf{V(Z) = 1}$

\bigbreak

L'on prend souvent l'écart-type d'une variable centrée réduite comme unité d'expression  
\tab car son carré comme sa variance sont égaux à 1.

\bigbreak

Une application : le centrage réduction de valeurs
$$ \rouge{z_{i}} = \frac{x_{i} - \bar{x}}{s_{x}} $$ 

