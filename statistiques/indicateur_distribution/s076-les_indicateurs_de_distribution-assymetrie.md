---
title: "s076. Les indicateurs de distribution : l'assymétrie"
subtitle: ""
author: Marc Le Bihan
keywords:
- assymétrie
- indicateur de distribution
- moment d'ordre 3
- dissymétrie à gauche
- dissymétrie à droite
---

L'assymétrie est le \definition{moment d'ordre 3}.
\begingroup \large $$ \rouge{m_{3}} = \frac{1}{n}\sum_{i=1}^{n}(x_{i} - \bar{x})^3 $$ \endgroup

   - Positif pour une dissymétrie à gauche.
   - Négatif pour une dissymétrie à droite.

\bigbreak

\underline{Formule alternative :} calculée à partir de la différence entre la moyenne et la médiane :
$$ m_{3} = 3 \times \bigg(\frac{\bar{x} - x_{0.5}}{\sigma}\bigg) $$

