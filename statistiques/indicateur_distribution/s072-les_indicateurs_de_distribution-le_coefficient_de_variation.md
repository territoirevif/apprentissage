---
title: "s072. Les indicateurs de distribution : le coefficient de variation"
subtitle: ""
author: Marc Le Bihan
keywords:
- indicateur de distribution
- coefficient de variation
- écart-type de population
- écart-type d'échantillon
- espérance
---

Le coefficient de variation se calcule ainsi, selon que l'on se base sur :

- son écart-type de population totale
   $$ \rouge{\text{c.v.}} = \frac{\sigma}{\mu} $$

\bigbreak

- ou son écart-type d'échantillon
   $$ \rouge{CV} = \frac{s}{\bar{x}} $$

