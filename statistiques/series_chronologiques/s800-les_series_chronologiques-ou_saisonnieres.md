---
title: "s800. Les séries chronologiques ou saisonnières"
subtitle: ""
author: Marc Le Bihan
---

## 1) Une série indexée par date

C'est une série qui est, de plus, indexée par date

   \demitab $\rightarrow$ il existe donc d'autres séries,  
      \tab avec d'autres valeurs,   
      \tab mais à d'autres dates.

Un des enjeux est de repérer les parties périodiques.

\bigbreak

L'objectif majeur de l'étude d'une série chronologique  
   \demitab est de faire des prévisions.

### Ttransformées de Fourier

### Lissage exponentiel, simple et double, 

La partie saisonnière d'une :

   - série 
   - tendance 
   - phénomène périodique

\bigbreak

se recherche avec des :

   - algorithmes de lissage, simples ou doubles
   - des transformées de Fourier pour identifier les périodes.

