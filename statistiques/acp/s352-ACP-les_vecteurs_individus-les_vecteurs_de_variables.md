---
title: "s352. L'Analyse en Composantes Principales (ACP)"
subtitle: "les vecteurs d'individus, de variables, et les nuages"
author: Marc Le Bihan
---

## 1) Les vecteurs d'individus

### a) Les composantes d'un vecteur d'individu sont les variables de cet individu

Une instance d'objet informatique est typiquement un individu.

Si l'on extrait les \bleu{n} variables membres de son instance

   \demitab elles forment un \definition{vecteur}  
   \demitab avec \bleu{n} composantes dedans

   \tab $\mauve{\overrightarrow{i_1}}\ (1.75\text{ m}, 72\text{ kg}, 25\text{ ans})$ $\implies$ $\mauve{\overrightarrow{i_1}} (1.80, 72, 25)$  
   \tab $\mauve{\overrightarrow{i_2}}\ (1.51, 32, 13)$  
   \tab $\mauve{\overrightarrow{i_3}}\ (1.69, 64, 57)$  

### b) Pour représenter graphiquement ces individus, il faut un axe par variable

Chaque individu a $\mauve{k}$ variables pour le décrire,  
   \demitab et se représente par un point  
   \demitab qui a autant de coordonnées que de variables.

\bigbreak

On le met sur un plan à deux dimensions s'il n'a que deux variables, par exemple.

   \demitab Avec $\mauve{\overrightarrow{i_1}}(1.80, 72)$, $\mauve{\overrightarrow{i_2}}\ (1.51, 32)$, $\mauve{\overrightarrow{i_3}}\ (1.69, 64)$ on mettrait :

   - $1.51$, $1.69$, $1.80$ sur l'axe des abscisses
   - $32$, $64$, $72$ sur l'axe des ordonnées

### c) Un individu moyen (imaginaire) devient l'origine des axes

On pourra mieux se représenter les individus les uns par rapport aux autres  

   \demitab si l'on forge un \definition{individu moyen}  
   \demitab à partir de tous les individus de la série

   \tab ici, ce serait $\rouge{\overrightarrow{i_{\text{moy}}}}\ (1.66, 56)$

\bigbreak

Et l'on fait de cet individu moyen aux coordonnées $(x, y)$ $(1.66, 56)$

   \demitab le centre de gravité $G$  
   \demitab ou l'origine $O$ des axes : ($O = G$)

de notre représentation graphique des individus :

   - $\overrightarrow{i_1}\ (1.80, 72)$ sera dans le quadrant en haut à droite
   - $\overrightarrow{i_2}\ (1.51, 32)$ sera dans le quadrant en bas à gauche
   - $\overrightarrow{i_3}\ (1.69, 64)$ sera dans le quadrant en haut à droite

## 2) Le nuage des individus

### a) Il représente chaque individu

Notre représentation graphique aura trois dimensions  
   \demitab si nous représentons trois variables de notre individu.

Et $\mauve{R^{k}}$ est l'\definition{espace vectoriel} à $\mauve{k}$ dimensions, en fin de compte.

   \begingroup \cadreAvantage

   > Les coordonnées du $5^{\text{ème}}$ individu \mauve{$i_{5}$}  
   > \tab d'une série qui décrit chaque individu avec \mauve{$\mauve{k} = 4$} variables,  \newline
   > 
   > seront, par exemple, \mauve{$\overrightarrow{i_{5}}\ (0.27, -0.48, 1.03, 1.25)$}

   \endgroup

### b) Beaucoup de dimensions => trop d'axes pour se donner une représentation graphique !

Comme il y a beaucoup d'axes,

   \demitab souvent plus de trois parce qu'on étudie souvent plus de deux variables,  
   \demitab les points se trouvent un peu n'importe où dans un espace à plusieurs dimensions :  
   \demitab c'est pour cela qu'on dit qu'ils sont ensemble dans un \definition{nuage}.

\bigbreak

Ca se perçoit très bien si on se donne un exemple à 3 variables = 3 dimensions.  
Oui, là, on voit parfaitement le nuage.

![Nuage des individus](statistiques/acp_nuage_individus.jpg)

Mais au delà de 3 dimensions, impossible de le représenter,  
\demitab et l'ACP va fournir des images planes de celles-ci,  
\demitab qui \attention{approchent} le mieux le nuage.

### c) La distance euclidienne des individus va dire leur ressemblance

C'est la distance euclidienne des individus qui est déterminante pour dire leur ressemblance.

Etudier ces __distances inter-individus__, c'est détecter dans le nuage : 

   - des \definition{partitions de points} (topologie)
   - des \definition{dimensions d'allongement remarquables} (dimensions de variabilité)

## 3) Les vecteurs de variables

### a) Extraits d'un ensemble d'individus ou une collection d'objets

De notre ensemble d'individus $\bleu{I}$ (ou notre collection d'objets informatiques)  
on extrait chacune de ses variables dans un vecteur dédié à cette variable :  

   \demitab par exemple, on rassemble tous les âges des individus

   \demitab $\rightarrow$ pour chaque variable, cela donne une suite de $\bleu{|I|}$ nombres   
   \demitab \ \ \ (autant qu'il y a d'individus)

   \demitab cela fait un vecteur à $\bleu{|I|}$ composantes :

   \tab $\mauve{\overrightarrow{\text{Age}}}\ (25, 13, 57)$  
   \tab $\mauve{\overrightarrow{\text{Taille}}}\ (1.75, 1.51, 1.69)$  
   \tab $\mauve{\overrightarrow{\text{Poids}}}\ (72, 32, 64)$

\bigbreak

Chaque dimension \underline{du vecteur} représente un individu.

### b) Le nuage des variables

Ainsi, le nuage des variables $\to$ un vecteur par variable

\bigbreak

L'\definition{espace vectoriel} de ce vecteur  
\demitab si l'on en avait besoin,  
\demitab serait : $\mauve{R^{I}}$

![](statistiques/nuage_des_variables_sur_une_sphere.jpg){width=60%}

