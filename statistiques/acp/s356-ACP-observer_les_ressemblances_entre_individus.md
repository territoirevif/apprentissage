---
title: "s356. L'Analyse en Composantes Principales (ACP)"
subtitle: "observer les ressemblances entre individus"
author: Marc Le Bihan
---

L'objectif des composantes principales est de mettre en évidence :

   \demitab __$\rightarrow$ Les ressemblances entre individus__  
   \tab Les liaisons entre variables

## 1) Exemple de question qui amène la recherche d'individus proches

   \begingroup \cadreAvantage

   > des ensembles de paramètres caractérisent-ils certaines stations de prélèvement ?  
   > \   
   >
   >   - à quelle type de station s'en rapproche une,  
   >     \demitab sachant telles valeurs observéees ?

   \endgroup

## 2) La distance Euclidienne

   \begingroup \cadreGris 

   > $\bleu{k} \in K$ : une variable associée aux individus  
   > $\bleu{i} \in I$ : un premier individu, comme comparande  
   > $\bleu{\ell} \in I$ : un deuxième individu, comme comparande  
   > \    
   > leur __ressemblance__ va être évaluée par leur \definition{distance euclidienne}
   > $$ \rouge{d^{2}(i,\ell)} = \sum\limits^{K}_{\bleu{k}=1}(x_{\bleu{i},\bleu{k}} - x_{\rouge{\ell},\bleu{k}})^{2} $$
   > c'est à dire __la somme des écarts au carré__   
   > \   
   > \demitab qu'il y a entre la valeur de l'individu $\bleu{i}$ et l'individu $\bleu{k}$,  
   > \tab pour chaque variable $k$ qui ddéfinit ces individus.  \newline 
   >
   > \underline{Remarque :}  
   > Très rarement, l'on attribue des poids différents $\bleu{m_{k}}$ aux variables,  
   > transformant l'expression ainsi : $d^{2}(i,\ell) = \sum\limits^{K}_{k=1}\bleu{m_{k}}(x_{i,k} - x_{\ell,k})^{2}$
 
   \endgroup

\bigbreak

   - Quelles sont leurs ressemblances, leurs différences ?
   - Peut-on faire une typologie des individus, en faisant apparaître des groupes homogènes ?

\bigbreak

   On cherche les principales __dimensions de variabilité__ des individus.

