---
title: "s358. L'Analyse en Composantes Principales (ACP)"
subtitle: "observer les liaisons entre DEUX variables"
author: Marc Le Bihan
---

L'objectif des composantes principales est de mettre en évidence :

   \tab Les ressemblances entre individus  
   \demitab __$\rightarrow$ Les liaisons entre variables__

\bigbreak

\underline{Remarque :} Ce chapitre est pour comprendre le principe de l'étude  

   \demitab car l'ACP ne devient réellement utile que s'il y a plus de deux variables :  
   \demitab sinon, le coefficient de corrélation \bleu{$r$} suffit.

## 1) Mettre en évidence la liaisons entre variables

### a) Calcul de r entre deux variables

   > \demitab $\bleu{k}$ : une variable associée aux individus  
   > \demitab $\bleu{h}$ : une autre variable associée aux individus  
   > \demitab $\rouge{r}$ : est le coefficient de corrélation linéaire
   > \   
   > $$ \begin{aligned}[t]
   > \rouge{r(k, h)} &= \frac{\text{covariance}(k,h)}{\sqrt{\text{variance}(k) \times \text{variance}(h)}} \\
   > \\
   >                 &= \frac{1}{[I]}\sum_{i \in I}\Big(\frac{x_{i,k} - \overline{x_{k}}}{s_{k}}\Big)\Big(\frac{x_{i,h} - \overline{x_{h}}}{s_{h}}\Big)
   > \end{aligned} $$
   > \    
   > \begingroup \small \tab (rappel: [I] est le cardinal de I) \endgroup

### b) Comment mettre en évidence des typologies de variables ?

   - Quelles variables sont corrélées positivement entre-elles ? Lequelles s'opposent négativement ?

      - ou aider à trouver des facteurs __latents__ : on ne peut mesurer directement ces variables, mais seulement voir leurs conséquences : anxiété, mémoire...

   - Peut-on mettre en évidence une typologie des variables ?

### c) Créer des classes d'individus, caractérisés par des variables

Pour relier la :

   - typologie des individus
   - typologie des variables

\bigbreak

on va caractériser des classes d'individus par des variables.  

   \begingroup \cadreGris

   > Par exemple : \newline 
   > 
   > - les individus d'une classe qui ont, pour des variables, des valeurs particulièrement petites ou particulièrement grandes.  
   > - ou des individus qui ont des valeurs particulièrement grandes (ou particulièrement petites) pour des variables corrélées positivement entre-elles.

   \endgroup \bigbreak

\underline{Idéalement :}

   - chaque groupe de variables caractérise un groupe d'individus
   - et chaque groupe d'individus rassemble les individus types d'un groupe de variables.

## 2) Représenter les corrélations entre DEUX variables sur un CERCLE

### a) Une représentation de r sur des données centrées réduites

Il s'agit d'une représentation, pour se faciliter l'interprétation, rien d'autre.  
Et \rouge{ne s'applique que si les données sont centrées réduites}.

\bigbreak

Elle se base sur $\bleu{r}$, le coefficient de _Pearson_, qui voit $r$ aller de $-1$ à $1$ suivant que :

   > $\mathbf{1}$ deux variables sont parfaitement corrélées
   >
   > $\mathbf{-1}$ si elles sont opposées
   >
   > $\mathbf{0}$, si elles ne sont pas corrélées
   >
   > et un autre nombre, compris entre -1 et 1, exclus, dans les autres situations intermédiaires.

### b) Des variables représentées par des vecteurs

Sur ce cercle, les __deux__ variables vont être représentées par des vecteurs qui :

   - ont tout deux pour origine l'origine du cercle,
   - et l'autre extrémité est sur le cercle des corrélation, 
   - tels que l'angle $\rouge{\alpha}$ que font ces __deux__ vecteurs vaut :
     $$ \rouge{\alpha} = \arccos{(r)} $$

\bigbreak

Et \avantage{c'est là, l'astuce} :

   \demitab c'est pour cela que les variables passent sur un cercle.  
   \demitab Parce que l'on transforme artificiellement   
   \tab leur coefficient de corrélation de _Pearson_ en un angle.

\bigbreak

![Représentation des corrélations des variables dans un cercle](statistiques/cercle-sphere-hypersphere-des-corrélations.jpg){width=60%}  
_(rappel : le grand diamètre et le petit diamètre, ici mentionnés, sont ceux d'oeufs)_

\bigbreak

Ainsi :

   > un $\bleu{r = 1}$ montrera deux vecteurs (= deux arcs) superposés,  
   > un $\bleu{r = -1}$, deux arcs opposés,  
   > un $\bleu{r = 0}$ deux arcs avec $90^{\circ}$ d'angle... ou bien $270^{\circ}$ : ce sera la même-chose.  

### c) Que devient ce cercle quand il y a plus de deux variables ?

(examiné au chapitre suivant)

\bigbreak

Trois variables ?

   \demitab $\rightarrow$ Ce ne sera plus un cercle, mais une sphère des corrélations

\bigbreak

Quatre ou plus ?

   \demitab $\rightarrow$ Une hypersphère

