---
title: "s354. L'Analyse en Composantes Principales (ACP)"
subtitle: "résumer des variables en composantes principales"
author: Marc Le Bihan
---

Les données d'une ACP sont quantitatives.  
Avant que leur analyse débute, elles doivent être centrées et souvent : réduites.

## 1) Centrage et réduction des données

### a) Centrage

En ACP, __les données seront toujours centrées__ 

   > on calcule $\begingroup \large \mauve{x_{ik} - \overline{x_{k}}} \endgroup$  
   > \demitab pour chaque valeur d'un individu pour une variable  
   > \demitab on soustrait la moyenne pour cette variable pour tous les individus.

### b) Réduction

Mais, \rouge{seulement centrée, l'ACP devient très sensible aux unités de mesure utilisées} :

   \demitab une variable exprimée en centimètres  
   \demitab et l'autre en mètres  
   \demitab va donner une influence de $100^{2}$ à celle en centimètres !

\bigbreak

L'on réduit donc les valeurs, à l'aide de :
$$ \begingroup \large \mauve{s^{2}_{k}} = \frac{1}{2|I|^2}\sum_{i,\ell}\big(x_{i,k} - x_{\ell,k}\big)^2 \endgroup $$

\begingroup \small \ \ \ (rappel: $I^{2}$ est une abréviation de $I \times I$, et $|I|$ son cardinal) \endgroup

### c) Quand les données ont toutes la même unité de mesure, ne pas les réduire est intéressant

Si les variables qui composent les individus ont toutes la même unité,  
   \demitab on peut souhaiter ne pas les réduire  
   \demitab et les variables se distingueront mieux par leur variance.

\bigbreak

De plus, nos composantes principales \avantage{auront aussi cette unité là}.

   \demitab $\rightarrow$ si les variables sont en toutes en mètres, et seulement centrées,  
   \demitab les composantes principales qui les résumeront seront aussi exprimées en mètres.

## 2) Résumer en un indicateur plusieurs variables d'un individu

L'Analyse en Composantes Principales va résumer des variables qui composent un individu

   \demitab en un indicateur  
   \demitab en réduisant ses dimensions

   \demitab $\rightarrow$ Ces \definition{composantes principales}  
   \tab sont de nouvelles variables dans notre \mauve{I $\times$ C}  
   \tab qui en résument d'autres

\bigbreak

En analysant plusieurs individus comme celui-ci :

\demitab $\mauve{\overrightarrow{i_1}}\ (1.67\text{ m}, 59\text{ kg}, 25\text{ ans}, 4 \text{\ étage}, 10\text{ séances de cinémas}, 14 \text{ jours de congés hors de son département})$
  
\bigbreak

elle pourra trouver, par exemple :

   - une composante principale \rouge{$\text{CP}_1$} __résumant__ $1.67\text{ m}, 59\text{ kg}, 25\text{ ans}$  
     \demitab parce que taille, poids et âge sont liés

   - une composante principale \rouge{$\text{CP}_2$} pour $10\text{ séances de cinémas}, 14 \text{ jours de congés hors de son département})$  
     \demitab $\rightarrow$ des loisirs vont se lier ensemble aussi

\bigbreak

Si la corrélation entre variables est forte  

   \demitab l'information contenue dans les variables liées est __redondante__,  
   \demitab et l'intérêt des composantes principales c'est qu'elle peut les remplacer.
 
   \begingroup \cadreAvantage

   > Existe t-il des arômes susceptibles d'apparaître simultanément ?

   \endgroup

## 3) Représentation des variables sur un axe

Un \definition{axe}, ça veut dire : une droite

### a) les composantes principales sont des combinaisons linéaires

Les composantes principales sont des combinaisons linéaires des variables originales.

   - la première a la plus grande variance
   - la seconde, la plus grande suivante  
     etc.

### b) La première composante principale est la droite de régression linéaire

Quand on a __deux__ variables, ce qui les résumera sera une droite de régression linaire  

   \demitab $\mauve{y = ax + b}$  
   \demitab et cette droite s'appellera \definition{la première composante principale} du nuage de points.

   > avec pour coefficients, dans le cas de deux variables :  
   >
   > \tab ${\bleu{x}}$ les valeurs de la première variable,  
   > \tab ${\bleu{y}}$ les valeurs de la seconde variable,  
   > $$ \begingroup \large \rouge{a} = \frac{s_{x}}{s_{y}} \endgroup $$
   > $$ \begingroup \large \rouge{b} = \overline{y} - a \overline{x} \endgroup $$

\bigbreak

   1. La droite de régression linéaire  
      minimise l'écart vertical entre les valeurs observées

\bigbreak

   2. La \definition{première composante principale}, elle :  
     \   
     minimise l'écart entre les valeurs observées  
     et la projection sur la droite __perpendiculaire__ à celle-ci.  
     Ici, devant des valeurs centrées réduites :

   ![ACP valeurs centrées-réduites](statistiques/acp_centré-réduit.png)

## 4) Puis, la seconde composante principale est l'axe perpendiculaire à la première

   3. La \definition{seconde composante principale}  
      est l'axe perpendiculaire à la première,  
      et qui passe par le \definition{barycentre} du nuage de points.

   4. On provoque alors une \definition{rotation}  
      pour que la $\text{CP}_1$ devienne l'axe des abscisses,  
      et la $\text{CP}_2$, celui des ordonnées.

\bigbreak

   ![ACP axe abscisse CP1 et ordonnée CP2 après rotation](statistiques/acp_cp1-cp2.png)

