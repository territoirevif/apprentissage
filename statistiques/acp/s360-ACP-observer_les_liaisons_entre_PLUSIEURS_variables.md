---
title: "s358. L'Analyse en Composantes Principales (ACP)"
subtitle: "observer les liaisons entre PLUSIEURS variables"
author: Marc Le Bihan
---

L'ACP ne devient réellement utile que s'il y a plus de deux variables.

   \demitab Sinon, le coefficient de corrélation \bleu{$r$} suffit.

\bigbreak

Quand il y a plus de deux variables :

   - Le nombre d'axes factoriels reste égal au nombre de variables
   - Chaque vecteur (c.à.d. variable) conserve pour origine, l'origine des axes

\bigbreak

Mais voici ce qui change :

## 1) Notre vecteur est maintenant dans une sphère, hypersphère, etc.

S'il y a trois variables dans notre analyse, le vecteur,  

   \demitab (dont les coordonnées ont été créées à partir de données centrées réduites pour la représentation),
   \demitab est maintenant dans une sphère, et non plus sur un cercle...

Et s'il y a quatre variables, sur une hypersphère, etc.

### a) On veut le re-projeter sur le cercle des corrélations

On projete chaque autre extrémité du vecteur

   \demitab (car son origine est l'origine des axes)  
   \demitab sur le plan

\bigbreak
 
Mais elle tombe alors rarement sur la bordure du cercle des corrélations.

### b) Trouver une variable qui peut servir à interpréter un axe

Un vecteur (c.à.d. variable) dont :

   1. l'angle par rapport à un axe est faible
   2. l'extrémité est proche de la bordure du cercle des corrélations

Marque une variable qui pourra servir à interpréter l'axe

### c) Vecteurs aux valeurs corrélées

Si plusieurs vecteurs ont des angles faibles entre-eux

   \demitab __ET__ que leur extrémité est proche de la bordure du cercle des corrélations  
   \demitab  alors c'est que leurs valeurs sont fortement corrélées

\bigbreak

Et si leurs angles sont proches de 180°  

   \demitab alors ils sont corrélés négativement

## 2) Les axes résument plus d'une variable

### a) La valeur propre d'un axe

Chaque axe renvoie une \definition{valeur propre}  

   \demitab si elle est supérieure à $1$  
   \demitab c'est qu'il résume plus d'une variable

\bigbreak

Ces valeurs propres disent donc combien d'axes il faut interpréter.

\bigbreak

   $\rightarrow$ La somme des valeurs propres  
   \tab est égale au nombre de variables

### b) Qualité de l'analyse : pourcentage de variance portée par chaque axe

Les valeurs propres indiquent le pourcentage de variance  
   \demitab (ou d'information)

portée par chaque axe.

\bigbreak

ce pourcentage évalue la qualité de l'analyse  
   \demitab en fonction du nombre d'axes sélectionnés

