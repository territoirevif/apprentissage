---
title: "s350. L'Analyse en Composantes Principales (ACP)"
subtitle: "ses objectifs"
author: Marc Le Bihan
---

## 1) Les différences entre l'ACP, l'AFC, l'ACM

L'Analyse en Composantes Principales (ACP) traite de tableaux  
   \demitab croisant des individus  
   \demitab et des variables __quantitatives__
 
\begingroup \small
 
quand l'Analyse Factorielle des Correspondances (AFC)  
   \demitab traite des tableaux de fréquences  

et l'Analyse des Correspondances Multiples (ACM)  
   \demitab ceux avec des individus  
   \demitab et variables __qualitatives__

\endgroup

\bigbreak

L'Analyse en Composantes Principales

  - cherche à résumer l'information multivariée
  - alors que les méthodes de classifications, elles, cherchent à la classer en groupes.

## 2) L'ACP cherche les variables corrélées

Elle cherche des groupes de variables intercorrélées

   \demitab sachant qu'ils peut y avoir plusieurs groupes de variables  
   \demitab liées à plusieurs autres groupes de variables :

   \tab $\mauve{A \sim D}$, tandis que $\mauve{B \sim E}$ et $\mauve{C \sim F}$   
   \tab avec $\mauve{A, B, C, D, E, F}$ des ensembles de variables

\bigbreak

\begingroup \cadreGris

> Quand les groupes de variables semblent avoir un facteur qui les détermine  
>    \demitab __mais que ce facteur n'est pas visible : on ne peut pas l'observer directement__  
>       \tab $\rightarrow$ on ne peut pas l'identifier  
> \     
>    \demitab il s'appelle \definition{variable latente} ou \definition{facteur latent}  
>    \tab parce qu'on peut juste voir ses conséquences.

\endgroup

## 3) Et observe comment les individus se comportent vis-à-vis de ces groupes de variables

L'ACP observe comment certains individus se comportent vis-à-vis de ces groupes de variables.

Ou bien, \avantage{si l'on prend une population différente} ?

Enfin, elle permet de voir quelles groupes de variables se rapprochent de celles non utilisées pour l'analyse.

