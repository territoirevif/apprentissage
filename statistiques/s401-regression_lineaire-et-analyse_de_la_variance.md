---
title: "s401. Régression linéaire et Analyse de la variance"
subtitle: "et calcul des résidus"

keywords:
- spark
- R
- régression
- régression linéaire
- analyse de la variance
- résidu
- variance expliquée
- calcul des résidus
- méthode des moindres carrés
- prédiction
- modèle
- régression polynomiale
- régression multiple
- régression des moindres carrés partiels
- régression logistique
- variables corrélées
- variable à prédire
- équation linéaire
- droite
- erreur de prédiction
- maximum de vraisemblance
- covariance
- coefficient de détermination
- coefficient de Beauvais-Pearson
- cor
- cov
- dispersion résiduelle
- variance des résidus
- variable dépendante
- interaction
- facteur secondaire

author: Marc Le Bihan
---


[Spark, R : détermination des coefficients $a$ et $b$ d'une régression linéaire par la méthode des moindres carrés](#regressionLineaire)
 
[Spark : calcul de la variance expliquée](#varianceExpliquee)

# I) Les différentes régressions

Les \definition{régressions} sont les premiers outils pour \definition{prédire} des valeurs.

Elles vont permettre, en faisant participer d'autres variables, de définir un \definition{modèle}.  

   > Il faut avoir détecté les variables qui ont une causalité avec ce qu'on veut prédire :
   >
   >   - c'est la __causalité__ dont il faut s'assurer : la corrélation ne suffit pas
   >   - et également sa direction

\bigbreak

\underline{La régression :}

   - \definition{linéaire} prédit une variable à partir d'une autre variable __unique__, où la relation entre les deux est escomptée linéaire
 
   - \definition{polynomiale} prédit aussi une variable à partir d'une autre, mais suppose la __relation non monotone__

   - \definition{multiple} prédit la variable à partir de __plusieurs__ autres

   - des \definition{moindre carrés partiels} prédit une variable à partir d'autres seulement __corrélées__

      - c'est à dire de variables latentes
      - ou virtuelles, inférées par plusieurs autres variables

   - \definition{logistique} prédit une variable binaire à partir de valeurs quantitatives

# II) La régression linéaire

Sur un graphique, la \avantage{variable à prédire} est sur l'axe des $y$ (ordonnée),  
et le \avantage{facteur} sur l'axe des $x$ (abscisses).

le \definition{modèle mathématique} sera une \definition{équation linéaire} : une droite.

L'écart entre la valeur réelle sur le graphe et celle qu'on prédit pour notre $\bleu{y}$ est représenté par un trait vertical pour chaque $\bleu{x}$ souhaité. Cet écart s'appelle le \definition{résidu}.

Pour trouver les paramètres $\bleu{a}$ et $\bleu{b}$ de l'équation $\bleu{y = ax + b}$, on peut utiliser :

   - la \definition{méthode des moindres carrés}, qui minimise le carré des erreurs de prédictions : 
     \begingroup \large $$ \bleu{cov_{xy}} = s_{xy} = \sum_{i=0}^n\limits\frac{(\textcolor{blue}{x_{i}}-m_{\textcolor{blue}{x}})(\textcolor{OliveGreen}{y_{i}}-m_{\textcolor{OliveGreen}{y}})}{n-1} $$ \endgroup
     \begingroup \large $$ \textcolor{red}{a} = \frac{cov_{xy}}{s^2_{x}} $$ \endgroup
     \begingroup \large $$ \textcolor{red}{b} = \overline{y} - \textcolor{blue}{a}\overline{x} $$ \endgroup

   - celle du \definition{maximum de vraisemblance}, qui minimise les écarts (et pas les carrés)

## Une régression linéaire avec Spark{#regressionLineaire}

   \code

   > \underline{Détermination des coefficients a et b de la droite de régression linéaire  
   > par la méthode des moindre carrés :}
   > \    
   > ```java
   > public void regressionLineaireMethodeMoindreCarres() {
   >    // Deux colonnes : glucid et insulin
   >    Dataset<Row> glucideEtInsuline = /* deux colonnes : glucid et insulin */
   >
   >    double cov = glucideEtInsuline.stat().cov("glucid", "insulin");
   >    LOGGER.info("covariance (glucides, insuline) = {}", cov);
   >
   >    double sd = glucideEtInsuline.selectExpr("stddev_samp(glucid)").head().getDouble(0);
   >    double varianceX = sd * sd;
   >    LOGGER.info("Ecart-type en x (glucides) = {} => variance = {}", sd, varianceX);
   >
   >    double a = cov / varianceX;
   >
   >    double moyenneGlucides = glucideEtInsuline.selectExpr("mean(glucid)").head().getDouble(0);
   >    double moyenneInsuline = glucideEtInsuline.selectExpr("mean(insulin)").head().getDouble(0);
   >    double b = moyenneInsuline - a * moyenneGlucides;
   >
   >    LOGGER.info("La droite de régression linéaire par la méthode des moindres carrés est : y = {}x + {}", a, b);
   > }
   > ```

   \endgroup

\bigbreak

\begingroup \small

\underline{à l'exécution :}

> ```txt
> covariance (glucides, insuline) = 249.2
> Ecart-type en x (glucides) = 16.18 => variance = 262.0
>
> La droite de régression linéaire par la méthode des moindres carrés est :
> y = 0.95x + 67.0
> ```

\endgroup

   > ![regression linéraire et résidus](images/regression-linéaire-résidus.jpg)

   \code

   > \underline{Son équivalent en R :}
   > \   
   > ```r
   > alim = read.table("Alim.txt", h = TRUE)
   >
   > covxy = cov(alim$glucid, alim$insulin)
   > varx = sd(alim$glucid)^2
   >
   > a = covxy / varx
   > b = mean(alim$insulin) - ((mean(alim$glucid) * (cov(alim$glucid, alim$insulin)/sd(alim$glucid)^2)))
   > ```

   \endgroup

# III) La variance expliquée et coefficient de détermination $R^2$

   > $\bleu{r}$ le coefficient de _Beauvais-Pearson_  
   > \    
   > \begingroup \large $\bleu{r} = \frac{cov_{xy}}{s_{x} \times s_{y}} = \frac{s_{xy}}{s_{x} \times s_{y}}$ \endgroup

\bigbreak

   > \begingroup \small En informatique, ce $\bleu{r}$ est renvoyé par `cor` (corrélation) qui retourne un résultat entre $-1$ et $1$  
   > plutôt qu'en manipulant `cov` qui n'a pas n'unités, renvoie des chiffres grands, obligeant une division. \endgroup
$$ \textcolor{red}{R^2} = r^2 $$
\tab $\rouge{R^2}$ est le \definition{taux de variance expliquée} (qu'on transforme en pourcentage)  
\tab il dit à quel point une variation de $\bleu{x}$ explique une variation de $\bleu{y}$  
\tab \avantage{S'il vaut 100\%, notre modèle est parfait !}

## Le calcul de la variance expliquée avec Spark {#varianceExpliquee}

   \code

   > \underline{Calcul de la variance expliquée $R^2$ :}
   > \   
   > ```java
   > double cor = glucideEtInsuline.stat().corr("glucid", "insulin", "pearson");
   > double r2 = cor * cor;
   > LOGGER.info("Le coefficient de corrélation : r^2 = {} (r = {})", r2, cor);
   > ```

   \endgroup

\bigbreak

\begingroup \small

\underline{à l'exécution :}

> ```txt
> Le coefficient de corrélation : r² = 0.446 (r = 0.668)
> (coefficient de corrélation = 44.6%)
> ```

\endgroup

# IV) Le calcul des résidus

La dispersion \definition{totale} de \bleu{$Y$} (= variance de $Y$), c'est :

   - la dispersion \avantage{expliquée} de \textcolor{OliveGreen}{$X$}
   - plus celle non expliquée de $X$ : la __dispersion résiduelle__ (= variance des résidus)

\bigbreak

   \code

   > \underline{Calcul des résidus :}
   > \      
   > ```java
   > double varianceTotaleY = glucideEtInsuline.selectExpr("var_samp(insulin)").head().getDouble(0);
   >
   > Column fonctionPrediction = (col("glucid").multiply(lit(a))).plus(b);
   > glucideEtInsuline = glucideEtInsuline.withColumn("prediction", fonctionPrediction);
   > double variancePrediction = glucideEtInsuline.selectExpr("var_samp(prediction)").head().getDouble(0);
   >
   > double residus = varianceTotaleY - variancePrediction;
   > LOGGER.info("Les résidus sont de {} sur une variance totale de {} qu'explique {} de X.", residus, varianceTotaleY, variancePrediction);
   > LOGGER.info("et {} / {} = {} la variance expliquée trouvée précédemment.", variancePrediction, varianceTotaleY, variancePrediction / varianceTotaleY);
   > ```

   \endgroup

\bigbreak

\begingroup \small

\underline{à l'exécution :}

> ```txt
> Les résidus sont de 294.4 sur une variance totale de 531.5 qu'explique 237.1 de X.
>
> et 237.1 / 531.5 = 0.446, la variance expliquée trouvée précédemment.
> ```

\endgroup

# V) L'analyse de la variance

On peut considérer que dans une analyse statistique : 

   - on résume une variable $\bleu{Y}$ à sa moyenne $\bleu{\mu}$  
   - où, lorsqu'il n'y a qu'une variable dépendante, agit notre facteur $\bleu{F}$, individu par individu, c'est à dire : $\bleu{F_{i}}$  
   - et quand il y en a plusieurs, par exemple $\bleu{F}$ et $\bleu{G}$, il faut aussi considérer leur interaction $\bleu{F \times G}$
   - un certain nombre de facteurs secondaires : $\bleu{\psi}$, qui vont eux aussi augmenter la variabilité intra-groupe. La randomisation peut les contrôler  
   - et un résidu, $\bleu{\epsilon}$ (qui peut aussi, parfois, être dû à une erreur de mesure).

\bigbreak

Dans une analyse à une seule variable, la forme du modèle prédisant la valeur d'une variable pour un individu est :
$$ \begingroup \large \textcolor{red}{Y_{i}} = \mu + F_{i} + [\psi + \epsilon] \endgroup $$

dans une analyse à deux variables :
$$ \begingroup \textcolor{red}{Y_{i}} = \mu + F_{i} + G_{i} + \bleu{F_{i} \times G_{i}} + [\psi + \epsilon] \endgroup $$

