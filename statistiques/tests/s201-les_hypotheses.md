---
title: "s201. Les hypothèses"
subtitle: "et la p-value"
author: Marc Le Bihan
category: statistiques
keywords:
- hypothèse
- p-value
- relation entre variables
- comparaison de valeurs
- facteur
- hypothèse nulle
- hasard
- hypothèse non nulle
- hypothèse opérationnelle
- H0
- erreur de type I
- erreur de première espèce
- échantillonnage
- risque d'erreur alpha
- booléen
---

Une hypothèse, __c'est une relation entre deux variables au moins__.
 
   > "_Je crois que. Ai-je raison ?_"  
   > \avantage{Une hypothèse est correcte si elle conduit à une comparaison de valeurs}.

\bigbreak

Une hypothèse a souvent __une seule variable__. Rarement deux ou trois simultanément.

On fait une hypothèse parce qu'on aimerait  
   \demitab mettre en évidence  
   \demitab le rôle d'un facteur particulier  
dans des données.

\bigbreak

Et pour cela, on utilise deux hypothèses :

## 1) L'hypothèse nulle, qui dit : "vous pourriez avoir votre résultat par hasard : il ne conclut rien"

La première hypothèse est l'hypothèse \definition{nulle $\mathbf{H_0}$} qui dit :
 
   > \avantage{"\emph{Non, ce n'est pas votre facteur qui agit, c'est seulement le hasard}"}

\bigbreak

et à cette hypothèse nulle est associée la notion de \definition{p-value}  
(appelée aussi "_risque d'erreur alpha_")  
qui donne la chance que l'évènement soit lié au hasard.

\bigbreak

   > \underline{Remarque :} le risque d'erreur \definition{$\mathbf{\alpha}$} ou \definition{"erreur de type I"} ou \definition{"erreur de première espèce"} peut parfaitement survenir alors qu'un a deux populations totales de grands effectifs de même moyennes et variablités !
   > 
   > car les séries d'échantillonnages et de tests statistiques que l'on répète finissent toujours par fonder à un moment des échantillons "exceptionnels" qui cassent l'hypothèse nulle...
   >
   > C'est que que la __p-value__ décrit : le risque que cette erreur apparaisse, par \underline{l'effet de l'échantillonnage}.
   >
   > \attention{La statistique, le fait de procéder par des échantillons, introduit des erreurs même là où il n'y en avait pas avant !}

## 2) L'hypothèse opérationnelle (ou non-nulle) : qui met en évidence le fait qu'on veut établir

L'hypothèse non nulle ou \definition{opérationnelle}, c'est le fait que l'on voudrait établir.

Celui qu'on voudrait dire vrai, et qui monterait que notre facteur est bien impliqué dans l'observation que l'on a faite.

   - parce que son apparition provoque des changements de valeurs visibles.
   - et l'on peut préciser, si on le désire, dans quel sens (__>__ ou __<__).

\bigbreak

\avantage{Intérêt :} Si mon hypothèse opérationnelle est retenue,  
\demitab alors je peux mettre un booléen à \fonction{true} dans mes données  
\demitab pour dire qu'un fait est établi : celui-là.

## 3) Choix de p-value

Typiquement, on fixe un seul de __p-value__ à 5% pour l'hypothèse nulle.

   > c'est à dire qu'on dit si cette _p-value_ est inférieure à 5%  
   > c'est que le hasard a moins de 5% de chances de provoquer cette situation
   > 
   > $\rightarrow$ Ce qui nous conforte dans notre idée  
   > \demitab que l'on peut promouvoir notre \definition{hypothèse opérationnelle}  
   > \demitab (celle non nulle, de changement)  
   > \demitab et rejeter l'hypothèse nulle.

