---
title: "s208. Les tests bilatéraux et unilatéraux"
subtitle: "détecter une différence, et aller jusqu'à en donner la direction"
author: Marc Le Bihan
---

Un test \definition{bilatéral} détecte si un facteur  
   \demitab est la cause d'un changement

un test \definition{unilatéral} est capable, de plus,  
   \demitab d'en dire la direction, le sens.

## 1) Le test bi-latéral, ou hypothèse faible

Nous voulons savoir si notre facteur agit, quel qu'en soit le sens. $\mauve{m_1 \neq m_2}$

   > Un test bilatéral à $5\%$ d'erreur $\alpha$,  
   > \demitab c'est en fait $2.5\%$ de chaque côté...

Lorsque des logiciels donnent une __p-value__, elle est bi-latérale.

## 2) Le test unilatéral, ou hypothèse forte

Nous voulons savoir si notre facteur agit, dans un sens particulier,  
et décidons avant si nous voudrons détecter $\mauve{m_1 > m_2}$ ou $\mauve{m_1 < m_2}$.

   > Un test unilatéral à $5\%$ d'erreur $\alpha$, c'est $5\%$ d'un seul côté.

