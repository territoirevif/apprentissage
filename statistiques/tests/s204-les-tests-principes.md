---
title: "s204. Les tests"
subtitle: "leurs principes"
author: Marc Le Bihan
---

## 1) Définition

Un \definition{test} confronte une \definition{hypothèse}  
sur des paramètres théoriques de la population  
   \demitab avec la réalité observée (l'échantillon).  

Puis, l'on __décide__.

## 2) Exemple

Un groupe va avoir un traitement, un autre non :  
Comment savoir si un facteur agit ?  

$\rightarrow$ On fait une moyenne dans chacun des groupes,  
et ces deux moyennes ayant des chances d'être différentes, l'on se demande :

   > "\emph{\bleu{La nouvelle moyenne que je vois, aurait-elle pu apparaître toute seule, si je n'avais pas donné de traitement au groupe qui l'a reçu ?}}"
   >
   >   - si oui, je ne peux rien conclure. 
   >
   >   - si non : l'effet du hasard dans ce changement est très peu probable, et je peux l'attribuer à mon traitement (c'est à dire : à mon facteur).

\bigbreak

Une \definition{statistique d'un test} c'est une valeur calculée par une formule  
   \demitab qui évalue la probablité d'avoir le résultat observé dans un échantillon  
   \demitab par le seul effet du hasard.

