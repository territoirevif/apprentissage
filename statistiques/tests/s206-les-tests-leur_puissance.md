---
title: "s206. La puissance d'un test"
subtitle: "risque alpha, risque beta"
author: Marc Le Bihan
---

[R : Calcul de la puissance d'un test](#puissance-test)

[R : Déterminer la taille de l'échantillon requis pour atteindre un risque alpha donné](#echantillon-minimal)

---

## 1) La capacité d'un test à détecter l'effet d'un facteur : sa puissance

La capacité d'un test statistique  
   \demitab à détecter l'effet d'un facteur  
   \demitab s'appelle \definition{la puissance du test}.

Il dépend de son amplitude, effectif, dispersion...

\bigbreak

La puissance du test augmente si :

   - le delta des moyennes demandées augmente : un gros écart est plus facile à détecter
   - l'effectif de l'échantillon augmente
   - si l'on accepte un taux d'erreur plus grand
   - ou que la variabilité de l'échantillon se réduit

\bigbreak

Si un test est imparfait, il peut déclarer :

   - qu'un facteur a un effet, alors qu'il n'en a pas : le risque alpha ($\alpha$)
   - qu'un facteur n'a pas d'effet, quand il en a un : le risque beta ($\beta$).

## 2) Le manque de puissance d'un test (risque beta)

Un test pourrait déclarer sur un échantillon qu'un facteur n'a pas d'effet, quand il en a un...

C'est \definition{"l'erreur de type II"}  
ou \definition{"erreur $\mathbf{\beta}$"}  
ou \definition{"erreur de deuxième espèce"} :  
elle amène à déclarer qu'un résultat n'existe pas, quand il est bien là.

   > Dans ce cas, un test __a manqué de puissance__.  
   > En théorie, un test statistique rigoureux ne devrait jamais amener une erreur de type II.

\bigbreak

| Ci-dessous: ce que l'on croît et décide      | Réalité : $H_0, \overline{H_1}$                | Réalité : $\overline{H_0}, H_1$ |
| -------------------------------------------- | ---------------------------------------------- | ------------------------------------------------------- |
| On rejette $H_0$ (le facteur a un effet)     | \danger{erreur de type I, risque $p = \alpha$} | \avantage{décision correcte, puissance $p = 1 - \beta$} |
| On ne rejette pas $H_0$ (facteur sans effet) | \avantage{décision correcte}                   | \attention{erreur de type II, risque $p = \beta$}       |

## 3) Le calcul de la puissance d'un test{#puissance-test}

La \definition{puissance d'un test} :  
   \demitab sa capacité à déclarer __significatif un effet réel__ du facteur  
   \demitab est définie par $\mathbf{1 - \beta}$

\bigbreak

Pour calculer cette puissance, il faut avoir :

   > $\bleu{n}$ : Le nombre d'éléments de l'échantillon
   >
   > $\bleu{\Delta m}$ : La différence de moyenne attendue, quand on va comparer les deux échantillons  
   > $\bleu{\alpha}$ : (le risque $\alpha$) Le taux d'erreur que l'on est prêt à accepter  
   > $\mauve{sd}$ L'écart-type de l'échantillon :
   >
   >  > Pour le test $t$ de Student, ce sera :
   >  $$ \mauve{sd} = \sqrt{\frac{\sum(x - m_x)^2 + \sum(x - m_y)^2}{(n_x - 1) + (n_y - 1)}} $$

   > \textbf{\underline{Sous R :}}  
   > \methode{power.t.test}{(n, delta, alpha, ecart-type)} va calculer cette puissance.

\bigbreak

\underline{R :} ici, la puissance du test lui donne $47 \%$ de chances de détecter un écart de moyenne de $20$ unités entre ddeux groupes, à $5\%$ de risque d'erreur

   \code

   > ```R
   > power.t.test(n=35, delta=20, sd=43.72, sig.level=0.05) # = 0.470769
   > ```

   \endgroup

## 4) Quelle taille minimale d'échantillon utiliser pour une expérimentation ?{#echantillon-minimal}

### a) Déterminer la taille de l'échantillon

En utilisant la formule de calcul de puissance d'un test  
\demitab on peut extraire une valeur de $n$ :

   > cette fois-ci, je remplace le paramètre \bleu{$n$}  
   > par la \bleu{puissance cible} que je voudrais avoir.  
   > \demitab Exemple : 95\% de chances de détection de l'écart

   > $\bleu{power}$ : La chance de détection de l'écart que l'on voudrait. Exemple : 95\%.  
   > $\bleu{\Delta m}$ : La différence de moyenne attendue, quand on va comparer les deux échantillons  
   > $\bleu{\alpha}$ : (le risque $\alpha$) Le taux d'erreur que l'on est prêt à accepter  
   > $\mauve{sd}$ L'écart-type de l'échantillon :
   >
   >  > Pour le test $t$ de Student, ce sera
   >  $$ \mauve{sd} = \sqrt{\frac{\sum(x - m_x)^2 + \sum(x - m_y)^2}{(n_x - 1) + (n_y - 1)}} $$

\underline{R :} pour être certains qu'un test peut détecter $20$ unités d'écart de moyenne dans $95\%$ des cas, il faut que l'échantillon ait au moins $126$ individus.

   \code

   > ```R
   > power.t.test(delta=20, sd=43.72, sig.level=0.05, power=0.95) # $n = 125.16$
   > ```

   \endgroup

### b) Cas d'utilisation : peut-on montrer qu'un facteur n'a pas d'effet ?

En théorie, montrer qu'un facteur n'a pas d'effet n'est pas possible...   
   \demitab mais l'on peut se débrouiller pour montrer que la probabilité qu'un facteur ait un effet  
   \demitab est inférieur à une valeur utile.

   \begingroup \cadreAvantage

   > Le sirop qui ne réduit la toux que d'une personne sur mille.

   \endgroup

\bigbreak

On calculera l'écart de moyenne qui rendrait le test intéressant.  
Puis, le risque $\alpha$ choisi, on calcule la puissance recherchée $1 - \beta$.

