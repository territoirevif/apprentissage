---
title: "s210. Les tests non paramétriques"
subtitle: "Test de Mann-Whitney, Wilcox"
author: Marc Le Bihan
---

# I) Pourquoi et quand utiliser un test non paramétrique ?

## 1) Situations d'emploi des tests non paramétriques

### a) Les atouts respectifs des tests paramétriques et non paramétriques

Les tests __paramétriques__ sont les plus __puissants__ :  
   \demitab ils sont les mieux capables de déclarer significatif un éffet réel d'un facteur.  
   \demitab C'est à dire, d'être justes.

   \demitab Mais ils ne conviennent que pour les variables quantitatives !  
   \tab $\rightarrow$ Si elles sont qualitatives, ratios ou rangs, ils ne peuvent plus être utilisés.

\bigbreak

Ceux __non paramétriques__, pour leur part, __résistent mieux aux valeurs extrêmes__.

   \demitab en particulier quand on compare des individus dont les valeurs seront de toutes façons très différentes,  
   \demitab parce que mélangeant plusieurs groupes déjà.

   \begingroup \cadreAvantage \small
   
   > Des tomates dans un échantillon qui ont des diamètres différents entre variétés.
   
   \endgroup

### b) Les situations d'emploi des tests non paramétriques

On les emploie aussi :

   - Quand le nombre de données est faible ($n < 30$).
   - Quand la distribution est inconnue,
   - ou qu'elle n'est pas normale, alors qu'elle l'est habituellement.
   - Pour comparer des distributions de données quantitatives,
   - ou des ratios.
   - Pour tester des co-occurences d'évènements,
   - ou l'indépendance entre variables quantitatives.

et plus rarement :

   - Pour tester des variables quantitatives brutes, quelle que soit leur distribution,  
   - ou quand leur distribution n'est pas pertinente au regard de la question posée  
    (test de la médiane, test de \mauve{\emph{Walsh}}).  
   - Pour traiter des données circulaires (statistiques circulaires, test de \mauve{\emph{Rayleigh}}).

## 2) Principe de fonctionnement d'un test non paramétrique

Le test non paramétrique fournit une __p-value__, tout comme un test paramétrique, lui disant "_la probabilité d'obtenir un résultat au moins aussi important que celui qu'il observe par le seul fait du hasard_".

Il y a deux types de tests non paramétriques différents :

### a) Tests pour les variables en rang

   Ils se fondent sur le fait que la somme des rangs d'un ensemble de valeurs d'effectif donné est constante.  
   Mmais il faut traiter les cas d'ex-aequo.

### b) Tests pour les variables qualitatives et les ratios

   > - Ils comparent, en général, des fréquences observées à des fréquences attendues (ou "_théoriques_"),  
   > - On calcule une distance du $\chi^2$ (Khi-Deux)  
       $\rightarrow$ cette étape "_normalise la variable_", quel que soit son type de donnée, en lui donnant une métrique qui suit les loi du $\chi^2$.
   >
   > (Remarque : ces tests de $\chi^2$ s'appliquent déjà naturellement aux données sous forme de ratios ou fréquences).

# II) Les tests non-paramétriques par rangs

   \begingroup \cadreAvantage \small

   > Les conditions d'ombre alternée ou continue ont-elles un effet sur la croissance des végétaux ?  
   > $\rightarrow$ la taille des végétaux est bien un nombre, mais pas le statut d'ombre ou ombre alternée.

   \endgroup

\bigbreak

On va s'appuyer sur le __rang relatif__ qu'on les individus dans leur échantillon, et plus directement eur la valeur que le critère a pris.

## 1) Le test U de Mann-Whitney 

C'est un test de comparaison de deux échantillons par rangs.

Soient les deux séries de $9$ individus (plantes) :

   > $C_{1} = \{2.9, 3.1, 2.2, 2.9, 2.8, 2.8, 3.1, 3.0, 2.3\}$ ombre alternée naturelle  
   > $C_{2} = \{2.2, 2.2, 2.3, 2.2, 2.9, 2.1, 2.3, 2.8, 2.1\}$ ombre complète

\bigbreak

1. On ordonne par valeurs des __deux séries__ \danger{\underline{regroupées}}

|             |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| ----------- | --: | --: | --: | --: | --: | --: | --: | --: | --: | --: | --: | --: | --: | --: | --: | --: | --: | --: |
| __Valeurs__ | 2.1 | 2.1 | 2.2 | 2.2 | 2.2 | 2.2 | 2.3 | 2.3 | 2.3 | 2.8 | 2.8 | 2.8 | 2.9 | 2.9 | 2.9 | 3.0 | 3.1 | 3.1 |
| __Rangs__   |   1 |   2 |   3 |   4 |   5 |   6 |   7 |   8 |   9 |  10 |  11 |  12 |  13 |  14 |  15 |  16 |  17 |  18 |

\bigbreak

2. Il y a des valeurs __ex-aequo__.

   On les traite en prenant la moyenne de leurs rangs :  
   exemple : les quatre valeurs $2.2$ successives ont pour rangs : $3, 4, 5, 6 \rightarrow \frac{3+4+5+6}{4} = 4.5$ 

|             |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |      |      |
| ----------- | --: | --: | --: | --: | --: | --: | --: | --: | --: | --: | --: | --: | --: | --: | --: | --: | ---: | ---: |
| __Valeurs__ | 2.1 | 2.1 | 2.2 | 2.2 | 2.2 | 2.2 | 2.3 | 2.3 | 2.3 | 2.8 | 2.8 | 2.8 | 2.9 | 2.9 | 2.9 | 3.0 | 3.1  | 3.1  |
| __Rangs__   | 1.5 | 1.5 | 4.5 | 4.5 | 4.5 | 4.5 |   8 |   8 |   8 |  11 |  11 |  11 |  14 |  14 |  14 |  16 | 17.5 | 17.5 |

\bigbreak

3. Alors, on remet ces rangs corrigés dans leurs échantillons, et l'on calcule la somme de ces rangs
 
   > $C_{1} = \{2.2, 2.3, 2.8, 2.8, 2.9, 2.9, 3.0, 3.1, 3.1\}$ ombre alternée naturelle  
   > $R_{1} = \{4.5, 8, 11, 11, 14, 14, 16, 17.5, 17.5\}$ dont la somme vaut $\mathbf{113.5}$  
   > $n_{1} = 9$
   >
   > $C_{2} = \{2.1, 2.1, 2.2, 2.2, 2.2, 2.3, 2.3, 2.8, 2.9\}$ ombre complète  
   > $R_{2} = \{1.5, 1.5, 4.5, 4.5, 4.5, 8, 8, 11, 14\}$ où elle vaut $\mathbf{57.5}$  
   > $n_{2} = 9$

> \underline{Remarque :}
>
> > Pour contrôle, la somme des rangs des deux séries regroupées  
> > (qui ensemble ont n=18 membres)  
> > est celle d'une suite arithmétique de 18 termes. 
>
> > $S = \frac{18 \times 19}{2} = 171$  
> > C'est bien ce que l'on a avec $113.5 + 57.5$

\bigbreak

4. On calcule les __U__ pour chaque échantillon
$$ \rouge{U_{i}} = R_{i} - \frac{n_{i}(n_{i} + 1)}{2} \text{, c'est à dire :} $$

   > > $U_{1} = R_{1} - \frac{n_{1}(n_{1} + 1)}{2} = 113.5 - \frac{9 \times 10}{2} = \mathbf{68.5}$
   > >
   > > $U_{2} = R_{2} - \frac{n_{2}(n_{2} + 1)}{2} = 57.5 - \frac{9 \times 10}{2} = \mathbf{12.5}$

\bigbreak

5. On affecte à $\mathbf{U_{0}}$ la valeur __la plus basse__ de tous les $U_{i}$ :

   $U_{0} = 12.5$

\bigbreak

6. On regarde dans une table de \mauve{\emph{Mann-Whitney}} au risque de x\% que l'on désire, l'hypothèse unilatérale ou bilatérale que l'on veut.  

   Exemple : 

      > pour un test bilatéral au risque de 5\%, pour nos échantillons $n_{1} = 9, n_{2} = 9$,  
      > la table nous répond : $\mathbf{U_{table} = 17}$

\bigbreak

   \begingroup \large \definition{$U_{0} < U_{table}$} \endgroup :  
   on peut rejeter l'hypothèse nulle et retenir l'hypothèse opérationelle, en déclarant qu'il y a bien une différence entre les deux échantillons $C_{1}$ et $C_{2}$.

\bigbreak

\underline{Remarque :}

   > Une autre méthode, avec une autre table, prend la valeur maximale des $U_{i}$.
   > et c'est cette méthode qui est utilisée par \definition{Wilcox}.

