---
title: "s230. Les plans d'étude et de test"
subtitle: ""
author: Marc Le Bihan
---

## 1) Le plan d'étude va décrire les tests probants

Un plan d'étude peut être :

   - en séquence (en étoile) 
   - ou simultané (factoriel ou multifactoriel).

\bigbreak

Il faut neutraliser les facteurs secondaires  
   \demitab ou s'assurer que chaque groupe étudié ait exactement les mêmes  
   \demitab ou qu'ils y soient parfaitement aléatoires.

