---
title: "s215. Le test du Khi-Deux"
subtitle: "Indépendance ou adéquation"
author: Marc Le Bihan
---

## 1) Pour tester l'indépendance de variables qualitatives

Quand les variables à tester ont des modalités, et plus des valeurs numériques.

\bigbreak

\avantage{Tester l'indépendance, c'est tester l'hypothèse nulle : déclarer que ce que l'on voit peut être attribuable au hasard.}

\bigbreak

Le principe du test va être pour chaque variable $\bleu{A}$ et $\bleu{B}$ faites de modalités :

### a) Calcul de la valeur théorique, comme si l'on était indépendants

À partir de :

   - \bleu{$N$} : l'effectif total de la population 
   - \bleu{chaque couple ($a_{i,j}$, $b_{i,j}$)} (avec $i$ : ligne, $j$ : colonne)  
     il correspond à une situation théorique possible

      - on prend l'effectif total de sa ligne \bleu{$n_{j}$} et l'effectif total de sa colonne \bleu{{$n_{i}$}}  

      - et on calcule la __valeur théorique__ qu'on devrait trouver __si seul le hasard jouait__ :  
        elle serait en proportion des deux totaux $n_{i}$ et $n_{j}$
        $$ \mauve{\text{valeur\_théorique}_{i,j}} = \frac{n_{i} \times n_{j}}{N} $$

### b) Calcul de la différence entre les valeurs théoriques et celles observées : le $\chi^{2}$ 

   > \bleu{$\text{valeur\_theorique}_{i,j}$} : les valeurs théoriques qu'on trouverait  
   > \demitab pour chaque combinaison $(a,b)$ possible,  
   > \demitab si les variables étaient indépendantes (si seul le hasard jouait)
   >
   > \bleu{$\text{valeur\_observée}_{i,j}$} : les valeurs qu'on observe actuellement (celles de l'expérience).
   >
   > \bleu{\text{C}} : nombre de couples ($a, b$) possibles avec les modalités de $A$ et de $B$.  
   > \tab le nombre de lignes multiplié par le nombre de colonnes  
   > \tab ou le nombre de modalités de $A$ par le nombre de modalités de $B$.  
   > \tab (si un tableau de contigence est utilisé, c'est son nombre de cellules).  
   
### c) Un tableau de contigences et le calcul de son le $\chi^{2}${#contingence-khi-deux}

Ce tableau a $C = 9$ cellules :

   | Modalités de A et B | $b_{1}$   | $b_{2}$   | $b_{3}$   |
   | :-----------------: | --------- | --------- | --------- |
   | $a_{1}$             | $n_{1,1}$ | $n_{1,2}$ | $n_{1,3}$ |
   | $a_{2}$             | $n_{2,1}$ | $n_{2,2}$ | $n_{2,3}$ |
   | $a_{3}$             | $n_{3,1}$ | $n_{3,2}$ | $n_{3,3}$ |

\begingroup \Large $\rouge{\chi^2} = \sum\limits_{b=1}^{C}\Big[\frac{(\text{valeur\_theorique}_{c} - \text{valeur\_observée}_{c})^2}{\text{valeur\_theorique}_{c}}\Big]$ \endgroup

### d) Confrontation du $\chi^{2}$ à ce qu'il devrait supporter si les variables étaient indépendantes

   On va confonter ce \donnee{$\chi^2$} à ce qu'il supporterait si nos variables étaient intépendantes.  
   le paramètre du $\chi^2$ c'est un nombre de degrés de liberté.
   
   > Le nombre de \definition{degrés de libertés (ddl)} c'est la quantité minimale d'information requise pour définir des données.

   >  > C'est le nombre total de possibilités d'observations (de classe),  
   >  > \demitab diminué du nombre d'estimations de paramètres de la loi,  
   >  > \demitab et diminué encore de 1.
   >  > 
   >  > en pratique, pour le test du $\chi^2$, c'est :  
   >  > $\rouge{\text{ddl}} = (\text{nombre de modalités de A} - 1) \times (\text{nombre de modalités de B} -1)$
   >  >
   >  > ou $\rouge{\text{ddl}} = {(\text{nombre de colonnes} - 1) \times (\text{nombre de lignes} - 1)}$,  
   >  > si on utilise un tableau de contingences.

## 2) Pour tester l'adéquation/conformité d'un échantillon à une distribution

Il s'agit, là encore, de vérifier que ce que l'on observe est bien ce que l'on devrait théoriquement voir.

 | Classe (en cm)       | $\leq 165$ | $]165, 170]$ | $]170, 175]$ | $]175, 180]$ | $]180, 185]$ | $\ge 185$ |
 | -------------------- | :--------: | :----------: | :----------: | :----------: | :----------: | :-------: |
 | Effectifs observés   | $9$        | $29$         | $38$         | $41$         | $20$         | $8$       |
 | Effectifs théoriques | $13.18$    | $25.30$      | $39.26$      | $38.07$      | $23.07$      | $11.13$   |

On a calculé la moyenne ($174.35$) et l'écart type ($7.09$) de notre échantillon (les observés),  
et l'on dit : "_\bleu{Comment devrait être la distribution si elle était $X \sim N(\mu=174.35, \sigma=7.09)$}_" ?

Ici, le nombre de paramètres est égal à \textbf{\underline{deux}} (la moyenne et l'écart-type),  
et le nombre de ddl à prendre en compte pour la loi du $\chi^2$ est : $6 - 2 - 1 = 3$

\bigbreak

Quand on fait le calcul vu ci dessus, $\chi^2 = \sum\limits_{c=1}^{C}\Big[\frac{(\text{valeur\_theorique}_{c} - \text{valeur\_observée}_{c})^2}{\text{valeur\_theorique}_{c}}\Big]$ :

   > $\chi^2 = \frac{(13.18-9)^2}{13.18} + \frac{(29-25.30)^2}{25.30} + \frac{(39.26-38)^2}{39.26} + \frac{(38.07-41)^2}{38.07} + \frac{(23.07-20)^2}{23.07} + \frac{(11.13-8)^2}{11.13} = \mauve{3.421}$

\bigbreak

on le confronte à un résultat qui dirait que si :

  - on suit une loi du $\chi^2$ de 3 ddl,
  - et qu'on cherche un taux d'erreur à 5\% (soit $1-\alpha = 0.95$)

\bigbreak
 
\textbf{on devrait dépasser un $\chi^2$ de 7.815}, pour rejeter $H_{0}$,  
l'hypothèse que l'on suit une loi normale.

Ce n'est pas le cas.  
On ne rejette pas $H_{0}$, et nous déclarons que notre échantillon suit une loi normale.

## 3) Rappel de propriétés sur l'indépendance de deux variables

__Si__ X et Y sont indépendantes  
    $\rouge{E(X . Y)} = E(X) . E(Y)$   

\bigbreak

__Si__ X et Y sont indépendantes  
    $\rouge{V(X + Y)} = V(X) + V(Y)$  
__sinon__  
    $\rouge{V(X + Y)} = V(X) + V(Y) + 2 cov(X, Y)$  

