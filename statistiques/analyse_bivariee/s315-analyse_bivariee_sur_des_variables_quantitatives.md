---
title: "s315. Analyse bivariée sur des variables quantitatives"
subtitle: ""
author: Marc Le Bihan
---

## 1) Distributions marginales et conditionnelles

### a) Distribution marginales

Les séries marginales sont celles sur les axes \mauve{D.M.(X)} et \mauve{D.M.(Y)}  
   \demitab et l'on peut en faire une analyse univariée sur ces axes.

   - L'indice des valeurs \bleu{$x$} s'appelle \mauve{$j$} (allant de $1$ à \mauve{$J$})  
     celui des valeurs \bleu{$y$}, \mauve{$k$} (allant de $1$ à \mauve{$K$}) :  
     \demitab \mauve{$J$} et \mauve{$K$} sont les nombres de valeurs distinctes que l'on peut trouver dans chaque série.  

   - on peut aussi utiliser des fréquences marginales  
     $\mauve{f_{j} = \frac{n_{j,}}{n}}$ et $\mauve{f_{k} = \frac{n_{,k}}{n}}$.  

### b) Distributions conditionnelles et profils

Comment pour une valeur ${\mauve{x_{j}}}$ (exemple : ${x_{4}}$) se distribuent les valeurs $\mauve{y_{4,1}}$ à $\mauve{y_{4,K}}$ ?
   
   - Pour un ${\mauve{x_{j}}}$, le \definition{profil ligne}  
    présente toutes le nombre d'occurrences ($n_{j,1}$, $n_{j,2}$, $n_{j,3}$, $n_{j,4}$...)  
    et termine par un total : \begingroup \large $\mauve{{n_{j,}}}$ \endgroup  

   - Pour un ${y_{k}}$, le \definition{profil colonne}  
    présente toutes le nombre d'occurrences ($n_{1,k}$, $n_{2,k}$, $n_{3,k}$, $n_{4,k}$...)  
    et termine par un total : \begingroup \large $\mauve{{n_{,k}}}$ \endgroup  

### c) Exploitation graphique des profils lignes et colonnes

Sur ces profils lignes et colonnes, l'on peut faire aussi une analyse univariée    
   \demitab pour étudier leur position, dispersion, forme...  
   \demitab $\rightarrow$ elle peut aussi aider à détecter des associations 

\bigbreak

On peut faire des diagrammes en barre de distribution conditionnelle

   \demitab $\mauve{D.C.(X|Y = 2)}$ $\rightarrow$ la répartition des valeurs de $\bleu{X}$ quand pour $\bleu{y_{2}}$ :   
   \demitab \{$n_{1,2}$, $n_{2,2}$, $n_{3,2}$, $n_{4,2}$...\}  

## 2) Droite de régression

Nous cherchons la droite d'équation $\mauve{\hat{y} = a + bx}$ qui ajuste au mieux le nuage de points.  
   \demitab l'écart qui demeure entre \rouge{ŷ} (prédite, ou "valeur ajustée")  
   \demitab et $y$ est appelé \definition{résidu} ou \definition{erreur d'ajustement}
\begingroup \large $$ \rouge{e_{i}} = y_{i} - \hat{y}_{i} $$
$$ \rouge{b} = \frac{s_{xy}}{s^2_{x}} = r.\frac{s_{y}}{s_{x}} $$
$$ \rouge{a} = \bar{y} - b\bar{x}$$ \endgroup

ou encore :
\begingroup \large $$ \rouge{\hat{y}} = \bar{y} + \frac{s_{xy}}{s^2_{x}}(x - \bar{x}) $$ \endgroup

\bigbreak

Cette droite passe par le centre de gravité.

### a) la moyenne de la variable ajustée ŷ

   - La somme des $\mauve{e_{i}}$ (et donc sa moyenne) est nulle  
     \demitab car les erreurs s'annulent  

     \demitab ($\rightarrow$ nous sommes sur une droite de régression :  
        \tab de temps en temps ça tombe au dessus,  
        \tab de temps en temps en dessous,  
        \tab mais globalement ça fait zéro)

\bigbreak

   - La moyenne des $\mauve{\hat{y}}$ est égale à $\bar{y}$  
     \demitab ce qui est aussi normal, puisque $\hat{y}$ tourne autour de y, et est censé le représenter fidèlement.

### b) sa variance

La variance de la variable ajustée \mauve{ŷ} est :
\begingroup \large $$ \rouge{s^2_{reg}} = r^2.s^2_{y} $$ \endgroup

\bigbreak

Celle de sa variable résiduelle (d'erreurs) :
\begingroup \large $$ \rouge{s^2_{y.x}} = (1 - r^2)s^2_{y} $$ \endgroup

\bigbreak

Et globalement : 
$$ \mauve{s^2_{y}} = s^2_{reg} + s^2_{y.x} $$

\bigbreak

   - $\mauve{s^2_{reg}}$ est la variance expliquée par la dépendance linéaire de Y en X.

   - $\mauve{s^2_{y.x}}$ celle qui n'est pas expliquée par cette dépendance.  
     \demitab Cette variance résiduelle est une mesure de la qualité de l'ajustement :  
        \tab plus elle est faible, meilleur il est.

   - $\mauve{r^2}$ est appelé le __coefficient de détermination__.  
     \demitab Il dit quel __pourcentage explique $s_{reg}$__ de la dépendance linéaire de Y en X :  
        \tab $\rightarrow$ sa capacité prédictive.
\begingroup \large $$ \rouge{r^2} = 1 - \frac{s^2_{y.x}}{s^2_{x}} $$ \endgroup

### c) r n'a de sens qu'avec les dépendances linaires
 
   - Corrélation n'implique pas causalité.  
     si la droite de régression ajuste bien un nuage, la valeur \underline{absolue} de \mauve{$r$} sera proche de 1,  
     mais l'inverse n'est pas vrai !

   - \mauve{$r$} ne peut déterminer que les dépendances linéraires.  
     Les autres le piègeront.

### d) Observation, pour contrôle, du graphique des résidus
 
Si l'on fait un __graphique des résidus__ $\mauve{e_{i}}$  

   - les points doivent être répartis équitablement au dessus et au dessous de la ligne des abscisses  
   - et sa boite à moustache ne fait pas apparaître de valeurs extérieures

\bigbreak

Si une structure particulière, au contraire, apparaît, l'ajustement n'est pas linéaire.

