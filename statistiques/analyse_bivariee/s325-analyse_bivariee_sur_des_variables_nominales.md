---
title: "s325. Analyse bivariée sur des variables ordinales"
subtitle: ""
author: Marc Le Bihan
---

Pour une matrice  
   \demitab aux colonnes \bleu{A}, \bleu{B}, \bleu{C}  
   \demitab et aux lignes \bleu{E} et \bleu{F}  

\bigbreak

ce sont les __effectifs théoriques__  
   \demitab que l'on s'attend à trouver pour (E,A), (E,B), {E,C), (F,A), (F,B), (F,C)  
   \demitab que l'on compare à ceux observés dans les faits.

