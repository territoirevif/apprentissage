---
title: "s320. Analyse bivariée sur des variables ordinales"
subtitle: ""
author: Marc Le Bihan
---

Ce ne sont plus les valeurs qui nous intéressent,  
   \demitab mais __leur position les unes par rapport aux autres__  
   \demitab dans deux séries de valeurs

## 1) Le coefficient de corrélation de Spearman

On calcule avec les rangs des valeurs dans les séries  
le __coefficient de corrélation de Spearman__.

\bigbreak

Il s'interprète comme celui de _Beauais-Pearson_, mais se calcule différement.
$$ \rouge{r} = 1 - \frac{6\sum^n_{i=1}{[R(x_{i}) - R(y_{i})]^2}}{n(n^2 - 1)} $$ 

\underline{Exemple :} Deux juges notent des sportifs :

\begingroup \scriptsize

| | | | | | | | | | | 
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | ---
| Notes $x_{i}$ | 8.3 | 7.6 | 9.1 | 9.5 | 8.4 | 6.9 | 9.2 | 7.8 | 8.6 | 8.2
| $R(x_{i})$ |  5 |  2 |  8 |  10 |  6 |  1 |  9 |  3 |  7 |  4 
| | | | | | | | | | |
| Notes $y_{i}$ | 7.9 | 7.4 | 9.1 | 9.3 | 8.4 | 7.5 | 9.0 | 7.2 | 8.2 | 8.1
| $R(y_{i})$ |  4 |  2 |  9 |  10 |  7 |  3 |  8 |  1 |  6 |  5

\endgroup

   \code
   
   > ```java
   > Public void ordonneSpearmanDeuxJugesNotentDesSportifs() {
   >    List<Row> data = Arrays.asList(
   >       RowFactory.create(Vectors.dense(8.3, 7.9)),
   >       RowFactory.create(Vectors.dense(7.6, 7.4)),
   >       RowFactory.create(Vectors.dense(9.1, 9.1)),
   >       RowFactory.create(Vectors.dense(9.5, 9.3)),
   >       RowFactory.create(Vectors.dense(8.4, 8.4)),
   >       RowFactory.create(Vectors.dense(6.9, 7.5)),
   >       RowFactory.create(Vectors.dense(9.2, 9.0)),
   >       RowFactory.create(Vectors.dense(7.8, 7.2)),
   >       RowFactory.create(Vectors.dense(8.6, 8.2)),
   >       RowFactory.create(Vectors.dense(8.2, 8.1))
   >    );
   > 
   >     StructType schema = new StructType(new StructField[]{
   >       new StructField("features", new VectorUDT(), false, Metadata.empty()),
   >     });
   > 
   >     Dataset<Row> df = this.session.createDataFrame(data, schema);
   > 
   >     Row r = Correlation.corr(df, "features", "spearman").head();
   >     LOGGER.info("Correlation entre les notes des deux juges pour les sportifs : {}", r.get(0).toString());
   > }
   > ```
  
   \endgroup

## 2) Coefficient de corrélation de Kendall

Détection de paires concordantes et de paires discordantes.

