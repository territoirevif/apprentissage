---
title: "s310. Analyse bivariée : dépendance et représentations graphiques"
subtitle: ""
author: Marc Le Bihan

keywords:
  - indépendance de deux variables
  - nuage de points = graphique de dispersion
---

## 1) Que signifie "des variables associées" ?

Une association ne signifie pas forcément causalité.

   \tab le nombre de cinémas et d'église dans une ville sont liés...  
   \tab ....mais n'ont pas de cause à effet.

On mesure le __degré d'association__ de deux variables.

### a) Dépendance fonctionnelle
 
Une \definition{dépendance fonctionnelle} est __précise__  
\demitab comme en physique où l'on écrit $vitesse =  distance / temps$.

\bigbreak

La taille d'une personne influe sur son poids $\rightarrow$ ces variables ne sont pas indépendantes

   \demitab mais il n'y a pas de dépendance fonctionnelle 
   \demitab parce que l'on ne peut pas écrire : 
   \demitab "_Si une personne fait 1m75, alors elle fait 75kg_"

\bigbreak

Cette association n'est pas __symétrique__ :  
   \demitab si le poids de la personne augmente, sa taille n'augmentera pas pour autant...

### b) Dépendance statistique

Dans notre exemple, il s'agit d'une \definition{dépendance statistique}.

   - la variable $\bleu{Y}$ qui évolue en fonction de $X$ s'appelle la __variable dépendante__.
   - Et $\bleu{X}$, la __variable explicative__.  

## 2) Alternative à une analyse bivariée : le test d'indépendance pour détecter une interaction

Sur un [tableau de contigences](./s215-le_test_du_Khi_Deux-independance_ou_adequation.pdf#contingence-khi-deux), on peut réaliser un \definition{Test du Khi-Deux} pour déterminer s'il y a interaction entre deux variables : c'est une alternative à une analyse bivariée.

   \demitab Sa limite est qu'il n'est employable que s'il y a peu de valeurs distinctes  
   \demitab (sinon le nombre de colonnes Y et le nombre de lignes X deviennent démesurés).

   \demitab Il faut alors faire des regroupements en classes.

## 3) Représentations graphiques

Quand on met dans un graphe

   \demitab une variable en ordonnée et l'autre en abscisse  
   \demitab pour afficher des points en (x, y)

   \tab $\rightarrow$ on fait déjà une analyse bivariée de visu.

### a) Graphique de dispersion (nuage de points)

Lorsque les valeurs étudiées sont quantitatives  

   \demitab elles peuvent être représentées par un __nuage de points__ appelé __graphique de dispersion__

   - sur chaque axe, l'on peut marquer la moyenne de la série se rapportant à cet axe.
   - et mettre une boite à moustache pour décrire ses quartiles et bornes.

### b) Représentation graphique d'un tableau de contigences

On peut tirer d'un tableau de contingences un graphique
(voir p. 148, 149)

