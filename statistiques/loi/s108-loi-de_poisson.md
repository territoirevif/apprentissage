---
title: "s108. La loi de Poisson"
subtitle: ""
author: Marc Le Bihan
keywords:
- loi de Poisson
- paramètres de la loi
- fonction de densité
- fonction de répartition
- variance
- espérance
- indicateur de tendance centrale
- indicateur de distribution
- cdf, ppf, pmf
- python
---

La loi de Poisson s'applique à des valeurs discrètes (entières).

On l'appelle la loi des évènements rares,  
\demitab Utilisée dans les situations où il y a un très grand nombre d'expériences,  
\demitab qui ont chacune une probablité très faible d'advenir.

## 1) Les paramètres de la distribution de Poisson $P(\lambda)$

$$ \mathbf{\textcolor{blue}{X \sim  P(\lambda)}} $$

   \tab \bleu{$\lambda$} \demitab un entier positif  

## 2) La fonction de densité et de répartition de sa distribution

### a) Sa fonction de densité

   $$ P(X = \bleu{k}) = e^{\mauve{-\lambda}}.\left(\frac{\lambda^{\bleu{k}}}{\bleu{k}!}\right) $$

\bigbreak

\underline{Python :} $P(X = 3)$ pour $X \sim P(2)$

   \methode{pmf}{(x)} est la _Print Mass Function_, qui renvoie la valeur de $P(X = \bleu{x})$

\bigbreak

   \code
 
   > ```python
   > from scipy.stats import poisson
   >
   > X = poisson(2)
   > print("P(X = 3) =", X.pmf(3))
   > ```

   > ```log
   > P(X = 3) = 0.18044704431548356
   > ```

   \endgroup

### b) Sa fonction de répartition

est compliquée (une fonction gamma incomplète, d'après Internet)

\underline{Python :} $P(X <= 2)$ et $P(X <= ?) = 0.8$ pour $P(2)$

   \methode{cdf}{(x)} est la _Cumulative Distribution Function_, qui renvoie la valeur de $P(X \le \bleu{k})$  
   \methode{ppf}{(x)} est la _Percent Point Function_, qui renvoie la valeur de $P(X \le \ ?) = \bleu{\text{probabilté}}$  

\bigbreak

   \code
 
   > ```python
   > from scipy.stats import poisson
   >
   > X = poisson(2)
   > print("P(X <= 2) =", X.cdf(2))
   > print("P(X <= ?) = 0.8 pour", X.ppf(0.8)
   > ```

   > ```log 
   > P(X <= 2) = 0.6766764161830634
   > P(X <= ?) = 0.8 pour 3.0
   > ```

   \endgroup

## 3) Ses indicateurs de tendance centrale et de distribution

### a) Espérance

   $$ E(X) = \lambda $$

   \code

   > ```python
   > from scipy.stats import poisson
   > 
   > X = poisson(2)
   > print("moyenne = ", X.mean())
   > print("médiane = ", X.median())
   > ```

   > ```log
   > moyenne =  2.0
   > médiane =  2.0
   > ```

   \endgroup

### b) Variance

   $$ V(X) = \lambda $$

   \code

   > ```python
   > from scipy.stats import poisson
   > 
   > X = poisson(2)
   > print("variance = ", X.var())
   > print("écart-type = ", X.std())
   > ```

   > ```log
   > variance =  2.0
   > écart-type =  1.4142135623730951
   > ```

   \endgroup

## 4) Ses propriétés particulières

### a) Si X et Y sont indépendants et suivent des lois de Poisson

 Si __X__ et __Y__ suivent des lois de Poisson ($\mathbf{X \sim P(\lambda_{1})}$ et $\mathbf{Y \sim P(\lambda_{2})}$)  
\demitab et qu'\attention{elles sont indépendantes},

   alors la somme de $\mathbf{X + Y}$ suit la loi de Poisson $\mathbf{P(\lambda_{1} + \lambda_{2})}$.

### b) Une loi binomiale peut être approximée par une loi de Poisson
   
S'il y a beaucoup d'éléments ($\mathbf{n > 50}$)  
\demitab et une probabilité très faible de succès de l'expérience ($\mathbf{p < 0.1})$,

une \avantage{loi binomiale peut être approximée par une loi de Poisson} : $\mathbf{B(n,p) \approx P(\lambda)}$

