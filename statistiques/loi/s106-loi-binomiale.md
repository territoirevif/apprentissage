---
title: "s106. La loi Binomiale"
subtitle: ""
author: Marc Le Bihan
keywords:
- loi Binomiale
- paramètres de la loi
- fonction de densité
- fonction de répartition
- variance
- espérance
- indicateur de tendance centrale
- indicateur de distribution
- cdf, ppf, pmf
- python
---

La loi Binomiale s'applique à une distribution discrète (valeurs entières).

   \demitab On l'utilise lorsque l'on répète plusieurs fois une expérience  
   \demitab qui a une probabilité constante de réussir.

## 1) Les paramètres de la distribution Binomiale

$$ \mathbf{\textcolor{blue}{X \sim B(n, p)}} $$

   \tab \bleu{$n$} \demitab nombre de répétitions de l'expérience que l'on fait  
   \tab \demitab ($\rightarrow$ la valeur de la variable aléatoire $X$ ne dépassera jamais $n$)

   \tab \bleu{$p$} \demitab la probabilité de succès d'une expérience  
   \tab \bleu{$k$} \demitab le nombre de succès

\bigbreak

C'est la probabilité d'obtenir $\bleu{k}$ succès  
\demitab à l'expérience qui a une probabilité de succès de $\bleu{p}$  
\demitab et qu'on répète $\bleu{n}$ fois.  

## 2) La fonction de densité et de répartition de sa distribution

### a) Sa fonction de densité

   $$ P(X = k) = \begingroup \large(^{x}_{n}).p^{k}.q^{n-k}\endgroup $$

\bigbreak

   \underline{Exemple :} La probabilité de tirer dans un jeu de cartes $\bleu{k=3}$  
   \demitab piques ($\bleu{p = 0.25}$)  
   \demitab au bout de $\mathbf{n=5}$ tirages  
   \demitab est de $\rouge{9\%}$.

\bigbreak

\underline{Python :} $P(X = 3)$ pour $X \sim \mathcal{B}(7, 0.65)$

   \methode{pmf}{(x)} est la _Print Mass Function_, qui renvoie la valeur de $P(X = \bleu{x})$

\bigbreak

   \code
 
   > ```python
   > from scipy.stats import binom
   > 
   > X = binom(7, 0.65)
   > print("P(X = 3) =", X.pmf(3))
   > ```

   > ```log
   > P(X = 3) = 0.14423819921874997
   > ```

   \endgroup

### b) Sa fonction de répartition

   $$ F(x \leq k) = \begingroup \large \sum_{j=0}^{k}{(^{n}_{j}). p^{j}.q^{n-j}} \endgroup $$

\bigbreak

\underline{Exemples :}

   - La probabilité d'avoir trois piques ou moins  
     \demitab ($\bleu{p = 0.25}$) de tirage d'un pique)  
     \demitab au bout de $\bleu{n=5}$ tirages 
     \demitab est de $\rouge{98\%}$.
    
   - Si un étudiant répond au hasard à un QCM dont les questions ont $\bleu{5}$ choix  
     \demitab (la chance de bien répondre à une question est $\bleu{p = 0.2}$),  
     \demitab il faudra $\rouge{n \geq 13}$ questions à ce QCM  
     \demitab pour qu'il ait moins de 10% de chances d'avoir la moyenne :  
     \demitab $\mathbf{F(X \geq n/2) \leq 0.1}$

\bigbreak

\underline{Python :} $P(X <= 5)$ et $P(X <= ?) = 0.8$ pour $\mathcal{B}(7, 0.65)$

   \methode{cdf}{(x)} est la _Cumulative Distribution Function_, qui renvoie la valeur de $P(X \le \bleu{k})$  
   \methode{ppf}{(x)} est la _Percent Point Function_, qui renvoie la valeur de $P(X \le \ ?) = \bleu{\text{probabilté}}$  

\bigbreak

   \code
 
   > ```python
   > from scipy.stats import binom
   > 
   > X = binom(7, 0.65)
   > print("P(X <= 5) =", X.cdf(5))
   > print("P(X <= ?) = 0.8 pour", X.ppf(0.8))  
   > ```

   > ```log 
   > P(X <= 5) = 0.7662014390624999
   > P(X <= ?) = 0.8 pour 6.0
   > ```

   \endgroup

## 3) Ses indicateurs de tendance centrale et de distribution

### a) Espérance et médiane

   $$ E(X) = np  $$

   \code

   > ```python
   > from scipy.stats import binom
   > 
   > X = binom(7, 0.65)
   > print("moyenne = ", X.mean())
   > print("médiane = ", X.median())
   > ```

   > ```log
   > moyenne =  4.55
   > médiane =  5.0   
   > ```

   \endgroup

### b) Variance et écart-type

   $$ V(X) = npq $$  
 
   \code

   > ```python
   > from scipy.stats import binom
   > 
   > X = binom(7, 0.65)
   > print("variance = ", X.var())
   > print("écart-type = ", X.std())
   > ```

   > ```log
   > variance =  1.5924999999999998
   > écart-type =  1.2619429464123961
   > ```

   \endgroup

