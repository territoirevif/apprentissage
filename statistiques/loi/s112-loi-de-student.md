---
title: "s112. La loi de Student"
subtitle: ""
author: Marc Le Bihan
keywords:
- loi de Student 
- test de Student
- comparaison de moyenne d'échantillon
- test du coefficient de corrélation de Pearson
---

La loi de Student est utilisée pour :

   - Les comparaisons de moyenne d'échantillons
   - Le test du coefficient de corrélation de Pearson

\bigbreak

donne la valeur d'un $\rouge{t}$ qui sert à ces tests, appelés _tests de Student_.

\bigbreak

Sa forme la fait ressembler à une loi normale.

## 1) Les paramètres de la distribution de Student


