---
title: "s100. Les Lois de probabilités"
subtitle: "une loi est l'idéalisation d'une distribution"
author: Marc Le Bihan
keywords:
- loi
- loi de probabilités
- distirubtion
- expérience
- échantillon
- moyenne
- variance
- indicateurs de distribution
- indicateurs de dispersion
- degré de liberté
- indicateurs de tendance centrale
- paramètre d'une loi
- famille de lois
- suivre une loi
- modèle
- prédiction
---

## 1) L'idéalisation d'une distribution

Une \definition{Loi} est l'idéalisation d'une distribution :  
si l'on répétait l'expérience un grand nombre de fois.

\bigbreak

Quand on commence à dire :

   - la moyenne d'un échantillon est celle-ci 
   - sa variance est celle-là
   - à lui trouver d'autres caractéristiques...

\bigbreak

alors, on peut commencer aussi à faire des hypothèses sur ce que l'on trouverait si : 

   - l'on prenait d'autres échantillons 
   - ou si l'on regardait la population totale

\bigbreak

\avantage{Nous décrivons une loi qu'on croit exister,}   
\avantage{et qui dit : \emph{"il se passe généralement ceci."}}

et on peut faire des prédictions.

## 2) Une Loi a des paramètres

Cette loi, connue ou non, a des \definition{paramètres}  

### a) Il faut les estimer à partir d'indicateurs

Si on ne connaît pas ces paramètres, ils doivent être estimés.  
Et c'est typiquement à partir d'un \definition{échantillon} qu'on le fait :  
   
   - ses \definition{indicateurs} de \definition{tendance centrale} de notre distribution
   - et ses indicateurs de \definition{dispersion / distribution}

Parfois, ce paramètre est un \definition{degré de liberté}.

### b) Si les paramètres ne sont pas estimés, nous sommes devant une famille de Lois
 
   - $\textcolor{blue}{N(m, \sigma^2)}$ est une famille de lois.  
   - alors que $\textbf{\textcolor{OliveGreen}{N(5, 1)}}$ est l'instance de la loi (distribution)  
     qui s'applique à une population particulière que l'on a observée.

## 3) Un emploi des lois : déterminer si un échantillon suit une loi

une fois une loi établie, \avantage{l'on peut prédire de futures valeurs ou évènements},  
\demitab et comme aboutissement, définir un \definition{modèle} qui reliera les variables aléatoires entre-elles.

