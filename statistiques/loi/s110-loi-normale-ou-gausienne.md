---
title: "s110. La loi Normale ou Gaussienne"
subtitle: ""
author: Marc Le Bihan
keywords:
- loi Normale
- loi de Gauss
- loi gaussienne
- paramètres de la loi
- fonction de densité
- fonction de répartition
- variance
- espérance
- indicateur de tendance centrale
- indicateur de distribution
- cdf, ppf, pdf
- python
---

La loi Normale ou Gaussienne s'applique à une distribution continue (valeurs réelles).

## 1) Les paramètres de la distribution Normale $\mathbf{X \sim N(m, \sigma^2)}$

$$ \bleu{\mathbf{X \sim N(m, \sigma^2)}} $$

ou sa version qui utilise des valeurs centrées réduites :  
$$ \bleu{\mathbf{\frac{X - m}{\sigma} \sim N(0,1)}} $$

\bigbreak

   \tab $\bleu{m}$ ou $\bleu{\mu}$ \demitab moyenne  
   \tab $\bleu{\sigma^2}$ \demitab \tab variance  

## 2) La fonction de densité et de répartition de sa distribution

### a) Sa fonction de densité

$$ \bleu{\mathbf{p(X = x)}} = \frac{1}{\sigma\sqrt{2\pi}}e^{-\frac{1}{2\sigma^2}(x-m)^2} $$

\bigbreak

\underline{Python :} $P(X = 3)$ pour $X \sim \mathcal{N}(20, 4)$

\bigbreak

   En _Python_, la loi \fonction{norm} de \fonction{scipy} prend pour deuxième paramètre un écart-type ($\sigma$).

   \methode{pdf}{(x)} est la _Print Density Function_, qui renvoie la valeur de $P(X = \bleu{x})$

\bigbreak

   \code
 
   > ```python
   > from scipy.stats import norm
   > 
   > X = norm(20, 4)
   > print("P(X = 3) =", X.pdf(3))
   > ```

   > ```log 
   > P(X = 3) = 1.1929659135301238e-05
   > ```

   \endgroup

### b) Sa fonction de répartition

\underline{Python :} $P(X <= 12)$ et $P(X <= ?) = 0.8$ pour $\mathcal{N}(20, 4)$

   \methode{cdf}{(x)} est la _Cumulative Distribution Function_, qui renvoie la valeur de $P(X \le \bleu{k})$  
   \methode{ppf}{(x)} est la _Percent Point Function_, qui renvoie la valeur de $P(X \le \ ?) = \bleu{\text{probabilté}}$  

\bigbreak

   \code
 
   > ```python
   > from scipy.stats import norm
   > 
   > X = norm(20, 4)
   > print("P(X <= 12) =", X.cdf(12))
   > print("P(X <= ?) = 0.8 pour X =", X.ppf(0.8))
   > ```

   > ```log 
   > P(X <= 12) = 0.022750131948179195
   > P(X <= ?) = 0.8 pour X = 23.366484934291655
   > ```

   \endgroup

## 3) Ses indicateurs de tendance centrale et de distribution

### a) Espérance et médiane

   $$ \bleu{\mathbf{E(X)}} = m $$

   \code
 
   > ```python
   > from scipy.stats import norm
   > 
   > X = norm(20, 4)
   > print("moyenne = ", X.mean())
   > print("médiane = ", X.median())
   > ```

   > ```log 
   > moyenne =  20.0
   > médiane =  20.0
   > ```

   \endgroup

\bigbreak

\underline{Espérance \textbf{de la moyenne} d'une loi normale} :
   $$ \bleu{\mathbf{\overline{X}}} \sim N\left(m, \frac{\sigma}{\sqrt{n}}\right) $$

ou, pour sa version utilisant des valeurs centrées réduites :
   $$ \bleu{\mathbf{\frac{\overline{X} - m}{\frac{\sigma}{\sqrt{n}}}}} \sim N(0, 1) $$

### b) Variance et écart-type

   $$ \bleu{\mathbf{V(X)}} = \sigma^2 $$

   \code
 
   > ```python
   > from scipy.stats import norm
   > 
   > X = norm(20, 4)
   > print("variance = ", X.var())
   > print("écart-type = ", X.std())   
   > ```

   > ```log 
   > variance =  16.0
   > écart-type =  4.0
   > ```

   \endgroup

### c) Applatissement

Son applatissement (\definition{kurtosis} : moment d'ordre 4) est égal à 3.

Certains logiciels retranchent ce 3 pour en faire un étalon à 0  
   \demitab en l'appelant \definition{coefficient de Fisher} ou \definition{excess kurtosis}.

## 4) Ses propriétés particulières

- Si $\mathbf{X}$ et $\mathbf{Y}$ suivent des lois normales ($\mathbf{X_{1} \sim N(m_{1}, \sigma_{1})}$ et $\mathbf{X_{2} \sim N(m_{2}, \sigma_{2})}$)  
\demitab et \attention{qu'elles sont indépendantes},  
\tab alors la somme de $\mathbf{X_{1} + X_{2}}$  
\tab suit la loi normale $\mathbf{N(m_{1} + m_{2}, \sqrt{\sigma_{1} + \sigma_{2}}})$

- si $\mathbf{X}$ suit une loi normale de moyenne $\mathbf{m}$ et de variance $\mathbf{\sigma}$  
\tab alors $\mathbf{aX+b}$ suit une loi normale $\mathbf{N(am+b, |a|\sigma)}$

