---
title: "s118. La loi Gamma"
subtitle: ""
author: Marc Le Bihan
keywords:
- loi Gamma
---

## 1) Les paramètres de la distribution Gamma

de paramètres $a$ et $b$

moyenne 

\begingroup \large $\mu = \frac{a}{b}$ \endgroup 

variance 

\begingroup \large $\psi = \frac{a}{b^2}$\endgroup
 
