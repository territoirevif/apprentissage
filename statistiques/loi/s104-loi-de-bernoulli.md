---
title: "s104. La loi de Bernoulli"
subtitle: ""
author: Marc Le Bihan
keywords:
- loi de Bernoulli
- paramètres de la loi
- fonction de densité
- fonction de répartition
- variance
- espérance
- indicateur de tendance centrale
- indicateur de distribution
- cdf, ppf, pmf
- python
---

Une loi de Bernoulli s'applique à une distribution discrète (valeurs entières),  
   \demitab pour une unique expérience.

   \demitab Elle a souvent pour exemple un unique lancé de dé ou de pièce,  
   \tab avec sa probabilité d'avoir un résultat donné.

## 1) Les paramètres de la distribution de Bernoulli

$$ \mathbf{\textcolor{blue}{X \sim B(1, p)}} $$

   \tab \bleu{$p$} \demitab la probabilité de succès  
   \tab \bleu{$q$} \demitab la probabilité d'échec, égale à \bleu{1 - p}

## 2) La fonction de densité et de répartition de sa distribution

### a) Sa fonction de densité

\underline{Python :} $P(X = 1)$ et $P(X = 0)$ pour $\mathcal{B}(0.4)$

   \methode{pmf}{(x)} est la _Print Mass Function_, qui renvoie la valeur de $P(X = \bleu{x})$

\bigbreak

   \code
 
   > ```python
   > from scipy.stats import binom
   > 
   > X = binom(1, 0.4)
   > print("P(X = 1) =", X.pmf(1))
   > print("P(X = 0) =", X.pmf(0))
   > ```

   > ```log
   > P(X = 1) = 0.4
   > P(X = 0) = 0.6  
   > ```

   \endgroup

### b) Sa fonction de répartition

Si $\bleu{k}$ est strictement inférieur à 1, c'est que l'expérience n'a connu aucun succès  
\demitab $\rightarrow$ cela n'a qu'\bleu{$1 - p$} chances de se produire.
 
$k < 0$ est une valeur "_illégale_" et sert de bouchon.

\bigbreak
 
| k            | $k < 0$ | $0 \leq k \le 1$  | $k \geq 1$ |   
| ------------ | ------- | ----------------- | ---------- |
| $F(X \le k)$ | 0       | $1 - p$           | 1          |

\bigbreak

\underline{Python :} $P(X <= 1)$ et $P(X <= ?) = 1$ pour $\mathcal{B}(0.4)$

   \methode{cdf}{(x)} est la _Cumulative Distribution Function_, qui renvoie la valeur de $P(X \le \bleu{k})$  
   \methode{ppf}{(x)} est la _Percent Point Function_, qui renvoie la valeur de $P(X \le \ ?) = \bleu{\text{probabilté}}$  

\bigbreak

   \code
 
   > ```python
   > from scipy.stats import binom
   > 
   > X = binom(1, 0.4)
   > print("P(X <= 1) =", X.cdf(1))
   > print("P(X <= ?) = 1 pour", X.ppf(1))
   > ```

   > ```log 
   > P(X <= 1) = 1.0
   > P(X <= ?) = 1 pour 1.0
   > ```

   \endgroup

## 3) Ses indicateurs de tendance centrale et de distribution
 
### a) Espérance et médiane

   $$ E(X) = p $$

   \code
 
   > ```python
   > from scipy.stats import binom
   > 
   > X = binom(1, 0.4)
   > print("moyenne = ", X.mean())
   > print("médiane = ", X.median())
   > ```

   > ```log
   > moyenne =  0.4
   > médiane =  0.0
   > ```

   \endgroup

### b) Variance et écart-type

   $$ V(X) = pq = p(1 - p) $$ 

   \code
 
   > ```python
   > from scipy.stats import binom
   > 
   > X = binom(1, 0.4)
   > print("variance = ", X.var())
   > print("écart-type = ", X.std())
   > ```

   > ```log
   > variance =  0.24
   > écart-type =  0.4898979485566356
   > ```

   \endgroup

