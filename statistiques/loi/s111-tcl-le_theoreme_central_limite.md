---
title: "s111. Le Théorème Cental Limite (TCL)"
subtitle: ""
author: Marc Le Bihan
keywords:
- Théorème Central Limite (TCL)
- TCL
- loi normale
- sous-population
---

## 1) Pourquoi le Théorème Cental Limite existe t-il ?

En biologie, le comportement de chaque élément ou individu  
\demitab est en général déterminé par un grand nombre de sous-éléments ou entités élémentaires,  
\demitab qui se comportent chacune comme une variable aléatoire.

\bigbreak

Pour cette raison, beaucoup de distributions  
\demitab pour peu qu'elles aient beaucoup d'éléments,  
\demitab suivent une loi normale.

## 2) Quand les distributions ne le suivent-ils PAS ?

  - quand elles suivent des distributions uniformes (exemple : variables circulaires : saisons ou jours dans l'année)  
  - ou une loi de Poisson (évènements rares)
  - ou binomiale (chances d'avoir $X=0, 1, 2...$)

\bigbreak
  
ainsi que pour les __sous-populations__ :

on étudie une espèce et voilà que se trouve dedans plusieurs variétés,  
\demitab menant à des distributions polymodales.

