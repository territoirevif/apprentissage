---
title: "s116. La loi du Khi-Deux"
subtitle: ""
author: Marc Le Bihan
keywords:
- loi du Khi-Deux
- Chi-Deux
- indépendance
- test d'indépendance de variables aléatoires
- test d'ajustement
- échantillons qui suivent la même distribution
- test du Khi-Deux
---

La loi du Khi-Deux ($\chi^{2}$) est utilisée pour tester :

   - l'\avantage{indépendance} de variables : \definition{test d'indépendance}
   - si un grand échantillon \avantage{suit une distribution particulière} : \definition{test d'ajustement}
   - si deux échantillons \avantage{suivent la même distribution}.

## 1) Les paramètres de la distribution du Khi-Deux

