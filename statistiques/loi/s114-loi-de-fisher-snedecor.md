---
title: "s114. La loi de Fisher-Snedecor"
subtitle: ""
author: Marc Le Bihan
keywords:
- loi de Fisher
- loi de Fisher-Snedecor
- comparaison de variance d'échantillon
- degré de liberté
---

La loi de Fisher-Snedecor est utilisée pour :

   - Les comparaisons de variance d'échantillons
   - l'analyse de cette variance

## 1) Les paramètres de la distribution de Fisher-Snedecor

Cette distribution est issue d'un quotient de deux variables aléatoires,  
et par là, se caractérise par deux degrés de liberté.

