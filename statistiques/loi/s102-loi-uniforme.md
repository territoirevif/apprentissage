---
title: "s102. La loi uniforme"
subtitle: ""
author: Marc Le Bihan
keywords:
- loi uniforme
- paramètres de la loi
- fonction de densité
- fonction de répartition
- variance
- espérance
- indicateur de tendance centrale
- indicateur de distribution
- cdf, ppf, pmf
- python
---

Une loi Uniforme s'applique à une distribution discrète (valeurs entières) ou continue (réelles)  
   \demitab où chacune des valeurs qu'elle prend a la même probabilité d'apparaître.

## 1) Les paramètres de la distribution uniforme

### a) Paramètre d'une version discrète (valeurs entières) U(n)

   > \bleu{$n$} : nombre d'éléments, dans sa version discrète
$$ \mathbf{\bleu{X \sim U(n)}} $$

### b) Paramètres d'une version continue (valeurs réelles) U([a,b])

   > \bleu{$[a, b]$} : un intervalle de valeurs, pour sa version continue
$$ \mathbf{\bleu{X \sim U[a,b]}} $$

## 2) La fonction de densité et de répartition de sa distribution

Les probabilités associées aux valeurs de $x$ sont les mêmes pour tous les éléments.    

### a) Sa fonction de densité

Sa fonction de densité est constante.
   $$ f(r) = P(X = r) = \frac{1}{n} $$
   $$ f(x) = \frac{1}{b - a} $$

### densité d'une distribution uniforme discrète

\underline{Python:} $P(X = 3)$ pour $U[1,6]$

   \methode{pmf}{(x)} est la _Print Mass Function_, qui renvoie la valeur de $P(X = \bleu{x})$

\bigbreak

Attention à la borne supérieure de la distribution \fonction{randint}, qui est la version discrète de la loi uniforme, en _Python_.  
   \demitab $\mauve{U[1,6]}$ c'est \methode{randint}{(1,7)}.

\bigbreak

\code

> ```python
> from scipy.stats import randint
> 
> X = randint(1,7) # U[1,6]
> print("P(X = 3) =", X.pmf(3))
> ```
 
> ```log
> P(X = 3) = 0.16666666666666666
> ```

\endgroup

### densité d'une distribution uniforme continue

\underline{Python:} $P(X = 2.0)$ pour $U[1.4, 4.1]$

Attention à la borne supérieure de la distribution \fonction{uniform}, qui est la version continue de la loi uniforme, en _Python_.  
Son deuxième paramètre est un `scale`: c'est à dire qu'on entre $U(min, max - min)$  
   \demitab $\mauve{U[1.4, 4.1]}$ c'est \methode{uniform}{(1.4, 2.7)} en _Python_.

\bigbreak

\code

> ```python
> from scipy.stats import uniform
>
> X = uniform(1.4, 2.7)
> print("P(X = 2.0) =", X.pdf(2.0))
> ```
 
> ```log
> P(X = 2.0) = 0.37037037037037035
> ```

\endgroup

### b) Sa fonction de répartition
 
   $$ F(X) = P(X \le r) = \frac{r}{n} $$
   
   > | __x__    | $x < a$ | $a \leq x \leq b$             | $x > b$   |   
   > | -------- | ------- | ----------------------------- | --------- |
   > | __F(X)__ | 0       | $$ \frac{(x - a)}{(b - a)} $$ | 1         |

### répartition d'une distribution uniforme discrète

\underline{Python:} $P(X \le 2)$ et $P(X <= ?) = 0.3$ pour $U[1,6]$

   \methode{cdf}{(x)} est la _Cumulative Distribution Function_, qui renvoie la valeur de $P(X \le \bleu{k})$  
   \methode{ppf}{(x)} est la _Percent Point Function_, qui renvoie la valeur de $P(X \le \ ?) = \bleu{\text{probabilté}}$  

\bigbreak

\code

> ```python
> from scipy.stats import randint
> 
> X = randint(1,7) # U[1,6]
> print("P(X <= 2) =", X.cdf(2))
> print("P(X <= ?) = 0.3 pour X =", X.ppf(0.3))
> ```
 
> ```log
> P(X <= 2) = 0.3333333333333333
> P(X <= ?) = 0.3 pour X = 2.0
> ```

\endgroup

### répartition d'une distribution uniforme continue

\underline{Python:} $P(X \le 1.8)$ et $P(X <= ?) = 0.4$ pour $U[1.4, 4.1]$

\code

> ```python
> from scipy.stats import uniform
> 
> X = uniform(1.4, 2.7)
> print("P(X <= 1.8) =", X.cdf(1.8))
> print("P(X <= ?) = 0.4 pour X =", X.ppf(0.4))
> ```

> ```log
> P(X <= 1.8) = 0.1481481481481482
> P(X <= ?) = 0.4 pour 2.48
> ```

\endgroup

## 3) Ses indicateurs de tendance centrale et de distribution

### a) Espérance et médiane

   $$ E(X) = \frac{n+1}{2} $$
   $$ E(X) = \frac{a+b}{2} $$

### moyenne et médiane d'une distribution uniforme discrète

\underline{Python:} moyenne et médiane d'une distribution discrète $U[1,6]$

\code

> ```python
> from scipy.stats import randint
> 
> X = randint(1,7) # U[1,6]
> print("moyenne = ", X.mean())
 ```
 
> ```log
> moyenne =  3.5
> médiane =  3.0
> ```

\endgroup

### moyenne et médiane d'une distribution uniforme continue

\underline{Python:} moyenne et médiane d'une distribution continue $U[1.4, 4.1]$

\code

> ```python
> from scipy.stats import uniform
> 
> X = uniform(1.4, 2.7)
> print("moyenne = ", X.mean())
> print("médiane = ", X.median())
> ```
 
> ```log
> moyenne =  2.75
> médiane =  2.75
> ```

\endgroup

### b) Variance et écart-type d'une distribution uniforme discrète

   $$ V(X) = \frac{n^2 - 1}{12} $$
   $$ V(X) = \frac{(b - a)^2}{12} $$


   \begingroup \cadreGris

   > $\mathbf{x_{p} = a + p(b - a)}$
   >
   > \ \ \ \ dans un intervalle $[3,5]$  
   > \ \ \ \ $x_{0}$ vaut 3 et $x_{1}$ vaut 5.

   \endgroup

### Variance et écart-type d'une distribution uniforme discrète

\underline{Python:} variance et écart-type pour $U[1,6]$

\code

> ```python
> from scipy.stats import randint
> 
> X = randint(1,7) # U[1,6]
> print("variance = ", X.var())
> print("écart-type = ", X.std())
> ```
 
> ```log
> variance =  2.9166666666666665
> écart-type =  1.707825127659933
> ```

\endgroup

### Variance et écart-type d'une distribution uniforme continue

\underline{Python:} variance et écart-ype pour $U[1.4, 4.1]$

\code

> ```python
> from scipy.stats import uniform
> 
> X = uniform(1.4, 2.7)
> print("variance = ", X.var())
> print("écart-type = ", X.std())
> ```
 
> ```log
> variance =  0.6075
> écart-type =  0.7794228634059948
> ```

\endgroup

