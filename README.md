
La commande de génération de pdf, par Pandoc, placée dans `gen.sh` est :

```bash
gen <fichier markdown, sans son extension md>

pandoc -f markdown-implicit_figures --syntax-definition=$APPRENTISSAGE_HOME/java.xml --syntax-definition=$APPRENTISSAGE_HOME/java.xml --metadata-file=$APPRENTISSAGE_HOME/metadata.json --include-in-header=$APPRENTISSAGE_HOME/apprentissage-header-include.tex --include-in-header=$APPRENTISSAGE_HOME/apprentissage-include.tex "${source}".md -o "${source}".pdf
```

L'indexation Elastic se fait par `index.sh`, qui execute un `curl` :

```bash
index <fichier pdf>
```

ou la soumission d'une série de fichiers :

```bash
for file in t???-*.pdf; do index $file; done
```

L'index `/apprentissage` a été créé par la commande :

```http
PUT apprentissage/
{
  "aliases": {},
  "mappings": {
    "properties": {
      "attachment": {
        "properties": {
          "author": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
          "content": {
            "type": "text",
            "analyzer": "french",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
          "content_length": {
            "type": "long"
          },
          "content_type": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
          "creator_tool": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
          "date": {
            "type": "date"
          },
          "format": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
          "keywords": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
          "language": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
          "modified": {
            "type": "date"
          },
          "title": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          }
        }
      },
      "data": {
        "type": "text",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      }
    }
  }
}
```

à tester :

```tex
\usepackage[nodisplayskipstretch]{setspace}
```
